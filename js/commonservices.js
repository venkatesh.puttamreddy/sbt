ouaApp.service('commonCorpService', function ($http, $rootScope, ServerService) {
    this.corporateCustomers = function () {
        // $scope.tokenresult = JSON.parse(sessionStorage.getItem('authToken'));
        // //  ajaxService.ajaxRequest('POST',ServerService.serverPath+'rest/repo / airport','{ "queryString" : "'+request.term+'" } ','json'),{ 
        // var headersOptions = {
        //     'Content-Type': 'application/json',
        //     'Authorization': 'Bearer ' + $scope.tokenresult
        // }


        return $http({
            method: 'GET',
            url: ServerService.dropDownPath + 'list/corporate-customers',
            // headers: headersOptions
        }).then(function successCallback(response, status) {
            if (response.data.success) {
                $rootScope.corporateCustomers = response.data.data;
                return response.data.data;
                // getLangList();
            } else { }
        }, function errorCallback(response) {
            if (response.status == 403) {
                var status = refreshService.refreshToken(tokenname.refreshToken, tokenname.authToken);
                status.then(function (greeting) {
                    corporateCustomers();
                });
            }
        });
    }

    this.fop = function (selectedCompanyId, id) {

        return $http.get(ServerService.dropDownPath + 'list/fop/corporate-customer/' + selectedCompanyId, {
        }).then(function successCallback(response) {
            if (response.data.success) {
                // $rootScope.companyFOPList = response.data.data;
                $rootScope.fopResponse = response.data.data;
                sessionStorage.setItem('fopResponse',JSON.stringify($rootScope.fopResponse ));
                return response.data.data;
            }
            else {

                $rootScope.companyFOPList = [];

            }

        });


    }


    this.corporateTravelType = function () {
return $http.get(ServerService.listPath + 'crud/list/corpTravelCategoryMaster',{
 
}).then(function successCallback(response) {
    if (response.data.success) {
        // $rootScope.companyFOPList = response.data.data;
        $rootScope.travelTypeResponse = response.data.data;
        return response.data.data;
    }else {

        $rootScope.corporateTravelType = [];

    }

});
      


    }

    this.providerList = function (selectedCompanyId, moduleName) {

        return $http.get(ServerService.dropDownPath + 'list/' + selectedCompanyId + '/' + moduleName + '/providers', {

        }).then(function successCallback(response) {
            if (response.data.success) {

                // $rootScope.companyProviderList = response.data.data;
                $rootScope.providerList = response.data.data;
                $rootScope.providerListvalue = true
                return response.data.data;
            }
            else {
                $rootScope.companyProviderList = [];
            }
        });
    }

    this.cancellationPolicyHotel = function () {

        $rootScope.sessionId=JSON.parse(sessionStorage.getItem('selectedHotelResp')).sessionId;
        $rootScope.selectedHotelResp = JSON.parse(sessionStorage.getItem('selectedHotelResp')).data;
        $rootScope.sessionvalue = sessionStorage.hotelSearchResp;
        
        $rootScope.hotelwisedata=$rootScope.selectedroomInfo;
		$rootScope.totalRooms=[];
		$rootScope.totalRooms.push($rootScope.hotelwisedata);
		$rootScope.tokenresult = JSON.parse(sessionStorage.getItem('authToken'));

        $rootScope.roomCancellation = [{
            "roomIndex": $rootScope.totalRooms[0].roomIndex,
            "roomRefNo": $rootScope.totalRooms[0].roomRefNo,
            "roomCode": $rootScope.totalRooms[0].roomCode
        }]
        var insurenceReq = {
            "sessionId": $rootScope.sessionId,

            "roomDtls": $rootScope.roomCancellation
        }
        return $http.post(ServerService.dropDownPath + 'customer/' + $rootScope.corprateCompanyId + '/hotel/cpolicy', insurenceReq, {
        }).then(function successCallback(response) {
            $rootScope.cancellationPolicy = response.data.data.cancellationPolicy
            sessionStorage.setItem('cancellationPolicy', JSON.stringify($rootScope.cancellationPolicy));
            return $rootScope.cancellationPolicy;

        }, function errorCallback(response) {
            if (response.status == 403) {
                var status = refreshService.refreshToken(tokenname.refreshToken, tokenname.authToken);
                status.then(function (greeting) {
                    rezlivehotelcply();
                });
            }
        });
    }
});