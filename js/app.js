var ouaApp = angular.module('ouaApp', ['ngAnimate','ui.select', 'ngTouch', 'ngSanitize', 'ui.bootstrap', 'ui.router', 'ngTasty', 'ui', 'ui-rangeSlider', 'daterangepicker', 'uiSwitch', 'duScroll', 'ngCookies', 'checklist-model', 'rzModule', 'localytics.directives', 'angularUtils.directives.dirPagination', 'angularytics']);

ouaApp.run(function(Angularytics) {
    Angularytics.init();

});

ouaApp.filter('unsafe', function($sce) { return $sce.trustAsHtml; });

ouaApp.config(function($stateProvider, $urlRouterProvider, $httpProvider, AngularyticsProvider,$qProvider) {
    $cookiesSessionId = Math.random().toString(36).substr(2);
	document.cookie = 'CSRF-TOKEN='+$cookiesSessionId+';path=/'
 // window.ga('create', 'UA-144585841-1', 'auto'); 
    // $rootScope.$on('$stateChangeSuccess', function (event) {
    // $window.ga('send', 'pageview', $location.path());
    // });
    // window.scroll(0,0);
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }
    $('html, body').animate({scrollTop:0},500);

    // Answer edited to include suggestions from comments
    // because previous version of code introduced browser-related errors

    //disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
    // extra
    $httpProvider.defaults.headers.get['Cache-Control'] = 'no-cache';
    $httpProvider.defaults.headers.get['Pragma'] = 'no-cache';
    AngularyticsProvider.setEventHandlers(['Console', 'GoogleUniversal']);
    $urlRouterProvider.otherwise('/home');


    // if(sessionStorage.timer){
    //     expiryService.sessionExpire();
    // }
    $stateProvider.state('home', {
        url: '/home',
        templateUrl: 'views/home/menuredirect.html',
        controller: 'menuRedirectController',
        resolve: {
            init: function(sessionService, $stateParams, $state, $rootScope) {
                sessionService.clearRouteData('home');
                sessionStorage.removeItem('activeService');
                // window.scrollTo(0, 0);
                angular.element('#flight-menu').addClass("active");
                angular.element('#hotel-menu').removeClass("active");
                angular.element('#holiday-menu').removeClass("active");
                angular.element('#uae-menu').removeClass("active");
                angular.element('#insurance-menu').removeClass("active");
                  $rootScope.tabmenuactive = 1; 
                 sessionStorage.setItem("activeService", $rootScope.tabmenuactive);
            }
        }
    }).state('flight', {
        url: '/flight',
        templateUrl: 'views/home/flight.html',
        controller: 'homeController',
        resolve: {
            init: function(sessionService, $stateParams, $state, $rootScope) {
                sessionService.clearRouteData('flight');
                // document.body.scrollTop = document.documentElement.scrollTop = 0;
                // window.scrollTo(0, 0);
                angular.element('#flight-menu').addClass("active");
                angular.element('#hotel-menu').removeClass("active");
                angular.element('#holiday-menu').removeClass("active");
                angular.element('#uae-menu').removeClass("active");
                angular.element('#insurance-menu').removeClass("active");
                 $rootScope.tabmenuactive = 1;
                 sessionStorage.setItem("activeService", $rootScope.tabmenuactive);

            }
        }
    }).state('hotel', {
        url: '/hotel',
        templateUrl: 'views/home/hotel.html',
        controller: 'homeController',
        resolve: {
            init: function(sessionService, $stateParams, $state, $rootScope) {
                sessionService.clearRouteData('hotel');
                // document.body.scrollTop = document.documentElement.scrollTop = 0;
                window.scrollTo(0, 0);
                angular.element('#flight-menu').removeClass("active");
                angular.element('#hotel-menu').addClass("active");
                angular.element('#holiday-menu').removeClass("active");
                angular.element('#insurance-menu').removeClass("active");
                angular.element('#uae-menu').removeClass("active");

                $rootScope.tabmenuactive = 2;
                sessionStorage.setItem("activeService", $rootScope.tabmenuactive);

            }
        }
    }).state('holiday', {
        url: '/holiday',
        templateUrl: 'views/holiday/holiday.html',
        controller: 'holidayController',
        resolve: {
            init: function(sessionService, $stateParams, $state, $rootScope) {
                sessionService.clearRouteData('holiday');
                // document.body.scrollTop = document.documentElement.scrollTop = 0;
                window.scrollTo(0, 0);
                angular.element('#flight-menu').removeClass("active");
                angular.element('#hotel-menu').removeClass("active");
                angular.element('#holiday-menu').addClass("active");
                angular.element('#uae-menu').removeClass("active");
                angular.element('#insurance-menu').removeClass("active");
            //    $rootScope.tabmenuactive = 3;
                sessionStorage.removeItem('packageDetails');
                sessionStorage.removeItem('packageDetailView');
                sessionStorage.setItem("activeService", $rootScope.tabmenuactive);
            }
        }
    }).state('insurance', {
        url: '/insurance',
        templateUrl: 'views/home/insurance.html',
        controller: 'homeController',
        resolve: {
            init: function(sessionService, $stateParams, $state, $rootScope) {
                sessionService.clearRouteData('insurance');
                // document.body.scrollTop = document.documentElement.scrollTop = 0;
                window.scrollTo(0, 0)
                angular.element('#flight-menu').removeClass("active");
                angular.element('#hotel-menu').removeClass("active");
                angular.element('#holiday-menu').removeClass("active");
                angular.element('#uae-menu').removeClass("active");
                angular.element('#insurance-menu').addClass("active");
                $rootScope.tabmenuactive = 3;
                sessionStorage.setItem("activeService", $rootScope.tabmenuactive);
            }
        }
    }).state('uaeAttraction', {
        url: '/uaeAttraction',
        templateUrl: 'views/uae-attraction/uae-attraction.html',
        controller: 'uaeController',
        resolve: {
            init: function(sessionService, $stateParams, $state, $rootScope) {
                sessionService.clearRouteData('uae');
                document.body.scrollTop = document.documentElement.scrollTop = 0;
                angular.element('#flight-menu').removeClass("active");
                angular.element('#hotel-menu').removeClass("active");
                angular.element('#holiday-menu').removeClass("active");
                angular.element('#uae-menu').addClass("active");
                angular.element('#insurance-menu').removeClass("active");
                $rootScope.tabmenuactive = 5;
                sessionStorage.removeItem('packageDetails');
                sessionStorage.removeItem('packageDetailView');
                sessionStorage.setItem("activeService", $rootScope.tabmenuactive);
            }
        }
    }).state('flightResult', {
        url: '/flightResult',
        templateUrl: 'views/flight/flightSearchResult.html',
        controller: 'flightResultController'
    }).state('quote-list', {
        url: '/quote-list',
        templateUrl: 'views/flight/flightQuotation.html',
        controller: 'flightQuotationController'
    }).state('view-quote', {
        url: '/view-quote',
        templateUrl: 'views/account/quotation/quotationDetails.html',
        controller: 'quotationDetailController'
    }).state('multiflightResult', {
        url: '/multiflightResult',
        templateUrl: 'views/flight/flighMulticitySearchResult.html',
        controller: 'flightMultiCityResultController'
    }).state('flightDetails', {
        url: '/flightDetails',
        templateUrl: 'views/flight/flightDetails.html',
        controller: 'flightDetailsController'
    }).state('flightReview', {
        url: '/flightReview',
        templateUrl: 'views/flight/flightReview.html',
        controller: 'flightReviewController'
    }).state('hotelResult', {
        url: '/hotelResult',
        templateUrl: 'views/hotel/hotelResult.html',
        controller: 'hotelResultController'
    }).state('hotelDetails', {
        url: '/hotelDetails',
        templateUrl: 'views/hotel/hotelDetails.html',
        controller: 'hotelDetailsController'
    }).state('holidayFlightDetails', {
        url: '/holidayFlightDetails',
        templateUrl: 'views/flight/flightDetails.html',
        controller: 'flightDetailsController'
    }).state('hotelReview', {
        url: '/hotelReview',
        templateUrl: 'views/hotel/hotelReview.html',
        controller: 'hotelReviewController'
    }).state('tourDetails', {
        url: '/tourDetails/:id/:packageName',
        templateUrl: 'views/holiday/tourDetails.html',
        controller: 'tourDetailsController'
    }).state('uaeDetails', {
        url: '/uaeDetails/:id/:packageName',
        templateUrl: 'views/uae-attraction/uae-tour.html',
        controller: 'uaetourDetailsController'
    })

    .state('holidaySearchResult', {
            url: '/holidaySearchResult',
            templateUrl: 'views/holiday/holidaySearchResult.html',
            controller: 'holidaySearchResultController'
        }).state('holidayReview', {
            url: '/holidayReview',
            templateUrl: 'views/holiday/holidayReview.html',
            controller: 'holidayReviewController'
        }).state('holidayBook', {
            url: '/holidayBook',
            templateUrl: 'views/holiday/holidayBook.html',
            controller: 'holidayBookController'
        }).state('insuranceResult', {
            url: '/insuranceResult',
            templateUrl: 'views/insurance/insuranceResult.html',
            controller: 'insuranceResultController'
        }).state('insuranceReview', {
            url: '/insuranceReview',
            templateUrl: 'views/insurance/insuranceReview.html',
            controller: 'insuranceReviewController'
        })
        .state('searchingFlight', {
            url: '/searchingFlight',
            templateUrl: 'views/loadingPage.html',
            controller: 'flightLoading'
        })
        .state('pamentredirect', {
            url: '/pamentredirect',
            templateUrl: 'paymentRedirector.html',
            controller: 'finalJsonCtrl'
        }).state('searchingHotel', {
            url: '/searchingHotel',
            templateUrl: 'views/hotelLoadingPage.html',
            controller: 'hotelLoadingCtrl'
        }).state('searchingHoliday', {
            url: '/searchingHoliday',
            templateUrl: 'views/loadingPage.html',
            controller: 'flightHotelController'
        }).state('searchingInsurance', {
            url: '/searchingInsurance',
            templateUrl: 'views/insurenceLoadingpage.html',
            controller: 'insuranceLoadingController'
        }).state('flightItenary', {
            url: '/flightItenary',
            // templateUrl: 'views/flight/flightItenary.html',
            templateUrl: 'views/flight/flightItenenary.html',
            controller: 'flightItenaryController'
        }).state('hotelItenary', {
            url: '/hotelItenary',
            templateUrl: 'views/hotel/hotelItenenary.html',
            controller: 'hotelItenenaryontroller'
        }).state('holidayItenary', {
            url: '/holidayItenary',
            templateUrl: 'views/holiday/holidayItenenary.html',
            controller: 'holidayItenenaryController'
        }).state('insuranceItenary', {
            url: '/insuranceItenary',
            templateUrl: 'views/insurance/insuranceItenenary.html',
            controller: 'insuranceItenenaryController'
        // }).state('login', {
        //     url: '/login',
        //     templateUrl: '/login.html',
        //     controller: 'adminloginCtrl'
        }).state('flightBookings', {
            url: '/flightBookings',
            templateUrl: 'views/flight/flightBookings.html',
            controller: 'retriveController'
        }).state('hotelBookings', {
            url: '/hotelBookings',
            templateUrl: 'views/hotel/hotelBookings.html',
            controller: 'hotelRetriveController'
        }).state('insuranceBookings', {
            url: '/insuranceBookings',
            templateUrl: 'views/insurance/insuranceBookings.html',
            controller: 'insuranceRetriveController'
        }).state('flightMyBookings', {
            url: '/flightMyBookings',
            templateUrl: 'views/flight/flightMyBookings.html',
            controller: 'flightMyBookingController'
        }).state('manageBookings', {
            url: '/manageBookings',
            templateUrl: 'views/flight/manageBookings.html',
            controller: 'manageBookingController'
        }).state('hotelMyBookings', {
            url: '/hotelMyBookings',
            templateUrl: 'views/hotel/hotelMyBookings.html',
            controller: 'hotelMyBookingController'
        }).state('insuranceMyBookings', {
            url: '/insuranceMyBookings',
            templateUrl: 'views/insurance/insuranceMyBookings.html',
            controller: 'insuranceMyBookingController'
        }).state('confailed', {
            url: '/confailed',
            templateUrl: 'confailed.html',
            controller: 'confailedController'
        }).state('searchTicket', {
            url: '/qpanel/searchTicket',
            templateUrl: 'views/pages/qpanel.html',
            resolve: {
                init: function($rootScope) {
                    $rootScope.tabmenuactive = 7;
                    sessionStorage.setItem("activeService", $rootScope.tabmenuactive);
                }
            }
        }).state('dailySalesReport', {
            url: '/reports/dailySales',
            templateUrl: 'views/pages/reports/dailySalesReport.html',
            resolve: {
                init: function($rootScope) {
                    $rootScope.tabmenuactive = 6;
                    sessionStorage.setItem("activeService", $rootScope.tabmenuactive);
                }
            }
        }).state('airlineReport', {
            url: '/reports/airline',
            templateUrl: 'views/pages/reports/airlineReport.html',
        }).state('productivityReportFlight', {
            url: '/productivityReport/flight',
            templateUrl: 'views/pages/reports/productivity_report_flight.html',
        }).state('productivityReportHotel', {
            url: '/productivityReport/hotel',
            templateUrl: 'views/pages/reports/productivity_report_hotel.html',
        }).state('productivityReportInsurance', {
            url: '/productivityReport/insurance',
            templateUrl: 'views/pages/reports/productivity_report_insurance.html',
        }).state('productivityReportRail', {
            url: '/productivityReport/rail',
            templateUrl: 'views/pages/reports/productivity_report_rail.html',
        }).state('productivityReportRailPass', {
            url: '/productivityReport/rail-pass',
            templateUrl: 'views/pages/reports/productivity_report_rail_pass.html',
        }).state('productivityReportTransfer', {
            url: '/productivityReport/transfer',
            templateUrl: 'views/pages/reports/productivity_report_transfer.html',
        }).state('railEurope', {
            url: '/railEurope',
            templateUrl: 'views/pages/rail-europe/railEurope.html',
        }).state('transfers', {
            url: '/transfers',
            templateUrl: 'views/pages/transfers/transfers.html',
        }).state('railMybooking', {
            url: '/railmybooking',
            templateUrl: 'views/pages/rail-europe/mybooking.html',
        }).state('railAllbooking', {
            url: '/railallbooking',
            templateUrl: 'views/pages/rail-europe/allbooking.html',
        }).state('transferMybooking', {
            url: '/transfermybooking',
            templateUrl: 'views/pages/transfers/mybooking.html',
        }).state('transferAllbooking', {
            url: '/transferallbooking',
            templateUrl: 'views/pages/transfers/allbooking.html',
        }).state('user', {
            url: '/user',
            templateUrl: 'views/account/user_home_view.html',
            controller: 'userTripsCtrl',
            resolve: {
                init: function(sessionService, $stateParams, $state) {
                    document.body.scrollTop = document.documentElement.scrollTop = 0;
                }
            }
        }).state('transaction', {
            url: '/transaction',
            templateUrl: 'views/account/my_transaction.html',
            controller: 'MyTripsCtrl',
            resolve: {
                init: function(sessionService, $stateParams, $state) {
                    document.body.scrollTop = document.documentElement.scrollTop = 0;
                }
            }
        }).state('traveller', {
            url: '/traveller',
            templateUrl: 'views/account/traveller/traveller-view.html',
            controller: 'userTravellerCtrl',
            resolve: {
                init: function(sessionService, $stateParams, $state) {
                    document.body.scrollTop = document.documentElement.scrollTop = 0;
                }
            }
        }).state('profile', {
            url: '/profile',
            templateUrl: 'views/account/profile/profile-view.html',
            controller: 'userProfileCtrl',
            resolve: {
                init: function(sessionService, $stateParams, $state) {
                    document.body.scrollTop = document.documentElement.scrollTop = 0;
                }
            }
        }).state('report', {
            url: '/report',
            templateUrl: 'views/account/report/report-view.html',
            controller: 'userReportCtrl',
            resolve: {
                init: function(sessionService, $stateParams, $state) {
                    document.body.scrollTop = document.documentElement.scrollTop = 0;
                }
            }
        }).state('reportAll', {
            url: '/reportAll',
            templateUrl: 'views/account/report/reportall-view.html',
            controller: 'userReportCtrls',
            resolve: {
                init: function(sessionService, $stateParams, $state) {
                    document.body.scrollTop = document.documentElement.scrollTop = 0;
                }
            }
        }).state('privacy-policy', {
            url: '/privacy-policy',
            templateUrl: 'views/pages/privacy_policy.html',
            resolve: {
                init: function(sessionService, $stateParams, $state) {
                    document.body.scrollTop = document.documentElement.scrollTop = 0;
                }
            }
        }).state('holiday-details', {
            url: '/holiday-details',
            templateUrl: 'views/pages/holiday_details.html',
            resolve: {
                init: function(sessionService, $stateParams, $state) {
                    document.body.scrollTop = document.documentElement.scrollTop = 0;
                }
            }
        }).state('city_breaks', {
            url: '/city_breaks/:ids',
            templateUrl: 'views/pages/city_breaks.html',
            resolve: {
                init: function(sessionService, $stateParams, $state) {
                    document.body.scrollTop = document.documentElement.scrollTop = 0;
                }
            }
        }).state('family_gateways', {
            url: '/family-gateways',
            templateUrl: 'views/pages/family_gateway.html',
            resolve: {
                init: function(sessionService, $stateParams, $state) {
                    document.body.scrollTop = document.documentElement.scrollTop = 0;
                }
            }
        }).state('holiday_packages', {
            url: '/holiday_packages',
            templateUrl: 'views/pages/holiday_packages.html',
            resolve: {
                init: function(sessionService, $stateParams, $state) {
                    document.body.scrollTop = document.documentElement.scrollTop = 0;
                }
            }
        }).state('detailspage', {
            url: '/package/:ids',
            templateUrl: 'views/pages/city_breaks.html',
            controller: 'enquiryController',
            resolve: {
                init: function(sessionService, $stateParams, $state) {
                    document.body.scrollTop = document.documentElement.scrollTop = 0;

                }
            }
        }).state('deals_and_discount', {
            url: '/deals_and_discount',
            templateUrl: 'views/pages/deals&discount.html',
            resolve: {
                init: function(sessionService, $stateParams, $state) {
                    document.body.scrollTop = document.documentElement.scrollTop = 0;
                }
            }
        }).state('clubMed', {
            url: '/clubMed',
            templateUrl: 'views/pages/clubMed.html',
            resolve: {
                init: function(sessionService, $stateParams, $state) {
                    document.body.scrollTop = document.documentElement.scrollTop = 0;
                }
            }
        }).state('Hertz', {
            url: '/hertz',
            templateUrl: 'views/pages/Hertz.html',
            resolve: {
                init: function(sessionService, $stateParams, $state) {
                    document.body.scrollTop = document.documentElement.scrollTop = 0;
                }
            }
        }).state('Trafalgar', {
            url: '/trafalgar',
            templateUrl: 'views/pages/Trafalgar.html',
            resolve: {
                init: function(sessionService, $stateParams, $state) {
                    document.body.scrollTop = document.documentElement.scrollTop = 0;
                }
            }
        }).state('register', {
            url: '/register?activationKey',
            templateUrl: 'register.html',
            
        }).state('resetpassword', {
            url: '/resetpassword?activationKey',
            templateUrl: 'resetPassword.html',
        }).state('enquiry-holiday', {
            url: '/enquiry-holiday',
            templateUrl: 'views/pages/enquiry_holiday_location.html',
            controller: 'enquiryController',
            resolve: {
                init: function(sessionService, $stateParams, $state) {
                    document.body.scrollTop = document.documentElement.scrollTop = 0;
                }
            }
        }).state('contact-us', {
            url: '/contact-us',
            templateUrl: 'views/pages/contact_us.html',
            controller: 'contactEnquiryController',
            resolve: {
                init: function(sessionService, $stateParams, $state) {
                    document.body.scrollTop = document.documentElement.scrollTop = 0;
                }
            }
        }).state('terms_conditions', {
            url: '/terms_conditions',
            templateUrl: 'views/pages/terms_conditions.html',
            resolve: {
                init: function(sessionService, $stateParams, $state) {
                    document.body.scrollTop = document.documentElement.scrollTop = 0;
                }
            }
        });


    //	$httpProvider.interceptors.push('APIInterceptor');
    $httpProvider.interceptors.push('serviceInterceptor');
    $httpProvider.interceptors.push('authService');
    $httpProvider.interceptors.push('AuthInterceptor');

    $qProvider.errorOnUnhandledRejections(false);

});
        // angular.module('app.lib.auth', []);
        // angular.module('app.lib.auth')
        //     .factory('authService', authService);
        // angular.module('app.lib.auth')
        //     .factory('AuthInterceptor', AuthInterceptor);
    
            ouaApp.service('authService', function($window,baseUrlService) {
            return {
                getToken: function() {
                    return JSON.parse($window.sessionStorage.getItem('authToken'));
                },
                getRefreshToken: function() {
                    return JSON.parse($window.sessionStorage.getItem('refreshToken'));
                },
                setRefreshToken: function(token) {
                    $window.sessionStorage.setItem('refreshToken', token);
                },
                setToken: function(token) {
                    $window.sessionStorage.setItem('authToken', token);
                },
                clearAllToken: function(){
                    $window.sessionStorage.removeItem('authToken');
                    $window.sessionStorage.removeItem('refreshToken');
                },
                clearToken: function(){
                    $window.sessionStorage.removeItem('authToken');
                },
                isLoggedIn: function() {
                    if ($window.sessionStorage.getItem('authToken') === null) {
                        return false;
                    }
                    else {
                        return true;
                    }
                },
                toLogin: function(){
                    Lobibox.alert('error', {
                        msg: 'Your session is expired. Please Login Again.',
                        callback: function (lobibox, type) {
                            if (type === 'ok') 
                            {
                                console.log(baseUrlService.UIRidrtUrl);
                                $window.location.href = baseUrlService.UIRidrtUrl;
                            }
                          }
                    });
                    // $location.path(baseUrlService.UIRidrtUrl);
                }
            }
        });
    
        ouaApp.service('AuthInterceptor', function($q, $injector, authService,ServerService,$cookies) {
            var inFlightAuthRequest = null;
            var URLS = {};
            URLS.api_refresh_token = ServerService.authPath + 'refresh-token';

            

            return {
                request : function(config) {
                    config.headers = config.headers || {};
                    // $cookiesSessionId = Math.random().toString(36).substr(2);
                    // $cookies.put('CSRF-TOKEN', $cookiesSessionId);
                    config.headers['CSRFToken']= $cookiesSessionId;
                    // config.headers['Set-Cookie'] = "CSRF-TOKEN="+$cookiesSessionId;
        
                    if(authService.getToken()){
                        config.headers['Authorization'] = 'Bearer ' +authService.getToken();
                        // config.headers['Content-Type']= 'application/json';

                    }
                    return config;
                },
                responseError : function(response) {
                    if(response.config.url == URLS.api_refresh_token){
                        console.log(JSON.stringify(response));
                        authService.clearAllToken();
                        authService.toLogin();
                    }else{
    
                        switch (response.status) {
                        case 401:
                            authService.clearToken();
                            var deferred = $q.defer();
                            if (!inFlightAuthRequest) {
                                inFlightAuthRequest = $injector.get("$http").post(
                                        URLS.api_refresh_token, { 
                                            'Content-Type': 'application/json',
                                            'Authorization':  authService.getRefreshToken()
                                        });
                            }
                            inFlightAuthRequest.then(function(r) {
                                inFlightAuthRequest = null;
                                console.log(JSON.stringify(r));
                                authService.setToken(r.data.accesstoken);
                                $injector.get("$http")(response.config).then(function(resp) {
                                    deferred.resolve(resp);
                                }, function(resp) {
                                    deferred.reject(resp);
                                });
                            }, function(error) {
                                inFlightAuthRequest = null;
                                deferred.reject();
                                authService.clearAllToken();
                                authService.toLogin();
                                return;
                            });
                            return deferred.promise;
                            break;
                        default:
                            return $q.reject(response);
                        break;
                        }
                        return response || $q.when(response);
                    }
                }
            }
        });
    

ouaApp.service('serviceInterceptor', function($q, $injector,$location,$window) {
    var service = this;
    // service.request = function(config) {
    //     return config;
    // };
    service.responseError = function(res) {
        console.log(res)
        if(res.status == -1){
            $(".artloader").attr("style","display:none !important");
            // $location.path('/confailed');
            // $window.location.href = '/confailed.html';

        }
        if(res.status == 401)
        {

        }
    };
}).directive('wrapOwlcarousel', function() {
    return {
        restrict: 'E',
        link: function($scope, element, attrs) {
            $scope.owlOptionsTestimonials = {
                autoPlay: 4000,
                stopOnHover: true,
                slideSpeed: 300,
                paginationSpeed: 600,
                items: 2
            }
          //  var options = scope.$eval($(element).attr('data-options'));
          //  $(element).owlCarousel(options);
         
        }
    };
});

ouaApp.controller('mainController', function($scope, $window,refreshService,expiryService, urlService, baseUrlService, $location, $rootScope, $anchorScroll, $location, $uibModal, $http, ServerService, $state) {

    $scope.redirectedURL = window.location.href

    $scope.isheaderdisabled =  $location.path() != '/view-quote' ? true : false;

    $("#load1").show();
     $scope.admin =sessionStorage.getItem('b2bLoginResponse');
     $scope.requestFrameObj={
        "quotation": {
          "flight": {
            "quote-details": {
              
            },
            "flight-items": [
             
            ],
            "hotel": [
              
            ],
            "ins": [
              
            ],
            "transfer": [
              
            ],
            "rail": [
              
            ]
          }
        }
    }
    sessionStorage.setItem('requestFrame', JSON.stringify( $scope.requestFrameObj));
    
    
    if(sessionStorage.getItem('companyDetails') == undefined){
         $window.location.href = baseUrlService.UIRidrtUrl;
        // $location.path(baseUrlService.UIRidrtUrl);

		// var location=$window.location.href;
			//  var websiteLoginLocation = location.concat('',"login.html"); 
			      /* $location.path('/home'); */
        //  $window.location.href=websiteLoginLocation;
    }else{
        $scope.tokenresult = JSON.parse(sessionStorage.getItem('companyDetails'));
        $scope.logindata=$scope.tokenresult;
        $rootScope.companyName=$scope.logindata.companyName;
        $rootScope.companyPhoneNo=$scope.logindata.companyPhoneNo;
        $scope.userEmail =sessionStorage.getItem('loginUserName');
        $scope.loggedValue=true;
        $rootScope.bookingHide = true;
        $rootScope.guestLogin = false;
        $rootScope.logoId = $scope.logindata.companyLogoId;
        $scope.logoId = $scope.logindata.companyLogoId;

        $rootScope.bookingHide1 = true; 
        $rootScope.dispName = $scope.logindata.userFirstName;
        $rootScope.userID = $scope.logindata.userId;
        $rootScope.ServerService = ServerService;
    }

    $scope.creditLimits=false;
    $scope.getCreditLimit = function()
    {
        // $("#dropmenuli").attr("style","display: block !important");
        // $("#credlmtdropdwn").addClass("open");

        // var token = JSON.parse(sessionStorage.authToken);
        // var headersOptions = {
        //     'Content-Type': 'application/json',
        //     'Authorization': 'Bearer ' + token
        // }
        // $scope.artloading = true;

        $http.get(ServerService.serverPath + 'creditLimit/current/limit', 
        {
            // headers: headersOptions
        }).then(function successCallback(response) 
        {
            $scope.artloading = false;
            if(response.data.success)
            {
                $scope.creditLimits=true;
                $scope.creditLimit = response.data.data;
                $("#dropmenuli").attr("style","display: block !important");
                return false;
            }
        })
    }

   
    window.onclick = function(event) {
      if($scope.creditLimits==false){
        $("#dropmenuli").attr("style","display: none !important");
      }
}
    // }
    // $scope.getCreditLimit();

    $scope.selectingTrvlr=function(indexvalue,travelType)
    {
      $scope.initSrchObj();
      $scope.modalInstance=$uibModal.open({
          templateUrl: 'myTestModal.tmpl.html',
          backdrop: 'static',
          keyboard: true,
          scope:$scope
      });
      $scope.phNoREQ = false;
      $scope.ccREQ = false;
      $scope.searchTrvlr(indexvalue,travelType);
    }
    function getphncountrycode(){
        $scope.tokenresult = JSON.parse(sessionStorage.getItem('authToken'));
        var headersOptions = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + $scope.tokenresult 
        }
        $scope.searchReq = JSON.parse(sessionStorage.getItem('InsuranceSearchReq'));

        $http.get(ServerService.listPath + 'list/phone-country', {

            headers: headersOptions
        }).then(function successCallback(response) {
            $scope.loading = false;
            $scope.disabled = false;
            if (response.data.success) {
                $scope.countryList = response.data.data;
                sessionStorage.setItem("countryList",JSON.stringify($scope.countryList));
               
            }
        });
    }
    getphncountrycode();
    $scope.menuServices = function(){
        $scope.tokenresult = JSON.parse(sessionStorage.getItem('companyDetails'));
        $scope.logindata=$scope.tokenresult;
        $rootScope.userID = $scope.logindata.userId;
    
        var tokenresult = JSON.parse(sessionStorage.getItem('authToken'));
        var headersOptions = {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + tokenresult
            }
            
            $http.get(ServerService.authPath + 'module/userMenus?userId=' +$rootScope.userID
            , {
                headers: headersOptions
            }).then(function(response) {
                $scope.allMenus = response.data.data;
                $window.sessionStorage.setItem("mainMenu",JSON.stringify($scope.allMenus));
                sessionStorage.setItem("newactiveService", 0);
                $rootScope.newtabmenuactive = 0;
                $location.path('/'+$scope.allMenus[0].path.split('#!/')[1]);
            }, function errorCallback(response) {
                if(response.status == 403){
                    var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
                    status.then(function(greeting) {
                        $scope.redirectPrivacy();
                    });     
                }
            });
    }

    // sessionStorage.mainMenu ? ''  : $scope.menuServices();
    if(sessionStorage.mainMenu == undefined)
    {
        $scope.menuServices();
    }else{
        $rootScope.newtabmenuactive = sessionStorage.getItem("newactiveService")? JSON.parse(sessionStorage.getItem("newactiveService")):0;
        $scope.allMenus = JSON.parse(sessionStorage.getItem("mainMenu"));
    }
    setTimeout(function () {
       $('#load1').hide();
    }, 4000);
    $scope.path = $location.absUrl().split('/')[5];
    $scope.path1 = $location.$$url;
    console.log("mainController");
    $scope.imgClick = function() {
            console.log("click");
            $location.path('/');
        }
        $('html, body').animate({scrollTop:0},500);
    
    //     //Warning alert
    //     $scope.warningMsg = "Need to help with travel plan impacted by COVID-19? Your online itineraryis the best place to cancel your trip. <br> <a href='#' style='text-decoration: underline !important;'>Go to your itinerary</a> ";
    //    $scope.lobi =  Lobibox.notify('warning', {
    //         msg: $scope.warningMsg,
    //         title: 'Travel Update',
    //         position: "top left",
    //         width:1340,
    //         delay: false
    //     });
    
     //ourservice
$scope.redirectHome = function(){
    $location.path('/home');
    $window.scroll(0,1100);
    window.scroll(0,1100);
    document.body.scrollTop = document.documentElement.scrollTop = 1100;
  
    // window.scrollTo({
    //     top: 1100,
    //     behavior: 'smooth',
    //   });

}

        // $scope.getId = $routeParams['ids'];


    // $scope.allbannerData=[];
    // $scope.banners=[];
    // 				$scope.allSpecialOfferData=[];
    // 				$scope.allMoreChoiceData=[];
    // $(document).ready(function () {  
    // 	$.ajax({  
    // 		type: "GET",  
    // 		url:$scope.baseurl+'/section-master/page/1',
    // 		success: function (data,status) {  
    // 			if(status ='success') {
    // 				$scope.bannerdata=[];
    // 				$scope.allbannerData=[];
    // 				$scope.banners=[];
    // 				$scope.allSpecialOfferData=[];
    // 				$scope.allMoreChoiceData=[];
    // 				$scope.cmsmaster = data.data;
    // 				angular.forEach($scope.cmsmaster , function (value, key) { 
    // 					$scope.bannerdata.push(value.id); 
    // 					var cmsName=value.sectionName;
    // 					var cmsid=value.id;
    // 					if(cmsName.toLowerCase()=='banner'){

    // 						$.get($scope.baseurl+'/section-details/published/section-master/'+cmsid).then(function (response) {

    // 						$scope.cmsbammer=response.data;
    // 						$scope.allbannerData=	$scope.cmsbammer; 
    // 						for(var i=0;i<$scope.allbannerData.length;i++){
    // 							$scope.banners.push($scope.allbannerData[i].content.banner);

    // 								$('.dynamic_carouseldes').owlCarousel().trigger('add.owl.carousel', 
    // 								[jQuery('<div class="owl-item"><img src="'+$scope.baseurl+'/image/'+$scope.allbannerData[i].content.banner.imageDeskId +'" style="width: 1151px;"></img></div>')]).trigger('refresh.owl.carousel');
    // 								$('.dynamic_carouselmob').owlCarousel().trigger('add.owl.carousel', 
    // 								[jQuery('<div class="owl-item"><img src="'+$scope.baseurl+'/image/'+$scope.allbannerData[i].content.banner.imageResId +'" style="width: 1151px;"></img></div>')]).trigger('refresh.owl.carousel');


    // 						}

    // 						console.log($scope.allbannerData,$scope.banners);
    // 					});
    // 				}else if(cmsName.toLowerCase()=='offers'){
    // 					$.get($scope.baseurl+'/section-details/published/section-master/'+cmsid).then(function (response) {
    // 						$scope.allSpecialOfferData=[];
    // 						$scope.cmsspecialdeal=response.data;
    // 						for(var i=0;i<$scope.cmsspecialdeal.length;i++){
    // 							$scope.allSpecialOfferData.push($scope.cmsspecialdeal[i].content.offers); 
    // 						}


    // 					});
    // 				}else if(cmsName.toLowerCase()=='more choices'){
    // 					$.get($scope.baseurl+'/section-details/published/section-master/'+cmsid).then(function (response) {
    // 						$scope.cmsspecialoffer=response.data;

    // 						$scope.allMoreChoiceData=$scope.cmsspecialoffer; 
    // 						for(var i=0;i<$scope.allMoreChoiceData.length;i++){
    // 							var showOFF,showOffTXT;
    // 						// angular.forEach($scope.allMoreChoiceData[i].content.choices,function(val,key){
    // 							if($scope.allMoreChoiceData[i].content.choices.offerPercent==undefined){
    // 								showOFF='trip-offer hideClass'
    // 								showOffTXT='hideClass'

    // 							}else{
    // 								showOFF='trip-offer showClass'
    // 								showOffTXT='showClass'
    // 							}
    // 							$('.owlcarouseltripslider').owlCarousel().trigger('add.owl.carousel', 
    // 							[jQuery('<div class="single-slider" ><div class="trip-head img-box"><div class="'+showOFF+'">'+$scope.allMoreChoiceData[i].content.choices.offerPercent+' OFF</div><img src="'+$scope.baseurl+'/image/'+$scope.allMoreChoiceData[i].content.choices.imageId +'"></img></div><div class="trip-details"><div class="left"><h4>'+$scope.allMoreChoiceData[i].content.choices.title+'</h4><p><i class="fa fa-clock-o"></i><span class="'+showOffTXT+'">'+$scope.allMoreChoiceData[i].content.choices.offerPercent+'</span>'+$scope.allMoreChoiceData[i].content.choices.subTitle+'</p></div><a ui-sref="clubMed" class="btn">View More </a></div></div>')]).trigger('refresh.owl.carousel');
    // 							// $('.dynamic_carouselmob').owlCarousel().trigger('add.owl.carousel', 
    // 							// [jQuery('<div class="owl-item"><img src="'+val.mobimg +'"></img></div>')]).trigger('refresh.owl.carousel');

    // 						//  });

    // 						}
    // 					});
    // 				}else{
    // 					console.log("data is not available");
    // 				}
    // 				});
    // 				} else {
    // 					console.log("data is not available");
    // 			}	
    // 		}  
    // 	});  
    // }); 
    /* reuseService.getrequiredData */
    /* $scope.$watch(function(){
    	return reuseService.getrequiredData;
    }, function(newValue, oldValue){
    	console.log("Test Value"+ newValue)
    	}) */
    var booking = JSON.parse(localStorage.getItem('userBookingdetails'))
    var display = JSON.parse(localStorage.getItem('loginName'));
    localStorage.getItem('authentication');
    // if (booking == '' || booking == null) {
    //     $scope.bookingHide1 = false;
    // } else {
    //     $scope.bookingHide1 = booking.bookingHide1;
    //     $scope.bookingHide = booking.bookingHide;
    //     if (display != null) {
    //         $scope.dispName = display.fname;
    //     }
    // }

    // $(document).ready(function () {  
    //     $scope.authenticationreq = {
    //         "username": DEFAULT_USER
    //     }

    //     $http.post(ServerService.authPath + 'guest-login', JSON.stringify($scope.authenticationreq), {
    //     headers: { 'Content-Type': 'application/json' }
    // }).then(function(response) {
    //     if (response.status == 200) {
    //         console.log('GUEST-1');
            
    //     }

   
    //     });
    // });


    $scope.openLogin = function(size, parentSelector) {
        var parentElem = parentSelector ?
            angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;

        $scope.animationEnabled = true;
        var modalInstance = $uibModal.open({
            animation: $scope.animationEnabled,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'login.html',
            controller: 'loginController',
            backdrop: 'static',
			keyboard: true,
            size: size,
            appendTo: parentElem,
            resolve: {
                items: function() {
                    return $scope.items
                }
            }
        })
    }

    $scope.changePwd = function(size, parentElem) {
        var modalInstance = $uibModal.open({
            animation: $scope.animationEnabled,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'changePwd.html',
            controller: 'passwordChangeController',
            backdrop: true,
            size: size,
            appendTo: parentElem,
            resolve: {
                items: function() {
                    return $scope.items
                }
            }
        });
        $('.dropdown-content').css({ 'display': 'none' });
    }

    $scope.myAccount = function() {
        $('.dropdown-content').css({ 'display': 'none' });
    }

    $scope.userlogout = function() {
        console.log('logout works');
        username = '';
        localStorage.islogged = false;
        $scope.tokenresult = JSON.parse(sessionStorage.getItem('authToken'));
        var headersOptions = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' +  $scope.tokenresult,
            "CSRFToken":$cookiesSessionId
        }
    
        
        if ($location.absUrl().split('username=')[1]) {
            username = $location.absUrl().split('username=')[1];
        } else {
            username = DEFAULT_USER
        }
        $scope.authenticationreq = {
                "username": username
            }

            $.ajax({
                type: 'DELETE',
                url:ServerService.authPathlatest + 'sign-out ',
               
                headers: headersOptions,
                success: function (response, status, jqXHR) {
                    if (response.success==true) {

                        localStorage.clear();
                        sessionStorage.clear();
                               
                                /* 	$location.path('/home');
                                    window.location.reload(); */
                                $scope.bookingHide = false;
                                $scope.bookingHide1 = false;
                
                                // sessionStorage.removeItem('authentication');1
                                localStorage.removeItem('userBookingdetails');
                                localStorage.removeItem('loginName');
                                
                                // $window.location.href="http://localhost/art-b2b/login.html";
                                $window.location.href = baseUrlService.UIRidrtUrl; 
                        // $location.path(baseUrlService.UIRidrtUrl);
                
                
                        $(".dropdown-content").hide();
                    
                        
                    }else{

                    }
                
                },error: function (response, status, err) {
                    if(response.status==401){
                        localStorage.clear();
                        sessionStorage.clear();
                               
                                /* 	$location.path('/home');
                                    window.location.reload(); */
                                $scope.bookingHide = false;
                                $scope.bookingHide1 = false;
                
                                // sessionStorage.removeItem('authentication');1
                                localStorage.removeItem('userBookingdetails');
                                localStorage.removeItem('loginName');
                                
                                //  $window.location.href="http://localhost/art-b2b/login.html";
                                // $window.location.href = baseUrlService.UIRidrtUrl; 
                                var redirectUrl=$location.absUrl().split('!');
                                var a=redirectUrl[0].split('#');
                                // $window.location.href = redirectUrl; 
                                $window.location.href = a[0]; 
                        // $location.path(baseUrlService.UIRidrtUrl);
                
                
                        $(".dropdown-content").hide();
                    
                    
                    } else if (response.status==403){
                        Lobibox.alert('error', {
                        msg:  response.message
                    });
                    }
},

            
            });
        
    }
    $(".userProfileMenu").click(function () {
        $(".dropdown-content").show();
    });
    

    $(document).click(function (e) {

        if (!$(e.target).hasClass("userProfileMenu") 
        && $(e.target).parents(".dropdown-content").length === 0) 
    {
        $(".dropdown-content").hide();
    }

   
    });

    if(sessionStorage.configKeys != undefined){
        let configallkey = JSON.parse(sessionStorage.getItem('configKeys'));
        // document.getElementById('gtaglink').setAttribute("src","https://www.googletagmanager.com/gtag/js?id="+configallkey.google_analytics_tag);

        // document.getElementById('gtagconfig').innerHTML = "window.dataLayer = window.dataLayer || [];function gtag(){dataLayer.push(arguments);}gtag('js', new Date());gtag('config', '"+configallkey.google_analytics_tag+"');";
        
        document.getElementById('recaptchaa').setAttribute("src","https://www.google.com/recaptcha/api.js?render="+configallkey.data.google_recaptcha_public_key);
    }
    $scope.subscribeMail;
    $scope.subscriptionFunc = function() {
        if ($scope.subscribeMail != null && $scope.subscribeMail != "") {
            var tokenresult = JSON.parse(sessionStorage.getItem('authToken'));
            var headersOptions = {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + tokenresult
            }
              $http({
                method: 'GET',
                url: ServerService.profilePath + 'subscriptions/new-letter/' + $scope.subscribeMail,
                headers: headersOptions
            }).then(function(data, status) {
                if (data.data.success) {
                    $scope.subscribeMail='';
                    Lobibox.alert('success', {
                        msg: data.data.message
                    });
                } else {
                    Lobibox.alert('error', {
                        msg: data.data.errorMessage
                    });
                }
            });

        } else {
            Lobibox.alert('error', {
                msg: "Please enter a valid emailId"
            });
        }
    }


    var userBookingdetail = localStorage.getItem('userBookingdetails');
    //console.log("userBookingdetails "+ userBookingdetails);
    var disp = JSON.parse(localStorage.getItem('loginName'));
    
    

     if (userBookingdetail != null) {
        $scope.bookingHide = true;
            $scope.bookingHide1 = true;
        if (disp !== null) {
            $scope.dispName = disp.fname.toLowerCase();
          
         }

     }



  


    $scope.flightCheck = function() {
        var val = "1"
        $scope.activateTabMenu(val);
        $scope.Flight = true;
        $scope.Hotel = false;
        $scope.insCheck = false;
        $scope.fhCheck = false;
        
        $scope.uCheck = false;
        $scope.osCheck = false;

        angular.element('#flight-menu').addClass("active");
        angular.element('#uae-menu').removeClass("active");
        angular.element('#hotel-menu').removeClass("active");
        angular.element('#holiday-menu').removeClass("active");
        angular.element('#insurance-menu').removeClass("active");

        $("html, body").stop().animate({
            scrollTop: $('#about-us').offset().top - 10
        }, '500', 'linear');
    }
    $scope.hotelCheck = function() {
        var val = "2"
        $scope.activateTabMenu(val);

        $scope.Flight = false;
        $scope.Hotel = true;
        $scope.insCheck = false;
        $scope.fhCheck = false;
        
        $scope.uCheck = false;
        $scope.osCheck = false;

        angular.element('#flight-menu').removeClass("active");
        angular.element('#hotel-menu').addClass("active");
        angular.element('#uae-menu').removeClass("active");
        angular.element('#holiday-menu').removeClass("active");
        angular.element('#insurance-menu').removeClass("active");

        $("html, body").stop().animate({
            scrollTop: $('#about-us').offset().top - 10
        }, '500', 'linear');
    }
    $scope.holidayCheck = function() {
        var val = "3"
        $scope.activateTabMenu(val);

        $scope.fCheck = false;
        $scope.hCheck = false;
        $scope.insCheck = false;
        
        $scope.uCheck = false;
        $scope.fhCheck = true;
        $scope.osCheck = false;

        angular.element('#flight-menu').removeClass("active");
        angular.element('#hotel-menu').removeClass("active");
        angular.element('#uae-menu').removeClass("active");
        angular.element('#holiday-menu').addClass("active");
        angular.element('#insurance-menu').removeClass("active");

        $("html, body").stop().animate({
            scrollTop: $('#about-us').offset().top - 10
        }, '500', 'linear');
    }
    $scope.insureCheck = function() {
        var val = "3"
        $scope.activateTabMenu(val);

        $scope.Flight = false;
        $scope.Hotel = false;
        $scope.Insurance = true;
        $scope.fhCheck = false;
        $scope.uCheck = false;
        $scope.osCheck = false;

        angular.element('#flight-menu').removeClass("active");
        angular.element('#hotel-menu').removeClass("active");
        angular.element('#uae-menu').removeClass("active");
        angular.element('#holiday-menu').removeClass("active");
        angular.element('#insurance-menu').addClass("active");

        $("html, body").stop().animate({
            scrollTop: $('#about-us').offset().top - 10
        }, '500', 'linear');

    }

    if (sessionStorage.getItem("activeService")) {
        
        $rootScope.tabmenuactive = sessionStorage.getItem("activeService");
        $rootScope.tabmenuactive = Number($rootScope.tabmenuactive);
        var activeTab = $rootScope.tabmenuactive;
        if (activeTab == 1) {
            $scope.fCheck = true;
            $scope.hCheck = false;
            $scope.fhCheck = false;
            $scope.insCheck = false;
            $scope.osCheck = false;
            $scope.uCheck=false;
        } else if (activeTab == 2) {
            $scope.fCheck = false;
            $scope.hCheck = true;
            $scope.fhCheck = false;
            $scope.insCheck = false;
            $scope.uCheck=false;
            $scope.osCheck = false;

        } else if (activeTab == 3) {
            $scope.fCheck = false;
            $scope.hCheck = false;
            $scope.fhCheck = true;
            $scope.insCheck = false;
            $scope.osCheck = false;
            $scope.uCheck=false;
        } else if (activeTab == 4) {
            $scope.fCheck = false;
            $scope.hCheck = false;
            $scope.fhCheck = false;
            $scope.insCheck = true;
            $scope.osCheck = false;
            $scope.uCheck=false;
        }else if (activeTab == 5) {
            $scope.fCheck = false;
            $scope.hCheck = false;
            $scope.fhCheck = false;
            $scope.insCheck = false;
            $scope.osCheck = false;
            $scope.uCheck=true;
        }else if (activeTab == 6) {
            $scope.fCheck = false;
            $scope.hCheck = false;
            $scope.fhCheck = false;
            $scope.insCheck = false;
            $scope.osCheck = true;
            $scope.uCheck=false;
        }
        // $scope.path = window.location.href;
        // $scope.isPackage = $scope.path.includes('package');
        // if($scope.isPackage === true){
        //     $scope.osCheck = true;
        // }
    } else {
        $rootScope.tabmenuactive = 1;
    }
    
    $scope.newactivateTabMenu = function(indx)
    {
        $rootScope.newtabmenuactive = indx;
        sessionStorage.setItem("newactiveService", Number($rootScope.newtabmenuactive));
        // angular.forEach($scope.allMenus, function(item) {
        //     item.active = $location.path().match(new RegExp(item.url)) ? true : false;
        //   });
    }
    $scope.activateTabMenu = function(value,values) {
        $('html, body').animate({scrollTop:0},500);
        var activebutton=values+1;
            $rootScope.tabmenuactive = Number(activebutton);
         
            sessionStorage.setItem("activeService", $rootScope.tabmenuactive);
         
          
              $scope.guestLoginName= JSON.parse(sessionStorage.getItem("guestLoginDatails"))
 if(sessionStorage.getItem('guestLoginDatails') && sessionStorage.getItem('guestLoginDatails')!="null"){
    
  
    $rootScope.bookingHide = false;
                        $rootScope.guestLogin = false;
                    
                                   $rootScope.bookingHide1 = false; 
    
 	$rootScope.dispName = "";
    
 }
        
    // if(value != '6'){
    //     $('#ourserviceDropdown').attr('style', 'display: none !important;');

    // }else{
    //     $('#ourserviceDropdown').attr('style', 'display: block !important;');
    // }
    // if(value != '7'){
    //     $('#moretrvldropdown').attr('style', 'display: none !important;');
    
    //  }else{
    //     $('#moretrvldropdown').attr('style', 'display: block !important;');
    //  }
            // $("html, body").stop().animate({
            //     // scrollTop: $('#about-us').offset().top - 10
            //     window.scrollTo(0,0),
            // }, '500', 'linear');
            //  forloader...
    //  $("#load").show();
    //  setTimeout(function () {
    //     $('#load').hide();
    //  }, 100000);
        }
        /*
        $rootScope.tabmenuactive = 0;
        $scope.activateTabMenu = function (value) {
        	$rootScope.tabmenuactive = Number(value);
        	$("html, body").stop().animate({
        		scrollTop: $('#about-us').offset().top - 10
        	}, '500', 'linear');
        }


        $rootScope.tabmenuactive = 0;
        $scope.activateInnerTabMenu = function (value) {
        	$rootScope.tabmenuactive = Number(value);

        	$("html, body").stop().animate({
        		scrollTop: $('#about-us').offset().top - 10
        	});
        }

        $rootScope.tabSubmenuactive = 0;
        $scope.flightCheck = function (val) {
        	$rootScope.tabSubmenuactive = Number(val);
        	$("html, body").stop().animate({
        		scrollTop: $('#about-us').offset().top - 10
        	}, '500', 'linear');
        }
        $scope.hotelCheck = function (val) {
        	$rootScope.tabSubmenuactive = Number(val);
        	$("html, body").stop().animate({
        		scrollTop: $('#about-us').offset().top - 10
        	}, '500', 'linear');
        }
        $scope.holidayCheck = function (val) {
        	$rootScope.tabSubmenuactive = Number(val);
        	$("html, body").stop().animate({
        		scrollTop: $('#about-us').offset().top - 10
        	}, '500', 'linear');
        }
        $scope.insureCheck = function (val) {
        	$rootScope.tabSubmenuactive = Number(val);
        	$("html, body").stop().animate({
        		scrollTop: $('#about-us').offset().top - 10
        	}, '500', 'linear');
        }
        */
        /* end */

    /* init function */
    $scope.initialReload = function() {
            $state.reload();
        }
        /* end */



    //terms condition
    $scope.redirectTerms = function() {
        var tokenresult = JSON.parse(sessionStorage.getItem('authToken'));
var headersOptions = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + tokenresult
    }

$http.get(ServerService.profilePath + 'company/retrieve/tandc/2', {
    headers: headersOptions
}).then(function successCallback(response) {
    console.log(response);
    $scope.termsresp = response.data.data;
    $scope.termsLength = response.data.data.length;
   // $window.open(ServerService.listPath + 'file/download/'+$scope.termsresp[$scope.termsLength - 1].fileId);
    $http.get(ServerService.listPath + 'file/download/'+$scope.termsresp[$scope.termsLength - 1].fileId,  {responseType:'arraybuffer'}).then(function successCallback(response) {
        var file = new Blob([response.data], {type: 'application/pdf'});
        var fileURL = URL.createObjectURL(file);
        $window.open(fileURL);
    });
    //$window.location.href = '/terms_conditions';
    //$location.path('/terms_conditions');
}, function errorCallback(response) {
    if(response.status == 403){
        // var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
        // status.then(function(greeting) {
        //     $scope.redirectTerms();
        // });     
    }
});
}


// $(document).ready(function () {  
//     $('#redirectPrivacy').click(function(){
//     $scope.baseurl=ServerService.profilePath;
    
//     $scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));
//     var tokenname = $scope.tokenresult.data;
//     var headersOptions = {
//         'Authorization': 'Bearer ' + tokenname.authToken
//     }

//     $.ajax({
//         url: ServerService.profilePath + 'company/latest/privacy-policy/1',
//         type: 'GET',
//         dataType: 'json',
//         headers: headersOptions,
//         contentType: 'application/json; charset=utf-8',
//         success: function (response) {
//            // $scope.policyresp = response.data.cppJsonData;
//             $('#privacypolicycontent').html(response.data.cppJsonData);
//             $location.path('/privacy-policy');
//         },
//         error: function (error) {
            
//         }
//     });
// });
// });

$scope.activateTabMenu = function(test,index,menuItem) {
    let path = menuItem.path.slice(3);
    if(path == 'railEurope' ||
       path == 'railallbooking' ||
       path == 'railmybooking'){
        reloadCurrentWindow(menuItem,3);
    }
    if(path == 'transfers' ||
       path == 'transferallbooking' ||
       path == 'transfermybooking'){
        reloadCurrentWindow(menuItem,4);
    }
}

function reloadCurrentWindow(menuItem,tabIndex){
    window.location.href = menuItem.path;
    sessionStorage.setItem('activeTabIndex',tabIndex);
    location.reload();
}

activeTabIndex();

function activeTabIndex(){
    if(sessionStorage.getItem('activeTabIndex') !== null){
        let activeIndex = JSON.parse(sessionStorage.getItem('activeTabIndex'));
        $rootScope.tabmenuactive = activeIndex;
    }
}

function parentCall(){
    alert('Its working');
}

//Privacy policy
$scope.redirectPrivacy = function(){
    var tokenresult = JSON.parse(sessionStorage.getItem('authToken'));
var tokenname = tokenresult.data;
var headersOptions = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + tokenresult
    }

    $http.get(ServerService.profilePath + 'company/latest/privacy-policy/2', {
        headers: headersOptions
    }).then(function(response) {
        $scope.policyresp = response.data.data.cppJsonData;
        
        //$window.location.href = '/terms_conditions';
        $location.path('/privacy-policy');
    }, function errorCallback(response) {
        if(response.status == 403){
            var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
            status.then(function(greeting) {
                $scope.redirectPrivacy();
            });     
        }
    });
}

});

ouaApp.service("ajaxService", function($http, $q) {
    var self = this;
    self.data = null;
    self.ajaxRequest = function(met, url, req, res) {
        var deferred = $q.defer();
        if (self.data !== null) {
            deferred.resolve(self.data);
        } else {
            $http({
                method: met,
                url: url,
                data: req,
                responseType: res
            }).then(function(response) {
                //self.data = response.data;
                deferred.resolve(response.data);
            }).catch(function(response) {
                deferred.reject(response);
            });
        }
        return deferred.promise;
    };
});

ouaApp.service('ConstantService', function() {
    // this.currency = 'OMR'
    this.currency = 'AED'
});

/* for nationality */
ouaApp.service('NationService', function($http,refreshService, $rootScope, ServerService, $timeout) {
    var NationService = this;
    NationService.nationGet = function() {
        var nationality = new Object();
        nationality.queryString = "";

        var tokenresult = JSON.parse(sessionStorage.getItem('authToken'));
            var headersOptions = {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + tokenresult
            }

        $http.get(ServerService.listPath + 'list/nationality', {
            headers: headersOptions
        }).then(function(data) {
            if (data.data.success == true) {
                localStorage.setItem('nationality', JSON.stringify(data.data));
                $rootScope.nationalityResult = data.data.data;
                return data.data;
            }

        }, function errorCallback(response) {
            if(response.status == 403){
                var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
                status.then(function(greeting) {
                    NationService.nationGet();
                });                
            }

            });

    };



});
/* end */

/* for country */
ouaApp.service('CountryService', function($http, $rootScope,refreshService, ServerService) {
    var CountryService = this
    CountryService.countryGet = function() {
        var country = new Object();
        country.queryString = "";
        var tokenresult = JSON.parse(sessionStorage.getItem('authToken'));
            var tokenname = tokenresult.data;
            var headersOptions = {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + tokenresult
            }
        $http({
            method: 'GET',
            url: ServerService.listPath + 'list/country',
            // data: country
            // {
            headers: headersOptions
                // }
        }).then(function(data, status) {
            if (status = 'success') {
                localStorage.setItem('country', JSON.stringify(data.data));

                $rootScope.countryResult = data.data;
            } else {
                alert('error');
            }
        }, function errorCallback(response) {
            if(response.status == 403){
                var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
                status.then(function(greeting) {
                    CountryService.countryGet();
                });     
            }
        });
    };
});

//Profile Gender
ouaApp.service('GenderService', function($http, $rootScope, ServerService) {
    this.GetGender = function() {
        var tokenresult = JSON.parse(sessionStorage.getItem('tokenresult'));
        var headersOptions = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + tokenresult
        }

        $http.get(ServerService.listPath + 'common?codeType=Gender', {
            headers: headersOptions
        }).then(function(data) {
            if (data.success == true) {
                $rootScope.Profilegender = data.data;
                console.log(data.data);
            }

        });
    };
});

//Meal Preference
ouaApp.service('mealService', function($http, $rootScope, ServerService) {
    this.Getmeal = function() {
        var tokenresult = JSON.parse(sessionStorage.getItem('authToken'));
        var headersOptions = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + tokenresult
        }

        $http.get(ServerService.listPath + 'common?codeType=MEAL-PREF', {
            headers: headersOptions
        }).then(function(data) {
            if (data.data.success == true) {
                $rootScope.mealPreference = data.data;
                console.log(data.data);
            }

        })
    };
});

//Seat Preference
ouaApp.service('seatPreferenceService', function($http, $rootScope, ServerService) {
    this.Getseat = function() {
        var tokenresult = JSON.parse(sessionStorage.getItem('authToken'));
        var headersOptions = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + tokenresult
        }

        $http.get(ServerService.listPath + 'common?codeType=MEAL-PREF', {
            headers: headersOptions
        }).then(function(data) {
            if (data.success == true) {
                $rootScope.SeatPreference = data.data;
                console.log(data.data);
            }

        })
    };
});

//Relationship
ouaApp.service('relationshipService', function($http, $rootScope, ServerService) {
    this.Getrelationship = function() {
        var tokenresult = JSON.parse(sessionStorage.getItem('authToken'));
        var headersOptions = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + tokenresult
        }

        $http.get(ServerService.listPath + 'common?codeType=RELATIONSHIP', {
            headers: headersOptions
        }).then(function(data) {
            if (data.success == true) {
                $rootScope.SeatPreference = data.data;
                console.log(data.data);
            }

        })
    };
});


ouaApp.factory('renderHTMLFactory', function() {
    var renderObj = {};
    var insTitle = [];
    renderObj.setTitle = function(title) {
        var setTitleVal;
        var setFamilyVal;
        var spltTitle = title.search('Tune Protect');
        if (spltTitle !== -1) {
            insTitle = title.split('-');
            var temp = title.split('Tune Protect');
            var setTitleVal = temp[1];

            var splfamily = setTitleVal.indexOf('(Family)');
            if (splfamily !== -1) {
                var tempfamily = setTitleVal.split('(Family)');
                var setFamilyVal = tempfamily[0].split('-');

                setTitleVal = setFamilyVal[0] + " Family -" + setFamilyVal[1];
                console.log(setTitleVal);
            } else {}
        } else {
            setTitleVal = title
        }
        return setTitleVal;
    }

    renderObj.renderHTML = function(html_code) {
        var decoded = angular.element('<textarea />').html(html_code).text();
        //var titlecontent= insTitle[1];
        var spltTitle = decoded.indexOf('with Tune Protect');
        if (spltTitle !== -1) {

            var titlecontent = decoded.replace(/with Tune Protect/g, "");
        } else {
            var titlecontent = decoded.replace(/Tune Protect/g, "");
        }

        return titlecontent;
    }
    return renderObj;
});
/* end */
ouaApp.service('urlService', function($location) {
    //this.relativePath = $location.absUrl().split('/')[6];
    var currentURL = $location.absUrl();
    this.relativePath = currentURL.substring(currentURL.lastIndexOf("/") + 1, currentURL.length);

});

ouaApp.service('loopService', function($scope) {
    var number = {};
    number.getNumber = function(num) {
        return new Array(num);
    }

});

ouaApp.factory('Month', function() {

    var month = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];

    return month;
});

ouaApp.factory('Year', function() {

    var year = ['17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31', '32', '33', '34', '35', '36', '37'];

    return year;
});
ouaApp.service('ServerService', function($location, baseUrlService) {
    this.serverPath = baseUrlService.baseUrl + "ibp/api/v1/";
    this.dropDownPath = baseUrlService.baseUrl + "corp/api/v1/";
    this.dropDownPath1= baseUrlService.redirectUrl + "corp/api/v1/";
    this.listPath = baseUrlService.baseUrl + "rest/api/v1/";
    this.packagePath = baseUrlService.baseUrl + "tours/api/v1/";
    this.profilePath = baseUrlService.baseUrl + "profile/api/v1/";
    this.authPath = baseUrlService.baseUrl + "auth/api/v1/";
    this.cmsPath = baseUrlService.baseUrl + "cms/api/v1/";
    this.authPathlatest = baseUrlService.baseUrl + "auth/api/v2/";
});
ouaApp.service('expiryService', function(refreshService) {
this.sessionExpire = function() {
   
   var targetTime=JSON.parse(localStorage.getItem('timer'));
    
        setInterval(function() { 
            targetTime=targetTime-1;
        if(targetTime==1){
            var tokenresult = JSON.parse(sessionStorage.getItem('authToken'));
            var refreshToken = JSON.parse(sessionStorage.getItem('refreshToken'));

            targetTime=30;
            refreshService.refreshToken(refreshToken,tokenresult);
            targetTime=JSON.parse(localStorage.getItem('timer'));
        }else{
            // targetTime=targetTime-1;
            if(targetTime>0){
                localStorage.setItem('timer',targetTime);
            }
       
        }
        },60*1000);
        
    
    
}
});



ouaApp.service('sessionService', function($http, $location, ServerService,expiryService) {
    $('html, body').animate({scrollTop:0},500);

    username = '';
    this.clearRouteData = function(value) {

        var itemList = value.length;
        // value == 'home' 
		/* value == 'flight'  */
        if (value == 'flight' || value == 'sign-out' || value == 'hotel' || value == 'holiday' || value == 'insurance' || value == 'user') {
           sessionStorage.removeItem("selectTraveldata");
           	var metas = document.getElementsByTagName('meta');
	var pageTitle = document.getElementsByTagName('title'); 
   pageTitle[0].innerHTML='ART'
	
 var loginDetails = null;
            var bookingDetail = null;
            var sinInDetails = null;
            var insurenceSearchRQ = null;
            var flightSearchRQ = null;
            var configKeys=null;
            var hotelSearchdataobj = null;
            var loginTravellerData = null;
            var refreshTkn = null;
            var companyDetails = null;
            var newactiveService  = null;
            var htlSelectTraveller =  null;
            // var guestLogin = null
            if (localStorage.getItem("loginName")) {
                loginDetails = localStorage.getItem("loginName");
            }
            if (localStorage.getItem("userBookingdetails")) {
                bookingDetail = localStorage.getItem("userBookingdetails");
            }
        //    if(sessionStorage.getItem('guestLoginDatails')){
        //     guestLogin=sessionStorage.getItem('guestLoginDatails');
        //    }
            if (sessionStorage.getItem("authToken")) {
                sinInDetails = sessionStorage.getItem("authToken");
            }
            if (sessionStorage.getItem("refreshToken")) {
                refreshTkn = sessionStorage.getItem("refreshToken");
            }
            if (sessionStorage.getItem("companyDetails")) {
                companyDetails = sessionStorage.getItem("companyDetails");
            }
            if (sessionStorage.getItem("insurenceSearchRQ")) {
                insurenceSearchRQ = sessionStorage.getItem("insurenceSearchRQ");
            }
            if (sessionStorage.getItem("flightSearchRQ")) {
                flightSearchRQ = sessionStorage.getItem("flightSearchRQ");
            }
            if (localStorage.getItem("configKeys")) {
                configKeys = localStorage.getItem("configKeys");
            }
            if (sessionStorage.getItem("hotelSearchdataobj")) {
                hotelSearchdataobj = sessionStorage.getItem("hotelSearchdataobj");
            }

            if (sessionStorage.getItem("mainMenu")) {
                mainMenu = sessionStorage.getItem("mainMenu");
            }

            if (sessionStorage.getItem("newactiveService")) {
                newactiveService = sessionStorage.getItem("newactiveService");
            }            
            
            if (sessionStorage.getItem("htlSelectTraveller")) {
                htlSelectTraveller = sessionStorage.getItem("htlSelectTraveller");
            }
             sessionStorage.clear();
            //  localStorage.clear();
            localStorage.removeItem("loginToken");

            if (flightSearchRQ) {
                sessionStorage.setItem("flightSearchRQ", flightSearchRQ);

            }
            if (configKeys) {
                localStorage.setItem("configKeys", configKeys);

            }
            if (insurenceSearchRQ) {
                sessionStorage.setItem("insurenceSearchRQ", insurenceSearchRQ);

            }
            if (hotelSearchdataobj) {
                sessionStorage.setItem("hotelSearchdataobj", hotelSearchdataobj);

            }
            if (mainMenu) {
                sessionStorage.setItem("mainMenu", mainMenu);
            }
            if (newactiveService) {
                sessionStorage.setItem("newactiveService", newactiveService);
            }
            if (htlSelectTraveller) {
                sessionStorage.setItem("htlSelectTraveller", htlSelectTraveller);
            }

            if (bookingDetail != null) {
                localStorage.setItem("userBookingdetails", bookingDetail);

            }
            // if (guestLogin != null) {
            //     sessionStorage.setItem("guestLoginDatails", guestLogin);

            // }

            if (loginDetails != null) {
                localStorage.setItem("loginName", loginDetails);

            }
            if (sinInDetails != null) {

                sessionStorage.setItem("authToken", sinInDetails);
                sessionStorage.setItem("companyDetails", companyDetails);
                sessionStorage.setItem("refreshToken", refreshTkn);

            } else if (loginDetails != null && sinInDetails != null) {
                localStorage.setItem("loginName", loginDetails);
                sessionStorage.setItem("authToken", sinInDetails);
                sessionStorage.setItem("companyDetails", companyDetails);
                sessionStorage.setItem("refreshToken", refreshTkn);

            } else {
           
            
            }
                // }
            }
             setTimeout(()=>{
                 
                // loadCMS();
             },400)
            
        }

    


}).filter('round', function() {
    /* Use this $filter to round Numbers UP, DOWN and to his nearest neighbour.
       You can also use multiples */

    /* Usage Examples:
        - Round Nearest: {{ 4.4 | round }} // result is 4
        - Round Up: {{ 4.4 | round:'':'up' }} // result is 5
        - Round Down: {{ 4.6 | round:'':'down' }} // result is 4
        ** Multiples
        - Round by multiples of 10 {{ 5 | round:10 }} // result is 10
        - Round UP by multiples of 10 {{ 4 | round:10:'up' }} // result is 10
        - Round DOWN by multiples of 10 {{ 6 | round:10:'down' }} // result is 0
    */
    return function(value, mult, dir) {
        dir = dir || 'nearest';
        mult = mult || 1;
        value = !value ? 0 : Number(value);
        if (dir === 'up') {
            return Math.ceil(value / mult) * mult;
        } else if (dir === 'down') {
            return Math.floor(value / mult) * mult;
        } else {
            return Math.round(value / mult) * mult;
        }
    };
}).filter('roundHotel', function() {
    /* Use this $filter to round Numbers UP, DOWN and to his nearest neighbour.
       You can also use multiples */

    /* Usage Examples:
        - Round Nearest: {{ 4.4 | round }} // result is 4
        - Round Up: {{ 4.4 | round:'':'up' }} // result is 5
        - Round Down: {{ 4.6 | round:'':'down' }} // result is 4
        ** Multiples
        - Round by multiples of 10 {{ 5 | round:10 }} // result is 10
        - Round UP by multiples of 10 {{ 4 | round:10:'up' }} // result is 10
        - Round DOWN by multiples of 10 {{ 6 | round:10:'down' }} // result is 0
    */
    return function(value, mult, dir) {
        dir = dir || 'nearest';
        mult = mult || 1;
        value = !value ? 0 : Number(value);
        if (dir === 'up') {
            return Math.ceil(value / mult) * mult;
        } else if (dir === 'down') {
            return Math.floor(value / mult) * mult;
        } else {
            return Math.ceil(value / mult) * mult;
        }
    };
}).filter('myDateFilter', ['$filter',
    function($filter) {
        return function(input) {

            // set minutes to seconds
            var seconds = input * 60

            // calculate (and subtract) whole days
            var days = Math.floor(seconds / 86400);
            seconds -= days * 86400;

            // calculate (and subtract) whole hours
            var hours = Math.floor(seconds / 3600) % 24;
            seconds -= hours * 3600;

            // calculate (and subtract) whole minutes
            var minutes = Math.floor(seconds / 60) % 60;
            return hours + 'h ' + minutes + 'm ';

            // return days + 'd ' + hours + 'h ' + minutes + 'm ';
        }
    }
]);
ouaApp.controller('loginController', function($scope, NationService, refreshService,$http, ServerService, $timeout,baseUrlService, $uibModalInstance, $window,$location, $rootScope) {
    NationService.nationGet();
    $scope.nationality1 = JSON.parse(localStorage.getItem('nationality'))
    $scope.nationality = JSON.stringify($scope.nationality1);
    $scope.signUp = false;
    $scope.signin = false;
    $scope.userCreation = false;
    $scope.signInTag = false;
    $scope.signUpTag = false;
    $scope.submitted = false;
    $scope.forgotPassTag = false;
    $scope.userRegFormSubmit = false;
    var displayName = {};
    var loginResp = {};
    $scope.userBooking = {}
    $('html, body').animate({scrollTop:0},500);


    /* date picker validation */
    var currentDate = new Date();
    var day = currentDate.getDate();
    var month = currentDate.getMonth() + 1;
    var year = currentDate.getFullYear() - 10;
    if (day < 10) {
        day = '0' + day
    }
    if (month < 10) {
        month = '0' + month
    }
    var today = day + '/' + month + '/' + year
    var maxYear = parseInt(year) - 118;

    $scope.loginOption = {
        singleDatePicker: true,
        showDropdowns: true,
        eventHandlers: {
            'show.daterangepicker': function(ev, picker) {
                $scope.loginOption = {
                    singleDatePicker: true,
                    format: 'DD-MMM-YYYY',
                    startDate: new Date(year, currentDate.getMonth(), currentDate.getDate()),
                    minDate: new Date(maxYear, currentDate.getMonth(), currentDate.getDate()),
                    maxDate: new Date(year, currentDate.getMonth(), currentDate.getDate()),
                    endDate: new Date(maxYear, currentDate.getMonth(), currentDate.getDate())
                }
            }
        }
    }

    $scope.dob = {
            startDate: null,
            endDate: null,
            locale: {
                format: 'YYYY/MM/DD'
            },
            text: ''
        }
        //Change password popup modal
    $scope.changePwd = function(size, parentSelector) {
        var parentElem = parentSelector ?
            angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
        var modalInstance = $uibModal.open({
            animation: $scope.animationEnabled,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'changePwd.html',
            controller: 'loginController',
            backdrop: true,
            size: size,
            appendTo: parentElem,
            resolve: {
                items: function() {
                    return $scope.items
                }
            }
        })
    }

    $scope.createNewAccnt = function() {
        $scope.signUp = true;
        $scope.signin = true;
        $scope.signInTag = true;
        $scope.signUpTag = true;
        $scope.forgotPassTag = false;
        $scope.registermailmsg = '';
        $scope.errorMsgReg ='';
        $scope.newUserRegMsg = false;
        // modal-content
        document.getElementById('registerEmail').value = '';
        $scope.errloginmsg = false;
    }

    $scope.showLogin = function() {
        $('#registerEmailErr').removeClass('has-error');
        $scope.submitted = false;
        $scope.signUp = false;
        $scope.signin = false;
        $scope.signUpTag = false;
        $scope.signInTag = false;
        $scope.forgotPassTag = false;

    }
    $scope.userCreationReqst = {}

    $scope.userRegisterReqst = {}

    /** for signup */
    $scope.userRegisterHeader = function(valid) {
        $scope.submitted = true;
        var regitserMail = {
            "username": $scope.userRegisterReqst.loginid
        };
        $scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));
        var tokenname = $scope.tokenresult.data;
        var headersOptions = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + tokenname.authToken
        }

        $scope.userRegisterReqst.triggermail = "N";
        if (valid) {
            $scope.loadingSignUp = true;
            $scope.disableSignUp = true;
            $http({
                method: 'POST',
                url: ServerService.authPath + 'user/register',
                data: regitserMail,
                headers: headersOptions
            }).then(function successCallback(response) {
                if (response.data.success == true) {
                    $scope.loadingSignUp = false;
                    $scope.disableSignUp = false;
                    $scope.submitted = false;
                    $scope.newUserRegMsg = true;
                    $scope.registermailmsg = response.data.message;
                    $scope.errorMsgReg = '';
                } 
                if (response.data.success == false) {
                    $scope.loadingSignUp = false;
                    $scope.disableSignUp = false;
                    $scope.submitted = false;
                    $scope.newUserRegMsg = false;
                    $scope.registermailmsg = '';
                    $scope.errorMsgReg = response.data.errorMessage;
                } 
            }, function errorCallback(error) {
                if(response.status == 403){
                    refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
                    $scope.userRegisterHeader(valid);
                }
                $scope.registermailmsg = '';
                $scope.loadingSignUp = false;
                $scope.disableSignUp = false;
                $scope.errorMsgReg = "Error message:&nbsp;"+error.status+"&nbsp;&nbsp;"+error.message;
              });
            

           

        }
    }

  

    /** login function**/

    $scope.$watch("loginUserName", function(value) {

        /* 	$('#login_Password').removeClass('has-error'); */
        $('#login_userName').removeClass('has-error');
        $('#login_userName1').removeClass('has-error');
    })
    $scope.$watch("loginPassword", function() {

        $('#login_Password').removeClass('has-error');

    })



 
    /* forgotPassDiv   */
    $rootScope.loggedvalue = JSON.parse(localStorage.getItem('islogged'));
    $scope.forgotPassDiv = function() {
        $('#login_userName1Err').removeClass('has-error');
        $scope.submitforgotuser = false;
        $scope.forForgot1 = true;
        $scope.forForgot = true;
        $scope.hideLoginErr = true;
        $scope.signUpTag = false;
        $scope.signInTag = true;
        $scope.forgotPassTag = true;
        $scope.errloginmsg = false;
    }

    $scope.returns = function() {
        $scope.forForgot1 = false;
        $scope.forForgot = false;
        $scope.signUpTag = false;
        $scope.signInTag = false;
        $scope.forgotPassTag = false;
    }



    $scope.changePasReq = {}
	$scope.userName = JSON.parse(sessionStorage.getItem('loginreq'));
	//$scope.changePasReq.userName = $scope.userName.userName;
	$scope.changePasReq.userName = $scope.userName.username;
    // $scope.changePasReq.oldPassword = $scope.userName.password;
		//$scope.changePasReq.activationDtlId = $scope.userName.activationDtlId;
	// $scope.changePasReq.activationDtlId = 135;
		$scope.close = function(){
		$uibModalInstance.close();
		}
		$scope.resetPaswrd = function(){
            $scope.tokenresult = JSON.parse(sessionStorage.getItem('authToken'));
            var headersOptions = {
              'Content-Type': 'application/json',
              'Authorization': 'Bearer ' + $scope.tokenresult
            }
			
				if($scope.changePasReq.password != $scope.changePasReq.confirmPassword){
					$scope.mismatch = "PassWord And Confirm Passowrd Should be Same";
			} else if($scope.changePasReq.password != '' && $scope.changePasReq.confirmPassword != '' && $scope.changePasReq.userName != '' && $scope.changePasReq.oldPassword != '' && $scope.changePasReq.activationDtlId !=''){
				$scope.mismatch = "";
			
				$http.post(ServerService.authPath+'user/change-password', $scope.changePasReq, { headers: headersOptions }).then(function successCallback(response){
					console.log(response);
					if(response == undefined){
						Lobibox.alert('error', {
                            msg: "Invalid old password"
                        })
					}
					if(response.data.success == true){
					$scope.dispsuccMsg = true;$scope.dispErrMsg = false;
						//$scope.successMsg = response.data.message;
						Lobibox.alert('success', {
							msg: response.data.message,
							callback: function(lobibox, type) {
								if (type === 'ok') {
								
										$scope.dispsuccMsg = false;
										$scope.dispErrMsg = false;
                                        $uibModalInstance.dismiss();
                                         $window.location.href = baseUrlService.UIRidrtUrl;
                                        // $window.location.href="http://localhost/art-b2b/login.html"; 
									
										
								
								}
							}
						});
						localStorage.islogged=false;
						$scope.username='';
						if($location.absUrl().split('username=')[1]){
							$scope.username=$location.absUrl().split('username=')[1];
						}else{
							$scope.username=DEFAULT_USER
						}
								var authenticationreq={
							 "username":$scope.username
							 }
							 if (localStorage.authentication == undefined) {

				/* var Serverurl="http://3.6.54.119:8091/ibp/api/v1/"; */
			 $http.post(ServerService.authPath+'guest-login', JSON.stringify(authenticationreq),{ 
				headers: {'Content-Type': 'application/json'}
					   }).then(function(response){
						   if(response.status == 200){
							$location.path('/home');
							window.location.reload();
							localStorage.removeItem('authentication');
							localStorage.removeItem('userBookingdetails');
							localStorage.removeItem('loginName');
							
						   }
				   var authenticationreqres=response;
				   localStorage.setItem('authentication',JSON.stringify(authenticationreqres));
			 }); 
			 
			}
					} else if(response.data.status == "failure" || response.data.status == "error") {
				$scope.dispErrMsg = true; $scope.dispsuccMsg = false;
					$scope.errorMsg = response.data.errorMessage
					}
					
					// $timeout(function(){
					// 	$scope.dispsuccMsg = false;
					// 	$scope.dispErrMsg = false;
					// 	$uibModalInstance.dismiss();
					// 	}, 2000)
					}, function errorCallback(response) {
						if(response.status == 403){
							var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
							status.then(function(greeting) {
								$scope.resetPaswrd(valid);
					});
						}
					  });	
			}
			
		}


		
	


    $scope.forgotpassword = function() {
            $scope.frtspinner = true;
            var forgotUser = $('#login_userName1').val();
            if (forgotUser == '' || forgotUser == null) {
                $scope.submitforgotuser = true;
                $scope.frtspinner = false;
            } else {
                $scope.submitforgotuser = false;
                var tokenresult = JSON.parse(sessionStorage.getItem('authToken'));
                var headersOptions = {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + tokenresult
                    }
                    //var validatePswdReqst = {"loginid": $scope.loginUserName, "triggermail": "N"};
                    //var forgotPswdReqst = {"loginid": $scope.loginUserName}
                $http.post(ServerService.authPath + 'user/forgot-password', forgotUser, { headers: headersOptions }).then(function(response) {
                    console.log("validatePswdReqst " + response);

                    if (response.data.success) {
                        $scope.frtspinner = false;
                        Lobibox.alert('success', {
                            msg: response.data.message
                        });
                    } else if (response.status == 500) {
                        $scope.frtspinner = false;
                        Lobibox.alert('error', {
                            msg: "Please try again"
                        });
                    }else{
                        $scope.frtspinner = false;
                        Lobibox.alert('error', {
                            msg: response.data.errorMessage
                        });
                    }

                    $uibModalInstance.dismiss();

                    /*else if(response.data.data.userExists == 'Y' && response.data.data.userRegistered == 'N'){
				$scope.travellerUserId = response.data.data.userId;
					$scope.travellerEmail = response.data.data.loginId;
					$scope.forgetRegMsg = true;
					$scope.userCreation = true;
					$scope.signUpTag = true;
					$scope.signInTag = true;
					$scope.forForgot = false;
					$scope.userCreationReqst.loginid = $scope.travellerEmail;
					$timeout(function(){
						$scope.forgetRegMsg = false;
					},4000)
			} else if(response.data.data.userExists == 'N' && response.data.data.userRegistered == 'N'){
				$scope.signUp = true;
				$scope.signUpTag = true;
				$scope.newUserRegMsg = true;
				$scope.forForgot = false;
				$scope.signInTag = true;
				$timeout(function(){
					$scope.newUserRegMsg = false;
					}, 5000)
				}*/
            }, function errorCallback(response) {
                $scope.frtspinner = false;
                if(response.status == 403){
                    var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
                    status.then(function(greeting) {
                        $scope.forgotpassword(loginLoc);
                    });                
                }
              });
            }

        }
        /* end */

    /* modelDismiss function */
    $scope.modelDismiss = function() {
            $uibModalInstance.close();
        }
        /* end */

});

/* unique http service */
ouaApp.service('httpService', function($http, ServerService) {
        var responseData;

        var httpCall = {
            async: function(url, data) {
                if (!responseData) {
                    // $http returns a responseData, which has a then function, which also returns a responseData
                    responseData = $http.post(ServerService.serverPath + url, data).then(function(response) {
                        // The then function here is an opportunity to modify the response
                        // console.log(response);
                        // The return value gets picked up by the then in the controller.
                        return response.data;
                    });
                }
                // Return the responseData to the controller
                return responseData;
            }
        }
        return httpCall;
    })
    /* end */

/*common RefreshToken */
ouaApp.service('refreshService', function($http, ServerService,$q){
    this.refreshToken = function(refToken,authToken){
        // perform some asynchronous operation, resolve or reject the promise when appropriate.
      return $q(function(resolve, reject) {
        var headersOptions = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + refToken
        }
         var authenticationreq = {
            "username": DEFAULT_USER
        }
        $http.post(ServerService.authPath + 'refresh-token', authToken,{headers: headersOptions})
        .then(function success(response) {
            if(response.data.authToken){
                sessionStorage.setItem('authToken', JSON.stringify(response.authToken));
                sessionStorage.setItem('companyDetails', JSON.stringify(response.loginDetails));
                sessionStorage.setItem('refreshToken', JSON.stringify(response.refreshToken));
                localStorage.setItem('timer',response.data.sessionDuration)
                resolve('true')
            }
        }, function error(response) {
                if(response.status == 403){
                    $http.post(ServerService.authPath + 'guest-login', JSON.stringify(authenticationreq), {
                        headers: { 'Content-Type': 'application/json' }
                    }).then(function(response) {
                        if (response.status == 200) {
                            // this.refreshToken(refToken,authToken);
                            var authenticationreqres;
                            if(response.data){
                                authenticationreqres = response;
                                
                                }else{
                                    authenticationreqres.data = response;
                                
                                }
                                localStorage.setItem('authentication', JSON.stringify(authenticationreqres));
                           
                            if(response.data){
                                localStorage.setItem('timer',response.data.sessionDuration)
           
                            }else{
                                localStorage.setItem('timer',response.sessionDuration)
           
                            }
                            
                            resolve('true')
                        }
                    });
                }
                // called asynchronously if an error occurs
                // or server returns response with an error status.
              });
            })
    }
})
/*end*/

/* for passing values from one controller to another */
ouaApp.service('reuseService', function() {
    var requiredData = {}
    return {
        getrequiredData: function() {
            return requiredData;
        },
        setrequiredData: function(values) {
            requiredData = values;
        }
    }

});
ouaApp.service('APIInterceptor', function($location) {
    if (sessionStorage.length == 0) {
        $location.path('/home');
    }

})

/*ouaApp.run(function($rootScope){
	 $rootScope.preventNavigation = false;
$rootScope.$on('$locationChangeStart', function(event, newUrl, oldUrl){
	
	if($rootScope.preventNavigation){
console.log(newUrl); 
        console.log(oldUrl); 
        event.preventDefault(); 

	} 
	
})
 
})*/

ouaApp.service('customSession', function($timeout, $location) {
    var s, m, h;
    var loginDetail = localStorage.getItem('loginName');
    var sessionOut = function() {
        s = 1000 * 60;
        m = s * 60;
        h = m;
        if (localStorage.getItem('loginName')) {
            $timeout(function() {
                //var a =30;
                /* 
                	1 hrs = 60 min;
                	1 min = 60 sec;
                	1 sec = 1000ms;
                	60 sec = 1000*60 = 60000ms;
                	60 min = 3,600,000ms;
                	1 hrs = 3,600,000ms
                	*/
                //var a =1000*60;
                /*s = 1000*60;
                m = s * 60;
                h = m;*/

                // Lobibox.alert('error', {
                //     msg: 'Your session gets expired!'
                // });

                $location.path('/home');

            }, h * 9);
        }

    }


    return sessionOut();
})

history.pushState(null, null, location.href);
window.onpopstate = function() {
    history.go(1);
};



