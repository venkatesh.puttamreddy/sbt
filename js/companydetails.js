ouaApp.service('corpCompanyDetails', function($http, $rootScope, ServerService) {

    this.getCompanyId  = function ()
    {
        $rootScope.hotelSearchTmp = JSON.parse(sessionStorage.getItem('hotelSearchdataobj'));
        $rootScope.companyDetailsTmp = JSON.parse(sessionStorage.getItem('companyDetails'));
        $rootScope.corprateCompanyId = "";

        if($rootScope.hotelSearchTmp && $rootScope.hotelSearchTmp.customers != "")
        {
            $rootScope.corprateCompanyId = $rootScope.hotelSearchTmp.customers;
            $rootScope.isDisableSendQuote = false;
        }
        else
        {
            $rootScope.corprateCompanyId = $rootScope.companyDetailsTmp.companyId;
            $rootScope.isDisableSendQuote = true;
        }
    }

    this.getCompanyIdFlight  = function ()
    {
        $rootScope.flightSearchTmp = JSON.parse(sessionStorage.getItem('flightSearchRQ'));
        $rootScope.companyDetailsTmp = JSON.parse(sessionStorage.getItem('companyDetails'));
        $rootScope.corprateCompanyId = "";

        if($rootScope.flightSearchTmp && $rootScope.flightSearchTmp.customers != ""  && $rootScope.flightSearchTmp.customers != null)
        {
            $rootScope.corprateCompanyId = $rootScope.flightSearchTmp.customers;
            $rootScope.isDisableSendQuoteF = false;
        }
        else
        {
            $rootScope.corprateCompanyId = $rootScope.companyDetailsTmp.companyId;
            $rootScope.isDisableSendQuoteF = true;
        }
    }
    this.getCompanyIdIns  = function ()
    {
        $rootScope.insuranceSearchTmp = JSON.parse(sessionStorage.getItem('insurenceSearchRQ'));
        $rootScope.companyDetailsTmp = JSON.parse(sessionStorage.getItem('companyDetails'));
        $rootScope.corprateCompanyId = "";

        if($rootScope.insuranceSearchTmp && $rootScope.insuranceSearchTmp.customers != "" && $rootScope.insuranceSearchTmp.customers != null)
        {
            $rootScope.corprateCompanyId = $rootScope.insuranceSearchTmp.customers;
            $rootScope.isDisableSendQuoteI = false;
        }
        else
        {
            $rootScope.corprateCompanyId = $rootScope.companyDetailsTmp.companyId;
            $rootScope.isDisableSendQuoteI = true;
        }
    }
})