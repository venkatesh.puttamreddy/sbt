ouaApp.controller('quotationDetailControllers', function($scope, $http,renderHTMLFactory, $location,$uibModal, ServerService,$rootScope) {
    // var Serverurl = "http://localhost:8090/ibp/api/v1/";
    $scope.tokenresult = JSON.parse(sessionStorage.getItem('authToken'));
    // $scope.quotationGroup = JSON.parse(sessionStorage.getItem('sendQuoteData'));
    // $rootScope.isheaderdisabled = false;
    // $scope.$apply();
    var headersOptions = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + $scope.tokenresult
    }
    $scope.renderHTMLFactory = renderHTMLFactory;	
        
    $scope.refrenceNo = $location.search().ref;

    // $scope.flightDetailsQuotation= $scope.quotationGroup;
    // var tokenname = $scope.tokenresult.data;
    $scope.eachFlightDetails = function(index, FlightDetails,childIndex) {
      
        $scope.flightDetailsQuotation[index].segGrpList[childIndex].isOpen = ! $scope.flightDetailsQuotation[index].segGrpList[childIndex].isOpen;
        $scope.scrollTraget = index;
        $scope.flightDetails1 = FlightDetails;
        $scope.segGrpListchildIndex = childIndex;

    }

    $scope.hotelQuotationLength = 0;
    $rootScope.cordinatorDetails = {}

    $scope.quotationObj={};
    $scope.getQuoteList = function(){
      
        $scope.filterrequest={
            "page": null,
            "getAllResults": true,
            "filter": {
              "logic": "and",
              "filters": [
                {
    
                    "field":"quotationNo", 
                    
                    "operator":"eq",
                    
                    "value":$scope.refrenceNo
                    
                            }
              ]
            }
          }

        // https://uat.convergentechnologies.com/b2b/corp/api/v1/quote/retrieve/quoteNo
      $http.post(ServerService.dropDownPath + 'quote/retrieve/quoteNo',$scope.filterrequest, {
       headers: headersOptions
      }).then(function successCallback(response) {
      $scope.artloading = false;
      if(response.data.success)
          {
            $scope.quotationStatus=response.data.data.data[0].quotationStatus;
              sessionStorage.setItem('bookingQuotationResponse',JSON.stringify(response.data.data.data[0]));
              $scope.quotationData = JSON.parse(sessionStorage.getItem('bookingQuotationResponse'));
              $scope.quotationObj.internalRemarks=$scope.quotationData.internalRemarks;  
              $scope.quotationObj.extranelRemarks=$scope.quotationData.externalRemarks; 
              $scope.quotationObj.toEmail=$scope.quotationData.quoteSentTo
              $scope.quotationRes =   $scope.quotationData.serviceDetails.quotation;
              $rootScope.agencyId =   $scope.quotationData.agencyId;
              $rootScope.logoId =   $scope.quotationData.logoId
              $rootScope.imagepath = ServerService.listPath;
            if($scope.quotationRes.hotel['hotel-items'] != undefined)
            {

                var hotelQuote = $scope.quotationRes.hotel['hotel-items'];

                // hotelQuote.forEach(element => {
                //     if(element.gblHotelQuatationList != undefined && element.gblHotelQuatationList.length>0){
                //         $scope.hotelQuotationLength++;
                //     }
                // });
        
                // $rootScope.cordinatorDetails.internalRemarks=$scope.quotationData.data.internalRemarks;  
                // $rootScope.cordinatorDetails.externalRemarks=$scope.quotationData.data.externalRemarks; 
                // $rootScope.cordinatorDetails.email=$scope.quotationData.data.toEmail;
                // $scope.selectTravellerData = $scope.quotationRes.hotel['quote-details'].paxProfiles;
                $scope.gblSelectedHotelResp = hotelQuote;
                // $scope.hotelQuotationLength = $scope.hotelQuotationLength;
                // $scope.selectTraveldata = JSON.parse(sessionStorage.getItem('selectTraveldata'));
                $scope.isDisabled = true;
                $scope.isRadioDisabled = false;
                $scope.product_type = "H";
            }else if($scope.quotationRes.flight['flight-items'] != undefined){
                $scope.product_type = "F";
                $scope.quotation=$scope.quotationData;
                $scope.flightDetailsQuotation =   $scope.quotationData .serviceDetails.quotation.flight['flight-items'];     
                $scope.selectTravellerData=   $scope.quotationData.serviceDetails.quotation.flight['quote-details'].paxProfiles;
                $scope.flightSearchRQ=   $scope.quotationData .serviceDetails.quotation.flight['quote-details'].searchDetails
                sessionStorage.setItem('flightSearchRQ',JSON.stringify($scope.flightSearchRQ));
            																																							
                 sessionStorage.setItem("sendQuoteData", JSON.stringify($scope.quotationGroup));
                 sessionStorage.setItem('selectTraveldata', JSON.stringify($scope.selectTravellerData));
                 $scope.flight=true;
            }
            else{
                $scope.product_type = "I";
                $scope.quotation=$scope.quotationData;
                $scope.selectedQuoteAvilplan=   $scope.quotationData.serviceDetails.quotation.ins['ins-items'].availablePlans;
                $scope.selectedQuoteRiderplan=   $scope.quotationData.serviceDetails.quotation.ins['ins-items'].upsellPlans;	
                $scope.selectTravellerData=   $scope.quotationData.serviceDetails.quotation.ins['quote-details'].paxProfiles;	

                $scope.Insurance=   $scope.quotationData.serviceDetails.quotation.ins['quote-details'].searchDetails
                sessionStorage.setItem('insurenceSearchRQ',JSON.stringify($scope.Insurance));
                sessionStorage.setItem('selectTraveldata', JSON.stringify($scope.selectTravellerData));
          $scope.insurence=true; 	
            }
  
  
          }
          else
          {
        
              Lobibox.alert('error', {
                  msg: response.data.errorMessage
              });
          }
      });
    
    }
    $scope.getQuoteList();
       
    $scope.quotationAtleastOne=false;
$scope.selectedQuotation=function(index){
    $scope.selectedQuotationFlight=[];
    $scope.quotationAtleastOne=true;
    $scope.selectedQuotationFlight.push($scope.flightDetailsQuotation[index])
    $scope.quotation.serviceDetails.quotation.flight['flight-items']=   $scope.selectedQuotationFlight  
}
    sessionStorage.removeItem('SelectedBagLst');
    sessionStorage.removeItem('SelectedMealLst');
    sessionStorage.removeItem('SelectedSeatLst');
    sessionStorage.removeItem("sendQuoteData");
    var priceStart=0;
    var priceEnd=0;
    $scope.hideprice=false;
    $scope.hidestar=false;
    $scope.departureTimehide=false;
    $scope.arrivelHide=false;
    $scope.airlinehideprice=false;
    $scope.fareTypeHide=false;
    $scope.baggageHide=false;
 
   
    $scope.travelPlanQuotation=function(index){
        $scope.quotationAtleastOne=true;
        $scope.lifestyleQuotation=[];
        $scope.lifestyleQuotation.push($scope.selectedQuoteAvilplan[index])
$scope.quotation.serviceDetails.quotation.ins['ins-items'].availablePlans=$scope.lifestyleQuotation;
// $scope.travelPlanQuotation=$scope.selectedQuoteAvilplan[index]
sessionStorage.setItem('avilplan',JSON.stringify($scope.travelPlanQuotation));
}
$scope.lifeStylePlanQuotationvalue=false;
$scope.selectedRiderPlansIndexValue=[];
$scope.selectedRaidPlan = [];
$scope.lifeStylePlanQuotation=function(index){
    $scope.lifeStylePlanQuotationvalue=true;
    // $scope.lifeStylePlanQuotation=[];
    // $scope.lifeStylePlanQuotation.push($scope.selectedQuoteRiderplan[index])


  
	if($scope.selectedRiderPlansIndexValue.includes(index)){
		$scope.selectedRaidPlan.pop($scope.selectedQuoteRiderplan[index])
        $scope.quotation.serviceDetails.quotation.ins['ins-items'].upsellPlans=$scope.selectedRaidPlan
		$scope.selectedRiderPlansIndexValue.pop(index);
	

	}else{
		$scope.riderList = $scope.selectedRaidPlan.push($scope.selectedQuoteRiderplan[index]);
                $scope.quotation.serviceDetails.quotation.ins['ins-items'].upsellPlans=$scope.selectedRaidPlan
		$scope.selectedRiderPlansIndexValue.push(index);
		$scope.isLifeStylePlanSelected=true;

	

	}
	sessionStorage.setItem('ridersplan',JSON.stringify($scope.selectedRaidPlan));

}

$scope.selectedPlanBean=[];
$scope.selectedriderPlans=[];
$scope.sendquotationApproval=function(){

 
   
    var currency="AED"
    $scope.quotationApproval={};

    $scope.quotationApproval=$scope.quotation;
   
    $http.post(ServerService.dropDownPath+$scope.quoteURL,$scope.quotationApproval,{ 
    // $http.post(ServerService.dropDownPath+'customer/'+''+$scope.quotationObj.corporateId+'/I/session/store',$scope.insQuotationBook,{ 
     
      
}).then(function successCallback(response){


       if(response.data.success==true){
        Lobibox.notify('success', {
            size: 'mini',
            position: 'top right',
            msg:response.data.message,
            delay: 1500
        });
        }else{
            $scope.artloading = true;
        } 
               
       
       
  });

}


$scope.approvalRemarks = "";
$scope.rejectRemarks = "";

$scope.sendApprovalRemarks = function(){

    var approvalRemarks = {};
    if( $scope.quotationAtleastOne==false && $scope.lifeStylePlanQuotationvalue==false ){
        Lobibox.notify('error', {
            size: 'mini',
            position: 'top right',
            msg:"Please select Travel plans or Lifestyle Plans!",
            delay: 1500
        });
        return;
    } 
    Lobibox.prompt('text', //Any input type will be valid
    {
        title: 'Please enter Approval Remarks',
        //Attributes of <input>
        attrs: { 
            placeholder: "Approval Remarks"
        },
        buttons: ['ok','cancel'],
        callback: function (lobibox, type) {
            if(type == 'ok')
            {
                approvalRemarks = document.getElementsByClassName("lobibox-input");
                $scope.approvalRemarks = approvalRemarks[0].value;
                $scope.quotation.approvalRemarks= $scope.approvalRemarks
                $scope.quoteURL =  "quote/customerApproval";
                $scope.sendquotationApproval();
            }
        }
    });
}

$scope.sendRejectRemarks = function(){
    var rejectRemarks = {};
    Lobibox.prompt('text', //Any input type will be valid
    {
        title: 'Please enter Rejection Remarks',
        //Attributes of <input>
        attrs: { 
            placeholder: "Rejection Remarks"
        },
        buttons: ['ok','cancel'],
        callback: function (lobibox, type) {
            if(type == 'ok')
            {
                rejectRemarks = document.getElementsByClassName("lobibox-input");
                $scope.rejectRemarks = rejectRemarks[0].value;
                $scope.quotation.rejectedRemarks= $scope.rejectRemarks
                $scope.quoteURL= "quote/reject";
                $scope.sendquotationApproval();
            }
        }
    });
}

$scope.miniFareRules = function(size, parentSelector, index) {
    var parentElem = parentSelector ?
        angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
    $rootScope.selectedFareIndex = index;
    $scope.animationEnabled = true;
    var modalInstance = $uibModal.open({
        animation: $scope.animationEnabled,
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: 'flightFareContent.html',
        controller: 'flightFareController',
        backdrop: true,
        size: size,
        appendTo: parentElem,
        resolve: {
            items: function() {
                return $scope.items
            }
        }
    })
}

$scope.outPolicyCheck = function(size, parentSelector, outPolicy,moduleName) {
    var parentElem = parentSelector ?
        angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
   
     $rootScope.outPolicyData = outPolicy;
     $rootScope.outPolicyData.module=moduleName


    $scope.animationEnabled = true;
    var modalInstance = $uibModal.open({
        animation: $scope.animationEnabled,
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: 'outPolicy.html',
        controller: 'outPolicyController',
        backdrop: true,
        size: size,
        appendTo: parentElem,
        resolve: {
            items: function() {
                return $scope.items
            }
        }
    })
}

})
ouaApp.controller('flightFareController', function($scope, $http, ServerService, $uibModalInstance, $rootScope) {

     
        var index = $rootScope.selectedFareIndex;

        $scope.farerule=index.fareRules
        $scope.flightDetail1 = $rootScope.selectedFareIndex
        $scope.baggagedatafinding = $scope.flightDetail1.additionalProperties.SSR;
        // $scope.refundtype =  $scope.flightDetail1.data.refund
        $scope.flightDetail2 = $rootScope.selectedFareIndex.fareslst;
        $scope.airlinenames = $rootScope.selectedFareIndex.segGrpList[0].segList[0].airlineName;
        $scope.aircraft = $rootScope.selectedFareIndex.segGrpList[0].segList[0].fltInfoList[0].aircraftType;
        if ($scope.aircraft == null) {
            $scope.aircraft = "-";
        }

    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };
    $scope.refundtypes = $rootScope.selectedFareIndex.refund;
    if ($scope.refundtypes == true) {
        $scope.refundtype = "Refundable";
    } else {
        $scope.refundtype = "Non-Refundable";
    }
  



})
ouaApp.controller('outPolicyController', function($scope, $http,ServerService, $uibModalInstance, $rootScope) {
    $scope.bookingQuotationResponse = JSON.parse(sessionStorage.getItem('bookingQuotationResponse'));
    $scope.corprateCompanyId=$scope.bookingQuotationResponse.data.agencyId;
       $scope.policydetails= $rootScope.outPolicyData;
       if( $scope.policydetails.module=="FLT"){
      $scope.policyRequestObj={};
      $scope.policyRequestObj.origin=  $scope.policydetails.origin
      $scope.policyRequestObj.dest=  $scope.policydetails.destination
      $scope.policyRequestObj.stops= $scope.policydetails.segGrpList[0].segList[0].stops
      $scope.policyRequestObj.refund=  $scope.policydetails.refund
      $scope.policyRequestObj.classType=  $scope.policydetails.classType
      $scope.policyRequestObj.designaiton="Manager",
      $scope.policyRequestObj.airlineCode=$scope.policydetails.segGrpList[0].segList[0].airline
      $scope.policyRequestObj.travelType= null
      $scope.quoteURL= "/flight/deviation";
       }else if($scope.policydetails.module=="INS"){
      
        $scope.Insurance = JSON.parse(sessionStorage.getItem('insurenceSearchRQ')); 
        $scope.policyRequestObj={};
        $scope.policyRequestObj.onwardDateTime=  $scope.Insurance.onwardDateTime
        $scope.policyRequestObj.returnDateTime=  $scope.Insurance.returnDateTime
        $scope.policyRequestObj.planTitle=  $scope.policydetails.planAdditionalInfoTitle
        $scope.policyRequestObj.designaiton= $scope.Insurance.designation,
        $scope.quoteURL= "/insurance/deviation";
       }else{
        
        $scope.hoteldata=$scope.bookingQuotationResponse.data.serviceDetails.quotation.hotel['hotel-items']
        $scope.hotelSearch=$scope.bookingQuotationResponse.data.serviceDetails.quotation.hotel['quote-details'].searchDetails
        $scope.policyRequestObj={};
        $scope.policyRequestObj.checkInDate=  $scope.hotelSearch.checkIn
        $scope.policyRequestObj.checkOutDate=  $scope.hotelSearch.checkOut
        $scope.policyRequestObj.city=  $scope.hotelSearch.city
        $scope.policyRequestObj.starRating= $scope.hoteldata[0].starRating.toFixed(1),
        $scope.policyRequestObj.totalFare=   $scope.hoteldata[0].totalFare,
        $scope.policyRequestObj.designation= $scope.hotelSearch.designation
        $scope.quoteURL= "/hotel/deviation";
       }
      $scope.tokenresult = JSON.parse(sessionStorage.getItem('authToken'));
      var headersOptions = {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + $scope.tokenresult
      }
  
    $http.post(ServerService.dropDownPath + 'customer/'+ $scope.corprateCompanyId + $scope.quoteURL,$scope.policyRequestObj, {
    
        headers: headersOptions
    }).then(function successCallback(response) {
        
    $scope.loading = false;
    $scope.disabled = false;
    if (response.data.success == true) {
        $scope.outPolicy=response.data.data;

    }
    });
    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };

});




