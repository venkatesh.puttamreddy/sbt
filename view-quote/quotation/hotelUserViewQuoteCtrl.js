ouaApp.controller('hotelUserViewQuoteCtrl', function($scope, $http,renderHTMLFactory, $location,$uibModal, ServerService,$rootScope) {
    
    $scope.hotelUserViewQuote = JSON.parse(sessionStorage.getItem('bookingQuotationResponse'));
    $scope.isUserView = true;
    $scope.roomSelection = '';
    
    $scope.isNewQuote = $scope.hotelUserViewQuote.quotationStatus == 25445 ? true : false;
    $scope.isDisabled = $scope.isNewQuote;
    $scope.selectedHotel=false;
    $scope.slectHotelquote =  function(event,selectedroom,parentindex,index){
        $scope.selectedParentIndx = parentindex;
        $scope.selectedIndx = index;
        $scope.selectedHotel=true;
    }

    approvalservice = function(tmpapprovalobj){

        $scope.quotationReq = $scope.hotelUserViewQuote.data;
        $scope.quotationReq.serviceDetails.quotation.hotel['hotel-items']= tmpapprovalobj;
        $scope.quotationReq.rejectedRemarks = $scope.rejectRemarks;
        $scope.quotationReq.approvalRemarks = $scope.approvalRemarks;

        $http.post(ServerService.dropDownPath + $scope.quoteURL,$scope.quotationReq, {
            // headers: { 'Content-Type': 'application/json','Authorization': 'Bearer ' + authenticationreqres.data.authToken }
            }).then(function successCallback(response) {
                $scope.artloading = false;
                if(response.data.success)
                {
                    $scope.isNewQuote = false;
                    Lobibox.notify('success', {
                        size: 'mini',
                        position: 'top right',
                        msg:response.data.message,
                        delay: 1500
                    });
                }
            });
    }

    $scope.approvalRemarks = "";
    $scope.rejectRemarks = "";

    $scope.sendApprovalRemarks = function(){

        var approvalRemarks = {};
        if( $scope.selectedHotel==false){
            Lobibox.notify('error', {
                size: 'mini',
                position: 'top right',
                msg:"Please select at least one hotel!",
                delay: 1500
            });
            return;
        } 
		Lobibox.prompt('text', //Any input type will be valid
		{
			title: 'Please enter Approval Remarks',
			//Attributes of <input>
			attrs: { 
				placeholder: "Approval Remarks"
			},
			buttons: ['ok','cancel'],
			callback: function (lobibox, type) {
				if(type == 'ok')
				{
                    let tmpapprovalobj = $scope.gblSelectedHotelResp;
                    tmpapprovalobj[$scope.selectedParentIndx].gblHotelQuatationList = tmpapprovalobj[$scope.selectedParentIndx].gblHotelQuatationList.filter(function(item,index){
                        return index == $scope.selectedIndx;
                    });
            
					approvalRemarks = document.getElementsByClassName("lobibox-input");
					$scope.approvalRemarks = approvalRemarks[0].value;
                    $scope.quoteURL =  "quote/customerApproval";
                    approvalservice(tmpapprovalobj);
				}
			}
		});
    }

    $scope.sendRejectRemarks = function(){
        var rejectRemarks = {};
		Lobibox.prompt('text', //Any input type will be valid
		{
			title: 'Please enter Rejection Remarks',
			//Attributes of <input>
			attrs: { 
				placeholder: "Rejection Remarks"
			},
			buttons: ['ok','cancel'],
			callback: function (lobibox, type) {
				if(type == 'ok')
				{
					rejectRemarks = document.getElementsByClassName("lobibox-input");
					$scope.rejectRemarks = rejectRemarks[0].value;
                    $scope.quoteURL =  "quote/reject";

                    approvalservice($scope.gblSelectedHotelResp);
				}
			}
		});
    }

    $scope.hotelTabAction  = function(tab,index)
    {
         switch(tab){
             case 1 : 
                     $(".roomopt_"+index).addClass('active');
                     $(".roomopt_"+index).addClass('show');
                     $(".facitiesopt_"+index).removeClass('active');
                     $(".facitiesopt_"+index).removeClass('show');
                     $(".locopt_"+index).removeClass('active');
                     $(".locopt_"+index).removeClass('show');
                     $(".aboutopt_"+index).removeClass('active');
                     $(".aboutopt_"+index).removeClass('show');
                 break;
             case 2 :
                     $(".roomopt_"+index).removeClass('active');
                     $(".roomopt_"+index).removeClass('show');
                     $(".facitiesopt_"+index).addClass('active');
                     $(".facitiesopt_"+index).addClass('show');
                     $(".locopt_"+index).removeClass('active');
                     $(".locopt_"+index).removeClass('show');
                     $(".aboutopt_"+index).removeClass('active');
                     $(".aboutopt_"+index).removeClass('show');
                 break;
             case 3 :
                     $(".roomopt_"+index).removeClass('active');
                     $(".roomopt_"+index).removeClass('show');
                     $(".facitiesopt_"+index).removeClass('active');
                     $(".facitiesopt_"+index).removeClass('show');
                     $(".locopt_"+index).addClass('active');
                     $(".locopt_"+index).addClass('show');
                     $(".aboutopt_"+index).removeClass('active');
                     $(".aboutopt_"+index).removeClass('show');
                 break;
             case 4 :
                     $(".roomopt_"+index).removeClass('active');
                     $(".roomopt_"+index).removeClass('show');
                     $(".facitiesopt_"+index).removeClass('active');
                     $(".facitiesopt_"+index).removeClass('show');
                     $(".locopt_"+index).removeClass('active');
                     $(".locopt_"+index).removeClass('show');
                     $(".aboutopt_"+index).addClass('active');
                     $(".aboutopt_"+index).addClass('show');
                 break;
         }
    }

    $scope.hotelcancelationPolicy = function (size, parentSelector, roomInfo,parentIndex) {
      sessionStorage.setItem('selectedHotelResp', JSON.stringify({data:$scope.gblSelectedHotelResp[parentIndex]}));
      var parentElem = parentSelector ?
          angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
          $rootScope.selectedroomInfo = roomInfo;
          $scope.animationEnabled = true;
          $rootScope.cancellationPolicy = roomInfo.hotelCPolicy
          $scope.hotelCancelationModalInstance=$uibModal.open({
            templateUrl: 'hotelCancelation.tmpl.html',
            backdrop: 'static',
            size: size,
            keyboard: true,
            scope:$scope
        });
      }
  
      $scope.hotelRate = function (size, parentSelector, roomInfo,parentIndex) {
      sessionStorage.setItem('selectedHotelResp', JSON.stringify({data:$scope.gblSelectedHotelResp[parentIndex]}));
      var parentElem = parentSelector ?
        angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
      $rootScope.selectedroomInfo = roomInfo;
      $rootScope.rateComments = roomInfo.hotelratecmds;
      $scope.animationEnabled = true;
      $scope.hotelNotesModalInstance=$uibModal.open({
        templateUrl: 'hotelRate.tmpl.html',
        backdrop: 'static',
        size: size,
        keyboard: true,
        scope:$scope
    });
    }

    $scope.cancelpsasolicy = function () {
      $scope.hotelCancelationModalInstance.dismiss('cancel');
    };

  $scope.notespsasolicy = function () {
      $scope.hotelNotesModalInstance.dismiss('cancel');
  };

});
