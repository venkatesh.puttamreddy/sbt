var ouaApp = angular.module('ouaApp', ['ngAnimate','ui.select', 'ngTouch', 'ngSanitize', 'ui.bootstrap', 'ui.router', 'ngTasty', 'ui', 'ui-rangeSlider', 'daterangepicker', 'uiSwitch', 'duScroll', 'ngCookies', 'checklist-model', 'rzModule', 'localytics.directives', 'angularUtils.directives.dirPagination', 'angularytics']);

ouaApp.run(function(Angularytics) {
    Angularytics.init();

});

ouaApp.filter('unsafe', function($sce) { return $sce.trustAsHtml; });

ouaApp.config(function($stateProvider, $urlRouterProvider, $httpProvider, AngularyticsProvider,$qProvider) {
    $cookiesSessionId = Math.random().toString(36).substr(2);
	document.cookie = 'CSRF-TOKEN='+$cookiesSessionId+';path=/'
 // window.ga('create', 'UA-144585841-1', 'auto'); 
    // $rootScope.$on('$stateChangeSuccess', function (event) {
    // $window.ga('send', 'pageview', $location.path());
    // });
    // window.scroll(0,0);
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }
    $('html, body').animate({scrollTop:0},500);

    // Answer edited to include suggestions from comments
    // because previous version of code introduced browser-related errors

    //disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
    // extra
    $httpProvider.defaults.headers.get['Cache-Control'] = 'no-cache';
    $httpProvider.defaults.headers.get['Pragma'] = 'no-cache';
    AngularyticsProvider.setEventHandlers(['Console', 'GoogleUniversal']);
    $urlRouterProvider.otherwise('/home');


    // if(sessionStorage.timer){
    //     expiryService.sessionExpire();
    // }
    $stateProvider.state('view-quote', {
        url: '/view-quote',
        templateUrl: 'quotation/quotationDetails.html',
        controller: 'quotationDetailControllers'
    });
    $qProvider.errorOnUnhandledRejections(false);

});


ouaApp.service("ajaxService", function($http, $q) {
    var self = this;
    self.data = null;
    self.ajaxRequest = function(met, url, req, res) {
        var deferred = $q.defer();
        if (self.data !== null) {
            deferred.resolve(self.data);
        } else {
            $http({
                method: met,
                url: url,
                data: req,
                responseType: res
            }).then(function(response) {
                //self.data = response.data;
                deferred.resolve(response.data);
            }).catch(function(response) {
                deferred.reject(response);
            });
        }
        return deferred.promise;
    };
});
ouaApp.factory('renderHTMLFactory', function() {
    var renderObj = {};
    var insTitle = [];
    renderObj.setTitle = function(title) {
        var setTitleVal;
        var setFamilyVal;
        var spltTitle = title.search('Tune Protect');
        if (spltTitle !== -1) {
            insTitle = title.split('-');
            var temp = title.split('Tune Protect');
            var setTitleVal = temp[1];

            var splfamily = setTitleVal.indexOf('(Family)');
            if (splfamily !== -1) {
                var tempfamily = setTitleVal.split('(Family)');
                var setFamilyVal = tempfamily[0].split('-');

                setTitleVal = setFamilyVal[0] + " Family -" + setFamilyVal[1];
                console.log(setTitleVal);
            } else {}
        } else {
            setTitleVal = title
        }
        return setTitleVal;
    }

    renderObj.renderHTML = function(html_code) {
        var decoded = angular.element('<textarea />').html(html_code).text();
        //var titlecontent= insTitle[1];
        var spltTitle = decoded.indexOf('with Tune Protect');
        if (spltTitle !== -1) {

            var titlecontent = decoded.replace(/with Tune Protect/g, "");
        } else {
            var titlecontent = decoded.replace(/Tune Protect/g, "");
        }

        return titlecontent;
    }
    return renderObj;
});
ouaApp.service('ConstantService', function() {
    // this.currency = 'OMR'
    this.currency = 'AED'
});


/* end */
ouaApp.service('urlService', function($location) {
    //this.relativePath = $location.absUrl().split('/')[6];
    var currentURL = $location.absUrl();
    this.relativePath = currentURL.substring(currentURL.lastIndexOf("/") + 1, currentURL.length);

});

ouaApp.service('ServerService', function($location, baseUrlService) {
    this.serverPath = baseUrlService.baseUrl + "ibp/api/v1/";
    this.dropDownPath = baseUrlService.baseUrl + "corp/api/v1/";
    this.listPath = baseUrlService.baseUrl + "rest/api/v1/";
    this.packagePath = baseUrlService.baseUrl + "tours/api/v1/";
    this.profilePath = baseUrlService.baseUrl + "profile/api/v1/";
    this.authPath = baseUrlService.baseUrl + "auth/api/v1/";
    this.cmsPath = baseUrlService.baseUrl + "cms/api/v1/";
    this.authPathlatest = baseUrlService.baseUrl + "auth/api/v2/";
});


ouaApp.service('sessionService', function($http, $location, ServerService,expiryService) {
    $('html, body').animate({scrollTop:0},500);

    username = '';
    this.clearRouteData = function(value) {

        var itemList = value.length;
        // value == 'home' 
		/* value == 'flight'  */
        if (value == 'flight' || value == 'sign-out' || value == 'hotel' || value == 'holiday' || value == 'insurance' || value == 'user') {
           sessionStorage.removeItem("selectTraveldata");
           	var metas = document.getElementsByTagName('meta');
	var pageTitle = document.getElementsByTagName('title'); 
   pageTitle[0].innerHTML='ART'
	
 var loginDetails = null;
            var bookingDetail = null;
            var sinInDetails = null;
            var insurenceSearchRQ = null;
            var flightSearchRQ = null;
            var configKeys=null;
            var hotelSearchdataobj = null;
            var loginTravellerData = null;
            var refreshTkn = null;
            var companyDetails = null;

            // var guestLogin = null
            if (localStorage.getItem("loginName")) {
                loginDetails = localStorage.getItem("loginName");
            }
            if (localStorage.getItem("userBookingdetails")) {
                bookingDetail = localStorage.getItem("userBookingdetails");
            }
        //    if(sessionStorage.getItem('guestLoginDatails')){
        //     guestLogin=sessionStorage.getItem('guestLoginDatails');
        //    }
            if (sessionStorage.getItem("authToken")) {
                sinInDetails = sessionStorage.getItem("authToken");
            }
            if (sessionStorage.getItem("refreshToken")) {
                refreshTkn = sessionStorage.getItem("refreshToken");
            }
            if (sessionStorage.getItem("companyDetails")) {
                companyDetails = sessionStorage.getItem("companyDetails");
            }
            if (sessionStorage.getItem("insurenceSearchRQ")) {
                insurenceSearchRQ = sessionStorage.getItem("insurenceSearchRQ");
            }
            if (sessionStorage.getItem("flightSearchRQ")) {
                flightSearchRQ = sessionStorage.getItem("flightSearchRQ");
            }
            if (localStorage.getItem("configKeys")) {
                configKeys = localStorage.getItem("configKeys");
            }
            if (sessionStorage.getItem("hotelSearchdataobj")) {
                hotelSearchdataobj = sessionStorage.getItem("hotelSearchdataobj");
            }

            // if (localStorage.getItem("loginTravellerData")) {
            //     loginTravellerData = localStorage.getItem("loginTravellerData");
            // }

             sessionStorage.clear();
            //  localStorage.clear();
            localStorage.removeItem("loginToken");

            if (flightSearchRQ) {
                sessionStorage.setItem("flightSearchRQ", flightSearchRQ);

            }
            if (configKeys) {
                localStorage.setItem("configKeys", configKeys);

            }
            if (insurenceSearchRQ) {
                sessionStorage.setItem("insurenceSearchRQ", insurenceSearchRQ);

            }
            if (hotelSearchdataobj) {
                sessionStorage.setItem("hotelSearchdataobj", hotelSearchdataobj);

            }
            // if (loginTravellerData) {
            //     sessionStorage.setItem("loginTravellerData", loginTravellerData);
            // }

            if (bookingDetail != null) {
                localStorage.setItem("userBookingdetails", bookingDetail);

            }
            // if (guestLogin != null) {
            //     sessionStorage.setItem("guestLoginDatails", guestLogin);

            // }

            if (loginDetails != null) {
                localStorage.setItem("loginName", loginDetails);

            }
            if (sinInDetails != null) {

                sessionStorage.setItem("authToken", sinInDetails);
                sessionStorage.setItem("companyDetails", companyDetails);
                sessionStorage.setItem("refreshToken", refreshTkn);

            } else if (loginDetails != null && sinInDetails != null) {
                localStorage.setItem("loginName", loginDetails);
                sessionStorage.setItem("authToken", sinInDetails);
                sessionStorage.setItem("companyDetails", companyDetails);
                sessionStorage.setItem("refreshToken", refreshTkn);

            } else {
           
            
            }
                // }
            }
             setTimeout(()=>{
                 
                // loadCMS();
             },400)
            
        }

    


}).filter('round', function() {
    /* Use this $filter to round Numbers UP, DOWN and to his nearest neighbour.
       You can also use multiples */

    /* Usage Examples:
        - Round Nearest: {{ 4.4 | round }} // result is 4
        - Round Up: {{ 4.4 | round:'':'up' }} // result is 5
        - Round Down: {{ 4.6 | round:'':'down' }} // result is 4
        ** Multiples
        - Round by multiples of 10 {{ 5 | round:10 }} // result is 10
        - Round UP by multiples of 10 {{ 4 | round:10:'up' }} // result is 10
        - Round DOWN by multiples of 10 {{ 6 | round:10:'down' }} // result is 0
    */
    return function(value, mult, dir) {
        dir = dir || 'nearest';
        mult = mult || 1;
        value = !value ? 0 : Number(value);
        if (dir === 'up') {
            return Math.ceil(value / mult) * mult;
        } else if (dir === 'down') {
            return Math.floor(value / mult) * mult;
        } else {
            return Math.round(value / mult) * mult;
        }
    };
}).filter('roundHotel', function() {
    /* Use this $filter to round Numbers UP, DOWN and to his nearest neighbour.
       You can also use multiples */

    /* Usage Examples:
        - Round Nearest: {{ 4.4 | round }} // result is 4
        - Round Up: {{ 4.4 | round:'':'up' }} // result is 5
        - Round Down: {{ 4.6 | round:'':'down' }} // result is 4
        ** Multiples
        - Round by multiples of 10 {{ 5 | round:10 }} // result is 10
        - Round UP by multiples of 10 {{ 4 | round:10:'up' }} // result is 10
        - Round DOWN by multiples of 10 {{ 6 | round:10:'down' }} // result is 0
    */
    return function(value, mult, dir) {
        dir = dir || 'nearest';
        mult = mult || 1;
        value = !value ? 0 : Number(value);
        if (dir === 'up') {
            return Math.ceil(value / mult) * mult;
        } else if (dir === 'down') {
            return Math.floor(value / mult) * mult;
        } else {
            return Math.ceil(value / mult) * mult;
        }
    };
}).filter('myDateFilter', ['$filter',
    function($filter) {
        return function(input) {

            // set minutes to seconds
            var seconds = input * 60

            // calculate (and subtract) whole days
            var days = Math.floor(seconds / 86400);
            seconds -= days * 86400;

            // calculate (and subtract) whole hours
            var hours = Math.floor(seconds / 3600) % 24;
            seconds -= hours * 3600;

            // calculate (and subtract) whole minutes
            var minutes = Math.floor(seconds / 60) % 60;
            return hours + 'h ' + minutes + 'm ';

            // return days + 'd ' + hours + 'h ' + minutes + 'm ';
        }
    }
]);

/* unique http service */
ouaApp.service('httpService', function($http, ServerService) {
        var responseData;

        var httpCall = {
            async: function(url, data) {
                if (!responseData) {
                    // $http returns a responseData, which has a then function, which also returns a responseData
                    responseData = $http.post(ServerService.serverPath + url, data).then(function(response) {
                        // The then function here is an opportunity to modify the response
                        // console.log(response);
                        // The return value gets picked up by the then in the controller.
                        return response.data;
                    });
                }
                // Return the responseData to the controller
                return responseData;
            }
        }
        return httpCall;
    })
    /* end */

/* for passing values from one controller to another */
ouaApp.service('reuseService', function() {
    var requiredData = {}
    return {
        getrequiredData: function() {
            return requiredData;
        },
        setrequiredData: function(values) {
            requiredData = values;
        }
    }

});
ouaApp.service('APIInterceptor', function($location) {
    if (sessionStorage.length == 0) {
        $location.path('/home');
    }

})

/*ouaApp.run(function($rootScope){
	 $rootScope.preventNavigation = false;
$rootScope.$on('$locationChangeStart', function(event, newUrl, oldUrl){
	
	if($rootScope.preventNavigation){
console.log(newUrl); 
        console.log(oldUrl); 
        event.preventDefault(); 

	} 
	
})
 
})*/

ouaApp.service('customSession', function($timeout, $location) {
    var s, m, h;
    var loginDetail = localStorage.getItem('loginName');
    var sessionOut = function() {
        s = 1000 * 60;
        m = s * 60;
        h = m;
        if (localStorage.getItem('loginName')) {
            $timeout(function() {
                //var a =30;
                /* 
                	1 hrs = 60 min;
                	1 min = 60 sec;
                	1 sec = 1000ms;
                	60 sec = 1000*60 = 60000ms;
                	60 min = 3,600,000ms;
                	1 hrs = 3,600,000ms
                	*/
                //var a =1000*60;
                /*s = 1000*60;
                m = s * 60;
                h = m;*/

                // Lobibox.alert('error', {
                //     msg: 'Your session gets expired!'
                // });

                $location.path('/home');

            }, h * 9);
        }

    }


    return sessionOut();
})

history.pushState(null, null, location.href);
window.onpopstate = function() {
    history.go(1);
};



