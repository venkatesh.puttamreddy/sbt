<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Get Payment Page</title>
</head>
<body>
	<script>
		window.onload = function() {
			document.paymentPage.submit();
		};
	</script>
	<form id="paymentPage" accept-charset="UTF-8" method="post" name="paymentPage" action="<%= request.getAttribute("postUrl")%>">
	    <input id="hdn_access_code" type="hidden" value="<%= request.getAttribute("access_code")%>" name="access_code">
	    <input id="hdn_language" type="hidden" value="<%= request.getAttribute("language")%>" name="language">
	    <input id="hdn_merchant_id" type="hidden" value="<%= request.getAttribute("merchant_identifier")%>" name="merchant_identifier">
	    <input id="hdn_merchant_ref" type="hidden" value="<%= request.getAttribute("merchant_reference")%>" name="merchant_reference">
	    <input id="hdn_return_url" type="hidden" value="<%= request.getAttribute("return_url")%>" name="return_url">
	    <input id="hdn_serv_command" type="hidden" value="<%= request.getAttribute("service_command")%>" name="service_command">
	    <input id="hdn_signature" type="hidden" value="<%= request.getAttribute("signature")%>" name="signature">
	    <input id="hdn_tok_name" type="hidden" value="<%= request.getAttribute("token_name") %>" name="token_name">
	</form>
</body>
</html>