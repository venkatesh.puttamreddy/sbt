var ouaApp = angular.module('ouaApp', ['ngAnimate', 'ngTouch', 'ngSanitize', 'ui.bootstrap', 'ui.router', 'ngTasty', 'ui', 'ui-rangeSlider', 'daterangepicker', 'uiSwitch', 'duScroll', 'ngCookies', 'checklist-model', 'rzModule']);

ouaApp.config(function($stateProvider, $urlRouterProvider, $httpProvider) {
    $urlRouterProvider.otherwise('/home');
    $stateProvider.state('home', {
        url: '/home',
        templateUrl: 'views/home/home.html',
        controller: 'homeController',
        resolve: {
            init: function(sessionService, $stateParams, $state) {
                sessionService.clearRouteData('home');
            }
        }
    }).state('flightResult', {
        url: '/flightResult',
        templateUrl: 'views/flight/flightSearchResult.html',
        controller: 'flightResultController'
    }).state('flightDetails', {
        url: '/flightDetails',
        templateUrl: 'views/flight/flightDetails.html',
        controller: 'flightDetailsController'
    }).state('flightReview', {
        url: '/flightReview',
        templateUrl: 'views/flight/flightReview.html',
        controller: 'flightReviewController'
    }).state('hotelResult', {
        url: '/hotelResult',
        templateUrl: 'views/hotel/hotelResult.html',
        controller: 'hotelResultController'
    }).state('hotelDetails', {
        url: '/hotelDetails',
        templateUrl: 'views/hotel/hotelDetails.html',
        controller: 'hotelDetailsController'
    }).state('holidayFlightDetails', {
        url: '/holidayFlightDetails',
        templateUrl: 'views/flight/flightDetails.html',
        controller: 'flightDetailsController'
    }).state('hotelReview', {
        url: '/hotelReview',
        templateUrl: 'views/hotel/hotelReview.html',
        controller: 'hotelReviewController'
    }).state('holidaySearchResult', {
        url: '/holidaySearchResult',
        templateUrl: 'views/holiday/holidaySearchResult.html',
        controller: 'holidaySearchResultController'
    }).state('holidayDetails', {
        url: '/holidayDetails',
        templateUrl: 'views/holiday/holidayDetails.html',
        controller: 'holidayDetailsController'
    }).state('holidayReview', {
        url: '/holidayReview',
        templateUrl: 'views/holiday/holidayReview.html',
        controller: 'holidayReviewController'
    }).state('holidayBook', {
        url: '/holidayBook',
        templateUrl: 'views/holiday/holidayBook.html',
        controller: 'holidayBookController'
    }).state('insuranceResult', {
        url: '/insuranceResult',
        templateUrl: 'views/insurance/insuranceResult.html',
        controller: 'insuranceResultController'
    }).state('insuranceReview', {
        url: '/insuranceReview',
        templateUrl: 'views/insurance/insuranceReview.html',
        controller: 'insuranceReviewController'
    }).state('searchingFlight', {
        url: '/searchingFlight',
        templateUrl: 'views/loadingPage.html',
        controller: 'flightLoading'
    }).state('searchingHotel', {
        url: '/searchingHotel',
        templateUrl: 'views/loadingPage.html',
        controller: 'hotelLoadingCtrl'
    }).state('searchingHoliday', {
        url: '/searchingHoliday',
        templateUrl: 'views/loadingPage.html',
        controller: 'flightHotelController'
    }).state('searchingInsurance', {
        url: '/searchingInsurance',
        templateUrl: 'views/loadingPage.html',
        controller: 'insuranceLoadingController'
    }).state('flightItenary', {
        url: '/flightItenary',
        templateUrl: 'views/flight/flightItenary.html',
        controller: 'flightItenaryController'
    }).state('hotelItenary', {
        url: '/hotelItenary',
        templateUrl: 'views/hotel/hotelItenenary.html',
        controller: 'hotelItenenaryontroller'
    }).state('holidayItenary', {
        url: '/holidayItenary',
        templateUrl: 'views/holiday/holidayItenenary.html',
        controller: 'holidayItenenaryController'
    }).state('insuranceItenary', {
        url: '/insuranceItenary',
        templateUrl: 'views/insurance/insuranceItenenary.html',
        controller: 'insuranceItenenaryController'
    }).state('user', {
        url: '/user',
        templateUrl: 'views/account/user_home_view.html',
        controller: 'userTripsCtrl'
    }).state('traveller', {
        url: '/traveller',
        templateUrl: 'views/account/traveller/traveller-view.html',
        controller: 'userTravellerCtrl'
    }).state('profile', {
        url: '/profile',
        templateUrl: 'views/account/profile/profile-view.html',
        controller: 'userProfileCtrl'
    }).state('privacy-policy', {
        url: '/privacy-policy',
        templateUrl: 'views/pages/privacy_policy.html'
    }).state('holiday-details', {
        url: '/holiday-details',
        templateUrl: 'views/pages/holiday_details.html'
    }).state('enquiry-holiday', {
        url: '/enquiry-holiday',
        templateUrl: 'views/pages/enquiry_holiday_location.html'
    }).state('contact-us', {
        url: '/contact-us',
        templateUrl: 'views/pages/contact_us.html'
    }).state('terms_conditions', {
        url: '/terms_conditions',
        templateUrl: 'views/pages/terms_conditions.html',

    });


    //$httpProvider.interceptors.push('httpInterceptor');
});

ouaApp.factory('httpInterceptor', function($q, $rootScope, $log) {
    return {
        request: function(config) {
            document.querySelector('.cp-preloader').style.display = "block";
        },
        response: function(response) {
            //            $('.cp-preloader').fadeOut('slow');
            document.querySelector('.cp-preloader').style.display = "none";
        },
        responseError: function(response) {
            document.querySelector('.cp-preloader').style.display = "none";
        }
    };
}).directive('wrapOwlcarousel', function() {
    return {
        restrict: 'E',
        link: function(scope, element, attrs) {
            var options = scope.$eval($(element).attr('data-options'));
            $(element).owlCarousel(options);
        }
    };
});

ouaApp.controller('mainController', function($scope, urlService, $location, $rootScope, $anchorScroll, $location, $uibModal, $http, ServerService, $state) {
    $scope.path = $location.absUrl().split('/')[5];
    /* reuseService.getrequiredData */
    /* $scope.$watch(function(){
    	return reuseService.getrequiredData;
    }, function(newValue, oldValue){
    	console.log("Test Value"+ newValue)
    	}) */
    var booking = JSON.parse(sessionStorage.getItem('bookingDetails'))
    var display = JSON.parse(sessionStorage.getItem('loginName'));
    if (booking == '' || booking == null) {
        $scope.bookingHide1 = false;
    } else {
        $scope.bookingHide1 = booking.bookingHide1;
        $scope.bookingHide = booking.bookingHide;
        if (display != null) {
            $scope.dispName = display.fname;
        }
    }


    $scope.openLogin = function(size, parentSelector) {
        var parentElem = parentSelector ?
            angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;

        $scope.animationEnabled = true;
        var modalInstance = $uibModal.open({
            animation: $scope.animationEnabled,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'login.html',
            controller: 'loginController',
            backdrop: true,
            size: size,
            appendTo: parentElem,
            resolve: {
                items: function() {
                    return $scope.items
                }
            }
        })
    }
    $scope.subscribeMail;
    $scope.subscribe = function() {
        if ($scope.subscribeMail != null && $scope.subscribeMail != "") {

            $http({
                method: 'POST',
                url: ServerService.serverPath + 'rest/newsletter/subscribe?subscriptionId=' + $scope.subscribeMail,
                data: $scope.subscribeMail
            }).then(function(data, status) {
                if (data.data.errorMessage == null) {
                    Lobibox.alert('success', {
                        msg: data.data.message
                    });
                } else {
                    Lobibox.alert('error', {
                        msg: data.data.errorMessage
                    });
                }
            });

        } else {
            Lobibox.alert('error', {
                msg: "Please enter a valid emailId"
            });
        }
    }


    var userBookingdetail = sessionStorage.getItem('userBookingdetails');
    //console.log("userBookingdetails "+ userBookingdetails);
    var disp = JSON.parse(sessionStorage.getItem('loginName'));

    if (userBookingdetail != null) {
        $scope.bookingHide = true;
        $scope.bookingHide1 = true;
        if (disp !== null) {
            $scope.dispName = disp.fname;
        }

    }


    $scope.flightCheck = function() {

        $scope.fCheck = true;
        $scope.hCheck = false;
        $scope.insCheck = false;
        $scope.fhCheck = false;
    }
    $scope.hotelCheck = function() {

        $scope.fCheck = false;
        $scope.hCheck = true;
        $scope.insCheck = false;
        $scope.fhCheck = false;
    }
    $scope.holidayCheck = function() {

        $scope.fCheck = false;
        $scope.hCheck = false;
        $scope.insCheck = false;
        $scope.fhCheck = true;
    }
    $scope.insureCheck = function() {

        $scope.fCheck = false;
        $scope.hCheck = false;
        $scope.insCheck = true;
        $scope.fhCheck = false;
    }
    $rootScope.tabmenuactive = 0;
    $scope.activateTabMenu = function(value) {
        $rootScope.tabmenuactive = Number(value);
        $("html, body").stop().animate({
            scrollTop: $('#about-us').offset().top - 10
        }, '500', 'linear');
    }


    $rootScope.tabmenuactive = 0;
    $scope.activateInnerTabMenu = function(value) {
        $rootScope.tabmenuactive = Number(value);

        $("html, body").stop().animate({
            scrollTop: $('#about-us').offset().top - 10
        });
    }

    /* flightCheck */
    $rootScope.tabSubmenuactive = 0;
    $scope.flightCheck = function(val) {
        $rootScope.tabSubmenuactive = Number(val);
        $("html, body").stop().animate({
            scrollTop: $('#about-us').offset().top - 10
        }, '500', 'linear');
    }
    $scope.hotelCheck = function(val) {
        $rootScope.tabSubmenuactive = Number(val);
        $("html, body").stop().animate({
            scrollTop: $('#about-us').offset().top - 10
        }, '500', 'linear');
    }
    $scope.holidayCheck = function(val) {
        $rootScope.tabSubmenuactive = Number(val);
        $("html, body").stop().animate({
            scrollTop: $('#about-us').offset().top - 10
        }, '500', 'linear');
    }
    $scope.insureCheck = function(val) {
            $rootScope.tabSubmenuactive = Number(val);
            $("html, body").stop().animate({
                scrollTop: $('#about-us').offset().top - 10
            }, '500', 'linear');
        }
        /* end */

    /* init function */
    $scope.initialReload = function() {
            $state.reload();
        }
        /* end */


});

ouaApp.service("ajaxService", function($http, $q) {
    var self = this;
    self.data = null;
    self.ajaxRequest = function(met, url, req, res) {
        var deferred = $q.defer();
        if (self.data !== null) {
            deferred.resolve(self.data);
        } else {
            $http({
                method: met,
                url: url,
                data: req,
                responseType: res
            }).then(function(response) {
                //self.data = response.data;
                deferred.resolve(response.data);
            }).catch(function(response) {
                deferred.reject(response);
            });
        }
        return deferred.promise;
    };
});

ouaApp.service('ConstantService', function() {
    this.currency = 'OMR'
    this.currency1 = 'AED'
});

/* for nationality */
ouaApp.service('NationService', function($http, $rootScope, ServerService) {
    this.nationGet = function() {
        var nationality = new Object();
        nationality.queryString = "";

        $http.post(ServerService.serverPath + 'rest/repo/nationality', nationality, {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
        }).then(function(data) {
            if (data.status == 200) {
                localStorage.setItem('nationality', JSON.stringify(data.data));
                $rootScope.nationalityResult = data.data;

            }

        })
    };
});
/* end */

/* for country */
ouaApp.service('CountryService', function($http, $rootScope, ServerService) {
    this.countryGet = function() {
        var country = new Object();
        country.queryString = "";
        $http({
            method: 'POST',
            url: ServerService.serverPath + 'rest/repo/country',
            data: country
        }).then(function(data, status) {
            if (status = 'success') {
                $rootScope.countryResult = data.data;
            } else {
                alert('error');
            }
        });
    };
});
/* end */

/* renderer html  */
ouaApp.factory('renderHTMLFactory', function() {
    var renderObj = {};
    renderObj.renderHTML = function(html_code) {
        var decoded = angular.element('<textarea />').html(html_code).text();
        return decoded;
    }
    renderObj.setTitle = function(title) {

        var setTitleVal;
        var spltTitle = title.search('Family');
        if (spltTitle !== -1) {
            var spltTitle = title.split('Family');
            setTitleVal = "Family  " + spltTitle[1];
        } else {
            var spltTitle = title.search('-');
            if (spltTitle != -1) {
                var spltTitle = title.split('-');
                setTitleVal = "Individual  " + spltTitle[1];
            } else {
                var spltTitle = title.search('After');
                if (spltTitle != -1) {
                    var spltTitle = title.split('After');
                    setTitleVal = "Individual  " + spltTitle[1];
                } else {
                    setTitleVal = title;
                }
            }
        }
        return setTitleVal;
    }
    return renderObj;
});
/* end */
ouaApp.service('urlService', function($location) {
    //this.relativePath = $location.absUrl().split('/')[6];
    var currentURL = $location.absUrl();
    this.relativePath = currentURL.substring(currentURL.lastIndexOf("/") + 1, currentURL.length);

});

ouaApp.service('loopService', function($scope) {
    var number = {};
    number.getNumber = function(num) {
        return new Array(num);
    }

});

ouaApp.factory('Month', function() {

    var month = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];

    return month;
});

ouaApp.factory('Year', function() {

    var year = ['17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31', '32', '33', '34', '35', '36', '37'];

    return year;
});
ouaApp.service('ServerService', function($location) {
    this.serverPath = $location.absUrl().split('index.html')[0];
});
ouaApp.service('sessionService', function() {
    this.clearRouteData = function(value) {
        var itemList = value.length;
        if (value == 'home' || value == 'sign-out') {
            var loginDetails = null;
            var bookingDetail = null;
            if (sessionStorage.getItem("loginName")) {
                loginDetails = sessionStorage.getItem("loginName");
            }
            if (sessionStorage.getItem("userBookingdetails")) {
                bookingDetail = sessionStorage.getItem("userBookingdetails");
            }
            sessionStorage.clear();
            if (loginDetails != null && bookingDetail != null) {
                sessionStorage.setItem("loginName", loginDetails);
                sessionStorage.setItem("userBookingdetails", bookingDetail);
            }
        }

    }
}).filter('round', function() {
    /* Use this $filter to round Numbers UP, DOWN and to his nearest neighbour.
       You can also use multiples */

    /* Usage Examples:
        - Round Nearest: {{ 4.4 | round }} // result is 4
        - Round Up: {{ 4.4 | round:'':'up' }} // result is 5
        - Round Down: {{ 4.6 | round:'':'down' }} // result is 4
        ** Multiples
        - Round by multiples of 10 {{ 5 | round:10 }} // result is 10
        - Round UP by multiples of 10 {{ 4 | round:10:'up' }} // result is 10
        - Round DOWN by multiples of 10 {{ 6 | round:10:'down' }} // result is 0
    */
    return function(value, mult, dir) {
        dir = dir || 'nearest';
        mult = mult || 1;
        value = !value ? 0 : Number(value);
        if (dir === 'up') {
            return Math.ceil(value / mult) * mult;
        } else if (dir === 'down') {
            return Math.floor(value / mult) * mult;
        } else {
            return Math.round(value / mult) * mult;
        }
    };
});
ouaApp.controller('loginController', function($scope, NationService, $http, ServerService, $timeout, $uibModalInstance, $rootScope) {
    NationService.nationGet();
    $scope.nationality1 = JSON.parse(localStorage.getItem('nationality'))
    $scope.nationality = JSON.stringify($scope.nationality1);
    $scope.signUp = false;
    $scope.signin = false;
    $scope.userCreation = false;
    $scope.signInTag = false;
    $scope.signUpTag = false;
    $scope.submitted = false;
    $scope.userRegFormSubmit = false;
    var displayName = {};
    $scope.userBooking = {}

    /* date picker validation */
    var currentDate = new Date();
    var day = currentDate.getDate();
    var month = currentDate.getMonth() + 1;
    var year = currentDate.getFullYear() - 10;
    if (day < 10) {
        day = '0' + day
    }
    if (month < 10) {
        month = '0' + month
    }
    var today = day + '/' + month + '/' + year
    var maxYear = parseInt(year) - 118;

    $scope.loginOption = {
        singleDatePicker: true,
        showDropdowns: true,
        eventHandlers: {
            'show.daterangepicker': function(ev, picker) {
                $scope.loginOption = {
                    singleDatePicker: true,
                    format: 'DD-MMM-YYYY',
                    startDate: new Date(year, currentDate.getMonth(), currentDate.getDate()),
                    minDate: new Date(maxYear, currentDate.getMonth(), currentDate.getDate()),
                    maxDate: new Date(year, currentDate.getMonth(), currentDate.getDate()),
                    endDate: new Date(maxYear, currentDate.getMonth(), currentDate.getDate())
                }
            }
        }
    }

    $scope.dob = {
        startDate: null,
        endDate: null,
        locale: {
            format: 'YYYY/MM/DD'
        },
        text: ''
    }

    $scope.createNewAccnt = function() {
        $scope.signUp = true;
        $scope.signin = true;
        $scope.signInTag = true;
        $scope.signUpTag = true;
    }

    $scope.showLogin = function() {
        $scope.signUp = false;
        $scope.signin = false;

    }
    $scope.userCreationReqst = {}

    $scope.userRegisterReqst = {}

    /** for signup */
    $scope.userRegisterHeader = function(valid) {
        $scope.submitted = true;

        $scope.userRegisterReqst.triggermail = "N";
        if (valid) {
            $scope.loadingSignUp = true;
            $scope.disableSignUp = true;
            $http.post(ServerService.serverPath + 'rest/user/validate', $scope.userRegisterReqst, {
                headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
            }).then(function(response) {
                $scope.loadingSignUp = false;
                $scope.disableSignUp = false;
                $scope.submitted = false;
                if (response.data.data.userExists == "N") {
                    $scope.travellerUserId = response.data.data.userId;
                    $scope.travellerEmail = response.data.data.loginId;
                    $scope.userCreation = true;
                    $scope.signUp = false;
                    $scope.signUpTag = true;
                    $scope.userCreationReqst.loginid = $scope.userRegisterReqst.loginid;
                } else if (response.data.data.userExists == "Y") {
                    $scope.loginUserName = response.data.data.loginId;
                    $scope.signin = false;
                    $scope.signUp = false;
                    $scope.regstmsg = true;
                    $timeout(function() {
                        $scope.regstmsg = false;
                    }, 5000)

                }
            })

        }
    }

    /** for creating new account */
    $scope.createAccounts = function(valid) {
        $scope.userRegFormSubmit = true;

        var date = new Date($scope.dob.text)
        var dateformat = date.getDate() + '-' + date.getMonth() + '-' + date.getFullYear()
        console.log(JSON.stringify($scope.dob) + " dob " + dateformat);
        $scope.userCreationReqst.oldpassword = null;
        $scope.userCreationReqst.profiledata = {
            'userid': $scope.travellerUserId,
            'profileid': 0,
            'agntid': 0,
            'contactdata': null,
            'miscdata': null
        };

        $scope.userCreationReqst.profiledata.personaldata = {
            'salutation': $scope.salutation,
            'fname': $scope.fname,
            'lname': $scope.lname,
            'dob': dateformat,
            'mobile': $scope.mobile,
            'nationalitycode': $scope.nationalitycode,
            'passportNo': $scope.passportNo
        }
        $scope.userCreationReqst.profiledata.personaldata.sname = null;
        $scope.userCreationReqst.profiledata.personaldata.gender = null;
        $scope.userCreationReqst.profiledata.personaldata.mobcntrycode = '91';

        $scope.userCreationReqst.profiledata.personaldata.email = $scope.userCreationReqst.loginid;
        console.log($scope.userCreationReqst.profiledata.personaldata.salutation);
        if (valid) {
            if ($scope.userCreationReqst.password != $scope.userCreationReqst.confirmpassword) {
                $scope.passwordmatch = "PassWord And Confirm Passowrd Should be Same"
            } else {
                $scope.passwordmatch = ''
                $scope.loadingAccount = true;
                $scope.disableAccount = true;
                $http.post(ServerService.serverPath + 'rest/user/register', $scope.userCreationReqst).then(function(response) {
                    $scope.loadingAccount = false;
                    $scope.disableAccount = false;
                    $scope.userRegFormSubmit = false;
                    if (response.data.status == 'SUCCESS') {
                        Lobibox.alert('success', {
                            msg: response.data.message
                        })
                    } else {
                        Lobibox.alert('error', {
                            msg: response.data.errorMessage
                        })
                    }
                    $timeout(function() {
                        $uibModalInstance.dismiss();
                    }, 5000)
                })

            }
        } else {}

    }

    /** login function**/
    $scope.login = function(loginLoc) {
        if (loginLoc == 'header') {
            var user_name = $('#login_userName').val();
            var user_pass = $('#login_Password').val();

        }
        if (user_name == "" || user_pass == "") {
            $scope.errorMsg = "Please provide both Username and Password to Sign-In."
            $scope.loginError = ""
        } else {
            $scope.errorMsg = ""
            $scope.disableSignIn = true;
            $scope.loadingsignIn = true;
            /* $http.post(ServerService.serverPath+'rest/user/signin', '{ "userName" : "'+user_name+'", "password" : "'+user_pass+'"}').then(function(response){ */
            $http.post(ServerService.serverPath + 'rest/user/signin', '{ "userName" : "' + user_name + '", "password" : "' + user_pass + '"}').then(function(response) {
                $scope.disableSignIn = false;
                $scope.loadingsignIn = false;
                console.log(response)
                if (response.data.status == "SUCCESS") {
                    $uibModalInstance.close();
                    $scope.$parent.bookingHide = true;
                    $scope.$parent.bookingHide1 = true;
                    $scope.userBooking.bookingHide = true;
                    $scope.userBooking.bookingHide1 = true;
                    var userBookingdetails = $scope.userBooking;
                    sessionStorage.setItem('userBookingdetails', JSON.stringify(userBookingdetails));
                    displayName.fname = response.data.data.profileData.personaldata.fname;
                    displayName.email = response.data.data.profileData.personaldata.email;
                    displayName.mobile = response.data.data.profileData.personaldata.mobile;
                    sessionStorage.setItem('loginName', JSON.stringify(displayName))
                    $rootScope.dispName = displayName.fname;
                    window.location.reload()
                } else if (response.data.status == "FAILURE") {
                    $scope.loginError = "Please register with us, before login."
                }
            })
        }

    }

    /* forgotPassDiv   */
    $scope.forgotPassDiv = function() {
        $scope.forForgot1 = true;
        $scope.forForgot = true;
        $scope.hideLoginErr = true;

    }

    $scope.returns = function() {
        $scope.forForgot1 = false;
        $scope.forForgot = false;
    }

    $scope.forgotpassword = function() {

            var forgotUser = $('#login_userName1').val();
            if (forgotUser == '' || forgotUser == null) {
                $scope.submitforgotuser = true;
            } else {
                $scope.submitforgotuser = false;
                var validatePswdReqst = { "loginid": $scope.loginUserName, "triggermail": "N" };
                var forgotPswdReqst = { "loginid": $scope.loginUserName }

                $http.post(ServerService.serverPath + 'rest/user/validate', validatePswdReqst).then(function(response) {
                    console.log("validatePswdReqst " + response);
                    if (response.data.data.userExists == 'Y' && response.data.data.userRegistered == 'Y') {
                        $http.post(ServerService.serverPath + 'rest/password/forgot', forgotPswdReqst).then(function(response) {
                            if (response.data.status == "SUCCESS") {
                                Lobibox.alert('success', {
                                    msg: response.data.message
                                });
                            } else {
                                Lobibox.alert('error', {
                                    msg: response.data.errorMessage
                                });
                            }

                            $uibModalInstance.dismiss();
                        })
                    } else if (response.data.data.userExists == 'Y' && response.data.data.userRegistered == 'N') {
                        $scope.travellerUserId = response.data.data.userId;
                        $scope.travellerEmail = response.data.data.loginId;
                        $scope.forgetRegMsg = true;
                        $scope.userCreation = true;
                        $scope.signUpTag = true;
                        $scope.signInTag = true;
                        $scope.forForgot = false;
                        $scope.userCreationReqst.loginid = $scope.travellerEmail;
                        $timeout(function() {
                            $scope.forgetRegMsg = false;
                        }, 4000)
                    } else if (response.data.data.userExists == 'N' && response.data.data.userRegistered == 'N') {
                        $scope.signUp = true;
                        $scope.signUpTag = true;
                        $scope.newUserRegMsg = true;
                        $scope.forForgot = false;
                        $scope.signInTag = true;
                        $timeout(function() {
                            $scope.newUserRegMsg = false;
                        }, 5000)
                    }
                })
            }

        }
        /* end */

    /* modelDismiss function */
    $scope.modelDismiss = function() {
            $uibModalInstance.close();
        }
        /* end */

});

/* unique http service */
ouaApp.service('httpService', function($http, ServerService) {
        var responseData;

        var httpCall = {
            async: function(url, data) {
                if (!responseData) {
                    // $http returns a responseData, which has a then function, which also returns a responseData
                    responseData = $http.post(ServerService.serverPath + url, data).then(function(response) {
                        // The then function here is an opportunity to modify the response
                        // console.log(response);
                        // The return value gets picked up by the then in the controller.
                        return response.data;
                    });
                }
                // Return the responseData to the controller
                return responseData;
            }
        }
        return httpCall;
    })
    /* end */

/* for passing values from one controller to another */
ouaApp.service('reuseService', function() {
    var requiredData = {}
    return {
        getrequiredData: function() {
            return requiredData;
        },
        setrequiredData: function(values) {
            requiredData = values;
        }
    }

})