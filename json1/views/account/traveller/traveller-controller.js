/** @author Gayathri  */
ouaApp.controller('userTravellerCtrl', function($scope, $location, $interval, $http, $timeout,$rootScope,ServerService, ajaxService,$window,NationService, CountryService, $uibModal){
	$scope.data = '';
	$('body').attr('id','top');
	$('#top').addClass('thebg');
	NationService.nationGet();
	CountryService.countryGet();
	
	
	/* $scope.init = {'page':1, 'count':10	}; */
	$scope.init = {'page':1, 'count':10	};
	$scope.userProfileReq = {};
	$scope.noRecords = false;
	$scope.noRecords1 = true;
	$scope.travelClick = false;
	$scope.travelReq = function(page,count) {
		$scope.userProfileReq.salutation = null;
		$scope.userProfileReq.fname = null;
		$scope.userProfileReq.lname = null;
		$scope.userProfileReq.dob = null;
		$scope.userProfileReq.nationalitycode = null;
		$scope.userProfileReq.mobile = null;
		$scope.userProfileReq.email = null;
		$scope.userProfileReq.passportno = null;
		$scope.userProfileReq.country = null;
		$scope.userProfileReq.passengertype = null;
		$scope.userProfileReq.operation = "TRAVELLER";
		$scope.userProfileReq.userid = 0;
		$scope.userProfileReq.createby = 0;
		$scope.userProfileReq.fromindex = 0;
		$scope.userProfileReq.toindex = 0;
		$scope.userProfileReq.page = page;
		$scope.userProfileReq.count = count;
	};
	//var params = {'page': 10, 'count': 10}
	
	$scope.getTraveller = function () {
	$scope.loading = true;
		if($scope.travelClick==false){
			$scope.userProfileReq = {};
		}
		var params = "page=1&count=10"
	var paramsObj = {'page': 1, 'count': 10, 'pagination': true, 'sortBy': undefined, 'sortOrder': undefined }
	
		$scope.travelReq(paramsObj.page,paramsObj.count);
		$scope.page = paramsObj.page;
		return $http.post(ServerService.serverPath+'rest/search/profile',$scope.userProfileReq).then(function (response) {
			$scope.loading = false;
			if(response.data.status=='SUCCESS' && response.data.data!='null') {
				$scope.travellerInfoData = response.data.data.travellerBeanList;
				$scope.totTrvCount = response.data.data.totalCount;
				$location.path('/traveller');
				return {"header": [],"rows": $scope.travellerInfoData,"pagination": {"count": 10,"page": $scope.page,"pages": $scope.totTrvCount/10,"size": $scope.totTrvCount}
				};
				} else {
				$scope.travellerInfoData = [];
				$scope.totTrvCount = 0;
				$scope.noRecords = true;
				Lobibox.alert('error',
				{
					msg: response.data.errorMessage
				});
				$location.path('/traveller');
				return {"header": [],"rows": $scope.travellerInfoData,"pagination": {"count": 10,"page": $scope.page,"pages": $scope.totTrvCount/10,"size": $scope.totTrvCount}
				};
			}
		});	
	};
	
	$scope.trvlModify = function(pagination) {
		pagination.count = 10;
		pagination.page = $scope.page;
		pagination.pages = Math.ceil($scope.totTrvCount/10);
		pagination.size = $scope.totTrvCount;
	};
	
	/* for update and edit traveller function */
	var travellerOpts;
	$scope.editTraveller = function(size, parentElem, travellerObj, addressObjProfile){
	if(travellerObj != null){
	$rootScope.editMode = true;
		travellerOpts = JSON.stringify(travellerObj)
	sessionStorage.setItem('travellerOpts', travellerOpts);
	} else {
	$rootScope.editMode = false;
		travellerOpts = travellerObj +','+ addressObjProfile
	sessionStorage.setItem('travellerOpts', travellerOpts);
		}
	
		 var modalInstance = $uibModal.open({
			animation: $scope.animationEnabled,
			ariaLabelledBy : 'modal-title',
			ariaDescribedBy : 'modal-body',
			templateUrl : 'travellerPage.html',
			controller : 'travellerDetailController',
			backdrop: true,
			size : size,
			appendTo : parentElem,
			resolve: {
				items: function(){
					return $scope.items
				}
			}
		})
		}
	
});
ouaApp.controller('travellerDetailController', function($scope, NationService, CountryService, $http, ServerService, $rootScope, $timeout, $uibModalInstance){
	$scope.salutationData = [{label: 'MR', value: 'MR'}, {label: 'MS', value: 'MS'}, {label: 'MRS', value: 'MRS'}, {label: 'MSTR', value: 'MSTR'}, {label: 'MISS', value: 'MISS'}];
	
	$scope.emails = [];
	$scope.freqflyerlist = [];
	$scope.phonenumbers = [];
	var travellerOpts = sessionStorage.getItem('travellerOpts');
	if($rootScope.editMode == true){
		var travellerObj = JSON.parse(travellerOpts)
		$scope.travellerObj = travellerObj;
		$scope.dob1 = travellerObj.personaldata.dob;
	} else {
	var travellerObj = travellerOpts.split(',')
	$scope.travellerObj=travellerObj[0];
		$scope.addressObjProfile=travellerObj[1];
	}
	/* var travellerObj = travellerOpts.split(',')
	$scope.travellerObj=travellerObj[0];
		$scope.addressObjProfile=travellerObj[1]; */
		$scope.data = '';
	$('body').attr('id','top');
	$('#top').addClass('thebg');
	NationService.nationGet();
	CountryService.countryGet();
		if($scope.travellerObj != "null"){
			$scope.operation = 'update';
			editPassenger(travellerObj);
			$scope.travellerTitle = "Edit";
			$scope.travellerButton = "Update";
			if($scope.travellerObj.miscdata.freqflyerlist!=null) {
				$scope.freqflyerlist = $scope.travellerObj.miscdata.freqflyerlist;
				} else {
				$scope.freqflyerlist = [];
			}
		} else {
			$scope.travellerObj = {};
			$scope.travellerObj.personaldata = {};
			$scope.travellerObj.personaldata.salutation = $scope.salutationData[0].label;
			$scope.operation = 'insert';
			$scope.travellerTitle = "Add New";
			$scope.travellerButton = "Save";
			$scope.emails = [];
			$scope.freqflyerlist = [];
			$scope.phonenumbers = [];
			}
		 /* date picker validation */
			var currentDate = new Date();
			var day = currentDate.getDate();
			var month = currentDate.getMonth() +1;
			var year = currentDate.getFullYear();
			if(day< 10){
				day = '0'+day
				}
				if(month< 10){
					month = '0'+month
					}
				var today = day+'/'+month+'/'+year	
				var maxYear = parseInt(year)-118;
			
			$scope.loginOption = {
	 singleDatePicker: true,
    showDropdowns: true,
   eventHandlers: {
			'show.daterangepicker': function(ev, picker){
				$scope.loginOption = {
					singleDatePicker: true,
					format: 'DD-MMM-YYYY',
					startDate: new Date(year, currentDate.getMonth(), currentDate.getDate()),
					minDate: new Date(maxYear, currentDate.getMonth(), currentDate.getDate()),
					maxDate: new Date(year, currentDate.getMonth(), currentDate.getDate()),
					 endDate: new Date(maxYear, currentDate.getMonth(), currentDate.getDate()) 
				}
				}
			}
	 }
	 
	 
	
	 
	 
	 
	 var fi;
	 /* autoAirlines autocomplete */
	 $scope.airlines = [];
	 $scope.airlineCode = [];
	 $scope.autoAirlines = function(value){
		 if(value != null && value.length == 1){
			 $http.post(ServerService.serverPath+'rest/repo/airline', '{ "queryString" : "'+value+'"}').then(function(response){
				 var airlist = response.data
				  if(response.data!='null'&&response.data.length>0){
  for(var i=0; i<response.data.length; i++){
     var airfrom= {
                     Title:  response.data[i].displayname +' - '+'('+response.data[i].code+')'
                      
     }
	 var aircode = {
		 Code: response.data[i].code
	 }
     $scope.airlines.push(airfrom);
	 $scope.airlineCode.push(aircode)
     
    }
    $scope.airlines.sort();
	$scope.airlineCode.sort();
  }
				 })
			 }
		 }
		
		$scope.airCodeChange = function(value){
			$scope.autoAirlines(value)
			}
	// Add FrequentFlyer Details
	$scope.addFrequentFlyerProfile = function () {	
		var isValid = true;
		var frequentflyerObj = null;
		var mode = 'insert';
		if ($scope.ffChildObjEdit == null) {
			frequentflyerObj = new Object();		
			} else {
			frequentflyerObj = $scope.ffChildObjEdit;
			mode = 'update';	
		}		
		//var ffairlinename = $('#profileAirlineName').val();
		
		//var ffnumber = $('#profileFlyerNo').val();
		
		//var ffairline = $('#profile_AirlineCode').val();		
		
		if ($scope.airlineName == "" || $scope.airlineName == null) {
			Lobibox.notify('error', {size: 'mini', delay : 1500, msg: "Please Choose Airline"});
			isValid = false;
			} else if ($scope.airCodes == "" || $scope.airCodes == null) {
			Lobibox.notify('error', {size: 'mini', delay : 1500, msg: "Please Enter Flyer No"});
			isValid = false;	
			} else {
			var ffairlinename = $scope.airlineName.Title;
			var ffnumber = $scope.airCodes.Code;
			$scope.profile_AirlineCode = ffairlinename.split('(')[1].substring(0,2);	
		var ffairline = $scope.profile_AirlineCode
			if (mode == 'insert') {
				if (!checkFFDtlDuplicate(ffairline)) {
					isValid = false;
					Lobibox.notify('warning', {size: 'mini', delay : 1500, msg: "Please Enter Flyer No"});				
				} 				
			}
			if (isValid == true) {
				frequentflyerObj.ffairlinename = ffairlinename;
				frequentflyerObj.ffairline = ffairline;
				frequentflyerObj.ffnumber = ffnumber;					
				clearFFFieldValues ();				
				if (mode == 'insert') {
					$scope.freqflyerlist.push(frequentflyerObj);
					//$scope.$apply();
					console.log($scope.freqflyerlist);
					Lobibox.notify('success', {size: 'mini', delay : 1500, msg: "Airline Added Successfully"});	
					} else {
					Lobibox.notify('success', {size: 'mini', delay : 1500, msg: "Airline Updated Successfully"});	
					$scope.ffChildObjEdit = null;
				}
			}		
		}		 			
	};
	
	
	// Add Communication Details
	$scope.addCommunicationProfile = function () {
		var isValid = true;
		var mode = 'insert';
		var phonenumberObj=null;
		var emailObj=null;
		var commtypecode = $('#profile_ContactType').val();
		var commvalue = $('#profile_ContactValue').val();
		
		if (commtypecode == "") {
			Lobibox.notify('error', {size: 'mini', delay : 1500, msg: "Please Enter The Contact Type"});		
			isValid = false;	
			} else if (commvalue == ""){
			Lobibox.notify('error', {size: 'mini', delay : 1500, msg: "Please Enter The Value"});	
			isValid = false;
			} else {		
			if (commtypecode == "EO" || commtypecode == "EP") {
				if (!validEmail(commvalue)) {
					Lobibox.notify('error', {size: 'mini', delay : 1500, msg: "Please Enter Correct email"});
					isValid = false;
					} else {						
					if ($scope.emailObjEdit == null) {
						if (!validateDuplicateEmail(commvalue)) {
							Lobibox.notify('warning', {size: 'mini', delay : 1500, msg: "Email Is Already Added"});
							isValid = false;
							} else {
							emailObj = new Object();					
							emailObj.commtypecode = commtypecode;
							emailObj.commvalue = commvalue;	
							emailObj.isPref = "N";
						}							
						} else {				
						mode = 'update';
						emailObj = $scope.emailObjEdit;						
						emailObj.commtypecode = commtypecode;
						emailObj.commvalue = commvalue;						
					}					
				}				
				if (isValid == true) {					
					clrCommunicationFlds();
					$scope.emailObjEdit = null;
					if (mode == 'insert') {
						$scope.emails.push(emailObj);
						Lobibox.notify('success', {size: 'mini', delay : 1500, msg: "Email Added Successfully"});
						} else {
						Lobibox.notify('success', {size: 'mini', delay : 1500, msg: "Email Updated Successfully"});
						
					}
				}					
				} else  {	
				if (!isNumber(commvalue)) {
					Lobibox.notify('error', {size: 'mini', delay : 1500, msg: "Please Enter Correct number"});
					isValid = false;
					} else {				
					if ($scope.phoneNumObjEdit == null) {
						if (validateDuplicateContact(commvalue) == false) {
							Lobibox.notify('warning', {size: 'mini', delay : 1500, msg: "Number Is Already Added"});					
							isValid = false;
							} else {
							phonenumberObj = new Object();									
							phonenumberObj.commtypecode = commtypecode;
							phonenumberObj.commvalue = commvalue;				 			
							phonenumberObj.isPref = "N";
						}					
						} else {								
						phonenumberObj = $scope.phoneNumObjEdit;
						mode = 'update';				
						phonenumberObj.commtypecode = commtypecode;
						phonenumberObj.commvalue = commvalue;				 			
					}								
					if (isValid == true) {					
						clrCommunicationFlds();					
						if (mode == 'insert') {
							$scope.phonenumbers.push(phonenumberObj);
							Lobibox.notify('success', {size: 'mini', delay : 1500, msg: "PhoneNumber Added Successfully"});
							} else {
							Lobibox.notify('success', {size: 'mini', delay : 1500, msg: "PhoneNumber Updated Successfully"});
							$scope.phoneNumObjEdit = null;
						}
					}
				}					
			}		
		}		
	};
	
	$scope.removePhoneNumbers = function (phonenumber) {		
		$scope.phonenumbers.pop(phonenumber);
	};
	
	$scope.removeEmails = function (email) {
		$scope.emails.pop(email);
	};
	
	$scope.removeFrequentFlyer = function (frequentflyer) {
		$scope.freqflyerlist.pop(frequentflyer);
	};
	function checkFFDtlDuplicate(currValue) {
		var isValid = true;		
		if($scope.freqflyerlist.length != 0) {		
			for(var i = 0, size = $scope.freqflyerlist.length; i < size ; i++) {
				var value = $scope.freqflyerlist[i].ffairline;
				if (currValue == value) {
					isValid = false;
				} 	 
			}
		}
		return isValid;
	}
	function clearFFFieldValues () {
		$('#profileFlyerNo').val("");
		$('#profileAirlineName').val("");
		$('#profile_AirlineCode').val("");
		$('#addFrequentFlyerProfile').show();
	}
	// Check The Variable Is A Email
	
	function validEmail(v) {	    
		var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
		return pattern.test(v);
	}
	
	// Check Whether Email Is Already Added
	
	function validateDuplicateEmail(currValue) {		
		var isValid = true;		
		if($scope.emails.length != 0) {		
			for(var i = 0, size = $scope.emails.length; i < size ; i++) {
				var value = $scope.emails[i].commvalue;
				if (currValue == value) {
					isValid = false;
				} 	 
			}
		}
		return isValid;
	}
	
	// Clear Communication UI Fields
	
	function clrCommunicationFlds() {
		$('#profile_ContactType').val("");
		$('#profile_ContactValue').val("");
		$('#addCommunicationProfile').show();
		$('#updateCommunication').hide();
	}
	
	// Check The Variable Is A Number
	
	function isNumber(n) {
		return !isNaN(parseFloat(n)) && isFinite(n);
	}
	// Check Whether The Phone Number Is Already Added
	
	function validateDuplicateContact(currValue) {
		var isValid = true;		
		if($scope.phonenumbers.length != 0) {		
			for(var i = 0, size = $scope.phonenumbers.length; i < size ; i++) {
				var value = $scope.phonenumbers[i].commvalue;
				if (currValue == value) {
					isValid = false;
				} 	 
			}
		}
		return isValid;
	}
			// WRITE THE VALIDATION SCRIPT IN THE HEAD TAG.
	function isNumber(evt) {
		var iKeyCode = (evt.which) ? evt.which : evt.keyCode
		if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
        return false;
		
		return true;
	}
	
	
	
	// Save Passenger Details
	
	var $validator = jQuery("#travellerForm").validate({
		highlight: function(element) {
			jQuery(element).closest('.form-group').removeClass('has-success').addClass('has-error');
		},
		success: function(element) {
			jQuery(element).closest('.form-group').removeClass('has-error');
		}
	});
	
	$scope.savePassenger = function () {
		var $valid = jQuery('#travellerForm').valid();
		if(!$valid) {
			return false;
			} else {
			var date = new Date($scope.dob);
			var month = date.getMonth() + 1;
					var dateformat = date.getDate() +'-'+month +'-'+ date.getFullYear() 
			$scope.travellerObj.personaldata.dob = dateformat
			console.log($scope.operation);
			$scope.loading = true;
			if ($scope.operation != 'update') {
				var contactdata = new Object();
				var miscdata = new Object();
				$scope.travellerObj.contactdata = contactdata;
				$scope.travellerObj.miscdata = miscdata;
				$scope.travellerObj.profileid = 0;
				$scope.travellerObj.agntid = 0;
				addNewPassenger($scope.travellerObj);
				} else {
				addNewPassenger($scope.travellerObj);
			}
		}
	};
	
	// Edit Passenger Details
	
	function editPassenger (travellerObj) {		
		
		$scope.travellerObj = travellerObj;
		
		if ($scope.travellerObj.contactdata.address != null) {	
			$scope.addressObjProfile = travellerObj.contactdata.address[0];
		}
		
		if ($scope.travellerObj.contactdata.phonenumbers != null) {
			$scope.phonenumbers = $scope.travellerObj.contactdata.phonenumbers;
		}
		if ($scope.travellerObj.contactdata.emails != null) {
			$scope.emails = $scope.travellerObj.contactdata.emails;
		}
		if ($scope.travellerObj.miscdata.mealpref != null) {
			$('#profileMealPreference').val($scope.travellerObj.miscdata.mealpref[0].prefkey);
		}
		if ($scope.travellerObj.miscdata.seatpref != null) {
			$('#profileSeatPreference').val($scope.travellerObj.miscdata.seatpref[0].prefkey);				
		}
		$scope.freqflyerlist = $scope.travellerObj.miscdata.freqflyerlist;
		
		if (travellerObj.miscdata.documentdata  != null) {
			$scope.documentdata = $scope.travellerObj.miscdata.documentdata;
		}
	};
	
	function addNewPassenger (travellerObj) {
		var address = [];
		/* if($scope.addressObjProfile== null){
			$scope.travellerObj.contactdata.address = address;
			}else{
			$scope.travellerObj.contactdata.address = address;
			$scope.travellerObj.contactdata.address.push($scope.addressObjProfile);
		} */
		if($scope.addressObjProfile != null){
			$scope.travellerObj.contactdata.address = address;
			//$scope.travellerObj.contactdata.address.push($scope.addressObjProfile);
			}
		$scope.travellerObj.contactdata.emails = $scope.emails;
		$scope.travellerObj.contactdata.phonenumbers = $scope.phonenumbers;
		if ($scope.travellerObj.miscdata.mealpref == null) {
			var mealpref = [];
			var mealPreference = new Object();					
			$scope.travellerObj.miscdata.mealpref = mealpref;
			$scope.travellerObj.miscdata.mealpref.push(mealPreference);
		}
		if ($scope.travellerObj.miscdata.seatpref == null) {
			var seatpref = [];
			var seatPreference = new Object();		
			$scope.travellerObj.miscdata.seatpref = seatpref;
			$scope.travellerObj.miscdata.seatpref.push(seatPreference);
		}
		$scope.travellerObj.miscdata.mealpref[0].prefkey = $('#profileMealPreference').val();				
		$scope.travellerObj.miscdata.seatpref[0].prefkey = $('#profileSeatPreference').val();	
		$scope.travellerObj.miscdata.freqflyerlist = $scope.freqflyerlist;
		console.log($scope.travellerObj);
		$http.put(ServerService.serverPath+'rest/manipulate/profile', $scope.travellerObj, {
			headers: {'Content-type': 'application/x-www-form-urlencoded; charset=UTF-8'}
		}).then(function(response){
			$scope.loading = true;
			if(response.data.status == "SUCCESS"){
				
				$timeout(function(){
					
					location.reload(true);
					
				},500);
					Lobibox.notify('success', {
						size: 'mini',
						msg: response.data.message
					});
			} else {
			Lobibox.notify('error', {
					size: 'mini',
					msg: response.data.errorMessage
				});
			}
			$uibModalInstance.dismiss();
			})
		
	}
})