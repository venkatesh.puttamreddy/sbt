/** @author Gayathri  */
ouaApp.controller( 'userProfileCtrl' , function( $scope, $location, $interval, $http, $timeout,$rootScope, ServerService, ajaxService,$window,NationService,CountryService) {
	$scope.data = '';
	$('body').attr('id','top');
	$('#top').addClass('thebg');
	NationService.nationGet();
	CountryService.countryGet();
	$scope.salutationData = [{label: 'MR', value: 'MR'}, {label: 'MS', value: 'MS'}, {label: 'MRS', value: 'MRS'}];
	var minYear; var maxYear; var minDate; var maxDate ;
	var userProfileReq = {"operation":"PROFILE","salutation":null,"fname":null,"lname":null,"dob":null,"nationalitycode":null,"mobile":null,"email":null,"passportno":null,"country":null,"passengertype":null,"page":1,"count":10,"fromindex":0,"toindex":0,"userid":0,"createby":0};
	ajaxService.ajaxRequest('POST',ServerService.serverPath+'rest/search/profile',userProfileReq,'json')
	.then(function(data,status) {
		if(data.status=="SUCCESS"){
			var jsondata=data.data;
			sessionStorage.setItem("profileJsondata",JSON.stringify(jsondata));
			$scope.profileJsondata = sessionStorage.getItem("profileJsondata");//Retrieve the stored profile data
			$scope.profileInfoData = JSON.parse($scope.profileJsondata).travellerBeanList[0];
			$scope.dob1 = $scope.profileInfoData.personaldata.dob;
			var setDates = $scope.dob1.split('-');
			 minDate = new Date(setDates[1]+'/'+setDates[0]+'/'+setDates[2])
			  maxDate = new Date();
			 minYear = minDate.getFullYear()
	// maxYear = parseInt(minDate.getFullYear())-110
	 maxYear = parseInt(1900)
			$location.path('/profile');
			} else {
			Lobibox.alert('error',
			{
				msg: data.errorMessage
			});
			$location.path('/profile');
		}
	});
	
	/* date validation */
	
	
	
	
	
	$scope.loginOption = {
	 singleDatePicker: true,
    showDropdowns: true,
   eventHandlers: {
			'show.daterangepicker': function(ev, picker){
				$scope.loginOption = {
					singleDatePicker: true,
					format: 'DD-MMM-YYYY',
					startDate: new Date(minYear, minDate.getMonth(), minDate.getDate()),
					minDate: new Date(maxYear, minDate.getMonth(), minDate.getDate()),
					 maxDate: new Date(maxDate.getFullYear(), minDate.getMonth(), minDate.getDate()), 
					 endDate: new Date(maxYear, minDate.getMonth(), minDate.getDate()) 
				}
				}
			}
	 }
	
	if(sessionStorage.getItem("profileJsondata")!=undefined || sessionStorage.getItem("profileJsondata")!=null){
		$scope.profileJsondata = sessionStorage.getItem("profileJsondata");//Retrieve the stored profile data
		$scope.profileInfoData = JSON.parse($scope.profileJsondata).travellerBeanList[0];
		// Global Variables
		$scope.addressObj;
		$scope.phonenumbers = [];
		$scope.emails = [];
		$scope.freqflyerlist = [];
		$scope.ffChildObjEdit;
		$scope.phoneNumObjEdit;
		$scope.emailObjEdit;
		
		if ($scope.profileInfoData.contactdata.address != null) {	
			$scope.addressObj = $scope.profileInfoData.contactdata.address[0];
		}
		if ($scope.profileInfoData.contactdata.emails != null) {	
			$scope.emails = $scope.profileInfoData.contactdata.emails;
		}
		if ($scope.profileInfoData.contactdata.phonenumbers != null) {	
			$scope.phonenumbers = $scope.profileInfoData.contactdata.phonenumbers;
		}
		if ($scope.profileInfoData.miscdata.mealpref != null) {
			$('#profile_mealPreference').val($scope.profileInfoData.miscdata.mealpref[0].prefkey);
		}
		if ($scope.profileInfoData.miscdata.seatpref != null) {
			$('#profile_seatPreference').val($scope.profileInfoData.miscdata.seatpref[0].prefkey);				
		}
		if ($scope.profileInfoData.miscdata.freqflyerlist != null) {
			$scope.freqflyerlist = $scope.profileInfoData.miscdata.freqflyerlist;				
		}
		
		// Profile Update code starts
		
		var fi;
		/* autoAirlines autocomplete */
	 $scope.airlines = [];
	 $scope.airlineCode = [];
	 $scope.autoAirlines = function(value){
		 if(value != null && value.length == 1){
			 $http.post(ServerService.serverPath+'rest/repo/airline', '{ "queryString" : "'+value+'"}').then(function(response){
				 var airlist = response.data
				  if(response.data!='null'&&response.data.length>0){
  for(var i=0; i<response.data.length; i++){
     var airfrom= {
                     Title:  response.data[i].displayname +' - '+'('+response.data[i].code+')'
                      
     }
	 var aircode = {
		 Code: response.data[i].code
	 }
     $scope.airlines.push(airfrom);
	 $scope.airlineCode.push(aircode)
     
    }
    $scope.airlines.sort();
	$scope.airlineCode.sort();
  }
				 })
			 }
		 }
		
		$scope.airCodeChange = function(value){
			$scope.autoAirlines(value)
			}
		/* $( "#profile_airline" ).autocomplete({
			source: function( request, response ) {
				ajaxService.ajaxRequest('POST',ServerService.serverPath+'rest/repo/airline','{ "queryString" : "'+request.term+'"}','json')
				.then(function(data,status) {
					if(data!=null && data!='') {
						fi=0;
						response( data );
						}else{
						$( "#profile_airline" ).val('');
						$('.ui-autocomplete').hide();
					}
				});
			},
			focus: function( event, ui ) {
				$( "#profile_airline" ).val( ui.item.displayname + " - " +"("+ui.item.code+")" );
				return false;
			},
			change : function(event,ui) {
				$timeout(function(){
					jQuery('.form-group').removeClass('has-error');
				},10);
			},
			
			minLength: 2,
			select: function( event, ui ) {
				$( "#profile_airline" ).val( ui.item.displayname +" - "+ "("+ui.item.code+")" );
				$( "#profileAirlineCode" ).val( ui.item.code );
				return false;
			},
			open: function() {
				$( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
			},
			close: function(event, ui) {
				$( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
			}
			}).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
			if(fi==0) {
				fi++;
				return $( "<li>" ).append( "<a class='ui-state-focus'>" + item.displayname +"-" +"("+item.code+")</a>" ).appendTo( ul );
				} else {
				return $( "<li>" ).append( "<a>" + item.displayname + "-" + "("+item.code+")</a>" ).appendTo( ul );
			}
		}; */ 
		
		// Add Communication Details
		
		$scope.addCommunication = function () {
			var isValid = true;
			var mode = 'insert';
			var phonenumberObj=null;
			var emailObj=null;
			var commtypecode = $('#profileContactType').val();
			var commvalue = $('#profileContactValue').val();
			
			if (commtypecode == "") {
				Lobibox.notify('error', {size: 'mini', delay : 1500, msg: "Please Enter The Contact Type"});		
				isValid = false;	
				} else if (commvalue == ""){
				Lobibox.notify('error', {size: 'mini', delay : 1500, msg: "Please Enter The Value"});	
				isValid = false;
				} else {		
				if (commtypecode == "EO" || commtypecode == "EP") {
					if (!validEmail(commvalue)) {
						Lobibox.notify('error', {size: 'mini', delay : 1500, msg: "Please Enter Correct email"});
						isValid = false;
						} else {						
						if ($scope.emailObjEdit == null) {
							if (!validateDuplicateEmail(commvalue)) {
								Lobibox.notify('warning', {size: 'mini', delay : 1500, msg: "Email Is Already Added"});
								isValid = false;
								} else {
								emailObj = new Object();					
								emailObj.commtypecode = commtypecode;
								emailObj.commvalue = commvalue;	
								emailObj.isPref = "N";
							}							
							} else {				
							mode = 'update';
							emailObj = $scope.emailObjEdit;						
							emailObj.commtypecode = commtypecode;
							emailObj.commvalue = commvalue;						
						}					
					}				
					if (isValid == true) {					
						clrCommunicationFlds();
						$scope.emailObjEdit = null;
						if (mode == 'insert') {
							$scope.emails.push(emailObj);
							Lobibox.notify('success', {size: 'mini', delay : 1500, msg: "Email Added Successfully"});
							} else {
							Lobibox.notify('success', {size: 'mini', delay : 1500, msg: "Email Updated Successfully"});
							
						}
					}					
					} else  {	
					if (!isNumber(commvalue)) {
						Lobibox.notify('error', {size: 'mini', delay : 1500, msg: "Please Enter Correct number"});
						isValid = false;
						} else {				
						if ($scope.phoneNumObjEdit == null) {
							if (validateDuplicateContact(commvalue) == false) {
								Lobibox.notify('warning', {size: 'mini', delay : 1500, msg: "Number Is Already Added"});					
								isValid = false;
								} else {
								phonenumberObj = new Object();									
								phonenumberObj.commtypecode = commtypecode;
								phonenumberObj.commvalue = commvalue;				 			
								phonenumberObj.isPref = "N";
							}					
							} else {								
							phonenumberObj = $scope.phoneNumObjEdit;
							mode = 'update';				
							phonenumberObj.commtypecode = commtypecode;
							phonenumberObj.commvalue = commvalue;				 			
						}								
						if (isValid == true) {					
							clrCommunicationFlds();					
							if (mode == 'insert') {
								$scope.phonenumbers.push(phonenumberObj);
								Lobibox.notify('success', {size: 'mini', delay : 1500, msg: "PhoneNumber Added Successfully"});
								} else {
								Lobibox.notify('success', {size: 'mini', delay : 1500, msg: "PhoneNumber Updated Successfully"});
								$scope.phoneNumObjEdit = null;
							}
						}
					}					
				}		
			}		
		};
		
		// Clear Communication UI Fields
		
		function clrCommunicationFlds() {
			$('#profileContactType').val("");
			$('#profileContactValue').val("");
			$('#addCommunication').show();
			$('#updateCommunication').hide();
		}
		
		$scope.removePhoneNumbers = function (phonenumber) {		
			$scope.phonenumbers.pop(phonenumber);
		};
		
		$scope.removeEmails = function (email) {
			$scope.emails.pop(email);
		};
		//Frequent flyer code starts
		
		$scope.addFrequentFlyer = function () {	
			var isValid = true;
			var frequentflyerObj = null;
			var mode = 'insert';
			if ($scope.ffChildObjEdit == null) {
				frequentflyerObj = new Object();		
				} else {
				frequentflyerObj = $scope.ffChildObjEdit;
				mode = 'update';	
			}		
			//var ffairlinename = $('#profile_airline').val();
			
			//var ffnumber = $('#profile_flyerNo').val();
			
			//var ffairline = $('#profileAirlineCode').val();		
			
			if ($scope.airlineName == "" || $scope.airlineName == null) {
				Lobibox.notify('error', {size: 'mini', delay : 1500, msg: "Please Choose Airline"});
				isValid = false;
				} else if ($scope.airCodes == "" || $scope.airCodes == null) {
				Lobibox.notify('error', {size: 'mini', delay : 1500, msg: "Please Enter Flyer No"});
				isValid = false;	
				} else {
				var ffairlinename = $scope.airlineName.Title;
				var ffnumber = $scope.airCodes.Code;
				$scope.profile_AirlineCode = ffairlinename.split('(')[1].substring(0,2)
				var ffairline = $scope.profile_AirlineCode;	
				if (mode == 'insert') {
					if (!checkFFDtlDuplicate(ffairline)) {				
						isValid = false;
						Lobibox.notify('warning', {size: 'mini', delay : 1500, msg: "Please Enter Flyer No"});				
					} 				
				} 
				if (isValid == true) {
					frequentflyerObj.ffairlinename = ffairlinename;
					frequentflyerObj.ffairline = ffairline;
					frequentflyerObj.ffnumber = ffnumber;					
					clearFFFieldValues ();				
					if (mode == 'insert') {
						$scope.freqflyerlist.push(frequentflyerObj);
						Lobibox.notify('success', {size: 'mini', delay : 1500, msg: "Airline Added Successfully"});	
						} else {
						Lobibox.notify('success', {size: 'mini', delay : 1500, msg: "Airline Updated Successfully"});	
						$scope.ffChildObjEdit = null;
					}
				}		
			}		 			
		};
		
		// Check Whether The Airline Is Already Added In The FF Details List
		
		function checkFFDtlDuplicate(currValue) {
			var isValid = true;		
			if($scope.freqflyerlist.length != 0) {		
				for(var i = 0, size = $scope.freqflyerlist.length; i < size ; i++) {
					var value = $scope.freqflyerlist[i].ffairline;
					if (currValue == value) {
						isValid = false;
					} 	 
				}
			}
			return isValid;
		}
		
		function clearFFFieldValues () {
			$('#profile_flyerNo').val("");
			$('#profileFlyerNo').val("");
			$('#profile_airline').val("");	
			$('#profileAirlineName').val("");	
			$('#profileAirlineCode').val("");
			$('#profile_AirlineCode').val("");
			$('#addFrequentFlyer').show();
		}
		
		$scope.removeFrequentFlyer = function (frequentflyer) {
			$scope.freqflyerlist.pop(frequentflyer);
		};
		// Check The Variable Is A Email
		
		function validEmail(v) {	    
			var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
			return pattern.test(v);
		}
		// Check The Variable Is A Number
		
		function isNumber(n) {
			return !isNaN(parseFloat(n)) && isFinite(n);
		}
		// Check Whether The Phone Number Is Already Added
		
		function validateDuplicateContact(currValue) {
			var isValid = true;		
			if($scope.phonenumbers.length != 0) {		
				for(var i = 0, size = $scope.phonenumbers.length; i < size ; i++) {
					var value = $scope.phonenumbers[i].commvalue;
					if (currValue == value) {
						isValid = false;
					} 	 
				}
			}
			return isValid;
		}
		// Check Whether Email Is Already Added
		
		function validateDuplicateEmail(currValue) {		
			var isValid = true;		
			if($scope.emails.length != 0) {		
				for(var i = 0, size = $scope.emails.length; i < size ; i++) {
					var value = $scope.emails[i].commvalue;
					if (currValue == value) {
						isValid = false;
					} 	 
				}
			}
			return isValid;
		}
		
		var $validator = jQuery("#profileForm").validate({
			highlight: function(element) {
				jQuery(element).closest('.form-group').removeClass('has-success').addClass('has-error');
			},
			success: function(element) {
				jQuery(element).closest('.form-group').removeClass('has-error');
			}
		});
		
		$scope.ownProfileUpdate = function () {
		var date = new Date($scope.dob);
					var dateformat = date.getDate() +'-'+date.getMonth() +'-'+ date.getFullYear() 
			$scope.profileInfoData.personaldata.dob = dateformat
			var $valid = jQuery('#profileForm').valid();
			if(!$valid) {
				return false;
				} else {
				$scope.loading = true;
				var address = [];
				if($scope.addressObj== null){
					$scope.profileInfoData.contactdata.address = address;
					}else{
					$scope.profileInfoData.contactdata.address = address;
					$scope.profileInfoData.contactdata.address.push($scope.addressObj);
				}
				$scope.profileInfoData.contactdata.emails = $scope.emails;
				$scope.profileInfoData.contactdata.phonenumbers = $scope.phonenumbers;
				$scope.profileInfoData.profileid = $scope.profileInfoData.profileid;
				$scope.profileInfoData.agntid = $scope.profileInfoData.agntid;
				$scope.profileInfoData.userid = $scope.profileInfoData.userid;
				
				if ($scope.profileInfoData.miscdata.mealpref == null) {
					var mealpref = [];
					var mealPreference = new Object();					
					$scope.profileInfoData.miscdata.mealpref = mealpref;
					$scope.profileInfoData.miscdata.mealpref.push(mealPreference);
				}
				if ($scope.profileInfoData.miscdata.seatpref == null) {
					var seatpref = [];
					var seatPreference = new Object();		
					$scope.profileInfoData.miscdata.seatpref = seatpref;
					$scope.profileInfoData.miscdata.seatpref.push(seatPreference);
				}
				$scope.profileInfoData.miscdata.mealpref[0].prefkey = $('#profile_mealPreference').val();				
				$scope.profileInfoData.miscdata.seatpref[0].prefkey = $('#profile_seatPreference').val();	
				$scope.profileInfoData.miscdata.freqflyerlist = $scope.freqflyerlist;
				//console.log($scope.profileInfoData);
				$http({			
					method:'put',
					url: ServerService.serverPath+'rest/manipulate/profile',
					data:$scope.profileInfoData
					}).then(function(response,status) {	
					$scope.loading = false;
					if (response.data.status = 'success') {
						
						var jsondata=response.data.data;
						sessionStorage.setItem("profileJsondata",JSON.stringify(jsondata));
						$location.path('/profile');
						Lobibox.notify('success', {
							size: 'mini',
							delay: 1500,
							msg: response.data.message
						});
						$timeout(function(){
							//$routeSegment.chain[0].reload();
							//location.reload(true);
						},500);
						}else {
						$location.path('/profile');
						Lobibox.notify('error', {
							size: 'mini',
							delay: 1500,
							msg: response.data.errorMessage
						});	
					}
				});
			}
		};
	}
});