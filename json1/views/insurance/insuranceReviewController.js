ouaApp.controller('insuranceReviewController', function ($scope, $http,$location, $parse, renderHTMLFactory, NationService, ServerService, $timeout) {
	$scope.renderHTMLFactory = renderHTMLFactory;
	NationService.nationGet();
    $scope.isCollapsed = true;
	$scope.selectedRidersplan = JSON.parse(sessionStorage.getItem('ridersplan'));
	$scope.selectedAvilplan = JSON.parse(sessionStorage.getItem('avilplan'));
	$scope.searchRequest = JSON.parse(sessionStorage.getItem('InsuranceSearchRequest'));
	var riderFare = 0;
		var totalFare=parseInt($scope.selectedAvilplan.totalFare);
			if($scope.selectedRidersplan != null || $scope.selectedRidersplan != undefined){
			
			for(var a=0; a<$scope.selectedRidersplan.length; a++){
				riderFare = $scope.selectedRidersplan[a].totalFare
					totalFare = totalFare+parseInt(riderFare);
				}
				
			} 
			
				$scope.totalFare =totalFare;
	//console.log(JSON.stringify($scope.searchRequest));
	/**
	@author Raghava Muramreddy
	to showing payment error message
	*/
	$scope.goBack=function(path){
		$location.path(path);
	}
	if(sessionStorage.getItem("paymentError")){
		var paymentError = JSON.parse(sessionStorage.getItem("paymentError"));
			Lobibox.alert('error',
			{
				msg: paymentError.errorMessage,
				callback: function(lobibox, type){
					if (type === 'ok'){
						sessionStorage.removeItem("paymentError");
					}
				}
			});
		
	}
	$scope.nationality1 = JSON.parse(localStorage.getItem('nationality'))
	$scope.nationality = JSON.stringify($scope.nationality1);
	$scope.bookingRequest = {}
	/**
	//@author Raghava Muramreddy
	Framing Traveller Details ...
	*/
	
	
	$scope.salutations=["Mr","Mrs","Miss"];
	$scope.childsalutations=["Miss","Mstr"];
	$scope.infantsalutations=["Inft"];
	var personaldata=function(){
									this.gender= "M";
     								this.salutation="Mr";
    								this.fname= '';
    								this.lname= '';
     								this.dob= '';
      								this.mobcntrycode= 0;
      								this.mobile='';
      								this.email= '';
      								this.nationalityname= '';
      								this.passportNo= null;
							}
	var childPersonaldata=function(){
									this.gender= "M";
     								this.salutation="Miss";
    								this.fname= '';
    								this.lname= '';
     								this.dob= '';
      								this.mobcntrycode= 0;
      								this.mobile= 0;
      								this.email= '';
      								this.nationalityname= '';
      								this.passportNo= null;
							}
	
	var traveller = function(){
		 this.personaldata=new personaldata();
		 this.agntid= 0;
	     this.profileid= 0;
	     this.travellerRefRPH= 'A1';
	}
	var childTraveller = function(){
		 this.personaldata=new childPersonaldata();
		 this.agntid= 0;
	     this.profileid= 0;
	     this.travellerRefRPH= 'A1';
	}
	var travellerByRoom=function(){
	    this.adultTravellersList=[];
		this.childTravellersList=[];
	}
	
	$scope.travellersListByRoom=JSON.parse(sessionStorage.getItem("travellersListByRoom"));
	if($scope.travellersListByRoom==undefined){
		$scope.travellersListByRoom = [];
				var travellerByRoomObj=new travellerByRoom();
			for (i=0;i<$scope.searchRequest.travelInsurance.adultCount;i++){
					var travellerObj = new traveller();
					travellerByRoomObj.adultTravellersList.push(travellerObj);
			}
			for(j=0;j<$scope.searchRequest.travelInsurance.childCount;j++){
					var travellerObj = new childTraveller();
					travellerByRoomObj.childTravellersList.push(travellerObj);
			}
			$scope.travellersListByRoom.push(travellerByRoomObj);
			
			console.log(JSON.stringify($scope.travellersListByRoom));
	}
	/**
		Framing Traveller Details end.
	*/
	/**
	$scope.adults = []
	$scope.childs = []
	var adult = $scope.searchRequest.travelInsurance.adultCount;
	for(var a=0; a< adult; a++){
		$scope.adults.push(a)
		}
		
	var child = $scope.searchRequest.travelInsurance.childCount
     for(var c=0; c< child; c++){
		 $scope.childs.push(a)
		 }
	 end */
	
	/* signIn function */
	$scope.showSignIn = false;
	$scope.signIn = function(){
		$scope.showSignIn = $scope.showSignIn ? false : true;
		
	}
	
	/* end */
	
	/* multiple date */
	
	$timeout(function() {
			$('.adultTrvlr').each(function(index){
				jQuery(this).attr('id','adultTrvlr_'+(index+1));
			});
			$('.childTrvlr').each(function(index){
				jQuery(this).attr('id','childTrvlr_'+(index+1));
			});
			$('.infantTrvlr').each(function(index){
				jQuery(this).attr('id','infantTrvlr_'+(index+1));
			});
			$('.dateOfBirth').each(function(index){
				jQuery(this).attr('id','dateOfBirth'+(index+1));
			});
			$('.cdatepicker').each(function(index){
				jQuery(this).attr('id','cdatepicker'+(index+1));
			});
			$('.idatepicker').each(function(index){
				jQuery(this).attr('id','idatepicker'+(index+1));
			});
			
			$('[id*=dateOfBirth]').daterangepicker({ 
				singleDatePicker: true,
    showDropdowns: true,
    minYear: 1901,
    maxYear: parseInt(moment().format('YYYY'),10)
  }, function(start, end, label) {
    var years = moment().diff(start, 'years');
	$scope.age=years;
  jQuery(this).attr('data-age',years);
			});
			
			$('[id*=cdateOfBirth]').daterangepicker({ 
				
				singleDatePicker: true,
    showDropdowns: true,
    minYear: 1901,
    maxYear: parseInt(moment().format('YYYY'),10)
  }, function(start, end, label) {
    var years = moment().diff(start, 'years');
   
	jQuery(this).attr('data-age',years);
			});
			
			$('[id*=idateOfBirth]').daterangepicker({ 
				
				singleDatePicker: true,
    showDropdowns: true,
    minYear: 1901,
    maxYear: parseInt(moment().format('YYYY'),10)
  }, function(start, end, label) {
    var years = moment().diff(start, 'years');
   
	jQuery(this).attr('data-age',years);
			});
			
			}, 2000 );
		
		
	/* end */
	
	
    $scope.status = {
        isFirstOpen: true
    };

    $scope.getNumber = function (num) {
        return new Array(num);
    };

    $scope.traveller = {}
/*     $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };
    $scope.format = 'dd-MMMM-yyyy';
    
    $scope.dates = [{
        date: '01-05-2001'
    }, {
        date: '05-05-2014'
    }]; */
	$('input[name="birthDate"]').daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
    minYear: 1901,
    maxYear: parseInt(moment().format('YYYY'),10)
  }, function(start, end, label) {
    var years = moment().diff(start, 'years');
    alert("You are " + years + " years old!");
  });
	 
	 
    $scope.open = function ($event, dt) {
        $event.preventDefault();
        $event.stopPropagation();
        dt.opened = true;
    };
	
	/* passengerHits function */
	$scope.paymentDetail = {}
	var expireDate = {}
	$scope.passengerHits = function(bookingWay){
	$scope.travellerdatalist = [];
	expireDate = $scope.paymentDetail.expireYear+$scope.paymentDetail.expireMonth
	
	/* passengerList */
	$('.adultTrvlr').each(function(index){
		jQuery(this).attr('id', 'adultTrvlr_'+ (index+1));
		});
		
		$('.childTrvlr').each(function(index){
		jQuery(this).attr('id', 'childTrvlr_'+ (index+1));
		});
		
		$('.travellers').each(function(ind){
			var id = $(this).attr('id');
			console.log(id);
			var passId = id.split('_')
			if(passId[0] == 'adultTrvlr'){
				var salutationVal = $(this).find('#salutation_'+ind).val();
				var fname = $(this).find('#afname_'+ind).val();
				var lnameVal = $(this).find('#alname_'+ind).val();
				var datepickerVal = $(this).find('#adateOfBirth_'+ind).val();
				var c_codeVal = '+91'
				var c_noVal = $(this).find('#aphone_'+ind).val();
				var emailid = $(this).find('#amail_'+ind).val();
				var nationalityVal = $(this).find('#anationalityVal_'+ind).val();
				var passport = $(this).find('#apassport_'+ind).val();
				
			} else if(passId[0] == 'childTrvlr'){
				var salutationVal = $(this).find('.csalutation').val();
				//var salutation = $(this).find('.salutation').val();
				var fname = $(this).find('.cfname').val();
				var lnameVal = $(this).find('.clname').val();
				var datepickerVal = $(this).find('.cdateOfBirth').val();
				var c_codeVal = '+91'
				var c_noVal = $(this).find('.cphone').val();
				var emailid = $(this).find('.cmail').val();
				var nationalityVal = $(this).find('.cnation').val();
				var passport = $(this).find('.cpassport').val();
				}
				var personalObj = new Object();
				var travellerdataObj = new Object();
				if(salutationVal == 'Mr'){
					personalObj.gender = 'M'
				} else {
				personalObj.gender = 'F'
				}
				personalObj.salutation = salutationVal;
				personalObj.fname = fname;
				personalObj.lname = lnameVal;
				personalObj.dob = datepickerVal;
				personalObj.mobcntrycode = c_codeVal;
				personalObj.mobile = c_noVal;
				personalObj.email = emailid;
				personalObj.nationalitycode = nationalityVal;
				personalObj.passportNo = passport;
				
				travellerdataObj.personaldata = personalObj;
				travellerdataObj.agntid = 0;
		travellerdataObj.profileid = "-0";
		travellerdataObj.travellerRefRPH = "A"+ind;
				$scope.travellerdatalist.push(travellerdataObj)
			})
		
	/* end */
	if(bookingWay == 'Payment'){
		$scope.bookingRequest.payfortTxnData = {
			"3ds_url": null,
				"access_code": null,
				"amount": null,
				"authorization_code": null,
				"card_bin": null,
				"card_number": $scope.paymentDetail.cardNo,
				"card_security_code": $scope.paymentDetail.cvv,
				"command": null,
				"currency": 'OMR',
				"customer_email": null,
				"customer_ip": null,
				"customer_name": $scope.paymentDetail.cardName,
				"eci": null,
				"expiry_date": expireDate,
				"fort_id": null,
				"language": null,
				"merchant_identifier": null,
				"merchant_reference": null,
				"order_description": null,
				"payment_option": $scope.path,
				"response_code": null,
				"response_message": null,
				"return_url": null,
				"service_command": "TOKENIZATION",
				"device_fingerprint": $('#device_fingerprint').val(),
				"device_id": null,
				"signature": null,
				"status": null,
				"token_name": null,
				"postUrl": null
			}
	} else if(bookingWay == 'NonPayment'){
	$scope.bookingReq.payfortTxnData = null;
	}
	$scope.passengerInfos=[];
	$scope.bookingRequest.travelInsuranceReq = $scope.searchRequest.travelInsurance;
	
	$scope.bookingRequest.selectedPlanBean = $scope.selectedAvilplan;
	$scope.bookingRequest.selectedriderPlans = $scope.selectedRidersplan;
	$scope.bookingRequest.passengerInfos = $scope.travellerdatalist;
	$scope.bookingRequest.totalFare=$scope.totalFare;
	/* $scope.bookingRequest.cardName = $scope.paymentDetail.cardName;
	$scope.bookingRequest.cardNo = $scope.paymentDetail.cardNo;
	$scope.bookingRequest.expireMonth = $scope.paymentDetail.expireMonth;
	$scope.bookingRequest.expireYear = $scope.paymentDetail.expireYear;
	$scope.bookingRequest.cvv = $scope.paymentDetail.cvv; */
	$scope.bookingRequest.passengerInfos=$scope.travellersListByRoom[0].adultTravellersList.concat($scope.travellersListByRoom[0].childTravellersList);
	sessionStorage.setItem("travellersListByRoom",JSON.stringify($scope.travellersListByRoom)) ;
	$http.post(ServerService.serverPath+'rest/insurance/purchase',$scope.bookingRequest,{
	headers : {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
	}).then(function(response){
		console.log("purchase Data>>"+response);
			$scope.loading = false;
			$scope.disabled = false;
			if(response.data.status=='SUCCESS'){
					window.location.href=response.data.returnUrl;
			    }else if(response.data.status=='FAILURE') {
							
							Lobibox.alert('error',
								{
									msg: response.data.message
							});
				}else{
						
							Lobibox.alert('error',
						{
							msg: 'Unexpected Error Occurred, Please Try Again Later'
						});
					}
		})
		$scope.traveller.open = true;
		}
	/* end */
	
	/* payNow function */
	$scope.payment = {}
	$scope.message = false;
	$scope.showCard = false;
	$scope.payNow = function(){
		var cardId = $('#cardNo').val();
		var firstDigit = cardId.charAt(0)
		var secondDigit = cardId.charAt(1)
		if(firstDigit == 3 && between(secondDigit,4,7) && cardId.length == 5){
			$scope.path = 'american';
			$scope.message = false;
			$scope.showCard = true;
			} else if(firstDigit == 3 && (secondDigit == 0 || secondDigit == 6 || secondDigit == 8) && cardId.length ==5){
			$scope.path = 'dinners';
			$scope.message = false;
			$scope.showCard = true;
			} else if(((firstDigit == 5 && between(secondDigit,1,5)) || (firstDigit == 2 && between(secondDigit,2,7)))&&cardId.length== 5){
			$scope.path = 'master';
			$scope.message = false;
			$scope.showCard = true;
			} else if( (firstDigit == 5 || firstDigit == 6) && (cardId.length == 5) ){
			$scope.path = "metro";
			$scope.message = false;
			$scope.showCard = true;
			} else if( (firstDigit == 4) && (cardId.length == 5) ){
			$scope.path = 'visa';
			$scope.message = false;
			$scope.showCard = true;
			} else if(cardId.length == 0){
			$scope.message = true;
			$scope.showCard = false;
		}
	}
	/* end */
	function daysBetween( date1, date2 ) {
			//Get 1 day in milliseconds
			var one_day=1000*60*60*24;
			
			// Convert both dates to milliseconds
			var date1_ms = date1.getTime();
			var date2_ms = date2.getTime();
			
			// Calculate the difference in milliseconds
			var difference_ms = date2_ms - date1_ms;
			
			// Convert back to days and return
			return Math.round(difference_ms/one_day); 
		}
		
		var years;
		function getAge(dateString,dateType,deptDate) {
			//var now = new Date();
			var depdate = deptDate;
			var depdate2exp = depdate.split('/');
			var now = new Date(depdate2exp[2],depdate2exp[1]-1,depdate2exp[0]);
			var today = new Date(now.getYear(),now.getMonth(),now.getDate());
			
			var yearNow = now.getYear();
			var monthNow = now.getMonth();
			var dateNow = now.getDate();
			
			if (dateType == 1)
			var dob = new Date(dateString.substring(0,4),
			dateString.substring(4,6)-1,
			dateString.substring(6,8));
			else if (dateType == 2)
			var dob = new Date(dateString.substring(0,2),
			dateString.substring(2,4)-1,
			dateString.substring(4,6));
			else if (dateType == 3)
			var dob = new Date(dateString.substring(6,10),
			dateString.substring(3,5)-1,
			dateString.substring(0,2));
			else if (dateType == 4)
			var dob = new Date(dateString.substring(6,8),
			dateString.substring(3,5)-1,
			dateString.substring(0,2));
			else
			return '';
			
			var yearDob = dob.getYear();
			var monthDob = dob.getMonth();
			var dateDob = dob.getDate();
			
			var yearAge = yearNow - yearDob;
			
			if (monthNow >= monthDob)
			var monthAge = monthNow - monthDob;
			else {
				yearAge--;
				var monthAge = 12 + monthNow -monthDob;
			}
			
			if (dateNow >= dateDob)
			var dateAge = dateNow - dateDob;
			else {
				monthAge--;
				var dateAge = 31 + dateNow - dateDob;
				
				if (monthAge < 0) {
					monthAge = 11;
					yearAge--;
				}
			}
			years=yearAge;
			//return yearAge + ' years ' + monthAge + ' months ' + dateAge + ' days';
			}
	/* end */
	
});
function between(x, min, max) {
  return x >= min && x <= max;
}