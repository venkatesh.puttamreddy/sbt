ouaApp.controller('insuranceResultController', function ($scope, $http, renderHTMLFactory, $location, ServerService) {
    sessionStorage.removeItem('flightplusSearchResp');
    sessionStorage.removeItem('hotelPlusSearchResp');
    sessionStorage.removeItem('repriseResponse');
    sessionStorage.removeItem('roomIndex');
    sessionStorage.removeItem('selectedFliResp');
    sessionStorage.removeItem('selectedHotResp');
	
	$scope.isCollapsed = true;
	$scope.renderHTMLFactory = renderHTMLFactory;
	var InsuranceSearchRequest = JSON.parse(sessionStorage.getItem('InsuranceSearchRequest'));
	$scope.InsuranceSearchReqItem = InsuranceSearchRequest;
	
	$scope.InsuranceSearchResult = JSON.parse(sessionStorage.getItem('InsuranceSearchResult'));
	console.log("InsuranceSearchRequest "+JSON.stringify($scope.InsuranceSearchReqItem));
	if($scope.InsuranceSearchReqItem.srchType == 2){
		$scope.trip = 'Round trip';
	} else if($scope.InsuranceSearchReqItem.srchType == 1){
		$scope.trip = 'One Way';
		}
		
		$scope.riderPlans = $scope.InsuranceSearchResult.riderPlans
	var plans = $scope.InsuranceSearchResult.availablePlans
	
	/* sorting based on totalFare */
	function sorting (array, key){
		return array.sort(function(a, b){
			var x = a[key]; var y = b[key];
			return ( (x < y) ? -1 : ((x > y) ? 1 : 0));
			});
		}
		$scope.availablePlans = sorting(plans, 'totalFare');
	/* end */
	$scope.insurancedate={
		startDate: moment(),
		endDate: moment(),
		locale:{
			format : 'D-MMM-YY'
			}
		}
	$scope.isLifeStylePlanSelected;
/* availablePlans function */
$scope.availPlans = function(index){
	$scope.selectedAvailPlan = $scope.availablePlans[index];
	sessionStorage.setItem('avilplan',JSON.stringify($scope.selectedAvailPlan));
	$scope.isLifeStylePlanSelected=true;
}

/* end */

/* ridersPlans function */
$scope.selectedRaidPlan = [];
$scope.ridersPlans = function(value){
	console.log(value);
	$scope.riderList = $scope.selectedRaidPlan.push($scope.riderPlans[value]);
	sessionStorage.setItem('ridersplan',JSON.stringify($scope.selectedRaidPlan));
}
/* end */
/* onBook function */
$scope.onBook = function(){
	//var checked = $('input[name="selectplans"]:checked').val();
	//var radioType = $('#selectplans').prop('checked');
	if(!$scope.isLifeStylePlanSelected){
		$scope.error = "Select Travel Plan"
	} else {
		$location.path('/insuranceReview')
		}
	
}
/* end */
	
});
