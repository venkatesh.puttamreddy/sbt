ouaApp.controller('flightReviewController', function ($scope, $http, NationService, $rootScope, Month, Year, $uibModal, ServerService, $location, $timeout) {
	NationService.nationGet();
	$('html, body').scrollTop(0);
	/**
	@author Raghava Muramreddy
	to showing payment error message
	*/
	if(sessionStorage.getItem("paymentError")){
		var paymentError = JSON.parse(sessionStorage.getItem("paymentError"));
			Lobibox.alert('error',
			{
				msg: paymentError.errorMessage,
				callback: function(lobibox, type){
					if (type === 'ok'){
						sessionStorage.removeItem("paymentError");
					}
				}
			});
		
	}
	$scope.goBack=function(path){
		$location.path(path);
	}
	
	//$scope.nationality = JSON.stringify(sessionStorage.getItem('nationality'));
	$scope.nationality1 = JSON.parse(localStorage.getItem('nationality'))
	$scope.nationality = JSON.stringify($scope.nationality1);
	
	$scope.month = Month;
	$scope.year = Year;
	
	
	$scope.flightBook = JSON.parse(sessionStorage.getItem('flightPriceData'));
	$scope.flightBooking = $scope.flightBook.selectedResult;
	
	
	$scope.totalFare = $scope.flightBooking.flightSearchResults[0].totFare;
	
	/**
	//@author Raghava Muramreddy
	Framing Traveller Details ...
	*/
	/*$scope.user=JSON.stringify(sessionStorage.getItem("user"));
	$scope.contactNumber="";
	$scope.contactEmail="";
	if(sessionStorage.getItem("user")){
	$scope.user=sessionStorage.getItem("user");
	$scope.contactNumber=$scope.user.contactNumber;
	$scope.contactEmail=$scope.user.contactEmail;
	}else{
	
	$scope.user={
		contactEmail:$scope.contactEmail,
		contactNumber:$scope.contactNumber
		}
	}*/
	$scope.nationality1 = JSON.parse(localStorage.getItem('nationality'))
	$scope.nationality = JSON.stringify($scope.nationality1);
	$scope.salutations=["Mr","Mrs","Miss"];
	$scope.childsalutations=["Miss","Mstr"];
	$scope.infantsalutations=["Inft"];
	var personaldata=function(){
									this.gender= "M";
     								this.salutation="Mr";
    								this.fname= '';
    								this.lname= '';
     								this.dob= '';
      								this.mobcntrycode= 0;
      								this.mobile='';
      								this.email= '';
      								this.nationalityname= '';
      								this.passportNo= null;
							}
	var childPersonaldata=function(){
									this.gender= "M";
     								this.salutation="Miss";
    								this.fname= '';
    								this.lname= '';
     								this.dob= '';
      								this.mobcntrycode= 0;
      								this.mobile= 0;
      								this.email= '';
      								this.nationalityname= '';
      								this.passportNo= null;
							}
	var infantPersonaldata=function(){
									this.gender= "M";
     								this.salutation="Inft";
    								this.fname= '';
    								this.lname= '';
     								this.dob= '';
      								this.mobcntrycode= 0;
      								this.mobile= 0;
      								this.email= '';
      								this.nationalityname= '';
      								this.passportNo= null;
							}
	var traveller = function(){
		 this.personaldata=new personaldata();
		 this.agntid= 0;
	     this.profileid= 0;
	     this.travellerRefRPH= 'A1';
	}
	var childTraveller = function(){
		 this.personaldata=new childPersonaldata();
		 this.agntid= 0;
	     this.profileid= 0;
	     this.travellerRefRPH= 'A1';
	}
	var infantTraveller = function(){
		 this.personaldata=new infantPersonaldata();
		 this.agntid= 0;
	     this.profileid= 0;
	     this.travellerRefRPH= 'A1';
	}

	var travellerByRoom=function(){
	    this.adultTravellersList=[];
		this.childTravellersList=[];
		this.infantTravellersList=[];
	}
	
	$scope.travellersListByRoom=JSON.parse(sessionStorage.getItem("travellersListByRoom"));
	if($scope.travellersListByRoom==undefined){
		$scope.travellersListByRoom = [];
				var travellerByRoomObj=new travellerByRoom();
			for (i=0;i<$scope.flightBooking.adtCnt;i++){
					var travellerObj = new traveller();
					travellerByRoomObj.adultTravellersList.push(travellerObj);
			}
			for(j=0;j<$scope.flightBooking.chldCnt;j++){
					var travellerObj = new childTraveller();
					travellerByRoomObj.childTravellersList.push(travellerObj);
			}
			for(j=0;j<$scope.flightBooking.infntCnt;j++){
					var travellerObj = new infantTraveller();
					travellerByRoomObj.infantTravellersList.push(travellerObj);
			}
				$scope.travellersListByRoom.push(travellerByRoomObj);
			
			console.log(JSON.stringify($scope.travellersListByRoom));
	}
	/**
		Framing Traveller Details end.
	*/
	
	/*$scope.adults = { afname: ''};
	
	$scope.adults = [];
	$scope.childs = [];
	var adult = $scope.flightBooking.adtCnt;
	var child = $scope.flightBooking.chldCnt
	for(var a=0; a < adult; a++){
		
		$scope.adults.push(a);
	}
	
		for(var a=0; a < child; a++){
		
		$scope.childs.push(a);
	}*/
	
	/**
	     //@author Raghava Murmareddy
		get Selected Insurance Details which we select at Review Time
		
	*/
	$scope.selectedInsurancePlanId=sessionStorage.getItem('selectedInsurancePlanId');
	if($scope.selectedInsurancePlanId!=null){
		$scope.withInsurance=true;
	$scope.selectedInsurancePlan =$scope.flightBook.insurancePlans.availablePlans[$scope.selectedInsurancePlanId];
	}
	
	/**
	Selected Insurance Details end
	*/
	
	/* flight details */
	
	$rootScope.selectedIndex = JSON.parse(sessionStorage.getItem('flightIndex'));
	$scope.openFlight = function(size, parentSelector){
	
		var parentElem = parentSelector ? 
		angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
		
		$scope.animationEnabled = true;
	    var modalInstance = $uibModal.open({
			animation: $scope.animationEnabled,
			ariaLabelledBy : 'modal-title',
			ariaDescribedBy : 'modal-body',
			templateUrl : 'flightDetailedinfoContent.html',
			controller : 'flightDetailedinfoContent',
			backdrop: true,
			size : size,
			appendTo : parentElem,
			resolve: {
				items: function(){
					return $scope.items
				}
			}
		})
		}
	/* end */
	/* Fare Rules*/
	$scope.openFareRules = function(size, parentSelector){
	
		var parentElem = parentSelector ? 
		angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
		
		$scope.animationEnabled = true;
	    var modalInstance = $uibModal.open({
			animation: $scope.animationEnabled,
			ariaLabelledBy : 'modal-title',
			ariaDescribedBy : 'modal-body',
			templateUrl : 'flightFareRulesContent.html',
			controller : 'flightFareRulesContent',
			backdrop: true,
			size : size,
			appendTo : parentElem,
			resolve: {
				items: function(){
					return $scope.items
				}
			}
		})
		}
	
	/* Fare Rules End*/
	/* AgencyTermsOfBusiness*/
	$scope.openAgencyTermsOfBusiness = function(size, parentSelector){
		
		var parentElem = parentSelector ? 
		angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
		
		$scope.animationEnabled = true;
	    var modalInstance = $uibModal.open({
			animation: $scope.animationEnabled,
			ariaLabelledBy : 'modal-title',
			ariaDescribedBy : 'modal-body',
			templateUrl : 'agencyTermsOfBusiness.html',
			controller : 'flightFareRulesContent',
			backdrop: true,
			size : size,
			appendTo : parentElem,
			resolve: {
				items: function(){
					return $scope.items
				}
			}
		})
		}
	
	/* AgencyTermsOfBusiness End*/
	/* signIn function */
	$scope.showSignIn = false;
	$scope.signIn = function(){
		$scope.showSignIn = $scope.showSignIn ? false : true;
	}
	
	/* end */
	
	/* multiple date */
	var depDate = $scope.flightBooking.deptDate;
		var depDateSplt = depDate.split('/');
		var minDepDate = new Date(depDateSplt[1]+'/'+depDateSplt[0]+'/'+depDateSplt[2]);
		var dt = daysBetween(new Date(),minDepDate)-1;
		var chldMinDate = daysBetween(new Date(),minDepDate)+2;
		
		
			$timeout(function() {
			$('.adultTrvlr').each(function(index){
				jQuery(this).attr('id','adultTrvlr_'+(index+1));
			});
			$('.childTrvlr').each(function(index){
				jQuery(this).attr('id','childTrvlr_'+(index+1));
			});
			$('.infantTrvlr').each(function(index){
				jQuery(this).attr('id','infantTrvlr_'+(index+1));
			});
			$('.dateOfBirth').each(function(index){
				jQuery(this).attr('id','dateOfBirth'+(index+1));
			});
			$('.cdatepicker').each(function(index){
				jQuery(this).attr('id','cdatepicker'+(index+1));
			});
			$('.idatepicker').each(function(index){
				jQuery(this).attr('id','idatepicker'+(index+1));
			});
			var minAdultYear=parseInt(minDepDate.getFullYear())-9;
		
			var maxAdultEndYear=parseInt(minDepDate.getFullYear())-109;
			$('[id*=dateOfBirth]').daterangepicker({ 
				locale: {
					format: 'DD/MM/YYYY'
				},
				singleDatePicker: true,
    showDropdowns: true,
    /**
     @author Venkatesh Adult DOB DropDown 
	*/
			startDate:new Date(minAdultYear,minDepDate.getMonth(),minDepDate.getDate()), 
			minDate:new Date(maxAdultEndYear,minDepDate.getMonth(),minDepDate.getDate()), 
			maxDate:new Date(minAdultYear,minDepDate.getMonth(),minDepDate.getDate()), 
		  endDate:new Date(maxAdultEndYear,minDepDate.getMonth(),minDepDate.getDate())
		  /**
		  Adult DOB DropDown End.
		  */
  }, function(start, end, label) {
    var years = moment().diff(start, 'years');
	$scope.age=years;
  jQuery(this).attr('data-age',years);
			});
			/**
           @author Venkatesh Child DOB DropDown 
			*/
			var childMaxYear=parseInt(minDepDate.getFullYear())-3;
			var childMinYear=parseInt(minDepDate.getFullYear())-9;
			$('[id*=cdateOfBirth]').daterangepicker({ 
						locale: {
							format: 'DD/MM/YYYY'
						},
						singleDatePicker: true,
						showDropdowns: true,
						minDate:new Date(childMinYear,minDepDate.getMonth(),minDepDate.getDate()),
						maxDate:new Date(childMaxYear,minDepDate.getMonth(),minDepDate.getDate()),
						maxYear: parseInt(moment().format('YYYY'),10)
			/**
			Child DOB DropDown End
			*/
  }, function(start, end, label) {
    var years = moment().diff(start, 'years');
   
	jQuery(this).attr('data-age',years);
			});
			/**
           @author Venkatesh Infant DOB DropDown 
			*/
			var infantmaxyear=parseInt(minDepDate.getFullYear());
		   var infantminyear=parseInt(minDepDate.getFullYear())-2;
			$('[id*=idateOfBirth]').daterangepicker({ 
				locale: {
					format: 'DD/MM/YYYY'
				},
				singleDatePicker: true,
				showDropdowns: true,
				minDate:new Date(infantminyear,minDepDate.getMonth(),minDepDate.getDate()),
				maxDate:new Date(infantmaxyear,minDepDate.getMonth(),minDepDate.getDate()),
				maxYear: parseInt(moment().format('YYYY'),10)
				/**
			Infant DOB DropDown End
			*/
  }, function(start, end, label) {
    var years = moment().diff(start, 'years');
   
	jQuery(this).attr('data-age',years);
			});
			
			}, 2000 );
		
		
	/* end */
	
	/* for datepicker */
	/* $('input[name="dateOfBirth"]').daterangepicker({
		singleDatePicker: true,
		showDropdowns: true,
		opens: 'top',
		minYear: 1919,
		maxYear: parseInt(moment().format('yyyy'),10)
		},function(start, end, label){
		var years = moment().diff(start, 'years');
	})  */
	
	
	/* end */
	
	
	/* proceedCustomDetails function */
	$scope.loading = false;
	$scope.submitted = false;
	$scope.disabled = false;
	$scope.paymentDetail = {}
	$scope.selectDoc = {}
	$scope.bookingReq = {}
	$scope.specialServiceRequest = {}
	$scope.selectDoc.dockNo = null;
	var expireDate  = {}
	var traveller = {};
	var childTraveller = {};
	var infantTraveller = {};
	$scope.proceedCustomDetails = function(bookingWay, valid){
	var cardId = $('#cardNo').val();
	if(cardId.length!=16){
		valid=false;
	}
	$scope.submitted = true;
	if(valid){
	$scope.errMsg = "";
	$scope.loading = true;
	$scope.disabled = true;
		expireDate = $scope.paymentDetail.expireYear+$scope.paymentDetail.expireMonth
		$scope.travellerdatalist = [];
		$('.adultTrvlr').each(function(index){
			jQuery(this).attr('id', 'adultTrvlr_'+(index+1));
		}); 
		$('.childTrvlr').each(function(index){
			jQuery(this).attr('id', 'childTrvlr_'+(index+1));
		});
		$('.infantTrvlr').each(function(index){
			jQuery(this).attr('id', 'infantTrvlr_'+(index+1));
		});
	/*	$('.traveller').each(function(ind){
			var id = $(this).attr('id');
			console.log(id);
			var passId = id.split('_');
			if(passId[0] == 'adultTrvlr'){
				console.log("adultTrvlr");
				var salutationVal = $(this).find('#salutationVal_'+ind).val();
				var fname = $(this).find('#afname_'+ind).val();
				var lnameVal = $(this).find('#lnameVal_'+ind).val();
				var datepickerVal = $(this).find('.dateOfBirth').val();
				var c_codeVal = '+91'
				var c_noVal = $(this).find('#c_noVal_'+ind).val();
				var emailid = $(this).find('#emailId_'+ind).val();
				var nationalityVal = $(this).find('#nationalityVal_'+ind).val();
				var passport = $(this).find('#passport_'+ind).val();
				} else if(passId[0] == 'childTrvlr'){
				var salutationVal = $(this).find('.csalutationVal').val();
				var fname = $(this).find('.cfname').val();
				var lnameVal = $(this).find('.clnameVal').val();
				var datepickerVal = $(this).find('.cdateOfBirth').val();
				var c_codeVal = '+91'
				var c_noVal = $(this).find('.c_noVal').val();
				var emailid = $(this).find('.cemailId').val();
				var nationalityVal = $(this).find('.cnationalityVal').val();
				var passport = $(this).find('.cpassport').val();
			} else if(passId[0] == 'infantTrvlr'){
				var salutationVal = $(this).find('.isalutationVal').val();
				var fname = $(this).find('.ifname').val();
				var lnameVal = $(this).find('.ilnameVal').val();
				var datepickerVal = $(this).find('.idateOfBirth').val();
				var c_codeVal = '+91'
				var c_noVal = $(this).find('.i_noVal').val();
				var emailid = $(this).find('.iemailId').val();
				var nationalityVal = $(this).find('.inationalityVal').val();
				var passport = $(this).find('.ipassport').val();
			}
			var personalObj = new Object();
			var travellerdataObj = new Object();
			if(salutationVal == 'Mr'){
				personalObj.gender = 'M'
				} else {
				personalObj.gender = 'F'
			}
			
			personalObj.salutation = salutationVal;
			personalObj.fname = fname;
			personalObj.lname = lnameVal;
			personalObj.dob = datepickerVal;
			personalObj.mobcntrycode = c_codeVal;
			personalObj.mobile = c_noVal;
			personalObj.email = emailid;
			personalObj.nationalitycode = nationalityVal;
			personalObj.passportNo = passport;
			personalObj.age = $scope.age;
			
			travellerdataObj.personaldata = personalObj;
			travellerdataObj.agntid = 0;
			travellerdataObj.profileid = "-0";
			travellerdataObj.travellerRefRPH = "A1";
			$scope.travellerdatalist.push(travellerdataObj)
			
	});*/
		if(bookingWay == 'Payment'){
			$scope.bookingReq.payfortTxnData = {
				"3ds_url": null,
				"access_code": null,
				"amount": null,
				"authorization_code": null,
				"card_bin": null,
				"card_number": $scope.paymentDetail.cardNo,
				"card_security_code": $scope.paymentDetail.cvv,
				"command": null,
				"currency": 'OMR',
				"customer_email": null,
				"customer_ip": null,
				"customer_name": $scope.paymentDetail.cardName,
				"eci": null,
				"expiry_date": expireDate,
				"fort_id": null,
				"language": null,
				"merchant_identifier": null,
				"merchant_reference": null,
				"order_description": null,
				"payment_option": $scope.path,
				"response_code": null,
				"response_message": null,
				"return_url": null,
				"service_command": "TOKENIZATION",
				"device_fingerprint": $('#device_fingerprint').val(),
				"device_id": null,
				"signature": null,
				"status": null,
				"token_name": null,
				"postUrl": null
			}
			} else if(bookingWay == 'NonPayment'){
			$scope.bookingReq.payfortTxnData = null;
		}
		
		$scope.bookingReq.docketNo = $scope.selectDoc.dockNo;
		$scope.bookingReq.specialServiceRequest = $scope.specialServiceRequest;
		$scope.bookingReq.flightsearchresult = $scope.flightBooking;
		 //$scope.bookingReq.cardName = $scope.paymentDetail.cardName;
		//$scope.bookingReq.cardNo = $scope.paymentDetail.cardNo;
		//$scope.bookingReq.cvv = $scope.paymentDetail.cvv;
		//$scope.bookingReq.expireMonth = $scope.paymentDetail.expireMonth;
		//$scope.bookingReq.expireYear = $scope.paymentDetail.expireYear; 
	/*	$scope.bookingReq.travellerdatalist=[];
		for(i=0;i<$scope.travellersListByRoom[0].adultTravellersList.length;i++){
		$scope.bookingReq.travellerdatalist.push($scope.travellersListByRoom[0].adultTravellersList[i]);
		}
		for(i=0;i<$scope.travellersListByRoom[0].childTravellersList.length;i++){
		$scope.bookingReq.travellerdatalist.push($scope.travellersListByRoom[0].childTravellersList[i]);
		}*/
		$scope.bookingReq.travellerdatalist=$scope.travellersListByRoom[0].adultTravellersList.concat($scope.travellersListByRoom[0].childTravellersList).concat($scope.travellersListByRoom[0].infantTravellersList);
		sessionStorage.setItem("travellersListByRoom",JSON.stringify($scope.travellersListByRoom)) ;
		sessionStorage.setItem("user",JSON.stringify($scope.user));
		$scope.paymentDetail=null;
		$http.post(ServerService.serverPath+'rest/flight/booking/confirm',$scope.bookingReq,{
			headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
			}).then(function(response){
			$scope.loading = false;
			$scope.disabled = false;
			if(response.data.status=='SUCCESS'){
					window.location.href=response.data.returnUrl;
			    }else if(response.data.status=='FAILURE') {
							
							Lobibox.alert('error',
								{
									msg: response.data.message
							});
				}else{
						
							Lobibox.alert('error',
						{
							msg: 'Unexpected Error Occurred, Please Try Again Later'
						});
					}
			
			
		})
	} else {
	$('html,body').scrollTop(240);
		$scope.errMsg = "Enter All * (Mandatory) field";
		}
	
	}
	
	function daysBetween( date1, date2 ) {
			//Get 1 day in milliseconds
			var one_day=1000*60*60*24;
			
			// Convert both dates to milliseconds
			var date1_ms = date1.getTime();
			var date2_ms = date2.getTime();
			
			// Calculate the difference in milliseconds
			var difference_ms = date2_ms - date1_ms;
			
			// Convert back to days and return
			return Math.round(difference_ms/one_day); 
		}
		
		var years;
		function getAge(dateString,dateType,deptDate) {
			//var now = new Date();
			var depdate = deptDate;
			var depdate2exp = depdate.split('/');
			var now = new Date(depdate2exp[2],depdate2exp[1]-1,depdate2exp[0]);
			var today = new Date(now.getYear(),now.getMonth(),now.getDate());
			
			var yearNow = now.getYear();
			var monthNow = now.getMonth();
			var dateNow = now.getDate();
			
			if (dateType == 1)
			var dob = new Date(dateString.substring(0,4),
			dateString.substring(4,6)-1,
			dateString.substring(6,8));
			else if (dateType == 2)
			var dob = new Date(dateString.substring(0,2),
			dateString.substring(2,4)-1,
			dateString.substring(4,6));
			else if (dateType == 3)
			var dob = new Date(dateString.substring(6,10),
			dateString.substring(3,5)-1,
			dateString.substring(0,2));
			else if (dateType == 4)
			var dob = new Date(dateString.substring(6,8),
			dateString.substring(3,5)-1,
			dateString.substring(0,2));
			else
			return '';
			
			var yearDob = dob.getYear();
			var monthDob = dob.getMonth();
			var dateDob = dob.getDate();
			
			var yearAge = yearNow - yearDob;
			
			if (monthNow >= monthDob)
			var monthAge = monthNow - monthDob;
			else {
				yearAge--;
				var monthAge = 12 + monthNow -monthDob;
			}
			
			if (dateNow >= dateDob)
			var dateAge = dateNow - dateDob;
			else {
				monthAge--;
				var dateAge = 31 + dateNow - dateDob;
				
				if (monthAge < 0) {
					monthAge = 11;
					yearAge--;
				}
			}
			years=yearAge;
			//return yearAge + ' years ' + monthAge + ' months ' + dateAge + ' days';
		}
	
	$scope.payment = {}
	$scope.message = false;
	$scope.showCard = false;
	$scope.payNow = function(){
		var cardId = $('#cardNo').val();
		var firstDigit = cardId.charAt(0)
		var secondDigit = cardId.charAt(1)
		if(firstDigit == 3 && between(secondDigit,4,7) && cardId.length == 5){
			$scope.path = 'american';
			$scope.message = false;
			$scope.showCard = true;
			} else if(firstDigit == 3 && (secondDigit == 0 || secondDigit == 6 || secondDigit == 8) && cardId.length ==5){
			$scope.path = 'dinners';
			$scope.message = false;
			$scope.showCard = true;
			} else if(((firstDigit == 5 && between(secondDigit,1,5)) || (firstDigit == 2 && between(secondDigit,2,7)))&&cardId.length== 5){
			$scope.path = 'master';
			$scope.message = false;
			$scope.showCard = true;
			} else if( (firstDigit == 5 || firstDigit == 6) && (cardId.length == 5) ){
			$scope.path = "metro";
			$scope.message = false;
			$scope.showCard = true;
			} else if( (firstDigit == 4) && (cardId.length == 5) ){
			$scope.path = 'visa';
			$scope.message = false;
			$scope.showCard = true;
			} else if(cardId.length == 0){
			$scope.message = true;
			$scope.showCard = false;
		}
	}
	/* end */
	
	$scope.getNumber = function(num) {
		return new Array(num);   
	}
	
});

function between(x, min, max) {
	return x >= min && x <= max;
}
ouaApp.controller('flightFareRulesContent', function($scope, $uibModalInstance, $rootScope, stopsService){
	$scope.flightDetails = JSON.parse(sessionStorage.getItem('flightPriceData'));
	
	$scope.fareRulesList=$scope.flightDetails.selectedResult.flightSearchResults[0].fareRules;
	
	/* if($scope.filter != null || $scope.filter != undefined){
		var filterDatas = stopsService.getStop($scope.flightDetails2, $scope.filter, $scope.airlineCode)
		$scope.flightDetails1 = filterDatas[value];
	} */
		

	$scope.cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};
});
/* ouaApp.factory('Month', function(){
	
	var month = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
	
	return month;
});

ouaApp.factory('Year', function(){
	
	var year = ['2017', '2018', '2019', '2020', '2021', '2022', '2023', '2024', '2025', '2026', '2027', '2028', '2029', '2030', '2031', '2032', '2033', '2034', '2035', '2036', '2037'];
	
	return year;
}); */