ouaApp.controller('flightDetailsController', function ($scope, $http, $uibModal, ConstantService, $location, $rootScope, $uibModal, $log, $document, urlService) {
	$('html, body').scrollTop(0);
	$scope.currency = ConstantService.currency
	console.log("currency "+$scope.currency)
	/* for flight+hotel */
	var currentURL=$location.absUrl();
	$scope.paths = currentURL.substring(currentURL.lastIndexOf("/") + 1, currentURL.length);
	sessionStorage.removeItem("selectedInsurancePlanId");
	if($scope.paths == "holidayFlightDetails"){
		$scope.flightResp = JSON.parse(sessionStorage.getItem('selectedFliResp'));
		$scope.pricingData = $scope.flightResp.selectedResult;
		$scope.totalFare = $scope.pricingData.flightSearchResults[0].totFare;
	$scope.flightdetails = $scope.pricingData.flightSearchResults[0].segGrpList[0].segList[0];
	$scope.flightdetails1 = $scope.pricingData.flightSearchResults[0].segGrpList[1].segList[0];
	$scope.InsuranceDetails = $scope.flightResp.insurancePlans;
	$rootScope.selectedIndex = JSON.parse(sessionStorage.getItem('fltIndex'))
	} else if($scope.paths == "flightDetails"){
		 $scope.flightPricingData = JSON.parse(sessionStorage.getItem('flightPriceData'));
	$scope.pricingData = $scope.flightPricingData.selectedResult;
	$scope.totalFare = $scope.pricingData.flightSearchResults[0].totFare;
	$scope.flightdetails = $scope.pricingData.flightSearchResults[0].segGrpList[0].segList[0];
	if($scope.pricingData.srchType == 2){
	$scope.flightdetails1 = $scope.pricingData.flightSearchResults[0].segGrpList[1].segList[0];	
		}
	
	$scope.InsuranceDetails = $scope.flightPricingData.insurancePlans;
	$rootScope.selectedIndex = JSON.parse(sessionStorage.getItem('flightIndex'));
		}
	/* end */
   $scope.goBack=function(path){
		$location.path(path);
	}
	/* openFlight function */
	
	$scope.openFlight = function(size, parentSelector){
	
	var parentElem = parentSelector ? 
		angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
		
		$scope.animationEnabled = true;
	    var modalInstance = $uibModal.open({
			animation: $scope.animationEnabled,
			ariaLabelledBy : 'modal-title',
			ariaDescribedBy : 'modal-body',
			templateUrl : 'flightDetailedinfoContent.html',
			controller : 'flightDetailedinfoContent',
			backdrop: true,
			size : size,
			appendTo : parentElem,
			resolve: {
				items: function(){
					return $scope.items
				}
			}
		})
		}
	/* end */
	
	$scope.extras = 0;
	$scope.getSelectedPlanId=function(selectedPlanId){
	sessionStorage.setItem('selectedInsurancePlanId',selectedPlanId);
	}
	 $scope.selectPlan = function(values, size, parentSelector){
		console.log("Values "+values);
		$rootScope.value = values;
		 $scope.palanContent = $scope.InsuranceDetails.availablePlans[values]
		$scope.extras = $scope.InsuranceDetails.availablePlans[values].totalFare
		var parentElem = parentSelector ? 
		angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
		//$rootScope.selectedIndex = index;
		$scope.animationEnabled = true;
	    var modalInstance = $uibModal.open({
			animation: $scope.animationEnabled,
			ariaLabelledBy : 'modal-title',
			ariaDescribedBy : 'modal-body',
			templateUrl : 'aboutInsurance.html',
			controller : 'insuranceCtrl',
			backdrop: true,
			size : size,
			appendTo : parentElem,
			resolve: {
				items: function(){
					return $scope.items
				}
			}
		})
		
		} 
}).controller('insuranceCtrl', function ($http, $scope,$location, $uibModalInstance, $rootScope, renderHTMLFactory, urlService) {
	
	var urlService = $location.absUrl().substring($location.absUrl().lastIndexOf("/") + 1, $location.absUrl().length);
	$scope.renderHTMLFactory = renderHTMLFactory;
	if(urlService == "flightDetails"){
		$scope.isureFlight = JSON.parse(sessionStorage.getItem('flightPriceData'));
   var index = $rootScope.value
   $scope.avaliablePlan = $scope.isureFlight.insurancePlans.availablePlans[index];
	} else if(urlService == "holidayFlightDetails") {
		$scope.holidaylightInst = JSON.parse(sessionStorage.getItem('selectedFliResp'));
		$scope.avaliablePlan = $scope.holidaylightInst.insurancePlans.availablePlans[index];
		}
   
	
    $scope.ok = function () {
        $uibModalInstance.close($scope.selected.item);
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
});