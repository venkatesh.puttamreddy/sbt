ouaApp.controller('holidayBookController', function ($scope, $http, $interval, $uibModal, $log, $document, Month, Year, ServerService, $timeout) {
	$('html, body').scrollTop(0);
	$scope.hotelResp = JSON.parse(sessionStorage.getItem('selectedHotResp'));
	$scope.flightResp = JSON.parse(sessionStorage.getItem('selectedFliResp'));
	$scope.reprice = JSON.parse(sessionStorage.getItem('repriseResponse'));
	$scope.hotelResp1 = $scope.hotelResp.searchResults[0];
	$scope.flightResult = $scope.flightResp.selectedResult
	$scope.selectedHotel = $scope.hotelResp.searchResults[0];
	
	$scope.hotelFare = $scope.reprice.repriceResponse.grandTotal;
	$scope.flightFare = $scope.flightResp.selectedResult.flightSearchResults[0].grndTotal
	$scope.totalFare = parseInt($scope.hotelFare) + parseInt($scope.flightFare);
	$scope.nationality1 = JSON.parse(localStorage.getItem('nationality'))
	$scope.nationality = JSON.stringify($scope.nationality1);
	
	$scope.month = Month;
	$scope.year = Year;
	
	/**
	@author Raghava Muramreddy
	to showing payment error message
	*/
	if(sessionStorage.getItem("paymentError")){
		var paymentError = JSON.parse(sessionStorage.getItem("paymentError"));
			Lobibox.alert('error',
			{
				msg: paymentError.errorMessage,
				callback: function(lobibox, type){
					if (type === 'ok'){
						sessionStorage.removeItem("paymentError");
					}
				}
			});
		
	}
	
	var roomIteration = $scope.reprice.repriceResponse.roomDetails;
	$scope.nonRefundFlag = false;
	for( var j=0; j<roomIteration.length; j++){
		$scope.nonRefund = roomIteration[j].nonRefundable;
		if ($scope.nonRefund == 'yes') {
			$scope.nonRefundFlag = true;
		}
	}
	
	/* signIn function */
	$scope.showSignIn = false;
	$scope.signIn = function(){
		$scope.showSignIn = $scope.showSignIn ? false : true;
		}
	
	/* end */
	
	/* multiple date */
	
	$timeout(function() {
			$('.adultTrvlr').each(function(index){
				jQuery(this).attr('id','adultTrvlr_'+(index+1));
			});
			$('.childTrvlr').each(function(index){
				jQuery(this).attr('id','childTrvlr_'+(index+1));
			});
			$('.infantTrvlr').each(function(index){
				jQuery(this).attr('id','infantTrvlr_'+(index+1));
			});
			$('.adatepicker').each(function(index){
				jQuery(this).attr('id','adatepicker'+(index+1));
			});
			$('.cdatepicker').each(function(index){
				jQuery(this).attr('id','cdatepicker'+(index+1));
			});
			$('.idatepicker').each(function(index){
				jQuery(this).attr('id','idatepicker'+(index+1));
			});
			
			$('[id*=adatepicker]').daterangepicker({ 
				singleDatePicker: true,
    showDropdowns: true,
    minYear: 1901,
    maxYear: parseInt(moment().format('YYYY'),10)
  }, function(start, end, label) {
    var years = moment().diff(start, 'years');
	$scope.age=years;
  jQuery(this).attr('data-age',years);
			});
			
			$('[id*=cdateOfBirth]').daterangepicker({ 
				
				singleDatePicker: true,
    showDropdowns: true,
    minYear: 1901,
    maxYear: parseInt(moment().format('YYYY'),10)
  }, function(start, end, label) {
    var years = moment().diff(start, 'years');
   
	jQuery(this).attr('data-age',years);
			});
			
			$('[id*=idateOfBirth]').daterangepicker({ 
				
				singleDatePicker: true,
    showDropdowns: true,
    minYear: 1901,
    maxYear: parseInt(moment().format('YYYY'),10)
  }, function(start, end, label) {
    var years = moment().diff(start, 'years');
   
	jQuery(this).attr('data-age',years);
			});
			
			}, 2000 );
		
		
	/* end */
	
	/* payNow function */
	$scope.payment = {}
	$scope.message = false;
	$scope.showCard = false;
	$scope.payNow = function(){
		var cardId = $('#cardNo').val();
		var firstDigit = cardId.charAt(0)
		var secondDigit = cardId.charAt(1)
		if(firstDigit == 3 && between(secondDigit,4,7) && cardId.length == 5){
			$scope.path = 'american';
			$scope.message = false;
			$scope.showCard = true;
			} else if(firstDigit == 3 && (secondDigit == 0 || secondDigit == 6 || secondDigit == 8) && cardId.length ==5){
			$scope.path = 'dinners';
			$scope.message = false;
			$scope.showCard = true;
			} else if(((firstDigit == 5 && between(secondDigit,1,5)) || (firstDigit == 2 && between(secondDigit,2,7)))&&cardId.length== 5){
			$scope.path = 'master';
			$scope.message = false;
			$scope.showCard = true;
			} else if( (firstDigit == 5 || firstDigit == 6) && (cardId.length == 5) ){
			$scope.path = "metro";
			$scope.message = false;
			$scope.showCard = true;
			} else if( (firstDigit == 4) && (cardId.length == 5) ){
			$scope.path = 'visa';
			$scope.message = false;
			$scope.showCard = true;
			} else if(cardId.length == 0){
			$scope.message = true;
			$scope.showCard = false;
		}
	}
	/* end */
	
	/* room based pass count */
	var occupancy = $scope.hotelResp.searchResults[0].occGroups
	$scope.adltCount = [];
	$scope.childCount = [];
	for(var i = 0; i<occupancy.length; i++){
		var roombean = occupancy[i].roomGrpBean.adultCount
		$scope.adltCount.push(roombean);
		
	}
	
	$scope.getNumber = function(num) {
		return new Array(num);   
	}
	/* end */
	
	
	
	/* $interval(function(){
		$('.adultTrvlr').each(function(index){
			jQuery(this).attr('id','adultTrvlr_'+(index+1));
		})
		
		 $('.adatepicker').each(function(index){
			jQuery(this).attr('id','adatepicker'+(index+1));
		}); 
		$('[id*=adatepicker]').focus(function(){
			$('.adatepicker').each(function(index){
				jQuery(this).attr('id','adatepicker'+(index+1));
			});
			if(jQuery(this).parents('#pasadd').find('.modal').is(":hidden"))
			{
				$('.ui-datepicker').appendTo($('body'));
			}
			
		});
		
		$('[id*=adatepicker]').daterangepicker({
		singleDatePicker: true,
		showDropdowns: true,
		format: 'dd/mm/yyyy',
		minYear: 1901,
		maxYear: parseInt(moment(),10)
		}, function(start, end, label) {
		var years = moment().diff(start, 'years');
		alert("You are " + years + " years old!");
		})
		}, 2000)   */
		
		
	
	/* end */
	
	$scope.roomIndex = JSON.parse(sessionStorage.getItem('roomIndex'));
	var parentIndex = $scope.roomIndex.parentIndex;
	var childIndex = $scope.roomIndex.childIndex;
	
	/* payment function */
	$scope.selectDoc = {}
	$scope.bookingReq = {}
	$scope.specialServiceRequest = {}
	$scope.selectDoc.dockNo = null;
	
	var traveller = {};
	var expireDate = {}
	$scope.paymentDetail = {}
	$scope.payment = function(bookingWay){
	expireDate = $scope.paymentDetail.expireYear+$scope.paymentDetail.expireMonth
		$scope.travellerdatalist = [];
	console.log("paymentDetail "+JSON.stringify($scope.paymentDetail))
	
		$('.adultTrvlr').each(function(index){
			jQuery(this).attr('id', 'adultTrvlr_'+(index+1))
			});
			jQuery('.traveller').each(function(ind){
			
				var id = $(this).attr('id');
				var passId = id.split('_');
				
				if(passId[0] == 'adultTrvlr'){
					var salutationVal = $(this).find('#salutation_'+ind).val();
				var fname = $(this).find('#afname_'+ind).val();
				var lnameVal = $(this).find('#alname_'+ind).val();
				var datepickerVal = $(this).find('#adateOfBirth_'+ind).val();
				var c_codeVal = '+91'
				var c_noVal = $(this).find('#aphone_'+ind).val();
				var emailid = $(this).find('#amail_'+ind).val();
				var nationalityVal = $(this).find('#anationalityVal_'+ind).val();
				var passport = $(this).find('#apassport_'+ind).val();
					}
					var personalObj = new Object();
					var travellerdataObj = new Object();
					if(salutationVal == 'Mr'){
					personalObj.gender = 'M'
				} else {
				personalObj.gender = 'F'
				}
				
				personalObj.salutation = salutationVal;
				personalObj.fname = fname;
				personalObj.lname = lnameVal;
				personalObj.dob = datepickerVal;
				personalObj.mobcntrycode = c_codeVal;
				personalObj.mobile = c_noVal;
				personalObj.email = emailid;
				personalObj.nationalitycode = nationalityVal;
				personalObj.passportNo = passport;
				

				travellerdataObj.personaldata = personalObj;
				travellerdataObj.agntid = 0;
		travellerdataObj.profileid = "-0";
		travellerdataObj.travellerRefRPH = "A1";
				$scope.travellerdatalist.push(travellerdataObj)
				
				});
				
				if(bookingWay == 'Payment'){
		$scope.bookingReq.payfortTxnData = {
							"3ds_url": null,
							"access_code": null,
							"amount": null,
							"authorization_code": null,
							"card_bin": null,
							"card_number": $scope.paymentDetail.cardNo,
							"card_security_code": $scope.paymentDetail.cvv,
							"command": null,
							"currency": 'OMR',
							"customer_email": null,
							"customer_ip": null,
							"customer_name": $scope.paymentDetail.cardName,
							"eci": null,
							"expiry_date": expireDate,
							"fort_id": null,
							"language": null,
							"merchant_identifier": null,
							"merchant_reference": null,
							"order_description": null,
							"payment_option": $scope.path,
							"response_code": null,
							"response_message": null,
							"return_url": null,
							"service_command": "TOKENIZATION",
							"device_fingerprint": $('#device_fingerprint').val(),
							"signature": null,
							"status": null,
							"token_name": null,
							"postUrl": null
		}
		} else if(bookingWay == 'NonPayment'){
			$scope.bookingReq.payfortTxnData = null;
			}
			
			/* FOR HOTEL */
			var bookingReqObj = new Object();
			if($scope.nonRefundFlag == false){
				bookingReqObj.reqCommand = 3;
			} else if($scope.nonRefundFlag == true){
			bookingReqObj.reqCommand = 1;
			}
			bookingReqObj.htlName = $scope.selectedHotel.name;
			bookingReqObj.htlCode = $scope.selectedHotel.htlCode;
			bookingReqObj.zcode = $scope.selectedHotel.zcode;
			bookingReqObj.address = $scope.selectedHotel.address;
			bookingReqObj.city = $scope.hotelResp.city;
			bookingReqObj.cityCode = $scope.selectedHotel.cityCode;
			bookingReqObj.country = $scope.selectedHotel.country;
			bookingReqObj.phNo = $scope.selectedHotel.phNo;
			bookingReqObj.htlDesc = $scope.selectedHotel.htlDesc;
			bookingReqObj.htlName = $scope.selectedHotel.starRating;
			bookingReqObj.provider = $scope.selectedHotel.provider;
			bookingReqObj.latitude = $scope.selectedHotel.latitude;
			bookingReqObj.longitude = $scope.selectedHotel.longitude;
			bookingReqObj.currency = $scope.selectedHotel.currency;
			bookingReqObj.comments = $scope.selectedHotel.comments;
			bookingReqObj.status = $scope.selectedHotel.status;
			bookingReqObj.checkInTime = $scope.selectedHotel.checkInTime;
			bookingReqObj.checkOutTime = $scope.selectedHotel.checkOutTime;
			bookingReqObj.totalRooms = $scope.hotelResp.totalRooms;
			bookingReqObj.totalPax = $scope.hotelResp.totalPaxs;
			bookingReqObj.providerCur = $scope.selectedHotel.providerCur;
			bookingReqObj.convRate = $scope.selectedHotel.convRate;
			bookingReqObj.grossFare = $scope.hotelFare;
			//bookingReqObj.location = $scope.selectedHotel.latitude;
			bookingReqObj.checkIn = $scope.hotelResp.checkInDate;
			bookingReqObj.checkOut = $scope.hotelResp.checkOutDate;
			bookingReqObj.offers = $scope.selectedHotel.offer;
			bookingReqObj.offerDesc = $scope.selectedHotel.offerDesc;
			bookingReqObj.offerDis = $scope.selectedHotel.offerDis;
			bookingReqObj.grsWithOutDiscount = $scope.selectedHotel.grsWithOutDiscount;
			bookingReqObj.nationality = $scope.selectedHotel.nationality;
			bookingReqObj.totalFare = $scope.reprice.repriceResponse.totalFare;
			bookingReqObj.duration = $scope.selectedHotel.duration;
			bookingReqObj.servChrg = $scope.selectedHotel.servChrg;
			bookingReqObj.rawGrandTotal = $scope.reprice.repriceResponse.rawGrandTotal;
			bookingReqObj.discount = $scope.selectedHotel.discount;
			bookingReqObj.markupAmount = $scope.selectedHotel.markupAmount;
			bookingReqObj.appMode = 2;
			
			var roomCategories = new Array();
			var roomObj = new Object();
			roomObj.status = $scope.selectedHotel.occGroups[parentIndex].roomInfos[childIndex].status;
			 roomObj.roomRefNo = $scope.selectedHotel.occGroups[parentIndex].roomInfos[childIndex].roomRefNo;
			roomObj.roomCode = $scope.selectedHotel.occGroups[parentIndex].roomInfos[childIndex].roomCode;
			roomObj.roomName = $scope.selectedHotel.occGroups[parentIndex].roomInfos[childIndex].roomName;
			roomObj.roomCheckIn = $scope.selectedHotel.occGroups[parentIndex].roomInfos[childIndex].roomCheckIn;
			roomObj.roomCheckOut = $scope.selectedHotel.occGroups[parentIndex].roomInfos[childIndex].roomCheckOut;
			roomObj.roomType = $scope.selectedHotel.occGroups[parentIndex].roomInfos[childIndex].roomType;
			roomObj.roomTypeChar = $scope.selectedHotel.occGroups[parentIndex].roomInfos[childIndex].roomTypeChar;
			roomObj.rateBasis = $scope.selectedHotel.occGroups[parentIndex].roomInfos[childIndex].rateBasis;
			roomObj.rateBasisCode = $scope.selectedHotel.occGroups[parentIndex].roomInfos[childIndex].rateBasisCode;
			roomObj.mealBoard = $scope.selectedHotel.occGroups[parentIndex].roomInfos[childIndex].mealBoard;
			roomObj.mealBoardDesc = $scope.selectedHotel.occGroups[parentIndex].roomInfos[childIndex].mealBoardDesc;
			roomObj.adultCount = $scope.selectedHotel.occGroups[parentIndex].roomInfos[childIndex].adultCount;
			roomObj.childCount = $scope.selectedHotel.occGroups[parentIndex].roomInfos[childIndex].childCount;
			roomObj.infantCount = $scope.selectedHotel.occGroups[parentIndex].roomInfos[childIndex].infantCount;
			roomObj.xtrBeds = $scope.selectedHotel.occGroups[parentIndex].roomInfos[childIndex].xtrBeds;
			roomObj.roomFare = $scope.selectedHotel.occGroups[parentIndex].roomInfos[childIndex].roomFare;
			roomObj.servChrg = $scope.selectedHotel.occGroups[parentIndex].roomInfos[childIndex].servChrg;
			roomObj.discount = $scope.selectedHotel.occGroups[parentIndex].roomInfos[childIndex].discount;
			roomObj.childAges = $scope.selectedHotel.occGroups[parentIndex].roomInfos[childIndex].childAges;
			roomObj.cancellationPolicy = $scope.selectedHotel.occGroups[parentIndex].roomInfos[childIndex].cancellationPolicy;
			
			if($scope.reprice.repriceResponse.roomDetails[0].psngrNamesRequird!=0) {
				roomObj.psngrNamesRequird = $scope.reprice.repriceResponse.roomDetails[0].psngrNamesRequird;
			} 
			
			  var validRoomOccupancy = new Object();
			if ($scope.reprice.repriceResponse.roomDetails[0].validRoomOccupancy != null) {					
				validRoomOccupancy.runno = $scope.reprice.repriceResponse.roomDetails[0].validRoomOccupancy.runno;
				validRoomOccupancy.roomCode = $scope.reprice.repriceResponse.roomDetails[0].validRoomOccupancy.roomCode;
				validRoomOccupancy.adult = $scope.reprice.repriceResponse.roomDetails[0].validRoomOccupancy.adult;
				validRoomOccupancy.children = $scope.reprice.repriceResponse.roomDetails[0].validRoomOccupancy.children;
				validRoomOccupancy.childrenAges = $scope.reprice.repriceResponse.roomDetails[0].validRoomOccupancy.childrenAges;
				validRoomOccupancy.extraBed = $scope.reprice.repriceResponse.roomDetails[0].validRoomOccupancy.extraBed;
				validRoomOccupancy.extraBedOccupant = $scope.reprice.repriceResponse.roomDetails[0].validRoomOccupancy.extraBedOccupant;
				roomObj.validRoomOccupancy = validRoomOccupancy;
				} else {										
				
				validRoomOccupancy.runno = 'null';
				validRoomOccupancy.roomCode = 'null';
				validRoomOccupancy.children = 'null';
				validRoomOccupancy.childrenAges = 'null';
				validRoomOccupancy.extraBed = 'null';
				validRoomOccupancy.extraBedOccupant = 'null';
				
				roomObj.validRoomOccupancy = validRoomOccupancy;					
			}  
			
			 roomObj.allocations = $scope.reprice.repriceResponse.roomDetails[0].allocations;
			roomObj.amendmentPolicy = $scope.selectedHotel.occGroups[parentIndex].roomInfos[childIndex].amendmentPolicy;
			roomObj.uniqueId = $scope.selectedHotel.occGroups[parentIndex].roomInfos[childIndex].uniqueId;
			roomObj.alertMsg = $scope.selectedHotel.occGroups[parentIndex].roomInfos[childIndex].alertMsg;
			roomObj.shrdBdng = $scope.selectedHotel.occGroups[parentIndex].roomInfos[childIndex].sharedBdng;
			roomObj.grossWithOutDis = $scope.selectedHotel.occGroups[parentIndex].roomInfos[childIndex].grsWithOutDiscount;
			roomObj.offerCode = $scope.selectedHotel.occGroups[parentIndex].roomInfos[childIndex].offerCode;
			roomObj.comments = $scope.selectedHotel.occGroups[parentIndex].roomInfos[childIndex].roomComments;
			roomObj.bkngId = $scope.selectedHotel.occGroups[parentIndex].roomInfos[childIndex].bkngId;
			roomObj.cancellationChrgCode = $scope.selectedHotel.occGroups[parentIndex].roomInfos[childIndex].cancellationChrgCode;
			roomObj.priceCode = $scope.selectedHotel.occGroups[parentIndex].roomInfos[childIndex].priceCode;
			roomObj.offerDis = $scope.selectedHotel.occGroups[parentIndex].roomInfos[childIndex].offerDiscount;
			roomObj.pymntGrndBy = $scope.selectedHotel.occGroups[parentIndex].roomInfos[childIndex].paymentGuaranteedBy;
			roomObj.srvChrgType = $scope.selectedHotel.occGroups[parentIndex].roomInfos[childIndex].servChrgType;
			roomObj.discChrgType = $scope.selectedHotel.occGroups[parentIndex].roomInfos[childIndex].discChrgType;
			roomObj.markupAmount = $scope.selectedHotel.occGroups[parentIndex].roomInfos[childIndex].markupAmount;
			roomObj.seqNo = $scope.selectedHotel.occGroups[parentIndex].roomInfos[childIndex].seqNo;
			if($scope.selectedHotel.occGroups[parentIndex].roomInfos[childIndex].servChrgKeys!=null) {
				var servChrgKeys = new Array();
				var servChrgKeyLength = $scope.selectedHotel.occGroups[parentIndex].roomInfos[childIndex].servChrgKeys.length;
				for(var s = 0; s<servChrgKeyLength; s++) {
					servChrgKeys.push($scope.selectedHotel.occGroups[parentIndex].roomInfos[childIndex].servChrgKeys[s]);
				}
				roomObj.servChrgKeys = servChrgKeys;
			} 
			
			roomObj.paxDtls = [];
				roomObj.paxDtls[0] = $scope.travellerdatalist;
				
			roomCategories.push(roomObj);
			bookingReqObj.docketNo = $scope.selectDoc.dockNo;
		bookingReqObj.roomCategories = roomCategories;
		bookingReqObj.payfortTxnData = {
			"3ds_url": null,
			"access_code": null,
			"amount": null,
			"authorization_code": null,
			"card_bin": null,
			"card_number": $scope.paymentDetail.cardNo,
			"card_security_code": $scope.paymentDetail.cvv,
			"command": null,
			"currency": 'OMR',
			"customer_email": null,
			"customer_ip": null,
			"customer_name": $scope.paymentDetail.cardName,
			"eci": null,
			"expiry_date": expireDate,
			"fort_id": null,
			"language": null,
			"merchant_identifier": null,
			"merchant_reference": null,
			"order_description": null,
			"payment_option": $scope.path,
			"response_code": null,
			"response_message": null,
			"return_url": null,
			"service_command": "TOKENIZATION",
			"device_fingerprint": $('#device_fingerprint').val(),
			"signature": null,
			"status": null,
			"token_name": null,
			"postUrl": null
		};
			/* END */
			
			$scope.bookingReq.docketNo = $scope.selectDoc.dockNo;
		$scope.bookingReq.specialServiceRequest = $scope.specialServiceRequest;
		$scope.bookingReq.flightsearchresult = $scope.flightResp.selectedResult;
		
		
		$scope.bookingReq.travellerdatalist = $scope.travellerdatalist;
	
		$scope.flightHotelReq = {}
		$scope.flightHotelReq.grandTotal = $scope.totalFare;
		$scope.flightHotelReq.flightBookCriteria = $scope.bookingReq;
		$scope.flightHotelReq.hotelBookCriteria = bookingReqObj;
		$scope.flightHotelReq.payfortTxnData = bookingReqObj.payfortTxnData;
		/* $scope.flightHotelReq.cardName = $scope.paymentDetail.cardName;
		$scope.flightHotelReq.cardNo = $scope.paymentDetail.cardNo;
		$scope.flightHotelReq.cvv = $scope.paymentDetail.cvv;
		$scope.flightHotelReq.expireMonth = $scope.paymentDetail.expireMonth;
		$scope.flightHotelReq.expireYear = $scope.paymentDetail.expireYear; */
		
		$http.get(ServerService.serverPath+'rest/validate/session').then(function(response){
			if(response.data.isSessionValid == true){
				$http.post(ServerService.serverPath+'rest/fltNhtl/booking/confirm',JSON.stringify($scope.flightHotelReq),{
				headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
				}).then(function(response){
					$scope.loading = false;
			$scope.disabled = false;
			if(response.data.status=='SUCCESS'){
					window.location.href=response.data.returnUrl;
			    }else if(response.data.status=='FAILURE') {
							
							Lobibox.alert('error',
								{
									msg: response.data.message
							});
				}else{
						
							Lobibox.alert('error',
						{
							msg: 'Unexpected Error Occurred, Please Try Again Later'
						});
					}
			
					})
				}
			})
	}
	
	
	
    $scope.getNumber = function (num) {
        return new Array(num);
	}
    
   /*  $http.get("json/flightReview.json")
	.then(function (response) {
		$scope.flightList = response.data;
	});
    $http.get("json/hotelResultFilter.json")
	.then(function (response) {
		$scope.filterOptions = response.data;
	});
    $http.get("json/hotelResultList.json")
	.then(function (response) {
   $scope.hotelList = response.data; */
	//});
    $scope.hotelDate = {
        startDate: moment(),
        endDate: moment(),
        locale: {
            format: "D-MMM-YY"
		}
	};
    $scope.range = function(n) {
        n = Number(n);
        return new Array(n);
	};
    
    $scope.myInterval = 5000;
    $scope.thumbnailSize = 5;
    $scope.thumbnailPage = 1;
    $scope.slides = [{
        image: "images/hotel/h1.jpeg"
		}, {
        image: "images/hotel/h2.jpeg"
		}, {
        image: "images/hotel/h3.jpeg"
		}, {
        image: "images/hotel/h4.jpeg"
		}, {
        image: "images/hotel/h5.jpeg"
		}, {
        image: "images/hotel/h6.jpeg"
		}, {
        image: "images/hotel/h7.jpeg"
		}, {
        image: "images/hotel/h8.jpeg"
	}];
	
    $scope.prevPage = function () {
        if ($scope.thumbnailPage > 1) {
            $scope.thumbnailPage--;
		}
        $scope.showThumbnails = $scope.slides.slice(($scope.thumbnailPage - 1) * $scope.thumbnailSize, $scope.thumbnailPage * $scope.thumbnailSize);
	}
	
    $scope.nextPage = function () {
        if ($scope.thumbnailPage <= Math.floor($scope.slides.length / $scope.thumbnailSize)) {
            $scope.thumbnailPage++;
		}
        $scope.showThumbnails = $scope.slides.slice(($scope.thumbnailPage - 1) * $scope.thumbnailSize, $scope.thumbnailPage * $scope.thumbnailSize);
	}
	
    $scope.showThumbnails = $scope.slides.slice(($scope.thumbnailPage - 1) * $scope.thumbnailSize, $scope.thumbnailPage * $scope.thumbnailSize);
    $scope.setActive = function (idx) {
        $scope.slides[idx].active = true;
	}
	
    $scope.reviews = [{
        "comment": "Good in general",
        "rating": "7.8",
        "rateValue": "Very Good",
        "reviewer": "Hisham",
        "location": "Shj"
		}, {
        "comment": "Good in general",
        "rating": "7.2",
        "rateValue": "Very Good",
        "reviewer": "Nijamuddin",
        "location": "Chennai"
	}];
	
    $scope.roomsLists = [{
        "title": "Luxury Room",
        "img": "images/hotel/13461.jpg",
        "description": ["Luxury Rooms are warm and elegant accommodations boasting views of the city.", "37 sqm - King size / twin bed (s) - Wi-Fi access - Air conditioned - Smart LED television - BOSE wave music system - Mini-bar - Safe - Hairdryer - bathrobe & slippers - Bath and shower"],
        rooms: [{
            "name": "Room Only",
            "fare": "1,888.00"
			}, {
            "name": "Bed And Breakfast",
            "fare": "1,935.00"
		}]
		}, {
        "title": "Luxury Room Park View",
        "img": "images/hotel/13462.jpg",
        "description": ["Luxury Rooms are warm and elegant high floor accommodations showcasing views of the park.", "37 sqm - King size / twin bed (s) - Wi-Fi access - Air conditioned - Smart LED television - BOSE wave music system - Mini-bar - Safe - Hairdryer - bathrobe & slippers - Bath and shower"],
        rooms: [{
            "name": "Room Only",
            "fare": "2,165.00"
			}, {
            "name": "Bed And Breakfast",
            "fare": "2,211.00"
		}]
	}];
    //hotelRoomsList
    var hotelRoomsList = angular.element(document.getElementById('hotelRoomsList'));
    $scope.toHotelRoomsList = function () {
        $document.scrollTo(hotelRoomsList, 30, 1000);
	}
	
	/* openFlightInfo function */
	$scope.openFlightInfo = function(size, parentSelector){
		var parentElem = parentSelector ?
		angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
        $uibModal.open({
            animation: $scope.animationsEnabled,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'flightInfo.html',
            controller: 'flightInfoCtrl',
            size: size,
            appendTo: parentElem
		});
	}
	/* end */
});
ouaApp.controller('flightInfoCtrl', function ($http, $scope, $uibModalInstance) {
	$scope.flightsearchResInfo = JSON.parse(sessionStorage.getItem('selectedFliResp'));
	$scope.flightsearchResdetails = $scope.flightsearchResInfo.selectedResult.flightSearchResults[0];
	
	
    $scope.ok = function () {
	$uibModalInstance.close($scope.selected.item);
    };
	
    $scope.cancel = function () {
	$uibModalInstance.dismiss('cancel');
    };
	});