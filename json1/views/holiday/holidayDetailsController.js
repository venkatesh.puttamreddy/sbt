ouaApp.controller('holidayDetailsController', function ($scope, $http, $uibModal, $log, $document, $location, urlService, renderHTMLFactory, ServerService) {
	 $('html, body').scrollTop(0);
	 $scope.hotelResp = JSON.parse(sessionStorage.getItem('selectedHotResp'));
	$scope.flightResp = JSON.parse(sessionStorage.getItem('selectedFliResp'));
	$scope.flightResult = $scope.flightResp.selectedResult;
	$scope.hotelPlusSearchResp = JSON.parse(sessionStorage.getItem('hotelPlusSearchResp'));  
	
	$scope.urlService = urlService.relativePath;
	
	$scope.renderHTMLFactory = renderHTMLFactory;
	
	$scope.hotelResult = $scope.hotelResp.searchResults[0];
	
	/* openFlightInfo function */
	$scope.openFlightInfo = function(size, parentSelector){
		var parentElem = parentSelector ?
		angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
        $uibModal.open({
            animation: $scope.animationsEnabled,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'flightInfo.html',
            controller: 'flightInfoCtrl',
            size: size,
            appendTo: parentElem
		});
	}
	/* end */
	//(date2 - date1) / 1000 / 60 / 60 / 24
	var noNight =  parseInt($scope.hotelPlusSearchResp.checkOutDate) -  parseInt($scope.hotelPlusSearchResp.checkInDate)
	/* onSelect function */
	$scope.loading = false;
	$scope.disabled = false;
	var roonList = [];
	var repriceObj = new Object();
	$scope.clsObj = {}
	$scope.onSelect = function(parent, child){
		$scope.loading = true;
		$scope.disabled = true;
		console.log('parent '+parent)
		console.log('child '+child)
		$scope.clsObj.parentIndex = parent;
		$scope.clsObj.childIndex = child;
		sessionStorage.setItem('roomIndex', JSON.stringify($scope.clsObj));
		$http.get(ServerService.serverPath+'rest/validate/session').then(function(response){
			
			if(response.data.isSessionValid == true){
				
				repriceObj.city = $scope.hotelResp.searchResults[0].cityCode;
				repriceObj.hotelCode = $scope.hotelResp.searchResults[0].htlCode;
				repriceObj.checkIn = $scope.hotelResp.checkInDate;
				repriceObj.checkOut = $scope.hotelResp.checkOutDate;
				repriceObj.duration = noNight;
				repriceObj.provider = $scope.hotelResp.searchResults[0].provider;
				repriceObj.currency = $scope.hotelResp.currency; 
				repriceObj.convRate = $scope.hotelResp.searchResults[0].convRate; 
				repriceObj.servChrg = $scope.hotelResp.searchResults[0].servChrg; 
				repriceObj.discount = $scope.hotelResp.searchResults[0].discount; 
				repriceObj.markupAmount = $scope.hotelResp.searchResults[0].markupAmount;
				var rmdtlsObj = {};
				rmdtlsObj.roomId = $scope.hotelResp.searchResults[0].occGroups[parent].roomInfos[child].roomRefNo;
				rmdtlsObj.roomCode = $scope.hotelResp.searchResults[0].occGroups[parent].roomInfos[child].roomCode;
				rmdtlsObj.noOfRooms = $scope.hotelResp.searchResults[0].occGroups[parent].roomGrpBean.roomCount;
				rmdtlsObj.adultCount = $scope.hotelResp.searchResults[0].occGroups[parent].roomGrpBean.adultCount;
				rmdtlsObj.childCount = $scope.hotelResp.searchResults[0].occGroups[parent].roomGrpBean.childCount+$scope.hotelResp.searchResults[0].occGroups[parent].roomGrpBean.infantCount;
				rmdtlsObj.xtraBed = $scope.hotelResp.searchResults[0].occGroups[parent].roomInfos[child].xtraBeds;
				rmdtlsObj.rateBasis = $scope.hotelResp.searchResults[0].occGroups[parent].roomInfos[child].rateBasisCode;
				//rmdtlsObj.nationality = $scope.search.nationality;
				//rmdtlsObj.countryOfResidence = $scope.search.countryOfResidence;
				rmdtlsObj.allocations = $scope.hotelResp.searchResults[0].occGroups[parent].roomInfos[child].allocations;
				rmdtlsObj.roomFare = $scope.hotelResp.searchResults[0].occGroups[parent].roomInfos[child].roomFare;
				rmdtlsObj.cancellationChrgCode = $scope.hotelResp.searchResults[0].occGroups[parent].roomInfos[child].cancellationChrgCode;
				
				if($scope.hotelResp.searchResults[0].occGroups[parent].roomInfos[child].servChrgKeys){
					var servChrgKeys = new Array();
					var servChrgKeyLength = $scope.hotelResp.searchResults[0].occGroups[parent].roomInfos[child].servChrgKeys.length
					for(var s=0; s<servChrgKeyLength; s++){
						servChrgKeys.push($scope.hotelResp.searchResults[0].occGroups[parent].roomInfos[child].servChrgKeys[i]);
					}
					rmdtlsObj.servChrgKeys = servChrgKeys;
				}
				
				if($scope.hotelResp.searchResults[0].occGroups[parent].roomGrpBean.childAges!=null){
					var childAges = new Array();
					var childAgeLength;
					if($scope.hotelResp.searchResults[0].occGroups[parent].roomGrpBean.childAges.length){
						childAgeLength = $scope.hotelResp.searchResults[0].occGroups[parent].roomGrpBean.childAges.split(',').length;
						}else {
						childAgeLength = $scope.hotelResp.searchResults[0].occGroups[parent].roomGrpBean.childAges.length;
					}
					for(var c=0; c<childAgeLength; c++){
						childAges.push($scope.hotelResp.searchResults[0].occGroups[parent].roomGrpBean.childAges.split(',')[c]);
					}
					rmdtlsObj.childAges = childAges;
				}
				
				roonList.push(rmdtlsObj);
				repriceObj.roomDtls = roonList;
				$scope.repriceObj = repriceObj;
				
				
				$http.post(ServerService.serverPath+'rest/hotel/reprice',$scope.repriceObj, {
					headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
					}).then(function(response){
					
					$scope.loading = false;
					$scope.disabled = false;
					$scope.repriseResponse = response.data.data;
					sessionStorage.setItem('repriseResponse',JSON.stringify($scope.repriseResponse));
					$location.path('/holidayReview');
				})
				
			} 
		})
		
	
	
	}
	
	/* end */
	
	$http.get("json/flightReview.json")
	.then(function (response) {
	$scope.flightList = response.data;
	});
    $http.get("json/hotelResultFilter.json")
	.then(function (response) {
	$scope.filterOptions = response.data;
	});
    $http.get("json/hotelResultList.json")
	.then(function (response) {
	$scope.hotelList = response.data;
	});
    $scope.hotelDate = {
	startDate: moment(),
	endDate: moment(),
	locale: {
	format: "D-MMM-YY"
	}
    };
    $scope.range = function(n) {
	n = Number(n);
	return new Array(n);
    };
    
    $scope.myInterval = 5000;
    $scope.thumbnailSize = 5;
    $scope.thumbnailPage = 1;
    $scope.slides = [{
	image: "images/hotel/h1.jpeg"
	}, {
	image: "images/hotel/h2.jpeg"
	}, {
	image: "images/hotel/h3.jpeg"
	}, {
	image: "images/hotel/h4.jpeg"
	}, {
	image: "images/hotel/h5.jpeg"
	}, {
	image: "images/hotel/h6.jpeg"
	}, {
	image: "images/hotel/h7.jpeg"
	}, {
	image: "images/hotel/h8.jpeg"
	}];
	
    $scope.prevPage = function () {
	if ($scope.thumbnailPage > 1) {
	$scope.thumbnailPage--;
	}
	$scope.showThumbnails = $scope.slides.slice(($scope.thumbnailPage - 1) * $scope.thumbnailSize, $scope.thumbnailPage * $scope.thumbnailSize);
    }
	
    $scope.nextPage = function () {
	if ($scope.thumbnailPage <= Math.floor($scope.slides.length / $scope.thumbnailSize)) {
	$scope.thumbnailPage++;
	}
	$scope.showThumbnails = $scope.slides.slice(($scope.thumbnailPage - 1) * $scope.thumbnailSize, $scope.thumbnailPage * $scope.thumbnailSize);
    }
	
    $scope.showThumbnails = $scope.slides.slice(($scope.thumbnailPage - 1) * $scope.thumbnailSize, $scope.thumbnailPage * $scope.thumbnailSize);
    $scope.setActive = function (idx) {
	$scope.slides[idx].active = true;
    }
	
    $scope.reviews = [{
	"comment": "Good in general",
	"rating": "7.8",
	"rateValue": "Very Good",
	"reviewer": "Hisham",
	"location": "Shj"
    }, {
	"comment": "Good in general",
	"rating": "7.2",
	"rateValue": "Very Good",
	"reviewer": "Nijamuddin",
	"location": "Chennai"
    }];
	
    $scope.roomsLists = [{
	"title": "Luxury Room",
	"img": "images/hotel/13461.jpg",
	"description": ["Luxury Rooms are warm and elegant accommodations boasting views of the city.", "37 sqm - King size / twin bed (s) - Wi-Fi access - Air conditioned - Smart LED television - BOSE wave music system - Mini-bar - Safe - Hairdryer - bathrobe & slippers - Bath and shower"],
	rooms: [{
	"name": "Room Only",
	"fare": "1,888.00"
	}, {
	"name": "Bed And Breakfast",
	"fare": "1,935.00"
	}]
    }, {
	"title": "Luxury Room Park View",
	"img": "images/hotel/13462.jpg",
	"description": ["Luxury Rooms are warm and elegant high floor accommodations showcasing views of the park.", "37 sqm - King size / twin bed (s) - Wi-Fi access - Air conditioned - Smart LED television - BOSE wave music system - Mini-bar - Safe - Hairdryer - bathrobe & slippers - Bath and shower"],
	rooms: [{
	"name": "Room Only",
	"fare": "2,165.00"
	}, {
	"name": "Bed And Breakfast",
	"fare": "2,211.00"
	}]
    }];
    //hotelRoomsList
    var hotelRoomsList = angular.element(document.getElementById('hotelRoomsList'));
    $scope.toHotelRoomsList = function () {
	$document.scrollTo(hotelRoomsList, 30, 1000);
    }
	});
	ouaApp.controller('flightInfoCtrl', function ($http, $scope, $uibModalInstance) {
	$scope.flightsearchResInfo = JSON.parse(sessionStorage.getItem('selectedFliResp'));
	$scope.flightsearchResdetails = $scope.flightsearchResInfo.selectedResult.flightSearchResults[0];
	
	
    $scope.ok = function () {
	$uibModalInstance.close($scope.selected.item);
    };
	
    $scope.cancel = function () {
	$uibModalInstance.dismiss('cancel');
    };
	});	