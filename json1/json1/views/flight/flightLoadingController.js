ouaApp.controller('flightLoading', function($scope,$http, $location, $timeout, ServerService){

$('html, body').scrollTop(0);
$scope.searching = "flight"
$scope.searchFlightObj = JSON.parse(sessionStorage.getItem('searchFlightObj'));
$scope.message = "Searching For Available Flight";
  $http.post(ServerService.serverPath+'rest/flight/search', JSON.stringify($scope.searchFlightObj),{ 
 /* $http.post('http://192.168.1.106:8080/rest/flight/search', JSON.stringify($scope.searchFlightObj),{ */
			headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
			}).then(function(response){
			$scope.loading = false;
			$scope.disabled = false;
			if(response.data.status == 'SUCCESS'){
				sessionStorage.setItem('flightTotalData',JSON.stringify(response.data));
				$location.path('/flightResult');
				
			} else {
			Lobibox.alert('error',{
					msg: response.data.errorMessage
				})
			$timeout(function(){
					$location.path('/')
					}, 1000);
			}
			
		});
});

ouaApp.controller('hotelLoadingCtrl', function($scope,$http, $location, $timeout, ServerService){

$('html, body').scrollTop(0);
$scope.searching = "hotel"
$scope.hotelRequest = JSON.parse(sessionStorage.getItem('hotelRequestObj'));
$scope.message = "Searching For Available Hotel";
 $http.post(ServerService.serverPath+'rest/hotel/search/city',$scope.hotelRequest,{
			headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
			}).then(function(response){
			$scope.loading = false;
			$scope.disabled = false;
			if(response.data.status == 'SUCCESS'){
				$scope.hotelSearchResp = response.data.data;
				sessionStorage.setItem('hotelSearchResp',JSON.stringify($scope.hotelSearchResp));
				$location.path('/hotelResult')
				} else {
				Lobibox.alert('error',{
					msg: response.data.errorMessage
				})
				$timeout(function(){
					$location.path('/')
					}, 1000);
			}
		})
});

ouaApp.controller('flightHotelController', function($scope,$http, $location, $interval, $timeout, ServerService){
$('html, body').scrollTop(0);

$scope.searching = "holiday"
$scope.hotelPlusRequest = JSON.parse(sessionStorage.getItem('hotelPlusRequest'));
$scope.flightPlusRequest = JSON.parse(sessionStorage.getItem('flightPlusRequest'));

$scope.message = "Searching For Available Flight and Hotel Option";

$scope.totalDisplayed = 3;
		$interval(function(){
			$scope.totalDisplayed += 3;
		}, 1000);
		$scope.totalDisplayed1 = 30;
		$interval(function(){ if( $scope.totalDisplayed1 >0 ) { $scope.totalDisplayed1 -= 1; } else { $scope.totalDisplayed1 = 10 }},1000)
		$scope.hotelStatus = 'PENDING'; $scope.flightStatus = 'PENDING';
		$scope.flightErr = false; $scope.hotelErr = false;
 $http.post(ServerService.serverPath+'rest/hotel/search/city',JSON.stringify($scope.hotelPlusRequest),{
			headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
			
			}).then(function(response){
			console.log(response);
			$scope.loading = false;
					$scope.disabled = false;
			if(response.data.status == 'SUCCESS' && !$scope.flightErr){
				console.log(response.data.data);
				sessionStorage.setItem('hotelPlusSearchResp', JSON.stringify(response.data.data));
				$scope.hotelStatus = 'SUCCESS'
				} else {
				$scope.hotelErr = true;
				if(!$scope.flightErr || response.data.data==null) {
					Lobibox.alert('error',
					{
					msg: response.data.errorMessage
					});
					$timeout(function(){
					$location.path('/')
					}, 1000);
					
				}  
			}
		}); 
		
		$http.post(ServerService.serverPath+'rest/flight/search',JSON.stringify($scope.flightPlusRequest),{
			headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
			}).then(function(response){
			checkCondnt ();
			function flightSuccess(){
				if(response.data.status == "SUCCESS" && !$scope.hotelErr && sessionStorage.getItem('hotelPlusSearchResp')){
					$scope.loading = false;
					$scope.disabled = false;
					sessionStorage.setItem('flightplusSearchResp', JSON.stringify(response.data.data));
					$location.path('/holidaySearchResult');
					$scope.flightStatus = 'SUCCESS';
					} else {
					$scope.loading = false;
					$scope.disabled = false;
					$scope.flightErr = true;
					//$scope.flightStatus = 'FAILURE';
					if(!$scope.hotelErr) {
						Lobibox.alert('error',
						{
							msg: response.data.errorMessage
						});
						$location.path('/');
					} 
				}
			}
			
			function hotelResultStatus () {
				$timeout(function(){
					checkCondnt ();
				},1000);
			}
			function checkCondnt () {
				if($scope.hotelStatus == 'PENDING') {
					hotelResultStatus();
					} else {
					flightSuccess ();
				}
			}
			
		}); 
});

ouaApp.controller('insuranceLoadingController', function($scope,$http, $location, $timeout, ServerService){
$('html, body').scrollTop(0);
$scope.searching = "insurance"
$scope.Insurance = JSON.parse(sessionStorage.getItem('InsuranceSearchRequest'));
$scope.message = "Searching For Available Insurance";
 $http.post(ServerService.serverPath+'rest/insurance/plan', JSON.stringify($scope.Insurance),{
				headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
			}).then(function(response){
				$scope.loading = false;
					$scope.disabled = false;
				sessionStorage.setItem('InsuranceSearchResult',JSON.stringify(response.data.data));
				if(response.data.status == 'SUCCESS'){
						$location.path('/insuranceResult');
					} else {
				Lobibox.alert('error',{
					msg: response.data.errorMessage
				})
				$timeout(function(){
					$location.path('/')
					}, 1000);
			}
				
				})
});