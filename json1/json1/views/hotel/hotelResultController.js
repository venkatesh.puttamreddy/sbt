ouaApp.controller('hotelResultController', function ($scope, $http,$rootScope, $location, ConstantService, starService, ServerService) {
    $scope.isCollapsed = true;
	$scope.hotelSearchResp = JSON.parse(sessionStorage.getItem('hotelSearchResp'));
	$scope.totalNights=$scope.hotelSearchResp.duration;
	$scope.checkInDate=$scope.hotelSearchResp.checkInDate;
	$scope.checkOutDate=$scope.hotelSearchResp.checkOutDate;
	$scope.totalRooms=$scope.hotelSearchResp.totalRooms;
	$rootScope.passengerInfo;
	$scope.filterParams = $scope.hotelSearchResp.filterParams ;
	$scope.stars = $scope.filterParams.starRatingBeans;
	$scope.currency = ConstantService.currency;
	$scope.hotelSearchResp1 = $scope.hotelSearchResp.searchResults
	/* selectedHotel function */
	$scope.hotelResults = {};
	$scope.selectedHotel = function(val){
	
		$http.get(ServerService.serverPath+'rest/validate/session').then(function(response){
			if(response.data.isSessionValid == true){
				document.cookie = "hotelIndex="+val;
			var hotelIndex=getCookie("hotelIndex");
			var paramIndex = hotelIndex;
		
		$scope.hotelResults.hotelCode = $scope.hotelSearchResp.searchResults[val].htlCode;
			$scope.hotelResults.hotelName = 'null';
			$scope.hotelResults.isHotelOnly = true;
			
			$http.post(ServerService.serverPath+'rest/hotel/search/hoteldetails',JSON.stringify($scope.hotelResults),{
			headers : {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
			}).then(function(response){
				console.log(response);
				if(response.data.status == 'SUCCESS'){
					$scope.selectedHotelResp = response.data.data;
					sessionStorage.setItem('selectedHotelResp',JSON.stringify($scope.selectedHotelResp));
					$location.path('/hotelDetails')
				} else {
					Lobibox.alert('error',{
						msg : response.data.errorMessage
						})
					}
				})
			
				}
			})
		}
	/* end */
	
	/* starChange */
	$scope.starsObj = {}
	$scope.starsObj.starsList = []
	$scope.starLists = $scope.filterParams.starRatingBeans;
	$scope.luxaryList = $scope.filterParams.luxuryBeans;		
	var starValue; var luxValue;
	$scope.starChange = function(value){
		$scope.starValue = $scope.filterParams.starRatingBeans[value].value;
		starValue = $scope.starValue;
		var filteredData = starService.getStar($scope.hotelSearchResp, starValue, luxValue);
		$scope.hotelSearchResp1 = filteredData;
		}
   /* end */
   
   /* starToggle function */
   $scope.checkAll = false;
   $scope.starToggle = function(){
	   if(!$scope.checkAll){
					$scope.checkAll = true;
					$scope.starsObj.starsList = $scope.starLists.map(function(item){
					$scope.hotelSearchResp1 = $scope.hotelSearchResp.searchResults;
						return item.value;
						})
				} else {
				$scope.checkAll = false;
					$scope.starsObj.starsList = [];
					$scope.hotelSearchResp1 = {};
					}
	   }
   /* end */
   
   /* luxuryChange function */
   $scope.luxuryChange = function(index){
	   luxValue = $scope.filterParams.luxuryBeans[value].value;
	  var filteredData = starService.getStar($scope.hotelSearchResp, starValue, luxValue);
		$scope.hotelSearchResp1 = filteredData;
	   }
   /* end */
   
   /* toggleLux function */
   $scope.luxObj = {}
   $scope.luxObj.luxObjList = [];
   $scope.toggleLux = function(){
	   if(!$scope.checkAll){
		   $scope.checkAll = true;
		   $scope.luxObj.luxObjList = $scope.luxaryList.map(function(item){
			   $scope.hotelSearchResp1 = $scope.hotelSearchResp.searchResults;
			   return item.value;
		   }) 
		   
		   } else {
			   $scope.checkAll = false;
			   $scope.luxObj.luxObjList = [];
			   $scope.hotelSearchResp1 = {}
			   }
		   }
	 //  }
   /* end */
   /* hotelChange function */
   $scope.hotelChange = function(){
	   
	   }
   
    $scope.hotelDate = {
        startDate: moment(),
        endDate: moment(),
        locale: {
            format: "D-MMM-YY"
        }
    };
    /* $scope.range = function(n) {
        n = Number(n);
        return new Array(n);
    }; */
});
ouaApp.service('starService', function(){
	this.getStar = function(listing, starItem, luxItem){
	var star = [];
	var hotelResp = listing;
	var hotelList = hotelResp.searchResults;
		for(var s = 0; s < hotelList.length; s++){
		var temp = hotelList[s]
		var starValue = hotelResp.searchResults[s].starRating;
		if(starValue == starItem){
			star.push(temp);
			}
			
			}
			console.log(star);
			return star;
		}
	
	
})