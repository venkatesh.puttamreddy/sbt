ouaApp.controller('hotelItenenaryontroller', function($http, $scope, renderHTMLFactory){
	$scope.renderHTMLFactory = renderHTMLFactory
	console.log("Test");
	
	
	$scope.tripsDetail = function() {
		$('body').addClass("loading");
		$location.path('/user-trips');
		$timeout(function() {
			$('body').removeClass("loading");
		}, 1000);
	};
	if(sessionStorage.getItem("hotelBookJson")!=undefined) {
		$scope.getBookJson = sessionStorage.getItem("hotelBookJson");//Retrieve the stored data
		$scope.confirmJson =  JSON.parse($scope.getBookJson);
		$scope.reference=$scope.confirmJson.itinerary;
		$scope.description=$scope.confirmJson.htlDesc;
		$scope.hotelname=$scope.confirmJson.htlName;
		$scope.stars=$scope.confirmJson.htlType;
		$scope.checkIn=$scope.confirmJson.checkInDate;
		$scope.checkOut=$scope.confirmJson.checkOutDate;
		$scope.checkInTime=$scope.confirmJson.checkInTime;
		$scope.checkOutTime=$scope.confirmJson.checkOutTime;
		$scope.address=$scope.confirmJson.address;
		$scope.phoneNo=$scope.confirmJson.phoneNo;
		$scope.bookingDate=$scope.confirmJson.bkngDate;
		$scope.bookingStatus=$scope.confirmJson.bkngStatus;
		$scope.docket=$scope.confirmJson.docket;
		$scope.totalNights=$scope.confirmJson.totalNights;
		$scope.totalRooms=$scope.confirmJson.totalRooms;
		$scope.grandTotal=$scope.confirmJson.grandTotal;
		//$scope.totalFare=$scope.confirmJson.totalFare;
		$scope.currency=$scope.confirmJson.currency;
		$scope.convert=$scope.confirmJson.convRate;
		$scope.rooms=$scope.confirmJson.roomOccGrps;
		$scope.agent=$scope.confirmJson.agentInfoBean;
		$scope.credit = $scope.agent.creditInfoBean;
		$scope.roomItems=$scope.confirmJson.roomOccGrps[0].roomInfos[0].roomItems;
		$scope.salutations=$scope.confirmJson.roomOccGrps[0].roomInfos[0].roomItems[0].paxDtl.personaldata.salutation;
		$scope.trvtype1=$scope.confirmJson.roomOccGrps[0].roomInfos[0].roomItems[0].paxDtl.personaldata.trvtype;
		$scope.fname=$scope.confirmJson.roomOccGrps[0].roomInfos[0].roomItems[0].paxDtl.personaldata.fname;
		$scope.lname=$scope.confirmJson.roomOccGrps[0].roomInfos[0].roomItems[0].paxDtl.personaldata.lname;
		setTimeout(function(){
			$( ".roomfare" ).each(function(){
				$(this).text(parseFloat($(this).text()).toFixed(2));
			});
		}, 500);
		
		jQuery('.nav-bracket ul').attr('style','');
	}
	//$scope.isPnr = $location.absUrl().split('home.jsf')[1].search('get-book');
	$scope.doTheBack = function() {
		window.history.back();
	};
	if($scope.getBookJson) {
	$scope.capitalizeFirstLetter = function(rName) {
		var t = rName.toLowerCase();
		return t.replace(/(?:^|\s)\S/g, function(a) {
			return a.toUpperCase();
		});
	}
	} else {
		$location.path('/');
	}
	
	$scope.printTkt = function(i){
		var printRequest = new Object();
		printRequest.itineraryCode = $scope.confirmJson.itinerary;
		var printRequestJson = JSON.stringify(printRequest);
		$http({
			method:'POST',
			data:printRequestJson,
			url:ServerService.serverPath+'rest/hotel/booking/retrieve'
			}).success(function(data,status){
			if(data.status=="SUCCESS") {
				data.data.tIndex = i;
				var jsondata=data.data;
				sessionStorage.setItem("eTicketHotelJson",JSON.stringify(jsondata));
				window.open("e-ticketHotel.jsf",'_blank');
				} else {
				Lobibox.alert('error',
				{
					msg: data.errorMessage
				});
			}
			
		})
	}
})