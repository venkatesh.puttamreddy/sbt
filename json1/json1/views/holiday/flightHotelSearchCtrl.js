ouaApp.controller('flightHotelCtrl', function($scope, ConstantService, $uibModal, $http, $location, $interval, $timeout, ServerService, NationService, ajaxService, $rootScope){
	$scope.currency = ConstantService.currency1;
	NationService.nationGet();
	//	$scope.passengerCount = 0
	$scope.cabinClassList=["Economy", "Premium Economy", "Business", "First"];
	$scope.airlineList=[{title:"All Airlines", code:""}, {title:"Emirates", code:"EK"}, {title:"Qatar Airways", code:"QR"}, {title:"Flydubai", code:"FZ"}, {title:"Egyptair", code:"MS"}
						, {title:"Jet Airways", code:"9W"}, {title:"Saudi Arabian", code:"SV"}, {title:"Etihad Airways", code:"EY"}, {title:"Turkish Airlines", code:"TK"}, {title:"Aer Lingus", code:"EI"}, {title:"Aeroflot", code:"SU"}, {title:"Aerosvit Airlines", code:"VV"}, {title:"Afriqiyah Airways", code:"8U"}, {title:"Aigle Azur", code:"ZI"}, {title:"Air Algerie", code:"AH"}, {title:"Air Arabia", code:"G9"}, {title:"Air Astana", code:"KC"}, {title:"Air Baltic", code:"BT"}, {title:"Air Berlin", code:"AB"}, {title:"Air Canada", code:"AC"}, {title:"Air China", code:"CA"}, {title:"Air France", code:"AF"}, {title:"Air India", code:"AI"}, {title:"Air Macau", code:"NX"}, {title:"Air New Zealand", code:"NZ"}, {title:"Air Pacific", code:"FJ"}, {title:"Alitalia", code:"AZ"}, {title:"American Airlines", code:"AA"}, {title:"Austrian Airlines", code:"OS"}, {title:"Azerbaijan Airlines", code:"J2"}, {title:"BMI", code:"BD"}, {title:"Bangkok Airways", code:"PG"}, {title:"Biman Bangladesh Airlines", code:"BG"}, {title:"British Airways", code:"BA"}, {title:"Cathay Pacific", code:"CX"}, {title:"China Airlines", code:"CI"}, {title:"Condor", code:"DE"}, {title:"Continental Airlines", code:"CO"}, {title:"Czech Airlines", code:"OK"}, {title:"Darwin Airlines", code:"F7"}, {title:"Delta Air Lines", code:"DL"}, {title:"Eritrean Airlines", code:"B8"}, {title:"Ethiopian Airlines", code:"ET"}, {title:"Eva Air", code:"BR"}, {title:"Finnair", code:"AY"}, {title:"Flynas", code:"XY"}, {title:"Garuda Indonesia", code:"GA"}, {title:"Gulf Air", code:"GF"}, {title:"Hahn Air", code:"HR"}, {title:"Hainan Airlines", code:"HU"}, {title:"Heliair Monaco", code:"YO"}, {title:"Hong Kong Airlines", code:"HX"}, {title:"Icelandair", code:"FI"}, {title:"Jet Lite", code:"S2"}, {title:"Kenya Airways", code:"KQ"}, {title:"Kuban Airlines", code:"GW"}, {title:"Kuwait Airways", code:"KU"}, {title:"Liat", code:"LI"}, {title:"Lufthansa", code:"LH"}, {title:"Malaysia Airlines", code:"MH"}, {title:"Middle East Airlines", code:"ME"}, {title:"Mihin Lanka", code:"MJ"}, {title:"Montenegro Airlines", code:"YM"}, {title:"Olympic Airlines", code:"OA"}, {title:"Oman Air", code:"WY"}, {title:"Pakistan International", code:"PK"}, {title:"Philippine Airlines", code:"PR"}, {title:"Precision Air", code:"PW"}, {title:"Qantas", code:"QF"}, {title:"Rossiya Airlines", code:"FV"}, {title:"Royal Air Maroc", code:"AT"}, {title:"Royal Brunei", code:"BI"}, {title:"Royal Jordanian Airlines", code:"RJ"}, {title:"Royal Phnom Penh Airways", code:"RL"}, {title:"Rwandair Express", code:"WB"}, {title:"SAS", code:"SK"}, {title:"Shanghai Airlines", code:"FM"}, {title:"Siberian Airlines", code:"S7"}, {title:"SilkAir", code:"MI"}, {title:"Singapore Airlines", code:"SQ"}, {title:"South African Airways", code:"SA"}, {title:"SriLankan Airlines", code:"UL"}, {title:"Swiss", code:"LX"}, {title:"TAAG Angola Airlines", code:"DT"}, {title:"Tarom", code:"RO"}, {title:"Thai Airways International", code:"TG"}, {title:"Thomas Cook Airlines", code:"HQ"}, {title:"Tunis Air", code:"TU"}, {title:"US Airways", code:"US"}, {title:"Ukraine International Airlines", code:"PS"}, {title:"United Airlines", code:"UA"}, {title:"Ural Airlines", code:"U6"}, {title:"Vietnam Airlines", code:"VN"}, {title:"Virgin Atlantic", code:"VS"}];

	$(document).ready(function(){
    $("select").mouseenter(function(){
        $("select").css("color", "#ed1c24");
    });
    $("select").mouseleave(function(){
        $("select").css("color", "white");
    });
});

	$scope.cabinClass = "Economy";
	$scope.airline = {};
	$scope.airline.title = "All Airlines";
	$scope.airline.code = "";
	$scope.airlinee = $scope.airline.code;
	$scope.prefAirlineList = [];
	
	$scope.changeCabinClass = function(value){
		$scope.cabinClass = value;
	}
	
	$scope.changeAirline = function(value){
		$scope.airline.title = value.title;
		$scope.airlinee = value.code;
	}

	$rootScope.classData = ['Economy','Business','First'];
	//if(sessionStorage.getItem('flightRequest')!=undefined) {
	if(localStorage.getItem('flightRequest')!=undefined) {
		//$scope.searchFlightObjJson = JSON.parse(sessionStorage.getItem('flightRequest'));
		$scope.searchFlightObjJson = JSON.parse(localStorage.getItem('flightRequest'));
			
		$('#from_places').val($scope.searchFlightObjJson.frmAirport);
		$('#to_places').val($scope.searchFlightObjJson.toAirport);
		var infArr = [];
		for( var k = 0; k<=$scope.searchFlightObjJson.adtCnt; k++) {
			infArr.push(k);
		}
		$scope.fInfantCount = infArr;
		//console.log($scope.searchFlightObjJson);
		} else {
		$scope.searchFlightObjJson = {};
		$scope.searchFlightObjJson.adtCnt = 1;
		$scope.searchFlightObjJson.chldCnt = 0;
		$scope.searchFlightObjJson.infCnt = 0;
		$scope.fInfantCount = [0,1];
		$scope.searchFlightObjJson.classType = $scope.classData[0];
		$scope.searchFlightObjJson.frmAirport = 'MUSCAT, Oman - Seeb (MCT)';
		$("#from_places").val('MUSCAT, Oman - Seeb (MCT)');
	}
	
	$scope.searchFlightObjJson.srchType = 2;
	$scope.searchFlightObjJson.deptTime = "Any Time";
	$scope.searchFlightObjJson.retTime = "Any Time";
	//$scope.searchFlightObjJson.classType = 'Economy';
	//$scope.searchFlightObjJson.prefNonStop = false;
	$scope.searchFlightObjJson.pageIndex = 1;
	$scope.searchFlightObjJson.resultPerPage = 1000;

	if(jQuery('#from_places').val()=='') {
		jQuery('#from_places').focus();
	}
	var fromset = false;
	var toset = false;
	var flag = false;
	var flag1 = false;
	var f;
	$( "#from_places" ).autocomplete({
		source: function( request, response ) {
			ajaxService.ajaxRequest('POST',ServerService.serverPath+'rest/repo/airport','{ "queryString" : "'+request.term+'"}','json')
			.then(function(data,status) {
				if(data!=null && data!='') {
					f=0;
					response( data );
					}else{
					$( "#from_places" ).val('');
					$( "#from_places" ).focus(0);
					$('.ui-autocomplete').hide();
				}
			});
		},
		focus: function( event, ui ) {
			$( "#from_places" ).val( ui.item.city + ", " + ui.item.country + " - " + ui.item.name+" ("+ui.item.code+")" );
			return false;
		},
		change : function(event,ui) {
			$( "#from_places" ).val($scope.searchFlightObjJson.frmAirport);
			if($( "#to_places" ).val()==$( "#from_places" ).val()) {
				$( "#from_places" ).val("");
				return false;
			}
			$timeout(function(){
				jQuery('.form-group').removeClass('has-error');
				$scope.emptyError = false;
			},10);
			/*if(flag==false) {
				$('#from_places').val('');
				$('#from_places').focus(0);
				return false;
			}*/
		},
		//autoFocus: true,
		minLength: 3,
		select: function( event, ui ) {
			if($( "#from_places" ).val()==$( "#to_places" ).val()) {
				$( "#from_places" ).val('');
				$('.destcheck').show().fadeOut(5000);
				} else {
				$( "#from_places" ).val( ui.item.city + ", " + ui.item.country + " - " + ui.item.name+" ("+ui.item.code+")" );
				$scope.searchFlightObjJson.frmAirport = ui.item.city + ", " + ui.item.country + " - " + ui.item.name+" ("+ui.item.code+")";
				$scope.$apply();
				$('#to_places').focus(0);
				flag = true;
			}
			return false;
		},
		open: function() {
			$( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
		},
		close: function(event, ui) {
			$( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
		}
		}).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
		if(f==0) {
			f++;
			$scope.searchFlightObjJson.frmAirport = item.city + ", " + item.country + " - " + item.name+" ("+item.code+")";
			return $( "<li>" ).append( "<a class='ui-state-focus'>" + item.city + "," + item.country + "-" + item.name+"("+item.code+")</a>" ).appendTo( ul );
			} else {
			return $( "<li>" ).append( "<a>" + item.city + "," + item.country + "-" + item.name+"("+item.code+")</a>" ).appendTo( ul );
		}
	};
	$( "#to_places" ).autocomplete({
		source: function( request, response ) {
			ajaxService.ajaxRequest('POST',ServerService.serverPath+'rest/repo/airport','{ "queryString" : "'+request.term+'"}','json')
			.then(function(data,status) {
				if(data!=null && data!='') {
					f=0;
					response( data );
					}else{
					$( "#to_places" ).val('');
					$( "#to_places" ).focus(0);
					$('.ui-autocomplete').hide();
				}
			});
		},
		focus: function( event, ui ) {
			$( "#to_places" ).val( ui.item.city + ", " + ui.item.country + " - " + ui.item.name+" ("+ui.item.code+")" );
			return false;
		},
		change : function(event,ui) {
			$scope.$apply();
			$( "#to_places" ).val($scope.searchFlightObjJson.toAirport);
			if($( "#to_places" ).val()==$( "#from_places" ).val()) {
				$( "#to_places" ).val("");
				$('.destcheck').show().fadeOut(10000);
				return false;
			}
			$timeout(function(){
				jQuery('.form-group').removeClass('has-error');
				$scope.emptyError = false;
			},10);
			/*if(flag1==false) {
				$('#to_places').val('');
				$('#to_places').focus(0);
				return false;
			}*/
		},
		//autoFocus: true,
		minLength: 3,
		select: function( event, ui ) {
			if($( "#from_places" ).val()==$( "#to_places" ).val()) {
				$( "#to_places" ).val('');
				$('.destcheck').show().fadeOut(10000);
				} else {
				flag1 = true;
				$( "#to_places" ).val( ui.item.city + ", " + ui.item.country + " - " + ui.item.name+" ("+ui.item.code+")" );
				$scope.searchFlightObjJson.toAirport = ui.item.city + ", " + ui.item.country + " - " + ui.item.name+" ("+ui.item.code+")";
				$scope.$apply();
				$('#datepicker3').focus();
			}
			return false;
		},
		open: function() {
			$( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
		},
		close: function() {
			$( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
		}
		}).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
		if(f==0) {
			f++;
			$scope.searchFlightObjJson.toAirport = item.city + ", " + item.country + " - " + item.name+" ("+item.code+")";
			return $( "<li>" ).append( "<a class='ui-state-focus'>" + item.city + "," + item.country + "-" + item.name+"("+item.code+")</a>" ).appendTo( ul );
			} else {
			return $( "<li>" ).append( "<a>" + item.city + "," + item.country + "-" + item.name+"("+item.code+")</a>" ).appendTo( ul );
		}
	};
	
	$scope.states = [];
	$scope.flightfromAutoComplet=function(value) {
	if(value!=null){
  if(value.length >= 2){
  var place = {'queryString': value}
  $http.post(ServerService.serverPath+'rest/repo/airport','{"queryString" : "'+value+'"}', 
  {
   headers: {
    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
   }
   }).then(function(response){
  
   if(response.data!='null'&&response.data.length>0){
   $scope.states = []
   for(var i=0; i<response.data.length; i++){
     var airfrom= {
                      Code: response.data[i].code,
       Title: response.data[i].city+","+response.data[i].country+"-"+response.data[i].name+"("+response.data[i].code+")",
                      
     }
     $scope.states.push(airfrom);
     
    }
    $scope.states.sort();
  }
  })
 }
 }
};
$scope.city = []
$scope.cityAutoComplet = function(totalcitys) {
	 console.log("HotelInCity"+ totalcitys);
	if(totalcitys.length==2){
	   console.log("HotelInCity"+ totalcitys);
		$http.post(ServerService.serverPath+'rest/hotel/cities','{"queryString" : "'+totalcitys+'"}', 
			{
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
				}
			}).then(function(response){
			/* $scope.hotelstates = JSON.stringify(response.data); */
			for(var i=0; i<response.data.length; i++){
					var hotelcity=	{
                        Title: response.data[i].city+","+response.data[i].country,
                      Code: response.data[i].code
					}
					$scope.city.push(hotelcity);
				}
			})
}
} 
	
	
    
    var intitialDate = {
        startDate: moment(),
        endDate: moment(),
        locale: {
            format: "D/MM/YYYY"
		}
	};
    
    $scope.date = intitialDate;
	
	$scope.hoteldate = {
		startDate: null,
		endDate: null
	}
	
	/* search function */
	$scope.flightRequest = {}
	$scope.hotelRequest = {}
	$scope.roombean = JSON.parse(sessionStorage.getItem('roombean'));
	/* if($scope.roombean != null){
		$scope.passengerCount = $scope.roombean.length	
	} */
	
	$scope.search = function(){
		
	}
	/* end */
	
	/* holidaySearch function */
	$scope.loading = false;
	$scope.disabled = false;
	var adoultCount=0;
	var childCount=0;
	$scope.hotelRequest = {}
	
	$scope.roomList=JSON.parse(sessionStorage.getItem('roomList'));
	if($scope.roomList!=null){
	for(i=0;i<$scope.roomList.length;i++){
			adoultCount=adoultCount+parseInt($scope.roomList[i].adultCount);
			childCount=childCount+parseInt($scope.roomList[i].childCount);
		}
	}
		$rootScope.passengerInfo="Adults - "+adoultCount+" + Childs - "+childCount;
	
	$scope.holidaySearch = function(valid){
	$scope.submitted = true;
		
		if(valid){
			var fromPlace = $('#from_places').val();
		var toPlace = $('#to_places').val();
		var flightDate1 = $scope.date;
		var fromFlightDate = flightDate1.startDate.format('D/MM/YYYY')
		var toFlightDate = flightDate1.endDate.format('D/MM/YYYY')
		
		/* var flightDate = $('#daterange1').val();
		var fromFlightDate = flightDate.split('-')[0];
		var toFlightDate = flightDate.split('-')[1]; */
		
		var hotelDate = $('#hoteldaterange').val();
		var hotelCheckIn = hotelDate.split('-')[0];
		var hotelCheckOut = hotelDate.split('-')[1];
		
		var fromHotelPlace = $scope.fromHotel.Title;
		var country = fromHotelPlace.split(',')[1];
		var city = fromHotelPlace.split(',')[0];
		
		var night = (parseInt(hotelCheckOut) - parseInt(hotelCheckIn))
		/* $scope.roombean = JSON.parse(sessionStorage.getItem('roombean'));
		$scope.infantCount = JSON.parse(sessionStorage.getItem('infant'));
		$scope.childCount = JSON.parse(sessionStorage.getItem('child'));
		$scope.adultCount = JSON.parse(sessionStorage.getItem('adult'));
		
		if($scope.infantCount == null || $scope.infantCount == undefined){
			$scope.infantCount = 0;
			} else {
			$scope.infantCount = $scope.infantCount;
		} */
		$scope.roomList=JSON.parse(sessionStorage.getItem('roomList'));
		if($scope.roomList!=null){
			  for(i=0;i<$scope.roomList.length;i++){
					var tempAdultCount = $scope.roomList[i].adultCount;
					delete $scope.roomList[i]['$$hashKey'];
				switch(tempAdultCount) {
				  case 1:
				  $scope.roomList[i].roomDesc='Single';
				  break;
				  case 2:
				  $scope.roomList[i].roomDesc='Double';
				  break;
				  case 3:
				  $scope.roomList[i].roomDesc='Triple';
				  break;
				  case 4:
				  $scope.roomList[i].roomDesc='Quad';
				  break;
				 }
				 tempAdultCount++;
				adoultCount=adoultCount+parseInt($scope.roomList[i].adultCount);
				childCount=childCount+parseInt($scope.roomList[i].childCount);
			}
		}
		
		$scope.flightRequest.frmAirport = fromPlace;
		$scope.flightRequest.toAirport = toPlace;
		$scope.flightRequest.adtCnt = adoultCount;
		$scope.flightRequest.chldCnt = childCount;
		$scope.flightRequest.strDeptDate = fromFlightDate;
		$scope.flightRequest.strRetDate = toFlightDate;
		$scope.flightRequest.binNumber = '552107';
		
		/* $scope.flightRequest.classType = 'Economy'; */
		$scope.flightRequest.classType = $scope.cabinClass;
			if($scope.airlinee != ""){
			$scope.prefAirlineList.push($scope.airlinee);
			}
			$scope.flightRequest.prefAirlineLst = $scope.prefAirlineList;
		$scope.flightRequest.deptTime = 'Any Time';
		$scope.flightRequest.retTime = 'Any Time';
		$scope.flightRequest.infCnt = $scope.infantCount;
		$scope.flightRequest.onlyFlight = false;
		$scope.flightRequest.pageIndex = 1;
		$scope.flightRequest.prefNonStop = false;
		$scope.flightRequest.resultPerPage = 1000;
		$scope.flightRequest.srchType = 2;
		$scope.hotelRequest.checkIn = hotelCheckIn;
		$scope.hotelRequest.checkOut = hotelCheckOut;
		$scope.hotelRequest.place = fromHotelPlace;
		$scope.hotelRequest.country = country;
		$scope.hotelRequest.city = city;
		$scope.hotelRequest.currency = $scope.currency;
		$scope.hotelRequest.htlName = null;
		$scope.hotelRequest.searchMode = 'search';
		$scope.hotelRequest.starRating = 0;
		
		// $scope.hotelRequest.roomBean = $scope.roombean;
		// $scope.hotelRequest.traveller = $scope.roombean.length
		$scope.hotelRequest.roomBean = $scope.roomList;
		
		$scope.hotelRequest.binNumber = '552107';
		$scope.hotelRequest.locationCode = city;
		sessionStorage.setItem('hotelPlusRequest', JSON.stringify($scope.hotelRequest));
		sessionStorage.setItem('flightPlusRequest', JSON.stringify($scope.flightRequest));
		$location.path('/searchingHoliday');
			}
		}
	
	
	/* open modal function */
	
	$scope.animationEnabled = true;
	$scope.open = function(size, parentSelector){
		var parentElem = parentSelector ? 
		angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
		var modalInstance = $uibModal.open({
			animation: $scope.animationEnabled,
			ariaLabelledBy : 'modal-title',
			ariaDescribedBy : 'modal-body',
			templateUrl : 'modalContent.html',
			controller : 'modalContentCtrl',
			backdrop: true,
			size : size,
			appendTo : parentElem,
			resolve: {
				items: function(){
					return $scope.items
				}
			}
		})
	}
	/* end */
	
}).controller('modalContentCtrl', function($scope,$rootScope, $uibModalInstance){
	
	  
   $scope.maxRooms=6;
    
  $scope.roomBean= {
      index: 1,
      adultCount: 1,
      cots: 0,
      childCount: 1,
      roomDesc: "Single",
      xtraBed: false,
      roomCount: 1,
      withChildrens: true,
      childrens: []
    };
	
	$scope.childBean={
          index: 0,
          childCount: 1,
          childBeans: [ ]
        };
		
	$scope.childInfo={
              name: null,
              age: 4
            };
	function createChildBean(){
		var childBean={};
	      childBean.index= 0;
          childBean.childCount= 1;
          childBean.childBeans= [ ];
		  return childBean;
        
	}
	function createChildInfo(){	
		var childInfo={};
              childInfo.name= null;
              childInfo.age= 1;
			  return childInfo;
            
    }			
    
	if(sessionStorage.getItem('roomList') ==undefined){
    $scope.roomList=[];
	 $scope.roomCount=1;
	   var roomBean=createRoomBean();
	   roomBean.roomCount=1;
       $scope.roomList.push(roomBean);	
	}
	else{
		$scope.roomList=JSON.parse(sessionStorage.getItem('roomList'));
		$scope.roomCount=$scope.roomList.length;
	}
  function createRoomBean(){
    var roomBeanObject= {};
        roomBeanObject.index=1;
        roomBeanObject.adultCount="2";
        roomBeanObject.cots=0;
        roomBeanObject.childCount="0";
        roomBeanObject.roomDesc='Single';
        roomBeanObject.xtraBed=false;
        roomBeanObject.roomCount=1;
        roomBeanObject.withChildrens=false;
        roomBeanObject.childrens=[]; 
        return roomBeanObject;
}  
  
    $scope.addRoom=function(){
    	  var roomBean=createRoomBean();
		  roomBean.roomCount=$scope.roomList.length+1;
          $scope.roomList.push(roomBean);		  
    }

     $scope.removeRoom=function(index){
       	  $scope.roomList.splice(index, 1);		  
    }


    $scope.updateRoomList=function(){
        if($scope.roomCount>0){
        if( $scope.roomList.length<$scope.roomCount){
		for(i=$scope.roomList.length; i<$scope.roomCount;i++){	
          var roomBean=createRoomBean();
		  roomBean.roomCount=i+1;
          $scope.roomList.push(roomBean);		  
		}
        }
		else{
			$scope.roomList.splice($scope.roomCount);
		}
        
      }
      
    }
	
	$scope.updateChildList=function(index){
         if($scope.roomList[index].childrens.length==0){
			 $scope.roomList[index].childrens.push(createChildBean());
			 $scope.roomList[index].childrens[0].childCount=$scope.roomList[index].childCount;
			 $scope.roomList[index].withChildrens=true;
			 for(i=0;i<$scope.roomList[index].childCount;i++){
				  $scope.roomList[index].childrens[0].childBeans.push(createChildInfo());
			 }
		 }
		 else {
			 if($scope.roomList[index].childCount==0){
				 $scope.roomList[index].withChildrens=false;
				 $scope.roomList[index].childrens[0].childBeans=null;
			 }
			 else if( $scope.roomList[index].childrens[0].childBeans.length < $scope.roomList[index].childCount)
				$scope.roomList[index].childrens[0].childBeans.push(createChildInfo());
			else{
				$scope.roomList[index].childrens[0].childBeans.splice(1);
			}
		 }
        
      }
      
    
	$scope.ok = function(){
		sessionStorage.setItem('roomList', JSON.stringify($scope.roomList));
		$scope.roomList=JSON.parse(sessionStorage.getItem('roomList'));
		var adultCount=0;
		var childCount=0;
		for(i=0;i<$scope.roomList.length;i++){
			adultCount=adultCount+parseInt($scope.roomList[i].adultCount);
			childCount=childCount+parseInt($scope.roomList[i].childCount);
		
		}
		$rootScope.totalPassengers=adultCount+childCount;
		$rootScope.passengerInfo="Adults - "+adultCount+" + Childs - "+childCount;
		$uibModalInstance.close();	
		
}
$scope.cancel = function(){
	$uibModalInstance.dismiss('cancel');
}

});

// ouaApp.controller('modalContentCtrl', function($uibModalInstance, $scope){
	// $scope.adults = [1, 2, 3, 4, 5];
	// $scope.childs = [0,1, 2];
	// $scope.childrenAge = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ,11];
	// $scope.rooms = [1,2,3,4,5,6];
	
	// /* roomChange function */
	// $scope.roomChange = function(values){
		// $scope.roomCount = [];
		// for(var r=0; r<values; r++){
			// $scope.roomCount.push(values);
		// }
		
	// }
	// /* end */
	
	// /* childAge function */
	// $scope.childAge = function(value){
		// $scope.child = [];
		// for(var c=0; c<value; c++){
			// $scope.child.push(value);
		// }
	// } 
	// /* end */
	// $scope.submitted = false;
	// $scope.passengers = function(valid){
	// $scope.submitted = true;
	// if(!valid){
		// $scope.flightChild = 0; $scope.flightInfant = 0; $scope.adultNo = 0;
		// var roomBean = new Array();
		// $('.passenger').each(function(index){
			// jQuery(this).attr('id', 'passenger_'+(index+1));
		// });
		// $('.roomlist').each(function(ind){
			
			// var id = jQuery(this).attr('id');
			// var roomBeanObject = new Object();
			// roomBeanObject.index = parseInt(ind)+1;
			// var roomId = id.split('_');
			// if(roomId[0] == 'passenger'){
			// console.log('passenger_passenger_passenger_')
			// roomBeanObject.adultCount = parseInt($(this).find('.adult').val());
			// $scope.adultNo += roomBeanObject.adultCount
			// sessionStorage.setItem('adult',JSON.stringify($scope.adultNo));
			// roomBeanObject.childCount = parseInt($(this).find('.child').val());
			// roomBeanObject.cots = 0;
			
			// if(roomBeanObject.childCount > 0){
				// roomBeanObject.withChildrens=true;
				// var childrens = new Object();
				// childrens.index = 0;
				// childrens.childCount = roomBeanObject.childCount;
				// var child_beans = new Array();
				// jQuery(this).find('.child-age').each(function(){
					// var child_object = new Object();
					// child_object.name = null;
					// child_object.age = $(this).find('.age').val();
					// child_beans.push(child_object);
					// if(child_object.age == 1){
						// $scope.flightInfant += 1
						// sessionStorage.setItem('infant',JSON.stringify($scope.flightInfant));
						
						// } else {
						// $scope.flightChild += 1
						// sessionStorage.setItem('child',JSON.stringify($scope.flightChild));
					// }
				// })
				// childrens.childBeans = child_beans;
				// var children_beans = new Array();
				// children_beans.push(childrens);
				// roomBeanObject.childrens = children_beans
				// } else {
				// roomBeanObject.withChildrens=false;
				// roomBeanObject.childrens = null
			// }
		// }
		// switch(roomBeanObject.adultCount){
			// case 1:
			// roomBeanObject.roomDesc='Single';
			// break;
			// case 2:
			// roomBeanObject.roomDesc='Double';
			// break;
			// case 3:
			// roomBeanObject.roomDesc='Triple';
			// break;
			// case 4:
			// roomBeanObject.roomDesc='Quad';
			// break;
		// }
		// roomBeanObject.xtraBed=false;
		// roomBeanObject.roomCount = roomId[1]
		
		// if(roomBeanObject.childCount == null || roomBeanObject.childCount == undefined || roomBeanObject.childCount == NaN){
			// roomBeanObject.childCount = 0;
			// }
		// $scope.$parent.passengerCount = parseInt(roomBeanObject.adultCount) + parseInt(roomBeanObject.childCount)
		
		// $scope.roombeanList = roomBean.push(roomBeanObject);
		// sessionStorage.setItem('roombean', JSON.stringify(roomBean));
	// });
	// $uibModalInstance.close();
		// }
		
// }
// $scope.cancel = function(){
	// $uibModalInstance.dismiss('cancel');
// }

// })