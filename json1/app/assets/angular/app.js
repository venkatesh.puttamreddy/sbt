"use strict";
var app = angular.module('myProject', ['ngRoute','route-segment', 'view-segment','ngResource','checklist-model','ngTasty','ngCookies','ngSanitize','angular-repeat-n','ngFileUpload']);
app.config(function($routeSegmentProvider){
	
	$routeSegmentProvider.
	when('/','index').segment('index', { templateUrl: 'app/flight/search/search-view.html', controller: 'searchCtrl' }).
	when('/flight','flight').segment('flight', { templateUrl: 'app/flight/search/search-view.html', controller: 'searchCtrl' }).
	when('/hotel','hotel').segment('hotel', { templateUrl: 'app/flight/search/search-view.html', controller: 'searchCtrl' }).
	when('/insurance','insurance').segment('insurance', { templateUrl: 'app/flight/search/search-view.html', controller: 'searchCtrl' }).
	when('/flight-hotel','flightHotel').segment('flightHotel', { templateUrl: 'app/flight/search/search-view.html', controller: 'searchCtrl' }).
	when('/searching-flight-hotel','searchingflightHotel').segment('searchingflightHotel', { templateUrl: 'app/includes/loadingPage.html', controller: 'flightHotelLoadCtrl' }).
	when('/flight-hotel-results','flightHotelresults').segment('flightHotelresults', { templateUrl: 'app/flight-hotel/search-results/flight-hotel-results-view.html', controller: 'hotelResultCtrl' }).
	when('/flight-hotel-review','flightHotelreview').segment('flightHotelreview', { templateUrl: 'app/includes/itinerary-template.html', controller: 'flightReviewCtrl' }).
	/* when('/searching-insurance','searching-insurance').segment('searching-insurance', { templateUrl: 'app/insurance/load/insurance-load-view.html', controller: 'insuranceLoadCtrl' }). */
	when('/searching-insurance','searching-insurance').segment('searching-insurance', { templateUrl: 'app/includes/loadingPage.html', controller: 'insuranceLoadCtrl' }).
	when('/insurance-results','insurance-results').segment('insurance-results', { templateUrl: 'app/insurance/search-results/insurance-results-view.html', controller: 'insuranceResultsCtrl' }).
	when('/searching-flight','searching-flight').segment('searching-flight', { templateUrl: 'app/includes/loadingPage.html', controller: 'flightLoadCtrl' }).
	when('/flight-results','flight-results').segment('flight-results', { templateUrl: 'app/flight/search-results/flight-results-view.html', controller: 'flightResultsCtrl' }).
	when('/flight-review','flight-review').segment('flight-review', { templateUrl: 'app/includes/itinerary-template.html', controller: 'flightReviewCtrl' }).	
	when('/flight-itinerary','flight-itinerary').segment('flight-itinerary', { templateUrl: 'app/flight/itinerary/flight-itinerary-view.html', controller: 'flightItineraryCtrl' }).
	when('/confirmed-flight-itinerary','flight-itinerary').segment('flight-itinerary', { templateUrl: 'app/flight/itinerary/flight-itinerary-view.html', controller: 'flightItineraryCtrl' }).
	when('/flight-hotel-itinerary','flight-hotel-itinerary').segment('flight-hotel-itinerary', { templateUrl: 'app/flight-hotel/itinerary/flight-hotel-itinerary-view.html', controller: 'flightItineraryCtrl' }).
	when('/confirmed-flight-hotel-itinerary','flight-hotel-itinerary').segment('flight-hotel-itinerary', { templateUrl: 'app/flight-hotel/itinerary/flight-hotel-itinerary-view.html', controller: 'flightItineraryCtrl' }).
	when('/insurance-itinerary','insurance-itinerary').segment('insurance-itinerary', { templateUrl: 'app/insurance/itinerary/insurance-itinerary-view.html', controller: 'insureItineraryCtrl' }).
	when('/confirmed-insurance-itinerary','insurance-itinerary').segment('insurance-itinerary', { templateUrl: 'app/insurance/itinerary/insurance-itinerary-view.html', controller: 'insureItineraryCtrl' }).
	when('/user','user').segment('user', { templateUrl: 'app/account/user-home-view.html', controller: 'userTripsCtrl'}).
	when('/user-profile','user.user-profile').within('user').segment('user-profile',{ templateUrl: 'app/account/profile/profile-view.html', controller: 'userProfileCtrl' }).
	up().
	when('/user-traveller','user.user-traveller').within('user').segment('user-traveller',{ templateUrl: 'app/account/traveller/traveller-view.html', controller: 'userTravellerCtrl' }).
	up().
	when('/user-trips','user.user-trips').within('user').segment('user-trips',{ templateUrl: 'app/account/trips/trips-view.html', controller: 'userTripsCtrl' }).
	up().
	when('/user-reports','user.user-reports').within('user').segment('user-reports',{ templateUrl: 'app/account/reports/reports-view.html', controller: 'userReportCtrl' }).
	up().
	when('/searching-hotel','searching-hotel').segment('searching-hotel', { templateUrl: 'app/includes/loadingPage.html', controller: 'hotelLoadCtrl' }).
	when('/hotel-results','hotel-results').segment('hotel-results', { templateUrl: 'app/hotel/search-results/hotel-results-view.html', controller: 'hotelResultCtrl' }).
	when('/hotel-summary','hotel-summary').segment('hotel-summary', { templateUrl: 'app/hotel/summary/hotel-summary-view.html', controller: 'hotelSummaryCtrl' }).
	when('/hotel-review','hotel-review').segment('hotel-review', { templateUrl: 'app/includes/itinerary-template.html', controller: 'hotelReviewCtrl' }).
	when('/hotel-itinerary','hotel-itinerary').segment('hotel-itinerary', { templateUrl: 'app/hotel/itinerary/hotel-itinerary-view.html', controller: 'hotelItineraryCtrl' }).
	when('/confirmed-hotel-itinerary','hotel-itinerary').segment('hotel-itinerary', { templateUrl: 'app/hotel/itinerary/hotel-itinerary-view.html', controller: 'hotelItineraryCtrl' }).
	when('/insurance-review','insurance-review').segment('insurance-review', { templateUrl: 'app/includes/itinerary-template.html', controller: 'insuranceReviewCtrl' }).
	when('/about-sata','about').segment('about', { templateUrl: 'app/includes/about-us.html'}).
	when('/tour-packages/:name','packages').segment('packages', { templateUrl: 'app/includes/packages/packages.html', controller:'packageCtrl'}).
	when('/family-packages','familyPackages').segment('familyPackages', { templateUrl: 'app/includes/packages/family-holidays.html', controller:'packageCtrl'}).
	when('/beach-packages','beachPackages').segment('beachPackages', { templateUrl: 'app/includes/packages/beach-holidays.html', controller:'packageCtrl'}).
	when('/honeymoon-packages','honeymoonPackages').segment('honeymoonPackages', { templateUrl: 'app/includes/packages/honeymoon-packages.html', controller:'packageCtrl'}).
	when('/adventure-packages','adventurePackages').segment('adventurePackages', { templateUrl: 'app/includes/packages/adventure-packages.html', controller:'packageCtrl'}).
	when('/attraction-local/:name','attraction').segment('attraction', { templateUrl: 'app/includes/attraction/attraction.html', controller:'attractionCtrl'}).
	when('/attraction-local-sharjah','attractionSharjah').segment('attractionSharjah', { templateUrl: 'app/includes/attraction/attraction-sharjah.html', controller:'attractionCtrl'}).
	when('/attraction-local-dubai','attractionDubai').segment('attractionDubai', { templateUrl: 'app/includes/attraction/attraction-dubai.html', controller:'attractionCtrl'}).
	when('/attraction-local-abudhabi','attractionAbudhabi').segment('attractionAbudhabi', { templateUrl: 'app/includes/attraction/attraction-abu-dhabi.html', controller:'attractionCtrl'}).
	when('/attraction-dubai-city','attractionDubaiCity').segment('attractionDubaiCity', { templateUrl: 'app/includes/attraction/details/attraction-detail-dubai-tour.html', controller:'attractionDetailCtrl'}).
	when('/attraction-desert-safari','attractionDesert').segment('attractionDesert', { templateUrl: 'app/includes/attraction/details/attraction-detail-desert-safari.html', controller:'attractionDetailCtrl'}).
	when('/attraction-dhow-cruise','attractionCruise').segment('attractionCruise', { templateUrl: 'app/includes/attraction/details/attraction-detail-cruise.html', controller:'attractionDetailCtrl'}).
	when('/attraction-atlantis-aquaventure','attractionAtlantis').segment('attractionAtlantis', { templateUrl: 'app/includes/attraction/details/attraction-detail-atlantis.html', controller:'attractionDetailCtrl'}).
	when('/attraction-abudhabi-city','attractionAbudhabiCity').segment('attractionAbudhabiCity', { templateUrl: 'app/includes/attraction/details/attraction-detail-abudhabi.html', controller:'attractionDetailCtrl'}).
	when('/attraction-iceland-waterpark','attractionIceland').segment('attractionIceland', { templateUrl: 'app/includes/attraction/details/attraction-detail-iceland.html', controller:'attractionDetailCtrl'}).
	when('/attraction-dubai-dolphinarium','attractionDolphinarium').segment('attractionDolphinarium', { templateUrl: 'app/includes/attraction/details/attraction-detail-dolphinarium.html', controller:'attractionDetailCtrl'}).
	when('/attraction-ferrari-themepark','attractionThemepark').segment('attractionThemepark', { templateUrl: 'app/includes/attraction/details/attraction-detail-themepark.html', controller:'attractionDetailCtrl'}).
	when('/attraction-burj-khalifa','attractionBurjkhalifa').segment('attractionBurjkhalifa', { templateUrl: 'app/includes/attraction/details/attraction-detail-burjkhalifa.html', controller:'attractionDetailCtrl'}).
	when('/attraction-yas-water-world','attractionYasWater').segment('attractionYasWater', { templateUrl: 'app/includes/attraction/details/attraction-detail-yaswater.html', controller:'attractionDetailCtrl'}).
	when('/attraction-musandam-dibba-tour','attractionMusandam').segment('attractionMusandam', { templateUrl: 'app/includes/attraction/details/attraction-detail-musandam.html', controller:'attractionDetailCtrl'}).
	when('/attraction-wildwadia-water-park','attractionWildwadia').segment('attractionWildwadia', { templateUrl: 'app/includes/attraction/details/attraction-detail-wildwadia.html', controller:'attractionDetailCtrl'}).
	when('/attraction-ski-dubai','attractionSkidubai').segment('attractionSkidubai', { templateUrl: 'app/includes/attraction/details/attraction-detail-skidubai.html', controller:'attractionDetailCtrl'}).
	when('/attraction-sharjah-aquarium','attractionSharjaAqua').segment('attractionSharjaAqua', { templateUrl: 'app/includes/attraction/details/attraction-detail-sharja-aquarium.html', controller:'attractionDetailCtrl'}).
	when('/attraction-deepsea-cruising','attractionSeaCruising').segment('attractionSeaCruising', { templateUrl: 'app/includes/attraction/details/attraction-detail-sea-cruising.html', controller:'attractionDetailCtrl'}).
	when('/attraction-deepsea-fishing','attractionSeaFishing').segment('attractionSeaFishing', { templateUrl: 'app/includes/attraction/details/attraction-detail-sea-fishing.html', controller:'attractionDetailCtrl'}).
	when('/attraction-wonder-bus','attractionWonderBus').segment('attractionWonderBus', { templateUrl: 'app/includes/attraction/details/attraction-detail-wonder-bus.html', controller:'attractionDetailCtrl'}).
	when('/attraction-hotair-balloon','attractionHotBallon').segment('attractionHotBallon', { templateUrl: 'app/includes/attraction/details/attraction-detail-hotair-balloon.html', controller:'attractionDetailCtrl'}).
	when('/attraction-musandam-kasab','attractionMusandamKasab').segment('attractionMusandamKasab', { templateUrl: 'app/includes/attraction/details/attraction-detail-musandam-kasab.html', controller:'attractionDetailCtrl'}).
	when('/attraction-culture-tour','attractionCultureTour').segment('attractionCultureTour', { templateUrl: 'app/includes/attraction/details/attraction-detail-culture-tour.html', controller:'attractionDetailCtrl'}).
	when('/attraction-maritime-museum','attractionMaritimeMuseum').segment('attractionMaritimeMuseum', { templateUrl: 'app/includes/attraction/details/attraction-detail-maritime-museum.html', controller:'attractionDetailCtrl'}).
	when('/attraction-sharjah-citytour','attractionSharjahCitytour').segment('attractionSharjahCitytour', { templateUrl: 'app/includes/attraction/details/attraction-detail-sharjah-citytour.html', controller:'attractionDetailCtrl'}).
	when('/attraction-mahatta-museum','attractionMahattaMuseum').segment('attractionMahattaMuseum', { templateUrl: 'app/includes/attraction/details/attraction-detail-mahatta-museum.html', controller:'attractionDetailCtrl'}).
	when('/attraction-almontazah-park','attractionAlmontazahMuseum').segment('attractionAlmontazahMuseum', { templateUrl: 'app/includes/attraction/details/attraction-detail-almontazah-park.html', controller:'attractionDetailCtrl'}).
	when('/attraction-fujaira-tour','attractionFujairaTour').segment('attractionFujairaTour', { templateUrl: 'app/includes/attraction/details/attraction-detail-fujaira-tour.html', controller:'attractionDetailCtrl'}).
	when('/attraction-arabias-wildlife','attractionArabiasWildlife').segment('attractionArabiasWildlife', { templateUrl: 'app/includes/attraction/details/attraction-detail-arabias-wildlife.html', controller:'attractionDetailCtrl'}).
	when('/attraction-print-sharja-aquarium','attraction-print-sharja-aquarium').segment('attraction-print-sharja-aquarium', { templateUrl: 'app/includes/attraction/print/attraction-print-sharja-aquarium.html'}).
	when('/attraction-print-desert-safari','attraction-print-desert-safari').segment('attraction-print-desert-safari', { templateUrl: 'app/includes/attraction/print/attraction-print-desert-safari.html'}).
	when('/attraction-print-iceland-waterpark','attraction-print-iceland-waterpark').segment('attraction-print-iceland-waterpark', { templateUrl: 'app/includes/attraction/print/attraction-print-iceland-waterpark.html'}).
	when('/attraction-print-musandam-dibbatour','attraction-print-musandam-dibbatour').segment('attraction-print-musandam-dibbatour', { templateUrl: 'app/includes/attraction/print/attraction-print-musandam-dibbatour.html'}).
	when('/attraction-print-musandam-kasab','attraction-print-musandam-kasab').segment('attraction-print-musandam-kasab', { templateUrl: 'app/includes/attraction/print/attraction-print-musandam-kasab.html'}).
	when('/attraction-print-maritime-museum','attraction-print-maritime-museum').segment('attraction-print-maritime-museum', { templateUrl: 'app/includes/attraction/print/attraction-print-maritime-museum.html'}).
	when('/attraction-print-sharjah-citytour','attraction-print-sharjah-citytour').segment('attraction-print-sharjah-citytour', { templateUrl: 'app/includes/attraction/print/attraction-print-sharjah-citytour.html'}).
	when('/attraction-print-mahatta-museum','attraction-print-mahatta-museum').segment('attraction-print-mahatta-museum', { templateUrl: 'app/includes/attraction/print/attraction-print-mahatta-museum.html'}).
	when('/attraction-print-culture-tour','attraction-print-culture-tour').segment('attraction-print-culture-tour', { templateUrl: 'app/includes/attraction/print/attraction-print-culture-tour.html'}).
	when('/attraction-print-almontazah-park','attraction-print-almontazah-park').segment('attraction-print-almontazah-park', { templateUrl: 'app/includes/attraction/print/attraction-print-almontazah-park.html'}).
	when('/attraction-print-arabias-wildlife','attraction-print-arabias-wildlife').segment('attraction-print-arabias-wildlife', { templateUrl: 'app/includes/attraction/print/attraction-print-arabias-wildlife.html'}).
	when('/attraction-print-dubai-tour','attraction-print-dubai-tour').segment('attraction-print-dubai-tour', { templateUrl: 'app/includes/attraction/print/attraction-print-dubai-tour.html'}).
	when('/attraction-print-burj-khalifa','attraction-print-burj-khalifa').segment('attraction-print-burj-khalifa', { templateUrl: 'app/includes/attraction/print/attraction-print-burj-khalifa.html'}).
	when('/attraction-print-abudhabi','attraction-print-abudhabi').segment('attraction-print-abudhabi', { templateUrl: 'app/includes/attraction/print/attraction-print-abudhabi.html'}).
	when('/attraction-print-atlantis','attraction-print-atlantis').segment('attraction-print-atlantis', { templateUrl: 'app/includes/attraction/print/attraction-print-atlantis.html'}).
	when('/attraction-print-cruise','attraction-print-cruise').segment('attraction-print-cruise', { templateUrl: 'app/includes/attraction/print/attraction-print-cruise.html'}).
	when('/attraction-print-dolphinarium','attraction-print-dolphinarium').segment('attraction-print-dolphinarium', { templateUrl: 'app/includes/attraction/print/attraction-print-dolphinarium.html'}).
	when('/attraction-print-fujaira-tour','attraction-print-fujaira-tour').segment('attraction-print-fujaira-tour', { templateUrl: 'app/includes/attraction/print/attraction-print-fujaira-tour.html'}).
	when('/attraction-print-hotair-balloon','attraction-print-hotair-balloon').segment('attraction-print-hotair-balloon', { templateUrl: 'app/includes/attraction/print/attraction-print-hotair-balloon.html'}).
	when('/attraction-print-yaswater','attraction-print-yaswater').segment('attraction-print-yaswater', { templateUrl: 'app/includes/attraction/print/attraction-print-yaswater.html'}).
	when('/attraction-print-sea-cruising','attraction-print-sea-cruising').segment('attraction-print-sea-cruising', { templateUrl: 'app/includes/attraction/print/attraction-print-sea-cruising.html'}).
	when('/attraction-print-sea-fishing','attraction-print-sea-fishing').segment('attraction-print-sea-fishing', { templateUrl: 'app/includes/attraction/print/attraction-print-sea-fishing.html'}).
	when('/attraction-print-skidubai','attraction-print-skidubai').segment('attraction-print-skidubai', { templateUrl: 'app/includes/attraction/print/attraction-print-skidubai.html'}).
	when('/attraction-print-themepark','attraction-print-themepark').segment('attraction-print-themepark', { templateUrl: 'app/includes/attraction/print/attraction-print-themepark.html'}).
	when('/attraction-print-wildwadia','attraction-print-wildwadia').segment('attraction-print-wildwadia', { templateUrl: 'app/includes/attraction/print/attraction-print-wildwadia.html'}).
	when('/attraction-print-wonder-bus','attraction-print-wonder-bus').segment('attraction-print-wonder-bus', { templateUrl: 'app/includes/attraction/print/attraction-print-wonder-bus.html'}).
	when('/tour-details-salalah','tour-detail-salalah').segment('tour-detail-salalah', { templateUrl: 'app/includes/packages/details/tour-detail-salalah.html', controller:'packagesDetailCtrl'}).
	when('/tour-details-istanbul','tourDetailIstanbul').segment('tourDetailIstanbul', { templateUrl: 'app/includes/packages/details/tour-detail-istanbul.html', controller:'packagesDetailCtrl'}).
	when('/tour-details-goa','tourDetailGoa').segment('tourDetailGoa', { templateUrl: 'app/includes/packages/details/tour-detail-goa.html', controller:'packagesDetailCtrl'}).
	when('/tour-details-sofia','tourDetailSofia').segment('tourDetailSofia', { templateUrl: 'app/includes/packages/details/tour-detail-sofia.html', controller:'packagesDetailCtrl'}).
	when('/tour-details-srilanka','tourDetailSrilanka').segment('tourDetailSrilanka', { templateUrl: 'app/includes/packages/details/tour-detail-srilanka.html', controller:'packagesDetailCtrl'}).
	when('/tour-details-bangkok','tourDetailBangkok').segment('tourDetailBangkok', { templateUrl: 'app/includes/packages/details/tour-detail-bangkok.html', controller:'packagesDetailCtrl'}).
	when('/tour-details-thailand','tourDetailThailand').segment('tourDetailThailand', { templateUrl: 'app/includes/packages/details/tour-detail-thailand.html', controller:'packagesDetailCtrl'}).
	when('/tour-details-georgia','tour-details-georgia').segment('tour-details-georgia', { templateUrl: 'app/includes/packages/details/tour-detail-georgia.html', controller:'packagesDetailCtrl'}).
	when('/tour-details-singapore','tourDetailSingapore').segment('tourDetailSingapore', { templateUrl: 'app/includes/packages/details/tour-detail-singapore.html', controller:'packagesDetailCtrl'}).
	when('/tour-details-malaysia','tourDetailMalaysia').segment('tourDetailMalaysia', { templateUrl: 'app/includes/packages/details/tour-detail-malaysia.html', controller:'packagesDetailCtrl'}).
	when('/tour-details-bosnia','tourDetailBosnia').segment('tourDetailBosnia', { templateUrl: 'app/includes/packages/details/tour-detail-bosnia.html', controller:'packagesDetailCtrl'}).
	when('/tour-details-europe1','tourDetailEurope').segment('tourDetailEurope', { templateUrl: 'app/includes/packages/details/tour-detail-europe1.html', controller:'packagesDetailCtrl'}).
	when('/tour-details-europe2','tourDetailEuropes').segment('tourDetailEuropes', { templateUrl: 'app/includes/packages/details/tour-detail-europe2.html', controller:'packagesDetailCtrl'}).
	when('/tour-details-waltdisney','tourDetailwaltdisney').segment('tourDetailwaltdisney', { templateUrl: 'app/includes/packages/details/tour-detail-waltdisney.html', controller:'packagesDetailCtrl'}).
	when('/tour-details-toronto','tourDetailtoronto').segment('tourDetailtoronto', { templateUrl: 'app/includes/packages/details/tour-detail-toronto.html', controller:'packagesDetailCtrl'}).
	when('/tour-details-peru','tourDetailperu').segment('tourDetailperu', { templateUrl: 'app/includes/packages/details/tour-detail-peru.html', controller:'packagesDetailCtrl'}).
	when('/tour-details-argentina','tourDetailargentina').segment('tourDetailargentina', { templateUrl: 'app/includes/packages/details/tour-detail-argentina.html', controller:'packagesDetailCtrl'}).
	when('/tour-details-lisbon','tourDetaillisbon').segment('tourDetaillisbon', { templateUrl: 'app/includes/packages/details/tour-detail-lisbon.html', controller:'packagesDetailCtrl'}).
	when('/tour-details-paris','tourDetailparis').segment('tourDetailparis', { templateUrl: 'app/includes/packages/details/tour-detail-paris.html', controller:'packagesDetailCtrl'}).
	when('/tour-details-london','tourDetaillondon').segment('tourDetaillondon', { templateUrl: 'app/includes/packages/details/tour-detail-london.html', controller:'packagesDetailCtrl'}).
	when('/tour-details-auckland','tourDetailauckland').segment('tourDetailauckland', { templateUrl: 'app/includes/packages/details/tour-detail-auckland.html', controller:'packagesDetailCtrl'}).
	when('/tour-details-bucharest','tourDetailbucharest').segment('tourDetailbucharest', { templateUrl: 'app/includes/packages/details/tour-detail-bucharest.html', controller:'packagesDetailCtrl'}).
	when('/tour-details-goldCoast','tourDetailgoldCoast').segment('tourDetailgoldCoast', { templateUrl: 'app/includes/packages/details/tour-detail-gold_coast.html', controller:'packagesDetailCtrl'}).
	when('/tour-details-hongKong','tourDetailhongKong').segment('tourDetailhongKong', { templateUrl: 'app/includes/packages/details/tour-detail-hongKong.html', controller:'packagesDetailCtrl'}).
	when('/tour-details-larnaca','tourDetaillarnaca').segment('tourDetaillarnaca', { templateUrl: 'app/includes/packages/details/tour-detail-larnaca.html', controller:'packagesDetailCtrl'}).
	when('/tour-details-zegreb','tourDetailzegreb').segment('tourDetailzegreb', { templateUrl: 'app/includes/packages/details/tour-detail-zegreb.html', controller:'packagesDetailCtrl'}).
	when('/tour-details-nairobi','tourDetailnairobi').segment('tourDetailnairobi', { templateUrl: 'app/includes/packages/details/tour-detail-nairobi.html', controller:'packagesDetailCtrl'}).
	when('/tour-details-yerevan','tourDetailyerevan').segment('tourDetailyerevan', { templateUrl: 'app/includes/packages/details/tour-detail-yerevan.html', controller:'packagesDetailCtrl'}).
	when('/tour-details-cairo','tourDetailcairo').segment('tourDetailcairo', { templateUrl: 'app/includes/packages/details/tour-detail-cairo.html', controller:'packagesDetailCtrl'}).
	when('/tour-details-barcelona','tourDetailbarcelona').segment('tourDetailbarcelona', { templateUrl: 'app/includes/packages/details/tour-detail-barcelona.html', controller:'packagesDetailCtrl'}).
	when('/tour-details-male','tourDetailmale').segment('tourDetailmale', { templateUrl: 'app/includes/packages/details/tour-detail-male.html', controller:'packagesDetailCtrl'}).
	when('/tour-details-malta','tourDetailmalta').segment('tourDetailmalta', { templateUrl: 'app/includes/packages/details/tour-detail-malta.html', controller:'packagesDetailCtrl'}).
	when('/tour-details-rome','tourDetailrome').segment('tourDetailrome', { templateUrl: 'app/includes/packages/details/tour-detail-rome.html', controller:'packagesDetailCtrl'}).
	when('/tour-details-bali','tourDetailbali').segment('tourDetailbali', { templateUrl: 'app/includes/packages/details/tour-detail-bali.html', controller:'packagesDetailCtrl'}).
	when('/tour-details-amman','tourDetailamman').segment('tourDetailamman', { templateUrl: 'app/includes/packages/details/tour-detail-amman.html', controller:'packagesDetailCtrl'}).
	when('/tour-details-baku','tourDetailbaku').segment('tourDetailbaku', { templateUrl: 'app/includes/packages/details/tour-detail-baku.html', controller:'packagesDetailCtrl'}).
	when('/tour-details-kerala','tourDetailkerala').segment('tourDetailkerala', { templateUrl: 'app/includes/packages/details/tour-detail-kerala.html', controller:'packagesDetailCtrl'}).
	when('/tour-details-mauritius','tourDetailmauritius').segment('tourDetailmauritius', { templateUrl: 'app/includes/packages/details/tour-detail-mauritius.html', controller:'packagesDetailCtrl'}).
	when('/tour-details-nepal','tourDetailnepal').segment('tourDetailnepal', { templateUrl: 'app/includes/packages/details/tour-detail-nepal.html', controller:'packagesDetailCtrl'}).
	when('/tour-details-mahe','tourDetailmahe').segment('tourDetailmahe', { templateUrl: 'app/includes/packages/details/tour-detail-mahe.html', controller:'packagesDetailCtrl'}).
	when('/tour-details-taipei','tourDetailtaipei').segment('tourDetailtaipei', { templateUrl: 'app/includes/packages/details/tour-detail-taipei.html', controller:'packagesDetailCtrl'}).
	when('/tour-print-salalah','printSalalah').segment('printSalalah', { templateUrl: 'app/includes/packages/print/tour-print-salalah.html', controller:'packagesDetailCtrl'}).
	when('/tour-print-istanbul','printIstanbul').segment('printIstanbul', { templateUrl: 'app/includes/packages/print/tour-print-istanbul.html', controller:'packagesDetailCtrl'}).
	when('/tour-print-goa','printGoa').segment('printGoa', { templateUrl: 'app/includes/packages/print/tour-print-goa.html', controller:'packagesDetailCtrl'}).
	when('/tour-print-sofia','printSofia').segment('printSofia', { templateUrl: 'app/includes/packages/print/tour-print-sofia.html', controller:'packagesDetailCtrl'}).
	when('/tour-print-srilanka','printSrilanka').segment('printSrilanka', { templateUrl: 'app/includes/packages/print/tour-print-srilanka.html', controller:'packagesDetailCtrl'}).
	when('/tour-print-bangkok','printBangkok').segment('printBangkok', { templateUrl: 'app/includes/packages/print/tour-print-bangkok.html', controller:'packagesDetailCtrl'}).
	when('/tour-print-georgia','tour-print-georgia').segment('tour-print-georgia', { templateUrl: 'app/includes/packages/print/tour-print-georgia.html', controller:'packagesDetailCtrl'}).
	when('/tour-print-singapore','printSingapore').segment('printSingapore', { templateUrl: 'app/includes/packages/print/tour-print-singapore.html', controller:'packagesDetailCtrl'}).
	when('/tour-print-bosnia','printBosnia').segment('printBosnia', { templateUrl: 'app/includes/packages/print/tour-print-bosnia.html', controller:'packagesDetailCtrl'}).
	when('/tour-print-malaysia','printMalaysia').segment('printMalaysia', { templateUrl: 'app/includes/packages/print/tour-print-malaysia.html', controller:'packagesDetailCtrl'}).
	when('/tour-print-europe1','printEurope1').segment('printEurope1', { templateUrl: 'app/includes/packages/print/tour-print-europe1.html', controller:'packagesDetailCtrl'}).
	when('/tour-print-europe2','printEurope2').segment('printEurope2', { templateUrl: 'app/includes/packages/print/tour-print-europe2.html', controller:'packagesDetailCtrl'}).
	when('/tour-print-waltdisney','printwaltdisney').segment('printwaltdisney', { templateUrl: 'app/includes/packages/print/tour-print-waltdisney.html', controller:'packagesDetailCtrl'}).
	when('/tour-print-toronto','printtoronto').segment('printtoronto', { templateUrl: 'app/includes/packages/print/tour-print-toronto.html', controller:'packagesDetailCtrl'}).
	when('/tour-print-peru','printperu').segment('printperu', { templateUrl: 'app/includes/packages/print/tour-print-peru.html', controller:'packagesDetailCtrl'}).
	when('/tour-print-argentina','printargentina').segment('printargentina', { templateUrl: 'app/includes/packages/print/tour-print-argentina.html', controller:'packagesDetailCtrl'}).
	when('/tour-print-lisbon','printlisbon').segment('printlisbon', { templateUrl: 'app/includes/packages/print/tour-print-lisbon.html', controller:'packagesDetailCtrl'}).
	when('/tour-print-paris','printparis').segment('printparis', { templateUrl: 'app/includes/packages/print/tour-print-paris.html', controller:'packagesDetailCtrl'}).
	when('/tour-print-london','printlondon').segment('printlondon', { templateUrl: 'app/includes/packages/print/tour-print-london.html', controller:'packagesDetailCtrl'}).
	when('/tour-print-auckland','printauckland').segment('printauckland', { templateUrl: 'app/includes/packages/print/tour-print-auckland.html', controller:'packagesDetailCtrl'}).
	when('/tour-print-bucharest','printbucharest').segment('printbucharest', { templateUrl: 'app/includes/packages/print/tour-print-bucharest.html', controller:'packagesDetailCtrl'}).
	when('/tour-print-goldCoast','printgoldCoast').segment('printgoldCoast', { templateUrl: 'app/includes/packages/print/tour-print-gold_coast.html', controller:'packagesDetailCtrl'}).
	when('/tour-print-hongKong','printhongKong').segment('printhongKong', { templateUrl: 'app/includes/packages/print/tour-print-hongKong.html', controller:'packagesDetailCtrl'}).
	when('/tour-print-larnaca','printlarnaca').segment('printlarnaca', { templateUrl: 'app/includes/packages/print/tour-print-larnaca.html', controller:'packagesDetailCtrl'}).
	when('/tour-print-zegreb','printzegreb').segment('printzegreb', { templateUrl: 'app/includes/packages/print/tour-print-zegreb.html', controller:'packagesDetailCtrl'}).
	when('/tour-print-nairobi','printnairobi').segment('printnairobi', { templateUrl: 'app/includes/packages/print/tour-print-nairobi.html', controller:'packagesDetailCtrl'}).
	when('/tour-print-yerevan','printyerevan').segment('printyerevan', { templateUrl: 'app/includes/packages/print/tour-print-yerevan.html', controller:'packagesDetailCtrl'}).
	when('/tour-print-cairo','printcairo').segment('printcairo', { templateUrl: 'app/includes/packages/print/tour-print-cairo.html', controller:'packagesDetailCtrl'}).
	when('/tour-print-barcelona','printbarcelona').segment('printbarcelona', { templateUrl: 'app/includes/packages/print/tour-print-barcelona.html', controller:'packagesDetailCtrl'}).
	when('/tour-print-male','printmale').segment('printmale', { templateUrl: 'app/includes/packages/print/tour-print-male.html', controller:'packagesDetailCtrl'}).
	when('/tour-print-malta','printmalta').segment('printmalta', { templateUrl: 'app/includes/packages/print/tour-print-malta.html', controller:'packagesDetailCtrl'}).
	when('/tour-print-rome','printrome').segment('printrome', { templateUrl: 'app/includes/packages/print/tour-print-rome.html', controller:'packagesDetailCtrl'}).
	when('/tour-print-bali','printbali').segment('printbali', { templateUrl: 'app/includes/packages/print/tour-print-bali.html', controller:'packagesDetailCtrl'}).
	when('/tour-print-amman','printamman').segment('printamman', { templateUrl: 'app/includes/packages/print/tour-print-amman.html', controller:'packagesDetailCtrl'}).
	when('/tour-print-baku','printbaku').segment('printbaku', { templateUrl: 'app/includes/packages/print/tour-print-baku.html', controller:'packagesDetailCtrl'}).
	when('/tour-print-kerala','printkerala').segment('printkerala', { templateUrl: 'app/includes/packages/print/tour-print-kerala.html', controller:'packagesDetailCtrl'}).
	when('/tour-print-mauritius','printmauritius').segment('printmauritius', { templateUrl: 'app/includes/packages/print/tour-print-mauritius.html', controller:'packagesDetailCtrl'}).
	when('/tour-print-nepal','printnepal').segment('printnepal', { templateUrl: 'app/includes/packages/print/tour-print-nepal.html', controller:'packagesDetailCtrl'}).
	when('/tour-print-mahe','printmahe').segment('printmahe', { templateUrl: 'app/includes/packages/print/tour-print-mahe.html', controller:'packagesDetailCtrl'}).
	when('/tour-print-taipei','printtaipei').segment('printtaipei', { templateUrl: 'app/includes/packages/print/tour-print-taipei.html', controller:'packagesDetailCtrl'}).
	when('/our-business','business').segment('business', { templateUrl: 'app/includes/our-business.html'}).
	when('/copthorne','copthorne').segment('copthorne', { templateUrl: 'app/includes/copthorne.html'}).
	when('/royal','royal').segment('royal', { templateUrl: 'app/includes/royal.html'}).
	when('/resort','resort').segment('resort', { templateUrl: 'app/includes/resort.html'}).
	when('/promotions','promotions').segment('promotions', { templateUrl: 'app/includes/promotions.html'}).
	when('/contact-us','contact').segment('contact', { templateUrl: 'app/includes/contact-us.html'}).
	when('/terms','terms').segment('terms', { templateUrl: 'app/includes/terms.html'}).
	when('/privacy-policy','privacy').segment('privacy', { templateUrl: 'app/includes/privacy.html'}).
	when('/our-services','our-services').segment('our-services', { templateUrl: 'app/includes/our-services/our-services.html', controller:'ourServiceCtrl'}).
	when('/our-services-meet-greet','our-services.meet-greet').within('our-services').segment('meet-greet', { templateUrl: 'app/includes/our-services/meet-greet.html', controller:'ourServiceCtrl'}).
	up().
	when('/our-services-transit-hotel','our-services.transit-hotel').within('our-services').segment('transit-hotel', { templateUrl: 'app/includes/our-services/transit-hotel.html', controller:'ourServiceCtrl'}).
	up().
	when('/our-services-business-travel','our-services.business-travel').within('our-services').segment('business-travel', { templateUrl: 'app/includes/our-services/business-travel.html', controller:'ourServiceCtrl'}).
	up().
	when('/our-services-car-rental','our-services.car-rental').within('our-services').segment('car-rental', { templateUrl: 'app/includes/our-services/car-rental.html', controller:'ourServiceCtrl'}).
	up().
	when('/our-services-edu-trips','our-services.edu-trips').within('our-services').segment('edu-trips', { templateUrl: 'app/includes/our-services/edu-trips.html', controller:'ourServiceCtrl'}).
	up().
	when('/our-services-europe-rail','our-services.europe-rail').within('our-services').segment('europe-rail', { templateUrl: 'app/includes/our-services/europe-rail.html', controller:'ourServiceCtrl'}).
	up().
	when('/our-services-mice','our-services.mice').within('our-services').segment('mice', { templateUrl: 'app/includes/our-services/mice.html', controller:'ourServiceCtrl'}).
	up().
	when('/our-services-strategic-partners','our-services.strategic-partners').within('our-services').segment('strategic-partners', { templateUrl: 'app/includes/our-services/strategic-partners.html', controller:'ourServiceCtrl'}).
	up().
	when('/our-services-travel-insurance','our-services.travel-insurance').within('our-services').segment('travel-insurance', { templateUrl: 'app/includes/our-services/travel-insurance.html', controller:'ourServiceCtrl'}).
	up().
	when('/our-services-visa-change','our-services.visa-change').within('our-services').segment('visa-change', { templateUrl: 'app/includes/our-services/visa-change.html', controller:'ourServiceCtrl'}).
	up().
	when('/visa','visa').segment('visa', { templateUrl: 'app/includes/visa/visa.html', controller:'visaCtrl'}).
	when('/vision','vision').segment('vision', { templateUrl: 'app/includes/visa/vision.html', controller:'visaCtrl'}).
	when('/visa-Copy','visa-Copy').segment('visa-Copy', { templateUrl: 'app/includes/visa/visa1.html', controller:'visaCtrl'}).
	when('/visa-detail','visa-detail').segment('visa-detail', { templateUrl: 'app/includes/visa/visa-detail.html', controller:'visaDetailCtrl'}).
	when('/visa-apply','visa-apply').segment('visa-apply', { templateUrl: 'app/includes/visa/visa-apply.html', controller:'visaApplyCtrl'}).
	when('/visa-itinerary','visa-itinerary').segment('visa-itinerary', { templateUrl: 'app/includes/visa/visa-itinerary.html', controller:'visaItineraryCtrl'}).
	when('/confirmed-visa-itinerary','visa-itinerary').segment('visa-itinerary', { templateUrl: 'app/includes/visa/visa-itinerary.html', controller:'visaItineraryCtrl'}).
	when('/notification','notification').segment('notification', { templateUrl: 'app/includes/notification.html'}).
	when('/visa-worldwide','visa-worldwide').segment('visa-worldwide', { templateUrl: 'app/includes/visa/visa-worldwide.html'});
});
app.value('mailId', 'customerexperiencebh@hsbc.com');
app.service("ajaxService", function($http, $q) {
	var self =  this;
	self.data = null;
	self.ajaxRequest = function(met,url,req,res) {
		var deferred = $q.defer();
		if(self.data !== null) {
			deferred.resolve(self.data);
			} else {
			$http({
				method: met,
				url: url,
				data:req,
				responseType:res
				}).then(function(response) {
				//self.data = response.data;
				deferred.resolve(response.data);
				}).catch(function(response) {
				deferred.reject(response);
			});
		}
		return deferred.promise;
	};
});
app.run(function ($rootScope, $location,$route, $timeout, $http, $window) {
    $rootScope.config = {};
    $rootScope.config.app_url = $location.url();
    $rootScope.config.app_path = $location.path();
    $rootScope.layout = {};
    $rootScope.layout.loading = false;
    $rootScope.$on('$routeChangeStart', function () {
		$rootScope.saveVisa = {};
		if(location.hash != '#/get-pnr' && location.hash != '#/ticket') {
			sessionStorage.removeItem('tfilterBy');
		}
		if(location.hash != '#/get-pnr' && location.hash != '#/dockets') {
			sessionStorage.removeItem('dfilterBy');
		}
		var x;
		x = $("#result-area").detach();
		$('#booking-summary').detach();
		//show loading gif
		$rootScope.layout.loading = true;
		noBack();
	});
    $rootScope.$on('$routeChangeSuccess', function () {
		//hide loading gif
    	var baseUrl = $location.absUrl().indexOf("home.jsf") > -1 ? true : false;
		if(baseUrl) {
			var path = 'en' + $location.path();
			ga('send', 'pageview', path);
		}
		$rootScope.layout.loading = false;
		yesBack();
		$timeout(function(){  
			$('[autofocus]').focus();
			$window.scrollTo(0, 0);
		},100);
		// $rootScope.isItinerary = $location.path().search('itinerary');
		// if($rootScope.isItinerary>0) {
			// location.assign(location.href.replace('arab.jsf','home.jsf'));
		// }
	});
	$rootScope.$on('$routeChangeError', function () {
		
		//hide loading gif
		alert('wtff');
		$rootScope.layout.loading = false;
		
	});
});
app.service('ServerService',function($location){
	this.serverPath = $location.absUrl().split('home.jsf')[0];
});

app.service('NationService', function($http,$rootScope,ServerService) {
	this.nationGet = function() {
		var nationality = new Object(); 
		nationality.queryString = "";
		$http({
			method:'POST',
			url:ServerService.serverPath+'rest/repo/nationality',		
			data:nationality
			}).success(function(data,status){
			if(status ='success') {
				$rootScope.nationalityResult = data;
				} else {
				alert('error');
			}
			
		});
	};
});

app.service('ImagePath', function($scope, $rootScope){
	$rootScope.imageSrc = 'app/images/danata';
	console.log("ImagePath "+$rootScope.imageSrc);
	
});
app.service('AgencyService', function($http,$rootScope,ServerService) {
	this.agencyName = function() {
		var agency = new Object();
		agency.queryString = "";
		$http({
			method:'GET',
			url:ServerService.serverPath+'rest/repo/agency',
			data:agency
			}).success(function(data,status){
			if(status ='success') {
				$rootScope.agentNames = data;
				} else {
				alert('error');
			}
			
		});
	};
});
app.service('CountryService', function($http,$rootScope,ServerService) {
	this.countryGet = function() {
		var country = new Object();
		country.queryString = ""; 
		$http({
			method:'POST',
			url:ServerService.serverPath+'rest/repo/country',  
			data:country
			}).success(function(data,status){
			if(status ='success') {
				$rootScope.countryResult = data;
				} else {
				alert('error');
			}
		});
	};
});
app.service('ImageService',function($rootScope){
	this.images = 'app/images/danata';
	this.loading = 'app/images/loaders/loading.gif'
});


app.factory('renderHTMLFactory', function() {
	var renderObj = {};
	renderObj.renderHTML = function(html_code) {
		var decoded = angular.element('<textarea />').html(html_code).text();
		return decoded;
	}
	renderObj.setTitle = function(title) {
		
		var setTitleVal;
		var spltTitle = title.search('Family');
		if(spltTitle!==-1){
			var spltTitle = title.split('Family');
			setTitleVal = "Family  "+spltTitle[1];
			}else{
			var spltTitle = title.search('-');
			if(spltTitle!=-1){
				var spltTitle = title.split('-');
				setTitleVal = "Individual  "+spltTitle[1];
				}else{
				var spltTitle = title.split('After');
				setTitleVal = "Individual  "+spltTitle[1];
			}
		}
		return setTitleVal;
	}
	return renderObj;
});
var INTEGER_REGEXP = /^[A-Za-z0-9]+$/;
app.directive('integer', function() {
	return {
		require: 'ngModel',
		link: function(scope, elm, attrs, ctrl) {
			ctrl.$validators.integer = function(modelValue, viewValue) {
				if (ctrl.$isEmpty(modelValue)) {
					// consider empty models to be valid
					return true;
				}
				
				if (INTEGER_REGEXP.test(viewValue)) {
					// it is valid
					return true;
				}
				
				// it is invalid
				return false;
			};
		}
	};
});

var PHONE_REGEXP = /^[-+0-9]+$/;
app.directive('phone', function() {
	return {
		require: 'ngModel',
		link: function(scope, elm, attrs, ctrl) {
			ctrl.$validators.phone = function(modelValue, viewValue) {
				if (ctrl.$isEmpty(modelValue)) {
					// consider empty models to be valid
					return true;
				}
				
				if (PHONE_REGEXP.test(viewValue)) {
					// it is valid
					return true;
				}
				
				// it is invalid
				return false;
			};
		}
	};
});
app.filter('unique', function() {
   return function(collection, keyname) {
      var output = [], 
          keys = [];

      angular.forEach(collection, function(item) {
          var key = item[keyname];
          if(keys.indexOf(key) === -1) {
              keys.push(key);
              output.push(item);
          }
      });

      return output;
   };
});
 app.service('ConstantService', function(){
	this.currency = 'BHD'
	
}) 
