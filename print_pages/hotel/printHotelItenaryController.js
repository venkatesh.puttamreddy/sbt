var app = angular.module("ouaApp", ['ngSanitize']);

app.controller('hotelItenaryController', function($scope, $http,$window){
$scope.hotelJson = JSON.parse(sessionStorage.getItem("eTicketHotelJson"));//Retrieve the stored data

$scope.navigation_Url=sessionStorage.getItem("navUrl");
$scope.Navigate_To = function() {
	// console.log("$scope.navigation_Url");
	// $window.location.href=$scope.navigation_Url;
	location.replace(navigation_Url);
};
}).filter('unique', function () {

  return function (items, filterOn) {

    if (filterOn === false) {
      return items;
    }

    if ((filterOn || angular.isUndefined(filterOn)) && angular.isArray(items)) {
      var hashCheck = {}, newItems = [];

      var extractValueToCompare = function (item) {
        if (angular.isObject(item) && angular.isString(filterOn)) {
          return item[filterOn];
        } else {
          return item;
        }
      };

      angular.forEach(items, function (item) {
        var valueToCheck, isDuplicate = false;

        for (var i = 0; i < newItems.length; i++) {
          if (angular.equals(extractValueToCompare(newItems[i]), extractValueToCompare(item))) {
            isDuplicate = true;
            break;
          }
        }
        if (!isDuplicate) {
          newItems.push(item);
        }

      });
      items = newItems;
    }
    return items;
  };
});