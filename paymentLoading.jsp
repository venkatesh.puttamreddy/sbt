<html> 
<body onload="myFunction()">
<script>
function myFunction() {
    var url = '<%= (String) request.getAttribute("url")%>';
    var externalReq = '<%= request.getAttribute("externalReq") %>';
    if (externalReq == 'true') {
    	self.location.href = url; // This is for mobile or external Device.
    } else {
    	window.top.location.href = url; // This is for web browser.
    }
}
</script>
</body>
</html>