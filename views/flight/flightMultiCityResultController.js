ouaApp.controller('flightMultiCityResultController', function($scope, $http, $location, $uibModal, $log, $document, $rootScope, ConstantService, stopsService, ServerService, $timeout, customSession) {
    $('html, body').scrollTop(0);
    $scope.currency = ConstantService.currency;
    $scope.loading = false;
    $scope.disabled = false;
    $scope.result = JSON.parse(sessionStorage.getItem('flightTotalData'));
    $scope.isAllStopsSelected = true;
    $scope.isAllAirLinesSelected = true;
    $scope.flightResultList = $scope.result.data;
    $scope.flightResultList1 = $scope.flightResultList.flightSearchResults
    for (var i = 0; i < $scope.flightResultList1; i++) {

        //	console.log($scope.flightResultList1)
    }

    sessionStorage.removeItem("flightFilterData");
    $scope.totalFlight = $scope.flightResultList.flightSearchResults;
    history.pushState(null, null, location.href);
    window.onpopstate = function() {
        history.go(1);
    };
    $scope.isCollapsed = true;
    $scope.date = {
        startDate: moment(),
        endDate: moment(),
        locale: {
            format: "D-MMM-YY"
        }
    };


    $(document).ready(function() {
        $(".js-range-slider").ionRangeSlider({
            type: "double",
            min: 150,
            max: 3000,
            from: 200,
            to: 2800,
            grid: true,
            prefix: "AED ",
        });

        var start = moment("2016-10-02 00:00", "YYYY-MM-DD HH:mm");
        var end = moment("2016-10-02 23:59", "YYYY-MM-DD HH:mm");

        $(".date-range-slider, .arrival-range-slider").ionRangeSlider({
            type: "double",
            grid: true,
            min: start.format("x"),
            max: end.format("x"),
            step: 1800000,
            prettify: function(num) {
                return moment(num, "x").format("h:mm A");
            },
        });

        $(".refine-search").click(function() {
            $(".flight-search").slideToggle();
        });
        $(".only-link").click(function(event) {
            event.preventDefault();
            let parent = $(this).closest(".checkbox-container").parent();
            let currentCb = $(this).closest(".checkbox-label");
            $("input", parent).prop("checked", false);
            $("input", currentCb).prop("checked", true);
        });

        $sidebar = $("#filterSideBar");
        $window = $(window);
        var sidebarOffset = $sidebar.offset();

        // $("#filterSideBar").sticky({ topSpacing: 150 });
        // $("#filterSideBar").on("bottom-reached", function () {
        //   console.log("bottom-reached");
        // });

        $(window).scroll(function() {
            var sticky = $("#filterSideBar"),
                scroll = $(window).scrollTop();
            var footerScroll =
                $(".footer").offset().top -
                $(".footer").height() -
                $(".header").height() -
                60;
            //$(".footer").first().scrollTop();
            	  console.log("footerScroll13454564", scroll, footerScroll);
            if (scroll >= 150 && scroll < footerScroll)
                sticky.addClass("sticky-sidebar");
            else sticky.removeClass("sticky-sidebar");
        });

        $('[data-toggle="tab"]').click(function() {
            let _href = $(this).attr("data-flightType");
            if (_href === "oneWay") {
                $(".flyingto").slideUp();
            } else {
                $(".flyingto").slideDown();
            }
        });

        // $window.scroll(function () {
        //   if ($window.scrollTop() > sidebarOffset.top) {
        //     $sidebar.css({
        //       "max-height": $window.height() - 160 + "px",
        //       overflow: "auto",
        //       position: "fixed",
        //       left: 50,
        //       width: 284,
        //     });
        //     $sidebar.stop().animate({
        //       top: 150,-
        //     });
        //   } else {
        //     $sidebar.css({
        //       "max-height": "unset",
        //       overflow: "unset",
        //       position: "static",
        //       width: "auto",
        //     });
        //     $sidebar.stop().animate({
        //       marginTop: 0,
        //     });
        //   }
        // });
    });

    $scope.flightResults = {}
    $scope.flightResults.flightSearchResults = [];
    $scope.maximum = $scope.flightResultList.filterParams.priceBean.maxPrice;
    $scope.minimum = $scope.flightResultList.filterParams.priceBean.minPrice;
    $scope.filtermodel = {};

    $scope.filtermodel.defaultSortType = "PRICE";
    $scope.filtermodel.selectedBound = "Outbound";
    $scope.filtermodel.isOnlySorting = false;

    $scope.filtermodel.pageIndex = 1;
    $scope.filtermodel.resultPerPage = 1000;
    $scope.filtermodel.defaultSortOrder = "ASC";
    $scope.filtermodel.priceBean = { "isEnabledPrice": true, "fromRange": $scope.minimum, "toRange": $scope.maximum };
    $scope.filtermodel.arrivalTimeBean = { "isEnabledArrival": true, "fromArrival": 0 * 60000, "toArrival": 1439 * 60000 };
    $scope.filtermodel.deptTimeBean = { "isEnabledDept": true, "fromDept": 0 * 60000, "toDept": 1439 * 60000 };

    $scope.filtermodel.durationBean = null;

    $scope.filtermodel.airlineBeanList = [];

    $scope.filtermodel.stopsBeanList = $scope.flightResultList.filterParams.stopsBeanList;
    $scope.filtermodel.providerBeanList = $scope.flightResultList.filterParams.providerBeanList;




    $scope.flightBook = function(index) {
        $('html, body').scrollTop(0);
        sessionStorage.setItem('flightIndex', JSON.stringify(index));
        $scope.loading = true;
        $scope.disabled = true;

        $http.get(ServerService.serverPath + 'rest/validate/session').then(function(response) {
            if (response.data.isSessionValid == true) {
                $scope.flightResults.frmAirprt = $scope.flightResultList.frmAirprt;
                $scope.flightResults.toAirprt = $scope.flightResultList.toAirprt;
                $scope.flightResults.adtCnt = $scope.flightResultList.adtCnt;
                $scope.flightResults.chldCnt = $scope.flightResultList.chldCnt;
                $scope.flightResults.infntCnt = $scope.flightResultList.infntCnt;
                $scope.flightResults.trvlType = $scope.flightResultList.trvlType;
                $scope.flightResults.srchType = $scope.flightResultList.srchType;
                $scope.flightResults.deptDate = $scope.flightResultList.deptDate;
                $scope.flightResults.arrDate = $scope.flightResultList.arrDate;
                $scope.flightResults.curncy = 'AED';
                $scope.flightResults.agncyConvrsnRate = $scope.flightResultList.agncyConvrsnRate;
                $scope.flightResults.clasType = $scope.flightResultList.clasType;
                if (!sessionStorage.getItem("flightFilterData")) {
                    $scope.flightResults.flightSearchResults.push($scope.flightResultList.flightSearchResults[index]);
                } else {
                    $scope.flightResults.flightSearchResults.push(JSON.parse(sessionStorage.getItem("flightFilterData"))[index]);
                }



                $http.post(ServerService.serverPath + 'rest/flight/price', $scope.flightResults, {
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                    }
                }).then(function(response) {
                    if (response.data.data != null || response.data.data != '') {
                        $scope.loading = false;
                        $scope.disabled = false;
                        $scope.flightPriceData = response.data.data
                        sessionStorage.setItem('flightPriceData', JSON.stringify($scope.flightPriceData));
                        $location.path('/flightDetails');
                    }
                })

            }
        })


    }


    var airlineCode;
    var stopIndex;
    $scope.airlineFilter;
    var filterDatas = {}


    for (var index in $scope.flightResultList.filterParams.airlineBeanList) {
        $scope.filtermodel.airlineBeanList.push({
            airlineCode: $scope.flightResultList.filterParams.airlineBeanList[index].airlineCode,
            airlineName: $scope.flightResultList.filterParams.airlineBeanList[index].airlineName,
            minFare: $scope.flightResultList.filterParams.airlineBeanList[index].minFare,
            count: $scope.flightResultList.filterParams.airlineBeanList[index].count
        });
    }


    $scope.minPrice = $scope.flightResultList.filterParams.priceBean.minPrice;
    $scope.maxPrice = $scope.flightResultList.filterParams.priceBean.maxPrice;




    /* stopsChange function */
    $scope.checkAll = false;
    $scope.filter1 = [];

    $scope.stopsObj = {}
    $scope.stopsObj.stopsList = [];

    $scope.stopsChange = function(index) {
        if ($scope.stopsObj.stopsList.length == $scope.flightResultList.filterParams.stopsBeanList.length)
            $scope.isAllStopsSelected = true;
        else
            $scope.isAllStopsSelected = false;

        $scope.filtermodel.stopsBeanList = [];
        for (var s in $scope.stopsObj.stopsList) {
            var targetStopsName = $scope.stopsObj.stopsList[s];
            for (var i in $scope.flightResultList.filterParams.stopsBeanList) {
                var orginalStopName = $scope.flightResultList.filterParams.stopsBeanList[i].stopName;
                if (targetStopsName == orginalStopName) {
                    $scope.filtermodel.stopsBeanList.push({
                        stopCode: $scope.flightResultList.filterParams.stopsBeanList[i].stopCode,
                        stopName: $scope.flightResultList.filterParams.stopsBeanList[i].stopName,
                        count: $scope.flightResultList.filterParams.stopsBeanList[i].count,
                        minFare: $scope.flightResultList.filterParams.stopsBeanList[i].minFare
                    });
                }
            }

        }

        if ($scope.stopsObj.stopsList != 0)
            $scope.filterParam($scope.filtermodel);
        else {
            $scope.flightResultList1 = [];
            $scope.totalFlight = $scope.flightResultList1;
        }
    }



    $scope.airLineObj = {};
    $scope.airLineObj.airLineList = [];
    $scope.stopsLists = $scope.flightResultList.filterParams.stopsBeanList
    $scope.airLineList = $scope.flightResultList.filterParams.airlineBeanList;

    $scope.toggleStops = function() {
            if (!$scope.checkAll) {
                $scope.checkAll = true;
                sessionStorage.removeItem("flightFilterData");
                $scope.isAllStopsSelected = true;
                $scope.stopsObj.stopsList = $scope.stopsLists.map(function(value) {
                    $scope.flightResultList1 = $scope.flightResultList.flightSearchResults;
                    $scope.totalFlight = $scope.flightResultList1;
                    return value.stopName;
                })
            } else {
                $scope.checkAll = false;
                $scope.isAllStopsSelected = false;
                $scope.stopsObj.stopsList = [];
                $scope.flightResultList1 = [];
                $scope.totalFlight = $scope.flightResultList1;
            }
        }
        /* end */

    /* toggleAirline function */

    $scope.toggleAirline = function() {
        if (!$scope.checkAll) {
            $scope.checkAll = true;
            sessionStorage.removeItem("flightFilterData");
            $scope.isAllAirLinesSelected = true;
            $scope.airLineObj.airLineList = $scope.airLineList.map(function(value) {
                $scope.flightResultList1 = $scope.flightResultList.flightSearchResults;
                $scope.totalFlight = $scope.flightResultList1;
                return value.airlineName;
            })
        } else {
            $scope.checkAll = false;
            $scope.isAllAirLinesSelected = false;
            $scope.airLineObj.airLineList = [];
            $scope.flightResultList1 = [];
            $scope.totalFlight = $scope.flightResultList1;
        }
    }

    $scope.airlineChange = function(index) {

        $scope.filtermodel.airlineBeanList = [];
        if ($scope.airLineObj.airLineList.length == $scope.flightResultList.filterParams.airlineBeanList.length)
            $scope.isAllAirLinesSelected = true;
        else
            $scope.isAllAirLinesSelected = false;

        for (var a in $scope.airLineObj.airLineList) {
            var targetAirLine = $scope.airLineObj.airLineList[a];

            for (var i in $scope.flightResultList.filterParams.airlineBeanList) {
                if ($scope.flightResultList.filterParams.airlineBeanList[i].airlineName == targetAirLine) {
                    $scope.filtermodel.airlineBeanList.push({
                        airlineCode: $scope.flightResultList.filterParams.airlineBeanList[i].airlineCode,
                        airlineName: $scope.flightResultList.filterParams.airlineBeanList[i].airlineName,
                        minFare: $scope.flightResultList.filterParams.airlineBeanList[i].minFare,
                        count: $scope.flightResultList.filterParams.airlineBeanList[i].count

                    });
                }
            }
        }

        if ($scope.airLineObj.airLineList != 0)
            $scope.filterParam($scope.filtermodel);
        else {
            $scope.flightResultList1 = [];
            $scope.totalFlight = $scope.flightResultList1;
        }

    }
    $scope.checkAllStops = function() {
        $scope.checkAll = true;
        sessionStorage.removeItem("flightFilterData");
        $scope.stopsObj.stopsList = $scope.stopsLists.map(function(value) {
            $scope.flightResultList1 = $scope.flightResultList.flightSearchResults;
            return value.stopName;
        })
    }

    $scope.checkAllAirline = function() {
        sessionStorage.removeItem("flightFilterData");
        $scope.checkAll = true;
        $scope.airLineObj.airLineList = $scope.airLineList.map(function(value) {
            $scope.flightResultList1 = $scope.flightResultList.flightSearchResults;
            return value.airlineName;
        })

    }
    $scope.filterParam = function(filtermodel) {
        $scope.flightResultList1 = [];
        $http.post(ServerService.serverPath + 'rest/flight/filter/apply', filtermodel, {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            }
        }).then(function(response) {
            $scope.flightResultList1 = response.data.data;
            $scope.totalFlight = $scope.flightResultList1;
            sessionStorage.setItem("flightFilterData", JSON.stringify($scope.flightResultList1))
        })
    }


    /* end */


    function toDate(dStr, format) {
        var now = new Date();
        if (format == "h:m") {
            now.setHours(dStr.substr(0, dStr.indexOf(":")));
            now.setMinutes(dStr.substr(dStr.indexOf(":") + 1));
            now.setSeconds(0);
            return now;
        } else
            return "Invalid Format";
    }




    /**
    support : siri end
     */

    /**
support:siri pathcs 
*/
    $scope.isMin = function(price) {
        var min = $scope.flightResultList.filterParams.stopsBeanList[0].minFare;
        for (var i = 0; i <= $scope.flightResultList.filterParams.stopsBeanList.length - 1; i++) {
            if (Number($scope.flightResultList.filterParams.stopsBeanList[i].minFare) < min) {
                min = $scope.flightResultList.filterParams.stopsBeanList[i].minFare;
            }
        }
        if (min === price) {
            return true;
        } else {
            return false;
        }
    }
    $scope.isairlineMin = function(price) {
        var min = $scope.flightResultList.filterParams.airlineBeanList[0].minFare;
        for (var i = 0; i <= $scope.flightResultList.filterParams.airlineBeanList.length - 1; i++) {
            if (Number($scope.flightResultList.filterParams.airlineBeanList[i].minFare) < min) {
                min = $scope.flightResultList.filterParams.airlineBeanList[i].minFare;
            }
        }
        if (min === price) {
            return true;
        } else {
            return false;
        }
    }

    $scope.pricerange = {
        minValue: $scope.flightResultList.filterParams.priceBean.minPrice,
        maxValue: $scope.flightResultList.filterParams.priceBean.maxPrice,
        options: {
            floor: $scope.flightResultList.filterParams.priceBean.minPrice,
            ceil: $scope.flightResultList.filterParams.priceBean.maxPrice,
            step: 1,
            minRange: 100,
            pushRange: true,
            showSelectionBar: true,
            getSelectionBarColor: function(value) {
                return '#fecb16';
            },
            id: 'sliderA',
            onChange: $scope.priceChange
        }
    };

    $(".refine-search").click(function() {
        $(".flight-search").slideToggle();
    });
    $("#departure-range").slider({
        range: true,
        min: 0,
        max: 1440,
        step: 5,
        values: [0, 1440],
        slide: function(e, ui) {
            var arhours1 = Math.floor(ui.values[0] / 60);
            var arseconds1 = ui.values[0];
            var arminutes1 = ui.values[0] - (arhours1 * 60);
            if (arhours1.length == 1) arhours1 = '0' + arhours1;
            if (arminutes1.length == 1) arminutes1 = '0' + arminutes1;
            if (arminutes1 >= 0 && arminutes1 <= 9) arminutes1 = '0' + arminutes1;
            if (arhours1 >= 0 && arhours1 <= 9) arhours1 = '0' + arhours1;
            arhours1 = arhours1;
            arminutes1 = arminutes1;
            $('.slider-dpt-time').html(arhours1 + ':' + arminutes1);

            var arhours2 = Math.floor(ui.values[1] / 60);
            var arseconds2 = ui.values[1];
            var arminutes2 = ui.values[1] - (arhours2 * 60);
            if (arhours2.length == 1) arhours2 = '0' + arhours2;
            if (arminutes2.length == 1) arminutes2 = '0' + arminutes2;
            if (arminutes2 >= 0 && arminutes2 <= 9) arminutes2 = '0' + arminutes2;
            if (arhours2 >= 0 && arhours2 <= 9) arhours2 = '0' + arhours2;
            arhours2 = arhours2;
            arhours2 = arhours2;

            $('.slider-dpt-time2').html(arhours2 + ':' + arhours2);

            var min = ui.values[0],
                max = ui.values[1];

            $scope.filtermodel.deptTimeBean = { "isEnabledDept": true, "fromDept": ui.values[0] * 60000, "toDept": ui.values[1] * 60000 };
            if ($scope.filtermodel.deptTimeBean.length == 0) {
                $scope.loadmore = false;
                $scope.filtermodel.deptTimeBean = '';

            } else {
                $scope.filterParam($scope.filtermodel);
            }
        }
    });

    // $("#arrival-range").slider({
    // 	range: true,
    // 	min: 0,
    // 	max: 1440,
    // 	step: 5,
    // 	values: [0, 1440],
    // 	slide: function (e, ui) {
    // 		var arhours1 = Math.floor(ui.values[0] / 60);
    // 		var arseconds1 = ui.values[0];
    // 		var arminutes1 = ui.values[0] - (arhours1 * 60);
    // 		if (arhours1.length == 1) arhours1 = '0' + arhours1;
    // 		if (arminutes1.length == 1) arminutes1 = '0' + arminutes1;
    // 		if (arminutes1 >= 0 && arminutes1 <= 9) arminutes1 = '0' + arminutes1;
    // 		if (arhours1 >= 0 && arhours1 <= 9) arhours1 = '0' + arhours1;
    // 		arhours1 = arhours1;
    // 		arminutes1 = arminutes1;

    // 		$('.slider-time').html(arhours1 + ':' + arminutes1);

    // 		var arhours2 = Math.floor(ui.values[1] / 60);
    // 		var arseconds2 = ui.values[1];
    // 		var arminutes2 = ui.values[1] - (arhours2 * 60);
    // 		if (arhours2.length == 1) arhours2 = '0' + arhours2;
    // 		if (arminutes2.length == 1) arminutes2 = '0' + arminutes2;
    // 		if (arminutes2 >= 0 && arminutes2 <= 9) arminutes2 = '0' + arminutes2;
    // 		if (arhours2 >= 0 && arhours2 <= 9) arhours2 = '0' + arhours2;
    // 		arhours2 = arhours2;
    // 		arhours2 = arhours2;

    // 		$('.slider-time2').html(arhours2 + ':' + arhours2);

    // 		var min = ui.values[0], max = ui.values[1];

    // 		$scope.filtermodel.arrivalTimeBean = { "isEnabledArrival": true, "fromArrival": ui.values[0] * 60000, "toArrival": ui.values[1] * 60000 };
    // 		if ($scope.filtermodel.arrivalTimeBean.length == 0) {
    // 			$scope.loadmore = false;
    // 			$scope.filtermodel.arrivalTimeBean = '';
    // 		} else {
    // 			$scope.filterParam($scope.filtermodel);
    // 		}
    // 	}
    // });

    // $("#price-range").slider({
    // 	range: true,
    // 	min: $scope.flightResultList.filterParams.priceBean.minPrice,
    // 	max: $scope.flightResultList.filterParams.priceBean.maxPrice,
    // 	step: 5,
    // 	values: [$scope.flightResultList.filterParams.priceBean.minPrice, $scope.flightResultList.filterParams.priceBean.maxPrice],
    // 	slide: function (e, ui) {
    // 		var min = ui.values[0], max = ui.values[1];
    // 		$('.slider-price1').html(min);
    // 		$('.slider-price2').html(max);
    // 		$scope.filtermodel.priceBean = { "isEnabledPrice": true, "fromRange": ui.values[0], "toRange": ui.values[1] };
    // 		if ($scope.filtermodel.priceBean.length == 0) {
    // 			$scope.loadmore = false;
    // 			$scope.filtermodel.priceBean = '';
    // 			$scope.noflight = true;
    // 			$rootScope.fResultsLen = 0;
    // 			$('.overlay').show().delay(1000).fadeOut();
    // 		} else {
    // 			$scope.filterParam($scope.filtermodel);
    // 		}
    // 	}
    // });

    /**
    @author Raghava Muramreddy
    open FareInfo Details
    */

    localStorage.setItem('baggagedata', JSON.stringify($scope.baggageDetails));
    $scope.isFareOpen = false;
    $scope.flightId = 0;
    $scope.openFareInfo = function(index) {
        $scope.flightId = index;
        $scope.isFareOpen = !$scope.isFareOpen;
    }
    $scope.openFareInfoClose = function() {
            if ($scope.isFareOpen)
                $scope.isFareOpen = !$scope.isFareOpen;
        }
        /**
        open FareInfo Details end
        */

    /**
    support : siri end
     */

    /* openFlight function */
    $scope.openFlight = function(size, parentSelector, index) {
            var parentElem = parentSelector ?
                angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
            $rootScope.selectedIndex = index;
            $scope.animationEnabled = true;
            var modalInstance = $uibModal.open({
                animation: $scope.animationEnabled,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'flightDetailedinfoContent.html',
                controller: 'flightDetailedinfoContent',
                backdrop: true,
                size: size,
                appendTo: parentElem,
                resolve: {
                    items: function() {
                        return $scope.items
                    }
                }
            })
        }
        /* end */
        ////baggage.......open....openBaggageInfo...

    $scope.openBaggageInfo = function(size, parentSelector, index) {
            var parentElem = parentSelector ?
                angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
            $rootScope.selectedFareIndex = index;
            $scope.animationEnabled = true;
            var modalInstance = $uibModal.open({
                animation: $scope.animationEnabled,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'flightBaggageInfo.html',
                controller: 'flightFareinfoControllers',
                backdrop: true,
                size: size,
                appendTo: parentElem,
                resolve: {
                    items: function() {
                        return $scope.items
                    }
                }
            })
        }
        /* open FareInfo */
    $scope.openFareInfo1 = function(size, parentSelector, index) {
        var parentElem = parentSelector ?
            angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
        $rootScope.selectedFareIndex = index;
        $scope.animationEnabled = true;
        var modalInstance = $uibModal.open({
            animation: $scope.animationEnabled,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'flightFareinfoContent.html',
            controller: 'flightFareinfoControllers',
            backdrop: true,
            size: size,
            appendTo: parentElem,
            resolve: {
                items: function() {
                    return $scope.items
                }
            }
        })
    }
    $scope.openFareTotal = function(size, parentSelector, index) {
            var parentElem = parentSelector ?
                angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
            $rootScope.selectedFareIndex = index;
            $scope.animationEnabled = true;
            var modalInstance = $uibModal.open({
                animation: $scope.animationEnabled,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'flightFareTotal.html',
                controller: 'flightFaretotalControllers',
                backdrop: true,
                size: size,
                appendTo: parentElem,
                resolve: {
                    items: function() {
                        return $scope.items
                    }
                }
            })
        }
        /* end */


});
ouaApp.controller('fareInfoDtlsContent', function($scope, $uibModalInstance, $rootScope, stopsService) {
    $scope.faresListArray = $rootScope.faresList;
});
ouaApp.controller('flightDetailedinfoContent', function($scope, $uibModalInstance, $rootScope, stopsService) {
    $scope.cabinClass = localStorage.getItem('cabinClass');
    var value = $rootScope.selectedIndex;
    if (!JSON.parse(sessionStorage.getItem('flightFilterData'))) {
        $scope.flightDetails = JSON.parse(sessionStorage.getItem('flightTotalData'))
        $scope.flightDetails1 = $scope.flightDetails.data.flightSearchResults[value];
    } else {
        $scope.flightDetails = JSON.parse(sessionStorage.getItem('flightFilterData'))
        $scope.flightDetails1 = $scope.flightDetails[value];
    }
    $scope.flightDetails2 = $scope.flightDetails
    $scope.filter = $rootScope.sCheckId
    $scope.airlineCode = $rootScope.airlineCodes;
    var flag = false;
    var filterDatas = {};
    if ($scope.filter != null && $scope.airlineCode != null) {

        filterDatas = stopsService.getStop($scope.flightDetails2, $scope.filter, $scope.airlineCode)
        $scope.flightDetails1 = filterDatas[value];
    } else if (!flag) {
        if ($scope.filter != null || $scope.filter != undefined) {
            filterDatas = stopsService.getStop($scope.flightDetails2, $scope.filter, $scope.airlineCode)
            $scope.flightDetails1 = filterDatas[value];
        } else if ($scope.airlineCode != null || $scope.airlineCode != undefined) {
            filterDatas = stopsService.getStop($scope.flightDetails2, $scope.filter, $scope.airlineCode)
            $scope.flightDetails1 = filterDatas[value];
        }
    }
    $scope.waitingDuration = [];
    var count = 0;
    $scope.getWaitingDuration = function(a, b, ind, x, key) {
        if (a != undefined && b !== undefined) {
            var startDate = new Date(a);
            var endDate = new Date(b);
            var d = (endDate.getTime() - startDate.getTime()) / 1000;
            d = Number(d);
            var h = Math.floor(d / 3600);
            var m = Math.floor(d % 3600 / 60);
            var hDisplay = h + "h";
            var mDisplay = m + "m";
            $scope.waitingDuration[parseInt(ind + "" + x + "" + key)] = hDisplay + " " + mDisplay;

        }

    }


    $scope.getTotalDuration = function(a, b) {
        $scope.totalDuration = timeConvert(parseInt(a) + parseInt(b));

    }

    function timeConvert(n) {
        var num = n;
        var hours = (num / 60);
        var rhours = Math.floor(hours);
        var minutes = (hours - rhours) * 60;
        var rminutes = Math.round(minutes);
        return rhours + "h " + rminutes + " m";
    }

    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };
});

ouaApp.controller('flightFareinfoControllers', function($scope, $http, ServerService, $uibModalInstance, $rootScope, ConstantService) {
    $scope.currency = ConstantService.currency;
    $scope.farerule = 'test';
    // $scope.adultBaggage = '20KG';
    // $scope.childBaggage = '10KG';
    // $scope.infantBaggage = '2KG';
    //console.log("flightFareinfoControllers");
    $scope.result = JSON.parse(sessionStorage.getItem('flightTotalData'));
    $scope.flightResultList = $scope.result.data;
    $scope.flightResultList1 = $scope.flightResultList.flightSearchResults
    if (sessionStorage.getItem('flightTotalData')) {
        var index = $rootScope.selectedFareIndex
        $scope.flightDetail = JSON.parse(sessionStorage.getItem('flightTotalData'));
        $scope.flightDetail1 = $scope.flightDetail.data.flightSearchResults[index]
            // $scope.refundtype =  $scope.flightDetail1.data.refund
        $scope.flightDetail2 = $scope.flightDetail.data.flightSearchResults[index].fareslst;
        // $scope.bagWeight = $scope.flightDetail.data.flightSearchResults[index].segGrpList[index].ssrDetails[index].baggageList[index].weight;
        // $scope.bagUnit = $scope.flightDetail.data.flightSearchResults[index].segGrpList[index].ssrDetails[index].baggageList[index].Unit;
        $scope.airlinenames = $scope.flightDetail.data.flightSearchResults[index].segGrpList[0].segList[0].airlineName;
        $scope.aircraft = $scope.flightDetail.data.flightSearchResults[index].segGrpList[0].segList[0].fltInfoList[0].aircraftType;
        if ($scope.aircraft == null) {
            // 	$scope.aircraft="$scope.flightDetail.data.flightSearchResults[index].segGrpList[0].segList[0].fltInfoList[0].aircraftType";
            // }else{
            $scope.aircraft = "-";
        }
        $scope.flightDetails = JSON.parse(sessionStorage.getItem('flightFilterData'))
        if ($scope.flightDetails == null) {

            $scope.seggrplist = $scope.flightDetail.data.flightSearchResults[index].segGrpList;
            $scope.baggageDetails = [];
            $scope.baggageDetails.segList = [];
            $scope.baggageDetails.ssrDetails = []
            for (var i = 0; i < $scope.seggrplist.length; i++) {
                for (var j = 0; j < $scope.seggrplist[i].segList.length; j++) {
                    $scope.baggageDetails.segList.push($scope.seggrplist[i].segList[j]);
                }
                if ($scope.seggrplist[i].ssrDetails != null) {
                    for (var j = 0; j < $scope.seggrplist[i].ssrDetails.length; j++) {
                        $scope.baggageDetails.ssrDetails.push($scope.seggrplist[i].ssrDetails[j]);
                    }
                } else {
                    $scope.baggagessrdetails = "nossr";
                }
            }
        } else {
            $scope.seggrplist = $scope.flightDetails[index].segGrpList
            $scope.baggageDetails = [];
            $scope.baggageDetails.segList = [];
            $scope.baggageDetails.ssrDetails = []
            for (var i = 0; i < $scope.seggrplist.length; i++) {
                for (var j = 0; j < $scope.seggrplist[i].segList.length; j++) {
                    $scope.baggageDetails.segList.push($scope.seggrplist[i].segList[j]);
                }
                for (var j = 0; j < $scope.seggrplist[i].ssrDetails.length; j++) {
                    $scope.baggageDetails.ssrDetails.push($scope.seggrplist[i].ssrDetails[j]);
                }
            }

        }
        localStorage.setItem('baggagedata', JSON.stringify($scope.baggageDetails));
        /* 	
        	$scope.baggagedetails=[];
        	
        	$scope.ssrdetails=$scope.seggrplist[0].ssrDetails;
        	for(var i=0; i<$scope.ssrdetails.length; i++){
        		for(var j=0;j<$scope.ssrdetails[i].baggageList.length;j++){
        			
        $scope.baggagedetails.push($scope.ssrdetails[i].baggageList[j]);
        		$scope.baggagedetails[j].paxType=$scope.ssrdetails[i].paxType;
        		}
        	} */
        // }

        // console.log($scope.baggageDetails);

        /* 	 $scope.baggagetottall=$scope.baggageDetails.ssrDetails[0].baggageList.length;
        	
        	$scope.returnbaggagedetails=[];
        	$scope.ssrdetailsreturn=$scope.seggrplist[1].ssrDetails;
        	for(var j=0; j<$scope.ssrdetailsreturn.length; j++){
        		for(var i=0;i<$scope.ssrdetailsreturn[j].baggageList.length;i++){
        $scope.returnbaggagedetails.push($scope.ssrdetailsreturn[j].baggageList[i]);
        		$scope.returnbaggagedetails[i].paxType=$scope.ssrdetailsreturn[j].paxType;
        		}
        	} */


        $scope.refundtypes = $scope.flightDetail.data.flightSearchResults[index].fareslst[0].refundable;
        if ($scope.refundtypes == true) {
            $scope.refundtype = "Refundable";
        } else {
            $scope.refundtype = "Non-Refundable";
        }
        $scope.provider = $scope.flightDetail.data.flightSearchResults[index].provider;
        if ($scope.flightDetail.data.flightSearchResults[index].fareslst[0].fareInfoRefKey) {
            $scope.farekey = $scope.flightDetail.data.flightSearchResults[index].fareslst[0].fareInfoRefKey[0];
        } else {
            $scope.farekey = $scope.flightDetail.data.flightSearchResults[index].fareslst[0].fareInfoRefKey;
        }



    }

    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };
    var fareRequest = new Object();
    fareRequest.provider = $scope.provider;
    fareRequest.key = $scope.farekey;
    var fareRequestJSON = JSON.stringify(fareRequest);
    $scope.loading = true;
    $scope.disabled = true;

    $.ajax({
        method: 'POST',
        data: fareRequestJSON,
        url: ServerService.serverPath + 'rest/flight/rules/retrieve'
    }).success(function(data, status) {
        var data = JSON.parse(data);
        if (data.status == "SUCCESS") {
            $('.loading').hide();
            $scope.loading = false;
            $scope.disabled = false;

            $scope.farerule = 'test';
            if (data.data) {


                $scope.farerule = data.data[16].content;
                $('.fareRules').append(data.data[16].content)
                $('.fareHeader').append(data.data[16].headerText)
            } else {
                $scope.farerule = "Fare rules not available";
                $('.fareRules').append('Fare rules not available');
                $('.fareRules').css('text-align', 'center');
                $('.fareRules').css('color', 'red');

            }
            //console.log($scope.farerule);
            // var fr = '';
            // for(var r in data.data) {
            // 	var farerule = data.data[r];
            // 	if(data.data[r].headerText=='RULE-16') {
            // 		fr='<div class="rule"><h4 style="color:#1e2a3a;">'+farerule.headerText+'</h4><p class="rulepara">'+farerule.content+'</p></div>';
            // 		break;
            // 	}
            // $scope.faredetail= JSON.parse(data);
            // $scope.fairrule = $scope.faredetail.data;

            // $scope.rule = JSON.parse('faredetail');
            // $scope.flightfare = $scope.rule.data;


            // }
        } else {
            $scope.farerule = "Fare rules not available";
            $('.fareRules').append('Fare rules not available');
            $('.fareRules').css('text-align', 'center');
            $('.fareRules').css('color', 'red');

            // $('.fareRules').style(text-align=center font-weight: 700)
            // $('.fareHeader').append(data.data[16].headerText)
        }
        // var data = JSON.parse(data);
        // if(data.status=="SUCCESS") {

        // var data = JSON.parse(data);
        // $scope.headerText = $scope.farerule.headerText
        // $scope.faredetail = $scope.fareRule[16].

        // if(data.data.status == 'SUCCESS'){
        // 	sessionStorage.setItem('flightFare',JSON.stringify(data.data));
        // 	$location.path('/flightSearchResult');

        // }
        // $scope.headerText = $scope.farerule.headerText
        // var data = JSON.parse(data);
        // if(data.status=="SUCCESS") {
        // 	$(current).removeClass('fare_rules_hit');
        // 	$(current).addClass('fare_rules_hitted');
        // 	var fr = '';
        // 	for(var r in data.data) {
        // 		var farerule = data.data[r];
        // 		if(data.data[r].headerText=='RULE-16') {
        // 			fr='<div class="rule"><h4 style="color:#1e2a3a;">'+farerule.headerText+'</h4><p class="rulepara">'+farerule.content+'</p></div>';
        // 			break;
        // 		} 
        // 	}
        // }
        // $http.post(ServerService.serverPath+'rest/flight/rules/retrieve', {

        // 		
    })
});
// controlller for totalfare...........popup........
ouaApp.controller('flightFaretotalControllers', function($scope, $http, ServerService, $uibModalInstance, $rootScope, ConstantService) {
    $scope.currency = ConstantService.currency;
    $scope.farerule = 'test';
    //console.log("flightFaretotalControllers");
    $scope.result = JSON.parse(sessionStorage.getItem('flightTotalData'));
    $scope.flightResultList = $scope.result.data;
    $scope.flightResultList1 = $scope.flightResultList.flightSearchResults
        // $scope.airlinenames=$scope.flightResultList1[0].segGrpList[0].segList[0].airlineName;
        // $scope.aircraft=$scope.flightResultList1[0].segGrpList[0].segList[0].fltInfoList[0].aircraftType;
    if (sessionStorage.getItem('flightTotalData')) {
        var index = $rootScope.selectedFareIndex
        $scope.flightDetail = JSON.parse(sessionStorage.getItem('flightTotalData'));
        $scope.flightDetail1 = $scope.flightDetail.data.flightSearchResults[index]
            // $scope.refundtype =  $scope.flightDetail1.data.refund
        $scope.flightDetail2 = $scope.flightDetail.data.flightSearchResults[index].fareslst;
        // $scope.provider = $scope.flightDetail.data.flightSearchResults[index].provider;
        // $scope.farekey = $scope.flightDetail.data.flightSearchResults[index].fareslst[0].fareInfoRefKey[0];
    }

    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };
    // var fareRequest = new Object();		
    // fareRequest.provider = $scope.provider ;
    // fareRequest.key = $scope.farekey;
    // var fareRequestJSON = JSON.stringify(fareRequest);

});

function formatNumber(n) {
    return Math.round(n);
}

ouaApp.service('stopsService', function() {
    this.getStop = function(listing, filter, airlineFilter) {
        var stops = [];
        var flage = false;
        var filterlength = listing.flightSearchResults.length
        for (var i = 0; i < filterlength; i++) {
            var tempData = listing.flightSearchResults[i]
            var oubound = listing.flightSearchResults[i].outboundFilter.stops;
            var outBoundAirline = listing.flightSearchResults[i].outboundFilter.airlineCode;
            if (filter != null && airlineFilter != null) {
                if (outBoundAirline == airlineFilter && filter == oubound) {
                    stops.push(tempData);
                    flage = true;

                }
            } else {
                if (!flage) {
                    if (filter == oubound) {
                        stops.push(tempData);
                    } else if (outBoundAirline == airlineFilter) {
                        stops.push(tempData);
                    }
                }
            }
        }
        //	console.log(stops);
        return stops
    }



})