ouaApp.controller('flightReviewController', function($scope,corpCompanyDetails, $compile,refreshService, $interval, $http, NationService, $window, $rootScope, Month, Year, $uibModal, baseUrlService, ServerService, $location, $timeout, httpService, $state, customSession, $uibModal, ConstantService, renderHTMLFactory) {
    $scope.countryList = {};
    NationService.nationGet();
    $("#modal").removeClass("show");
    $scope.mealsselectedlist = [];
    $("html,body").scrollTop(10);
    $scope.backtodetail = false;
    $scope.backtotraveler = false;
    $scope.loggedvalue=true;
    corpCompanyDetails.getCompanyIdFlight();
    //$scope.emailFormat = /^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/;
    // $scope.loggedvalue = false;
    $(function () {
		$('[data-toggle="tooltip"]').tooltip()
      })

      $scope.initSrchObj = function()
      {
        $scope.trvellersrch = {
            paxName:"",
            birthdate:null,
            birthmonth:null,
            birthyear:null,
            email:"",
            mobcntrycodetrobj:"",
            mobile:"",
            passportNo:"",
            surName:""
          };    
          $scope.phNoREQ = false;
          $scope.ccREQ = false;
      }

      $scope.isBookDisable = true;
      $rootScope.$on('FopBookValueUpdate', function(event,data) {
          $scope.isBookDisable = data.value;
      });

      $scope.isShowUploadedcheck = false;

      $scope.uploadFile = function(){
          var filename = event.target.files[0].name;
          $scope.fopName = filename;
          $scope.isShowUploadedcheck = true;
          $scope.$apply();
      };

      if (sessionStorage.getItem('bookingQuotationResponse') != undefined) {
            $scope.isQuote = true;
        }else{
            $scope.isQuote = false;
        }
        $scope.flightquotationData = JSON.parse(sessionStorage.getItem('bookingQuotationResponse'));
        if($scope.flightquotationData!=null)
        if($scope.flightquotationData.quotationStatus=='25446'){
            sessionStorage.setItem("QuoteApproved",true);
        }
    $scope.isQuoteApproved = sessionStorage.getItem("QuoteApproved") != undefined ? true : false;

      $scope.initSrchObj();

      $scope.tokenresult = JSON.parse(sessionStorage.getItem('authToken'));

      var headersOptions = {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + $scope.tokenresult
      }
    $scope.flightBookSession = JSON.parse(sessionStorage.getItem('flightPriceData'));
    sessionStorage.setItem("FopSessionId",$scope.flightBookSession.sessionId);
    $scope.flightBook = $scope.flightBookSession.data;
    sessionStorage.setItem("fopBeansList",JSON.stringify($scope.flightBook.selectedResult.flightSearchResults[0].fopBeans));
    $scope.flightquotationData = JSON.parse(sessionStorage.getItem('bookingQuotationResponse'));
    $scope.generateDocket = function()
    {
        var token = JSON.parse(sessionStorage.authToken);
        var headersOptions = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token
        }

        var sessionId=JSON.parse(sessionStorage.getItem('flightPriceData'));

        $scope.artloading = true;

        $http.get(ServerService.serverPath + 'docket/common/docketNo/F', 
        {
            headers: headersOptions
        }).then(function successCallback(response) 
        {
            $scope.artloading = false;
            if(response.data.success)
            {
                $scope.docket = response.data.data;
            }
        })
    }

    $scope.docket = "";

    $scope.generateDocket();
    // $scope.greatGrandTotal = 0;
    $scope.getGrantotal = function(){

        var grandtotal = 0;
        if(grandtotal > 0){
            grandtotal = $scope.fngrandtotal+$scope.selectedInsurancePlan.totalFare;
        }if($scope.fopNewPrice > 0){
            grandtotal = $scope.fopNewPrice;
        }else{
            grandtotal = $scope.flightBooking.flightSearchResults[0].grndTotal+$scope.totalSSR+$scope.loopTot+$scope.totMeals+$scope.totseat;
        }
        sessionStorage.setItem('GreatGrandTotal',grandtotal);
        return grandtotal;
    }

    $scope.companyDetailsTmp = JSON.parse(sessionStorage.getItem('companyDetails'));  
$scope.flightSearch={};
if($scope.flightquotationData!=null){
    $scope.flightFop=$scope.flightquotationData.fop;
    $scope.flightSearch.fop= $scope.flightFop.toString();
}else{
    $scope.flightSearch.fop=sessionStorage.fopValue;
  
}

if(sessionStorage.companyId !=undefined){
    $scope.companyId=sessionStorage.companyId 
}else if($scope.flightquotationData!=null){
    
    $scope.companyId=$scope.flightquotationData.agencyId;
}
else{
        $scope.companyId=$scope.companyDetailsTmp.companyId 
    }


    $scope.fopResponseCreditCard=function(){
    
        $scope.modalInstance=$uibModal.open({
            animation: $scope.animationEnabled,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
              templateUrl: 'fop.tmpl.html',
              backdrop: 'true',
              
              size: 'fop',
              keyboard: true,
              scope:$scope
          });
    }
    
    $scope.closefop=function(){
        $scope.flightSearch.fop= $scope.fopRequest.fopBeans[0].foppId;
        $scope.flightBooking = $scope.flightBook.selectedResult;     
        $scope.modalInstance.dismiss();//$scope.modalInstance.close() also works I think
        return;
    };
$scope.fopPriceChange=function(){
    $scope.flightBooking = $scope.fopNewPrice.selectedResult;
    $scope.modalInstance.dismiss();
}

$scope.fopService=function(){
    $http.post(ServerService.dropDownPath+'customer/'+''+  $scope.companyId+'/flight/reprice/fop',$scope.fopRequest, {
        headers: headersOptions
       }).then(function successCallback(response) {
           if(response.data.success){

           $scope.artloading = false;

            //    $scope.fopResponseCreditCard();
            $scope.isFareChanged = response.data.data.priceChange
            if($scope.isFareChanged)
            {
                Lobibox.alert('error',{
                    msg:  response.data.data.msg,
                    callback: function (lobibox, type) {
                        if (type === 'ok') 
                        {
                            $scope.fopNewPrice = response.data.data
         
                            $scope.flightBooking = $scope.fopNewPrice.selectedResult;
                            // sessionStorage.setItem('FopChangedTotal', JSON.stringify($scope.fopNewPrice));
                            // $scope.isCreditCard = $scope.hotelSearch.fopId == 25441 ?  true : false;
                            // sessionStorage.setItem('hotelSearchdataobj', JSON.stringify($scope.hotelSearch));
                            $scope.$apply();
                        }
                    }
                });

            }else{
                // $scope.isCreditCard = $scope.hotelSearch.fopId == 25441 ?  true : false;
                // sessionStorage.setItem('hotelSearchdataobj', JSON.stringify($scope.hotelSearch));
            }
         
          

       }else{
      
           $scope.artloading = false;
           Lobibox.notify('error', {
               size: 'mini',
               position: 'top right',
               msg:response.data.errorMessage,
               delay: 1500
           });
       }
   });
}

$scope.fopChange=function(fopValue){
    $scope.sessionid = $scope.flightBookSession.sessionId;

    $scope.fopRequest={

        "sessionId": $scope.sessionid,
    
        "fopBeans":[{
    
        "fopId":fopValue,
    
      
    
        }]
    
    }
 
        // $scope.fopResponseCreditCard();
   

    $scope.fopService();
  

}

    $scope.fop=function(){
        // $scope.selectedCompanyname=true;
        // sessionStorage.setItem("companyId",selectedCompanyId);
        // $scope.artloading = true;
        $scope.tokenresult = JSON.parse(sessionStorage.getItem('authToken'));

var headersOptions = {
    'Content-Type': 'application/json',
    'Authorization': 'Bearer ' + $scope.tokenresult
}
        $http.get(ServerService.dropDownPath +  'list/fop/corporate-customer/'+$scope.companyId, {
        
            headers: headersOptions
        }).then(function successCallback(response) {
        $scope.artloading = false;
        if(response.data.success)
            {
              
                $scope.fopResponse=response.data.data;
		        $rootScope.companyFOPList = response.data.data;
                
    
            }
            else
            {
          
                
            }
        });
    }
$scope.fop();
    $scope.complete=function(){
        $( "#docket" ).autocomplete({
            minLength: 2,
            source: function(request, response) {

                var token = JSON.parse(sessionStorage.authToken);
        var headersOptions = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token
        }

        // $scope.artloading = true;

        $http.get(ServerService.listPath + 'list/docketNo?searchString='+$scope.docket, 
        {
            headers: headersOptions
        }).then(function successCallback(data) 
        {
            // $scope.artloading = false;
            var success = data.data.success
            var data = data.data.data;
            if (success) {
                f = 0;
                response(data);
            } else {
                $("#docket").val('');
                $("#docket").focus(0);
                $('.ui-autocomplete').hide();
            }
            // if(response.data.success)
            // {
            //     $scope.availableTags = response.data.data;
            // }
        })
              
            },

            change: function(event, ui) {
                var uiCity = JSON.stringify(ui.item); 
                if(uiCity == 'null'){
                    $("#docket").val('');
                }
            },
            autoFocus: true,
            minLength: 3,
            select: function(event, ui) {
                    $("#docket").val(ui.item.code);
                    $scope.docket = ui.item.code;
                    $scope.$apply();
                    // $("#docket").parent().removeClass('has-error');
                    flag = true;
            return false;
            },
            open: function() {
                $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
            },
            close: function(event, ui) {
                $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            if (f == 0) {
                f++;
                return $("<li>").append("<a class='ui-corner-all'>" + item.code+ "</a>").appendTo(ul);
            } else {
                return $("<li>").append("<a>" + item.code+ "</a>").appendTo(ul);
            }
        };
    // $scope.getDocketList();
    } 
    
    $scope.promocode = "";
    $scope.discount = 0;
    $scope.getpromocode = function(promocode)
    {
        var token = JSON.parse(sessionStorage.authToken);
        var headersOptions = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token
        }

        var sessionId=JSON.parse(sessionStorage.getItem('flightPriceData'));

        $scope.artloading = true;

        $http.post(ServerService.dropDownPath+ 'customer/'+ $scope.corprateCompanyId + '/promoCode/apply-promo/FLT/'+promocode, sessionId.sessionId, 
        {
            headers: headersOptions
        }).then(function successCallback(response) 
        {
            $scope.artloading = false;
            if(response.data.success)
            {
                $scope.discount = response.data.data;
            }
            else{
                Lobibox.alert('error',
                {
                    msg: response.data.errorMessage
                });
            }
        });


    }
 
    // $scope.loggedIn = false;

    // if (sessionStorage.loginToken != null) {
    //     $scope.loggedvalue = true;
    // }
    $scope.loopTot = 0;
    $scope.trvellerSeat = 0;
    $scope.artloading = true;
    $scope.paysubmitted = false;
    $scope.selectedInsurancePlantotalFare = 0;
    $('html, body').scrollTop(0);

    $scope.monthcnvrt =  function(val){
        return new Date(2014, val-1);
                // called asynchronously if an error occurs
                // or server returns response with an error status.
    }

    function range(start, end) {
        return Array(end - start + 1).fill().map((_, idx) => start + idx)
      }

    //calculate age from today date
    var curd = new Date();
    var curyear = curd.getFullYear();
    var curmonth = curd.getMonth();
    var curday = curd.getDate();
    var maxinfantdate = new Date(curyear - 2, curmonth, curday);
    var maxchilddate = new Date(curyear - 12, curmonth, curday);
    var maxadultdate = new Date(curyear - 55, curmonth, curday);

    var maxINFD = maxinfantdate.getDate();
    var maxINFY = maxinfantdate.getFullYear();
    var maxINFM = maxinfantdate.getMonth();

    var maxCNND = maxchilddate.getDate();
    var maxCNNY = maxchilddate.getFullYear();
    var maxCNNM = maxchilddate.getMonth();

    var maxADTD = maxadultdate.getDate();
    var maxADTY = maxadultdate.getFullYear();
    var maxADTM = maxadultdate.getMonth();


    $scope.months = 12;
     $scope.getminexpirymonth = 12;

     $scope.dobdates = range(1, 31);
     $scope.cdobdates = range(1, 31);
     $scope.idobdates = range(1, 31);

     $scope.iyears = range(curyear-2, curyear);
     $scope.cyears = range(curyear-12, maxINFY);
     $scope.ayears = range(curyear-55, maxCNNY);
     
     $scope.cdobmonths = range(1, 12);
     $scope.idobmonths = range(1, 12);
     $scope.dobmonths = range(1, 12);

    $scope.changeDOB = function(selectedYear,trvlrType,selectedMonth,ismonth)
    {
        var days = 30
        if(ismonth)
        {
            if (selectedMonth == 2) 
            {
                days = 28;
            } 
            else if (selectedMonth == 4 || selectedMonth == 6 || selectedMonth == 9 || selectedMonth == 11) 
            {
                days = 30;
            } 
            else 
            {
                days = 31;
            }
        }   
        switch(trvlrType)
        {
            case 'infant':
                // var selectedYear = trvlrData.infant.personaldata.birthyear
                if(selectedYear == curyear)
                {
                    $scope.idobmonths = range(1, maxINFM+1);
                    $scope.idobdates = range(1, maxINFD);
                }
                else if(selectedYear == maxINFY )
                {
                    $scope.idobmonths = range(maxINFM+1, 12);
                    $scope.idobdates = range(maxINFD+1, days);
                }
                else
                {
                    $scope.idobmonths = range(1, 12);
                    $scope.idobdates = range(1, days);
                }

                $scope.trvellersrchday = $scope.idobdates;
                $scope.trvellersrchmonth = $scope.idobmonths;
            break;

            case 'child':
                // var selectedYear = trvlrData.child.personaldata.birthyear
                if(selectedYear == maxINFY)
                {
                    $scope.cdobmonths = range(1, maxCNNM+1);
                    $scope.cdobdates = range(1, maxCNND);
                }
                else if(selectedYear == maxCNNY)
                {
                    $scope.cdobmonths = range(maxCNNM+1, 12);
                    $scope.cdobdates = range(maxCNND+1, days);
                }
                else
                {
                    $scope.cdobmonths = range(1, 12);
                    $scope.cdobdates = range(1, days);
                }

                $scope.trvellersrchday = $scope.cdobdates;
                $scope.trvellersrchmonth = $scope.cdobmonths;                
            break;

            case 'adult':
                // var selectedYear = trvlrData.infant.personaldata.birthyear
                if(selectedYear == maxCNNY)
                {
                    $scope.dobmonths = range(1, maxADTM+1);
                    $scope.dobdates = range(1, maxADTD);
                }
                else if(selectedYear == maxADTY)
                {
                    $scope.dobmonths = range(maxADTM+1, 12);
                    $scope.dobdates = range(maxADTD-1, days);
                }
                else
                {
                    $scope.dobdates = range(1, days);
                    $scope.dobmonths = range(1, 12);
                }

                $scope.trvellersrchday = $scope.dobdates;
                $scope.trvellersrchmonth = $scope.dobmonths;

            break;

        }
        
    }

        // forinfantYear......for..passportexpiry..
        $scope.ipemonths = 12;
        var ipemonth = [];
        for (var i = 0; i < $scope.imonths; i += 1) {
         ipemonth.push(i + 1);
        }
        $scope.ipemonths = ipemonth;
        var iexpiryyear = new Date().getFullYear();
        var aexpiryyear = new Date().getFullYear();
        var cexpiryyear = new Date().getFullYear();
        var iperange = [];
        iperange.push(iexpiryyear);
        for (var i = 1; i < 50; i++) {
         iperange.push(iexpiryyear - i);
        }
        $scope.iexpiryyears = iperange;
        $scope.changeinfantpeYear = function(selectedYear) {
            var iexpiryyear = new Date().getFullYear();
            var infantToday = new Date();
            var getInfanntmonth = infantToday.getMonth();
    
            var selectedYear = selectedYear.infant.personaldata.expiryyear || 0;
            if (selectedYear == iexpiryyear) {
                $scope.currentmonth = getInfanntmonth;
            } else {
                $scope.currentmonth = 0;
                $scope.imonths = 12;
            }
    
            var ipemonth = [];
            for (var i = $scope.currentmonth; i < $scope.imonths; i += 1) {
             ipemonth.push(i + 1);
            }
            $scope.ipemonths = ipemonth;
    
            var iperange = [];
            iperange.push(iexpiryyear);
            for (var i = 1; i < 50; i++) {
             iperange.push(iexpiryyear - i);
            }
            $scope.iexpiryyears = iperange;
        }
 
 
     
 //passport expiry

//  var apemonth = [];
//  var cpemonth = [];
//  for (var i = 0; i < $scope.getminexpirymonth; i += 1) {
//      apemonth.push(i + 1);
//      cpemonth.push(i + 1);
//  }
//  $scope.apemonths = apemonth;
//  $scope.cpemonths = cpemonth;

 $scope.pdates=31;
 var apdate= [];
 var cpdate = [];
 for (var i = 0; i < $scope.pdates; i += 1) {
    apdate.push(i + 1);
     cpdate.push(i + 1);
 }
 $scope.pedates = apdate;
 $scope.cpedates = cpdate;

           // adultYearchange..for..passportexpiry.
           var apemonth = [];
           $scope.amonths = 12;
           for (var i = 0; i < $scope.amonths; i += 1) {
               apemonth.push(i + 1);
           }
           $scope.apemonths = apemonth;
         
           var pefullyear = new Date().getFullYear();
           var arange = [];
           arange.push(pefullyear);
           for (var i = 1; i < 50; i++) {
               arange.push(pefullyear + i);
           }
           $scope.apeyears = arange
         $scope.changeadultpeYear = function(selectedYear) {
         

               var aexpiryyear = new Date().getFullYear();
               var adultToday = new Date();
               var getAdultmonth = adultToday.getMonth();
       
               var selectedYear = selectedYear.adult.personaldata.expiryyear || 0;
               if (selectedYear == pefullyear) {
                   $scope.currentmonth = getAdultmonth;
               } else {
                   $scope.currentmonth = 0;
                   $scope.amonths = 12;
               }
       
               var apemonth = [];
               for (var i = $scope.currentmonth; i < $scope.amonths; i += 1) {
                apemonth.push(i + 1);
               }
               $scope.apemonths = apemonth;
       
               var aperange = [];
               aperange.push(aexpiryyear);
               for (var i = 1; i < 50; i++) {
                aperange.push(aexpiryyear - i);
               }
               $scope.aexpiryyears = aperange;
         }


          // adultmonthchange..for..passportexpiry.
     $scope.apedays = 31;
     $scope.changeadultpeMonth = function(selectedMonth) {
             var selectedPeMonth = selectedMonth.adult.personaldata.expirymonth || 0;
             if (selectedPeMonth == 2) {
                 $scope.apedays = 28;
             } else if (selectedPeMonth == 4 || selectedPeMonth == 6 || selectedPeMonth == 9 || selectedPeMonth == 11) {
                 $scope.apedays = 30;
             } else {
                 $scope.apedays = 31;
             }
            var currentday = new Date();
            var today = new Date().getDate();
            var aexpirymonth = currentday.getMonth()+1;
            var selectedYear = selectedMonth.adult.personaldata.expiryyear;
            if (selectedPeMonth == aexpirymonth && selectedYear ==pefullyear ) {
                $scope.currentDate = today;
            } else {
                $scope.currentDate = 0;
            }
            var pedate = [];
            for (var i = $scope.currentDate; i < $scope.apedays; i += 1) {
                pedate.push(i + 1);
            }
            $scope.pedates = pedate;
         }

   
             // childYearchange..for..passportexpiry.
             var cpemonth = [];
             $scope.cmonths = 12;
             for (var i = 0; i < $scope.cmonths; i += 1) {
                 cpemonth.push(i + 1);
             }
             $scope.cpemonths = cpemonth;
           
             var cyear = new Date().getFullYear();
             var crange = [];
             crange.push(cyear);
             for (var i = 1; i < 50; i++) {
                 crange.push(cexpiryyear + i);
             }
             $scope.cexpiryyears = crange
           $scope.changechildpeYear = function(selectedYear) {
           
  
                 var cexpiryyear = new Date().getFullYear();
                 var childToday = new Date();
                 var getChildmonth = childToday.getMonth();
         
                 var selectedYear = selectedYear.child.personaldata.expiryyear || 0;
                 if (selectedYear == cyear) {
                     $scope.currentmonth = getChildmonth;
                 } else {
                     $scope.currentmonth = 0;
                     $scope.cmonths = 12;
                 }
         
                 var cpemonth = [];
                 for (var i = $scope.currentmonth; i < $scope.cmonths; i += 1) {
                  cpemonth.push(i + 1);
                 }
                 $scope.cpemonths = cpemonth;
         
                //  var cperange = [];
                //  cperange.push(cexpiryyear);
                //  for (var i = 1; i < 50; i++) {
                //   cperange.push(cexpiryyear - i);
                //  }
                //  $scope.cexpiryyears = cperange;
           }
          // childmonthchange..for..passport..expiry..
     $scope.cpedays = 31;
     $scope.changechildpeMonth = function(selectedMonth) {
             var selectedpeMonth = selectedMonth.child.personaldata.expirymonth || 0;
             if (selectedpeMonth == 2) {
                 $scope.cpedays = 28;
             } else if (selectedpeMonth == 4 || selectedpeMonth == 6 || selectedpeMonth == 9 || selectedpeMonth == 11) {
                 $scope.cpedays = 30;
             } else {
                 $scope.cpedays = 31;
             }
            var currentday = new Date();
            var today = new Date().getDate();
            var cexpirymonth = currentday.getMonth()+1;
            // if (selectedpeMonth == cexpirymonth) {
                var selectedYear = selectedMonth.child.personaldata.expiryyear;
            if (selectedpeMonth == cexpirymonth && selectedYear ==pefullyear ) {
                $scope.currentDate = today;
            } else {
                $scope.currentDate = 0;
            }
            var cpedate = [];
            for (var i = $scope.currentDate; i < $scope.cpedays; i += 1) {
                cpedate.push(i + 1);
            }
            $scope.cpedates = cpedate;
         }

        // infantmonthchange.for.passport...expirydate.
        $scope.ipedays = 31;
        var ipedate = [];
        for (var i = 0; i < $scope.ipedays; i += 1) {
            ipedate.push(i + 1);
        }
        $scope.ipedates = ipedate;

        $scope.changeinfantpeMonth = function(selectMonth) {
            //console.log("selectedMonth",selectedMonth)
            var selectedMonth = selectMonth.infant.personaldata.expirymonth || 0;
            if (selectedMonth == 2) {
                $scope.ipedays = 28;
            } else if (selectedMonth == 4 || selectedMonth == 6 || selectedMonth == 9 || selectedMonth == 11) {
                $scope.ipedays = 30;
            } else {
                $scope.ipedays = 31;
            }

            var currentday = new Date();
            var today = new Date().getDate();
            var iexpirymonth = currentday.getMonth()+1;
            var selectedYear = selectMonth.infant.personaldata.expiryyear;
            if (selectedMonth == iexpirymonth && selectedYear== pefullyear ) {
                // $scope.currentDate = iexpirymonth;
                $scope.currentDate = today;
            } else {
                $scope.currentDate = 0;
            }
            var ipedate = [];
            for (var i = $scope.currentDate; i < $scope.ipedays; i += 1) {
                ipedate.push(i + 1);
            }
            $scope.ipedates = ipedate;
        }
 
    // ends.....dob and pe......
    $scope.handler = function(event) {
        //  //console.log("min date", $scope.min);
        var date = new Date($scope.min);

        // var month = date.getMonth() + 7; //months from 1-12
        // var day = date.getDate();
        // var year = date.getFullYear();
        date.setMonth(date.getMonth() + 6);
        // var newDate = year + '-' + month + '-' + day;
        //   console.log("date", date);
        var selectedDate = event.target.value;
        //   console.log('selected date', event.target.value);
        var date1 = new Date(selectedDate);
        if (date1 <= date) {
            //	Lobibox.notify('info', { size: 'mini', delay: 1500, msg: "Loreum ipsum asdfa asdfklj asdlkfj; ljadsf" });
            Lobibox.alert('info', //AVAILABLE TYPES: "error", "info", "success", "warning"
                {
                    msg: "Please note, if you travel to any country  other than your passport issued country, then as a general rule passports should have at least six months of validity.Please contact us for more information."
                });
        }
    }
       /*backloader   */
       $scope.timeload = function(){
        $("#load").show();
        setTimeout(function () {
           $('#load').hide();
        }, 1000);
    }
        $scope.backloader=function() {
            $scope.timeload();
            if(localStorage.islogged == 'true'){
                      $scope.$parent.bookingHide = true;
                $scope.$parent.bookingHide1 = true;
            }else{
                    $scope.$parent.bookingHide = false;
                $scope.$parent.bookingHide1 = false;
            }
                        document.getElementById("load").style.display="block";
            setTimeout("hide()", 500); 
            $location.path('/flightDetails');
    }
        $scope.hide=function() {
            document.getElementById("load").style.display="none";
           
            // $scope.setTimeout("",5000);
        }

    $scope.searchReq = JSON.parse(sessionStorage.getItem('flightSearchRQ'));
    var multi = $scope.searchReq.multicityInfos;

    // sessionStorage.setItem('multiCity', JSON.stringify($scope.searchFlightObj.multicityInfos));
    if (multi === undefined) {
        $scope.hideCity = true;
    } else {
        $scope.hideCity = false;
    }
    //   $scope.tokenresult = JSON.parse(sessionStorage.getItem('authentication'));

    // var tokenname = $scope.tokenresult.data;
    // var headersOptions = {
    // 'Content-Type': 'application/json',
    // 'Authorization': 'Bearer ' + tokenname.authToken
    // }

    $scope.flightTermsDetails = function(info,size, parentSelector, index) {
       
        var parentElem = parentSelector ?
        
            angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
        $rootScope.selectedFareIndex = index;
        $scope.animationEnabled = true;
        var modalInstance = $uibModal.open({
            animation: $scope.animationEnabled,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'flightTermsandConditions.html',
            controller: 'flightTermsController',
            backdrop: true,
            size: size,
            appendTo: parentElem,
            resolve: {
                items: function() {
                    return $scope.items
                }
            }
        })
    }

    $scope.totMeals = 0;
    $scope.totseat = 0;

    getTotMeals = function(){
        $scope.totMeals = 0;
            for(let mealseg in $scope.ssrbindData.meals)
            {
                if($scope.ssrbindData.meals[mealseg].mealsData != undefined)
                {
                    for(var ml=0;ml<$scope.ssrbindData.meals[mealseg].mealsData.length;ml++)
                    {
                        $scope.ismealavailable = true;
                        $scope.totMeals = Number($scope.ssrbindData.meals[mealseg].mealsData[ml].travelerDetails.price) + $scope.totMeals;
                    }
                }
            }
    }

    $scope.selectmealDetails = function(size, parentSelector, mealsDetails, parentIndex, segkey,mealsdtls,meal) {
        //console.log($scope.mealsData);

        var parentElem = parentSelector ?
            angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
            sessionStorage.setItem("bindmealsdetails1", JSON.stringify(mealsDetails))

        // $rootScope.bindmealsdetails1 = mealsDetails;
        $rootScope.parentIndex = parentIndex;
     // $rootScope.key = $scope.mealsData[parentIndex].key;
     var segkey = segkey;
     $rootScope.multiMealsEnabled = meal.mealmultiple;
     $rootScope.bundledFareId = mealsdtls.prf;
        $scope.animationEnabled = true;
        var modalInstance = $uibModal.open({
            animation: $scope.animationEnabled,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'mealDetailedinfoContent.html',
            controller: 'mealinfoContent',
            backdrop: true,
            size: size,
            appendTo: parentElem,
            resolve: {
                items: function() {
                    return $scope.items
                }
            }
        })

        modalInstance.result.then(function(bindmealsdetails, mealsselectedlist) {
            var totprice = 0;
            var count = 0;
            $scope.fngrandtotal = 0;
            // console.log($scope.mealsData[parentIndex].travelerDetails[index]);
            // $scope.mealsselectedlist.push(JSON.parse(sessionStorage.getItem("mealsselectedlist")));
            bindmealsdetails.forEach(element => {
                if (!isNaN(element.totprice))
                    totprice = element.totprice + totprice;
                if (!isNaN(element.count))
                    count = Number(element.count) + count;
            });
            // console.log(selectedItem);
            // console.log($scope.mealsData[parentIndex].travelerDetails[index]);
            $scope.ssrbindData.meals[segkey].mealsData[parentIndex].travelerDetails.quantity = Number(count);
            $scope.ssrbindData.meals[segkey].mealsData[parentIndex].travelerDetails.price = Number(totprice);
            $scope.ssrbindData.meals[segkey].mealsData[parentIndex].travelerDetails.mealslist = bindmealsdetails;

            //Get totmeals
            getTotMeals();

        }, function() {
            $log.info('Modal dismissed at: ' + new Date());
        });
    }

    SEGsplit = function(or,dt){
        return or.split('(')[1].split(')')[0]+"⇌"+dt.split('(')[1].split(')')[0];
    }

    $scope.loggedvalue = JSON.parse(localStorage.getItem('islogged'));

    $scope.isLcc = $scope.flightBook.selectedResult.flightSearchResults[0].isLcc;

    // $scope.providerName="JJR";
    if (!$scope.isLcc) {

        $scope.serviceshide = "true";
    } else {
        $scope.serviceshide = "false";
        $scope.validateservicedata = "false";
    }
    if ($scope.searchReq.multicityInfos) {
        $scope.min = ($scope.searchReq.multicityInfos[0].strDeptDate).split('/')[2] + '-' + ($scope.searchReq.multicityInfos[0].strDeptDate).split('/')[1] + '-' + ($scope.searchReq.multicityInfos[0].strDeptDate).split('/')[0]

        var arrivalDateTime = $scope.searchReq.multicityInfos[0].strDeptDate;
        var depDateSplt = arrivalDateTime.split('/');
        $scope.adultmaxDate = parseInt(depDateSplt[2]) - 12;
        $scope.adultminDate = parseInt(depDateSplt[2]) - 76;
        $scope.adultmaxyear = $scope.adultmaxDate.toString() + '-' + parseInt(depDateSplt[1]) + '-' + parseInt(depDateSplt[0])
        $scope.adultminyear = $scope.adultminDate.toString() + '-' + parseInt(depDateSplt[1]) + '-' + parseInt(depDateSplt[0])
        $scope.childmaxyear = parseInt(depDateSplt[2]) - 2 + '-' + parseInt(depDateSplt[1]) + '-' + parseInt(depDateSplt[0]);
        $scope.childtminyear = (parseInt(depDateSplt[2]) - 12) + '-' + parseInt(depDateSplt[1]) + '-' + parseInt(depDateSplt[0])
        $scope.infminyear = (parseInt(depDateSplt[2]) - 2) + '-' + parseInt(depDateSplt[1]) + '-' + parseInt(depDateSplt[0])
        $scope.infmaxyear = parseInt(depDateSplt[2]) + '-' + parseInt(depDateSplt[1]) + '-' + parseInt(depDateSplt[0]);
    } else if ($scope.searchReq.strRetDate) {
        $scope.min = ($scope.searchReq.strRetDate).split('/')[2] + '-' + ($scope.searchReq.strRetDate).split('/')[1] + '-' + ($scope.searchReq.strRetDate).split('/')[0]
        var arrivalDateTime = $scope.searchReq.strRetDate;
        var depDateSplt = arrivalDateTime.split('/');
        $scope.adultmaxDate = parseInt(depDateSplt[2]) - 12;
        $scope.adultminDate = parseInt(depDateSplt[2]) - 76;
        $scope.adultmaxyear = $scope.adultmaxDate.toString() + '-' + parseInt(depDateSplt[1]) + '-' + parseInt(depDateSplt[0])
        $scope.adultminyear = $scope.adultminDate.toString() + '-' + parseInt(depDateSplt[1]) + '-' + parseInt(depDateSplt[0])
        $scope.childmaxyear = parseInt(depDateSplt[2]) - 2 + '-' + parseInt(depDateSplt[1]) + '-' + parseInt(depDateSplt[0]);
        $scope.childtminyear = (parseInt(depDateSplt[2]) - 12) + '-' + parseInt(depDateSplt[1]) + '-' + parseInt(depDateSplt[0])
        $scope.infminyear = (parseInt(depDateSplt[2]) - 2) + '-' + parseInt(depDateSplt[1]) + '-' + parseInt(depDateSplt[0])
        $scope.infmaxyear = parseInt(depDateSplt[2]) + '-' + parseInt(depDateSplt[1]) + '-' + parseInt(depDateSplt[0]);
    } else {
        // $scope.min=($scope.searchReq.onwardDateTime)
        $scope.min = ($scope.searchReq.strDeptDate).split('/')[2] + '-' + ($scope.searchReq.strDeptDate).split('/')[1] + '-' + ($scope.searchReq.strDeptDate).split('/')[0]

        var arrivalDateTime = $scope.searchReq.strDeptDate;
        var depDateSplt = arrivalDateTime.split('/');
        $scope.adultmaxDate = parseInt(depDateSplt[2]) - 12;
        $scope.adultminDate = parseInt(depDateSplt[2]) - 76;
        $scope.adultmaxyear = $scope.adultmaxDate.toString() + '-' + parseInt(depDateSplt[1]) + '-' + parseInt(depDateSplt[0])
        $scope.adultminyear = $scope.adultminDate.toString() + '-' + parseInt(depDateSplt[1]) + '-' + parseInt(depDateSplt[0])
        $scope.childmaxyear = parseInt(depDateSplt[2]) - 2 + '-' + parseInt(depDateSplt[1]) + '-' + parseInt(depDateSplt[0]);
        $scope.childtminyear = (parseInt(depDateSplt[2]) - 12) + '-' + parseInt(depDateSplt[1]) + '-' + parseInt(depDateSplt[0])
        $scope.infminyear = (parseInt(depDateSplt[2]) - 2) + '-' + parseInt(depDateSplt[1]) + '-' + parseInt(depDateSplt[0])
        $scope.infmaxyear = parseInt(depDateSplt[2]) + '-' + parseInt(depDateSplt[1]) + '-' + parseInt(depDateSplt[0]);
    }

    $(document).ready(function() {

        var current_fs, next_fs, previous_fs; //fieldsets
        var opacity;

        $(".next").click(function() {

            current_fs = $(this).parent();
            next_fs = $(this).parent().next();

            //Add Class Active
            $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

            //show the next fieldset
            next_fs.show();
            //hide the current fieldset with style
            current_fs.animate({ opacity: 0 }, {
                step: function(now) {
                    // for making fielset appear animation
                    opacity = 1 - now;

                    current_fs.css({
                        'display': 'none',
                        'position': 'relative'
                    });
                    next_fs.css({ 'opacity': opacity });
                },
                duration: 600
            });
        });

        $(".previous").click(function() {

            current_fs = $(this).parent();
            previous_fs = $(this).parent().prev();

            //Remove class active
            $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

            //show the previous fieldset
            previous_fs.show();

            //hide the current fieldset with style
            current_fs.animate({ opacity: 0 }, {
                step: function(now) {
                    // for making fielset appear animation
                    opacity = 1 - now;

                    current_fs.css({
                        'display': 'none',
                        'position': 'relative'
                    });
                    previous_fs.css({ 'opacity': opacity });
                },
                duration: 600
            });
        });

        $('.radio-group .radio').click(function() {
            $(this).parent().find('.radio').removeClass('selected');
            $(this).addClass('selected');
        });

        $(".submit").click(function() {
            return false;
        })

    });

    function clearFFFieldValues() {
        $('#profile_flyerNo').val("");
        $('#profileFlyerNo').val("");
        $('#profile_airline').val("");
        $('#profileAirlineName').val("");
        $('#profileAirlineCode').val("");
        $('#profile_AirlineCode').val("");
        $('#addFrequentFlyer').show();
    }
    $scope.removeFrequentFlyer = function(trvltype, frequentflyer, indexvalue) {
        $scope.freqflyerlist.pop(frequentflyer);
        if(trvltype == 'ADT')
        $scope.travellersListByRoom[0].adultTravellersList[indexvalue].personaldata.customerFfDtls.splice(frequentflyer,1);
        if(trvltype == 'CNN')
        $scope.travellersListByRoom[0].childTravellersList[indexvalue].personaldata.customerFfDtls.splice(frequentflyer,1);
        if(trvltype == 'INF')
        $scope.travellersListByRoom[0].infantTravellersList[indexvalue].personaldata.customerFfDtls.splice(frequentflyer,1);

    };

    $scope.freqflyerlist = [];
    $scope.ffChildObjEdit;
    $scope.addFrequentFlyer = function(indexvalue, trvltype) {
        // console.log("hi");
        var isValid = true;
        var frequentflyerObj = null;
        var mode = 'insert';
        if ($scope.ffChildObjEdit == null) {
            frequentflyerObj = new Object();
        } else {
            frequentflyerObj = $scope.ffChildObjEdit;
            mode = 'update';
        }
        $scope.airlineName = $('#profile_airline_'+trvltype+'_'+indexvalue).val();
			
        $scope.airCodes = $('#profile_flyerNo_'+trvltype+'_'+indexvalue).val();


        if ($scope.airlineName == "" || $scope.airlineName == null) {
            Lobibox.notify('error', { size: 'mini', delay: 5000,   position: 'right top', msg: "Please Choose Airline" });
            isValid = false;
        } else if ($scope.airCodes == "" || $scope.airCodes == null || $scope.airCodes == undefined) {
            Lobibox.notify('error', { size: 'mini', delay: 5000,  position: 'right top', msg: "Please Enter Flyer No" });
            isValid = false;
        } else {
            var ffairlinename = $scope.airlineName;
            var ffnumber = $scope.airCodes;
            $scope.profile_AirlineCode = ffairlinename;
            var ffairline = $scope.profile_AirlineCode;
            if (mode == 'insert') {
                if (!checkFFDtlDuplicate(ffairline)) {
                    isValid = false;
                    Lobibox.notify('warning', { size: 'mini', delay: 5000,  position: 'right top', msg: "Please Enter Flyer No" });
                }
            }
            if (isValid == true) {
                frequentflyerObj.airline = ffairlinename;
                /* frequentflyerObj.ffairline = ffairline; */
                frequentflyerObj.ffNo = ffnumber;
                clearFFFieldValues();
                if (mode == 'insert') {
                    $scope.freqflyerlist.push(frequentflyerObj);
                    if(trvltype == 'ADT'){
                    $scope.travellersListByRoom[0].adultTravellersList[indexvalue].personaldata.customerFfDtls=($scope.travellersListByRoom[0].adultTravellersList[indexvalue].personaldata.customerFfDtls || []);
                    $scope.travellersListByRoom[0].adultTravellersList[indexvalue].personaldata.customerFfDtls.push(frequentflyerObj);
                    }
                    if(trvltype == 'CNN'){
                    $scope.travellersListByRoom[0].childTravellersList[indexvalue].personaldata.customerFfDtls=($scope.travellersListByRoom[0].childTravellersList[indexvalue].personaldata.customerFfDtls || []);
                    $scope.travellersListByRoom[0].childTravellersList[indexvalue].personaldata.customerFfDtls.push(frequentflyerObj);
                    }
                    if(trvltype == 'INF'){
                    $scope.travellersListByRoom[0].infantTravellersList[indexvalue].personaldata.customerFfDtls=($scope.travellersListByRoom[0].infantTravellersList[indexvalue].personaldata.customerFfDtls || []);

                    $scope.travellersListByRoom[0].infantTravellersList[indexvalue].personaldata.customerFfDtls.push(frequentflyerObj);
                    }
                    //   console.log("array of flyerlist", $scope.freqflyerlist);
                    Lobibox.notify('success', { size: 'mini', delay: 5000,  position: 'right top', msg: "Airline Added Successfully" });
                } else {
                    Lobibox.notify('success', { size: 'mini', delay: 5000,   position: 'right top',msg: "Airline Updated Successfully" });
                    $scope.ffChildObjEdit = null;
                }
            }
        }

        $scope.airlineName = $('#profile_airline_'+trvltype+'_'+indexvalue).val("");
			
        $scope.airCodes = $('#profile_flyerNo_'+trvltype+'_'+indexvalue).val("");
    };


    function checkFFDtlDuplicate(currValue) {
        var isValid = true;
        if ($scope.freqflyerlist.length != 0) {
            for (var i = 0, size = $scope.freqflyerlist.length; i < size; i++) {
                var value = $scope.freqflyerlist[i].ffairline;
                if (currValue == value) {
                    isValid = false;
                }
            }
        }
        return isValid;
    }




    
    $scope.autoAirlines = function(value) {
            var token = JSON.parse(sessionStorage.authToken);
            var headersOptions = {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token
            }
        if (value != null && value.length == 1) {
            $http.get(ServerService.serverPath + 'flight/airlines/' + value.toUpperCase(), {
                headers: headersOptions
            }).then(function successCallback(response, status) {
                $scope.airlines = [];
                $scope.airlineCode = [];
                var airlist = response.data.data
                if (response.data.data != 'null' && response.data.data.length > 0) {
                    for (var i = 0; i < response.data.data.length; i++) {
                        var airfrom = {
                            Title: response.data.data[i].displayname + ' - ' + '(' + response.data.data[i].shortName + ')'

                        }
                        var aircode = {
                            Code: response.data.data[i].shortName
                        }
                        $scope.airlines.push(airfrom);
                        $scope.airlineCode.push(aircode);
                        //console.log($scope.airlines);

                    }
                    $scope.airlines.sort();
                    $scope.airlineCode.sort();
                }


            }, function errorCallback(response) {
                if(response.status == 403){
                    var status = refreshService.refreshToken(token.data.refreshToken,token.data.authToken);
                    status.then(function(greeting) {
                    $scope.autoAirlines(value);
                    });
                }
                // called asynchronously if an error occurs
                // or server returns response with an error status.
              });
        }


    }


    $scope.airCodeChange = function(value) {
        $scope.autoAirlines(value)
    }




    /**
    @author Raghava Muramreddy
    to showing payment error message
    */
    // $scope.isActive = false;


    $scope.payment = false;

    $scope.receiveInfo = {
        value: 'yes'
    }
    $scope.currencies = ConstantService.currency;
    if (sessionStorage.getItem("paymentError")) {
        var paymentError = JSON.parse(sessionStorage.getItem("paymentError"));
        Lobibox.alert('error', {
            msg: paymentError.errorMessage,
            callback: function(lobibox, type) {
                if (type === 'ok') {
                    sessionStorage.removeItem("paymentError");
                }
            }
        });

    }
    $scope.seatTotal = 0;
    $scope.mealTotal = 0;
    // $scope.seatSelect= function (s){$scope.selectedButton =s }
    $scope.secondMeal = true;

    $(document).on('change keyup', '.required', function(e) {
        let Disabled = true;
        $(".required").each(function() {
            let value = this.value
            if ((value) && (value.trim() != '')) {
                Disabled = false
            } else {
                Disabled = true
                return false
            }
        });

        if (Disabled) {
            $('.toggle-disabled').prop("disabled", true);
        } else {
            $('.toggle-disabled').prop("disabled", false);
        }
    })

    $scope.travelerselectdata = '';

    $scope.pamentbutton = true;

    $scope.showMealQuantity = false;

    $scope.goBack = function(path) {
        $location.path(path);
    }

    var loginUser = "";
    $scope.user = {};

    $scope.showSignIn = false;

    var loginCredential;
    $scope.isValidUser = false;
    var booking = {};

    $("#contact").on("keyup", function(e) {
        if ($(this).val() === '0') {
            $(this).val('');
        }
    });
    //for (var a=0; a<5; a++){


    $("#acontact_0").on("keyup", function(e) {
        if ($(this).val() === '0') {
            $(this).val('');
        }
    });
    //}

    var allSegments=[];
    $scope.flightResults=JSON.parse(sessionStorage.getItem('flightSearchRS'));
    $scope.flightDeatailsList=JSON.parse(sessionStorage.getItem('firstFlightResponse'));
    // $scope.flightDeatailsList=$scope.flightResults.data.flightSearchResults;
    var Outbound=$scope.flightDeatailsList;

    for(var d=0; d<Outbound.segList[0].fltInfoList.length; d++){
        // var origin=Outbound.segList[0].fltInfoList[d].origin.split(',')[1].trim();
        // var destination=Outbound.segList[0].fltInfoList[d].dest.split(',')[1].trim();
        var origin=Outbound.segList[0].fltInfoList[d].origin;
        var destination=Outbound.segList[0].fltInfoList[d].dest;
        if(!allSegments.includes(origin))
        allSegments.push(origin);
        if(!allSegments.includes (destination))
        allSegments.push(destination);

    }



    $scope.normalnationalityIds=true;
    $scope.nationalityChange=function(nationalty,indexvalue){
       for(var a=0;a<$scope.travellersListByRoom[0].adultTravellersList.length; a++){
        $scope.travellersListByRoom[0].adultTravellersList[indexvalue].personaldata.passIssueGovt=nationalty;
       }
        if(allSegments.length==1){
            $scope.countryResult.forEach(nlt=>{
                if(nlt.code==nationalty){
                    $scope.saudinationalityId= allSegments[0].toUpperCase().includes (nlt.name.toUpperCase());
                    // if(  $scope.saudinationalityId=='true'){
                    //     $scope.saudinationalityIds=true;
                    //     $scope.normalnationalityIds=false;
                    // }
                    //console.log( $scope.nationalityId)
                    return;
                }
            })
      
     
        }  
       else{
        $scope.nationalityId  = false;
       }
    }

    $scope.nationalityChangeChaild=function(nationalty,indexvalue){
        for(var a=0;a<$scope.travellersListByRoom[0].childTravellersList.length; a++){
            $scope.travellersListByRoom[0].childTravellersList[indexvalue].personaldata.passIssueGovt=nationalty;
           }
        if(allSegments.length==1){
            $scope.countryResult.forEach(nlt=>{
                if(nlt.code==nationalty){
                    $scope.saudinationalityIdchild= allSegments[0].toUpperCase().includes (nlt.name.toUpperCase());
                    // if(  $scope.saudinationalityId=='true'){
                    //     $scope.saudinationalityIds=true;
                    //     $scope.normalnationalityIds=false;
                    // }
                    //console.log( $scope.nationalityId)
                    return;
                }
            })
      
     
        }  
       else{
        $scope.nationalityId  = false;
       }
    }
    
    $scope.nationalityChangeInfant=function(nationalty,indexvalue){
        for(var a=0;a<$scope.travellersListByRoom[0].infantTravellersList.length; a++){
            $scope.travellersListByRoom[0].infantTravellersList[indexvalue].personaldata.passIssueGovt=nationalty;
           }
        if(allSegments.length==1){
            $scope.countryResult.forEach(nlt=>{
                if(nlt.code==nationalty){
                    $scope.saudinationalityIdInfant= allSegments[0].toUpperCase().includes (nlt.name.toUpperCase());
                    // if(  $scope.saudinationalityId=='true'){
                    //     $scope.saudinationalityIds=true;
                    //     $scope.normalnationalityIds=false;
                    // }
                    //console.log( $scope.nationalityId)
                    return;
                }
            })
      
     
        }  
       else{
        $scope.nationalityId  = false;
       }
    }

    $scope.month = Month;
    $scope.year = Year;


    $scope.seatDetailss = null;
    $scope.mealDetails = null;

    $scope.totalSSR = 0;


    $scope.searchtotalldata = $scope.flightBook.selectedResult;
    $scope.myJson = $scope.flightBook.selectedResult.flightSearchResults;
    $scope.outsegpref = $scope.flightBook.selectedResult.flightSearchResults[0].segGrpList[0];
    if($scope.flightBook.selectedResult.flightSearchResults[0].segGrpList.length>1)
    $scope.insegpref = $scope.flightBook.selectedResult.flightSearchResults[0].segGrpList[1];

    /* $scope.bagageinfo=$scope.flightBook.selectedResult.flightSearchResults[0].baggageDtls[0].baggageInfo; */
    $scope.baggageDetails = $scope.flightBook.selectedResult.flightSearchResults[0].baggageDtls;

    $scope.travelerscount = [];
    $scope.baggagedata = false



    $scope.flightBooking = $scope.flightBook.selectedResult;
    //console.log("$scope.flightBooking",$scope.flightBooking);
    $scope.fromairport = $scope.flightBooking.frmAirprt;
    $scope.toairport = $scope.flightBooking.toAirprt;
    $scope.flightresults = $scope.flightBooking.flightSearchResults;
    

    // $scope.bagdetails=$scope.flightresults[0].baggageDtls;
    // $scope.bagageinfo=$scope.bagdetails[0].baggageInfo;
    $scope.totalFare = $scope.flightBooking.flightSearchResults[0].totFare;

    $scope.fetchCity = function(place) {
        var placeSplit = place.split(',');
        return placeSplit[0];
    }

    seatPef = function(){
    $scope.tokenresult = JSON.parse(sessionStorage.getItem('authToken'));
        var headersOptions = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + $scope.tokenresult
        }
        
        $http({
            method: 'GET',
            url: ServerService.listPath + 'common?codeType=SEAT_PREF',
            // data: country
            // {
            headers: headersOptions
                // }
        }).then(function successCallback(data, status) {
            if (status = 'success') {


                $rootScope.seatList = data.data.data;
                mealPef();
            } else {
                alert('error');
            }
        }, function errorCallback(response) {
            if(response.status == 403){
             var status= refreshService.refreshToken(token.refreshToken,token.authToken);
             status.then(function(greeting) {
                seatPef();
            });
            }
            // called asynchronously if an error occurs
            // or server returns response with an error status.
          });

    }


    
    mealPef = function(){
        $scope.tokenresult = JSON.parse(sessionStorage.getItem('authToken'));
            var headersOptions = {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' +     $scope.tokenresult
            }
        $http({
            method: 'GET',
            url: ServerService.listPath + 'common?codeType=MEAL_PREF',
            // data: country
            // {
            headers: headersOptions
                // }
        }).then(function successCallback(data, status) {
            if (status = 'success') {


                $rootScope.totallMealsList = data.data.data;
            } else {
                alert('error');
            }
        }, function errorCallback(response) {
            if(response.status == 403){
                var status = refreshService.refreshToken(token.refreshToken,token.authToken);
                status.then(function(greeting) {
                mealPef();
                });
            }
            // called asynchronously if an error occurs
            // or server returns response with an error status.
          });
    }

    nationalityget = function(){
        $scope.tokenresult = JSON.parse(sessionStorage.getItem('authToken'));
            var headersOptions = {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' +  $scope.tokenresult
            }
        $http({
            method: 'GET',
            url: ServerService.listPath + 'list/nationality',

            headers: headersOptions
        }).then(function successCallback(data, status) {
            if (status = 'success') {
                $scope.nationalityResult = data;
                $scope.allNationalitys = $scope.nationalityResult.data.data;
                $scope.nationality1 = $scope.allNationalitys;
                $scope.nationality = JSON.stringify($scope.nationality1);
                countrycode();
            } else {
                alert('error');
            }

        }, function errorCallback(response) {
            if(response.status == 403){
                var status = refreshService.refreshToken(token.refreshToken,token.authToken);
                status.then(function(greeting) {
                nationalityget();
                });
            }
            // called asynchronously if an error occurs
            // or server returns response with an error status.
          });
    }


    countryget = function(){
        $scope.tokenresult = JSON.parse(sessionStorage.getItem('authToken'));
            var headersOptions = {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' +  $scope.tokenresult
            }
        $http({
            method: 'GET',
            url:  ServerService.listPath + 'list/country',

            headers: headersOptions
        }).then(function successCallback(data, status) {
            if (status = 'success') {
                $scope.countryResult = data.data.data;
            } else {
                alert('error');
            }

        }, function errorCallback(response) {
            if(response.status == 403){
                var status = refreshService.refreshToken(token.refreshToken,token.authToken);
                status.then(function(greeting) {
                countryget();
                });
            }
            // called asynchronously if an error occurs
            // or server returns response with an error status.
          });
    }

    countryget();
    gettingssr = function() {
        $scope.index = JSON.parse(sessionStorage.getItem('flightindex'));
        var sessionId=JSON.parse(sessionStorage.getItem('flightSessionId'));
        var repriceDetails = {
            "sessionId": sessionId,
            "resultIndexId": $scope.index
        }


        // if (localStorage.loginToken != null) {
        //     $scope.loggedvalue = true;
        // }
            var token = JSON.parse(sessionStorage.authToken);
            var headersOptions = {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token
            }
        $http.post(ServerService.dropDownPath+ 'customer/'+ $scope.corprateCompanyId + '/flight/ssr-new', repriceDetails, {

            headers: headersOptions
        }).then(function successCallback(response) {
            $scope.artloading = false;

            if (!response.data.success) {
                $scope.noSsr = false;
                $scope.noSsrerror = response.data.errorMessageS;
                sessionStorage.removeItem('ssrtotalData');
                $scope.fltbking = true;
                $scope.seatFunction=false;
                $scope.mealFunction = false;
                $scope.ssrFunction = false;
            } else {
                //console.log(response);

                $scope.flightSsrData = response
                sessionStorage.setItem('ssrtotalData', JSON.stringify($scope.flightSsrData));

                $scope.ssrTotalData = JSON.parse(sessionStorage.getItem('ssrtotalData'));
                $scope.noSsr = true;
            }

            // $scope.outboundsegemtData= $scope.ssrTotalData.data['DEL-SHJ'];
            // console.log($scope.ssrTotalData.data['DEL-SHJ'],"$scope.ssrTotalData.data['DEL-SHJ']");


        }, function errorCallback(response) {
            if(response.status == 403){
                var status = refreshService.refreshToken(token.refreshToken,token.authToken);
                status.then(function(greeting) {
                    gettingssr();
                   });                
            }
        });
    }
    countrycode = function(){
        $scope.tokenresult = JSON.parse(sessionStorage.getItem('authToken'));
            var headersOptions = {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + $scope.tokenresult
            }
        $http.get(ServerService.listPath + 'list/phone-country', {

            headers: headersOptions
        }).then(function successCallback(response) {
            $scope.loading = false;
            $scope.disabled = false;
            if (response.data.success) {
                $scope.countryList = response.data.data;

                for(var a=0; a<  $scope.countryList.length;a++ ){
                    if( $scope.countryList[a].name==='971'){
                        $scope.guestuser.ccode=$scope.countryList[1];
                        $scope.travellersListByRoom[0].adultTravellersList[0].personaldata.mobcntrycodetrobj = $scope.guestuser.ccode;
                    }
                }

                // var index=$scope.countryList.splice($scope.countryList.findIndex(a => a.name ===971) , 1)
	  
                // $scope.guestuser.ccode=$scope.countryList[1];
                
            
                if($scope.isLcc){
                    gettingssr();
                    $scope.selectedLst = JSON.parse(sessionStorage.getItem('Selectedssrlist'));
                    if($scope.selectedLst != undefined){
                        $scope.loopTot =$scope.selectedLst.totbagPrice;
                
                        $scope.totMeals =$scope.selectedLst.totmealPrice;

                        $scope.totseat =$scope.selectedLst.totseatPrice;
                        $scope.ssrbindData = $scope.selectedLst.ssrlst;
                        $scope.fngrandtotal = $scope.selectedLst.fngrandtotal;
                        $scope.discount = $scope.selectedLst.discount;

                    }
                }else{
                    $scope.artloading = false;
                }
                // $scope.allcountrys = response.data.data;
                // $scope.countryList = $scope.allcountrys.sort((a, b) => a.name.localeCompare(b.name));
                //     } else {
            }
        }, function errorCallback(response) {
            if(response.status == 403){
                var status = refreshService.refreshToken(token.refreshToken,token.authToken);
                status.then(function(greeting) {
                countrycode();
                });
            }
            // called asynchronously if an error occurs
            // or server returns response with an error status.
          });
    }
    nationalityget();

    dropdownfun = function(){
        seatPef();
        // countrycode();
        $scope.tokenresult = JSON.parse(sessionStorage.getItem('authToken'));
            var headersOptions = {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' +  $scope.tokenresult
            }
        $http({
            method: 'GET',
            url: ServerService.listPath + 'list/airline',
            // data: country
            // {
            headers: headersOptions
                // }
        }).then(function successCallback(data, status) {
            if (status = 'success') {


                $rootScope.freqFlyerList = data.data.data;
            } else {
                alert('error');
            }
        }, function errorCallback(response) {
            if(response.status == 403){
                var status = refreshService.refreshToken(token.refreshToken,token.authToken);
                status.then(function(greeting) {
                mealPef();
                });
            }
            // called asynchronously if an error occurs
            // or server returns response with an error status.
          });

    };

    dropdownfun();
    $scope.travellerSelect = function(b, f, t) {
        $scope.sbound = b;
        $scope.sflight = f;
        $scope.straveller = t;
        $scope.seatRows = $scope.seatDetails[b].flightInfoDtls[f].seatRows;
        $scope.seatIterate(b, f, t, 'travel');
    };

    $scope.seatRemove = function(b, f, t) {
        $('#seatMapTraveller_' + b + '_' + f + '_' + t).find('.seatNo').empty();
        $('#seatMapTraveller_' + b + '_' + f + '_' + t).find('.seatPrice').empty();
        $('#seatMapTraveller_' + b + '_' + f + '_' + t).find('.seatremove').hide();
        $scope.seatIterate(b, f, t, 'seat');
    };

    $scope.sbound = 0;
    $scope.sflight = 0;
    $scope.straveller = 0;
    // $scope.seatRows = $scope.seatDetails[$scope.sbound].flightInfoDtls[$scope.sflight].seatRows;
    /* $timeout(function(){
        document.getElementById('traveller_0_0_0').checked = true;
    },10);
    */

    $scope.baggagedataList = [];


    $scope.bagChange = function(bagData,selectbag,index) {
        // console.log(bagData);
        $scope.baggagedataList = [];
        $scope.loopTot = [];
        $scope.showBag = true;
        $scope.grandtotal = $scope.myJson[0].grndTotal;
        $scope.loopTot = 0;
        var zero = '0';
        $scope.fngrandtotal = 0;
        for(let baglst in $scope.ssrbindData.baggages)
        {
            if($scope.ssrbindData.baggages[baglst].baggageData != undefined && $scope.ssrbindData.baggages[baglst].baggageData.length>0)
            {
                for (var b = 0; b < $scope.ssrbindData.baggages[baglst].baggageData.length; b++) 
                {
                    if ($scope.ssrbindData.baggages[baglst].baggageData[b].passengerType != 'Infant') 
                    {
                        if($scope.ssrbindData.baggages[baglst].baggageData[b].travellerBaggages.selectedBaggage != undefined)
                        {
                            // $scope.baggagedataList.push($scope.baggageData[b].travellerBaggages[tevlbg].selectedBaggage);
                            $scope.isbagavailable = true;
                        
                            $scope.loopTot += $scope.ssrbindData.baggages[baglst].baggageData[b].travellerBaggages.selectedBaggage.price;
                        }
                    }
                }
            }
        }
        return $scope.loopTot;
    }

    // $scope.nationality1 = JSON.parse(localStorage.getItem('nationality')).data;
    $scope.nationality = JSON.stringify($scope.nationality1);
    $scope.salutations = ["Mr", "Mrs", "Miss"];
    $scope.childsalutations = ["Miss", "Mstr"];
    $scope.infantsalutations = ["Miss", "Mstr"];
    var personaldata = function() {
        this.gender = "M";
        this.salutation = "";
        this.fname = '';
        this.lname = '';
        this.dob = '';
        this.mobcntrycode = '';
        this.mobile = null;;
        this.email = null;
        this.nationalitycode = null;
        this.nationalityname = null;
        this.passportNo = null;
        // this.code="";
        // this.charge="";
        this.trvtype = "ADT";
        this.age = 32;
        this.city = "Chennai";
        this.custPrefTypeInbound = null;
        this.custPrefTypeOutbound = null;
        this.seatPrefTypeOutbound = null;
        this.seatPrefTypeInbound = null;
        this.customerFfDtls = []
        this.country = "IN";
        this.birthmonth = "";
        this.birthdate = "";
        this.birthyear = "";
        this.expirymonth="";
        this.expirydate="";
        this.expiryyear="";
    }
    var childPersonaldata = function() {
        this.gender = "M";
        this.salutation = "";
        this.fname = '';
        this.lname = '';
        this.dob = '';
        this.mobcntrycode = '';
        this.mobile = null;;
        this.email = null;
        this.nationalitycode = null;
        this.nationalityname = null;
        this.passportNo = null;
        // this.code="";
        // this.charge="";
        this.trvtype = "CNN";
        this.age = 5;
        this.city = "Chennai";
        this.custPrefTypeInbound = null;
        this.custPrefTypeOutbound = null;
        this.seatPrefTypeOutbound = null;
        this.seatPrefTypeInbound = null;
        this.customerFfDtls = []
        this.country = "IN";
        this.birthmonth = "";
        this.birthdate = "";
        this.birthyear = "";
        this.expirymonth="";
        this.expirydate="";
        this.expiryyear="";
    }
    var infantPersonaldata = function() {
        this.gender = "M";
        this.salutation = "";
        this.fname = '';
        this.lname = '';
        this.dob = '';
        this.mobcntrycode = '';
        this.mobile = null;;
        this.email = null;
        this.nationalitycode = null;
        this.nationalityname = null;
        this.passportNo = null;
        // this.code="";
        // this.charge="";
        this.trvtype = "INF";
        this.age = 1;
        this.city = "Chennai";
        this.custPrefTypeInbound = null;
        this.custPrefTypeOutbound = null;
        this.seatPrefTypeOutbound = null;
        this.seatPrefTypeInbound = null;
        this.customerFfDtls = []
        this.country = "IN";
        this.birthmonth = "";
        this.birthdate = "";
        this.birthyear = "";
        this.expirymonth="";
        this.expirydate="";
        this.expiryyear="";
    }

    var miscdata = function(){
        this.mealpref=[
            {
                "code":null
            }
        ];
        this.seatpref =[
            {
                "code":null
            }
        ];
    }
    var traveller = function() {
        this.personaldata = new personaldata();
        this.miscdata = new miscdata();
        /*  this.agntid= 0;
         this.profileid= 0;
         this.travellerRefRPH= 'A1'; */
    }
    var childTraveller = function() {
        this.personaldata = new childPersonaldata();
        this.miscdata = new miscdata();
        /*  this.agntid= 0;
         this.profileid= 0;
         this.travellerRefRPH= 'A1'; */
    }
    var infantTraveller = function() {
        this.personaldata = new infantPersonaldata();
        this.miscdata = new miscdata();
        /*  this.agntid= 0;
         this.profileid= 0;
         this.travellerRefRPH= 'A1'; */
    }

    var travellerByRoom = function() {
        this.adultTravellersList = [];
        this.childTravellersList = [];
        this.infantTravellersList = [];
    }

    $scope.traveller = function() {
        this.personaldata = new personaldata();
        /*  this.agntid= 0;
         this.profileid= 0;
         this.travellerRefRPH= 'A1'; */
    }

    /**We have to maintain same Order below until "update Traveller Details end" */
    var travellerByRoomObj = new travellerByRoom();
    $scope.travellersListByRoom = JSON.parse(sessionStorage.getItem("travellersListByRoom"));
    if ($scope.travellersListByRoom != null) {

        $scope.travelerscount = [];
        $scope.travelerscount.push($scope.travellersListByRoom[0].adultTravellersList);
        $scope.travelerscount.push($scope.travellersListByRoom[0].childTravellersList);
        $scope.travelerscount.push($scope.travellersListByRoom[0].infantTravellersList);
    }

    $scope.userDetails = JSON.parse(sessionStorage.getItem("contactDetails"))
    $scope.guestLoginName = JSON.parse(sessionStorage.getItem("guestLoginDatails"))
    if (sessionStorage.getItem('guestLoginDatails') && sessionStorage.getItem('guestLoginDatails') != "null") {
        $scope.loggedvalue = true;
        $scope.$parent.bookingHide = true;
        $scope.$parent.guestLogin = true;
        $scope.$parent.bookingHide1 = true;
        $rootScope.dispName = $scope.guestLoginName;
    }
    if ($scope.userDetails != null) {
        $scope.user = $scope.userDetails;
    }
    /* check user Already login*/
    if (localStorage.getItem('loginName') && localStorage.getItem('loginName') != "null") {
        $scope.isValidUser = true;
        $scope.loggedvalue = true;
        $rootScope.loginsignIn=true;
        loginUser = JSON.parse(localStorage.getItem('loginName'));
        $scope.user.email = loginUser.email;
        $scope.user.contactNumber = parseInt(loginUser.contactNumber);
        $scope.user.mobcntrycode = loginUser.mobcntrycode;
        /*check user Already login end*/
        /**update Traveller Details  */

        if ($scope.travellersListByRoom == undefined) {
            $scope.travellersListByRoom = [];
            // var travellerByRoomObj=new travellerByRoom();
            for (i = 0; i < $scope.flightBooking.adtCnt; i++) {
                var travellerObj = new traveller();
                travellerByRoomObj.adultTravellersList.push(travellerObj);
                travellerObj.personaldata.email = $scope.user.email;
                // travellerObj.personaldata.fname = $scope.user.fname;
                travellerObj.personaldata.mobile = parseInt($scope.user.contactNumber);
                travellerObj.personaldata.mobcntrycode = $scope.user.mobcntrycode;
            }
            for (j = 0; j < $scope.flightBooking.chldCnt; j++) {
                var travellerObj = new childTraveller();
                travellerByRoomObj.childTravellersList.push(travellerObj);
                travellerObj.personaldata.email = $scope.user.email;
                // travellerObj.personaldata.fname = $scope.user.fname;
                travellerObj.personaldata.mobile = parseInt($scope.user.contactNumber);
                travellerObj.personaldata.mobcntrycode = $scope.user.mobcntrycode;
            }
            for (j = 0; j < $scope.flightBooking.infntCnt; j++) {
                var travellerObj = new infantTraveller();
                travellerByRoomObj.infantTravellersList.push(travellerObj);
                travellerObj.personaldata.email = $scope.user.email;
                // travellerObj.personaldata.fname = $scope.user.fname;
                travellerObj.personaldata.mobile = parseInt($scope.user.contactNumber);
                travellerObj.personaldata.mobcntrycode = $scope.user.mobcntrycode;
            }
            $scope.travellersListByRoom.push(travellerByRoomObj);

        }

    } else {

        if ($scope.travellersListByRoom == undefined) {
            $scope.travellersListByRoom = [];
            var travellerByRoomObj = new travellerByRoom();
            for (i = 0; i < $scope.flightBooking.adtCnt; i++) {
                var travellerObj = new traveller();
                travellerByRoomObj.adultTravellersList.push(travellerObj);
            }
            for (j = 0; j < $scope.flightBooking.chldCnt; j++) {
                var travellerObj = new childTraveller();
                travellerByRoomObj.childTravellersList.push(travellerObj);
            }
            for (j = 0; j < $scope.flightBooking.infntCnt; j++) {
                var travellerObj = new infantTraveller();
                travellerByRoomObj.infantTravellersList.push(travellerObj);
            }
            $scope.travellersListByRoom.push(travellerByRoomObj);

        }

    }
    
    // document.querySelector(".guestmobilenumm").addEventListener("keypress", function (evt) {
    //     if (evt.which != 8 && evt.which != 0 && evt.which < 48 || evt.which > 57)
    //     {
    //         evt.preventDefault();
    //     }
    // });

    
    $scope.selectpaxChecked = function(i,pax,paxAge) {
        document.getElementById('checkedvalue'+i).checked = true;
        $scope.addTravel('true',pax,paxAge,i);
    }

    /*assign guestUserDetails to Travellers */
    $scope.updateGustUserDetailsToTravller = function() {
            $scope.travellersListByRoom[0].adultTravellersList[0].personaldata.email = jQuery('#email').val();
            if ($scope.user.emailId) {
                $scope.travellersListByRoom[0].adultTravellersList[0].personaldata.email = $scope.user.emailId;
            }
            $scope.travellersListByRoom[0].adultTravellersList[0].personaldata.mobile = $scope.guestuser.contactNumber;

            $scope.travellersListByRoom[0].adultTravellersList[0].personaldata.mobcntrycodetrobj = $scope.guestuser.ccode;

        }
        /*assign guestUserDetails to Travellers end*/


    $scope.mealCalculate = function() {
        $scope.mealTotal = 0;
        if ($scope.showBag == false) {
            $scope.baggageTot = $scope.totBagCal();
        } else {
            $scope.baggageTot = $scope.loopBagFun();
        }
        $scope.grandtotal = $scope.myJson[0].grndTotal + $scope.loopTot + $scope.seatTotal + $scope.insureAmt;
        $('.mealCharge').each(function() {
            if ($(this).text() != '') {
                $scope.mealTotal += parseInt($(this).text());
            }
        });
        $scope.grandtotal += $scope.mealTotal;
    };

    $scope.noMeal = function(likeMeal) {
        $scope.likeMeal = likeMeal;
        if (likeMeal == 0) {
            $scope.totMeals = 0;
             // sessionStorage.removeItem('SelectedMealLst');
             $timeout(function(){
                for(let meal in $scope.ssrbindData.meals)
                {
                    if($scope.ssrbindData.meals[meal].meals.length>0)
                    {
                        var meallist = $scope.ssrbindData.meals[meal].mealsData;
                        for(var bg=0; bg<meallist.length; bg++)
                        {
                            $scope.ssrbindData.meals[meal].mealsData[bg].travelerDetails.price = 0 ;
                            $scope.ssrbindData.meals[meal].mealsData[bg].travelerDetails.quantity = 0 ;
                            $scope.ssrbindData.meals[meal].mealsData[bg].travelerDetails.mealslist.forEach(mlst => 
                            {
                                if(mlst.count > 0)
                                {
                                    mlst.count = 0;
                                }
                            })
                        }
                    }
                }
            },10);
    $scope.fngrandtotal = 0;
            $scope.grandtotal = $scope.myJson[0].grndTotal + $scope.loopTot + $scope.seatTotal + $scope.insureAmt;
        } else {
            // $scope.mealCalculate();
        }
    }
    
    $scope.noseatshow = true;
    $scope.noSeat = function(likeseat) {
        $scope.likeSeat = likeseat;
        if (likeseat == 1) {
            $scope.noseatshow = false;
            $scope.totseat = 0;
            $timeout(function(){

                for(let seatlst in $scope.ssrbindData.seats)
                {
                    if($scope.ssrbindData.seats[seatlst].seatData.length>0)
                    {
                        var seatlist = $scope.ssrbindData.seats[seatlst].seatData;
                        for(var bg=0; bg<seatlist.length; bg++)
                        {
                            $scope.ssrbindData.seats[seatlst].seatData[bg].travelerDetails.price = 0;
                            $scope.ssrbindData.seats[seatlst].seatData[bg].travelerDetails.seatNumber = '-';
                            $scope.ssrbindData.seats[seatlst].seatData[bg].travelerDetails.seatsslist[0].selectedSeat = null;
                        }
                    }
                }
            },10);
            
    $scope.fngrandtotal = 0;
    $scope.seats = [];  
            $scope.grandtotal = $scope.myJson[0].grndTotal + $scope.loopTot + $scope.seatTotal + $scope.insureAmt;
        } else {
            $scope.noseatshow = true;
            // $scope.mealCalculate();
        }
    }


    /**
         //@author Raghava Murmareddy
        get Selected Insurance Details which we select at Review Time
    	
    */
    $scope.selectedInsurancePlanId = sessionStorage.getItem('selectedInsurancePlanId');
    $scope.travelInsurancee = JSON.parse(sessionStorage.getItem('travelInsurancee'));

    if ($scope.travelInsurancee != undefined && $scope.selectedInsurancePlanId != null) {
        $scope.withInsurance = true;
        $scope.selectedInsurancePlan = $scope.travelInsurancee.availablePlans[$scope.selectedInsurancePlanId];
        $scope.selectedInsurancePlantotalFare = $scope.selectedInsurancePlan.totalFare;
    } else {
        $scope.selectedInsurancePlantotalFare = 0;
        
    }
    $scope.fltbking = false;

    $scope.fngrandtotal = 0;
    repriceCall= function(selectedSSRBean){
        var token = JSON.parse(sessionStorage.authToken);
        var headersOptions = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token
        }
        // var flightPrceDatasession = JSON.parse(sessionStorage.getItem('flightPriceData'));
        // var sessionId=flightPrceDatasession.sessionId
        var sessionId=JSON.parse(sessionStorage.getItem('flightSessionId'));
        
    var repriceDetail = {
            "sessionId": sessionId,
            "resultIndexId": JSON.parse(sessionStorage.getItem('flightindex')),
            "selectedSSRBean": selectedSSRBean
        }
    sessionStorage.setItem("ssrRQ",JSON.stringify(selectedSSRBean));
        $scope.artloading = true;
    $http.post(ServerService.dropDownPath+ 'customer/'+ $scope.corprateCompanyId + '/flight/reprice', repriceDetail, {
        headers: headersOptions
    }).then(function successCallback(response) {
        if(response.data.success){
            if(response.data.statusCode == "S003"){
                Lobibox.alert('success', {
                    msg:  response.data.data.msg,
                    // callback: function (lobibox, type) {
                    //   if (type === 'ok') {
                       
                    //   }
                    // }
                  });
                  $scope.artloading = false;
                  $scope.loading = false;
                  $scope.disabled = false;
                //   $scope.validateservicedatapament = true;
                //   $scope.validateservicedata = false;
                  $scope.validatedata = false;
                  $scope.validateguestdata = true;
                  $scope.fngrandtotal = response.data.data.selectedResult.flightSearchResults[0].grndTotal;
                  sessionStorage.setItem('flightPriceData', JSON.stringify(response.data));
                  $scope.flightBookSession = JSON.parse(sessionStorage.getItem('flightPriceData'));
                  $scope.flightBook = $scope.flightBookSession.data;
                  $scope.flightBooking = $scope.flightBook.selectedResult;
                  $scope.fltbking = true;
                  $scope.ssrFunction=false;

           }else{
                $scope.artloading = false;
                $scope.loading = false;
                $scope.disabled = false;
                // $scope.validateservicedatapament = true;
                // $scope.validateservicedata = false;
                $scope.validatedata = false;
                $scope.validateguestdata = true;
                sessionStorage.setItem('flightPriceData', JSON.stringify(response.data));
                $scope.flightBookSession = JSON.parse(sessionStorage.getItem('flightPriceData'));
                $scope.flightBook = $scope.flightBookSession.data;
                 $scope.flightBooking = $scope.flightBook.selectedResult;
        $scope.ssrFunction=false;
        $scope.fltbking = true;
           }

        }else{
            Lobibox.alert('error',
            				{
            					msg: response.data.errorMessage
            			});
        }
        $scope.artloading = false;

    }, function errorCallback(response) {
        if(response.status == 403){
           var status =  refreshService.refreshToken(token.data.refreshToken,token.data.authToken);
           status.then(function(greeting) {
            repriceCall(selectedSSRBean);
        });
        }
    });
    }

    $scope.seatfunctionality = function() 
    {
        $scope.loading = false;
        $scope.billing=true
        $scope.ssrTotalData = JSON.parse(sessionStorage.getItem('ssrtotalData'));
        // var selectedSSRBean = {};
        // selectedSSRBean.selectedSSR = {};
        var ssrlstobj = {};
        var meals = {};
        var seats = {};
        var baggages = {};
        var ssrbackuplst = {
            "ssrlst":$scope.ssrbindData,
            "totseatPrice": $scope.totseat,
            "totbagPrice": $scope.loopTot,
            "totmealPrice": $scope.totMeals,
            "selectedMeal": false,
            "selectedSeat":false,
            "selectedBaggage":false,
            "fngrandtotal":$scope.fngrandtotal,
            "discount":$scope.discount
        };

        var isSlected = false;
        
        if($scope.ssrTotalData)
        {

            for (let keyseg in $scope.ssrbindData.meals) 
            {
                var mealSSRs = [];
                if($scope.ssrbindData.meals[keyseg].mealsData != undefined)
                {
                    $scope.ssrbindData.meals[keyseg].mealsData.forEach(trvldets => 
                    {
                        trvldets.travelerDetails.mealslist.forEach(mlst => {
                            if(mlst.count > 0)
                            {
                                mealSSRs.push(mlst); 
                                isSlected = true;
                                ssrbackuplst.selectedMeal = true;
                            }
                        })
                    });
                    
                    meals[`${keyseg}`] = mealSSRs;
                }
            }

            
            for (let keyseg in $scope.ssrbindData.seats) 
            {
                var seatsSSRs = [];
                if($scope.ssrbindData.seats[keyseg].seatData != undefined)
                {
                    for(var seatvar=0; seatvar<$scope.ssrbindData.seats[keyseg].seatData.length; seatvar++)
                    // $scope.ssrbindData.seats[keyseg].seatData.forEach(trvldets => 
                    {
                        if($scope.ssrbindData.seats[keyseg].seatData[seatvar].travelerDetails.seatsslist[0].selectedSeat != null)
                        {
                            isSlected = true;
                            seatsSSRs.push($scope.ssrbindData.seats[keyseg].seatData[seatvar].travelerDetails.seatsslist[0]);
                            seatsSSRs[0].seatRows=[];
                            // $scope.ssrbindData.seats[keyseg].seatRows = null;
                            ssrbackuplst.selectedSeat = true;
                        }
                    }
                    seats[`${keyseg}`] = seatsSSRs;
                }
            }

            for (let keyseg in $scope.ssrbindData.baggages) 
            {
                var baggageSSRs = [];
                if($scope.ssrbindData.baggages[keyseg].baggageData != undefined)
                {
                    $scope.ssrbindData.baggages[keyseg].baggageData.forEach(trvldets => {
                            // if (trvldets.travellerBaggages.selectedBaggage.price > 0) {
                        isSlected = true;
                        baggageSSRs.push(trvldets.travellerBaggages.selectedBaggage);
                        ssrbackuplst.selectedBaggage = true;
                        // }
                    });
                    baggages[`${keyseg}`] = baggageSSRs;
                }
            }

            if(isSlected)
            {
                sessionStorage.setItem("Selectedssrlist",JSON.stringify(ssrbackuplst));
                ssrlstobj = {
                    meals : meals,
                    seats : seats,
                    baggages: baggages
                };
            }

            if(ssrlstobj != null){
                repriceCall(ssrlstobj);
            }else{
                // $scope.validateservicedatapament = true;
                // $scope.validateservicedata = false;
                $scope.validatedata = false;
                $scope.validateguestdata = true;
                $("#serbill").addClass("active");
        $scope.ssrFunction=false;
        $scope.fltbking = true;
            }

        }
        else
        {
        // $scope.validateservicedatapament = true;
        // $scope.validateservicedata = false;
        $scope.validatedata = false;
        $scope.validateguestdata = true;
        $("#serbill").addClass("active");
        $scope.ssrFunction=false;
        $scope.fltbking = true;
    }
    $window.scrollTo(0, 0);
    }



    // gettingssr();

    $scope.guestuser = {};
    var displayName = {};
    var loginResp = {};
    $scope.userBooking = {}
    $scope.gsubmitted = false;
   


  
 

    $scope.allTravellerData = JSON.parse(localStorage.getItem('loginTravellerData'));
    var adultCount = $scope.travellersListByRoom[0].adultTravellersList.length;
    var chaildCount = $scope.travellersListByRoom[0].childTravellersList.length;
    var infantCount = $scope.travellersListByRoom[0].infantTravellersList.length;
    $scope.selectTraveldata = JSON.parse(sessionStorage.getItem('selectTraveldata'))
    if(  $scope.selectTraveldata!=null){
    $scope.totaladults=[];
    $scope.totalchilds=[];
    $scope.totalinfant=[];
    if( $scope.selectTraveldata!=null){
        for(a=0; a< $scope.selectTraveldata.length; a++){
            if( $scope.selectTraveldata[a].age >= 12){
              $scope.totaladults.push($scope.selectTraveldata[a]);
            }else if( $scope.selectTraveldata[a].age >= 2 &&  $scope.selectTraveldata[a].age < 12){
                $scope.totalchilds.push($scope.selectTraveldata[a]);
            }else{
                if( $scope.selectTravellerData[a].age <= 2){
                    $scope.totalinfant.push($scope.selectTraveldata[a]);  
            }
                    }
    }
}
function capitalize(string) {
    return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
}
$scope.travelobject=function(a,singlePsgr){
    $scope.passenger={};
    $scope.passenger.salutation = capitalize(singlePsgr.salutation);
    $scope.passenger.fname =  singlePsgr.firstName
    $scope.passenger.lname =  singlePsgr.lastName
    $scope.passenger.customerFfDtls= singlePsgr.customerFfDtls;
    $scope.passenger.customerPreferences= singlePsgr.customerPreferences
    var unwantedCharacter = "0";
    if(singlePsgr.dateOfBirth != undefined)
    {
        
    // var dob=singlePsgr.dateOfBirth.split("-");
    // var birthdate=dob[2];
    // if( birthdate.charAt( 0 ) === unwantedCharacter ){
    //     birthdate = birthdate.slice( 1 );

    // }
    // var birthmonth=dob[1];
    
   
    // if( birthmonth.charAt( 0 ) === unwantedCharacter ){
    //     birthmonth = birthmonth.slice( 1 );

    // }

    $scope.passenger.birthdate= (new Date(singlePsgr.dateOfBirth).getDate()+1).toString();

    $scope.passenger.birthmonth=  (new Date(singlePsgr.dateOfBirth).getMonth()).toString();

    $scope.passenger.birthyear=  (new Date(singlePsgr.dateOfBirth).getFullYear()).toString();
    }
    var  mobcntrycodeObj=[];
    if( singlePsgr.countryCode === '' ||  singlePsgr.countryCode === null ||  singlePsgr.countryCode === undefined){
        $scope.passenger.mobcntrycodetrobj =  singlePsgr.countryCode;
    }else{
    for(var c=0; c<$scope.countryList.length; c++){
        if($scope.countryList[c].name== singlePsgr.countryCode){
             mobcntrycodeObj.push($scope.countryList[c]);
        }
    }
    $scope.passenger.mobcntrycodetrobj = mobcntrycodeObj[0];
}
$scope.passenger.mobile =  singlePsgr.phoneNumber
$scope.passenger.email =  singlePsgr.emailId
$scope.passenger.gender =  singlePsgr.gender
$scope.passenger.nationalitycode =  singlePsgr.nationality

$scope.passenger.nationalityname=singlePsgr.nationality
$scope.passenger.passportNo = singlePsgr.passportNo
$scope.passenger.customerId = singlePsgr.customerId


if( singlePsgr.passportExpiryDate!=null){
    // var passExp= singlePsgr.passportExpiryDate.split("-");
    // var expirydate=passExp[2];
    // if( expirydate.charAt( 0 ) === unwantedCharacter ){
    //     expirydate = expirydate.slice( 1 );

    // }
    // var expMonth = passExp[1];
   
    // if( expMonth.charAt( 0 ) === unwantedCharacter ){
    //     expMonth = expMonth.slice( 1 );

    // }
    $scope.passenger.expirydate=(new Date(singlePsgr.passportExpiryDate).getDate()+1).toString();
    $scope.passenger.expirymonth=(new Date(singlePsgr.passportExpiryDate).getMonth()).toString();;
    $scope.passenger.expiryyear=(new Date(singlePsgr.passportExpiryDate).getFullYear()).toString();
 
}
$scope.passenger.nationalityId =singlePsgr.saudiNationalityId
$scope.passenger.passIssueGovt =singlePsgr.nationality
}
if(adultCount!=0){
    for(var a=0; a< $scope.travellersListByRoom[0].adultTravellersList.length; a++){

     if($scope.totaladults[a] !=null || $scope.totaladults[a] !=undefined){
    $scope.travelobject(a,$scope.totaladults[a]);
}
     
     
        $scope.travellersListByRoom[0].adultTravellersList[a].personaldata=$scope.passenger
    }
}

if(chaildCount!=0){
    for(var c=0; c< $scope.travellersListByRoom[0].childTravellersList.length; c++){
        if($scope.totalchilds[c] !=null || $scope.totalchilds[c] !=undefined){
            $scope.travelobject(c,$scope.totalchilds[c]);
        }
     
        $scope.travellersListByRoom[0].childTravellersList[c].personaldata=$scope.passenger
    }
}

if(infantCount!=0){
    for(var i=0; i< $scope.travellersListByRoom[0].infantTravellersList.length; i++){
      if($scope.totalinfant[i] !=null || $scope.totalinfant[i] !=undefined){
            $scope.travelobject(i,$scope.totalinfant[i]);
        }
        $scope.travellersListByRoom[0].infantTravellersList[i].personaldata=$scope.passenger
    }
}
}



    $scope.addTravel = function(checked, singlePsgr, age, indexvalue) {

        $scope.particularindexvalue=indexvalue;
        $scope.checked=checked
        //  $scope.allTravellerData = JSON.parse(sessionStorage.getItem('loginTravellerData'));
        $scope.usersObj = {};
        $scope.usersObj.salutation = singlePsgr.salutation;
        $scope.usersObj.fname = singlePsgr.firstName
        $scope.usersObj.lname = singlePsgr.lastName
        $scope.usersObj.customerFfDtls=singlePsgr.customerFfDtls;
        $scope.usersObj.customerPreferences=singlePsgr.customerPreferences
      
        var unwantedCharacter = "0";
        if(singlePsgr.dateOfBirth != undefined)
        {
        // var dob=singlePsgr.dateOfBirth.split("-");
        // var birthdate=dob[2];
        // if( birthdate.charAt( 0 ) === unwantedCharacter ){
        //     birthdate = birthdate.slice( 1 );

        // }
        // var birthmonth=dob[1];
        
       
        // if( birthmonth.charAt( 0 ) === unwantedCharacter ){
        //     birthmonth = birthmonth.slice( 1 );

        // }
        $scope.usersObj.birthdate= (new Date(singlePsgr.dateOfBirth).getDate()+1).toString();

        $scope.usersObj.birthmonth=(new Date(singlePsgr.dateOfBirth).getMonth()).toString();

        $scope.usersObj.birthyear= (new Date(singlePsgr.dateOfBirth).getFullYear()).toString();
       
        }
        var  mobcntrycodeObj=[];
        if(singlePsgr.countryCode === '' || singlePsgr.countryCode === null || singlePsgr.countryCode === undefined){
            $scope.usersObj.mobcntrycodetrobj = singlePsgr.countryCode;
        }else{
        for(var c=0; c<$scope.countryList.length; c++){
            if($scope.countryList[c].name==singlePsgr.countryCode){
                 mobcntrycodeObj.push($scope.countryList[c]);
            }
        }
        $scope.usersObj.mobcntrycodetrobj = mobcntrycodeObj[0];
    }
       
        // $scope.usersObj.mobcntrycode = mobcntrycodeObj[0];
        $scope.usersObj.mobile = singlePsgr.phoneNumber
        $scope.usersObj.email = singlePsgr.emailId
        $scope.usersObj.gender = singlePsgr.gender
        $scope.usersObj.nationalitycode = singlePsgr.nationality
        
        $scope.usersObj.nationalityname = singlePsgr.nationality
        $scope.usersObj.passportNo = singlePsgr.passportNo
        $scope.usersObj.customerId = singlePsgr.customerId
        // $scope.usersObj.city = singlePsgr.passportNo
        // $scope.usersObj.country = singlePsgr.passportNo

        if(singlePsgr.passportExpiryDate!=null){
        // var passExp=singlePsgr.passportExpiryDate.split("-");
        // var expirydate=passExp[2];
        // if( expirydate.charAt( 0 ) === unwantedCharacter ){
        //     expirydate = expirydate.slice( 1 );

        // }
        // var expMonth = passExp[1];
       
        // if( expMonth.charAt( 0 ) === unwantedCharacter ){
        //     expMonth = expMonth.slice( 1 );

        // }
        $scope.usersObj.expirydate=(new Date(singlePsgr.passportExpiryDate).getDate()+1).toString();
        $scope.usersObj.expirymonth=(new Date(singlePsgr.passportExpiryDate).getMonth()).toString();;
        $scope.usersObj.expiryyear=(new Date(singlePsgr.passportExpiryDate).getFullYear()).toString();
       
    }
        $scope.usersObj.nationalityId =singlePsgr.saudiNationalityId
        $scope.usersObj.passIssueGovt =singlePsgr.nationality
            //  var adultCount=$scope.travellersListByRoom[0].adultTravellersList.length;
        if (age >= 12) {
        // if (true) {
            // $scope.usersObj.clsname = 'adultage';
            $scope.usersObj.trvtype = 'ADT';
            if ( $scope.checked) {
                if (adultCount != 0) {
                    $scope.usersObj=$scope.usersObj;
                   
                } else {
                  
                }








            } else {
                if (adultCount != $scope.travellersListByRoom[0].adultTravellersList.length)
                    adultCount++
                    $scope.travellersListByRoom[0].adultTravellersList[$scope.adultIndex].personaldata = "";

            }
        } else if (age >= 2 && age < 12) {
            $scope.usersObj.trvtype = 'CNN';
            if ( $scope.checked) {
                if (chaildCount != 0) {
                    /* chaildCount-- */
                    $scope.childusersObj=$scope.usersObj;
                    // $scope.travellersListByRoom[0].childTravellersList[ $scope.childIndex].personaldata = $scope.usersObj


                  
                } else {
                   
                }








            } else {
                if (chaildCount != $scope.travellersListByRoom[0].childTravellersList.length)
                    chaildCount++
                    $scope.travellersListByRoom[0].childTravellersList[ $scope.childIndex].personaldata = "";

            }


        } else {
            $scope.usersObj.trvtype = 'INF';
            if ( $scope.checked) {
                if (infantCount != 0) {
                    /* chaildCount-- */
                    $scope.infantusersObj=$scope.usersObj;
                    // $scope.travellersListByRoom[0].infantTravellersList[ $scope.infantIndex].personaldata = $scope.usersObj


                    // Lobibox.notify('success', {
                    //     size: 'mini',
                    //     position: 'bottom left',
                    //     msg: $scope.usersObj.fname + " " + $scope.usersObj.lname + " added successfully!",
                    //     delay: 1500
                    // });
                } else {
                    // Lobibox.notify('error', {
                    //     size: 'mini',
                    //     position: 'bottom left',
                    //     msg: "Infant limit Reached!",
                    //     delay: 1500
                    // });
                }








            } else {
                if (infantCount != $scope.travellersListByRoom[0].infantTravellersList.length)
                infantCount++
                    $scope.travellersListByRoom[0].infantTravellersList[$scope.infantIndex].personaldata = "";

            }
        }

    }

    $scope.ConfirmTravellerData=function(){
        if($scope.TravelType=='adult'){
if($scope.TravelType=='adult' && $scope.usersObj!=undefined &&  $scope.checked==true){
    Lobibox.notify('success', {
        size: 'mini',
        position: 'top right',
        msg: $scope.usersObj.fname + " " + $scope.usersObj.lname + " added successfully!",
        delay: 1500
    });
    $scope.modalInstance.dismiss();
    $scope.travellersListByRoom[0].adultTravellersList[$scope.adultIndex].personaldata = $scope.usersObj
    if($scope.usersObj.customerPreferences.length > 0){
    $scope.travellersListByRoom[0].adultTravellersList[$scope.adultIndex].miscdata.mealpref[0].code = $scope.usersObj.customerPreferences[0].custPrefType;
    
    $scope.travellersListByRoom[0].adultTravellersList[$scope.adultIndex].miscdata.seatpref[0].code = $scope.usersObj.customerPreferences[1].custPrefType;

    }

}else{
    // Lobibox.notify('error', {
    //     size: 'mini',
    //     position: 'top right',
    //     msg: "Please select at least one traveller!",
    //     delay: 1500
    // });
    Lobibox.alert('error', {
        msg:  "Please select at least one traveller!",
        callback: function (lobibox, type) {
          if (type === 'ok') {
           
          }
        }
      });
}
        } else if($scope.TravelType=='child'){
            if($scope.TravelType=='child' && $scope.childusersObj!=undefined &&  $scope.checked==true){
                $scope.travellersListByRoom[0].childTravellersList[ $scope.childIndex].personaldata = $scope.childusersObj
            
            
                Lobibox.notify('success', {
                    size: 'mini',
                    position: 'top right',
                    msg: $scope.childusersObj.fname + " " + $scope.childusersObj.lname + " added successfully!",
                    delay: 1500
                });
                $scope.modalInstance.dismiss();
            }else{
                Lobibox.alert('error', {
                    msg:  "Please select at least one traveller!",
                    callback: function (lobibox, type) {
                      if (type === 'ok') {
                       
                      }
                    }
                  });
                // Lobibox.notify('error', {
                //     size: 'mini',
                //     position: 'top right',
                //     msg: "Please select at least one traveller!",
                //     delay: 1500
                // });
            }
        }else{
            if($scope.TravelType=='infant' && $scope.infantusersObj!=undefined &&  $scope.checked=='true'){
                $scope.travellersListByRoom[0].infantTravellersList[ $scope.infantIndex].personaldata = $scope.infantusersObj
            
            
                Lobibox.notify('success', {
                    size: 'mini',
                    position: 'top right',
                    msg: $scope.infantusersObj.fname + " " + $scope.infantusersObj.lname + " added successfully!",
                    delay: 1500
                });
                $scope.modalInstance.dismiss();
            }else{
                Lobibox.alert('error', {
                    msg:  "Please select at least one traveller!",
                    callback: function (lobibox, type) {
                      if (type === 'ok') {
                       
                      }
                    }
                  });
                // Lobibox.notify('error', {
                //     size: 'mini',
                //     position: 'top right',
                //     msg: "Please select at least one traveller!",
                //     delay: 1500
                // });
            }
        }

    }

            
    $scope.infantsearchTrvlr = function(infantindexvalue,travelType) {
        $scope.modalInstance=$uibModal.open({
            animation: $scope.animationEnabled,
            templateUrl: 'myTestModal.tmpl.html',
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            backdrop: true,
            scope:$scope,
            size :'lg',
            appendTo: parentElem,
            resolve: {
                items: function() {
                    return $scope.items
                }
            }                   });
        $scope.infantusersObj=undefined;
    
        $scope.tokenresult = JSON.parse(sessionStorage.getItem('authToken'));

        var tokenname = $scope.tokenresult.data;
        var headersOptions = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + $scope.tokenresult
        }
        $http.get(ServerService.authPath + 'user/retrieve', {
            headers: headersOptions
        }).then(function successCallback(response) {
            $scope.trvSearchdata = response.data.data;

            $scope.trvSearchRes = $scope.trvSearchdata.companyCustomers;
            // sessionStorage.setItem('authentication', JSON.stringify(response));
            // $scope.trvSearchRes.push()
            sessionStorage.setItem('loginTravellerData', JSON.stringify($scope.trvSearchRes));
         
            $scope.allTravellerData=[];
            $scope.infantIndex=infantindexvalue;
            $scope.TravelType=travelType;
                     $scope.allTravellerDataa = JSON.parse(sessionStorage.getItem('loginTravellerData'));
                     for(var t=0;  t<$scope.allTravellerDataa.length; t++){
    if( $scope.allTravellerDataa[t].age >= 0 && $scope.allTravellerDataa[t].age < 2){
        $scope.allTravellerData.push($scope.allTravellerDataa[t])
    }
                     }
                     if($scope.allTravellerData.length==0) {
                        $scope.childText=" Traveller Data matching the age criteria given is not available."
                    }
        }, function errorCallback(response) {
            if(response.status == 403){
                var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
                status.then(function(greeting) {
                    $scope.infantsearchTrvlr();
                });
            }
          });
    
                };


                $scope.isdependents = false;

                $scope.dependent=function(cheked){
                $scope.dependents=cheked
                }

              $scope.trverlindx = '';
              $scope.selectingTrvlr=function(indexvalue,travelType)
              {
                $scope.initSrchObj();
                $scope.modalInstance=$uibModal.open({
                    templateUrl: 'myTestModal.tmpl.html',
                    backdrop: 'static',
                    keyboard: true,
                    size: 'xl',
                    scope:$scope
                });
                $scope.phNoREQ = false;
                $scope.ccREQ = false;
                $scope.searchTrvlr(indexvalue,travelType);
              }

              $scope.phNoREQ = false;
              $scope.ccREQ = false;

              $scope.contactNochange = function()
              {
                const countryCode = $scope.trvellersrch.mobcntrycodetrobj;
                const phonenumber = $scope.trvellersrch.mobile;
            
                if ((countryCode != '' && countryCode != null && phonenumber != '' && phonenumber != null) || ((countryCode == '' || countryCode == null) && (phonenumber == '' || phonenumber == null)))
                {
                    $scope.phNoREQ = false;
                    $scope.ccREQ = false;
                }
                else
                {
                    if (countryCode != '' && countryCode != null)
                    {
                        $scope.phNoREQ = true;
                        // $scope.ccREQ = true;
                    }
                    else
                    {
                    if(phonenumber == '' && phonenumber == null)
                    {
                        $scope.phNoREQ = false;
                        $scope.ccREQ = false;
                    }
                    else
                    {
                        // $scope.phNoREQ = true;
                        $scope.ccREQ = true;
                    }
                    }
                }
            
              }
            
           
              $scope.searchTrvlr=function(indexvalue,travelType){
                $scope.companyId=sessionStorage.companyId
                if($scope.companyId =='' ||   $scope.companyId ==undefined){
                    $scope.companyDetailsTmp = JSON.parse(sessionStorage.getItem('companyDetails'));
                    $scope.companyId = $scope.companyDetailsTmp.companyId;
                }
               
                $scope.isdependents = $scope.dependents;
              
                $scope.artloadingcancellation = true;
                var templist =[];
                if($scope.trvellersrch.paxName != "")
                {
                    var innerobj = {
                        "field": "firstName",
                        "operator": "contains",
                        "value": $scope.trvellersrch.paxName
                    }
                    templist.push(innerobj);
                }
            
                if($scope.trvellersrch.surName != "")
                {
                    var innerobj = {
                        "field": "lastName",
                        "operator": "contains",
                        "value": $scope.trvellersrch.surName
                    }
                    templist.push(innerobj);
                }
            
                if($scope.trvellersrch.birthdate != null && $scope.trvellersrch.birthmonth != null && $scope.trvellersrch.birthyear != null)
                {
                    var innerobj = {
                        "field": "dateOfBirth",
                        "operator": "eq",
                        "value": $scope.trvellersrch.birthyear + "-" + $scope.trvellersrch.birthmonth + "-" + $scope.trvellersrch.birthdate
                    }
                    templist.push(innerobj);
                }
            
                if($scope.trvellersrch.email != "")
                {
                    var innerobj = {
                        "field": "emailId",
                        "operator": "eq",
                        "value": $scope.trvellersrch.email
                    }
                    templist.push(innerobj);
                }
            
                if($scope.trvellersrch.passportNo != "")
                {
                    var innerobj = {
                        "field": "passportNo",
                        "operator": "eq",
                        "value": $scope.trvellersrch.passportNo
                    }
                    templist.push(innerobj);
                }
            
                if($scope.trvellersrch.mobile != "" )
                {
                    if($scope.trvellersrch.mobcntrycodetrobj !="")
                    {
            
                    }
                }
                if($scope.trvellersrch.mobile != "" && $scope.trvellersrch.mobcntrycodetrobj !="")
                {
                    var innerobj = {
                        "field": "countryCode",
                        "operator": "eq",
                        "value": $scope.trvellersrch.mobcntrycodetrobj.name
                    }
                    templist.push(innerobj);
                }
            
                if($scope.trvellersrch.mobile != "" && $scope.trvellersrch.mobcntrycodetrobj !="")
                {
                    var innerobj = {
                        "field": "phoneNumber",
                        "operator": "eq",
                        "value": $scope.trvellersrch.mobile
                    }
                    templist.push(innerobj);
                }
                    var filterReq = {
                        "page": null,
                        "getAllResults": true,            
                        "filter": {
                          "logic": "and",
                          "filters": templist
                       },
                      
                        "sort": [
                          {
                            "field": "createTime",
                            "dir": "desc"
                          }
                      
                        ]
                      
                      }
            
                    
                    $scope.usersObj=undefined;
                    $scope.artloadingcancellation=true;
                    $scope.tokenresult = JSON.parse(sessionStorage.getItem('authToken'));
                                var headersOptions = {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + $scope.tokenresult
                    }
             
                    $http.post(ServerService.listPath + 'search/corporate-customers',filterReq, {
                        headers: headersOptions
                    }).then(function successCallback(response) {
                            $scope.trvSearchdata = response.data.data.data;
                            $scope.artloadingddd=false;
                            $scope.artloadingcancellation = false;
                    
                        
                            sessionStorage.setItem('loginTravellerData', JSON.stringify($scope.trvSearchdata));
                            $scope.allTravellerData=[];
                            $scope.TravelType=travelType;
                            $scope.trverlindx = indexvalue;
                            $scope.allTravellerDataa = JSON.parse(sessionStorage.getItem('loginTravellerData'));
                            for(var t=0;  t<$scope.allTravellerDataa.length; t++)
                        {
                            if($scope.TravelType == 'adult')
                            {
                            if( $scope.allTravellerDataa[t].age>= 12){
                                $scope.allTravellerData.push($scope.allTravellerDataa[t])
                            }
                            $scope.adultIndex=indexvalue;
                            $scope.trvellersrchyears = $scope.ayears;
                            $scope.trvellersrchday = $scope.dobdates;
                            $scope.trvellersrchmonth = $scope.dobmonths;    
                            }
                        
                            if($scope.TravelType == 'infant')
                            {
                            if( $scope.allTravellerDataa[t].age >= 0 && $scope.allTravellerDataa[t].age < 2){
                                $scope.allTravellerData.push($scope.allTravellerDataa[t])
                            }
                            $scope.infantIndex=indexvalue;
                            $scope.trvellersrchyears = $scope.iyears
                            $scope.trvellersrchday = $scope.idobdates;
                            $scope.trvellersrchmonth = $scope.idobmonths;  
                            }
                        
                            if($scope.TravelType == 'child')
                            {
                            if($scope.allTravellerDataa[t].age >= 2 && $scope.allTravellerDataa[t].age < 12){
                                $scope.allTravellerData.push($scope.allTravellerDataa[t])
                            }
                            $scope.childIndex=indexvalue;
                            $scope.trvellersrchyears = $scope.cyears;
                            $scope.trvellersrchday = $scope.cdobdates;
                            $scope.trvellersrchmonth = $scope.cdobmonths;     
                            }
                        }
                        if($scope.allTravellerData.length==0) {
                        $scope.childText=" Traveller Data matching the age criteria given is not available."
                    }
                                $scope.trvellersrchyears = $scope.ayears;
                                $scope.trvellersrchday = $scope.dobdates;
                                $scope.trvellersrchmonth = $scope.dobmonths;    
                                $scope.countryList = JSON.parse(sessionStorage.getItem('countryList'));
                             
                        
                     
                        });
                    
                }

                $scope.custmerSearchFun = function(){
                    console.log( $scope.search);
                }

                $scope.childsearchTrvlr = function(childindexvalue,travelType) {
                    $scope.modalInstance=$uibModal.open({
                        templateUrl: 'myTestModal.tmpl.html',
                        backdrop: 'static',
                        keyboard: true,
                        scope:$scope
                    });
                    $scope.childusersObj=undefined;
                    $scope.tokenresult = JSON.parse(sessionStorage.getItem('authToken'));
                        var headersOptions = {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' +  $scope.tokenresult
                    }
                    $http.get(ServerService.authPath + 'user/retrieve', {
                        headers: headersOptions
                    }).then(function successCallback(response) {
                        $scope.trvSearchdata = response.data.data;
            
                        $scope.trvSearchRes = $scope.trvSearchdata.companyCustomers;
                        // sessionStorage.setItem('authentication', JSON.stringify(response));
                        // $scope.trvSearchRes.push()
                        sessionStorage.setItem('loginTravellerData', JSON.stringify($scope.trvSearchRes));
                        $scope.allTravellerData=[];
                    $scope.childIndex=childindexvalue;
                    $scope.TravelType=travelType;
                             $scope.allTravellerDataa = JSON.parse(sessionStorage.getItem('loginTravellerData'));
                             for(var t=0;  t<$scope.allTravellerDataa.length; t++){
             if($scope.allTravellerDataa[t].age >= 2 && $scope.allTravellerDataa[t].age < 12){
                $scope.allTravellerData.push($scope.allTravellerDataa[t])
            }
                             }
                            if($scope.allTravellerData.length==0) {
                                $scope.childText="Traveller Data matching the age criteria given is not available."
                            }
                    }, function errorCallback(response) {
                        if(response.status == 403){
                            var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
                            status.then(function(greeting) {
                                $scope.childsearchTrvlr();
                            });
                        }
                      });
                  
                }
            
                
                $scope.close=function(){
                    $scope.modalInstance.dismiss();//$scope.modalInstance.close() also works I think
                };


    /* AgencyTermsOfBusiness End*/
    /* signIn function */
    $scope.showSignInText = "Sign in to book faster";
    $scope.showSignIn = false;
    $scope.signIn = function() {
            $scope.showSignIn = $scope.showSignIn ? false : true;

            var buttonText = "Sign in to book faster";
            if ($scope.showSignIn == true) {
                var buttonText = "OR Continue As Guest";
                $("input#password").attr("required", "true");
                $("input#contact").removeAttr("required");
            } else {
                var buttonText = "Sign in to book faster";
                $("input#password").removeAttr("required");
                $("input#contact").attr("required", "true");
            }

            $scope.showSignInText = buttonText;

        }
      
    $scope.paymentDetail = {

        firstName: "",
        lastName: "",
        address: "",
        emailAddress: "",
        countryCode: "",


        city: ""
    }

    /* proceedCustomDetails function */
    $scope.loading = false;
    $scope.submitted = false;
    $scope.disabled = false;
    /* $scope.paymentDetail = {} */
    $scope.selectDoc = {}
    $scope.bookingReq = {}
    $scope.specialServiceRequest = {}
    $scope.selectDoc.dockNo = null;
    var expireDate = {}
    var traveller = {};
    var childTraveller = {};
    var infantTraveller = {};

    $scope.travelerscount = [];
    $scope.baggagedata = false

    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };
    $scope.backdetail=function() {
        $scope.loggedvalue = true;
         $("#trvl").removeClass("active");
        $scope.backtodetail = false;
        $scope.backtotraveler = false;
        $scope.validateguestdata = false;
        // $scope.validateservicedatapament = true;
        $scope.validatedata = false;
        // $scope.validateservicedata = true;
        // $("#trvl").addClass("active")
        // $("#bill").removeClass("active");
        // $window.scrollTo(0, 0);

    }
    $scope.backtraveler=function() {
        // $scope.timeload();
        
        $("#bill").removeClass("active");
        $("#serv").removeClass("active");
        $("#serbill").removeClass("active");
        
        
        $scope.backtodetail = true;
        $scope.backtotraveler = false;
        $scope.validateservicedatapament = false;
       
        $scope.validatedata = true;
        $scope.validateservicedata=false
        
    }
    $scope.backttoService=function() {
        // $scope.timeload();
        
        $("#bill").removeClass("active");
      
        $("#serbill").removeClass("active");
        $("#serv").addClass("active")
        $scope.billing=false
       
        $scope.backtotraveler = true;
        $scope.validateservicedatapament = false;
       
        $scope.validatedata = false;
        $scope.validateservicedata=true
        
    }
    $scope.pamentToTraveller = function() {
        if ($("#trvl").hasClass("active")) 
        {
            $scope.timeload();
            $scope.backtodetail = true;
            $scope.validateguestdata = true;
            $scope.validateservicedatapament = false;
            $scope.validatedata = true;
            $scope.validateservicedata = false;
            $("#trvl").addClass("active")
            $("#bill").removeClass("active");
            $("#serbill").removeClass("active");

            $("#serv").removeClass("active");
            
            $window.scrollTo(0, 0);
        }
    }
    $scope.continueToTraveller = function() {
        $scope.timeload();
        $scope.backtodetail = true;
        $scope.validateguestdata = true;
        $scope.validateservicedatapament = false;
        $scope.validatedata = true;
        $scope.validateservicedata = false;
        $("#trvl").addClass("active")
        $("#bill").removeClass("active");
        $("#serbill").removeClass("active");

        $("#serv").removeClass("active");
        
        $window.scrollTo(0, 0);
    
}
    $scope.travellerToLoginPage = function() {
        $scope.timeload();
        $scope.backtodetail = false;
        $scope.backtotraveler = false;
        
        $scope.validatedata = false;
        $("#trvl").removeClass("active");
        $scope.validateguestdata = false;
        $scope.validateservicedatapament = false;
        $scope.validateservicedata = false;
        $window.scrollTo(0, 0);
    }
    $scope.loginAndContinue = function() {
        $scope.timeload();
        $("#trvl").addClass("active");
        $scope.validatedata = true;
        $scope.validateguestdata = true;
        $window.scrollTo(0, 0);
    }

    $scope.seatToTraveller = function() {
        $("#trvl").addClass("active");
        $("#serv").removeClass("active");
        $scope.validatedata = true;
        $scope.validateservicedata = false;
        $scope.validateservicedatapament = false;
        $window.scrollTo(0, 0);
        $scope.validateguestdata = true;
$scope.backtotraveler=true;


    }

    getTotSeat = function()
    {
        $scope.totseat = 0;
        for(let seatseg in $scope.ssrbindData.seats)
        {
            if($scope.ssrbindData.seats[seatseg].seatData != undefined)
            {
                for(var ml=0;ml<$scope.ssrbindData.seats[seatseg].seatData.length;ml++)
                {
                    $scope.isseatavailable = true;
                $scope.totseat = Number($scope.ssrbindData.seats[seatseg].seatData[ml].travelerDetails.price) + $scope.totseat;
                }
            }
        }
    }

    seatcheckfun = function(checkseat){
   
        $scope.seatRowDetails.seatRows.forEach(singleSeat => {
            singleSeat.seating[0].seatColumn.forEach(Seat => {
                $('#'+Seat.seatNumber).prop("checked", false);
                $(this).removeClass('active');
            });
            singleSeat.seating[1].seatColumn.forEach(Seat => {
                $('#'+Seat.seatNumber).prop("checked", false);
                $(this).addClass('active');
            });
        });
        
        if(checkseat != undefined)
        {
            $timeout(function(){
                $('#'+checkseat).prop("checked", true);
            },10)
        }
       
    }
    
    $scope.onseatchange = function(seat,seatNumber, segkey,trvlindx) 
    {
        $scope.trlseatlist = seat.travelerDetails.seatsslist[0];
        $scope.travellerPassengerkey = seat.travelerDetails.seatsslist[0].passengerKey;
        $scope.seatRowDetails = $scope.ssrbindData.seats[segkey].seatData[trvlindx].travelerDetails.seatsslist[0];
        $scope.seatsegment = $scope.ssrbindData.seats[segkey].seatData[trvlindx].key;
        $scope.seatTrvlrIndx = trvlindx;
        $scope.parentIndx = segkey;
        seatcheckfun(seatNumber);
    
    }
    $scope.ssrDetailsBind = function() {
        $scope.isbagavailable = false;
        $scope.isseatavailable = false;
        $scope.ismealavailable = false;
        $scope.fltbking = false;
        $scope.ssrFunction=false;
        $scope.seatFunction=true;
        $scope.ssrTotalData = JSON.parse(sessionStorage.getItem('ssrtotalData'));
        // $scope.ssrTotalData = baseUrlService.ssrTempData[0];
        $scope.ssrbindData = $scope.ssrTotalData.data.data;
        $scope.routeInfoseg = $scope.ssrbindData.routes;
        //Mapping Traveler details from flightSearchResults Json using ""pasangerkeylist[pskl].travellerRefNo""
    
        var pasangerkeylist = $scope.flightBook.selectedResult.flightSearchResults[0].fareslst;
        var psKeyList = [];
        for (var pskl = 0; pskl < pasangerkeylist.length; pskl++) {
            for (var keyl = 0; keyl < pasangerkeylist[pskl].travellerRefNo.length; keyl++) {
                if(pasangerkeylist[pskl].passengerType == 'ADT'){
                    psKeyList.push(pasangerkeylist[pskl].travellerRefNo[keyl]);
                }
                if(pasangerkeylist[pskl].passengerType == 'CNN'){
                    psKeyList.push(pasangerkeylist[pskl].travellerRefNo[keyl]);
                }
            }
        }
        for (var t = 0; t < $scope.travelerstotallcount.length; t++) {
        if ($scope.travelerstotallcount[t].personaldata.trvtype != 'INF') {
                $scope.travelerstotallcount[t].travellerRefRPH = ($scope.travelerstotallcount[t].travellerRefRPH || '');
                $scope.travelerstotallcount[t].travellerRefRPH = psKeyList[t].rph;
            }
        } 
          
        $scope.loopTot = 0
        $scope.selectedBagLst = JSON.parse(sessionStorage.getItem('Selectedssrlist'));
        if($scope.selectedLst != undefined)
        {
            $scope.ssrbindData = $scope.selectedLst.ssrlst;
        }
    
        if($scope.selectedBagLst != undefined && $scope.selectedBagLst.selectedBaggage)
        {
            // $scope.baggagedataList = $scope.selectedBagLst.bagRQ;
            $scope.loopTot =$scope.selectedBagLst.totbagPrice;
            $scope.bagChange();
            $scope.fngrandtotal = $scope.selectedBagLst.fngrandtotal;
            $scope.discount = $scope.selectedBagLst.discount;
    
        }
        else{
            $scope.baggageData = [];
            var totTravel = 0;
            for(let bag in $scope.ssrbindData.baggages)
            {
                if($scope.ssrbindData.baggages[bag].baggages.length>0)
                {
                    $scope.baggageData = [];
                    for (var t = 0; t < $scope.travelerstotallcount.length; t++) 
                    {
                        if ($scope.travelerstotallcount[t].personaldata.trvtype != 'INF') 
                        {
                            $scope.baggageData[t] = {};
                            $scope.baggageData[t].travellerBaggages = {};
                            var bagage = $scope.ssrbindData.baggages[bag].baggages;
    
                            $scope.baggageData[t].profileid = $scope.travelerstotallcount[t].profileid;
                            $scope.baggageData[t].fname = $scope.travelerstotallcount[t].personaldata.fname;
                            $scope.baggageData[t].lname = $scope.travelerstotallcount[t].personaldata.lname;
                            $scope.baggageData[t].passengerType = $scope.travelerstotallcount[t].personaldata.trvtype;
                            $scope.baggageData[t].travelseg = bag;
    
                            for(var bg=0; bg<bagage.length; bg++)
                            {
                                if($scope.travelerstotallcount[t].travellerRefRPH == bagage[bg].passengerKey)
                                {
                                    $scope.baggageData[t].travellerBaggages.baggagelist = ($scope.baggageData[t].travellerBaggages.baggagelist || []);
                                    $scope.baggageData[t].travellerBaggages.baggagelist.push(bagage[bg]);
                                    $scope.isbagavailable = true;
                                    
                                        if(bagage[bg].price == '0')
                                        {
                                            $scope.baggageData[t].travellerBaggages.selectedBaggage = bagage[bg];
                                        }
                                }
                            }
                            $scope.ssrbindData.baggages[bag].baggageData = $scope.baggageData;
    
                        }
                    }
                }
            }
        }
        console.log($scope.baggageData);
        $scope.selectedMealLst = JSON.parse(sessionStorage.getItem('Selectedssrlist'));
    
        if($scope.selectedMealLst != undefined && $scope.selectedMealLst.selectedMeal)
        {
            // $scope.mealsselectedlist = $scope.selectedMealLst.mealRQ;
            $scope.totMeals =$scope.selectedMealLst.totmealPrice;
            getTotMeals();
            $scope.fngrandtotal = $scope.selectedMealLst.fngrandtotal;
            $scope.discount = $scope.selectedMealLst.discount;
    
        }
        else
        {
            $scope.mealsData = [];
    
            for(let meal in $scope.ssrbindData.meals)
            {
                if($scope.ssrbindData.meals[meal].meals.length>0)
                {
                    $scope.mealsData = [];
                    for (var t = 0; t < $scope.travelerstotallcount.length; t++) 
                    {
                        if ($scope.travelerstotallcount[t].personaldata.trvtype != 'INF') 
                        {
                            $scope.mealsData[t] = {};
                            $scope.mealsData[t].travelerDetails ={};
                            var meallist = $scope.ssrbindData.meals[meal].meals;
    
                            $scope.mealsData[t].key = meal;
                            // $scope.mealsData[t].mealmultiple = $scope.totalboundmealsdata[t].mealmultiple;
                            $scope.mealsData[t].travelerDetails.fname = $scope.travelerstotallcount[t].personaldata.fname;
                            $scope.mealsData[t].travelerDetails.lname = $scope.travelerstotallcount[t].personaldata.lname;
                            $scope.mealsData[t].travelerDetails.price = 0;
                            $scope.mealsData[t].travelerDetails.quantity = 0;
                            $scope.mealsData[t].travelerDetails.prf = $scope.outsegpref;
    
                            for(var bg=0; bg<meallist.length; bg++)
                            {
                                if ($scope.travelerstotallcount[t].travellerRefRPH == meallist[bg].passengerKey) {
                                    // $scope.totalboundmealsdata[b].boundlist[bt].paxIndex = t;
                                    var ssrlist = meallist[bg].ssrCode;
                                    $scope.ssrbindData.meals[meal].meals[bg].ssrData = $scope.ssrbindData.meals[meal].mealCodesMap[ssrlist];
                                    $scope.mealsData[t].travelerDetails.mealslist = ($scope.mealsData[t].travelerDetails.mealslist || []);
                                    $scope.mealsData[t].travelerDetails.mealslist.push($scope.ssrbindData.meals[meal].meals[bg]);
                                    $scope.ismealavailable = true;
                                $scope.mealsData[t].mealmultiple = $scope.ssrbindData.meals[meal].multiMealsEnabled;

                                }
                            }
    
                            $scope.ssrbindData.meals[meal].mealsData = $scope.mealsData;
                        }
                    }
                }
            }
        }
    
        $scope.SelectedSeatLst = JSON.parse(sessionStorage.getItem('Selectedssrlist'));
    
        if($scope.SelectedSeatLst !=undefined && $scope.SelectedSeatLst.selectedSeat)
        {
    
            $scope.seats = $scope.SelectedSeatLst.seatRQ;
            // $scope.seatRowDetails = [];
            var firstsegkey = Object.keys($scope.ssrbindData.seats)[0];
            // $scope.seatRowDetails = $scope.ssrbindData.seats[firstsegkey].seatData[0].travelerDetails.seatsslist[0];
            $scope.totseat =$scope.SelectedSeatLst.totseatPrice;
            getTotSeat();       
            $scope.fngrandtotal = $scope.SelectedSeatLst.fngrandtotal;
            $scope.discount = $scope.SelectedSeatLst.discount;
    
            var seatcounts = 0;
            for(let seatlst in $scope.ssrbindData.seats)
            {
                if($scope.ssrbindData.seats[seatlst].seats.length>0)
                {
                    for(var bg=0; bg<$scope.ssrbindData.seats[seatlst].seatData.length; bg++)
                    {
                        seatcounts++;
                        if(seatcounts == 1)
                        {
                            $scope.onseatchange($scope.ssrbindData.seats[seatlst].seatData[bg],$scope.ssrbindData.seats[seatlst].seatData[bg].travelerDetails.seatNumber, seatlst,bg);
                            $timeout(function(){
                                $('#seatrdio_'+seatlst+'_0').prop("checked", true);
                            },10);
                        }
                    }
                }
            }
    
        }
        else
        {
            // $('#'+Seat.seatNumber).prop("checked", false);
            
            $scope.seatData = [];
            var seatcount = 0;
            for(let seatlst in $scope.ssrbindData.seats)
            {
                if($scope.ssrbindData.seats[seatlst].seats.length>0)
                {
                    $scope.seatData = [];
                    for (var t = 0; t < $scope.travelerstotallcount.length; t++) 
                    {
                        if ($scope.travelerstotallcount[t].personaldata.trvtype != 'INF') 
                        {
                            seatcount++;
                            $scope.seatData[t] = {};
                            $scope.seatData[t].travelerDetails = {};
                            var seatlist = $scope.ssrbindData.seats[seatlst].seats;
    
                            $scope.seatData[t].key = seatlst;
                            $scope.seatData[t].travelerDetails.fname = $scope.travelerstotallcount[t].personaldata.fname;
                            $scope.seatData[t].travelerDetails.lname = $scope.travelerstotallcount[t].personaldata.lname;
                            $scope.seatData[t].travelerDetails.price = 0;
                            $scope.seatData[t].travelerDetails.selectedseat1 = 0;
    
                            for(var bg=0; bg<seatlist.length; bg++)
                            {
                                if ($scope.travelerstotallcount[t].travellerRefRPH == seatlist[bg].passengerKey) 
                                {
                                    // var ssrlst = seatlist[bg].ssrCode;
                                    $scope.ssrbindData.seats[seatlst].seats[bg].seatRows = $scope.ssrbindData.seats[seatlst].seatRows;
                                    $scope.isseatavailable = true;
                                    $scope.seatData[t].travelerDetails.seatsslist = ($scope.seatData[t].travelerDetails.seatsslist || []);
                                    $scope.seatData[t].travelerDetails.seatsslist.push($scope.ssrbindData.seats[seatlst].seats[bg]);
                                }
                            }
                        }
    
                        $scope.ssrbindData.seats[seatlst].seatData = $scope.seatData;
                        if(seatcount == 1)
                        {
                            $scope.onseatchange($scope.ssrbindData.seats[seatlst].seatData[t],$scope.ssrbindData.seats[seatlst].seatData[t].travelerDetails.seatNumber, seatlst,t);
                            $timeout(function(){
                                $('#seatrdio_'+seatlst+'_0').prop("checked", true);
                            },10);
                        }
                    }
                }
            }
        }
    }
    

    $scope.seatsegment = "";
    $scope.trlseatlist = {};


    $scope.selectedSSRBean = [];
    $scope.seats = [];
 
    $scope.onhoverSeat = function(seatData, trldtls,event) {
        $scope.reprice= $scope.flightBookSession;
         var provider=$scope.reprice.data.selectedResult.flightSearchResults[0].provider;
         if(provider=='JZR')
         {
            var passengerKey=$scope.travellerPassengerkey  
           
            if(passengerKey==trldtls.passengerKey){
                $scope.seatToltip=trldtls.groupWisePrice[seatData.groupId].priceAmount;
            }
         }
         else
         {
            $scope.seatToltip = seatData.seatPrice;
         }
    }

    $scope.onselectSeat = function(seatData, trldtls,event) 
    {
        keepGoing = true;
        var tmpseatlist = $scope.ssrbindData.seats[$scope.parentIndx].seatData
        for(var stele=0; stele<tmpseatlist.length;stele++)
        {
            if(tmpseatlist[stele].travelerDetails.seatsslist[0].selectedSeat != null && 
                tmpseatlist[stele].travelerDetails.seatsslist[0].selectedSeat.seatNumber == seatData.seatNumber 
                && stele != $scope.seatTrvlrIndx)
            {

                Lobibox.alert('error',
                {
                    msg: "Seat already selected, Please try another"
                });

                keepGoing = false;
                $timeout(function(){
                    $('#'+seatData.seatNumber).prop("checked", false);
                },20)
                
                $(this).addClass('active');
            }
        }
        if(keepGoing)
        {
            $scope.fngrandtotal = 0;
            $scope.reprice= $scope.flightBookSession;
            var provider=$scope.reprice.data.selectedResult.flightSearchResults[0].provider;
            if(provider=='JZR')
            {
                var passengerKey=$scope.travellerPassengerkey  

                if(passengerKey==trldtls.passengerKey)
                {
                    var seatpricJjr=trldtls.groupWisePrice[seatData.groupId].priceAmount;
                    seatData.seatPrice=seatpricJjr;
                }
            }
            seatcheckfun(seatData.seatNumber);
            // angular.element(event.currentTarget).addClass('ng-not-empty');
            //set price for all travele
            $scope.ssrbindData.seats[$scope.parentIndx].seatData[$scope.seatTrvlrIndx].travelerDetails.seatsslist[0].selectedSeat = seatData;
            $scope.ssrbindData.seats[$scope.parentIndx].seatData[$scope.seatTrvlrIndx].travelerDetails.price = seatData.seatPrice;
            $scope.ssrbindData.seats[$scope.parentIndx].seatData[$scope.seatTrvlrIndx].travelerDetails.seatNumber = ($scope.ssrbindData.seats[$scope.parentIndx].seatData[$scope.seatTrvlrIndx].travelerDetails.seatNumber || 0)
            $scope.ssrbindData.seats[$scope.parentIndx].seatData[$scope.seatTrvlrIndx].travelerDetails.seatNumber = seatData.seatNumber;

            keepGoing = true; 
        
        //get total seat price
        getTotSeat();
        }
    }

    $("#one-tab").removeClass("blinking-inact");
    $("#two-tab").addClass("blinking-inact");
    $("#three-tab").addClass("blinking-inact");

    $scope.ffCall = function(val,index,prodct){
        if(val == 'one'){
            $("."+index + '_' +prodct +'_'+ val).addClass('active');
            $("."+index + '_'+prodct +'_' +val).addClass('show');
            $("."+index +'_'+prodct + "_two").removeClass('active');
            $("."+index +'_'+prodct + "_two").removeClass('show');
            $("."+index +'_'+prodct + "_three").removeClass('active');
            $("."+index +'_'+prodct + "_three").removeClass('show');
            $("#one-tab").removeClass("blinking-inact");
            $("#two-tab").addClass("blinking-inact");
            $("#three-tab").addClass("blinking-inact");
        }else if(val == 'two'){
            $("."+index + '_'+prodct +'_' +val).addClass('active');
            $("."+index + '_'+prodct +'_' +val).addClass('show');
            $("."+index +'_'+prodct + "_one").removeClass('active');
            $("."+index +'_'+prodct + "_one").removeClass('show');
            $("."+index +'_'+prodct + "_three").removeClass('active');
            $("."+index +'_'+prodct + "_three").removeClass('show');
            $("#two-tab").removeClass("blinking-inact");
            $("#one-tab").addClass("blinking-inact");
            $("#three-tab").addClass("blinking-inact");
        }else{
            $("."+index + '_'+prodct +'_' +val).addClass('active');
            $("."+index + '_'+prodct +'_' +val).addClass('show');
            $("."+index +'_'+prodct + "_two").removeClass('active');
            $("."+index +'_'+prodct + "_two").removeClass('show');
            $("."+index  +'_'+prodct + "_one").removeClass('active');
            $("."+index  +'_'+prodct + "_one").removeClass('show');
            $("#three-tab").removeClass("blinking-inact");
            $("#two-tab").addClass("blinking-inact");
            $("#two-tab").addClass("blinking-inact");
        }
    }
    // $scope.seatFunction=false;
    $scope.mealFunction=false;

    $scope.ssrtab =  function(val){
        if(val == 1){
         $scope.mealFunction=false;
         $scope.seatFunction = true
         $scope.ssrFunction=false;
         $scope.fltbking = false;
         $("#mealTabActivate").removeClass('active');
         $("#seatTabActivate").removeClass('active');
         $("#baggageTabActivate").addClass('active');
            
            $("#baggage").addClass('active');
            $("#baggage").addClass('show');
            $("#seat").removeClass('active');
            $("#seat").removeClass('show');
            $("#meal").removeClass('active');
            $("#meal").removeClass('show');
        }else if(val == 2){
            $scope.seatFunction=false;
            $scope.ssrFunction=false;
            $scope.mealFunction=true;
            $scope.fltbking = false;
            $("#mealTabActivate").removeClass('active');
            $("#seatTabActivate").addClass('active');

            $("#baggageTabActivate").removeClass('active');
            
            $("#seat").addClass('active');
            $("#seat").addClass('show');
            $("#baggage").removeClass('active');
            $("#baggage").removeClass('show');
            $("#meal").removeClass('active');
            $("#meal").removeClass('show');
            $("html,body").scrollTop(10);
        }else{
            $scope.ssrFunction=true;
            $scope.seatFunction=false;
            $scope.mealFunction=false;
            $scope.fltbking = false;
            $("#baggageTabActivate").removeClass('active');
            $("#seatTabActivate").removeClass('active');
            $("#mealTabActivate").addClass('active');
            $("#meal").addClass('active');
            $("#meal").addClass('show');
            $("#baggage").removeClass('active');
            $("#baggage").removeClass('show');
            $("#seat").removeClass('active');
            $("#seat").removeClass('show');
            $("html,body").scrollTop(10);
        }
    }

    $scope.totseat = 0;
    $scope.totMeals = 0;
    $scope.docketinvalid = false;
    $scope.servicesDetails = function(valid) {
        // if ($("#trvl").addClass("active")) {
            $scope.backtodetail=false;
            $scope.backtotraveler=true;
        // $scope.seatFunction=true;
            $("#trvl").addClass("active");
        $scope.submitted = true;
        var passengerform=true;
        if (valid && ($scope.docket !="" && $scope.docket != undefined)) {
            $scope.docketinvalid = false;
            if(passengerform==true){
                $scope.totaladultdata=[];
                $scope.totalchailddata=[];
                $scope.totalInfantdata=[];
                $scope.travelList=$scope.travellersListByRoom;
                for(var k=0; k<$scope.travelList.length; k++){
                for(var m=0; m<$scope.travelList[k].adultTravellersList.length; m++){		
                $scope.totaladultdata.push($scope.travelList[k].adultTravellersList[m]);
                }
                for(var n=0; n<$scope.travelList[k].childTravellersList.length; n++){
                $scope.totalchailddata.push($scope.travelList[k].childTravellersList[n]);
                }
                for(var i=0; i<$scope.travelList[k].infantTravellersList.length; i++){
                    $scope.totalInfantdata.push($scope.travelList[k].infantTravellersList[i]);
                    }
                }
                /* $scope.totaladultdata=$scope.travelList[0].adultTravellersList;
                $scope.totalchailddata=$scope.travelList[0].childTravellersList; */
                $scope.totalpassengers = $scope.totaladultdata.concat($scope.totalchailddata).concat($scope.totalInfantdata);
                // $scope.totalpassengers=$scope.totalpassenger.concat($scope.totalInfantdata);
        //@author Raghava start...
        for(var i=0;i<$scope.totalpassengers.length; i++){
            var count=0;
            var outerFirstName=$scope.totalpassengers[i].personaldata.fname;
        var outerPassport=$scope.totalpassengers[i].personaldata.passportNo;
            for(var j=0;j<$scope.totalpassengers.length; j++){
                var innerFirstName=$scope.totalpassengers[j].personaldata.fname;
                var innerPassport=$scope.totalpassengers[j].personaldata.passportNo;
        if(innerFirstName===outerFirstName||innerPassport===outerPassport){
        count++;
        if(count>1){
            Lobibox.alert('error',
            {
                msg: "Guest details are duplicated."
            });
        
            // alert("duplicate Traveller Found.")
        
            return;
        }
        }
        
        
            }
        }
            }
            if ($('#term_con').is(":checked")) {
                $scope.errorMesg = null;
                $("#serv").addClass("active");
                // $scope.paymentForm = {
                // 	// firstName:"",
                // 	// lastName:"",
                // 	// address:"",
                // 	// emailAddress:"",
                // 	// countryCode:"",
                // 	// city:""
                // };
                $scope.searchFlightObj = JSON.parse(sessionStorage.getItem('flightSearchRQ'));
                $scope.validateservicedata = true;
                $scope.validatedata = false;

                $scope.travelerstotallcount = [];

                $scope.travelerscount = $scope.travellersListByRoom[0].adultTravellersList;

                $scope.travelerscount1 = $scope.travellersListByRoom[0].childTravellersList;
                $scope.travelerscount2 = $scope.travellersListByRoom[0].infantTravellersList;


                $scope.travelerscount.forEach(function(selectedItem) {
                    /* 		
                    var d = new Date($scope.flightSearchRQ.strDeptDate);
                                var day = d.getDate();
                                var monthIndex = d.getMonth() + 1;
                                var year = d.getFullYear();

                                $scope.checkin = day + '/' + monthIndex + '/' + year; */
                    $scope.travelerstotallcount.push(selectedItem);
                });
                $scope.travelerscount1.forEach(function(selectedItem) {
                    $scope.travelerstotallcount.push(selectedItem);
                });
                $scope.travelerscount2.forEach(function(selectedItem) {
                    $scope.travelerstotallcount.push(selectedItem);
                });

                $scope.outboundroutemapdata = [];
                $scope.inboundroutemapdata = [];
                $scope.likeSeat = 1;
                $scope.likeMeal = 1;
                $scope.adultSelectedSsrCode = "";
                $scope.adultSelectedSsrAmount = 0;

                $scope.childSelectedSsrCode = "";
                $scope.childSelectedSsrAmount = 0;

                if($scope.noSsr)
                {
                    $scope.ssrDetailsBind();
                }

               

            } else {
                $scope.errorMesg = "Please agree the terms & condition"
            }
        } else {
            if($scope.docket != '' && $scope.docket != undefined)
            {
                $scope.docketinvalid = false;
            }
            else
            {
                $scope.docketinvalid = true;
            }
            $('html,body').scrollTop(240);
            $scope.errorMesg = "Enter All * (Mandatory) Fields";
            return false;
        }
        $window.scrollTo(0, 0);

        // $scope.seatFunction=false;

        // }
    }

    $scope.paymentForm = {
        firstName: "",
        lastName: "",
        address: "",
        emailAddress: "",
        countryCode: "",
        city: ""
    };

    $scope.paymentDel = {
        firstName: "",
        lastName: "",
        address: "",
        emailAddress: "",
        countryCode: "",
        city: ""
    };
        $scope.billing = function() {
        if ($("#bill").hasClass("active") || $("#serbill").hasClass("active")) {
            $scope.validateservicedata = false;
            $scope.validateservicedatapament = true;
            $scope.validatedata = false;
            $scope.validateguestdata = true;
        }
    }
    $scope.serbilling = function() {
        if ($("#serv").hasClass("active")) {
            
            $("#serbill").removeClass('active');
            $scope.validateguestdata = true;
            $scope.validateservicedata = true;
            $scope.validateservicedatapament = false;
            $scope.validatedata = false;

        }
    }

    $scope.errorMesg = null;
    $scope.proceedTravellerDetails = function(valid) {
        // if ($("#trvl").hasClass("active")) {
            $scope.backtodetail = false;
            $scope.backtotraveler = true;
        $scope.submitted = true;
        var passengerform=true;
        if (valid) {
            $window.scrollTo(0, 0);
           
            if(passengerform==true){
                $scope.totaladultdata=[];
                $scope.totalchailddata=[];
                $scope.totalInfantdata=[];
                $scope.travelList=$scope.travellersListByRoom;
                for(var k=0; k<$scope.travelList.length; k++){
                for(var m=0; m<$scope.travelList[k].adultTravellersList.length; m++){		
                $scope.totaladultdata.push($scope.travelList[k].adultTravellersList[m]);
                }
                for(var n=0; n<$scope.travelList[k].childTravellersList.length; n++){
                $scope.totalchailddata.push($scope.travelList[k].childTravellersList[n]);
                }
                for(var i=0; i<$scope.travelList[k].infantTravellersList.length; i++){
                    $scope.totalInfantdata.push($scope.travelList[k].infantTravellersList[i]);
                    }
                }
                /* $scope.totaladultdata=$scope.travelList[0].adultTravellersList;
                $scope.totalchailddata=$scope.travelList[0].childTravellersList; */
                $scope.totalpassengers = $scope.totaladultdata.concat($scope.totalchailddata).concat($scope.totalInfantdata);
                // $scope.totalpassengers=$scope.totalpassenger.concat($scope.totalInfantdata);
        //@author Raghava start...
        for(var i=0;i<$scope.totalpassengers.length; i++){
            var count=0;
            var outerFirstName=$scope.totalpassengers[i].personaldata.fname;
        var outerPassport=$scope.totalpassengers[i].personaldata.passportNo;
            for(var j=0;j<$scope.totalpassengers.length; j++){
                var innerFirstName=$scope.totalpassengers[j].personaldata.fname;
                var innerPassport=$scope.totalpassengers[j].personaldata.passportNo;
        if(innerFirstName===outerFirstName||innerPassport===outerPassport){
        count++;
        if(count>1){
            Lobibox.alert('error',
            {
                msg: "Guest details are duplicated."
            });
        
            // alert("duplicate Traveller Found.")
        
            return;
        }
        }
        
        
            }
        }
            }
            if ($('#term_con').is(":checked")) {
                $("#bill").addClass("active");
                $scope.validateguestdata = true;
                $scope.validateservicedata = false;
                $scope.validateservicedatapament = true;
                $scope.validatedata = false;
                $scope.errorMesg = null;
                $scope.paymentDel.firstName = $scope.travellersListByRoom[0].adultTravellersList[0].personaldata.fname;
                $scope.paymentDel.lastName = $scope.travellersListByRoom[0].adultTravellersList[0].personaldata.lname;
                $scope.paymentDel.emailAddress = $scope.travellersListByRoom[0].adultTravellersList[0].personaldata.email;
                $scope.paymentDel.countryCode = $scope.travellersListByRoom[0].adultTravellersList[0].personaldata.nationalitycode;
                // $scope.paymentDel.city = $scope.travellersListByRoom[0].adultTravellersList[0].personaldata.city;
           
                      } else {
                $scope.errorMesg = "Please agree the terms & condition"
            }
        } else {
            $('html,body').scrollTop(240);
            $scope.errorMesg = "Enter All * (Mandatory) Fields"
            return false;
        }
        // }
    }

    $scope.pnrNumber = "";
    $scope.isTickecting = "";
    $scope.ticketing = function(booking)
    {
        var token = JSON.parse(sessionStorage.authToken);
        var headersOptions = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token
        }

        $http.post(ServerService.dropDownPath+ 'customer/'+ $scope.corprateCompanyId + '/flight/credit/book/issueTicket',booking, {
            headers: headersOptions
        }).then(function successCallback(response) {
            $scope.loading = false;
            $scope.disabled = false;
            $scope.artloading = false;
            /* $scope.paymentDetail = $scope.cardDetail; */
            if (response.data.success) {
                sessionStorage.setItem('bookingItneryResponse',JSON.stringify(response.data));       
                $location.path('/flightItenary');
            } else {

                Lobibox.alert('error', {
                    msg: response.data.errorMessage
                });

            }


        }, function errorCallback(response) {
            if(response.status == 403){
                var status = refreshService.refreshToken(token.data.refreshToken,token.data.authToken);
                status.then(function(greeting) {
                pnrcreate(booking);
                });
            }
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });
    }

    pnrcreate = function(booking){
        var token = JSON.parse(sessionStorage.authToken);
        var headersOptions = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token
        }
        
        var tempurl = '';
        tempurl = "/flight/credit/pnr/create";

        // if($scope.isLcc)
        // {
        //     tempurl = "flight/credit/pnr/create";
        // }
        // else
        // {
        //     tempurl = "flight/credit/book/issueTicket"
        // }

        $http.post(ServerService.dropDownPath+ 'customer/'+ $scope.corprateCompanyId + tempurl, booking, {
            headers: headersOptions
        }).then(function successCallback(response) {
            $scope.loading = false;
            $scope.disabled = false;
            $scope.artloading = false;
            /* $scope.paymentDetail = $scope.cardDetail; */
            if (response.data.success) {
                sessionStorage.setItem('bookingItneryResponse',JSON.stringify(response.data));       
                $location.path('/flightItenary');
                // sessionStorage.setItem("bookingResponse", JSON.stringify(response.data.data));
                // window.open(response.data.data.hostedPageLink, "_self")
            } else {
                Lobibox.alert('error', {
                    msg: response.data.errorMessage
                });
            }
        }, function errorCallback(response) {
            if(response.status == 403){
                var status = refreshService.refreshToken(token.data.refreshToken,token.data.authToken);
                status.then(function(greeting) {
                pnrcreate(booking);
                });
            }
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });
    }


    pnrcreateb2c = function(booking){
        var token = JSON.parse(sessionStorage.authToken);
        var headersOptions = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token
        }
      $scope.artloading = true;
    $http.post(ServerService.dropDownPath+ 'customer/'+ $scope.corprateCompanyId + '/flight/pnr/create', booking, {
        headers: headersOptions
    }).then(function successCallback(response) {
        $scope.loading = false;
        $scope.disabled = false;
        $scope.artloading = false;
        /* $scope.paymentDetail = $scope.cardDetail; */
        if (response.data.success) {
            // sessionStorage.setItem("bookingResponse", JSON.stringify(response.data.data));

            window.open(response.data.data.hostedPageLink, "_self")
        } else {

            Lobibox.alert('error', {
                msg: response.data.errorMessage
            });

        }


    }, function errorCallback(response) {
        if(response.status == 403){
            var status = refreshService.refreshToken(token.data.refreshToken,token.data.authToken);
            status.then(function(greeting) {
            pnrcreate(booking);
            });
        }
        // called asynchronously if an error occurs
        // or server returns response with an error status.
      });
}

booktmp = function(fopQuoteApproveReq,bookingReqObj,isticket,isb2c){
      $scope.artloading = true;
      $http.post(ServerService.dropDownPath + 'quote/approve', fopQuoteApproveReq,{
    }).then(function successCallback(response){
      $scope.artloading = false;
      if(response.data.success){
        sessionStorage.setItem("QuoteApproved",true);
        $scope.isQuoteApproved = true;

            if(isticket)
                $scope.ticketing($scope.bookingReq);
            else if(isb2c)
                pnrcreateb2c(bookingReqObj);
            else
                pnrcreate($scope.bookingReq);

        }else{

        }
    });
}

$scope.updateQuote = function(bookingReqObj,isticket,isb2c){
    $scope.artloading = false;

    var approvalRemarks = {};
    Lobibox.prompt('text', //Any input type will be valid
    {
        title: 'Please enter Approval Remarks',
        //Attributes of <input>
        attrs: { 
            placeholder: "Approval Remarks"
        },
        buttons: ['ok'],
        callback: function (lobibox, type) {
            if(type == 'ok')
            {
                approvalRemarks = document.getElementsByClassName("lobibox-input");
                $scope.approvalRemarks = approvalRemarks[0].value;

                var fopQuoteApproveReq = JSON.parse(sessionStorage.getItem('bookingQuotationResponse'));
                fopQuoteApproveReq.approvalRemarks = $scope.approvalRemarks;

                booktmp(fopQuoteApproveReq,bookingReqObj,isticket,isb2c);
            }
        }
    });

}



$scope.proceedCustomDetailsb2c = function(ticketing) {
    $scope.isTickecting = ticketing;
    $scope.paysubmitted = true;
    $scope.submitted = true;
    //  if (valid && ($scope.docket !="" && $scope.docket != undefined)) {
        $scope.docketinvalid = false;
        $scope.baggagedata = false
        $scope.payment = true;
        $scope.pamentbutton = false;
        $scope.likeMeal = 1;
        $scope.travelerstotallcount = [];
        for (var adt = 0; adt < $scope.travellersListByRoom[0].adultTravellersList.length; adt++) {
            var adttrl = $scope.travellersListByRoom[0].adultTravellersList[adt].personaldata;
            $scope.travellersListByRoom[0].adultTravellersList[adt].personaldata.mobcntrycode = adttrl.mobcntrycodetrobj.name;
            $scope.travellersListByRoom[0].adultTravellersList[adt].personaldata.dob =
                adttrl.birthdate + "/" + adttrl.birthmonth + "/" + adttrl.birthyear;
                if( $scope.travellersListByRoom[0].adultTravellersList[adt].personaldata.expirydate==null ||$scope.travellersListByRoom[0].adultTravellersList[adt].personaldata.expirymonth==''|| $scope.travellersListByRoom[0].adultTravellersList[adt].personaldata.expiryyear==undefined){
                    $scope.travellersListByRoom[0].adultTravellersList[adt].personaldata.passExp=null; 
                }else{
                   
                    $scope.travellersListByRoom[0].adultTravellersList[adt].personaldata.passExp =
                    adttrl.expirydate + "/" + adttrl.expirymonth + "/" + adttrl.expiryyear;
                }

             
        }
        for (var adt = 0; adt < $scope.travellersListByRoom[0].childTravellersList.length; adt++) {
            var adttrl = $scope.travellersListByRoom[0].childTravellersList[adt].personaldata;
            if( $scope.travellersListByRoom[0].childTravellersList[adt].personaldata.mobcntrycode==undefined ||$scope.travellersListByRoom[0].childTravellersList[adt].personaldata.mobcntrycode==''|| $scope.travellersListByRoom[0].childTravellersList[adt].personaldata.mobcntrycode==null){
                $scope.travellersListByRoom[0].childTravellersList[adt].personaldata.mobcntrycode=null; 
            }else{
               
            $scope.travellersListByRoom[0].childTravellersList[adt].personaldata.mobcntrycode = adttrl.mobcntrycodetrobj.name;
            }

            $scope.travellersListByRoom[0].childTravellersList[adt].personaldata.dob =
                adttrl.birthdate + "/" + adttrl.birthmonth + "/" + adttrl.birthyear;
                
                if( $scope.travellersListByRoom[0].childTravellersList[adt].personaldata.expirydate==null ||$scope.travellersListByRoom[0].childTravellersList[adt].personaldata.expirymonth==''|| $scope.travellersListByRoom[0].childTravellersList[adt].personaldata.expiryyear==undefined){
                    $scope.travellersListByRoom[0].childTravellersList[adt].personaldata.passExp=null; 
                }else{
                   
                    $scope.travellersListByRoom[0].childTravellersList[adt].personaldata.passExp =
                    adttrl.expirydate + "/" + adttrl.expirymonth + "/" + adttrl.expiryyear;
                }

             
        }
        for (var adt = 0; adt < $scope.travellersListByRoom[0].infantTravellersList.length; adt++) {
            var adttrl = $scope.travellersListByRoom[0].infantTravellersList[adt].personaldata;
            if( $scope.travellersListByRoom[0].infantTravellersList[adt].personaldata.mobcntrycode==undefined || $scope.travellersListByRoom[0].infantTravellersList[adt].personaldata.mobcntrycode=='' || $scope.travellersListByRoom[0].infantTravellersList[adt].personaldata.mobcntrycode==null){
                $scope.travellersListByRoom[0].infantTravellersList[adt].personaldata.mobcntrycode=null; 
            }else{
               
                $scope.travellersListByRoom[0].infantTravellersList[adt].personaldata.mobcntrycode = adttrl.mobcntrycodetrobj.name;
            }

           
            $scope.travellersListByRoom[0].infantTravellersList[adt].personaldata.dob =
                adttrl.birthdate + "/" + adttrl.birthmonth + "/" + adttrl.birthyear;
                if( $scope.travellersListByRoom[0].infantTravellersList[adt].personaldata.expirydate==null ||$scope.travellersListByRoom[0].infantTravellersList[adt].personaldata.expirymonth==''|| $scope.travellersListByRoom[0].infantTravellersList[adt].personaldata.expiryyear==undefined){
                    $scope.travellersListByRoom[0].infantTravellersList[adt].personaldata.passExp=null; 
                }else{
                   
                    $scope.travellersListByRoom[0].infantTravellersList[adt].personaldata.passExp =
                    adttrl.expirydate + "/" + adttrl.expirymonth + "/" + adttrl.expiryyear;
                }

              

        }
        $scope.travelerscount = $scope.travellersListByRoom[0].adultTravellersList;

        $scope.travelerscount1 = $scope.travellersListByRoom[0].childTravellersList;
        $scope.travelerscount2 = $scope.travellersListByRoom[0].infantTravellersList;
        $scope.artloading = true;

        $scope.travelerscount.forEach(function(selectedItem) {
            $scope.travelerstotallcount.push(selectedItem);
        });
        $scope.travelerscount1.forEach(function(selectedItem) {
            $scope.travelerstotallcount.push(selectedItem);
        });
        $scope.travelerscount2.forEach(function(selectedItem) {
            $scope.travelerstotallcount.push(selectedItem);
        });
        $scope.travelerstotallcount.forEach(travellerObj => {

            if (Object.prototype.toString.call(travellerObj.personaldata.passExp) === "[object Date]") {
                var d = new Date(travellerObj.personaldata.passExp);
                var day = d.getDate();
                var monthIndex = d.getMonth() + 1;
                var year = d.getFullYear();

                travellerObj.personaldata.passExp = day + '/' + monthIndex + '/' + year;
            }


        });
        sessionStorage.setItem("fltTravellerdatalist", JSON.stringify($scope.travelerstotallcount));
        sessionStorage.setItem("travellersListByRoom", JSON.stringify($scope.travellersListByRoom))

        $scope.submitted = true;
        // $scope.validateservicedata = false;
        // $scope.validatedata = false;
        // $scope.validateguestdata = true;
        // $scope.validateservicedatapament = true; //false
        $scope.travelerstotallcount = [];
        $scope.travelerstotallcount = JSON.parse(sessionStorage.getItem("fltTravellerdatalist"));
        $scope.flightBookSession = JSON.parse(sessionStorage.getItem('flightPriceData'));

        $scope.sessionid = $scope.flightBookSession.sessionId;

        $scope.providerName = $scope.flightBook.selectedResult.flightSearchResults[0].provider;

        $scope.searchtotalldata = $scope.flightBook.selectedResult;
        $scope.bookingReq = new Object();

        $scope.bookingReq.sessionId = $scope.flightBookSession.sessionId;
        if($scope.selectedInsurancePlantotalFare){
        $scope.bookingReq.selectedInsurancePlanId = $scope.selectedInsurancePlan.sessionId;
        }else{
            $scope.bookingReq.selectedInsurancePlan = null;
        }
        $scope.bookingReq.paymentMode = "PAYMENT";
        $scope.bookingReq.paymentParameters = $scope.paymentDel;
        $scope.bookingReq.paymentParameters.successUrl = "paymentRedirector.html";
            $scope.bookingReq.paymentParameters.failureUrl = "paymentRedirector.html";
            $scope.bookingReq.paymentParameters.cancelUrl = "!#!/flightReview";
            $scope.bookingReq.paymentParameters.baseUrl= baseUrlService.paymentBaseUrl+'/',
            
            // var tempgrandtot = 0;
            // if($scope.fngrandtotal >0)
            // {
            //     tempgrandtot = $scope.fngrandtotal + $scope.selectedInsurancePlan.totalFare - $scope.discount;
            // }
            // else
            // {
            //     tempgrandtot = $scope.flightBooking.flightSearchResults[0].grndTotal + $scope.selectedInsurancePlan.totalFare + $scope.totalSSR + $scope.loopTot + $scope.totMeals + $scope.totseat - $scope.discount;
            // }

            $scope.travelerstotallcount.forEach(ele => {
                if(ele.miscdata.mealpref[0].code == null)
                {
                    ele.miscdata.mealpref = [];
                }
                if(ele.miscdata.seatpref[0].code == null)
                {
                    ele.miscdata.seatpref = [];
                }
            });
            var tempgrandtot = 0;
            if($scope.fngrandtotal >0 && $scope.selectedInsurancePlantotalFare >0)
            {
                tempgrandtot = $scope.fngrandtotal + $scope.selectedInsurancePlan.totalFare - $scope.discount;
            }
            if($scope.fngrandtotal == 0 && $scope.selectedInsurancePlantotalFare >0)
            {
                tempgrandtot = $scope.flightBooking.flightSearchResults[0].grndTotal + $scope.selectedInsurancePlan.totalFare + $scope.totalSSR + $scope.loopTot + $scope.totMeals + $scope.totseat - $scope.discount;
            }
            
            if($scope.fngrandtotal >0 && $scope.selectedInsurancePlantotalFare == 0)
            {
                tempgrandtot = $scope.fngrandtotal - $scope.discount;
            }
            if($scope.fngrandtotal == 0 && $scope.selectedInsurancePlantotalFare == 0)
            {
                tempgrandtot = $scope.flightBooking.flightSearchResults[0].grndTotal + $scope.totalSSR + $scope.loopTot + $scope.totMeals + $scope.totseat - $scope.discount;
            }
            $scope.bookingReq.travellerdatalist = $scope.travelerstotallcount;
            // $scope.bookingReq.docketNo = $( "#docket" ).val();
            $scope.bookingReq.paymentParameters.promoCodeDiscount = $scope.discount;
            $scope.bookingReq.paymentParameters.grandTotal = tempgrandtot;
            $scope.bookingReq.paymentParameters.promoCode = $scope.promocode;

        $scope.bookingReq = $scope.bookingReq;

        localStorage.setItem('flight/pnr/create', JSON.stringify($scope.bookingReq))
     
        				
                if($scope.isQuote && !$scope.isQuoteApproved)
                    $scope.updateQuote($scope.bookingReq,false,true);
                else
                    pnrcreateb2c($scope.bookingReq);       

       

}






    $scope.proceedCustomDetails = function(ticketing) {
        $scope.isTickecting = ticketing;
        $scope.paysubmitted = true;
        $scope.submitted = true;
        //  if (valid && ($scope.docket !="" && $scope.docket != undefined)) {
            $scope.docketinvalid = false;
            $scope.baggagedata = false
            $scope.payment = true;
            $scope.pamentbutton = false;
            $scope.likeMeal = 1;
            $scope.travelerstotallcount = [];
            for (var adt = 0; adt < $scope.travellersListByRoom[0].adultTravellersList.length; adt++) {
                var adttrl = $scope.travellersListByRoom[0].adultTravellersList[adt].personaldata;
                $scope.travellersListByRoom[0].adultTravellersList[adt].personaldata.mobcntrycode = adttrl.mobcntrycodetrobj.name;
                $scope.travellersListByRoom[0].adultTravellersList[adt].personaldata.dob =
                    adttrl.birthdate + "/" + adttrl.birthmonth + "/" + adttrl.birthyear;
                    if( $scope.travellersListByRoom[0].adultTravellersList[adt].personaldata.expirydate==null ||$scope.travellersListByRoom[0].adultTravellersList[adt].personaldata.expirymonth==''|| $scope.travellersListByRoom[0].adultTravellersList[adt].personaldata.expiryyear==undefined){
                        $scope.travellersListByRoom[0].adultTravellersList[adt].personaldata.passExp=null; 
                    }else{
                       
                        $scope.travellersListByRoom[0].adultTravellersList[adt].personaldata.passExp =
                        adttrl.expirydate + "/" + adttrl.expirymonth + "/" + adttrl.expiryyear;
                    }

                 
            }
            for (var adt = 0; adt < $scope.travellersListByRoom[0].childTravellersList.length; adt++) {
                var adttrl = $scope.travellersListByRoom[0].childTravellersList[adt].personaldata;
                if( $scope.travellersListByRoom[0].childTravellersList[adt].personaldata.mobcntrycode==undefined ||$scope.travellersListByRoom[0].childTravellersList[adt].personaldata.mobcntrycode==''|| $scope.travellersListByRoom[0].childTravellersList[adt].personaldata.mobcntrycode==null){
                    $scope.travellersListByRoom[0].childTravellersList[adt].personaldata.mobcntrycode=null; 
                }else{
                   
                $scope.travellersListByRoom[0].childTravellersList[adt].personaldata.mobcntrycode = adttrl.mobcntrycodetrobj.name;
                }

                $scope.travellersListByRoom[0].childTravellersList[adt].personaldata.dob =
                    adttrl.birthdate + "/" + adttrl.birthmonth + "/" + adttrl.birthyear;
                    
                    if( $scope.travellersListByRoom[0].childTravellersList[adt].personaldata.expirydate==null ||$scope.travellersListByRoom[0].childTravellersList[adt].personaldata.expirymonth==''|| $scope.travellersListByRoom[0].childTravellersList[adt].personaldata.expiryyear==undefined){
                        $scope.travellersListByRoom[0].childTravellersList[adt].personaldata.passExp=null; 
                    }else{
                       
                        $scope.travellersListByRoom[0].childTravellersList[adt].personaldata.passExp =
                        adttrl.expirydate + "/" + adttrl.expirymonth + "/" + adttrl.expiryyear;
                    }

                 
            }
            for (var adt = 0; adt < $scope.travellersListByRoom[0].infantTravellersList.length; adt++) {
                var adttrl = $scope.travellersListByRoom[0].infantTravellersList[adt].personaldata;
                if( $scope.travellersListByRoom[0].infantTravellersList[adt].personaldata.mobcntrycode==undefined || $scope.travellersListByRoom[0].infantTravellersList[adt].personaldata.mobcntrycode=='' || $scope.travellersListByRoom[0].infantTravellersList[adt].personaldata.mobcntrycode==null){
                    $scope.travellersListByRoom[0].infantTravellersList[adt].personaldata.mobcntrycode=null; 
                }else{
                   
                    $scope.travellersListByRoom[0].infantTravellersList[adt].personaldata.mobcntrycode = adttrl.mobcntrycodetrobj.name;
                }

               
                $scope.travellersListByRoom[0].infantTravellersList[adt].personaldata.dob =
                    adttrl.birthdate + "/" + adttrl.birthmonth + "/" + adttrl.birthyear;
                    if( $scope.travellersListByRoom[0].infantTravellersList[adt].personaldata.expirydate==null ||$scope.travellersListByRoom[0].infantTravellersList[adt].personaldata.expirymonth==''|| $scope.travellersListByRoom[0].infantTravellersList[adt].personaldata.expiryyear==undefined){
                        $scope.travellersListByRoom[0].infantTravellersList[adt].personaldata.passExp=null; 
                    }else{
                       
                        $scope.travellersListByRoom[0].infantTravellersList[adt].personaldata.passExp =
                        adttrl.expirydate + "/" + adttrl.expirymonth + "/" + adttrl.expiryyear;
                    }

                  

            }
            $scope.travelerscount = $scope.travellersListByRoom[0].adultTravellersList;

            $scope.travelerscount1 = $scope.travellersListByRoom[0].childTravellersList;
            $scope.travelerscount2 = $scope.travellersListByRoom[0].infantTravellersList;
            $scope.artloading = true;

            $scope.travelerscount.forEach(function(selectedItem) {
                $scope.travelerstotallcount.push(selectedItem);
            });
            $scope.travelerscount1.forEach(function(selectedItem) {
                $scope.travelerstotallcount.push(selectedItem);
            });
            $scope.travelerscount2.forEach(function(selectedItem) {
                $scope.travelerstotallcount.push(selectedItem);
            });
            $scope.travelerstotallcount.forEach(travellerObj => {

                if (Object.prototype.toString.call(travellerObj.personaldata.passExp) === "[object Date]") {
                    var d = new Date(travellerObj.personaldata.passExp);
                    var day = d.getDate();
                    var monthIndex = d.getMonth() + 1;
                    var year = d.getFullYear();

                    travellerObj.personaldata.passExp = day + '/' + monthIndex + '/' + year;
                }


            });
            sessionStorage.setItem("fltTravellerdatalist", JSON.stringify($scope.travelerstotallcount));
            sessionStorage.setItem("travellersListByRoom", JSON.stringify($scope.travellersListByRoom))

            $scope.submitted = true;
            // $scope.validateservicedata = false;
            // $scope.validatedata = false;
            // $scope.validateguestdata = true;
            // $scope.validateservicedatapament = true; //false
            $scope.travelerstotallcount = [];
            $scope.travelerstotallcount = JSON.parse(sessionStorage.getItem("fltTravellerdatalist"));
            $scope.flightBookSession = JSON.parse(sessionStorage.getItem('flightPriceData'));

            $scope.sessionid = $scope.flightBookSession.sessionId;

            $scope.providerName = $scope.flightBook.selectedResult.flightSearchResults[0].provider;

            $scope.searchtotalldata = $scope.flightBook.selectedResult;
            $scope.bookingReq = new Object();

            $scope.bookingReq.sessionId = $scope.flightBookSession.sessionId;
            // if($scope.selectedInsurancePlantotalFare){
            // $scope.bookingReq.selectedInsurancePlanId = $scope.selectedInsurancePlan.sessionId;
            // }else{
            //     $scope.bookingReq.selectedInsurancePlan = null;
            // }
            // $scope.bookingReq.paymentMode = "PAYMENT";
            // $scope.bookingReq.paymentParameters = $scope.paymentDel;
            // $scope.bookingReq.paymentParameters.successUrl = "paymentRedirector.html";
            //     $scope.bookingReq.paymentParameters.failureUrl = "paymentRedirector.html";
            //     $scope.bookingReq.paymentParameters.cancelUrl = "#!/flightReview";
                
                // var tempgrandtot = 0;
                // if($scope.fngrandtotal >0)
                // {
                //     tempgrandtot = $scope.fngrandtotal + $scope.selectedInsurancePlan.totalFare - $scope.discount;
                // }
                // else
                // {
                //     tempgrandtot = $scope.flightBooking.flightSearchResults[0].grndTotal + $scope.selectedInsurancePlan.totalFare + $scope.totalSSR + $scope.loopTot + $scope.totMeals + $scope.totseat - $scope.discount;
                // }

                $scope.travelerstotallcount.forEach(ele => {
                    if(ele.miscdata.mealpref[0].code == null)
                    {
                        ele.miscdata.mealpref = [];
                    }
                    if(ele.miscdata.seatpref[0].code == null)
                    {
                        ele.miscdata.seatpref = [];
                    }
                });
                
                $scope.bookingReq.travellerdatalist = $scope.travelerstotallcount;
                $scope.bookingReq.docketNo = $( "#docket" ).val();
                // $scope.bookingReq.paymentParameters.promoCodeDiscount = $scope.discount;
                // $scope.bookingReq.paymentParameters.grandTotal = tempgrandtot;
                // $scope.bookingReq.paymentParameters.promoCode = $scope.promocode;

            $scope.bookingReq = $scope.bookingReq;

            localStorage.setItem('flight/pnr/create', JSON.stringify($scope.bookingReq))
            if($scope.isTickecting == 'true')
            {
                if($scope.isQuote && !$scope.isQuoteApproved)
                    $scope.updateQuote($scope.bookingReq,true,false);
                else
                    $scope.ticketing($scope.bookingReq);
            }
            else
            {
                if($scope.isQuote && !$scope.isQuoteApproved)
                    $scope.updateQuote($scope.bookingReq,false,false);
                else
                    pnrcreate($scope.bookingReq);
            }
        //  } else {
        //     if($scope.docket != '' && $scope.docket != undefined)
        //     {
        //         $scope.docketinvalid = false;
        //     }
        //     else
        //     {
        //         $scope.docketinvalid = true;
        //     }
        //     $('html,body').scrollTop(240);
        //     $scope.errorMesg = "Enter All * (Mandatory) Fields";
        //     return false;
        // }
    }

    $scope.gdsBooking = function(valid,ticketing)
    {
        $scope.submitted = true;
        if (valid && ($scope.docket !="" && $scope.docket != undefined)) 
        {
            $scope.docketinvalid = false;
            var passengerform=true;
            $scope.totaladultdata=[];
            $scope.totalchailddata=[];
            $scope.totalInfantdata=[];
            $scope.travelList=$scope.travellersListByRoom;
            for(var k=0; k<$scope.travelList.length; k++){
            for(var m=0; m<$scope.travelList[k].adultTravellersList.length; m++){		
            $scope.totaladultdata.push($scope.travelList[k].adultTravellersList[m]);
            }
            for(var n=0; n<$scope.travelList[k].childTravellersList.length; n++){
            $scope.totalchailddata.push($scope.travelList[k].childTravellersList[n]);
            }
            for(var i=0; i<$scope.travelList[k].infantTravellersList.length; i++){
                $scope.totalInfantdata.push($scope.travelList[k].infantTravellersList[i]);
                }
            }
            /* $scope.totaladultdata=$scope.travelList[0].adultTravellersList;
            $scope.totalchailddata=$scope.travelList[0].childTravellersList; */
            $scope.totalpassengers = $scope.totaladultdata.concat($scope.totalchailddata).concat($scope.totalInfantdata);
            // $scope.totalpassengers=$scope.totalpassenger.concat($scope.totalInfantdata);
    //@author Raghava start...
    for(var i=0;i<$scope.totalpassengers.length; i++){
        var count=0;
        var outerFirstName=$scope.totalpassengers[i].personaldata.fname;
    var outerPassport=$scope.totalpassengers[i].personaldata.passportNo;
        for(var j=0;j<$scope.totalpassengers.length; j++){
            var innerFirstName=$scope.totalpassengers[j].personaldata.fname;
            var innerPassport=$scope.totalpassengers[j].personaldata.passportNo;
    if(innerFirstName===outerFirstName||innerPassport===outerPassport){
    count++;
    if(count>1){
        Lobibox.alert('error',
        {
            msg: "Guest details are duplicated."
        });
    
        // alert("duplicate Traveller Found.")
    
        return;
    }
    }
        }
    }
        if ($('#term_con').is(":checked")) {
            $scope.errorMesg = null;
            $("#serv").addClass("active");

            $scope.proceedCustomDetails(ticketing);

        } else {
            $scope.errorMesg = "Please agree the terms & condition";
        }
    }

        else
        {
            if($scope.docket != '' && $scope.docket != undefined)
            {
                $scope.docketinvalid = false;
            }
            else
            {
                $scope.docketinvalid = true;
            }
            $('html,body').scrollTop(240);
            $scope.errorMesg = "Enter All * (Mandatory) Fields";
            return false;
        }
    }

        /* end */
        // console.log($scope.flightSearchResults);
    $scope.getNumber = function(num) {
            return new Array(num);
        }
        /* } */

        sessionStorage.setItem('GreatGrandTotal',$scope.fngrandtotal > 0 ? $scope.fngrandtotal + $scope.selectedInsurancePlan.totalFare : $scope.flightBooking.flightSearchResults[0].grndTotal + $scope.totalSSR + $scope.loopTot + $scope.totMeals + $scope.totseat);

});


function between(x, min, max) {
    return x >= min && x <= max;
}

    ouaApp.controller('mealinfoContent', function($scope, $uibModalInstance, $rootScope) {
        var bindmealsdetailssub = [];
        bindmealsdetailssub = JSON.parse(sessionStorage.getItem("bindmealsdetails1"));
        $scope.multiMealsEnabled = $rootScope.multiMealsEnabled;
        $scope.bundledFareId = $rootScope.bundledFareId ;
        $scope.bindmealsdetailssub = bindmealsdetailssub;

        // if($scope.bundledFareId.bundledFareId != null){
        //     $scope.multiMealsEnabled =  'false';
        // }

        var price = 0;
        var index = 0;
        var mealsselectedlist = []
            // for(var cnt=0; cnt<$scope.bindmealsdetailssub.length; cnt++){
            // 	// document.getElementById("myNumber_"+index).value = 0;
            // 	$scope.bindmealsdetailssub[index].totprice = 0;
            // 	$scope.bindmealsdetailssub[index].totqnty = 0;
            // }
        $scope.up = function(max, meal, index) {
            price = 0;
            index = index;
            var totcount = 0;
            $scope.bindmealsdetailssub.forEach(element => {
                totcount += element.count;
            });
            // if($scope.bundledFareId.bundledFareId != null && totcount > 1){
            //     Lobibox.alert('error',
            //     {
            //         msg: "Only one Meal selection will be Allowed!"
            //     });
            // }
            // else{
                document.getElementById("myNumber_" + index).value = parseInt(document.getElementById("myNumber_" + index).value) + 1;
                if (document.getElementById("myNumber_" + index).value >= parseInt(max)) {
                    document.getElementById("myNumber_" + index).value = max;
                }
                price = Number(meal.price) * Number(document.getElementById("myNumber_" + index).value)
                $scope.bindmealsdetailssub[index].totprice = price;
                $scope.bindmealsdetailssub[index].count = document.getElementById("myNumber_" + index).value;
    
            // }

            // $scope.bindmealsdetailssub[index].selectedMeal = true;
            // if (mealsselectedlist.length > 0) {
            //     mealsselectedlist.forEach(element => {
            //         if (element.index != index) {
            //             mealsselectedlist.push({ index: index, list: $scope.bindmealsdetailssub[index], key: $rootScope.key });
            //         }
            //     });
            // } else {
            //     mealsselectedlist.push({ index: index, list: $scope.bindmealsdetailssub[index], key: $rootScope.key });
            // }
        }
        $scope.down = function(min, meal, index) {
                price = 0;
                index = index;
                document.getElementById("myNumber_" + index).value = parseInt(document.getElementById("myNumber_" + index).value) - 1;
                if (document.getElementById("myNumber_" + index).value <= parseInt(min)) {
                    document.getElementById("myNumber_" + index).value = min;
                }
                price = Number(meal.price) * Number(document.getElementById("myNumber_" + index).value)
                $scope.bindmealsdetailssub[index].totprice = price;
                $scope.bindmealsdetailssub[index].count = document.getElementById("myNumber_" + index).value;
                // if (Number(document.getElementById("myNumber_" + index).value) > 0) {
                //     // $scope.bindmealsdetailssub[index].selectedMeal = true;
                //     if (mealsselectedlist.length > 0) {
                //         mealsselectedlist.forEach(element => {
                //             if (element.index != index) {
                //                 mealsselectedlist.push({ index: index, list: $scope.bindmealsdetailssub[index], key: $rootScope.key });
                //             }
                //         });
                //     } else {
                //         mealsselectedlist.push({ index: index, list: $scope.bindmealsdetailssub[index], key: $rootScope.key });
                //     }
                // }
            }
            // $scope.mealsData = MealsData;
            // $scope.mealsData[$rootScope.parentIndex].travelerDetails[$rootScope.index].quantity = 

        $scope.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        };
        $scope.ok = function() {
            sessionStorage.setItem("mealsselectedlist", JSON.stringify(mealsselectedlist))
            $uibModalInstance.close($scope.bindmealsdetailssub, mealsselectedlist);
        };
    }).controller('flightTermsController', function($http,ServerService,renderHTMLFactory, $scope, $uibModalInstance, $rootScope) {

        var tokenresult = JSON.parse(sessionStorage.getItem('authToken'));
             var headersOptions = {
                    'Content-Type': 'application/json',
                     'Authorization': 'Bearer ' + tokenresult
                 }
            
             $http.get(ServerService.profilePath + 'company/latest/tandc/1', {
                 headers: headersOptions
             }).then(function successCallback(response) {
                 $scope.termsresp = response.data.data.content;
             
             }, function errorCallback(response) {
                 if(response.status == 403){
                    var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
                status.then(function(greeting) {
                        $scope.flightTermsDetails();
                       });
                 }
             });

        $scope.ok = function() {
            $uibModalInstance.close($scope.selected.item);
        };

        $scope.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        };
    })
    ouaApp.controller('loginTravelController', function($scope, $uibModalInstance, $rootScope) {
        //console.log("fghfg");
            var travelIndexvalue=$rootScope.travelindex
            var whichpassenger=$rootScope.whichTraveller
            if(whichpassenger=='adult'){
                        $scope.allTravellerData=[];
                        $scope.adultIndex=travelIndexvalue;
                                 $scope.allTravellerDataa = JSON.parse(localStorage.getItem('loginTravellerData'));
                                  for(var t=0;  t<$scope.allTravellerDataa.length; t++){
                 if( $scope.allTravellerDataa[t].age>= 12){
                     $scope.allTravellerData.push($scope.allTravellerDataa[t])
                 }
                                 }
                                  if($scope.allTravellerData.length==0) {
                                     $scope.childText=" Traveller Data Not Available"
                                }
                            }
        
                            if(whichpassenger=='child'){
        
        
                                $scope.allTravellerData=[];
                                $scope.childIndex=travelIndexvalue;
                                         $scope.allTravellerDataa = JSON.parse(localStorage.getItem('loginTravellerData'));
                                         for(var t=0;  t<$scope.allTravellerDataa.length; t++){
                         if($scope.allTravellerDataa[t].age >= 2 && $scope.allTravellerDataa[t].age < 12){
                            $scope.allTravellerData.push($scope.allTravellerDataa[t])
                        }
                                         }
                                        if($scope.allTravellerData.length==0) {
                                            $scope.childText="Traveller Data Not Available"
                                        }
                            }
                            if(whichpassenger=='Infant'){
                                $scope.allTravellerData=[];
                                $scope.infantIndex=travelIndexvalue;
                                         $scope.allTravellerDataa = JSON.parse(localStorage.getItem('loginTravellerData'));
                                         for(var t=0;  t<$scope.allTravellerDataa.length; t++){
                        if( $scope.allTravellerDataa[t].age >= 0 && $scope.allTravellerDataa[t].age < 2){
                            $scope.allTravellerData.push($scope.allTravellerDataa[t])
                        }
                                         }
                                         if($scope.allTravellerData.length==0) {
                                            $scope.childText=" Traveller Data Not Available"
                                        }
        
                            }
        
                
            $scope.cancel = function() {
                $uibModalInstance.dismiss('cancel');
            };
        
            /* modelDismiss function */
            $scope.modelDismiss = function() {
                $uibModalInstance.close();
            }
        }).directive('expand', function () {
            function link($scope, element, attrs) {
                $scope.$on('onExpandAll', function (event, args) {
                    $scope.expanded = args.expanded;
                });
            }
            return {
                link: link
            };
        });