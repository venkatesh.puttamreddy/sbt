ouaApp.controller('flightQuotationController', function($scope, $http,corpCompanyDetails, renderHTMLFactory,$location, $uibModal, baseUrlService, expiryService, ConstantService, ServerService,$rootScope) {
    // var Serverurl = "http://localhost:8090/ibp/api/v1/";
    $scope.tokenresult = JSON.parse(sessionStorage.getItem('authentication'));
    // var tokenname = $scope.tokenresult.data;
    sessionStorage.removeItem('SelectedBagLst');
    sessionStorage.removeItem('SelectedMealLst');
    sessionStorage.removeItem('SelectedSeatLst');
    sessionStorage.removeItem("sendQuoteData");
    var priceStart=0;
    var priceEnd=0;
	    $scope.renderHTMLFactory = renderHTMLFactory;		
    $scope.hideprice=false;
    $scope.hidestar=false;
    $scope.departureTimehide=false;
    $scope.arrivelHide=false;
    $scope.airlinehideprice=false;
    $scope.fareTypeHide=false;
    $scope.baggageHide=false;
 
    corporateGridView = function(){
        $scope.tokenresult = JSON.parse(sessionStorage.getItem('authToken'));
        //  ajaxService.ajaxRequest('POST',ServerService.serverPath+'rest/repo / airport','{ "queryString" : "'+request.term+'" } ','json'),{ 
        var headersOptions = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + $scope.tokenresult
        }
        $scope.artloading = true;

$scope.filterrequest={
    "page": null,
    "getAllResults": true,
    "filter": {
      "logic": "and",
      "filters": [
//           {
//   "field":"quotationId",
//   "operator":"eq",
//   "value":"1"
//           }
      ]
    }
  }
  $http.post(ServerService.listPath + 'search/quotationMasterViewEntity',$scope.filterrequest, {
    headers: headersOptions
}).then(function successCallback(response) {
    $scope.quotationviewdata = response.data.data.data;
    $scope.quotationviewdata.reverse();
    $scope.artloadingcancellation = false;
    $scope.artloading = false;
    

});
    }

    corporateGridView();
    $scope.hotelQuotationLength = 0;
    $rootScope.cordinatorDetails = {};  

    $scope.hotelQuotationPopup = function(parentSelector){
        var parentElem = parentSelector ?
		angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;

        var hotelQuote = $scope.quotationRes.hotel['hotel-items'];
        $scope.hotelQuoteSearch = $scope.quotationRes.hotel['quote-details'].searchDetails;
        
        hotelQuote.forEach(element => {
            if(element.gblHotelQuatationList != undefined && element.gblHotelQuatationList.length>0){
                $scope.hotelQuotationLength++;
            }
        });

        $rootScope.cordinatorDetails.internalRemarks=$scope.quotationData.data[0].internalRemarks;  
        $rootScope.cordinatorDetails.externalRemarks=$scope.quotationData.data[0].externalRemarks;
        $rootScope.cordinatorDetails.status=$scope.quotationData.data[0].statusDesc; 
        $rootScope.cordinatorDetails.email=$scope.quotationData.data[0].toEmail
        $scope.selectTravellerData = $scope.quotationRes.hotel['quote-details'].paxProfiles;
        sessionStorage.setItem('selectTraveldata', JSON.stringify($scope.selectTravellerData));

        sessionStorage.setItem("QuoteCordinatorDetails",JSON.stringify($rootScope.cordinatorDetails));
        sessionStorage.setItem("HotelQuotationList",JSON.stringify(hotelQuote));
        sessionStorage.setItem("hotelSearchdataobj",JSON.stringify($scope.hotelQuoteSearch));
		sessionStorage.setItem("hotelQuotationLength",JSON.stringify($scope.hotelQuotationLength));

		$scope.animationEnabled = true;
		var modalInstance = $uibModal.open({
			animation: $scope.animationEnabled,
			ariaLabelledBy: 'modal-title',
			ariaDescribedBy: 'modal-body',
			templateUrl: 'hotelQuotation.html',
			controller: 'quotationControllers',
			backdrop: true,
			size: 'xl',
			appendTo: parentElem,
			resolve: {
				items: function() {
					return $scope.items
				}
			}
		})
    }

    $scope.flightQuotationPopup = function(parentSelector){
        var parentElem = parentSelector ?
		angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;

       
        $scope.hi = "hisf";
		$scope.animationEnabled = true;

        $scope.modalInstance=$uibModal.open({
            templateUrl: 'flightQuotations.tmpl.html',
            backdrop: 'static',
            
            size: 'xl',
            keyboard: true,
            scope:$scope
        });
	
    }
   

    $scope.cancel = function() {
        $scope.modalInstance.dismiss('cancel');
    };

    $scope.quotationObj={};
$scope.viewDetails=function(size,parentSelector,quotation){
    var parentElem = parentSelector ?
    angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
    $scope.tokenresult = JSON.parse(sessionStorage.getItem('authToken'));
    //  ajaxService.ajaxRequest('POST',ServerService.serverPath+'rest/repo / airport','{ "queryString" : "'+request.term+'" } ','json'),{ 
    var headersOptions = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + $scope.tokenresult
    }
    $scope.filterrequest={
        "page": null,
        "getAllResults": true,
        "filter": {
          "logic": "and",
          "filters": [
            {

                "field":"quotationNo", 
                
                "operator":"eq",
                
                "value":quotation.quotationNo
                
                        }
          ]
        }
      }
    $scope.artloading = true;
    $http.post(ServerService.listPath + 'search/quotationMasterViewEntity',$scope.filterrequest, {
    
        headers: headersOptions
    }).then(function successCallback(response) {
    $scope.artloading = false;
    if(response.data.success)
        {
			     $scope.flight=false;
            $scope.insurence=false;					
            $scope.quotationRes = response.data.data.data[0].serviceDetails.quotation;
            sessionStorage.setItem('bookingQuotationResponse',JSON.stringify(response.data.data.data[0]));
            $scope.quotationData = response.data.data;
            $scope.quotationObj.internalRemarks=$scope.quotationData.data[0].internalRemarks;  
            $scope.quotationObj.extranelRemarks=$scope.quotationData.data[0].externalRemarks; 
            $scope.quotationObj.toEmail=$scope.quotationData.data[0].quoteSentTo
            $scope.quotationObj.corporateId =$scope.quotationData.data[0].agencyId
            $scope.quotationObj.status =$scope.quotationData.data[0].statusDesc
																																						
            // sessionStorage.setItem("sendQuoteData", JSON.stringify($scope.quotationGroup));
			//    sessionStorage.setItem('selectTraveldata', JSON.stringify($scope.selectTravellerData));
            
            if($scope.quotationRes.hotel['hotel-items'] != undefined)
            {
                $scope.hotelQuotationPopup(parentSelector);
            }else if( $scope.quotationRes.flight['flight-items'] != undefined){
                $scope.flightDetailsQuotation = $scope.quotationRes.flight['flight-items'];     
                $scope.quotationGroup=$scope.flightDetailsQuotation
                  $scope.selectTravellerData= $scope.quotationRes.flight['quote-details'].paxProfiles;
                  $scope.flightSearchRQ= $scope.quotationRes.flight['quote-details'].searchDetails
                  sessionStorage.setItem('flightSearchRQ',JSON.stringify($scope.flightSearchRQ));
                  $scope.flightQuotationPopup(parentSelector);																																							
                sessionStorage.setItem("sendQuoteData", JSON.stringify($scope.quotationGroup));
                   sessionStorage.setItem('selectTraveldata', JSON.stringify($scope.selectTravellerData));
                   $scope.flight=true;
               
            }else{
                $scope.selectedQuoteAvilplan= $scope.quotationRes.ins['ins-items'].availablePlans;
                $scope.selectedQuoteRiderplan= $scope.quotationRes.ins['ins-items'].upsellPlans;		
                $scope.selectTravellerData= $scope.quotationRes.ins['quote-details'].paxProfiles;	
                $scope.flightQuotationPopup(parentSelector);
                $scope.Insurance= $scope.quotationRes.ins['quote-details'].searchDetails
                sessionStorage.setItem('insurenceSearchRQ',JSON.stringify($scope.Insurance));
                sessionStorage.setItem('selectTraveldata', JSON.stringify($scope.selectTravellerData));
          $scope.insurence=true; 
            }

        }
        else
        {
      
            Lobibox.alert('error', {
                msg: response.data.errorMessage
            });
        }
    });
     
}

$scope.selectedQuotation=function(index){
    $scope.selectedQuotationFlight=[];
    $scope.selectedQuotationFlight.push($scope.flightDetailsQuotation[index])
}
$scope.travelPlanQuotationvalue=false;
		$scope.travelPlanQuotation=function(index){
            $scope.travelPlanQuotationvalue=true;
    // $scope.travelPlanQuotation=[];
    $scope.travelPlanQuotation=$scope.selectedQuoteAvilplan[index]
    sessionStorage.setItem('avilplan',JSON.stringify($scope.travelPlanQuotation));
}
$scope.lifeStylePlanQuotationvalue=false;
$scope.selectedRiderPlansIndexValue=[];
$scope.selectedRaidPlan = [];
$scope.lifeStylePlanQuotation=function(index){
    $scope.lifeStylePlanQuotationvalue=true;
    // $scope.lifeStylePlanQuotation=[];
    // $scope.lifeStylePlanQuotation.push($scope.selectedQuoteRiderplan[index])


  
	if($scope.selectedRiderPlansIndexValue.includes(index)){
		$scope.selectedRaidPlan.pop($scope.selectedQuoteRiderplan[index])
		$scope.selectedRiderPlansIndexValue.pop(index);
	

	}else{
		$scope.riderList = $scope.selectedRaidPlan.push($scope.selectedQuoteRiderplan[index]);
		$scope.selectedRiderPlansIndexValue.push(index);
		$scope.isLifeStylePlanSelected=true;

	

	}
	sessionStorage.setItem('ridersplan',JSON.stringify($scope.selectedRaidPlan));

}
								   
$scope.quotationReprice=function(sessionId,flightInfo){
    $scope.tokenresult = JSON.parse(sessionStorage.getItem('authToken'));
    //  ajaxService.ajaxRequest('POST',ServerService.serverPath+'rest/repo / airport','{ "queryString" : "'+request.term+'" } ','json'),{ 
    var headersOptions = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + $scope.tokenresult
    }
    var repriceReq={
        "sessionId": sessionId,
        "resultIndexId": flightInfo.flightSearchResults[0].indx,
        "corp":true
    }
        $http.post(ServerService.dropDownPath+'customer/'+''+$scope.quotationObj.corporateId+'/flight/reprice', repriceReq, {
         headers: headersOptions
        }).then(function successCallback(response) {
            if(response.data.success){

            $scope.artloading = false;
         
              
            $scope.flightPriceData = response.data.data
				 sessionStorage.setItem('firstFlightResponse', JSON.stringify(  $scope.flightPriceData.selectedResult.flightSearchResults[0].segGrpList[0]));																																	
            sessionStorage.setItem('flightPriceData', JSON.stringify(response.data));
           
            $location.path('/flightReview');     

        }else{
            $scope.artloading = true;
            Lobibox.notify('error', {
                size: 'mini',
                position: 'top right',
                msg:response.data.errorMessage,
                delay: 1500
            });
        }
    });
    
}


$scope.selectedPlanBean=[];
$scope.selectedriderPlans=[];
$scope.insurenceBook=function(){
    if( $scope.travelPlanQuotationvalue==false && $scope.lifeStylePlanQuotationvalue==false ){
        Lobibox.notify('error', {
            size: 'mini',
            position: 'top right',
            msg:"Please select Travel plans or Lifestyle Plans!",
            delay: 1500
        });
        return;
    } 
    $scope.tokenresult = JSON.parse(sessionStorage.getItem('authToken'));
    
    var headersOptions = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + $scope.tokenresult
    }
    $scope.InsuranceSearchRequest = JSON.parse(sessionStorage.getItem('InsuranceSearchReq'));
    var currency="AED"
    $scope.insQuotationBook={};

    $scope.insQuotationBook.travelInsuranceReq=$scope.InsuranceSearchRequest;
    // $scope.insQuotationBook.selectedPlanBean=$scope.selectedPlanBean;
    $scope.insQuotationBook.selectedPlanBean= $scope.travelPlanQuotation;
    $scope.insQuotationBook.selectedriderPlans=$scope.selectedriderPlans;
    $scope.insQuotationBook.selectedriderPlans=$scope.lifeStylePlanQuotation
    $http.post(ServerService.dropDownPath+'customer/'+''+$scope.quotationObj.corporateId+'/I/session/store',$scope.insQuotationBook,{ 
			   
        headers: headersOptions
}).then(function successCallback(response){


       if(response.data.success==true){
        $scope.sessionId = response.data.data;
        response.data.sessionId= $scope.sessionId;
        sessionStorage.setItem('InsuranceSearchResult',JSON.stringify(response.data));
   
        $location.path('/insuranceReview');     
           
             
        }else{
            $scope.artloading = true;
        } 
               
       
       
  });

}

$scope.quotationBook=function(){
    if( $scope.selectedQuotationFlight==undefined){
        Lobibox.notify('error', {
            size: 'mini',
            position: 'top right',
            msg:"Please select at least one quotation",
            delay: 1500
        });
        return;
    }
    $scope.tokenresult = JSON.parse(sessionStorage.getItem('authToken'));
    //  ajaxService.ajaxRequest('POST',ServerService.serverPath+'rest/repo / airport','{ "queryString" : "'+request.term+'" } ','json'),{ 
    var headersOptions = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + $scope.tokenresult
    }
    $scope.flightSearchRQ = JSON.parse(sessionStorage.getItem('flightSearchRQ'));
    var currency="AED"
    $scope.flightQuotationBook={};
    $scope.flightQuotationBook.adtCnt=  $scope.flightSearchRQ.adtCnt
    $scope.flightQuotationBook.chldCnt=  $scope.flightSearchRQ.chldCnt
    $scope.flightQuotationBook.infCnt=  $scope.flightSearchRQ.infCnt
    $scope.flightQuotationBook.curncy= currency
    $scope.flightQuotationBook.flightSearchResults= $scope.selectedQuotationFlight
    $scope.artloading = true;
    $http.post(ServerService.dropDownPath+'customer/'+''+$scope.quotationObj.corporateId+'/F/session/store',$scope.flightQuotationBook,{ 
			   
        headers: headersOptions
}).then(function successCallback(response){


       if(response.data.success==true){
        $scope.sessionId = response.data.data;
   
              $scope.quotationReprice($scope.sessionId,$scope.flightQuotationBook);
           
             
        }else{
            $scope.artloading = true;
        } 
               
       
       
  }, function errorCallback(response) {
      if(response.status == 403){
          var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
          status.then(function(greeting) {
           hotelcply();
          });
      }
    });
    
}
$scope.eachFlightDetails = function(index, FlightDetails,childIndex) {
      
    $scope.flightDetailsQuotation[index].segGrpList[childIndex].isOpen = ! $scope.flightDetailsQuotation[index].segGrpList[childIndex].isOpen;
    $scope.scrollTraget = index;
    $scope.flightDetails1 = FlightDetails;
    $scope.segGrpListchildIndex = childIndex;

}

$scope.outPolicyCheck = function(size, parentSelector, outPolicy,moduleName) {
    var parentElem = parentSelector ?
        angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
   
     $rootScope.outPolicyData = outPolicy;
     $rootScope.outPolicyData.module=moduleName


    $scope.animationEnabled = true;
    var modalInstance = $uibModal.open({
        animation: $scope.animationEnabled,
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: 'outPolicy.html',
        controller: 'outPolicyControllerList',
        backdrop: true,
        size: size,
        appendTo: parentElem,
        resolve: {
            items: function() {
                return $scope.items
            }
        }
    })
}

})

ouaApp.controller('outPolicyControllerList', function($scope, $http,ServerService, $uibModalInstance, $rootScope) {
    $scope.bookingQuotationResponse = JSON.parse(sessionStorage.getItem('bookingQuotationResponse'));
    $scope.corprateCompanyId=$scope.bookingQuotationResponse.agencyId;
       $scope.policydetails= $rootScope.outPolicyData;
       if( $scope.policydetails.module=="FLT"){
      $scope.policyRequestObj={};
      $scope.policyRequestObj.origin=  $scope.policydetails.origin
      $scope.policyRequestObj.dest=  $scope.policydetails.destination
      $scope.policyRequestObj.stops= $scope.policydetails.segGrpList[0].segList[0].stops
      $scope.policyRequestObj.refund=  $scope.policydetails.refund
      $scope.policyRequestObj.classType=  $scope.policydetails.classType
      $scope.policyRequestObj.designaiton="Manager",
      $scope.policyRequestObj.airlineCode=$scope.policydetails.segGrpList[0].segList[0].airline
      $scope.policyRequestObj.travelType= null
      $scope.quoteURL= "/flight/deviation";
       }else if($scope.policydetails.module=="INS"){
      
        $scope.Insurance = JSON.parse(sessionStorage.getItem('insurenceSearchRQ')); 
        $scope.policyRequestObj={};
        $scope.policyRequestObj.onwardDateTime=  $scope.Insurance.onwardDateTime
        $scope.policyRequestObj.returnDateTime=  $scope.Insurance.returnDateTime
        $scope.policyRequestObj.planTitle=  $scope.policydetails.planAdditionalInfoTitle
        $scope.policyRequestObj.designaiton= $scope.Insurance.designation,
        $scope.quoteURL= "/insurance/deviation";
       }else{
       
       }
      $scope.tokenresult = JSON.parse(sessionStorage.getItem('authToken'));
      var headersOptions = {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + $scope.tokenresult
      }
  
    $http.post(ServerService.dropDownPath + 'customer/'+ $scope.corprateCompanyId + $scope.quoteURL,$scope.policyRequestObj, {
    
        headers: headersOptions
    }).then(function successCallback(response) {
        
    $scope.loading = false;
    $scope.disabled = false;
    if (response.data.success == true) {
        $scope.outPolicy=response.data.data;

    }else{
$scope.noPolicy="Don't Have Any No Policys"
    }
  
    });
    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };

});


