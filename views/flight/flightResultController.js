ouaApp.controller('flightResultController', function($scope,refreshService,corpCompanyDetails, $timeout,$http, $location, $uibModal, $rootScope, ServerService) {
    // var Serverurl = "http://localhost:8090/ibp/api/v1/";
    $scope.tokenresult = JSON.parse(sessionStorage.getItem('authentication'));
    // var tokenname = $scope.tokenresult.data;
    corpCompanyDetails.getCompanyIdFlight();
    sessionStorage.removeItem('SelectedBagLst');
    sessionStorage.removeItem('SelectedMealLst');
    sessionStorage.removeItem('SelectedSeatLst');
    sessionStorage.removeItem("sendQuoteData");
    var priceStart=0;
    var priceEnd=0;
    $scope.hideprice=false;
    $scope.hidestar=false;
    $scope.departureTimehide=false;
    $scope.arrivelHide=false;
    $scope.airlinehideprice=false;
    $scope.fareTypeHide=false;
    $scope.baggageHide=false;
    $scope.quotationRequest = JSON.parse(sessionStorage.getItem('requestFrame'));
    // document.getElementById("myCheck").checked = true;
    
     $(".pricebutton").click(function(){
        
         if($scope.hideprice==false){
            $scope.hideprice=true;
        $("#collapseOne").css("display", "none");
         }else{
            $scope.hideprice=false;
            $("#collapseOne").css("display", "block");
         }
      });
      $scope.starRatingFunction=function(){
        if($scope.hidestar==false){
            $scope.hidestar=true;
        $("#StopsFilter").css("display", "none");
         }else{
            $scope.hidestar=false;
            $("#StopsFilter").css("display", "block");
         }  
      }

      $scope.airlineFunction=function(){
        
        if($scope.airlinehideprice==false){
           $scope.airlinehideprice=true;
       $("#AirlineFilter").css("display", "none");
        }else{
           $scope.airlinehideprice=false;
           $("#AirlineFilter").css("display", "block");
        }
     };

      $(".departureTime").click(function(){
        if($scope.departureTimehide==false){
            $scope.departureTimehide=true;
        $("#DepartureSlider").css("display", "none");
         }else{
            $scope.departureTimehide=false;
            $("#DepartureSlider").css("display", "block");
         }
        
      });

      $(".arrivalTime").click(function(){
        if($scope.arrivelHide==false){
            $scope.arrivelHide=true;
        $("#ArrivalSlider").css("display", "none");
         }else{
            $scope.arrivelHide=false;
            $("#ArrivalSlider").css("display", "block");
         }
     
      });
      $(".fareTypebutton").click(function(){
        if($scope.fareTypeHide==false){
            $scope.fareTypeHide=true;
        $("#FareTypeFilter").css("display", "none");
         }else{
            $scope.fareTypeHide=false;
            $("#FareTypeFilter").css("display", "block");
         }
     
      });
      $(".baggagebutton").click(function(){
        if($scope.baggageHide==false){
            $scope.baggageHide=true;
        $("#BaggageFilter").css("display", "none");
         }else{
            $scope.baggageHide=false;
            $("#BaggageFilter").css("display", "block");
         }
     
      });


    $scope.moreOptionFun1 = function() {
        $scope.flightSearchResults1 = [];
        var result = {};
        $scope.flightSearchResults.forEach(function(val) {


            var key = val.segGrpList[0].segList[0].airlineName + ":" + val.grndTotal;

            result[key] = result[key] || { airlineName: val.segGrpList[0].segList[0].airlineName, grndTotal: val.grndTotal, airlineList: [] };
            result[key].airlineList.push(val);
        });

        $scope.showMore = [];
        for (var i = 0; i < $scope.flightSearchResults1.length; i++) {
            $scope.showMore[i] = false;

        }
        return Object.keys(result).map(function(key) {
            $scope.flightSearchResults1.push(result[key]);

        });




    }

    $scope.fareDetails = function(quotation){
        $scope.tokenresult = JSON.parse(sessionStorage.getItem('authToken'));
        var index=quotation
        var headersOptions = {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Credentials': 'true',
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'GET, POST, PATCH, DELETE, PUT, OPTIONS',
            'Access-Control-Allow-Headers': 'Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With',
            'Authorization': 'Bearer ' + $scope.tokenresult
        }
        var sessionId=JSON.parse(sessionStorage.getItem('flightSessionId'));
        var repriceDetails = {
            "sessionId": sessionId,
            "resultIndexId": index.indx
        }
    
        // $scope.artloading = true;
    
        $http.post(ServerService.dropDownPath+ 'customer/'+ $scope.corprateCompanyId  + '/flight/farerules', repriceDetails, {
    
            headers: headersOptions
    	}).then(function(response){
            // $scope.artloading = false;
            var data = response.data.data;
            $scope.farerule = data;
          
            quotation.fareRules= $scope.farerule
            $scope.quotationGroup.push(quotation);
            sessionStorage.setItem("sendQuoteData", JSON.stringify($scope.quotationGroup));
            if (data != null) {
                $('.loading').hide();
                $scope.loading = false;
                $scope.disabled = false;
    
    
                $scope.farerule = data;
              
         
    
            } else {
                $scope.farerule = [];//"Fare rules not available";
                // $('.fareRules').append('Fare rules not available');
                $('.fareRules').css('text-align', 'center');
                $('.fareRules').css('color', 'red');
    
    
            }
    
        });
    
        }
	 
    $scope.filterFunction=function(){
        
    
            $(".js-range-slider").ionRangeSlider({
                type: "double",
                min: priceStart,
                max: priceEnd,
                from: priceStart,
                to: priceEnd,
                grid: true,
                prefix: "AED ",
                onFinish: function(data) {
                    var fromRange = data.from_pretty.replace(" ", "");
                    var toRange = data.to_pretty.replace(" ", "");
                    $scope.filtermodel.priceBean.fromRange = fromRange;
                    $scope.filtermodel.priceBean.toRange = toRange;
                    $scope.filterParam();
                }
            });
    
            var start = moment("2016-10-02 00:00", "YYYY-MM-DD HH:mm");
            var end = moment("2016-10-02 23:59", "YYYY-MM-DD HH:mm");
    
            // DEPT FILTER
            $(".departure-range-slider").ionRangeSlider({
                type: "double",
                grid: true,
                min: start.format("x"),
                max: end.format("x"),
                step: 1800000,
                prettify: function(num) {
    
                    return moment(num, "x").format("HH:mm:ss");
                },
                onFinish: function(data) {
                    var fromtimeArr = data.from_pretty.split(':');
    
                    var fromtimeInMilliseconds = (fromtimeArr[0] * 3600000) + (fromtimeArr[1] * 60000);
                    var totimeArr = data.to_pretty.split(':');
    
                    var totimeInMilliseconds = (totimeArr[0] * 3600000) + (totimeArr[1] * 60000);
    
                    $scope.filtermodel.deptTimeBean.fromDept = fromtimeInMilliseconds; // data.from_pretty;
                    $scope.filtermodel.deptTimeBean.toDept = totimeInMilliseconds; //data.to_pretty;
                    $scope.filterParam();
                }
            });
            // ARRIVAL FILTER
            $(".arrival-range-slider").ionRangeSlider({
                type: "double",
                grid: true,
                min: start.format("x"),
                max: end.format("x"),
                step: 1800000,
                prettify: function(num) {
    
                    return moment(num, "x").format("HH:mm:ss");
                },
                onFinish: function(data) {
                    var fromtimeArr = data.from_pretty.split(':');
    
                    var fromtimeInMilliseconds = (fromtimeArr[0] * 3600000) + (fromtimeArr[1] * 60000);
                    var totimeArr = data.to_pretty.split(':');
    
                    var totimeInMilliseconds = (totimeArr[0] * 3600000) + (totimeArr[1] * 60000);
    
                    $scope.filtermodel.arrivalTimeBean.fromArrival = fromtimeInMilliseconds; //data.from_pretty;
                    $scope.filtermodel.arrivalTimeBean.toArrival = totimeInMilliseconds; //data.to_pretty;
                    $scope.filterParam();
                }
            });
    
    
            $sidebar = $("#filterSideBar");
            $window = $(window);
            var sidebarOffset = $sidebar.offset();
    
            // $("#filterSideBar").sticky({ topSpacing: 150 });
            // $("#filterSideBar").on("bottom-reached", function () {
      
            // });
    
            $(window).scroll(function() {
                var sticky = $("#filterSideBar"),
             
                    scroll = $(window).scrollTop();
                  
                var footerScroll =
                    $(".footer").offset().top -
                    $(".footer").height() -
                    $(".header").height() -
                    125;
                if (scroll >= 150 && scroll < footerScroll)
                    sticky.addClass("sticky-sidebar");
                 
                if (scroll >= 2320 && scroll < footerScroll)
                    sticky.removeClass("sticky-sidebar");
                else sticky.removeClass("sticky-sidebar");
               
            });
           
    
            $('[data-toggle="tab"]').click(function() {
                let _href = $(this).attr("data-flightType");
                if (_href === "oneWay") {
                    $("#flyingto").slideUp();
                } else {
                    $("#flyingto").slideDown();
                }
            });
            $(".add-trip-btn").click(function() {
                let self = $(this),
                    _clone = $(self.attr("data-clone")),
                    _append = $(self.attr("data-append"));
                let _groups = $(".multicity-group", _append).length;
           
                if (_groups + 1 < 5) {
                    _clone.clone().appendTo(_append);
                }
            });
            $(".remove-trip-btn").click(function() {
                let _groups = $(".multicity-group", ".multicity-groups");
                if (_groups.length >= 3) {
                    _groups.last().remove();
                }
            });
    
            $('[data-toggle="tab"]').click(function() {
                $(".boottabpane").removeClass("active");
                let _href = $(self).attr("href");
                $(_href).addClass("active");
            });
    
    
    

    
    }
    $scope.tokenresult = JSON.parse(sessionStorage.getItem('authToken'));
    var tokenname = $scope.tokenresult.data;
    var headersOptions = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + $scope.tokenresult
    }
    
    getcode =  function(airport){
       return airport.split('(')[1].split(')')[0];
    }

	 $scope.searchFlightObj = JSON.parse(sessionStorage.getItem('flightSearchRQ'));
     if($scope.searchFlightObj.frmAirport != undefined)
         $scope.searchFlightObj.frmAirport = getcode($scope.searchFlightObj.frmAirport);
    else
        $scope.searchFlightObj.frmAirport = getcode($scope.searchFlightObj.multicityInfos[0].frmAirport);


     if($scope.searchFlightObj.frmAirport != undefined){
        if($scope.searchFlightObj.toAirport)
            $scope.searchFlightObj.toAirport = getcode($scope.searchFlightObj.toAirport);
     }else{
        $scope.searchFlightObj.toAirport = getcode($scope.searchFlightObj.multicityInfos[1].toAirport);
     }

	 $scope.artloading = true;
     
	        $http.post(ServerService.dropDownPath + 'customer/'+ $scope.corprateCompanyId + '/flight/availability', JSON.stringify($scope.searchFlightObj), {
    
            headers: headersOptions
        }).then(function successCallback(response) {
            
        $scope.loading = false;
        $scope.disabled = false;
        if (response.data.status == 'success') {
            // sessionStorage.setItem('flightSearchRS', JSON.stringify(response.data));

               $scope.artloading = false;
    $scope.loading = false;
    $scope.disabled = false;
    // $scope.result = JSON.parse(sessionStorage.getItem('flightSearchRS'));
    $scope.result = response.data;
    sessionStorage.setItem('flightSessionId', JSON.stringify(response.data.sessionId));
    
    // $scope.flightSearchbind = JSON.parse(sessionStorage.getItem('flightSearchRQ'));
    $scope.flightSearchRQ = JSON.parse(sessionStorage.getItem('flightSearchRQ'));
    $scope.quotationRequest = JSON.parse(sessionStorage.getItem('requestFrame'));
    $scope.isAllStopsSelected = true;
    $scope.isAllAirLinesSelected = true;
    $scope.flightResultList = $scope.result.data;

sessionStorage.setItem("ispolicyApplied", $scope.flightResultList.isPolicyAvailable);

    $scope.flightResultList = $scope.result.data;
    $scope.additionaldata = $scope.flightResultList.additionalProperties;

 

    $scope.flightSearchResults = $scope.flightResultList.flightSearchResults;
    $scope.firstFlightRes=$scope.flightSearchResults[0];
    sessionStorage.setItem('resultsFirst', JSON.stringify($scope.firstFlightRes));
    sessionStorage.setItem('firstFlightResponse', JSON.stringify($scope.firstFlightRes.segGrpList[0]));
    var searchflights = $scope.flightResultList.flightSearchResults;
    // $scope.loopFlightHotelResults($scope.flightSearchResults);

    sessionStorage.removeItem("flightFilterData");



    /* 	var fareRequest = new Object();		
      fareRequest.provider = $scope.provider ;
      fareRequest.key = $scope.farekey;
      var fareRequestJSON = JSON.stringify(fareRequest); */
    $scope.loading = true;
    $scope.disabled = true;



    $scope.flightResults = {}
    $scope.flightResults.flightSearchResults = [];
    if($scope.flightResultList.filterParams!=null){
    $scope.flightResultList.filterParams.stopsBeanList.forEach(stoplst => {
        stoplst.selected = true;
    });

    $scope.flightResultList.filterParams.airlineBeanList.forEach(stoplst => {
        stoplst.selected = true;
    });
    var priceStart = $scope.flightResultList.filterParams.priceBean.minPrice;
    var priceEnd = $scope.flightResultList.filterParams.priceBean.maxPrice;
    $scope.filtermodel = {};
    $scope.filtermodel.sessionId =  $scope.result.sessionId;

    if($scope.flightResultList.filterParams.priceBean.minPrice && $scope.flightResultList.filterParams.priceBean.maxPrice)
    $scope.ispricethr = true;
    else
    $scope.ispricethr = false;
    if($scope.flightResultList.filterParams.deptTimeBean){
    if($scope.flightResultList.filterParams.deptTimeBean.maxDeparture && $scope.flightResultList.filterParams.deptTimeBean.minDeparture)
    $scope.isdepthr = true;
    else
    $scope.isdepthr = false;

    if($scope.flightResultList.filterParams.arrivalTimeBean.maxArrival && $scope.flightResultList.filterParams.arrivalTimeBean.minArrival)
    $scope.isarrthr = true;
    else
    $scope.isarrthr = false;
}else{
    $scope.isarrthr = false;
}
    // if($scope.)
    $scope.filtermodel.selectedBound = "Outbound";
    $scope.filtermodel.isOnlySorting = false;
    $scope.filtermodel.priceBean = { fromRange: priceStart, toRange: priceEnd };

    $scope.filtermodel.fareType = { refundable: true, nonRefundable: true }
    $scope.filtermodel.baggageBean = { withBaggage: true, withOutBaggage: true }

    $scope.filtermodel.arrivalTimeBean = { "fromArrival": $scope.flightResultList.filterParams.arrivalTimeBean.minArrival, "toArrival": $scope.flightResultList.filterParams.arrivalTimeBean.maxArrival };
    $scope.filtermodel.deptTimeBean = { "fromDept": $scope.flightResultList.filterParams.deptTimeBean.minDeparture, "toDept": $scope.flightResultList.filterParams.deptTimeBean.maxDeparture };

    $scope.filtermodel.durationBean = { "fromDuration": $scope.flightResultList.filterParams.durationBean.minDuration, "toDuration": $scope.flightResultList.filterParams.durationBean.maxDuration };

    $scope.filtermodel.airlineBeanList = [];

    $scope.filtermodel.stopsBeanList = [];
    $scope.filtermodel.providerBeanList = [];
	
    $scope.checkAll = false;
    $scope.filter1 = [];
    $scope.sortItem = "PRICE,ASC";
    var sortList = $scope.sortItem.split(",");
    $scope.filtermodel.defaultSortType = sortList[0];
    $scope.filtermodel.defaultSortOrder = sortList[1];
	    var airlineCode;
    var stopIndex;
    $scope.airlineFilter;
    var filterDatas = {}


     for (var index in $scope.flightResultList.filterParams.airlineBeanList) {
         $scope.filtermodel.airlineBeanList.push(

             $scope.flightResultList.filterParams.airlineBeanList[index].airlineCode,


         );
     }
     for (var index in $scope.flightResultList.filterParams.providerBeanList) {
         $scope.filtermodel.providerBeanList.push(

           $scope.flightResultList.filterParams.providerBeanList[index].code,


        );
    }

     for (var index in $scope.flightResultList.filterParams.stopsBeanList) {
         $scope.filtermodel.stopsBeanList.push(

             $scope.flightResultList.filterParams.stopsBeanList[index].stopCode,


       );
     }
    }
	     if ($scope.result.data.deptDate !== null) {
        // $scope.getflightData = sessionStorage.getItem("flightSearchRS");
        $scope.searchresults =  $scope.result.data;
        $scope.currency = 'AED';
        var currency = $scope.currency;

        $scope.fromAirport = $scope.searchresults.frmAirprt;
        var fromAirport = $scope.fromAirport;
        var fromAirportSplt = fromAirport.split(',');
        $scope.fromAirportSplt = fromAirportSplt[0];
        $scope.toAirport = $scope.searchresults.toAirprt;
        $scope.departureDate = $scope.searchresults.deptDate;
        $scope.returnDate = $scope.searchresults.arrDate;
        var toAirport = $scope.toAirport;
        var toAirportSplt = toAirport.split(',');
        $scope.toAirportSplt = toAirportSplt[0];
        $scope.hideCity = false;
    } else {
        $scope.city = JSON.parse(sessionStorage.getItem('flightSearchRQ'))
            //    $scope.city = tmp.multicityInfos;
        $scope.hideCity = true;
    }
        $(".js-range-slider").ionRangeSlider({
            type: "double",
            min: priceStart,
            max: priceEnd,
            from: priceStart,
            to: priceEnd,
            grid: true,
            prefix: "AED ",
            onFinish: function(data) {
                var fromRange = data.from_pretty.replace(" ", "");
                var toRange = data.to_pretty.replace(" ", "");
                $scope.filtermodel.priceBean.fromRange = fromRange;
                $scope.filtermodel.priceBean.toRange = toRange;
                $scope.filterParam();
            }
        });

        var start = moment("2016-10-02 00:00", "YYYY-MM-DD HH:mm");
        var end = moment("2016-10-02 23:59", "YYYY-MM-DD HH:mm");

        // DEPT FILTER
        $(".departure-range-slider").ionRangeSlider({
            type: "double",
            grid: true,
            min: start.format("x"),
            max: end.format("x"),
            step: 1800000,
            prettify: function(num) {

                return moment(num, "x").format("HH:mm:ss");
            },
            onFinish: function(data) {
                var fromtimeArr = data.from_pretty.split(':');

                var fromtimeInMilliseconds = (fromtimeArr[0] * 3600000) + (fromtimeArr[1] * 60000);
                var totimeArr = data.to_pretty.split(':');

                var totimeInMilliseconds = (totimeArr[0] * 3600000) + (totimeArr[1] * 60000);

                $scope.filtermodel.deptTimeBean.fromDept = fromtimeInMilliseconds; // data.from_pretty;
                $scope.filtermodel.deptTimeBean.toDept = totimeInMilliseconds; //data.to_pretty;
                $scope.filterParam();
            }
        });
        // ARRIVAL FILTER
        $(".arrival-range-slider").ionRangeSlider({
            type: "double",
            grid: true,
            min: start.format("x"),
            max: end.format("x"),
            step: 1800000,
            prettify: function(num) {

                return moment(num, "x").format("HH:mm:ss");
            },
            onFinish: function(data) {
                var fromtimeArr = data.from_pretty.split(':');

                var fromtimeInMilliseconds = (fromtimeArr[0] * 3600000) + (fromtimeArr[1] * 60000);
                var totimeArr = data.to_pretty.split(':');

                var totimeInMilliseconds = (totimeArr[0] * 3600000) + (totimeArr[1] * 60000);

                $scope.filtermodel.arrivalTimeBean.fromArrival = fromtimeInMilliseconds; //data.from_pretty;
                $scope.filtermodel.arrivalTimeBean.toArrival = totimeInMilliseconds; //data.to_pretty;
                $scope.filterParam();
            }
        });


        $sidebar = $("#filterSideBar");
        $window = $(window);
        var sidebarOffset = $sidebar.offset();

        // $("#filterSideBar").sticky({ topSpacing: 150 });
        // $("#filterSideBar").on("bottom-reached", function () {
  
        // });

        $(window).scroll(function() {
            var sticky = $("#filterSideBar"),
                scroll = $(window).scrollTop();
            var footerScroll =
                $(".footer").offset().top -
                $(".footer").height() -
                $(".header").height() -
                125;
            if (scroll >= 150 && scroll < footerScroll)
                sticky.addClass("sticky-sidebar");
            if (scroll >= 2320 && scroll < footerScroll)
                sticky.removeClass("sticky-sidebar");
            else sticky.removeClass("sticky-sidebar");
        });

        $('[data-toggle="tab"]').click(function() {
            let _href = $(this).attr("data-flightType");
            if (_href === "oneWay") {
                $("#flyingto").slideUp();
            } else {
                $("#flyingto").slideDown();
            }
        });
        $(".add-trip-btn").click(function() {
            let self = $(this),
                _clone = $(self.attr("data-clone")),
                _append = $(self.attr("data-append"));
            let _groups = $(".multicity-group", _append).length;
       
            if (_groups + 1 < 5) {
                _clone.clone().appendTo(_append);
            }
        });
        $(".remove-trip-btn").click(function() {
            let _groups = $(".multicity-group", ".multicity-groups");
            if (_groups.length >= 3) {
                _groups.last().remove();
            }
        });

        $('[data-toggle="tab"]').click(function() {
            $(".boottabpane").removeClass("active");
            let _href = $(self).attr("href");
            $(_href).addClass("active");
        });




      
	                     $scope.moreOptionFun1();
                    

                    $scope.filterFunction();
                    $scope.artloading = false;
        } else {
$scope.artloading=false;
            Lobibox.alert('error', {
                msg: response.data.errorMessage
            })
            $timeout(function() {
             
                $location.path('/flight')
            }, 1000);
        }

    }, function errorCallback(response) {
        if(response.status == 403){
           var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
           status.then(function(greeting) {
           searchflight();
           });
            // $scope.autoAirlines(value);
        }
        // called asynchronously if an error occurs
        // or server returns response with an error status.
      });



    
    $(document).ready(function(){
        $(window).scrollTop(0);
         $(this).scrollTop(0);
         $('html, body').animate({scrollTop:0},500);
    });
    document.getElementById('amenitiesfilter').style.height = '250px';
    $(window).scroll(function(){ 
        
        if (document.getElementById('amenitiesfilter') !=null && $(this).scrollTop() > 10) { 
            document.getElementById('amenitiesfilter').style.height = '200px';
        }
        if (document.getElementById('amenitiesfilter') !=null && $(this).scrollTop() > 80) { 
            document.getElementById('amenitiesfilter').style.height = '280px';
        }
        if (document.getElementById('amenitiesfilter') !=null && $(this).scrollTop() > 150) { 
            document.getElementById('amenitiesfilter').style.height = '300px';
        }
        if (document.getElementById('amenitiesfilter') !=null && $(this).scrollTop() > 200) { 
            document.getElementById('amenitiesfilter').style.height = '370px';
        }
        //  else { 
        //     document.getElementById('amenitiesfilter').style.height = '250px';
        // } 
    }); 
    

if(sessionStorage.getItem("travellersListByRoom")!=null){
    // sessionStorage.removeItem("travellersListByRoom"); 
}

    var headersOptions = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + $scope.tokenresult .authToken
    }
    sessionStorage.removeItem("selectedInsurancePlanId");

    $scope.monthcnvrt =  function(val){
        return new Date(2014, val-1);
    }

    $scope.stationBind = function(plc){
       var place1 = plc.split("(")[0];
       var place2 = plc.split(",")[1];
       return place1+ ', '+place2;
    }

$scope.isOnly=function(stopCode,indexvalue,filtervalue){
    var i = indexvalue;
  
 


  
   
if(filtervalue=="stopvalue"){
    $scope.filtermodel.stopsBeanList = [];
//     $scope.flightResultList.filterParams.stopsBeanList.forEach(stopBean => {

//         if (stopBean.stopCode === stopCode) {
//             $scope.filtermodel.stopsBeanList.push(stopBean.stopCode);
//             for(var s=0; s< $scope.flightResultList.filterParams.stopsBeanList.length; s++){
// if(s==i){
//     $('#stop_selected_' + s).prop('checked', true);
// }else{
//     $('#stop_selected_' + s).prop('checked', false);
// }
//             }
//         }



//     });
    $scope.flightResultList.filterParams.stopsBeanList.forEach(stoplst => {
        if(stopCode == stoplst.stopCode){
            stoplst.selected = true;
            $scope.filtermodel.stopsBeanList.push(stopCode);
        }
        else
            stoplst.selected = false;
    });
}
if(filtervalue=="airlines"){
    
    $scope.filtermodel.airlineBeanList = [];
    // $scope.flightResultList.filterParams.airlineBeanList.forEach(airlineBean => { //checking with actual list
    //     if (airlineBean.airlineCode === stopCode) {

    //         $scope.filtermodel.airlineBeanList.push(airlineBean.airlineCode);
    //         for(var s=0; s< $scope.flightResultList.filterParams.airlineBeanList.length; s++){
    //             if(s==i){
    //                 $('#airline_selected_' + s).prop('checked', true);
    //             }else{
    //                 $('#airline_selected_' + s).prop('checked', false);
    //             }
    //                         }
    //     }

    // });
    $scope.flightResultList.filterParams.airlineBeanList.forEach(airlineBean => {
        if(stopCode == airlineBean.airlineCode){
            airlineBean.selected = true;
            $scope.filtermodel.airlineBeanList.push(stopCode);
        }
        else
            airlineBean.selected = false;
    });
}
if(filtervalue=="refundablevalue"){
    if(stopCode=='true'){
    $scope.filtermodel.fareType.refundable = true;
    $('#refundable_selected_' + 0).prop('checked', true);
    $scope.filtermodel.fareType.nonRefundable = false;

    $('#nonrefundable_selected_' + 1).prop('checked', false);
    }
}
if(filtervalue=="nonRefundable"){
    if(stopCode=='true'){
    $scope.filtermodel.fareType.nonRefundable = true;
    $('#nonrefundable_selected_' + 1).prop('checked', true);
    $scope.filtermodel.fareType.refundable = false;

    $('#refundable_selected_' + 0).prop('checked', false);
    }
}
if(filtervalue=="withBaggage"){
    if(stopCode=='true'){
    $scope.filtermodel.baggageBean.withBaggage = true;
    $('#withBaggage_selected_' + 0).prop('checked', true);
    $scope.filtermodel.baggageBean.withOutBaggage = false;

    $('#withoutBaggage_selected_' + 1).prop('checked', false);
    }
}
if(filtervalue=="withOutBaggage"){
    if(stopCode=='true'){
    $scope.filtermodel.baggageBean.withOutBaggage = true;
    $('#withoutBaggage_selected_' + 1).prop('checked', true);
    $scope.filtermodel.baggageBean.withBaggage = false;

    $('#withBaggage_selected_' + 0).prop('checked', false);
    }
}




    $scope.tokenresult = JSON.parse(sessionStorage.getItem('authToken'));
    var headersOptions = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + $scope.tokenresult
    }
  
    $scope.flightSearchResults1 = [];
    $scope.flightSearchResults = [];
    $scope.artloading = true;
    $http.post(ServerService.dropDownPath+ 'customer/'+ $scope.corprateCompanyId  + '/flight/filter', $scope.filtermodel, {
        headers: headersOptions
    }).then(function successCallback(response) {
        if (response.data.success) {
          
            $scope.flightSearchResults = response.data.data;
            $scope.firstFlightFilter=$scope.flightSearchResults[0];
             sessionStorage.setItem("flightFilterData", JSON.stringify($scope.firstFlightFilter));
            $scope.showErrorMesg = false;
            $(window).scrollTop(0);
            $(this).scrollTop(0);
            $('html, body').animate({scrollTop:0},500);
            $scope.moreOptionFun1();
            $scope.artloading = false;
    } else {
        $scope.showErrorMesg = true;
        $scope.artloading = false;

     
    }
}, function errorCallback(response) {
    if(response.status == 403){
        var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
        status.then(function(greeting) {
            $scope.isOnly();
           });
    }
   
  });
}

    function formatNumber(n) {
        return Math.round(n);
    }

    function fetchCityOnly(place) {
        var placeSplit = place.split(',');
        var parenthSplt = placeSplit[0].split("(");
        return parenthSplt[0];
    }

    function flydur(n1, n2) {
        return parseInt((parseInt(n1) + parseInt(n2)) / 60);
    }

    function flypos(n1, n2) {
        var returnvalue = parseInt((parseInt(n1) + parseInt(n2)) % 60);
        if (returnvalue > 9) {
            return returnvalue;
        } else {
            return "0" + returnvalue;
        }
    }
   

    $scope.flightSearchResults1 = [];

    $scope.convertDt = function(convertDt) {
            var oneR = convertDt;
            var yr = oneR.substring(0, 4);
            var month = oneR.substring(4, 6);
            var date = oneR.substring(6, 8);
            return yr + '-' + month + '-' + date;
        }
        // $scope.showLess=true;

    $scope.showResultGroup = function(index,clientRefNo) {
        
        

            $scope.showMore[index] = !$scope.showMore[index];
            $scope.scrollTraget = index;
            var scroll = $(window).scrollTop();
            // console.log(scroll);
            $scope.scrollTraget = scroll;
        }
        $scope.showResultGroupHide = function(index) {
            $scope.showMore[index] = !$scope.showMore[index];
            $('html, body').animate({scrollTop:$scope.scrollTraget},500);
            // $('html,body').animate({
            //         scrollTop: $("#scrolltop"+index).offset().top},
            //         'slow');
        }
        // $scope.showLess=true;
 
    $scope.sort = function(sortItem) {
            var sortList = sortItem.split(",");
            $scope.filtermodel.defaultSortType = sortList[0];
            $scope.filtermodel.defaultSortOrder = sortList[1];
            $scope.filterParam();
        }
        //STOP'S FILTER
    var stopsCount = 0;
    $scope.isStopSelected = true;
     $scope.isSelected = true;
    // $scope.option.selected=true;
$scope.clearFilter=function(){


    $scope.flightResults = {}
    $scope.flightResults.flightSearchResults = [];
    var priceStart = $scope.flightResultList.filterParams.priceBean.minPrice;
    var priceEnd = $scope.flightResultList.filterParams.priceBean.maxPrice;
    $scope.filtermodel = {};
    $scope.filtermodel.sessionId =  $scope.result.sessionId;

    var instance = $(".js-range-slider").data("ionRangeSlider");

    instance.update({
        min: priceStart,
        max: priceEnd,
        from: priceStart,
        to: priceEnd,
    });
   
 
    // if($scope.)
   
    for(var s=0; s< $scope.flightResultList.filterParams.stopsBeanList.length; s++){

        $scope.flightResultList.filterParams.stopsBeanList[s].selected = false;


$('#stop_selected_' + s).prop('checked', false);

    }
    for(var s=0; s< $scope.flightResultList.filterParams.airlineBeanList.length; s++){
       
        $scope.flightResultList.filterParams.airlineBeanList[s].selected = false;
          
       
            $('#airline_selected_' + s).prop('checked', false);
        
                    }
    $('#refundable_selected_' + 0).prop('checked', false);
    

    $('#nonrefundable_selected_' + 1).prop('checked', false);
    $('#withBaggage_selected_' + 0).prop('checked', false);


    $('#withoutBaggage_selected_' + 1).prop('checked', false);
    $scope.filtermodel.selectedBound = "Outbound";
    $scope.filtermodel.isOnlySorting = false;
    $scope.filtermodel.priceBean = { fromRange: priceStart, toRange: priceEnd };

    $scope.filtermodel.fareType = { refundable: true, nonRefundable: true }
    $scope.filtermodel.baggageBean = { withBaggage: true, withOutBaggage: true }

    // $scope.filtermodel.arrivalTimeBean = { "fromArrival": 0 * 60000, "toArrival": 1439 * 60000 };
    // $scope.filtermodel.deptTimeBean = { "fromDept": 0 * 60000, "toDept": 1439 * 60000 };

    // $scope.filtermodel.durationBean = { "fromDuration": 0, "toDuration": 950 };
    $scope.filtermodel.arrivalTimeBean = { "fromArrival": $scope.flightResultList.filterParams.arrivalTimeBean.minArrival, "toArrival": $scope.flightResultList.filterParams.arrivalTimeBean.maxArrival };
    $scope.filtermodel.deptTimeBean = { "fromDept": $scope.flightResultList.filterParams.deptTimeBean.minDeparture, "toDept": $scope.flightResultList.filterParams.deptTimeBean.maxDeparture };

    $scope.filtermodel.airlineBeanList = [];

    $scope.filtermodel.stopsBeanList = [];
    $scope.filtermodel.providerBeanList = [];
    $scope.filtermodel.defaultSortType="PRICE";
    $scope.filtermodel.defaultSortOrder="ASC";
  

        for (var index in $scope.flightResultList.filterParams.providerBeanList) {
            $scope.filtermodel.providerBeanList.push(
   
              $scope.flightResultList.filterParams.providerBeanList[index].code,
   
   
           );
       }
        $scope.filtermodel.providerBeanList = [];
        $scope.showErrorMesg = true;
        $scope.artloading = false;
        $scope.flightSearchResults =  $scope.flightResultList.flightSearchResults;
        $scope.firstFlightFilter=$scope.flightSearchResults[0];
        sessionStorage.setItem("flightFilterData", JSON.stringify($scope.firstFlightFilter));
        $scope.showErrorMesg = false;
        $scope.refundable = false;
        $scope.nonRefundable = false;
        
     $scope.withBaggage = false;
     $scope.withOutBaggage = false;
     $scope.isStopSelected = false;
     $scope.isSelected = false;
        $(window).scrollTop(0);
        $(this).scrollTop(0);
        $('html, body').animate({scrollTop:0},500);
        $scope.moreOptionFun1();
        $scope.artloading = false;
        var start = moment("2016-10-02 00:00", "YYYY-MM-DD HH:mm");
        var end = moment("2016-10-02 23:59", "YYYY-MM-DD HH:mm");


        var depinstance = $(".departure-range-slider").data("ionRangeSlider");
    
        depinstance.update({
            min: start.format("x"),
            max: end.format("x"),
            from: start.format("x"),
        to: end.format("x"),
            step: 1800000,
            prettify: function(num) {

                return moment(num, "x").format("HH:mm:ss");
            },
        });
    
        var arrinstance = $(".arrival-range-slider").data("ionRangeSlider");
    
        arrinstance.update({
            min: start.format("x"),
            max: end.format("x"),
            from: start.format("x"),
        to:end.format("x"),
            step: 1800000,
            prettify: function(num) {

                return moment(num, "x").format("HH:mm:ss");
            },
        });
        $scope.filterParam();
        console.log($scope.filtermodel)
        $scope.withBaggage = false;
        $scope.withOutBaggage = false;
        $scope.refundable = false;
        $scope.nonRefundable = false;
}

    $scope.stopsChange = function(stopCode, isSelected) {
        $scope.filtermodel.stopsBeanList = [];
        $scope.flightResultList.filterParams.stopsBeanList.forEach(stopBean => {

            if (stopBean.selected) {
                $scope.filtermodel.stopsBeanList.push(stopBean.stopCode);
            }

        });

            // if (isSelected) {
            //     // if (stopsCount == 0) {
            //     //     $scope.filtermodel.stopsBeanList = [];
            //     //     stopsCount++;
            //     // }

            //     $scope.flightResultList.filterParams.stopsBeanList.forEach(stopBean => {

            //         if (stopBean.stopCode === stopCode) {
            //             $scope.filtermodel.stopsBeanList.push(stopBean.stopCode);
            //         }



            //     });
            // } else {
            //     var i = 0;
            //     $scope.filtermodel.stopsBeanList.forEach(existedStopCode => {
            //         if (existedStopCode === stopCode) {
            //             $scope.filtermodel.stopsBeanList.splice(i, 1);
            //         }
            //         i++;
            //     });
            // }

            // if ($scope.filtermodel.stopsBeanList.length == $scope.flightResultList.filterParams.stopsBeanList.length)
            //     $scope.isAllStopsSelected = true;
            // else
            //     $scope.isAllStopsSelected = false;
            $scope.filterParam();

        }
        // FARE TYPE FILTER

     $scope.refundable = true;
     $scope.nonRefundable = true;

    $scope.fareType = function(refundable, nonRefundable) {

            // if (!refundable)
            //     $scope.filtermodel.fareType.refundable = false;
            // else
        $scope.filtermodel.fareType.nonRefundable = nonRefundable;
        $scope.filtermodel.fareType.refundable = refundable;
            $scope.filterParam();
    }
   
    
  

    $scope.addQuotations=function(quotation,checkedvalue,uniqueValue,event){
      
        $scope.quotationGroup = JSON.parse(sessionStorage.getItem('sendQuoteData'));
        if($scope.quotationGroup==null){
            $scope.quotationGroup=[];
        }
        if(event.currentTarget.checked){
            if($scope.searchFlightObj.srchType==1){
                document.getElementById('myCheck_' +quotation.clientRefNo).checked = true; 
            }else{
                document.getElementById('moreoptions_' +quotation.clientRefNo).checked = true;
                document.getElementById('myCheck_' +quotation.clientRefNo).checked = true;  
            }
         
            $scope.fareDetails(quotation)
            
        
            
           
          
        }else{
           
            for(a=0; a< $scope.quotationGroup.length; a++){
                if($scope.quotationGroup[a].clientRefNo==uniqueValue){
                $scope.quotationGroup.splice(a, 1); 
                }
            }
            // document.getElementById('moreoptions_' +quotation.clientRefNo).checked = false; 
            if($scope.searchFlightObj.srchType==1){
                document.getElementById('myCheck_' +quotation.clientRefNo).checked = false;
                document.getElementById('moreoptions_' +quotation.clientRefNo).checked = false; 
            }else{
                document.getElementById('moreoptions_' +quotation.clientRefNo).checked = false; 
            } 
           

        }
         
    } 

    $scope.nonfareType = function(refundable, nonRefundable) {

        $scope.filtermodel.fareType.nonRefundable = nonRefundable;
        $scope.filtermodel.fareType.refundable = refundable;

        $scope.filterParam();
    }
        //BAGGAGE FILTERS

     $scope.withBaggage = true;
     $scope.withOutBaggage = true;

    $scope.baggage = function(withBaggage, withOutBaggage) {

        // if(!withBaggage &&)
        // if (withBaggage == null)
        //     $scope.filtermodel.baggageBean.withBaggage = false;
        // else
        $scope.filtermodel.baggageBean.withBaggage = $scope.withBaggage;
        $scope.filtermodel.baggageBean.withOutBaggage = $scope.withOutBaggage;

        $scope.filterParam();
    }
    $scope.withoutbaggagefun = function(withBaggage, withOutBaggage) {

        $scope.filtermodel.baggageBean.withOutBaggage = $scope.withOutBaggage;
        $scope.filtermodel.baggageBean.withBaggage = $scope.withBaggage;

        $scope.filterParam();
    }

    // AIRLINE FILTERS
    var airlineCount = 0;
    $scope.airlineFilter = function(airlineCode, isSelected) {
        $scope.filtermodel.airlineBeanList = [];
        $scope.flightResultList.filterParams.airlineBeanList.forEach(airlineBean => {

            if (airlineBean.selected) {
                $scope.filtermodel.airlineBeanList.push(airlineBean.airlineCode);
            }

        });
        // if (!isSelected) {
        //     // if (airlineCount == 0) {
        //     //     $scope.filtermodel.airlineBeanList = [];
        //     //     airlineCount++;
        //     // }
        //     $scope.flightResultList.filterParams.airlineBeanList.forEach(airlineBean => { //checking with actual list
        //         if (airlineBean.airlineCode === airlineCode) {

        //             $scope.filtermodel.airlineBeanList.push(airlineBean.airlineCode);
        //         }

        //     });
        // } else {
        //     var i = 0;
        //     $scope.filtermodel.airlineBeanList.forEach(existedAirlineCode => {
        //         if (existedAirlineCode === airlineCode) {
        //             $scope.filtermodel.airlineBeanList.splice(i, 1);
        //         }
        //         i++;
        //     });
        // }

        $scope.filterParam();
    }

    //PRICE FILTER

    function refreshpage() {
        // The last "domLoading" Time //
        var currentDocumentTimestamp =
            new Date(performance.timing.domLoading).getTime();
        // Current Time //
        var now = Date.now();
        // Ten Seconds //
        var tenSec = 10 * 1000;
        // Plus Ten Seconds //
        var plusTenSec = currentDocumentTimestamp + tenSec;
        if (now > plusTenSec) {
            location.reload();
        } else {}
    }

    $(".refine-search").click(function() {
        $(".flight-search").slideToggle();
    });
    $(".refine-search-close").click(function() {
        $(".flight-search").slideUp();
    });

    $(".only-link").click(function(event) {
        event.preventDefault();
        let parent = $(this).closest(".checkbox-container").parent();
        let currentCb = $(this).closest(".checkbox-label");
        $("input", parent).prop("checked", false);
        $("input", currentCb).prop("checked", true);
    });

  
       $scope.showErrorMesg = false;
        $scope.filterParam = function() {
            $scope.tokenresult = JSON.parse(sessionStorage.getItem('authToken'));
            var headersOptions = {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + $scope.tokenresult
            }
        
            $scope.flightSearchResults1 = [];
            $scope.flightSearchResults = [];
            $scope.artloading = true;
            $http.post(ServerService.dropDownPath+ 'customer/'+ $scope.corprateCompanyId  + '/flight/filter', $scope.filtermodel, {
                headers: headersOptions
            }).then(function successCallback(response) {
                if (response.data.success) {
                    $scope.flightSearchResults = response.data.data;
                    $scope.firstFlightFilter=$scope.flightSearchResults[0];
                     sessionStorage.setItem("flightFilterData", JSON.stringify($scope.firstFlightFilter));
                    $scope.showErrorMesg = false;
                    $scope.moreOptionFun1();
                    $scope.artloading = false;
            } else {
                $scope.showErrorMesg = true;
                $scope.artloading = false;

                //       if(response.data.error=='001'){
                //         $scope.searchFlightObj = JSON.parse(sessionStorage.getItem('flightSearchRQ'));
                // /* var Serverurl = "http://localhost:8090/ibp/api/v1/"; */
                // $scope.tokenresult = JSON.parse(sessionStorage.getItem('authentication'));
                // var tokenname = $scope.tokenresult.data;
                // var headersOptions = {
                //     'Content-Type': 'application/json',
                //     'Authorization': 'Bearer ' + tokenname.authToken
                // }

                // $http.post(ServerService.serverPath + 'flight/availability', JSON.stringify($scope.searchFlightObj), {

                //     headers: headersOptions
                // }).then(function (response) {
                //     $scope.loading = false;
                //     $scope.disabled = false;
                //     if (response.data.status == 'success') {
                //         sessionStorage.setItem('flightSearchRS', JSON.stringify(response.data));

                //       	$location.path('/flightResult');
                //     } else {

                //         Lobibox.alert('error', {
                //             msg: response.data.errorMessage
                //         })
                //         $timeout(function () {
                //          	$location.path('/flight')
                //         }, 1000);
                //     }

                // });


                //       }
            }
        }, function errorCallback(response) {
            if(response.status == 403){
                var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
                status.then(function(greeting) {
                    $scope.filterParam();
                   });
            }
            // called asynchronously if an error occurs
            // or server returns response with an error status.
          });
    }
    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.flightBook = function(flightInfo) {
        $scope.tokenresult = JSON.parse(sessionStorage.getItem('authToken'));
        var headersOptions = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + $scope.tokenresult
        }
  
        sessionStorage.setItem('flightindex', JSON.stringify(flightInfo.indx));

        var repriceDetails = {
            "sessionId":  $scope.result.sessionId,
            "resultIndexId": flightInfo.indx
            
        }


        $scope.artloading = true;
        $http.post(ServerService.dropDownPath+ 'customer/'+ $scope.corprateCompanyId  + '/flight/reprice', repriceDetails, {

            headers: headersOptions
        }).then(function successCallback(response) {
            if(response.data.success){

            $scope.artloading = false;
         
            $scope.flightPriceData = response.data.data
            sessionStorage.setItem('flightPriceData', JSON.stringify(response.data));
            // sessionStorage.setItem('flightPriceData', JSON.stringify($scope.flightPriceData));

            $location.path('/flightDetails');

        }else{
            $scope.artloading = false;
                     Lobibox.alert('error', {
                            msg: response.data.errorMessage
                        })
        }
    }, function errorCallback(response) {
        if(response.status == 403){
            var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
            status.then(function(greeting) {
                $scope.flightBook(flightInfo);
            });
        }
        // called asynchronously if an error occurs
        // or server returns response with an error status.
      });
    }


    $scope.totalFare = function(size, parentSelector, index) {
        var parentElem = parentSelector ?
            angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
        $rootScope.selectedFareIndex = index;
        $scope.animationEnabled = true;
        var modalInstance = $uibModal.open({
            animation: $scope.animationEnabled,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'flightFareTotal.html',
            controller: 'flightFaretotalControllers',
            backdrop: true,
            size: size,
            appendTo: parentElem,
            resolve: {
                items: function() {
                    return $scope.items
                }
            }
        })
    }

    $scope.eachFlightQuotation = function(size, parentSelector, FlightDetails) {
        var parentElem = parentSelector ?
            angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
        $rootScope.selectedIndex = FlightDetails.indx;
        $rootScope.selectedFlightDetails = FlightDetails;
    

        $scope.animationEnabled = true;
        var modalInstance = $uibModal.open({
            animation: $scope.animationEnabled,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'flightQuotation.html',
            controller: 'flightQuotationControllerss',
            backdrop: true,
            size: size,
            appendTo: parentElem,
            resolve: {
                items: function() {
                    return $scope.items
                }
            }
        })
    }


    $scope.flightBaggageDetails = function(size, parentSelector, BagDetails) {
        var parentElem = parentSelector ?
            angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
        $rootScope.selectedBagIndex = BagDetails.indx;
        $rootScope.selectedBagDetails = BagDetails;

        $scope.animationEnabled = true;
        var modalInstance = $uibModal.open({
            animation: $scope.animationEnabled,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'flightBaggageInfo.html',
            controller: 'flightbaggageinfoController',
            backdrop: true,
            size: size,
            appendTo: parentElem,
            resolve: {
                items: function() {
                    return $scope.items
                }
            }
        })
    }

    $scope.flightBaggageDetailsInclusions = function(size, parentSelector, BagDetails) {

        var parentElem = parentSelector ?
        angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
    $rootScope.selectedBagIndex = BagDetails.indx;
    $rootScope.selectedBagDetails = BagDetails;
    
    $scope.animationEnabled = true;
    var modalInstance = $uibModal.open({
        animation: $scope.animationEnabled,
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: 'flightBaggageInclusions.html',
        controller: 'flightbaggageinfoController',
        backdrop: true,
        size: size,
        appendTo: parentElem,
        resolve: {
            items: function() {
                return $scope.items
            }
        }
    })
    
    }
    $scope.miniFareRules = function(size, parentSelector, index) {
        var parentElem = parentSelector ?
            angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
        $rootScope.selectedFareIndex = index;
        $scope.animationEnabled = true;
        var modalInstance = $uibModal.open({
            animation: $scope.animationEnabled,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'flightFareinfoContent.html',
            controller: 'flightFareinfoControllers',
            backdrop: true,
            size: size,
            appendTo: parentElem,
            resolve: {
                items: function() {
                    return $scope.items
                }
            }
        })
    }

    $scope.eachFlightDetails = function(size, parentSelector, FlightDetails) {
        var parentElem = parentSelector ?
            angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
        $rootScope.selectedIndex = FlightDetails.indx;
        $rootScope.selectedFlightDetails = FlightDetails;
    

        $scope.animationEnabled = true;
        var modalInstance = $uibModal.open({
            animation: $scope.animationEnabled,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'flightDetailedinfoContent.html',
            controller: 'flightDetailedinformationContent',
            backdrop: true,
            size: size,
            appendTo: parentElem,
            resolve: {
                items: function() {
                    return $scope.items
                }
            }
        })
    }


    $scope.outPolicyCheck = function(size, parentSelector, FlightDetails) {
        var parentElem = parentSelector ?
            angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
        $rootScope.selectedIndex = FlightDetails.indx;
         $rootScope.selectedFlightDetails = FlightDetails;
    

        $scope.animationEnabled = true;
        var modalInstance = $uibModal.open({
            animation: $scope.animationEnabled,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'outPolicy.html',
            controller: 'outPolicyControllerFlight',
            backdrop: true,
            size: size,
            appendTo: parentElem,
            resolve: {
                items: function() {
                    return $scope.items
                }
            }
        })
    }

  
    $scope.addTravel = function(checked, singlePsgr, age, indexvalue,uniqueValue) {

      
if(checked){

}else{

}
    }
});

ouaApp.controller('flightQuotationControllerss', function($scope, $http,ServerService,$uibModal, $uibModalInstance, $rootScope) {
    $scope.searchFlightObj = JSON.parse(sessionStorage.getItem('flightSearchRQ'));
    $scope.cordinatorDetails={};
    $scope.isPolicyApplied=sessionStorage.ispolicyApplied;
    $scope.cordinatorDetails = JSON.parse(sessionStorage.getItem('cordinatorDetails'));
    if($scope.cordinatorDetails!=null){
    $scope.cordinatorDetails.email=$scope.cordinatorDetails.coordinatorEmail
    }
    $scope.currency = "AED";
    $scope.farerule = 'test';
    $scope.monthcnvrt =  function(val){
        return new Date(2014, val-1);
    }
    var selectTraveller = new Map();
    $scope.addTravel = function(checked, singlePsgr, age, indexvalue,uniqueValue) {

      
        if(checked){
            selectTraveller.set(singlePsgr.customerId,singlePsgr);
            $scope.selectTravellerData=Array.from(selectTraveller.values());
      
            sessionStorage.setItem('selectTraveldata', JSON.stringify($scope.selectTravellerData));
        }else{
            selectTraveller.delete(singlePsgr.customerId)
            $scope.selectTravellerData=Array.from(selectTraveller.values());
            sessionStorage.setItem("selectTraveldata", JSON.stringify($scope.selectTravellerData));
        }
            }

            $scope.ConfirmTravellerData=function(){
                $scope.travelData = JSON.parse(sessionStorage.getItem('selectTraveldata'))
                if($scope.travelData!=null ){
       $scope.selectTraveldata=  $scope.travelData;
            Lobibox.notify('success', {
                size: 'mini',
                position: 'top right',
                msg: "Travellers  added successfully!",
                delay: 1500
            });
            $scope.modalInstance.dismiss();
       
                } else{
                    Lobibox.notify('error', {
                        size: 'mini',
                        position: 'top right',
                        msg:"Please select at least one traveller!",
                        delay: 1500
                    });
                }
        
            }
    
            $scope.flightQuotation={};
    $scope.markup=function(quatationsegmentGroup,indexvalue) {
        $scope.quotationGroup = JSON.parse(sessionStorage.getItem('sendQuoteData'))
        $scope.quatationsegmentGroup=quatationsegmentGroup
        var markup=document.getElementById('markupValue_'+indexvalue).value; 
        if(markup==''){
            markup=0;    
        }
        var discount=document.getElementById('discountValue_'+indexvalue).value; 
        if(discount==''){
            discount=0;    
        }
      
         $scope.quotationGroup[indexvalue].markUp=parseInt(markup);
         $scope.quotationGroup[indexvalue].discnt=parseInt(discount);
      
        $scope.quatationsegmentGroup.grndTotal=(quatationsegmentGroup.ftotFare + parseInt(markup) - parseInt(discount))
        $scope.quotationGroup[indexvalue].grndTotal= $scope.quatationsegmentGroup.grndTotal;
        sessionStorage.setItem("sendQuoteData", JSON.stringify($scope.quotationGroup));
        // if(markup!=''){
        // $scope.quotationFare=(quatationsegmentGroup.grndTotal + parseInt(markup))
        // $scope.quatationsegmentGroup.grndTotal=$scope.quotationFare
        // }else{
        //     $scope.quatationsegmentGroup.grndTotal=quatationsegmentGroup.grndTotal;
        // }
     }
    //  $scope.discount=function(quatationsegmentGroup) {
    //     $scope.quatationsegmentGroup=quatationsegmentGroup
    //     var markup=document.getElementById('markupValue').value; 
    //     if(markup==''){
    //         markup=0;    
    //     }
    //     var discount=document.getElementById('discountValue').value; 
    //     $scope.quatationsegmentGroup.grndTotal=(quatationsegmentGroup.totFare - parseInt(discount))
      
    //  }
    $("#trvl").addClass("active");
    $scope.eachFlightDetails = function(index, FlightDetails,childIndex) {
      
        $scope.flightDetailsQuotation[index].segGrpList[childIndex].isOpen = ! $scope.flightDetailsQuotation[index].segGrpList[childIndex].isOpen;
        $scope.scrollTraget = index;
        $scope.flightDetails1 = FlightDetails;
        $scope.segGrpListchildIndex = childIndex;

    }
    $scope.miniFareRules = function(size, parentSelector, index) {
        var parentElem = parentSelector ?
            angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
        $rootScope.selectedFareIndex = index;
        $scope.animationEnabled = true;
        var modalInstance = $uibModal.open({
            animation: $scope.animationEnabled,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'flightFareinfoContent.html',
            controller: 'flightFareinfoControllers',
            backdrop: true,
            size: size,
            appendTo: parentElem,
            resolve: {
                items: function() {
                    return $scope.items
                }
            }
        })
    }
    $scope.outPolicyCheck = function(size, parentSelector, FlightDetails) {
        var parentElem = parentSelector ?
            angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
        $rootScope.selectedIndex = FlightDetails[0].indx;
         $rootScope.selectedFlightDetails = FlightDetails[0];
    

        $scope.animationEnabled = true;
        var modalInstance = $uibModal.open({
            animation: $scope.animationEnabled,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'outPolicy.html',
            controller: 'outPolicyControllerFlight',
            backdrop: true,
            size: size,
            appendTo: parentElem,
            resolve: {
                items: function() {
                    return $scope.items
                }
            }
        })
    }

    $scope.allOpen = true;
    $scope.searchFlightObj = JSON.parse(sessionStorage.getItem('flightSearchRQ'));
    $scope.searchFlightObj.isPolicyAvailable=$scope.isPolicyApplied

    $scope.quotationGroup = JSON.parse(sessionStorage.getItem('sendQuoteData'))
    $scope.selectTraveldata = JSON.parse(sessionStorage.getItem('selectTraveldata'))
    $scope.flightDetailsQuotation= $scope.quotationGroup;
    if(  $scope.flightDetailsQuotation !=null){
    for(var a=0; a<$scope.flightDetailsQuotation.length; a++){
        $scope.flightDetailsQuotation[a].ftotFare=$scope.flightDetailsQuotation[a].grndTotal;
    }
}
    if($scope.selectTraveldata==null){
        $scope.travelFunctionality=true;
    }else{
        $scope.travelFunctionality=false;
    }
 
    $scope.initvalue = function(){
        $scope.flightDetailsQuotation.forEach(Qelement => {
            Qelement.segGrpList.forEach(element => {
                element.isOpen = false;
            });
            
        });
    }

    
if($scope.flightDetailsQuotation !=null){
        $scope.initvalue();
}
        $scope.quotationRemove=function(removeQuotation,indexvalue,parentIndex){
            
           document.getElementById("myCheck_" +removeQuotation.clientRefNo).checked = false;
         document.getElementById('moreoptions_' +removeQuotation.clientRefNo).checked = false;
            //  document.getElementById("myCheck").checked = false;
      
            $scope.quotationGroup = JSON.parse(sessionStorage.getItem('sendQuoteData'))
            for(a=0; a< $scope.quotationGroup.length; a++){
                if($scope.quotationGroup[a].clientRefNo==removeQuotation.clientRefNo){
                    // $('#FlightIndex_' + $scope.segmentGroup[a].clientRefNo).prop('checked', false);
                    $scope.quotationGroup.splice(a, 1);
                    sessionStorage.setItem("sendQuoteData", JSON.stringify($scope.quotationGroup));
                    $scope.flightDetailsQuotation= $scope.quotationGroup;
                    $scope.initvalue();
                    break;
                }
            }
        }
$scope.quotationReq={};
$scope.allServiceDetails={};
$scope.quotationData={};
$scope.flightObj={};
$scope.hotelObj={};
$scope.insObj={};
$scope.transferObj={};
$scope.railObj={};
$scope.quotationDetails={};
$scope.hotelQuotation=[];
$scope.insQuotation=[];
$scope.trsnsferQuotation=[];
$scope.railQuotation=[];
        $scope.sendQuotation=function(){
            $scope.quotationGroup = JSON.parse(sessionStorage.getItem('sendQuoteData'))
            $scope.flightDetailsQuotation= $scope.quotationGroup;
             var email=document.getElementById('email').value;
             var internalRemarks=document.getElementById('internalRemarks').value;
             var extranelRemarks=document.getElementById('extranelRemarks').value;
            $scope.tokenresult = JSON.parse(sessionStorage.getItem('authToken'));
            //  ajaxService.ajaxRequest('POST',ServerService.serverPath+'rest/repo / airport','{ "queryString" : "'+request.term+'" } ','json'),{ 
            var headersOptions = {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + $scope.tokenresult
            }
            let date = new Date();

            var todayDate = date.getFullYear()  + "-" + (date.getMonth()+1) + "-" + date.getDate();
            
          
             $scope.quotationReq.quotationNo=null,
            $scope.quotationReq.quotationStatus=null,
            $scope.quotationReq.requestDate=null;
            $scope.quotationReq.noOfServices=$scope.flightDetailsQuotation.length,
            $scope.quotationReq.coordinatorId=$scope.cordinatorDetails.id,
            $scope.quotationReq.toEmail=email;
            $scope.quotationReq.agencyId=sessionStorage.companyId,
            $scope.quotationReq.lpoNo=null,
            $scope.quotationReq.fop=sessionStorage.fopValue,
            $scope.quotationReq.serviceDetails=$scope.allServiceDetails;
            $scope.quotationReq.serviceDetails.quotation=$scope.quotationData;
            $scope.quotationReq.serviceDetails.quotation.flight=$scope.flightObj;
            $scope.quotationReq.serviceDetails.quotation.flight['quote-details']=$scope.quotationDetails;
            $scope.quotationReq.serviceDetails.quotation.flight['quote-details'].paxProfiles=$scope.selectTraveldata;
            $scope.quotationReq.serviceDetails.quotation.flight['quote-details'].searchDetails= $scope.searchFlightObj 
            $scope.quotationReq.serviceDetails.quotation.flight['flight-items']= $scope.flightDetailsQuotation;
            $scope.quotationReq.serviceDetails.quotation.hotel=$scope.hotelObj;
            $scope.quotationReq.serviceDetails.quotation.hotel['hotel']= $scope.hotelQuotation;
            $scope.quotationReq.serviceDetails.quotation.ins=$scope.insObj;
            $scope.quotationReq.serviceDetails.quotation.ins['ins']= $scope.insQuotation;
            $scope.quotationReq.serviceDetails.quotation.transfer=$scope.transferObj;
            $scope.quotationReq.serviceDetails.quotation.transfer['transfer']= $scope.trsnsferQuotation;
            $scope.quotationReq.serviceDetails.quotation.rail=$scope.railObj;
            $scope.quotationReq.serviceDetails.quotation.rail['rail']= $scope.railQuotation;
            $scope.quotationReq.approvalRemarks=null,
            $scope.quotationReq.internalRemarks=internalRemarks;
            $scope.quotationReq.externalRemarks=extranelRemarks;
            $scope.quotationReq.version=1
           
            
	  	   $http.post(ServerService.dropDownPath+'quote/sentForApproval',$scope.quotationReq,{ 
			   
            headers: headersOptions
   }).then(function successCallback(response){


           if(response.data.success==true){
            $scope.cancellationPolicy = response.data
       
                   sessionStorage.setItem('quotationSaveResponse',JSON.stringify(response.data.data));
               
                   Lobibox.notify('success', {
                    size: 'mini',
                    position: 'top right',
                    msg: response.data.message,
                    delay: 1500
                }); 
                  $uibModalInstance.dismiss('cancel');  
            }else{
                Lobibox.notify('error', {
                    size: 'mini',
                    position: 'top right',
                    msg:response.data.errorMessage,
                    delay: 1500
                });
            } 
                   
           
           
      }, function errorCallback(response) {
          if(response.status == 403){
              var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
              status.then(function(greeting) {
               hotelcply();
              });
          }
        });
        }



      
$scope.isdependents = false;

$scope.dependent=function(cheked){
$scope.dependents=cheked
}

$scope.searchTrvlr=function(indexvalue,travelType){
    $scope.companyId=sessionStorage.companyId
    if($scope.companyId =='' ||   $scope.companyId ==undefined){
        $scope.companyDetailsTmp = JSON.parse(sessionStorage.getItem('companyDetails'));
        $scope.companyId = $scope.companyDetailsTmp.companyId;
    }
   
    $scope.isdependents = $scope.dependents;
  
    $scope.artloadingcancellation = true;
    var templist =[];
    if($scope.trvellersrch.paxName != "")
    {
        var innerobj = {
            "field": "firstName",
            "operator": "contains",
            "value": $scope.trvellersrch.paxName
        }
        templist.push(innerobj);
    }

    if($scope.trvellersrch.surName != "")
    {
        var innerobj = {
            "field": "lastName",
            "operator": "contains",
            "value": $scope.trvellersrch.surName
        }
        templist.push(innerobj);
    }

    if($scope.trvellersrch.birthdate != null && $scope.trvellersrch.birthmonth != null && $scope.trvellersrch.birthyear != null)
    {
        var innerobj = {
            "field": "dateOfBirth",
            "operator": "eq",
            "value": $scope.trvellersrch.birthyear + "-" + $scope.trvellersrch.birthmonth + "-" + $scope.trvellersrch.birthdate
        }
        templist.push(innerobj);
    }

    if($scope.trvellersrch.email != "")
    {
        var innerobj = {
            "field": "emailId",
            "operator": "eq",
            "value": $scope.trvellersrch.email
        }
        templist.push(innerobj);
    }

    if($scope.trvellersrch.passportNo != "")
    {
        var innerobj = {
            "field": "passportNo",
            "operator": "eq",
            "value": $scope.trvellersrch.passportNo
        }
        templist.push(innerobj);
    }

    if($scope.trvellersrch.mobile != "" )
    {
        if($scope.trvellersrch.mobcntrycodetrobj !="")
        {

        }
    }
    if($scope.trvellersrch.mobile != "" && $scope.trvellersrch.mobcntrycodetrobj !="")
    {
        var innerobj = {
            "field": "countryCode",
            "operator": "eq",
            "value": $scope.trvellersrch.mobcntrycodetrobj.name
        }
        templist.push(innerobj);
    }

    if($scope.trvellersrch.mobile != "" && $scope.trvellersrch.mobcntrycodetrobj !="")
    {
        var innerobj = {
            "field": "phoneNumber",
            "operator": "eq",
            "value": $scope.trvellersrch.mobile
        }
        templist.push(innerobj);
    }
        var filterReq = {
            "page": null,
            "getAllResults": true,            
            "filter": {
              "logic": "and",
              "filters": templist
           },
          
            "sort": [
              {
                "field": "createTime",
                "dir": "desc"
              }
          
            ]
          
          }

        
        $scope.usersObj=undefined;
        $scope.artloadingcancellation=true;
        $scope.tokenresult = JSON.parse(sessionStorage.getItem('authToken'));
                    var headersOptions = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + $scope.tokenresult
        }
        if($scope.dependents==true){
        $http.post(ServerService.dropDownPath + 'pax/search/'+$scope.companyId+ '/dependents'  ,filterReq, {
            headers: headersOptions
        }).then(function successCallback(response) {
            $scope.trvSearchdata = response.data.data;
            $scope.artloadingddd=false;
            $scope.artloadingcancellation = false;
         
        
            sessionStorage.setItem('loginTravellerData', JSON.stringify($scope.trvSearchdata));
            $scope.allTravellerData=[];
            $scope.TravelType=travelType;
            $scope.trverlindx = indexvalue;
            $scope.allTravellerDataa = JSON.parse(sessionStorage.getItem('loginTravellerData'));
            $scope.groups = [
                {name:'Letters', values:['a','b','c']},
                {name:'Odd Numbers', values:[1,3,5,7]},
                {name:'Even Numbers', values:[2,4,6,8]}
            ];
                $scope.trvellersrchyears = $scope.ayears;
                $scope.trvellersrchday = $scope.dobdates;
                $scope.trvellersrchmonth = $scope.dobmonths;    
                $scope.countryList = JSON.parse(sessionStorage.getItem('countryList'));
             
        
     
        });
        }else{
            $http.post(ServerService.dropDownPath + 'pax/search/'+$scope.companyId ,filterReq, {
                headers: headersOptions
            }).then(function successCallback(response) {
                $scope.trvSearchdata = response.data.data;
                $scope.artloadingddd=false;
                $scope.artloadingcancellation = false;
        
            
                sessionStorage.setItem('loginTravellerData', JSON.stringify($scope.trvSearchdata));
                $scope.allTravellerData=[];
                $scope.TravelType=travelType;
                $scope.trverlindx = indexvalue;
                $scope.allTravellerDataa = JSON.parse(sessionStorage.getItem('loginTravellerData'));
           
                    $scope.trvellersrchyears = $scope.ayears;
                    $scope.trvellersrchday = $scope.dobdates;
                    $scope.trvellersrchmonth = $scope.dobmonths;    
                    $scope.countryList = JSON.parse(sessionStorage.getItem('countryList'));
                 
            
         
            });
        }
    }
    $scope.initSrchObj = function()
    {
      $scope.trvellersrch = {
          paxName:"",
          birthdate:null,
          birthmonth:null,
          birthyear:null,
          email:"",
          mobcntrycodetrobj:"",
          mobile:"",
          passportNo:"",
          surName:""
        };    
        $scope.phNoREQ = false;
        $scope.ccREQ = false;
    }

    
    $scope.initSrchObj();

            function range(start, end) {
                return Array(end - start + 1).fill().map((_, idx) => start + idx)
              }
              //calculate age from today date
              var curd = new Date();
              var curyear = curd.getFullYear();
              var curmonth = curd.getMonth();
              var curday = curd.getDate();
              var maxinfantdate = new Date(curyear - 2, curmonth, curday);
              var maxchilddate = new Date(curyear - 0, curmonth, curday);
              var maxadultdate = new Date(curyear - 55, curmonth, curday);
          
             
          
              var maxCNNY = maxchilddate.getFullYear();
          
          
         
          
          
              $scope.months = 12;
               $scope.getminexpirymonth = 12;
          
               $scope.dobdates = range(1, 31);
            
          
               $scope.ayears = range(curyear-55, maxCNNY);
              
               $scope.dobmonths = range(1, 12);
        $scope.selectingTrvlr=function(event)
        {
      

              $scope.modalInstance=$uibModal.open({
                  templateUrl: 'myQuotation.tmpl.html',
                  backdrop: 'static',
                  
                  size: 'xl',
                  keyboard: true,
                  scope:$scope
              });
              $scope.searchTrvlr();
          
        
        }

      

    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };
  
    /*   $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
      }; */
    // var fareRequest = new Object();		
    // fareRequest.provider = $scope.provider ;
    // fareRequest.key = $scope.farekey;
    // var fareRequestJSON = JSON.stringify(fareRequest);

});
// flightdetailcontroller....
ouaApp.controller('flightDetailedinformationContent', function($scope, $uibModalInstance, $rootScope) {

    $scope.monthcnvrt =  function(val){
        return new Date(2014, val-1);
    }

    $scope.cabinClass = localStorage.getItem('cabinClass');
    var value = $rootScope.selectedIndex;
    if (!JSON.parse(sessionStorage.getItem('flightFilterData'))) {
        $scope.flightDetails = $scope.result;
        $scope.flightDetails1 = $rootScope.selectedFlightDetails;
    } else {
        $scope.flightDetails = JSON.parse(sessionStorage.getItem('flightFilterData'))
        $scope.flightDetails1 = $rootScope.selectedFlightDetails;
    }

    $scope.waitingDuration = [];
    var count = 0;
    $scope.getWaitingDuration = function(a, b, ind, x, key) {
        if (a != undefined && b !== undefined) {
            var startDate = new Date(a);
            var endDate = new Date(b);
            var d = (endDate.getTime() - startDate.getTime()) / 1000;
            d = Number(d);
            var h = Math.floor(d / 3600);
            var m = Math.floor(d % 3600 / 60);
            var hDisplay = h + "h";
            var mDisplay = m + "m";
            $scope.waitingDuration[parseInt(ind + "" + x + "" + key)] = hDisplay + " " + mDisplay;

        }

    }


    $scope.getTotalDuration = function(a, b) {
        $scope.totalDuration = timeConvert(parseInt(a) + parseInt(b));

    }

    function timeConvert(n) {
        var num = n;
        var hours = (num / 60);
        var rhours = Math.floor(hours);
        var minutes = (hours - rhours) * 60;
        var rminutes = Math.round(minutes);
        return rhours + "h " + rminutes + " m";
    }

    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };
});
// flightdetailcontrollerends.....




ouaApp.controller('flightbaggageinfoController', function($scope,refreshService, $uibModalInstance, $rootScope, $http, ServerService) {

    var flightInfo = $rootScope.selectedBagIndex;

    // if ($rootScope.selectedBagDetails.additionalProperties.PTCFareBreakdownList != undefined) {
    //     $scope.baggageWeight = $rootScope.selectedBagDetails.additionalProperties.PTCFareBreakdownList[0].passengerFare.tpaextensions1.baggageInformationList.baggageInformations[0].allowanceWeight;
    // } else {
    //     $scope.baggageWeight = 0
    // }
    $scope.monthcnvrt =  function(val){
        return new Date(2014, val-1);
    }

    splitFun = function(orgin,dest){
        var orgins = orgin.split("(")[1].split(")")[0];
        var dests = dest.split("(")[1].split(")")[0];
        return orgins+"_"+dests;
    }
    $scope.result = JSON.parse(sessionStorage.getItem('flightSearchRS'));
    $scope.flightDetails1 = $scope.selectedBagDetails;

    // $scope.flightDetails1.segGrpList.forEach(element => {
    //     if(element.inclusions != null){
    //     element.ssrInclsions = (element.ssrInclsions || {});
    //     if(element.inclusions._PURCHASE_BAGGAGE){
    //         element.ssrInclsions.PurchaseBaggage = (element.ssrInclsions.PurchaseBaggage || "");
    //         element.ssrInclsions.PurchaseBaggage =  element.inclusions._PURCHASE_BAGGAGE
    //     }
    //     if(element.inclusions._CABIN_BAGGAGE){
    //         element.ssrInclsions.CabinBaggage = (element.ssrInclsions.CabinBaggage || "");
    //         element.ssrInclsions.CabinBaggage =  element.inclusions._CABIN_BAGGAGE
    //     }
    //     if(element.inclusions._CHECKED_BAGGAGE){
    //         element.ssrInclsions.CheckedBaggage = (element.ssrInclsions.CheckedBaggage || "");
    //         element.ssrInclsions.CheckedBaggage =  element.inclusions._CHECKED_BAGGAGE
    //     }
    //     if(element.inclusions._MEALS){
    //         element.ssrInclsions.Meal = (element.ssrInclsions.Meal || "");
    //         element.ssrInclsions.Meal =  element.inclusions._MEALS
    //     }
    //     if(element.inclusions._SEATS){
    //         element.ssrInclsions.Seat = (element.ssrInclsions.Seat || "");
    //         element.ssrInclsions.Seat =  element.inclusions._SEATS
    //     }
    //     if(element.inclusions._CANCELLATION){
    //         element.ssrInclsions.Cancellation = (element.ssrInclsions.Cancellation || "");
    //         element.ssrInclsions.Cancellation =  element.inclusions._CANCELLATION
    //     }
       
    // }
    // });
    baggagedtls = function(){

    // if ($scope.particularFlightData.additionalProperties.PTCFareBreakdownList != undefined) {
    //     $scope.baggageWeight = $scope.particularFlightData.additionalProperties.PTCFareBreakdownList[0].passengerFare.tpaextensions1.baggageInformationList.baggageInformations[0].allowanceWeight;
    // } else {
    //     $scope.baggageWeight = 0
    // }
    $scope.tokenresult = JSON.parse(sessionStorage.getItem('authToken'));
    var headersOptions = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + $scope.tokenresult
    }

    var sessionId=JSON.parse(sessionStorage.getItem('flightSessionId'));
    var repriceDetails = {
            "sessionId": sessionId,
            "resultIndexId": flightInfo
        }
    $scope.artloading = true;
    $http.post(ServerService.dropDownPath+ 'customer/'+ $scope.corprateCompanyId  + '/flight/ssr', repriceDetails, {

        headers: headersOptions
    }).then(function successCallback(response) {
        if(response.data.success){
         
        $scope.flightSsrData = response
        $scope.baggageSsr = $scope.flightSsrData.data.data;
        $totallBagaggeInfo = [];
        // for (var key in $scope.baggageSsr) {
            $scope.DEFAULT_CABIN_BAGGAGE_COMMENTS=$scope.flightSsrData.data.clientAdditionalProperties.DEFAULT_CABIN_BAGGAGE_COMMENTS;
            $scope.DEFAULT_CABIN_BAGGAGE=$scope.baggageSsr.DEFAULT_CABIN_BAGGAGE;
        for(var fl=0; fl<$scope.flightDetails1.segGrpList.length;fl++){
            for(var sl=0; sl<$scope.flightDetails1.segGrpList[fl].segList.length;sl++){
                for(var fls=0; fls<$scope.flightDetails1.segGrpList[fl].segList[sl].fltInfoList.length;fls++){
                    var fltinfo = $scope.flightDetails1.segGrpList[fl].segList[sl].fltInfoList[fls];
                    var key = splitFun(fltinfo.origin,fltinfo.dest);
                    if($scope.baggageSsr[key] != undefined ){
                    $scope.baggageinfo = $scope.baggageSsr[key].baggageSSRs;
                    var defaultBag = $scope.baggageSsr[key].defaultBaggage[0].cabin;
                    for (var b = 0; b < $scope.baggageinfo.length; b++) {
                        if($scope.baggageinfo[b].passengerKey == 'ADT'){
                            $scope.flightDetails1.segGrpList[fl].segList[sl].fltInfoList[fls].adtBaggageWt = ($scope.flightDetails1.segGrpList[fl].segList[sl].fltInfoList[fls].adtBaggageWt || "");
                            $scope.flightDetails1.segGrpList[fl].segList[sl].fltInfoList[fls].adtBaggageWt = $scope.baggageinfo[b].ssrData.ssrDetails;
                            $scope.flightDetails1.segGrpList[fl].segList[sl].fltInfoList[fls].adtCabinBagWt = ($scope.flightDetails1.segGrpList[fl].segList[sl].fltInfoList[fls].adtCabinBagWt || "");
                            $scope.flightDetails1.segGrpList[fl].segList[sl].fltInfoList[fls].adtCabinBagWt = defaultBag;
                        }
                        if($scope.baggageinfo[b].passengerKey == 'CNN'){
                            $scope.flightDetails1.segGrpList[fl].segList[sl].fltInfoList[fls].cnnBaggageWt = ($scope.flightDetails1.segGrpList[fl].segList[sl].fltInfoList[fls].cnnBaggageWt || "");
                            $scope.flightDetails1.segGrpList[fl].segList[sl].fltInfoList[fls].cnnBaggageWt = $scope.baggageinfo[b].ssrData.ssrDetails;
                            $scope.flightDetails1.segGrpList[fl].segList[sl].fltInfoList[fls].cnnCabinBagWt = ($scope.flightDetails1.segGrpList[fl].segList[sl].fltInfoList[fls].cnnCabinBagWt || "");
                            $scope.flightDetails1.segGrpList[fl].segList[sl].fltInfoList[fls].cnnCabinBagWt = defaultBag;
                       }
                        if($scope.baggageinfo[b].passengerKey == 'INF'){
                            $scope.flightDetails1.segGrpList[fl].segList[sl].fltInfoList[fls].infBaggageWt = ($scope.flightDetails1.segGrpList[fl].segList[sl].fltInfoList[fls].infBaggageWt || "");
                            $scope.flightDetails1.segGrpList[fl].segList[sl].fltInfoList[fls].infBaggageWt = $scope.baggageinfo[b].ssrData.ssrDetails;
                            $scope.flightDetails1.segGrpList[fl].segList[sl].fltInfoList[fls].infCabinBagWt = ($scope.flightDetails1.segGrpList[fl].segList[sl].fltInfoList[fls].infCabinBagWt || "");
                            $scope.flightDetails1.segGrpList[fl].segList[sl].fltInfoList[fls].infCabinBagWt = defaultBag;
                        }
                    }
                 }
                }
            }
        }
        $scope.artloading = false;

        // }
        
        sessionStorage.setItem('ssrtotalData', JSON.stringify($scope.flightSsrData));
        $scope.ssrTotalData = JSON.parse(sessionStorage.getItem('ssrData'));
    }else{
        $scope.artloading = false;
    }


}, function errorCallback(response) {
    if(response.status == 403){
        var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
                       status.then(function(greeting) {
                        baggagedtls();
                    });
        // $scope.autoAirlines(value);
    }
    // called asynchronously if an error occurs
    // or server returns response with an error status.
  });
}

if(!$scope.flightDetails1.isLcc){
    baggagedtls();
}

    //  fromflight......for.
    // $scope.cabinClass = localStorage.getItem('cabinClass');
    // var value = $rootScope.selectedBagIndex;
    // if (!JSON.parse(sessionStorage.getItem('flightFilterData'))) {
    //     $scope.flightDetails = JSON.parse(sessionStorage.getItem('flightSearchRS'))
    //     $scope.flightDetails1 = $scope.selectedBagDetails;
    // } else {
    //     $scope.flightDetails = JSON.parse(sessionStorage.getItem('flightFilterData'))
    //     $scope.flightDetails1 = $scope.selectedBagDetails;
    // }
    //  $scope.flightDetails2 = $scope.flightDetails
    //  $scope.filter = $rootScope.sCheckId
    //  $scope.airlineCode = $rootScope.airlineCodes;
    //  var flag = false;
    //  var filterDatas = {};
    //  if ($scope.filter != null && $scope.airlineCode != null) {

    //    filterDatas = stopsService.getStop($scope.flightDetails2, $scope.filter, $scope.airlineCode)
    //    $scope.flightDetails1 = filterDatas[value];
    //  } else if (!flag) {
    //    if ($scope.filter != null || $scope.filter != undefined) {
    //      filterDatas = stopsService.getStop($scope.flightDetails2, $scope.filter, $scope.airlineCode)
    //      $scope.flightDetails1 = filterDatas[value];
    //    } else if ($scope.airlineCode != null || $scope.airlineCode != undefined) {
    //      filterDatas = stopsService.getStop($scope.flightDetails2, $scope.filter, $scope.airlineCode)
    //      $scope.flightDetails1 = filterDatas[value];
    //    }
    //  }
    $scope.waitingDuration = [];
    var count = 0;
    $scope.getWaitingDuration = function(a, b, ind, x, key) {
        if (a != undefined && b !== undefined) {
            var startDate = new Date(a);
            var endDate = new Date(b);
            var d = (endDate.getTime() - startDate.getTime()) / 1000;
            d = Number(d);
            var h = Math.floor(d / 3600);
            var m = Math.floor(d % 3600 / 60);
            var hDisplay = h + "h";
            var mDisplay = m + "m";
            $scope.waitingDuration[parseInt(ind + "" + x + "" + key)] = hDisplay + " " + mDisplay;

        }

    }


    $scope.getTotalDuration = function(a, b) {
        $scope.totalDuration = timeConvert(parseInt(a) + parseInt(b));

    }

    function timeConvert(n) {
        var num = n;
        var hours = (num / 60);
        var rhours = Math.floor(hours);
        var minutes = (hours - rhours) * 60;
        var rminutes = Math.round(minutes);
        return rhours + "h " + rminutes + " m";
    }
    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };
});



ouaApp.controller('flightFaretotalControllers', function($scope, $http, $uibModalInstance, $rootScope) {
    $scope.currency = "AED";
    $scope.farerule = 'test';
    $scope.monthcnvrt =  function(val){
        return new Date(2014, val-1);
    }
        var index = $rootScope.selectedFareIndex.indx;
        // $scope.flightDetail = JSON.parse(sessionStorage.getItem('flightSearchRS'));
        $scope.flightDetail1 = $rootScope.selectedFareIndex


        // $scope.refundtype =  $scope.flightDetail1.data.refund
        $scope.flightDetail2 = $rootScope.selectedFareIndex.fareslst;
    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };
    /*   $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
      }; */
    // var fareRequest = new Object();		
    // fareRequest.provider = $scope.provider ;
    // fareRequest.key = $scope.farekey;
    // var fareRequestJSON = JSON.stringify(fareRequest);

});


ouaApp.controller('outPolicyControllerFlight', function($scope, $http,ServerService, $uibModalInstance, $rootScope) {
    $scope.currency = "AED";
    $scope.farerule = 'test';
    $scope.searchFlightObj = JSON.parse(sessionStorage.getItem('flightSearchRQ'));
    var value = $rootScope.selectedIndex;
       $scope.policydetails= $rootScope.selectedFlightDetails;
      $scope.policyRequestObj={};
      $scope.policyRequestObj.origin=  $scope.policydetails.origin
      $scope.policyRequestObj.dest=  $scope.policydetails.destination
      $scope.policyRequestObj.stops= $scope.policydetails.segGrpList[0].segList[0].stops
      $scope.policyRequestObj.refund=  $scope.policydetails.refund
      $scope.policyRequestObj.classType=  $scope.policydetails.classType
      $scope.policyRequestObj.designaiton= $scope.searchFlightObj.designation
      $scope.policyRequestObj.airlineCode=$scope.policydetails.segGrpList[0].segList[0].airline
      $scope.policyRequestObj.travelType= $scope.searchFlightObj.travelType

      $scope.tokenresult = JSON.parse(sessionStorage.getItem('authToken'));
      var headersOptions = {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + $scope.tokenresult
      }
  
    $http.post(ServerService.dropDownPath + 'customer/'+ $scope.corprateCompanyId + '/flight/deviation',$scope.policyRequestObj, {
    
        headers: headersOptions
    }).then(function successCallback(response) {
        
    $scope.loading = false;
    $scope.disabled = false;
    if (response.data.success == true) {
        $scope.outPolicy=response.data.data;

    }else{

    }
  
    });
    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };

});

ouaApp.controller('flightFareinfoControllers', function($scope, $http,refreshService, ServerService, $uibModalInstance, $rootScope) {

    $scope.monthcnvrt =  function(val){
        return new Date(2014, val-1);
    }    
        var index = $rootScope.selectedFareIndex;
        // $scope.flightDetail = JSON.parse(sessionStorage.getItem('flightSearchRS'));
        $scope.flightDetail1 = $rootScope.selectedFareIndex
        $scope.baggagedatafinding = $scope.flightDetail1.additionalProperties.SSR;
        // $scope.refundtype =  $scope.flightDetail1.data.refund
        $scope.flightDetail2 = $rootScope.selectedFareIndex.fareslst;
        $scope.airlinenames = $rootScope.selectedFareIndex.segGrpList[0].segList[0].airlineName;
        $scope.aircraft = $rootScope.selectedFareIndex.segGrpList[0].segList[0].fltInfoList[0].aircraftType;
        if ($scope.aircraft == null) {
            $scope.aircraft = "-";
        }
        $scope.flightDetails = JSON.parse(sessionStorage.getItem('flightFilterData'))
        if ($rootScope.selectedFareIndex != null) {

            $scope.seggrplist = $rootScope.selectedFareIndex.segGrpList;
            $scope.baggageDetails = [];
            $scope.baggageDetails.segList = [];
            $scope.baggageDetails.ssrDetails = []
            for (var i = 0; i < $scope.seggrplist.length; i++) {
                for (var j = 0; j < $scope.seggrplist[i].segList.length; j++) {
                    $scope.baggageDetails.segList.push($scope.seggrplist[i].segList[j]);
                }
                if ($scope.seggrplist[i].ssrDetails != null) {
                    for (var j = 0; j < $scope.seggrplist[i].ssrDetails.length; j++) {
                        $scope.baggageDetails.ssrDetails.push($scope.seggrplist[i].ssrDetails[j]);
                    }
                } else {
                    $scope.baggagessrdetails = "nossr";
                }
            }
        } else {
            $scope.seggrplist = $scope.flightDetails[index].segGrpList
            $scope.baggageDetails = [];
            $scope.baggageDetails.segList = [];
            $scope.baggageDetails.ssrDetails = []
            for (var i = 0; i < $scope.seggrplist.length; i++) {
                for (var j = 0; j < $scope.seggrplist[i].segList.length; j++) {
                    $scope.baggageDetails.segList.push($scope.seggrplist[i].segList[j]);
                }

                for (var j = 0; j < $scope.seggrplist[i].ssrDetails.length; j++) {
                    $scope.baggageDetails.ssrDetails.push($scope.seggrplist[i].ssrDetails[j]);
                }
            }

        }
        localStorage.setItem('baggagedata', JSON.stringify($scope.baggageDetails));






        $scope.refundtypes = $rootScope.selectedFareIndex.refund;
        if ($scope.refundtypes == true) {
            $scope.refundtype = "Refundable";
        } else {
            $scope.refundtype = "Non-Refundable";
        }
        $scope.provider = $rootScope.selectedFareIndex.provider;
        if ($rootScope.selectedFareIndex.fareslst[0].fareInfoRefKey) {
            $scope.farekey = $rootScope.selectedFareIndex.fareslst[0].fareInfoRefKey[0];
        } else {
            $scope.farekey = $rootScope.selectedFareIndex.fareslst[0].fareInfoRefKey;
        }



    

    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.loading = true;
    $scope.disabled = true;
    faredetils = function(){
    $scope.tokenresult = JSON.parse(sessionStorage.getItem('authToken'));
    var headersOptions = {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Credentials': 'true',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'GET, POST, PATCH, DELETE, PUT, OPTIONS',
        'Access-Control-Allow-Headers': 'Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With',
        'Authorization': 'Bearer ' + $scope.tokenresult
    }
    var sessionId=JSON.parse(sessionStorage.getItem('flightSessionId'));
    var repriceDetails = {
        "sessionId": sessionId,
        "resultIndexId": index.indx
    }

    $scope.artloading = true;

    $http.post(ServerService.dropDownPath+ 'customer/'+ $scope.corprateCompanyId  + '/flight/farerules', repriceDetails, {

        headers: headersOptions
    }).then(function successCallback(response) {
        $scope.artloading = false;
        var data = response.data.data;
        $scope.farerule = data;
        if (data != null) {
            $('.loading').hide();
            $scope.loading = false;
            $scope.disabled = false;


            $scope.farerule = data;
            // if (data != null) {

            //     // for (var f = 0; f < data.length; f++) {
            //     //     $scope.farerule = data[f].content;
            //     //     $('.fareRules').append(data[f].content)
            //     //     $('.fareHeader').append(data[f].headerText)
            //     // }
            // } else {
            //     $scope.farerule = [];//"Fare rules not available";
            //     $('.fareRules').append('Fare rules not available');
            //     $('.fareRules').css('text-align', 'center');
            //     $('.fareRules').css('color', 'red');

            // }
     

        } else {
            $scope.farerule = [];//"Fare rules not available";
            // $('.fareRules').append('Fare rules not available');
            $('.fareRules').css('text-align', 'center');
            $('.fareRules').css('color', 'red');


        }

    }, function errorCallback(response) {
        if(response.status == 403){
           var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
                          status.then(function(greeting) {
                            faredetils();
                   });
            // $scope.autoAirlines(value);
        }
        // called asynchronously if an error occurs
        // or server returns response with an error status.
      });

    }

    faredetils();

}).directive('expand', function () {
    function link($scope, element, attrs) {
        $scope.$on('onExpandAll', function (event, args) {
            $scope.expanded = args.expanded;
        });
    }
    return {
        link: link
    };
});