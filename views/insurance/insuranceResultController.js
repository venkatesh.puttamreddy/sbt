ouaApp.controller('insuranceResultController', function ($scope,$timeout,$rootScope,$http,$uibModal, renderHTMLFactory, $location, ServerService, customSession) {
    sessionStorage.removeItem('flightplusSearchResp');
    sessionStorage.removeItem('hotelPlusSearchResp');
    sessionStorage.removeItem('repriseResponse');
    sessionStorage.removeItem('roomIndex');
    sessionStorage.removeItem('selectedFliResp');
    sessionStorage.removeItem('selectedHotResp');
	angular.element('#flight-menu').removeClass("active");
	angular.element('#hotel-menu').removeClass("active");
	angular.element('#holiday-menu').removeClass("active");
	angular.element('#uae-menu').removeClass("active");
	angular.element('#insurance-menu').addClass("active");
	$rootScope.newtabmenuactive=2;
	// sessionStorage.removeItem('ridersplan');
	// sessionStorage.removeItem('avilplan')
	$scope.isCollapsed = true;
	$scope.renderHTMLFactory = renderHTMLFactory;
	$('html, body').scrollTop(0);


    $(".refine-search").click(function () {
      $(".toggle").slideToggle();
    });
    $(".refine-search-close").click(function () {
      $(".toggle").slideUp();
    });
	$scope.timeload = function(){
        $("#load").show();
        setTimeout(function () {
           $('#load').hide();
        }, 1000);
    }
	
	 $scope.Insurance = JSON.parse(sessionStorage.getItem('insurenceSearchRQ')); 
	$scope.InsuranceSearchRequest = JSON.parse(sessionStorage.getItem('InsuranceSearchReq'));
	$scope.InsuranceSearchReqItem = $scope.InsuranceSearchRequest ;
	if(!$scope.Insurance.infantCount){
		$scope.Insurance.infantCount=0;
	}
	$scope.passengerCount = Number($scope.Insurance.adultCount) + Number($scope.Insurance.childCount) +Number($scope.Insurance.infantCount)
	
	$scope.InsuranceSearchResult = JSON.parse(sessionStorage.getItem('InsuranceSearchResult'));

	if($scope.InsuranceSearchReqItem.returnDateTime){
		$scope.trip = 'Round trip';
	} else {
		$scope.trip = 'One Way';
		}
		
		$scope.riderPlans = $scope.InsuranceSearchResult.data.upsellPlans
	var plans = $scope.InsuranceSearchResult.data.availablePlans
	
	/* sorting based on totalFare */
	function sorting (array, key){
		return array.sort(function(a, b){
			var x = a[key]; var y = b[key];
			return ( (x < y) ? -1 : ((x > y) ? 1 : 0));
			});
		}
		$scope.availablePlans = sorting(plans, 'totalFare');
	/* end */
	$scope.insurancedate={
		startDate: moment(),
		endDate: moment(),
		locale:{
			format : 'D-MMM-YY'
			}
		}
	$scope.isLifeStylePlanSelected;
/* availablePlans function */
$scope.allInsPlan = [];
$scope.grandTotal = 0;
$scope.serviceCharge=0;
$scope.disount=0;
$scope.markUp=0;
$scope.selectAvailablePlanAlredySelected=false;

$scope.calcGrandTotal = function(){
	$scope.grandTotal = 0;
	$scope.disount=0;
	$scope.serviceCharge=0;
	$scope.markUp=0;
	if($scope.selectedRaidPlan != null){
		if($scope.selectedRaidPlan.length>0){
			$scope.selectedRaidPlan.forEach(element => {
				$scope.serviceCharge +=element.serviceCharge;
				$scope.grandTotal += element.totalFare;
				$scope.disount += element.disount;
				$scope.markUp +=element.markUp;
			});
		}
	}
		if($scope.selectedAvailPlan){
			$scope.grandTotal += $scope.selectedAvailPlan.totalFare;
			$scope.disount += $scope.selectedAvailPlan.disount;
			$scope.serviceCharge +=$scope.selectedAvailPlan.serviceCharge;
			$scope.markUp +=$scope.selectedAvailPlan.markUp;
		}
	}




$scope.selectedRiderPlansIndex=[];
$scope.selectedRaidPlan = [];

$scope.selectedAvailPlan = JSON.parse(sessionStorage.getItem('avilplan'));
if($scope.selectedAvailPlan){
	$scope.isLifeStylePlanSelected=true;
}
$scope.selectedRaidPlan = JSON.parse(sessionStorage.getItem('ridersplan'));
if($scope.selectedRaidPlan){
	$scope.isLifeStylePlanSelected=true;
}
if( $scope.selectedRaidPlan!=null){
	$scope.calcGrandTotal(); 
	setTimeout(function(){
	for(var i=0;i<$scope.riderPlans.length;i++){
		$scope.selectedRaidPlan.forEach(plans => {
			if(plans.sessionId == $scope.riderPlans[i].sessionId){
					$("#selectriders_"+i).prop("checked", true);
					$scope.selectedRiderPlansIndex.push(i);
			}
		});
	}
},1000)

}else{
$scope.selectedRaidPlan = [];
}
if($scope.selectedAvailPlan != null){
	$scope.calcGrandTotal(); 
	setTimeout(function(){
		for(var pi=0;pi<$scope.availablePlans.length;pi++){
			if($scope.selectedAvailPlan.sessionId == $scope.availablePlans[pi].sessionId){
				$("#selectplans_"+pi).attr("checked", true);
			}
		}
	},1000)
	
}else{
}

$scope.selectedQuoteAvilplan = JSON.parse(sessionStorage.getItem('avilplanSendQuote'));
if( $scope.selectedQuoteAvilplan!=null){
	$scope.selectedQuoteAvilplan =$scope.selectedQuoteAvilplan[0];
	setTimeout(function(){
	for(var availquote=0;availquote<$scope.availablePlans.length;availquote++){
		if($scope.selectedQuoteAvilplan.sessionId == $scope.availablePlans[availquote].sessionId){
			$("#selectplans1_"+availquote).prop("checked", true);
		}
	}
},1000)

}else{
$scope.selectedQuoteAvilplan = [];
}
	$scope.selectedQuoteRiderplan = JSON.parse(sessionStorage.getItem('ridersplanSendQuote'));
	if( $scope.selectedQuoteRiderplan!=null){
	
		setTimeout(function(){
		for(var i=0;i<$scope.riderPlans.length;i++){
			$scope.selectedQuoteRiderplan.forEach(plans => {
				if(plans.sessionId == $scope.riderPlans[i].sessionId){
						$("#lifeStyle1_"+i).prop("checked", true);
						$scope.selectedRiderPlansIndex.push(i);
				}
			});
		}
	},1000)
	
	}else{
	$scope.selectedQuoteRiderplan = [];
	}

	$scope.markupValueAdd=function(flightDetails,indexvalue) {

		$scope.grandTotalIns = 0;
		
		if($scope.selectedRaidPlan != null){
			if($scope.selectedRaidPlan.length>0){
				$scope.selectedRaidPlan.forEach(element => {
				
					$scope.grandTotalIns += element.totalFare;
				
				});
			}
		}
			if($scope.selectedAvailPlan){
				$scope.grandTotalIns += $scope.selectedAvailPlan.totalFare;
				
			}
		var markup=document.getElementById('markupValue').value; 
		if(markup==''){
			markup=0;    
		}
		sessionStorage.setItem("additionalMarkup",markup);
		var discount=document.getElementById('discountValue').value; 
		if(discount==''){
			discount=0;    
		}
		sessionStorage.setItem("additionalDiscount",discount);
	  
	
		$scope.grandTotal=$scope.grandTotalIns + parseInt(markup) - parseInt(discount)
		
	 }												   

$scope.availPlans = function(index){
	$scope.selectedAvailPlan = $scope.availablePlans[index];
	sessionStorage.setItem('avilplan',JSON.stringify($scope.selectedAvailPlan));
	$scope.isLifeStylePlanSelected=true;
	$scope.calcGrandTotal(); 
		$scope.markupValueAdd();
}

/* end */

/* ridersPlans function */

$scope.ridersPlans = function(value){
	if($scope.selectedRiderPlansIndex.includes(value)){
		$scope.selectedRaidPlan.pop($scope.riderPlans[value])
		$scope.selectedRiderPlansIndex.pop(value);
		$("#selectriders_"+value).prop("checked", false);

	}else{
		$scope.riderList = $scope.selectedRaidPlan.push($scope.riderPlans[value]);
		$scope.selectedRiderPlansIndex.push(value);
		$scope.isLifeStylePlanSelected=true;

		$("#selectriders_"+value).prop("checked", true);

	}
	sessionStorage.setItem('ridersplan',JSON.stringify($scope.selectedRaidPlan));
	$scope.calcGrandTotal();
$scope.markupValueAdd();
}
$scope.selectedQuoteAvailPlan=[];

$scope.addLifeStyleQuotations=function(value,event){
	event.stopPropagation();
		$scope.selectedQuoteAvailPlan=[]
		$("#selectplans1_"+value).attr("checked", true);
		
		 $scope.selectedQuoteAvailPlan.push($scope.availablePlans[value]);
	sessionStorage.setItem('avilplanSendQuote',JSON.stringify($scope.selectedQuoteAvailPlan));

} 
$scope.selectedRaidPlanQuote=[];
$scope.selectedRiderPlansIndexValue=[];
$scope.addRiderStyleQuotations=function(value,event){
	event.stopPropagation();
	if($scope.selectedRiderPlansIndexValue.includes(value)){
		$scope.selectedRaidPlanQuote.pop($scope.riderPlans[value])
		$scope.selectedRiderPlansIndexValue.pop(value);
		$("#lifeStyle1_"+value).prop("checked", false);

	}else{
		$scope.riderList = $scope.selectedRaidPlanQuote.push($scope.riderPlans[value]);
		$scope.selectedRiderPlansIndexValue.push(value);
		// $scope.isLifeStylePlanSelected=true;

		$("#lifeStyle1_"+value).prop("checked", true);

	}
	sessionStorage.setItem('ridersplanSendQuote',JSON.stringify($scope.selectedRaidPlanQuote));
	// $scope.calcGrandTotal();
}

if (sessionStorage.getItem('guestLoginDatails') && sessionStorage.getItem('guestLoginDatails') != "null") {
	$scope.loggedvalue = true;
	$rootScope.bookingHide = false;
	$rootScope.guestLogin = true;
	$rootScope.bookingHide1 = false;
	
	$rootScope.dispName = "";
	sessionStorage.removeItem("guestLoginDatails");
	localStorage.removeItem("loginToken");
	
}

// AvailPlansMarkupAndDiscount
$scope.normalstyle={}
$scope.availmarkup=function(availablePlan,indexvalue) {
	
	$scope.selectedQuoteAvilplan = JSON.parse(sessionStorage.getItem('avilplanSendQuote'));
	$scope.availablePlan=availablePlan
	var markup=document.getElementById('markupValue_'+indexvalue).value; 
	if(markup==''){
		markup=0;    
	}
	var discount=document.getElementById('discountValue_'+indexvalue).value; 
	if(discount==''){
		discount=0;    
	}
  
	 $scope.selectedQuoteAvilplan[indexvalue].markUp=parseInt(markup);
	 $scope.selectedQuoteAvilplan[indexvalue].disount=parseInt(discount);
	 $scope.selectedQuoteAvilplan[indexvalue].grndTotal=($scope.availablePlan.totalFare + parseInt(markup) - parseInt(discount))
	 sessionStorage.setItem("avilplanSendQuote", JSON.stringify($scope.selectedQuoteAvilplan));
	

 }

 $scope.ridermarkup=function(riderPlan,indexvalue) {
	
	$scope.selectedQuoteRiderplan = JSON.parse(sessionStorage.getItem('ridersplanSendQuote'));
	$scope.riderPlan=riderPlan
	var markup=document.getElementById('markupRiderValue_'+indexvalue).value; 
	if(markup==''){
		markup=0;    
	}
	var discount=document.getElementById('discountRiderValue_'+indexvalue).value; 
	if(discount==''){
		discount=0;    
	}
  
	 $scope.selectedQuoteRiderplan[indexvalue].markUp=parseInt(markup);
	 $scope.selectedQuoteRiderplan[indexvalue].disount=parseInt(discount);
	 $scope.selectedQuoteRiderplan[indexvalue].grndTotal=($scope.riderPlan.totalFare + parseInt(markup) - parseInt(discount))
	 sessionStorage.setItem("ridersplanSendQuote", JSON.stringify($scope.selectedQuoteRiderplan));
	

 }

 $scope.lifeStyleRemove=function(uniqueValue,index){
	
	document.getElementById("lifeStyle1_" +index).checked = false;
	$scope.selectedQuoteRiderplan = JSON.parse(sessionStorage.getItem('ridersplanSendQuote'));
	for(var a=0; a<	$scope.selectedQuoteRiderplan.length; a++){
		if($scope.selectedQuoteRiderplan[a].sessionId==uniqueValue){
			$scope.selectedQuoteRiderplan.splice(a, 1);
			sessionStorage.setItem("ridersplanSendQuote", JSON.stringify($scope.selectedQuoteRiderplan));
			$scope.selectedQuoteRiderplan=	$scope.selectedQuoteRiderplan;
		break;
		}
	}
	
 }



$scope.insurenceQuotationddd = function(size, parentSelector, FlightDetails) {

    $scope.selectedQuoteAvilplan = JSON.parse(sessionStorage.getItem('avilplanSendQuote'));
	if(  $scope.selectedQuoteAvilplan !=null){
		for(var a=0; a<$scope.selectedQuoteAvilplan.length; a++){
			$scope.selectedQuoteAvilplan[a].grndTotal=$scope.selectedQuoteAvilplan[a].totalFare;
		}
	}
	$scope.selectedQuoteRiderplan = JSON.parse(sessionStorage.getItem('ridersplanSendQuote'));
	if(  $scope.selectedQuoteRiderplan !=null){
		for(var a=0; a<$scope.selectedQuoteRiderplan.length; a++){
			$scope.selectedQuoteRiderplan[a].grndTotal=$scope.selectedQuoteRiderplan[a].totalFare;
		}
	}
	$scope.selectTraveldata = JSON.parse(sessionStorage.getItem('selectTraveldata'))
	if($scope.selectTraveldata==null){
        $scope.travelFunctionality=true;
    }else{
        $scope.travelFunctionality=false;
    }
}

$scope.outPolicyCheck=function(outPolicyData){
	$scope.Insurance = JSON.parse(sessionStorage.getItem('insurenceSearchRQ')); 
	$scope.policydetails=outPolicyData;
	$scope.policyRequestObj={};
	$scope.policyRequestObj.onwardDateTime=  $scope.Insurance.onwardDateTime
	$scope.policyRequestObj.returnDateTime=  $scope.Insurance.returnDateTime
	$scope.policyRequestObj.planTitle=  $scope.policydetails.planAdditionalInfoTitle
	$scope.policyRequestObj.designation= $scope.Insurance.designation,
	
	

	$scope.tokenresult = JSON.parse(sessionStorage.getItem('authToken'));
	var headersOptions = {
		'Content-Type': 'application/json',
		'Authorization': 'Bearer ' + $scope.tokenresult
	}

  $http.post(ServerService.dropDownPath + 'customer/'+ $scope.corprateCompanyId + '/insurance/deviation',$scope.policyRequestObj, {
  
	  headers: headersOptions
  }).then(function successCallback(response) {
	  
  $scope.loading = false;
  $scope.disabled = false;
  if (response.data.success == true) {
	$scope.outPolicy=response.data.data;

  }else{
$scope.noPolicy="Dont Have Any No Policys"
  }

  });
}

$scope.initSrchObj = function()
{
  $scope.trvellersrch = {
	  paxName:"",
	  birthdate:null,
	  birthmonth:null,
	  birthyear:null,
	  email:"",
	  mobcntrycodetrobj:"",
	  mobile:"",
	  passportNo:"",
	  surName:""
	};    
	$scope.phNoREQ = false;
	$scope.ccREQ = false;
}


$scope.initSrchObj();
$scope.isdependents = false;

$scope.dependent=function(cheked){
$scope.dependents=cheked
}
$scope.selectingTrvlr=function(){
    $scope.companyId=sessionStorage.companyId
    if($scope.companyId =='' ||   $scope.companyId ==undefined){
        $scope.companyDetailsTmp = JSON.parse(sessionStorage.getItem('companyDetails'));
        $scope.companyId = $scope.companyDetailsTmp.companyId;
    }
   
    $scope.isdependents = $scope.dependents;
  
    $scope.artloadingcancellation = true;
    var templist =[];
    if($scope.trvellersrch.paxName != "")
    {
        var innerobj = {
            "field": "firstName",
            "operator": "contains",
            "value": $scope.trvellersrch.paxName
        }
        templist.push(innerobj);
    }

    if($scope.trvellersrch.surName != "")
    {
        var innerobj = {
            "field": "lastName",
            "operator": "contains",
            "value": $scope.trvellersrch.surName
        }
        templist.push(innerobj);
    }

    if($scope.trvellersrch.birthdate != null && $scope.trvellersrch.birthmonth != null && $scope.trvellersrch.birthyear != null)
    {
        var innerobj = {
            "field": "dateOfBirth",
            "operator": "eq",
            "value": $scope.trvellersrch.birthyear + "-" + $scope.trvellersrch.birthmonth + "-" + $scope.trvellersrch.birthdate
        }
        templist.push(innerobj);
    }

    if($scope.trvellersrch.email != "")
    {
        var innerobj = {
            "field": "emailId",
            "operator": "eq",
            "value": $scope.trvellersrch.email
        }
        templist.push(innerobj);
    }

    if($scope.trvellersrch.passportNo != "")
    {
        var innerobj = {
            "field": "passportNo",
            "operator": "eq",
            "value": $scope.trvellersrch.passportNo
        }
        templist.push(innerobj);
    }

    if($scope.trvellersrch.mobile != "" )
    {
        if($scope.trvellersrch.mobcntrycodetrobj !="")
        {

        }
    }
    if($scope.trvellersrch.mobile != "" && $scope.trvellersrch.mobcntrycodetrobj !="")
    {
        var innerobj = {
            "field": "countryCode",
            "operator": "eq",
            "value": $scope.trvellersrch.mobcntrycodetrobj.name
        }
        templist.push(innerobj);
    }

    if($scope.trvellersrch.mobile != "" && $scope.trvellersrch.mobcntrycodetrobj !="")
    {
        var innerobj = {
            "field": "phoneNumber",
            "operator": "eq",
            "value": $scope.trvellersrch.mobile
        }
        templist.push(innerobj);
    }
        var filterReq = {
            "page": null,
            "getAllResults": true,            
            "filter": {
              "logic": "and",
              "filters": templist
           },
          
            "sort": [
              {
                "field": "createTime",
                "dir": "desc"
              }
          
            ]
          
          }

        
        $scope.usersObj=undefined;
        $scope.artloadingcancellation=true;
        $scope.tokenresult = JSON.parse(sessionStorage.getItem('authToken'));
                    var headersOptions = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + $scope.tokenresult
        }
        if($scope.dependents==true){
        $http.post(ServerService.dropDownPath + 'pax/search/'+$scope.companyId+ '/dependents'  ,filterReq, {
            headers: headersOptions
        }).then(function successCallback(response) {
            $scope.trvSearchdata = response.data.data;
            $scope.artloadingddd=false;
            $scope.artloadingcancellation = false;
         
        
            sessionStorage.setItem('loginTravellerData', JSON.stringify($scope.trvSearchdata));
            $scope.allTravellerData=[];
           
            $scope.allTravellerDataa = JSON.parse(sessionStorage.getItem('loginTravellerData'));
         
                $scope.trvellersrchyears = $scope.ayears;
                $scope.trvellersrchday = $scope.dobdates;
                $scope.trvellersrchmonth = $scope.dobmonths;    
                $scope.countryList = JSON.parse(sessionStorage.getItem('countryList'));
             
        
     
        });
        }else{
            $http.post(ServerService.dropDownPath + 'pax/search/'+$scope.companyId ,filterReq, {
                headers: headersOptions
            }).then(function successCallback(response) {
                $scope.trvSearchdata = response.data.data;
                $scope.artloadingddd=false;
                $scope.artloadingcancellation = false;
        
            
                sessionStorage.setItem('loginTravellerData', JSON.stringify($scope.trvSearchdata));
                $scope.allTravellerData=[];
              
                $scope.allTravellerDataa = JSON.parse(sessionStorage.getItem('loginTravellerData'));
           
                    $scope.trvellersrchyears = $scope.ayears;
                    $scope.trvellersrchday = $scope.dobdates;
                    $scope.trvellersrchmonth = $scope.dobmonths;    
                    $scope.countryList = JSON.parse(sessionStorage.getItem('countryList'));
                 
            
         
            });
        }
    }

var selectTraveller = new Map();
$scope.addTravel = function(checked, singlePsgr, age, indexvalue,uniqueValue) {

      
	if(checked){
		selectTraveller.set(singlePsgr.customerId,singlePsgr);
		$scope.selectTravellerData=Array.from(selectTraveller.values());
  
		sessionStorage.setItem('selectTraveldata', JSON.stringify($scope.selectTravellerData));
	}else{
		selectTraveller.delete(singlePsgr.customerId)
		$scope.selectTravellerData=Array.from(selectTraveller.values());
		sessionStorage.setItem("selectTraveldata", JSON.stringify($scope.selectTravellerData));
	}
		}

		$scope.ConfirmTravellerData=function(){
			$scope.travelData = JSON.parse(sessionStorage.getItem('selectTraveldata'))
			if($scope.travelData!=null ){
   $scope.selectTraveldata=  $scope.travelData;
		Lobibox.notify('success', {
			size: 'mini',
			position: 'top right',
			msg: "Travellers  added successfully!",
			delay: 1500
		});
		$scope.modalInstance.dismiss();
   
			} else{
				Lobibox.notify('error', {
					size: 'mini',
					position: 'top right',
					msg:"Please select at least one traveller!",
					delay: 1500
				});
			}
	
		}
		$scope.cordinatorDetails={};

		$scope.cordinatorDetails = JSON.parse(sessionStorage.getItem('cordinatorDetails'));
		if($scope.cordinatorDetails!=null){
		$scope.cordinatorDetails.email=$scope.cordinatorDetails.coordinatorEmail
		}

		$scope.quotationReq={};
		$scope.allServiceDetails={};
		$scope.quotationData={};
		$scope.flightObj={};
		$scope.hotelObj={};
		$scope.insObj={};
		$scope.transferObj={};
		$scope.railObj={};
		$scope.quotationDetails={};
		$scope.insquotationDetails={};
		$scope.hotelQuotation=[];
		$scope.insQuotation=[];
		$scope.trsnsferQuotation=[];
		$scope.railQuotation=[];
				$scope.sendQuotation=function(){
					$scope.Insurance.isPolicyAvailable=$scope.InsuranceSearchResult.data.isPolicyAvailable
					 var email=document.getElementById('email').value;
					 var internalRemarks=document.getElementById('internalRemarks').value;
					 var extranelRemarks=document.getElementById('extranelRemarks').value;
					$scope.tokenresult = JSON.parse(sessionStorage.getItem('authToken'));
					//  ajaxService.ajaxRequest('POST',ServerService.serverPath+'rest/repo / airport','{ "queryString" : "'+request.term+'" } ','json'),{ 
					var headersOptions = {
						'Content-Type': 'application/json',
						'Authorization': 'Bearer ' + $scope.tokenresult
					}
					let date = new Date();
		
					var todayDate = date.getFullYear()  + "-" + (date.getMonth()+1) + "-" + date.getDate();
					
				  
					 $scope.quotationReq.quotationNo=null,
					$scope.quotationReq.quotationStatus=null,
					$scope.quotationReq.requestDate=null;
					$scope.quotationReq.noOfServices=2,
					$scope.quotationReq.coordinatorId=$scope.cordinatorDetails.id,
					$scope.quotationReq.toEmail=email;
					$scope.quotationReq.agencyId=sessionStorage.companyId,
					$scope.quotationReq.lpoNo=null,
					$scope.quotationReq.fop=sessionStorage.fopValue,
					$scope.quotationReq.serviceDetails=$scope.allServiceDetails;
					$scope.quotationReq.serviceDetails.quotation=$scope.quotationData;
					$scope.quotationReq.serviceDetails.quotation.flight=$scope.flightObj;

					$scope.quotationReq.serviceDetails.quotation.flight['flight-items']= $scope.flightDetailsQuotation;
				
					$scope.quotationReq.serviceDetails.quotation.hotel=$scope.hotelObj;
					$scope.quotationReq.serviceDetails.quotation.hotel['hotel']= $scope.hotelQuotation;
					$scope.quotationReq.serviceDetails.quotation.ins=$scope.insObj;
					$scope.quotationReq.serviceDetails.quotation.ins['quote-details']=$scope.quotationDetails;
					$scope.quotationReq.serviceDetails.quotation.ins['quote-details'].paxProfiles=$scope.selectTraveldata;
					$scope.quotationReq.serviceDetails.quotation.ins['quote-details'].searchDetails= $scope.Insurance;

					$scope.quotationReq.serviceDetails.quotation.ins['ins-items']=$scope.insquotationDetails;

					$scope.quotationReq.serviceDetails.quotation.ins['ins-items'].availablePlans= $scope.selectedQuoteAvilplan;
					$scope.quotationReq.serviceDetails.quotation.ins['ins-items'].upsellPlans= $scope.selectedQuoteRiderplan;
					$scope.quotationReq.serviceDetails.quotation.transfer=$scope.transferObj;
					$scope.quotationReq.serviceDetails.quotation.transfer['transfer']= $scope.trsnsferQuotation;
					$scope.quotationReq.serviceDetails.quotation.rail=$scope.railObj;
					$scope.quotationReq.serviceDetails.quotation.rail['rail']= $scope.railQuotation;
					$scope.quotationReq.approvalRemarks=null,
					$scope.quotationReq.internalRemarks=internalRemarks;
					$scope.quotationReq.externalRemarks=extranelRemarks;
					$scope.quotationReq.version=1
				   
					
					 $http.post(ServerService.dropDownPath+'quote/sentForApproval',$scope.quotationReq,{ 
					   
					headers: headersOptions
		   }).then(function successCallback(response){
		
		
				   if(response.data.success==true){
					$scope.cancellationPolicy = response.data
			   
						   sessionStorage.setItem('quotationSaveResponse',JSON.stringify(response.data.data));
					   
						   Lobibox.notify('success', {
							size: 'mini',
							position: 'top right',
							msg: response.data.message,
							delay: 1500
						}); 
						  $uibModalInstance.dismiss('cancel');  
					}else{
						Lobibox.notify('error', {
							size: 'mini',
							position: 'top right',
							msg:response.data.errorMessage,
							delay: 1500
						});
					} 
						   
				   
				   
			  }, function errorCallback(response) {
				  if(response.status == 403){
					  var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
					  status.then(function(greeting) {
					   hotelcply();
					  });
				  }
				});
				}


/* end */
/* onBook function */
$scope.onBook = function(){
	// $scope.timeload();
	// $scope.artloading = true;
	//var checked = $('input[name="selectplans"]:checked').val();
	//var radioType = $('#selectplans').prop('checked');
	if(!$scope.isLifeStylePlanSelected){
		// $scope.artloading = false;
		Lobibox.alert('warning', {
			msg: "Please Select At Least One Travel Plan To Process Booking."
		});
		
	}else if(!$scope.isLifeStylePlanSelected){
		// $scope.artloading = false;
		Lobibox.alert('warning', {
			msg: "Please Select At Least One Travel Plan To Process Booking."
		});
	} else {

	$location.path('/insuranceReview')
		$('html,body').scrollTop(50);
}
																 
		
	
}
/* end */
	
console.log($scope.selectedRaidPlan);
}).directive('expand', function () {
    function link($scope, element, attrs) {
        $scope.$on('onExpandAll', function (event, args) {
            $scope.expanded = args.expanded;
        });
    }
    return {
        link: link
    };
});


