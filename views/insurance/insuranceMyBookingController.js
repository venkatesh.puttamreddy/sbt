ouaApp.controller('insuranceMyBookingController', function ($scope, $http, $location, $uibModal, baseUrlService, expiryService, ConstantService, ServerService,$rootScope){
  
    $scope.tabActivate=sessionStorage.activeService;
    
 
  
        $scope.value="INS";
  
    $scope.allBooking=function(){
   var bookingrequest= {
    "page": 1,
    "pageSize": 10,
    "take": 10,
    // "skip": 1,
    "filter": {
      "logic": "and",
      "filters": [
        {
            "field":"product",
            "operator":"eq",
            "value":"INS"
        }
      ]
    },
    "sort": [
      {
        "field": "createTime",
        "dir": "desc"
      }
    ]
   }
    $scope.tokenresult = JSON.parse(sessionStorage.getItem('authToken'));
    var headersOptions = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + $scope.tokenresult
    }
    $scope.artloading = true;
    $http.post(ServerService.listPath + 'search/myBookingsIns', JSON.stringify(bookingrequest), {
    
        headers: headersOptions
    }).then(function successCallback(response) {
        $scope.artloading = false;
        $scope.curentpage = 5;
        $scope.bookingResponse=response.data.data.data;
        for(var a=0; $scope.bookingResponse.length; a++) { 
            if($scope.bookingResponse[a].action!=null){                                                                                
         var booking=$scope.bookingResponse[a].action;
         var book=booking.replace(/[\[\]']/g,'' );
         var bookticket=book.split(',');
         $scope.bookingResponse[a].action=bookticket
            }
        }
    }, function errorCallback(response) {
        if(response.status == 403){
           var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
                          status.then(function(greeting) {
                            allBooking();
                   });
            // $scope.autoAirlines(value);
        }
        // called asynchronously if an error occurs
        // or server returns response with an error status.
      });


    }


    $scope.selectingTrvlr=function(bookingData,header)
    {
        $scope.booking=bookingData
        $scope.headerTable=header
      
      $scope.modalInstance=$uibModal.open({
          templateUrl: 'myTestModal.tmpl.html',
          backdrop: 'static',
          keyboard: true,
          scope:$scope
      });
     
    }



    $scope.ConfirmcancelTicket=function(valid){
        var remarks= $('#exampleFormControlTextarea1').val();
        if(remarks==""){
            $scope.errorMesg = "Enter All * (Mandatory) Fields"
            return false;
        }
        if( $scope.headerTable=='Cancel Ticket'){
        var remarks= $('#exampleFormControlTextarea1').val();
        $('.btn-block').prop('disabled', true);
        var cancelrequest={
            "pnrNo":  $scope.booking.bookingRefNo,
            "remarks": remarks
            }

            $scope.tokenresult = JSON.parse(sessionStorage.getItem('authToken'));
            var headersOptions = {
    'Content-Type': 'application/json',
    'Authorization': 'Bearer ' + $scope.tokenresult
}
$scope.artloadingcancellation=true;
$http.post(ServerService.serverPath + 'insurance/pnr/cancel',cancelrequest, {
    headers: headersOptions
}).then(function successCallback(response) {
    if(response.data.success==true){
        $scope.artloadingcancellation=false;
        $scope.bookingResponse=response.data.data;
        $scope.modalInstance.dismiss();
        Lobibox.alert('success', {
            msg: response.data.message,
            callback: function (lobibox, type) {
                if (type === 'ok') {
                    location.reload();
                }
            }
            });
        // Lobibox.alert('success', {
        //     msg: response.data.message
        // });
       
        }else{
            $scope.artloadingcancellation=false;
            $scope.modalInstance.dismiss();
            Lobibox.alert('error', {
                msg: response.data.errorMessage
            });
        }

})
        }else{
            var remarks= $('#exampleFormControlTextarea1').val();
            $('.btn-block').prop('disabled', true);
            var voidrequest={
                "pnrNo":  $scope.booking.bookingRefNo,
                "ticketNo": "",
                "remarks": remarks
                }
    
                $scope.tokenresult = JSON.parse(sessionStorage.getItem('authToken'));
                var headersOptions = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + $scope.tokenresult
    }
    $scope.artloadingcancellation=true;
    $http.post(ServerService.serverPath + 'insurance/pnr/void',voidrequest, {
        headers: headersOptions
    }).then(function successCallback(response) {
        if(response.data.success==true){
            $scope.artloadingcancellation=false;
            $scope.bookingResponse=response.data.data;
            $scope.modalInstance.dismiss();
            Lobibox.alert('success', {
                msg: response.data.message,
                callback: function (lobibox, type) {
                    if (type === 'ok') {
                        location.reload();
                    }
                }
                });
            // Lobibox.alert('success', {
            //     msg: response.data.message
            // });
           
            }else{
                $scope.artloadingcancellation=false;
                $scope.modalInstance.dismiss();
                Lobibox.alert('error', {
                    msg: response.data.errorMessage
                });
            }
    
    })
        }
    }

    $scope.bookingCancel = function(agencyId)
    {
        $scope.tokenresult = JSON.parse(sessionStorage.getItem('authToken'));
        var headersOptions = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + $scope.tokenresult
        }
        $scope.artloading = true;
        $http.post(ServerService.listPath + 'search/bookingAir', JSON.stringify(bookingrequest), {
        
            headers: headersOptions
        }).then(function successCallback(response) {
        $scope.artloading = false;
        if(response.data.success)
            {
        $scope.allBooking();
                $scope.curentpage = 5;
                $scope.bookingResponse=response.data.data.data;
            }
            else
            {

            }
           
        }, function errorCallback(response) {
            if(response.status == 403){
               var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
                              status.then(function(greeting) {
                                allBooking();
                       });
                // $scope.autoAirlines(value);
            }
            // called asynchronously if an error occurs
            // or server returns response with an error status.
          });
    }

    $scope.viewDetails = function(bookdata)
    {
        var token = JSON.parse(sessionStorage.authToken);
        var headersOptions = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token
        }
        $scope.artloading = true;
        $http.get(ServerService.serverPath + 'booking-dtls/I/'+bookdata.bookingRefNo, {
        
            headers: headersOptions
        }).then(function successCallback(response) {
            $scope.artloading = false;
            if(response.data.success)
            {
                sessionStorage.setItem('bookingItneryResponse',JSON.stringify(response.data));
                $rootScope.bookdata = bookdata;

                var modalInstance = $uibModal.open({
                    animation: $scope.animationEnabled,
                    ariaLabelledBy: 'modal-title',
                    ariaDescribedBy: 'modal-body',
                    templateUrl: 'ViewinsBookings.html',
                    controller: 'ViewinsBookingsController',
                    backdrop: true,
                    // size: size,
                    // appendTo: parentElem,
                    resolve: {
                        items: function() {
                            return $scope.items
                        }
                    }
                })
            }
            else
            {
                sessionStorage.removeItem('bookingItneryResponse');
                Lobibox.alert('error', {
                    msg: response.data.errorMessage
                });
            }
        }, function errorCallback(response) {
            if(response.status == 403){
               var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
                status.then(function(greeting) {
                    allBooking();
                });
                // $scope.autoAirlines(value);
            }
            // called asynchronously if an error occurs
            // or server returns response with an error status.
          });

    }
    $scope.issueTicket = function(agencyId)
    {
        var token = JSON.parse(sessionStorage.authToken);
        var headersOptions = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token
        }
        $scope.artloading = true;
        $http.get(ServerService.serverPath + 'insurance/credit/issueTicket/'+agencyId, {
        
            headers: headersOptions
        }).then(function successCallback(response) {
        $scope.artloading = false;
        if(response.data.success)
            {
                $scope.allBooking();
                Lobibox.alert('success', {
                    msg: response.data.message
                });
            }
            else
            {
                $scope.allBooking();
                Lobibox.alert('error', {
                    msg: response.data.errorMessage
                });
            }
        }, function errorCallback(response) {
            if(response.status == 403){
               var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
                status.then(function(greeting) {
                    allBooking();
                });
                // $scope.autoAirlines(value);
            }
            // called asynchronously if an error occurs
            // or server returns response with an error status.
          });
    }
    $scope.allBooking();

  
    $scope.pageChanged = function(page){
		$scope.curentpage = page;
	}

});
ouaApp.controller('ViewinsBookingsController', function($scope,refreshService, $uibModalInstance, $rootScope, $http, ServerService) {

    $scope.bookingData = $rootScope.bookdata;
    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };
});