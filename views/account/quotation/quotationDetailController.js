ouaApp.controller('quotationDetailController', function($scope, $http,renderHTMLFactory, $location, $uibModal, baseUrlService, expiryService, ConstantService, ServerService,$rootScope) {
    // var Serverurl = "http://localhost:8090/ibp/api/v1/";
    $scope.tokenresult = JSON.parse(sessionStorage.getItem('authentication'));
    // $scope.quotationGroup = JSON.parse(sessionStorage.getItem('sendQuoteData'));
    // $rootScope.isheaderdisabled = false;
    // $scope.$apply();
    $scope.renderHTMLFactory = renderHTMLFactory;	
    $scope.refrenceNo = $location.search().ref;

    // $scope.flightDetailsQuotation= $scope.quotationGroup;
    // var tokenname = $scope.tokenresult.data;
    $scope.eachFlightDetails = function(index, FlightDetails,childIndex) {
      
        $scope.flightDetailsQuotation[index].segGrpList[childIndex].isOpen = ! $scope.flightDetailsQuotation[index].segGrpList[childIndex].isOpen;
        $scope.scrollTraget = index;
        $scope.flightDetails1 = FlightDetails;
        $scope.segGrpListchildIndex = childIndex;

    }

    $scope.hotelQuotationLength = 0;
    $rootScope.cordinatorDetails = {}
    $scope.viewDetails=function(){
 
   
      $scope.tokenresult = JSON.parse(sessionStorage.getItem('authToken'));
      //  ajaxService.ajaxRequest('POST',ServerService.serverPath+'rest/repo / airport','{ "queryString" : "'+request.term+'" } ','json'),{ 
      var headersOptions = {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + $scope.tokenresult
      }
  
      $scope.artloading = true;
    //   /retrieve/quoteNo/{quotationNo}
      $http.get(ServerService.dropDownPath + 'quote/retrieve/quoteNo/'+
    //   71,{
      $scope.refrenceNo, {
      
          headers: headersOptions
      }).then(function successCallback(response) {
      $scope.artloading = false;
      if(response.data.success)
          {
              sessionStorage.setItem('bookingQuotationResponse',JSON.stringify(response.data));
              $scope.quotationData = JSON.parse(sessionStorage.getItem('bookingQuotationResponse'));
              $scope.quotationRes = response.data.data.serviceDetails.quotation;
    
    
            if($scope.quotationRes.hotel['hotel-items'] != undefined)
            {

                var hotelQuote = $scope.quotationRes.hotel['hotel-items'];

                // hotelQuote.forEach(element => {
                //     if(element.gblHotelQuatationList != undefined && element.gblHotelQuatationList.length>0){
                //         $scope.hotelQuotationLength++;
                //     }
                // });
        
                // $rootScope.cordinatorDetails.internalRemarks=$scope.quotationData.data.internalRemarks;  
                // $rootScope.cordinatorDetails.externalRemarks=$scope.quotationData.data.externalRemarks; 
                // $rootScope.cordinatorDetails.email=$scope.quotationData.data.toEmail;
                // $scope.selectTravellerData = $scope.quotationRes.hotel['quote-details'].paxProfiles;
                $scope.gblSelectedHotelResp = hotelQuote;
                // $scope.hotelQuotationLength = $scope.hotelQuotationLength;
                // $scope.selectTraveldata = JSON.parse(sessionStorage.getItem('selectTraveldata'));
                $scope.isDisabled = true;
                $scope.isRadioDisabled = false;
                $scope.product_type = "H";
            }else if($scope.quotationRes.flight['flight-items'] != undefined){
                $scope.product_type = "F";
                $scope.flightDetailsQuotation =$scope.quotationData.data.serviceDetails.quotation.flight['flight-items'];     
            }
            else{
                $scope.product_type = "I";
                $scope.selectedQuoteAvilplan=$scope.quotationData.data.serviceDetails.quotation.ins['ins-items'].availablePlans;
                $scope.selectedQuoteRiderplan=$scope.quotationData.data.serviceDetails.quotation.ins['ins-items'].upsellPlans;		
            }
  
  
          }
          else
          {
        
              Lobibox.alert('error', {
                  msg: response.data.errorMessage
              });
          }
      });
       
  }
  
  $scope.viewDetails();
    sessionStorage.removeItem('SelectedBagLst');
    sessionStorage.removeItem('SelectedMealLst');
    sessionStorage.removeItem('SelectedSeatLst');
    sessionStorage.removeItem("sendQuoteData");
    var priceStart=0;
    var priceEnd=0;
    $scope.hideprice=false;
    $scope.hidestar=false;
    $scope.departureTimehide=false;
    $scope.arrivelHide=false;
    $scope.airlinehideprice=false;
    $scope.fareTypeHide=false;
    $scope.baggageHide=false;


 



});

