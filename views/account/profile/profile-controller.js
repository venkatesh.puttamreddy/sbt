ouaApp.controller('userProfileCtrl', function($scope, $location,refreshService, $interval, $http, $timeout, $rootScope, ServerService, ajaxService, $window, NationService, CountryService, $uibModal, customSession) {
    $scope.data = '';
    $('body').attr('id', 'top');
    $('#top').addClass('thebg');
    NationService.nationGet();
    $("#tabcss").addClass("active");
    $scope.minDate = new Date();
    $scope.dobMinDate = new Date(new Date().setFullYear(new Date().getFullYear() - 18));
    $(function () {
		$('[data-toggle="tooltip"]').tooltip()
	  })
    var tokenresult = JSON.parse(localStorage.getItem('authentication'));
    var tokenname = tokenresult.data;
    var headersOptions = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + tokenname.authToken
        }

        $scope.communicationMobile=function(code){
            console.log(code);
            if(code=='9'){
                $scope.mobileCountryCode=true;
            }
                    }
    // Global Variables
    $scope.addressObj={};
    $scope.customer;
    $scope.traveller;
    $scope.phonenumbers = [];
    $scope.emails = [];
    $scope.freqflyerlist = [];
    $scope.ffChildObjEdit;
    $scope.phoneNumObjEdit;
    $scope.emailObjEdit;



    $scope.logged = JSON.parse(sessionStorage.getItem('loginName'));

    crudCountryList = function() {
    var tokenresult = JSON.parse(localStorage.getItem('authentication'));
    var tokenname = tokenresult.data;
    var headersOptions = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + tokenname.authToken
    }

        $http({
            method: 'GET',
            url: ServerService.listPath + 'list/country',
            headers: headersOptions
        }).then(function successCallback(data, status) {
            if (status = 'success') {
                localStorage.setItem('country', JSON.stringify(data.data));

                $rootScope.countryResult = data.data;
            } else {
            }
        }, function errorCallback(response) {
            if(response.status == 403){
                var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
                status.then(function(greeting) {
                    crudCountryList();
                   });                
            }
            });

    }

    getSeatperf = function(){
        var tokenresult = JSON.parse(localStorage.getItem('authentication'));
        var tokenname = tokenresult.data;
        var headersOptions = {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + tokenname.authToken
            }

        $http({
            method: 'GET',
            url: ServerService.listPath + 'common?codeType=SEAT_PREF',
            headers: headersOptions
        }).then(function successCallback(data, status) {
            if (status = 'success') {
                $rootScope.seatList = data.data.data;
                crudCountryList();
            } else {
            }
        }, function errorCallback(response) {
            if(response.status == 403){
                var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
                status.then(function(greeting) {
                    getSeatperf();
                   });                
            }
            });

    }
    getcodetype = function(){
        var tokenresult = JSON.parse(localStorage.getItem('authentication'));
        var tokenname = tokenresult.data;
        var headersOptions = {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + tokenname.authToken
            }
    $http({
        method: 'GET',
            url: ServerService.listPath + 'common?codeType=COMM_TYPE',
        // data: country
        // {
        headers: headersOptions
                // }
        }).then(function successCallback(data, status) {
            if (status = 'success') {
                $rootScope.communicationListList = data.data.data;
                getSeatperf();
            } else {
            }
        }, function errorCallback(response) {
            if(response.status == 403){
                var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
                status.then(function(greeting) {
                    getcodetype();
                   });                
            }
            });
    }
    getMealpef = function(){
        var tokenresult = JSON.parse(localStorage.getItem('authentication'));
        var tokenname = tokenresult.data;
        var headersOptions = {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + tokenname.authToken
            }
        $http({
            method: 'GET',
            url: ServerService.listPath + 'common?codeType=MEAL_PREF',
            // }
            headers: headersOptions
    }).then(function(data, status) {
        if (status = 'success') {
                $rootScope.mealList = data.data.data;
                getcodetype();
            } else {
            }
        }, function errorCallback(response) {
        if(response.status == 403){
            var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
            status.then(function(greeting) {
                getMealpef();
               });                
        }
        });
    }

    getRelatioship = function(){
        var tokenresult = JSON.parse(localStorage.getItem('authentication'));
        var tokenname = tokenresult.data;
        var headersOptions = {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + tokenname.authToken
            }

        $http({
            method: 'GET',
            url: ServerService.listPath + 'common?codeType=RELATIONSHIP',
                headers: headersOptions
        }).then(function successCallback(data, status) {
            if (status = 'success') {
                $rootScope.relationshipList = data.data.data;
                getMealpef();

        } else {
            // alert('error');
        }
        }, function errorCallback(response) {
            if(response.status == 403){
                var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
                status.then(function(greeting) {
                    getRelatioship();
    });
            }
            });
    }

    getLangList = function(){
        var tokenresult = JSON.parse(localStorage.getItem('authentication'));
        var tokenname = tokenresult.data;
        var headersOptions = {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + tokenname.authToken
            }

        $http({
            method: 'GET',
            url: ServerService.listPath + 'crud/list/language',
            headers: headersOptions
        }).then(function successCallback(response, status) {
            if (response.data.success) {
                $scope.PreferedLanguage = response.data.data;
                getRelatioship();
            } else {}
        }, function errorCallback(response) {
            if(response.status == 403){
                var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
                status.then(function(greeting) {
                    getLangList();
                   });                
            }
            });


    }
    getcountryDrpdwn = function(){
        var tokenresult = JSON.parse(localStorage.getItem('authentication'));
        var tokenname = tokenresult.data;
        var headersOptions = {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + tokenname.authToken
            }


        $http({
            method: 'GET',
            url: ServerService.listPath + 'list/country',
            headers: headersOptions
        }).then(function successCallback(response, status) {
            if (response.data.success) {
                $scope.countryList = response.data.data;
                getLangList();
            } else {}
        }, function errorCallback(response) {
            if(response.status == 403){
                var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
                status.then(function(greeting) {
                    getcountryDrpdwn();
                   });                
            }
            });
    }

    getgenderdrpdwn= function(){
        var tokenresult = JSON.parse(localStorage.getItem('authentication'));
        var tokenname = tokenresult.data;
        var headersOptions = {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + tokenname.authToken
            }

        $http({
            method: 'GET',
            url: ServerService.listPath + 'common?codeType=Gender',
            headers: headersOptions
        }).then(function successCallback(data, status) {
            if (data.data.success) {


                $rootScope.Profilegender = data.data.data;
                getcountryDrpdwn();
            } else {
            }
        }, function errorCallback(response) {
            if(response.status == 403){
                var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
                status.then(function(greeting) {
                    getgenderdrpdwn();
                   });                
            }
            });
    }
    getphnCountryDrpdwn = function(){
        var tokenresult = JSON.parse(localStorage.getItem('authentication'));
        var tokenname = tokenresult.data;
        var headersOptions = {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + tokenname.authToken
            }
            //  console.log('url11', ServerService.listPath + "list/phone-country");
        $http({
            type: "GET",
            url: ServerService.listPath + "list/phone-country",

            headers: headersOptions,

        }).then(function successCallback(data, status) {

                if (data.data.success) {
                    //   console.log('country code', data)
                    $rootScope.countrycode = data.data.data;
                    getgenderdrpdwn();
                } else {
                    // alert('error');
                }
            }, function errorCallback(response) {
                if(response.status == 403){
                    var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
                    status.then(function(greeting) {
                        getphnCountryDrpdwn();
            });




            // data: country
            // {
                // }


                // alert('error');
            }
        });
    }

    userRetriew = function(){
        var tokenresult = JSON.parse(localStorage.getItem('authentication'));
        var tokenname = tokenresult.data;
        var headersOptions = {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + tokenname.authToken
            }
        $http.get(ServerService.authPath + 'user/retrieve', {
            // data: country
            // {
            headers: headersOptions
                // }
        }).then(function successCallback(response) {

            $scope.profileInfoData = response.data.data;

            for(var c=0;c< $scope.profileInfoData.companyCustomers.length; c++){
                if($scope.profileInfoData.companyCustomers[c].customerId==$scope.profileInfoData.companyCustomers[c].parentId){
                    $scope.profileInfoData.salutation = $scope.profileInfoData.companyCustomers[c].salutation;
                    $scope.addressObj=$scope.profileInfoData.companyCustomers[c].customerAddresses[0];
                    $scope.profileInfoData.firstName = $scope.profileInfoData.companyCustomers[c].firstName;
                    $scope.profileInfoData.lastName = $scope.profileInfoData.companyCustomers[c].lastName;
                    $scope.profileInfoData.userEmail = $scope.profileInfoData.companyCustomers[c].emailId;
                    $scope.profileInfoData.passportIssueGovt = $scope.profileInfoData.companyCustomers[c].passportIssueGovt;
                    $scope.profileInfoData.languagePref1 = $scope.profileInfoData.companyCustomers[c].languagePref1;
                    $scope.profileInfoData.languagePref2 = $scope.profileInfoData.companyCustomers[c].languagePref2;
                    $scope.customer=$scope.profileInfoData.companyCustomers[c].customerPreferences[0];
		$scope.traveller=$scope.profileInfoData.companyCustomers[c].customerPreferences[1];
                    $scope.profileInfoData.parentRelation = $scope.profileInfoData.companyCustomers[c].parentRelation;
                    $scope.profileInfoData.countryCode = $scope.profileInfoData.companyCustomers[c].countryCode;
                    $scope.profileInfoData.phoneNumber = $scope.profileInfoData.companyCustomers[c].mobileNo;
                    $scope.profileInfoData.mobileNo = $scope.profileInfoData.companyCustomers[c].mobileNo;
                    $scope.freqflyerlist= $scope.profileInfoData.companyCustomers[c].customerFfDtls;
                    $scope.communication= $scope.profileInfoData.companyCustomers[c].customerCommunications;
                    $scope.phonenumbers=[]
                    $scope.emails=[]
                    for(var p=0; p<$scope.communication.length; p++){
                        if($scope.communication[p].type=='9' || $scope.communication[p].type=='10' || $scope.communication[p].type=='10'){
            $scope.phonenumbers.push($scope.communication[p]);
                        }else{
                            $scope.emails.push($scope.communication[p]);
                        }
                    }
                    $scope.dob1=$scope.profileInfoData.companyCustomers[c].dateOfBirth;
                    
		var dob=$scope.dob1.split("/");
		 var year=dob[2];
		 var monnth=dob[1];
		 var date=dob[0];
		 $scope.profileInfoData.dateOfBirth=new Date(year+"/"+monnth+"/"+date)
                    $scope.profileInfoData.gender = $scope.profileInfoData.companyCustomers[c].gender;
                    $scope.profileInfoData.passportNo = $scope.profileInfoData.companyCustomers[c].passportNo;
                    $scope.profileInfoData.nationality = $scope.profileInfoData.companyCustomers[c].nationality;
                    $scope.issuedob= $scope.profileInfoData.companyCustomers[c].passportIssueDate;
                if($scope.issuedob!=null){
                    var issuedatedob=$scope.issuedob.split("/");
                    var issueyear=issuedatedob[2];
                    var issuemonnth=issuedatedob[1];
                    var issuedate=issuedatedob[0];
                    $scope.profileInfoData.passportIssueDate=new Date(issueyear+"/"+issuemonnth+"/"+issuedate)
                }else{
                    $scope.profileInfoData.passportIssueDate=  $scope.issuedob
                }
                    $scope.expirdob= $scope.profileInfoData.companyCustomers[c].passportExpiryDate;

                    if($scope.expirdob!=null){     
		var expiredatedob=$scope.expirdob.split("/");
		var eyear=expiredatedob[2];
		var emonnth=expiredatedob[1];
		var edate=expiredatedob[0];
		$scope.profileInfoData.passportExpiryDate=new Date(eyear+"/"+emonnth+"/"+edate)
                    }else{
                        $scope.profileInfoData.passportExpiryDate= $scope.expirdob;
                    }
                }
                        }
                     
                        
              


            getphnCountryDrpdwn();

            // data: country
            // {
                // }
        }, function errorCallback(response) {
            if(response.status == 403){
                var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
                status.then(function(greeting) {
                    userRetriew();
        });





            // data: country
            // {
                // }


                // alert('error');
            }
        });


            // data: country
            // {
                // }


                // alert('error');
            }

    getUser = function(){
    var tokenresult = JSON.parse(localStorage.getItem('authentication'));
    var tokenname = tokenresult.data;
            // data: country
            // {
    var headersOptions = {
                // }
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + tokenname.authToken
                // alert('error');
            }



        $http({
            method: 'GET',
        url: ServerService.authPath + 'user-details',
            // data: country
            // {
            headers: headersOptions
                // }
    }).then(function successCallback(data, status) {
            if (status = 'success') {


            $scope.userId = data.data.userId;
            userRetriew();
            } else {
                // alert('error');
            }
    }, function errorCallback(response) {
        if(response.status == 403){
            var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
            status.then(function(greeting) {
                getUser();
        });



            // data: country
            // {
                // }

                // alert('error');
            }
        });

    }
    getUser();
    $scope.userServiceURL = ServerService.profilePath;

    $scope.userId = 2;

    $scope.salutationData = [{ label: 'Mr', value: 'Mr' }, { label: 'Ms', value: 'Ms' }, { label: 'Mrs', value: 'Mrs' }];


    $scope.salutationDat = function(item) {

        var title = item.salutation;
        if (title == 'MR' || title == 'MASTER') {
            $scope.profileInfoData.gender = 'M';
        } else if (title == 'MS' || title == 'MISS' || title == 'MRS') {
            $scope.profileInfoData.gender = 'F';
        } else {
            $scope.profileInfoData.gender = '';
        }
    }



$scope.dateChangeFunction=function(){
    $scope.passportMinDate = $scope.profileInfoData.dateOfBirth;

   
}



    // $scope.airlines = [];
    // $scope.airlineCode = [];



    $scope.autoAirlines = function(value) {
        var tokenresult = JSON.parse(localStorage.getItem('authentication'));
        var tokenname = tokenresult.data;
        var headersOptions = {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + tokenname.authToken
            }
       
            $http.get(ServerService.serverPath + 'flight/airlines/' + value.toUpperCase(), {
                headers: headersOptions
            }).then(function successCallback(response, status) {
                $scope.airlines = [];
                $scope.airlineCode = [];
                var airlist = response.data.data
                if(airlist==null){
					$("#profile_airline").val('');
					$scope.airlineName;
				   }
                if (response.data.data != 'null' && response.data.data.length > 0) {
                    for (var i = 0; i < response.data.data.length; i++) {
                        var airfrom = {
                            Title: response.data.data[i].displayname + ' - ' + '(' + response.data.data[i].shortName + ')'

                        }
                        var aircode = {
                            Code: response.data.data[i].shortName
                        }
                        $scope.airlines.push(airfrom);
                        $scope.airlineCode.push(aircode)

                    }
                    $scope.airlines.sort();
                    $scope.airlineCode.sort();
                }


            }, function errorCallback(response) {
                if(response.status == 403){
                    var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
                    status.then(function(greeting) {
                        $scope.autoAirlines(value);
                       });                
                }
                });
        


    }


    $scope.airCodeChange = function(value) {
        $scope.autoAirlines(value)
    }








    $scope.addFrequentFlyer = function() {
        var isValid = true;
        var frequentflyerObj = null;
        var mode = 'insert';
        if ($scope.ffChildObjEdit == null) {
            frequentflyerObj = new Object();
        } else {
            frequentflyerObj = $scope.ffChildObjEdit;
            mode = 'update';
        }
        //var ffairlinename = $('#profile_airline').val();

        //var ffnumber = $('#profile_flyerNo').val();

        //var ffairline = $('#profileAirlineCode').val();		

        if ($scope.airlineName == "" || $scope.airlineName == null) {
            Lobibox.notify('error', { size: 'mini', delay: 5000,   position: 'right top', msg: "Please Choose Airline" });
            isValid = false;
        } else if ($scope.airCodes == "" || $scope.airCodes == null) {
            Lobibox.notify('error', { size: 'mini', delay: 5000,  position: 'right top', msg: "Please Enter Flyer No" });
            isValid = false;
        } else {
            var ffairlinename = $scope.airlineName.Title;
            var ffnumber = $scope.airCodes;
           $scope.profile_AirlineCode = ffairlinename.split('(')[1].substring(0, 2)
            var ffairline = $scope.profile_AirlineCode;
            var orgairlineffname = ffairlinename.indexOf('(');
            var airlineffname = ffairlinename.substr(0,(orgairlineffname-2));
            if (mode == 'insert') {
                if (!checkFFDtlDuplicate(ffairline)) {
                    isValid = false;
                    Lobibox.notify('warning', { size: 'mini', delay: 5000,  position: 'right top', msg: "Please Enter Flyer No" });
                }
            }
            if (isValid == true) {
                frequentflyerObj.airline = $scope.profile_AirlineCode;
                frequentflyerObj.orgarlinename = airlineffname; 
                frequentflyerObj.ffNo = ffnumber;
                clearFFFieldValues();
                if (mode == 'insert') {
                    $scope.freqflyerlist.push(frequentflyerObj);
                    Lobibox.notify('success', { size: 'mini', delay: 5000,  position: 'right top', msg: "Airline Added Successfully" });
                } else {
                    Lobibox.notify('success', { size: 'mini', delay: 5000,   position: 'right top', msg: "Airline Updated Successfully" });
                    $scope.ffChildObjEdit = null;
                }
            }
        }
    };





    function checkFFDtlDuplicate(currValue) {
        var isValid = true;
        if ($scope.freqflyerlist.length != 0) {
            for (var i = 0, size = $scope.freqflyerlist.length; i < size; i++) {
                var value = $scope.freqflyerlist[i].ffairline;
                if (currValue == value) {
                    isValid = false;
                }
            }
        }
        return isValid;
    }



    $scope.addCommunicationProfile = function() {
        var isValid = true;
        var mode = 'insert';
        var phonenumberObj = null;
        var emailObj = null;
        var commtypecode = $('#profileContactType').val();
        var commvalue = $('#profileContactValue').val();
        var countrycode = $('#profileContactValuecode').val();

        if (commtypecode == "") {
            Lobibox.notify('error', { size: 'mini', delay: 5000,   position: 'right top',msg: "Please Enter The Contact Type" });
            isValid = false;
        } else if (commvalue == "") {
            Lobibox.notify('error', { size: 'mini', delay: 5000,   position: 'right top', msg: "Please Enter The Value" });
            isValid = false;
        } else {
            if (commtypecode == "7" || commtypecode == "8") {
                if (!validEmail(commvalue)) {
                    Lobibox.notify('error', { size: 'mini', delay: 5000,  position: 'right top', msg: "Please Enter Correct email" });
                    isValid = false;
                } else {
                    if ($scope.emailObjEdit == null) {
                        if (!validateDuplicateEmail(commvalue)) {
                            Lobibox.notify('warning', { size: 'mini', delay: 5000,  position: 'right top', msg: "Email Is Already Added" });
                            isValid = false;
                        } else {
                            emailObj = new Object();
                            emailObj.communicationId = commtypecode;
                            emailObj.commValue = commvalue;
                            emailObj.isPreferred = "N";
                        }
                    } else {
                        mode = 'update';
                        emailObj = $scope.emailObjEdit;
                        emailObj.communicationId = commtypecode;
                        emailObj.commValue = commvalue;
                    }
                }
                if (isValid == true) {
                    clrCommunicationFlds();
                    $scope.emailObjEdit = null;
                    if (mode == 'insert') {
                        $scope.emails.push(emailObj);
                        Lobibox.notify('success', { size: 'mini', delay: 5000,  position: 'right top', msg: "Email Added Successfully" });
                    } else {
                        Lobibox.notify('success', { size: 'mini', delay: 5000,  position: 'right top', msg: "Email Updated Successfully" });

                    }
                }
            } else {
                if (!isNumber(commvalue)) {
                    Lobibox.notify('error', { size: 'mini', delay: 5000,  position: 'right top', msg: "Please Enter Correct number" });
                    isValid = false;
                } else {
                    if ($scope.phoneNumObjEdit == null) {
                        if (validateDuplicateContact(commvalue) == false) {
                            Lobibox.notify('warning', { size: 'mini', delay: 5000,  position: 'right top', msg: "Number Is Already Added" });
                            isValid = false;
                        } else {
                            phonenumberObj = new Object();
                            phonenumberObj.communicationId = commtypecode;
                            phonenumberObj.countryCode = countrycode;		
                            phonenumberObj.commValue = commvalue;
                            phonenumberObj.isPreferred = "N";
                        }
                    } else {
                        phonenumberObj = $scope.phoneNumObjEdit;
                        mode = 'update';
                        phonenumberObj.communicationId = commtypecode;
                        phonenumberObj.countryCode = countrycode;		
                        phonenumberObj.commValue = commvalue;
                    }
                    if (isValid == true) {
                        clrCommunicationFlds();
                        if (mode == 'insert') {
                            $scope.phonenumbers.push(phonenumberObj);

                            Lobibox.notify('success', { size: 'mini', delay: 5000,  position: 'right top', msg: "PhoneNumber Added Successfully" });
                        } else {
                            Lobibox.notify('success', { size: 'mini', delay: 5000,  position: 'right top', msg: "PhoneNumber Updated Successfully" });
                            $scope.phoneNumObjEdit = null;
                        }
                    }
                }
            }
        }
    };



    function clrCommunicationFlds() {
        $('#profileContactType').val("");
        $('#profileContactValue').val("");
        $('#addCommunication').show();
        $('#updateCommunication').hide();
    }
    $scope.removePhoneNumbers = function(phonenumber) {
        $scope.phonenumbers.pop(phonenumber);
    };

    $scope.removeEmails = function(email) {
        $scope.emails.pop(email);
    };

    function validEmail(v) {
        var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
        return pattern.test(v);
    }
    // Check The Variable Is A Number

    function isNumber(n) {
        return !isNaN(parseFloat(n)) && isFinite(n);
    }
    // Check Whether The Phone Number Is Already Added

    function validateDuplicateContact(currValue) {
        var isValid = true;
        if ($scope.phonenumbers.length != 0) {
            for (var i = 0, size = $scope.phonenumbers.length; i < size; i++) {
                var value = $scope.phonenumbers[i].commvalue;
                if (currValue == value) {
                    isValid = false;
                }
            }
        }
        return isValid;
    }
    // Check Whether Email Is Already Added

    function validateDuplicateEmail(currValue) {
        var isValid = true;
        if ($scope.emails.length != 0) {
            for (var i = 0, size = $scope.emails.length; i < size; i++) {
                var value = $scope.emails[i].commvalue;
                if (currValue == value) {
                    isValid = false;
                }
            }
        }
        return isValid;
    }


    function clearFFFieldValues() {
        $('#profile_flyerNo').val("");
        $('#profileFlyerNo').val("");
        $('#profile_airline').val("");
        $('#profileAirlineName').val("");
        $('#profileAirlineCode').val("");
        $('#profile_AirlineCode').val("");
        $('#addFrequentFlyer').show();
    }
    $scope.removeFrequentFlyer = function(frequentflyer) {
        $scope.freqflyerlist.pop(frequentflyer);
    };

    var reminder2 = "";
    jQuery('#prefered_airline').keyup(function(e) {
        $scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));
        var tokenname = $scope.tokenresult.data;
        //  ajaxService.ajaxRequest('POST',ServerService.serverPath+'rest/repo / airport','{ "queryString" : "'+request.term+'" } ','json'),{ 
        var headersOptions = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + tokenname.authToken
        }

        // console.log(jQuery(this).val().toUpperCase());
        if (jQuery(this).val().length == 2) {
            $http.get(ServerService.serverPath + 'flight/airlines/' + jQuery(this).val().toUpperCase(), {
                headers: headersOptions
            }).then(function successCallback(result, status) {
                var availableTags = [];
                var data = result.data.data;
                for (var i = 0; i < data.length; i++) {
                    var prefcount = 0;
                    var contentcount = 0;
                    var checkCity4 = data[i].shortName;
                    var checkCity1 = checkCity4.toLowerCase();
                    var checkCity2 = data[i].displayname + ' (' + data[i].shortName + ')';
                    $scope.profileInfoData.airline = checkCity2;
                }


            }, function errorCallback(response) {
                if(response.status == 403){
                    var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
                    status.then(function(greeting) {
                       });                
                }
                });
        }
    });



    $scope.customerAddresses = [];
    $scope.customerPreferences = [];

    createCustomerRQ = function(){
        $scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));
        var tokenname = $scope.tokenresult.data;
        var headersOptions = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + tokenname.authToken
        }
        $http({
            method: 'POST',
            url: ServerService.profilePath + 'customer/create',
            headers: headersOptions,
            data: $scope.profilesaveData
        }).then(function successCallback(data, status) {
            if (data.data.success) {
                var jsondata = data.data;
                sessionStorage.setItem("profileJsondata", JSON.stringify(jsondata));
                $location.path('/profile');
                $('body').removeClass('loading');
                Lobibox.alert('success', {
                    
                    msg: data.data.message
                });
                $timeout(function() {
                }, 500);
            } else {
                $location.path('/profile');
                Lobibox.alert('error', {
                    
                    msg: data.data.errorMessage
                });
            }
        }, function errorCallback(response) {
            if(response.status == 403){
                var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
                status.then(function(greeting) {
                    createCustomerRQ();
                   });                
            }
            });
    }

    $scope.ownProfileUpdate = function() {
        
        $scope.profilesaveData={};



        $scope.profilesaveData.salutation = $scope.profileInfoData.salutation;
        for(var c=0;c< $scope.profileInfoData.companyCustomers.length; c++){
if($scope.profileInfoData.companyCustomers[c].customerId==$scope.profileInfoData.companyCustomers[c].parentId){
    $scope.profilesaveData.customerId=$scope.profileInfoData.companyCustomers[c].customerId;
}
        }
     
        $scope.profilesaveData.firstName = $scope.profileInfoData.firstName;
        $scope.profilesaveData.lastName = $scope.profileInfoData.lastName;
        $scope.profilesaveData.userEmail = $scope.profileInfoData.userEmail;
        $scope.profilesaveData.countryCode = $scope.profileInfoData.countryCode;
        $scope.profilesaveData.mobileNo = $scope.profileInfoData.mobileNo;
        var dateOfBirth= $scope.profileInfoData.dateOfBirth.toLocaleDateString();
         var profiledateOfBirth=dateOfBirth.split('/')[1]+'/'+dateOfBirth.split('/')[0]+'/'+dateOfBirth.split('/')[2];
        $scope.profilesaveData.dateOfBirth=profiledateOfBirth;
        $scope.profilesaveData.gender = $scope.profileInfoData.gender;
        $scope.profilesaveData.passportNo = $scope.profileInfoData.passportNo;
        $scope.profilesaveData.nationality = $scope.profileInfoData.nationality;
        if($scope.profileInfoData.passportIssueDate!=null){
            var passportIssue= $scope.profileInfoData.passportIssueDate.toLocaleDateString();
            var passportIssueDate=passportIssue.split('/')[1]+'/'+passportIssue.split('/')[0]+'/'+passportIssue.split('/')[2];
            $scope.profilesaveData.passportIssueDate = passportIssueDate;
        }else{
            $scope.profilesaveData.passportIssueDate = $scope.profileInfoData.passportIssueDate; 
        }
        if($scope.profileInfoData.passportExpiryDate!=null){
        var passportExpiry= $scope.profileInfoData.passportExpiryDate.toLocaleDateString();
        var passportExpiryDate=passportExpiry.split('/')[1]+'/'+passportExpiry.split('/')[0]+'/'+passportExpiry.split('/')[2];
        $scope.profilesaveData.passportExpiryDate = passportExpiryDate;
        }else{
            $scope.profilesaveData.passportExpiryDate = $scope.profileInfoData.passportExpiryDate; 
        }
        $scope.profilesaveData.customerPreferences = [];
        $scope.profilesaveData.customerAddresses = [];
        $scope.profilesaveData.customerCommuncnDtls = [];
        $scope.profilesaveData.customerFfDtls = [];

        $scope.totalCommunicationData = $scope.phonenumbers.concat($scope.emails);

        // $scope.profilesaveData.customerAddresses.push($scope.addressObj)
        $scope.profilesaveData.customerFfDtls = $scope.freqflyerlist;
        $scope.profilesaveData.customerCommuncnDtls = $scope.totalCommunicationData;
       

        if($scope.customer==undefined||angular.equals($scope.customer, {})){
            $scope.profilesaveData.customerPreferences = [];
        }else{
					  $scope.profilesaveData.customerPreferences.push($scope.customer)
        }


        if($scope.addressObj==undefined||angular.equals($scope.addressObj, {})){
            $scope.profilesaveData.customerAddresses = [];
        }else{
			
				$scope.profilesaveData.customerAddresses.push($scope.addressObj)
			
		
        }

        // if($scope.addressObj==undefined){
        //     $scope.profilesaveData.customerAddresses = [];
        // }else{
		// 	$scope.profilesaveData.customerAddresses.push($scope.addressObj)
        // }
        // if($scope.traveller==undefined){
        //     $scope.profilesaveData.customerPreferences = [];
        // }else{
        // $scope.profilesaveData.customerPreferences.push($scope.traveller)
        // }

        if($scope.traveller==undefined ||angular.equals($scope.traveller, {})){
            $scope.profilesaveData.customerPreferences = [];
            if($scope.customer!=undefined){
			// if($scope.customer.custPrefType==$scope.customer.custPrefType){
				$scope.profilesaveData.customerPreferences.push($scope.customer)
			 }
        }else{
					  $scope.profilesaveData.customerPreferences.push($scope.traveller)
        }



       
        $scope.profilesaveData.parentRelation = $scope.profileInfoData.parentRelation;
       
        $scope.profilesaveData.shortName = "";
        $scope.profilesaveData.designation = "",
         
            $scope.profilesaveData.languagePref1 = $scope.profileInfoData.languagePref1,
            $scope.profilesaveData.languagePref2 = $scope.profileInfoData.languagePref2,
            $scope.profilesaveData.maritialStatus = "";
       
        $scope.profilesaveData.religion = "",
        $scope.profilesaveData.emailId = $scope.profileInfoData.userEmail;
            $scope.profilesaveData.userId = $scope.userId;
        
        
       
        $scope.profilesaveData.passengerType = "";
       
        
        $scope.profilesaveData.passportIssueGovt = $scope.profileInfoData.passportIssueGovt;
        
        $scope.profilesaveData.countryResidence = $scope.profileInfoData.countryResidence;
        $scope.profilesaveData.otherLanguagePref = "";
        $scope.profilesaveData.otherLanguagePref2 = "";
        $scope.profilesaveData.newsLetter = "",
            $scope.profilesaveData.saudiNationalityId = "";
            $scope.profilesaveData.airline = $scope.profileInfoData.airline;
       

            createCustomerRQ();


                        //$routeSegment.chain[0].reload();
                        //location.reload(true);


    };
    






});