"use strict";
ouaApp.controller( 'userTravellerCtrl' , function( $scope,refreshService, $location, $interval, $http, $timeout,$rootScope, ServerService,$uibModal, ajaxService,$window,NationService,CountryService) {
	$scope.data = '';
	$('body').attr('id','top');
	$('#top').addClass('thebg');
	NationService.nationGet();
	CountryService.countryGet();
	var id=$rootScope.userid;
	

	$scope.userServiceURL=ServerService.profilePath;
	$scope.logged = JSON.parse(localStorage.getItem('loginName'));
	$scope.travellerdata = JSON.parse(sessionStorage.getItem("travellerdata"));
	$scope.logindata = JSON.parse(localStorage.getItem("logindata"));
	$scope.minDate = new Date();

	 $scope.maxDate=new Date();
	 $scope.maxDate.setFullYear(2070);
	 $scope.maxDate.setMonth(11);
	 $scope.maxDate.setDate(31);
	

	$scope.dateChangeFunction=function(){
		$scope.passportMinDate = $scope.travellerObj.dateOfBirth;
	
	   
	}
	
	$(function () {
		$('[data-toggle="tooltip"]').tooltip()
	  })
	function retriveUser (){
		  	var tokenresult = JSON.parse(localStorage.getItem('authentication'));
		var tokenname = tokenresult.data;
		var headersOptions = {
			'Content-Type': 'application/json',
			'Authorization': 'Bearer ' + tokenname.authToken
		}
	
	
		$http.get(ServerService.authPath+'user/retrieve',{ 	
			headers: headersOptions
		}).then(function successCallback(response){
		$scope.travellerInfoData=response.data.data.companyCustomers;
	}, function errorCallback(response) {
		if(response.status == 403){
			var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
			status.then(function(greeting) {
				retriveUser();
			});
		}
	  });
	}
	function getUserDtls(){
		var tokenresult = JSON.parse(localStorage.getItem('authentication'));
		var tokenname = tokenresult.data;
		var headersOptions = {
			'Content-Type': 'application/json',
			'Authorization': 'Bearer ' + tokenname.authToken
		}
	
			$http({
			method: 'GET',
			url: ServerService.authPath + 'user-details',
			// data: country
			// {
				headers:headersOptions
			// }
		}).then(function successCallback(data, status) {
			if (status = 'success') {
				
				
				$scope.userId = data.data.userId;
				retriveUser();
				
				
	
	
	
				} else {
				alert('error');
			}
		}, function errorCallback(response) {
            if(response.status == 403){
                var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
                status.then(function(greeting) {
                    getUserDtls();
		});
            }
          });
	}
	getUserDtls();
		
	$scope.deleteTraveller = function (deleteRqUserId) {
		var tokenresult = JSON.parse(localStorage.getItem('authentication'));
		var tokenname = tokenresult.data;
		var headersOptions = {
			'Content-Type': 'application/json',
			'Authorization': 'Bearer ' + tokenname.authToken
		}
		Lobibox.confirm({
			title : '&nbsp;',
			iconClass: false,
			msg:'Are you sure want to Delete the Traveller?',
			callback: function ($this, type, ev) {
				if(type == 'yes') {
					$('body').addClass("loading");
					$http.delete(ServerService.profilePath+'customer/delete/'+	deleteRqUserId,{ 	
						headers: headersOptions
					}).then(function successCallback(response){
						if(response.data.message=="success") {
							retriveUser();
							Lobibox.alert('success',
							{
								msg: 'Record Deleted Successfully.'
							});
							
							} else {
							Lobibox.alert('error',
							{
								msg: "Unable to Delete Record."
							});
						}
				}, function errorCallback(response) {
					if(response.status == 403){
						var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
						status.then(function(greeting) {
							retriveUser();
						});
					}
				  });
					
				}
			}
		});
	}
		
			/* for update and edit traveller function */
	var travellerOpts;
	$scope.editTraveller = function(size, parentElem, travellerObj, addressObjProfile){
	if(travellerObj != null){
	$rootScope.editMode = true;
		travellerOpts = JSON.stringify(travellerObj)
	sessionStorage.setItem('travellerOpts', travellerOpts);
	} else {
	$rootScope.editMode = false;
		travellerOpts = travellerObj +','+ addressObjProfile
	sessionStorage.setItem('travellerOpts', travellerOpts);
		}
	
		 var modalInstance = $uibModal.open({
			animation: $scope.animationEnabled,
			ariaLabelledBy : 'modal-title',
			ariaDescribedBy : 'modal-body',
			templateUrl : 'travellerPage.html',
			controller : 'travellerDetailController',
			backdrop: 'static',
			keyboard: true,
			size : size,
			windowClass: 'traveller-modal',
			appendTo : parentElem,
			resolve: {
				items: function(){
					return $scope.items
				}
			}
		})
	}
		
		
		
		
		
		
		
		
		
});


ouaApp.controller('travellerDetailController', function($scope,refreshService, NationService,$uibModal, $location, CountryService, $http, ServerService, $rootScope, $timeout, $uibModalInstance){
	$scope.salutationData = [{label: "4189", value: "Mr"}, {label: "4191", value: "Ms"}, {label: "4190", value: "Mrs"}, {label: "4188", value: "Mstr"}, {label: "4192", value: "Miss"}];
	
	$scope.emails = [];
	$scope.freqflyerlist = [];
	$scope.phonenumbers = [];
	$scope.addressObj={};
	$scope.customer={};
	$scope.traveller={};
	$scope.phonenumbers = [];
	$scope.emails = [];
	$scope.freqflyerlist = [];
	$scope.ffChildObjEdit;
	$scope.phoneNumObjEdit;
	$scope.emailObjEdit;
	var givenDate ;
	$scope.travellerObj;
	$scope.modelDismiss = function() {
		$uibModalInstance.close();
	}
	var travellerOpts = sessionStorage.getItem('travellerOpts');
	if($rootScope.editMode == true){
		var travellerObj = JSON.parse(travellerOpts)
		$scope.travellerObj =travellerObj;
		$scope.travellerObj.salutation=$scope.travellerObj.salutation;
	 
		if($scope.travellerObj.customerAddresses[0]!=null)
		$scope.addressObj=$scope.travellerObj.customerAddresses[0];
		if($scope.travellerObj.customerPreferences[0]!=null)
		$scope.customer=$scope.travellerObj.customerPreferences[0];
		if($scope.travellerObj.customerPreferences[1]!=null)
		$scope.traveller=$scope.travellerObj.customerPreferences[1];
		$scope.freqflyerlist=$scope.travellerObj.customerFfDtls;
		$scope.communication=$scope.travellerObj.customerCommunications;
		$scope.phonenumbers=[]
		$scope.emails=[]
		for(var p=0; p<$scope.communication.length; p++){
			if($scope.communication[p].type=='9' || $scope.communication[p].type=='10' || $scope.communication[p].type=='10'){
$scope.phonenumbers.push($scope.communication[p]);
			}else{
				$scope.emails.push($scope.communication[p]);
			}
		}
	
		
		 $scope.dob1 = $scope.travellerObj.dateOfBirth;
		 if( $scope.dob1!=null){
		   var dob=$scope.dob1.split("/");
		    var year=dob[2];
		    var monnth=dob[1];
		   var date=dob[0];
		 }
		    $scope.travellerObj.dateOfBirth=new Date(year+"/"+monnth+"/"+date)
	//   $scope.travellerObj.dateOfBirth=	$scope.dob1 
		$scope.expirdob = $scope.travellerObj.passportExpiryDate;
		if($scope.expirdob!=null){
		var expiredatedob=$scope.expirdob.split("/");
		var eyear=expiredatedob[2];
		var emonnth=expiredatedob[1];
		var edate=expiredatedob[0];
		}
		$scope.travellerObj.passportExpiryDate=new Date(eyear+"/"+emonnth+"/"+edate)

		$scope.issuedob = $scope.travellerObj.passportIssueDate;
		if($scope.issuedob!=null){
		var issuedatedob=$scope.issuedob.split("/");
		var issueyear=issuedatedob[2];
		var issuemonnth=issuedatedob[1];
		var issuedate=issuedatedob[0];
}
if($scope.travellerObj.customerId==$scope.travellerObj.parentId){
	$scope.isDisabled=true;
}
		$scope.travellerObj.passportIssueDate=new Date(issueyear+"/"+issuemonnth+"/"+issuedate)
		$scope.travellerObj.passportIssueGovt=$scope.travellerObj.passportIssueGovt;
		$scope.travellerObj.countryCode=$scope.travellerObj.countryCode;
		
		givenDate = $scope.dob1 
		//$scope.dob = $scope.dob1;
	} else {
	var travellerObj = travellerOpts.split(',')
	$scope.travellerObj=travellerObj[0];
	$scope.travellerObj = {};
		//$scope.addressObjProfile=travellerObj[1];
	}
	/* var travellerObj = travellerOpts.split(',')
	$scope.travellerObj=travellerObj[0];
		$scope.addressObjProfile=travellerObj[1]; */
		$scope.data = '';
	$('body').attr('id','top');
	$('#top').addClass('thebg');
	NationService.nationGet();
	CountryService.countryGet();


		  	var tokenresult = JSON.parse(localStorage.getItem('authentication'));
		var tokenname = tokenresult.data;
		var headersOptions = {
			'Content-Type': 'application/json',
			'Authorization': 'Bearer ' + tokenname.authToken
		}
		
	

		
		
	 
	  $(document).ready(function () {  
	  	var tokenresult = JSON.parse(localStorage.getItem('authentication'));
		var tokenname = tokenresult.data;
		var headersOptions = {
			'Content-Type': 'application/json',
			'Authorization': 'Bearer ' + tokenname.authToken
		}
	
		$http({  
			type: "GET",  
			url: ServerService.listPath+"/list/phone-country",
				headers:headersOptions,
			}).then( function (data,status) {  
				if(status ='success') {
					$rootScope.phonecode = data.data;
					console.log($rootScope.countrycode)
					} else {
					alert('error');
				}	
			});  
		
		
		
		
			$http({
				method: 'GET',
			url: ServerService.listPath + 'common?codeType=Gender',
			// data: country
			// {
				headers:headersOptions
			// }
		}).then(function (data, status) {
			if (status = 'success') {
		
				
				$rootScope.Profilegender = data.data.data;
			} else {
				alert('error');
			}
		});
		
		
			
			$http({
				method: 'GET',
			url: ServerService.listPath + 'list/country',
			// data: country
			// {
				headers:headersOptions
			// }
		}).then(function (response, status) {
			if (response.data.success) {
					$scope.countryList = response.data.data;
				} else {
				}
		});
		
				$http({
				method: 'GET',
				url: ServerService.listPath + 'crud/list/language',
			// data: country
			// {
				headers:headersOptions
			// }
		}).then(function (response, status) {
			if (response.data.success) {
					$scope.PreferedLanguage = response.data.data;
				} else {
				}
		});
		
		
				
		
		
		$http({
				method: 'GET',
			url: ServerService.listPath + 'common?codeType=RELATIONSHIP',
			// data: country
			// {
				headers:headersOptions
			// }
		}).then(function (data, status) {
			if (status = 'success') {
		
				
				$rootScope.relationshipList = data.data.data;
			} else {
				alert('error');
			}
		});
		
		
			$http({
				method: 'GET',
			url: ServerService.listPath + 'common?codeType=MEAL_PREF',
			// data: country
			// {
				headers:headersOptions
			// }
		}).then(function (data, status) {
			if (status = 'success') {
		
				
				$rootScope.mealList = data.data.data;
			} else {
				alert('error');
			}
		});
		
		
			
			$http({
				method: 'GET',
			url: ServerService.listPath + 'common?codeType=COMM_TYPE',
			// data: country
			// {
				headers:headersOptions
			// }
		}).then(function (data, status) {
			if (status = 'success') {
		
				
				$rootScope.communicationListList = data.data.data;
			

			} else {
				alert('error');
			}
		});
		
		
		
		$http({
			method: 'GET',
		url: ServerService.listPath + 'list/phone-country',
		// data: country
		// {
			headers:headersOptions
		// }
	}).then(function (response, status) {
		if (response.data.success) {
	
			
			$scope.phonecode = response.data.data;
		} else {
			// alert('error');
		}
	});



				$http({
					method: 'GET',
			url: ServerService.listPath + 'common?codeType=SEAT_PREF',
			// data: country
			// {
				headers:headersOptions
			// }
		}).then(function (data, status) {
			if (status = 'success') {
		
				
				$rootScope.seatList = data.data.data;
			} else {
				alert('error');
			}
		});
		
		
		
		$http({
			method: 'GET',
			url: ServerService.listPath + 'list/country',
			// data: country
			// {
				headers:headersOptions
			// }
		}).then(function (data, status) {
			if (status = 'success') {
				localStorage.setItem('country', JSON.stringify(data.data));
				
				$rootScope.countryResult = data.data;
			} else {
				alert('error');
			}
		});
		
	  });
	 
			  
	// 	   $scope.airlines = [];
	//  $scope.airlineCode = [];
	
	 $scope.autoAirlines = function(value){
		$scope.typeaheadOpts = {
			minLength: 1,
		  };
		 
	    $http.get(ServerService.serverPath + 'flight/airlines/' + value.toUpperCase(), {
                headers: headersOptions
				}).then(function(response, status) {
					$scope.airlines = [];
					$scope.airlineCode = [];
      			 var airlist = response.data.data
				   if(airlist==null){
					$("#profile_airline").val('');
					$scope.airlineName;
				   }
				  if(response.data.data!='null'&&response.data.data.length>0){
					  for(var i=0; i<response.data.data.length; i++){
     var airfrom= {
                     Title:  response.data.data[i].displayname +' - '+'('+response.data.data[i].shortName+')'
                      
     }
	 var aircode = {
		 Code: response.data.data[i].shortName
	 }
	 $scope.typeaheadOpts = {
		minLength: 1,
	  };
     $scope.airlines.push(airfrom);
	 $scope.airlineCode.push(aircode)
     
    }
    $scope.airlines.sort();
	$scope.airlineCode.sort();
  }
             
           
            })
		 
		
		//  $scope.airlines=[];
		 
	 }
	 
	 
	 	$scope.airCodeChange = function(value){
			$scope.autoAirlines(value)
			}
	 

	 
	 
	 
	 
		  
		  
			$scope.addFrequentFlyer = function () {	
			var isValid = true;
			var frequentflyerObj = null;
			var mode = 'insert';
			if ($scope.ffChildObjEdit == null) {
				frequentflyerObj = new Object();		
				} else {
				frequentflyerObj = $scope.ffChildObjEdit;
				mode = 'update';	
			}		
			$scope. airlineName = $('#profile_airline').val();
			
			$scope.airCodes = $('#profile_flyerNo').val();
			
			//var ffairline = $('#profileAirlineCode').val();		
			
			if ($scope.airlineName == "" || $scope.airlineName == null) {
				Lobibox.notify('error', {size: 'mini', delay : 5000, position: 'right top', msg: "Please Choose Airline"});
				isValid = false;
				} else if ($scope.airCodes == "" || $scope.airCodes == null) {
				Lobibox.notify('error', {size: 'mini', delay : 5000,  position: 'right top', msg: "Please Enter Flyer No"});
				isValid = false;	
				} else {
				var ffairlinename = $scope.airlineName;
				var ffnumber = $scope.airCodes;
				$scope.profile_AirlineCode = ffairlinename.split('(')[1].substring(0,2)
				var ffairline = $scope.profile_AirlineCode;	
				var orgairlineffname = ffairlinename.indexOf('(');
				var airlineffname = ffairlinename.substr(0,(orgairlineffname-2));
				if (mode == 'insert') {
					if (!checkFFDtlDuplicate(ffairline)) {				
						isValid = false;
						Lobibox.notify('warning', {size: 'mini', delay : 5000,  position: 'right top',msg: "Please Enter Flyer No"});				
					} 				
				} 
				if (isValid == true) {
					frequentflyerObj.airline = 	$scope.profile_AirlineCode;
					frequentflyerObj.orgarlinename = airlineffname; 
					frequentflyerObj.ffNo = ffnumber;					
					clearFFFieldValues ();				
					if (mode == 'insert') {
						$scope.freqflyerlist.push(frequentflyerObj);
						Lobibox.notify('success', {size: 'mini', delay : 5000,  position: 'right top', msg: "Airline Added Successfully"});	
						} else {
						Lobibox.notify('success', {size: 'mini', delay : 5000,  position: 'right top', msg: "Airline Updated Successfully"});	
						$scope.ffChildObjEdit = null;
					}
				}		
			}		 			
		};  
		  
		  
		  
		  
		  
		  	function checkFFDtlDuplicate(currValue) {
				var isValid = true;		
			if($scope.freqflyerlist.length != 0) {		
				for(var i = 0, size = $scope.freqflyerlist.length; i < size ; i++) {
					var value = $scope.freqflyerlist[i].ffairline;
					if (currValue == value) {
						isValid = false;
					} 	 
				}
			}
			return isValid;
		}
		
		$scope.communicationMobile=function(code){
console.log(code);
if(code=='9'){
	$scope.mobileCountryCode=true;
}
		}
		
			$scope.addCommunicationProfile = function () {
			var isValid = true;
			var mode = 'insert';
			var phonenumberObj=null;
			var emailObj=null;
			var commtypecode = $('#profileContactType').val();
			var commvalue = $('#profileContactValue').val();
			var countrycode = $('#profileContactValuecode').val();

			
			
			if (commtypecode == "") {
				Lobibox.notify('error', {size: 'mini', delay : 5000,  position: 'right top',msg: "Please Enter The Contact Type"});		
				isValid = false;	
				} else if (commvalue == ""){
				Lobibox.notify('error', {size: 'mini', delay : 5000,  position: 'right top', msg: "Please Enter The Value"});	
				isValid = false;
				} else {		
				if (commtypecode == "7" || commtypecode == "8") {
					if (!validEmail(commvalue)) {
						Lobibox.notify('error', {size: 'mini', delay : 5000,  position: 'right top', msg: "Please Enter Correct email"});
						isValid = false;
						} else {						
						if ($scope.emailObjEdit == null) {
							if (!validateDuplicateEmail(commvalue)) {
								Lobibox.notify('warning', {size: 'mini', delay : 5000,  position: 'right top', msg: "Email Is Already Added"});
								isValid = false;
								} else {
								emailObj = new Object();					
								emailObj.type = commtypecode;
								emailObj.commValue = commvalue;	
								emailObj.isPreferred = "N";
							}							
							} else {				
							mode = 'update';
							emailObj = $scope.emailObjEdit;						
							emailObj.type = commtypecode;
							emailObj.commValue = commvalue;						
						}					
					}				
					if (isValid == true) {					
						clrCommunicationFlds();
						$scope.emailObjEdit = null;
						if (mode == 'insert') {
							$scope.emails.push(emailObj);
							Lobibox.notify('success', {size: 'mini', delay : 5000,  position: 'right top', msg: "Email Added Successfully"});
							} else {
							Lobibox.notify('success', {size: 'mini', delay : 5000,  position: 'right top', msg: "Email Updated Successfully"});
							
						}
					}					
					} else  {	
					if (!isNumber(commvalue)) {
						Lobibox.notify('error', {size: 'mini', delay : 5000,  position: 'right top', msg: "Please Enter Correct number"});
						isValid = false;
						} else {				
						if ($scope.phoneNumObjEdit == null) {
							if (validateDuplicateContact(commvalue) == false) {
								Lobibox.notify('warning', {size: 'mini', delay : 5000,  position: 'right top', msg: "Number Is Already Added"});					
								isValid = false;
								} else {
								phonenumberObj = new Object();									
								phonenumberObj.type = commtypecode;
								phonenumberObj.commValue = commvalue;	
								phonenumberObj.countryCode = countrycode;			 			
								phonenumberObj.isPreferred = "N";
							}					
							} else {								
							phonenumberObj = $scope.phoneNumObjEdit;
							mode = 'update';				
							phonenumberObj.type = commtypecode;
							phonenumberObj.commValue = commvalue;	
							phonenumberObj.countryCode = countrycode;			 			
						}								
						if (isValid == true) {					
							clrCommunicationFlds();					
							if (mode == 'insert') {
								$scope.phonenumbers.push(phonenumberObj);
							
								Lobibox.notify('success', {size: 'mini', delay : 5000,  position: 'right top', msg: "PhoneNumber Added Successfully"});
								} else {
								Lobibox.notify('success', {size: 'mini', delay : 5000,  position: 'right top', msg: "PhoneNumber Updated Successfully"});
								$scope.phoneNumObjEdit = null;
							}
						}
					}					
				}		
			}		
		};
		
		
		
			function clrCommunicationFlds() {
			$('#profileContactType').val("");
			$('#profileContactValue').val("");
			$('#addCommunication').show();
			$('#updateCommunication').hide();
		}
		$scope.removePhoneNumbers = function (phonenumber) {		
			$scope.phonenumbers.pop(phonenumber);
		};
		
		$scope.removeEmails = function (email) {
			$scope.emails.pop(email);
		};
		
		function validEmail(v) {	    
			var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
			return pattern.test(v);
		}
		// Check The Variable Is A Number
		
		function isNumber(n) {
			return !isNaN(parseFloat(n)) && isFinite(n);
		}
		// Check Whether The Phone Number Is Already Added
		
		function validateDuplicateContact(currValue) {
			var isValid = true;		
			if($scope.phonenumbers.length != 0) {		
				for(var i = 0, size = $scope.phonenumbers.length; i < size ; i++) {
					var value = $scope.phonenumbers[i].commvalue;
					if (currValue == value) {
						isValid = false;
					} 	 
				}
			}
			return isValid;
		}
		// Check Whether Email Is Already Added
		
		function validateDuplicateEmail(currValue) {		
			var isValid = true;		
			if($scope.emails.length != 0) {		
				for(var i = 0, size = $scope.emails.length; i < size ; i++) {
					var value = $scope.emails[i].commvalue;
					if (currValue == value) {
						isValid = false;
					} 	 
				}
			}
			return isValid;
		}
		
		
		function clearFFFieldValues () {
			$('#profile_flyerNo').val("");
			$('#profileFlyerNo').val("");
			$('#profile_airline').val("");	
			$('#profileAirlineName').val("");	
			$('#profileAirlineCode').val("");
			$('#profile_AirlineCode').val("");
			$('#addFrequentFlyer').show();
		}
		$scope.removeFrequentFlyer = function (frequentflyer) {
			$scope.freqflyerlist.pop(frequentflyer);
		};
		  
		  
		  		$scope.customerAddresses=[];
		$scope.customerPreferences=[];
		





		


		
		$scope.savePassenger = function () {
					
			$scope.travellerObjsave={};

			$scope.travellerObjsave.salutation=$scope.travellerObj.salutation;
			$scope.travellerObjsave.customerId=$scope.travellerObj.customerId;
			$scope.travellerObjsave.firstName=$scope.travellerObj.firstName;
			$scope.travellerObjsave.lastName=$scope.travellerObj.lastName;
			$scope.travellerObjsave.emailId=$scope.travellerObj.emailId;

			$scope.travellerObjsave.countryCode=$scope.travellerObj.countryCode;
			$scope.travellerObjsave.mobileNo=$scope.travellerObj.mobileNo;
			var dateOfBirth= $scope.travellerObj.dateOfBirth.toLocaleDateString();
			var year=dateOfBirth.split('/')[2];
			var day=dateOfBirth.split('/')[1]
			var month=dateOfBirth.split('/')[0]
			var today = new Date();
var age = today.getFullYear() - year;
if (today.getMonth() < month || (today.getMonth() == month && today.getDate() < day)) {
  age--;
}
if(age >= 0 && age < 12){

}else{
	if ($scope.travellerObjsave.emailId && $scope.travellerObjsave.countryCode && 	$scope.travellerObjsave.mobileNo && age >= 12) {
		$scope.emailAndMobileValidate=true;
		}else{
			$scope.emailAndMobileNotValidate=true;
			Lobibox.alert('error', {
				msg:"Please enter your email and mobile number to create the profile!"
			});
			return;
		}
}


			 var travellerdateOfBirth=dateOfBirth.split('/')[1]+'/'+dateOfBirth.split('/')[0]+'/'+dateOfBirth.split('/')[2];
			$scope.travellerObjsave.dateOfBirth=travellerdateOfBirth;
			$scope.travellerObjsave.gender=$scope.travellerObj.gender,
			$scope.travellerObjsave.passportNo=$scope.travellerObj.passportNo;
			$scope.travellerObjsave.nationality=$scope.travellerObj.nationality;
			if($scope.travellerObj.passportIssueDate!=null){
			var passportIssue= $scope.travellerObj.passportIssueDate.toLocaleDateString();
			var passsportIssueDate=passportIssue.split('/')[1]+'/'+passportIssue.split('/')[0]+'/'+passportIssue.split('/')[2];
			$scope.travellerObjsave.passportIssueDate=passsportIssueDate;
		}else{
			$scope.travellerObjsave.passportIssueDate=$scope.travellerObj.passportIssueDate;
		}
			// $scope.travellerObjsave.passportIssueDate=passsportIssueDate;
			if($scope.travellerObj.passportExpiryDate!=null){
			var passportExpiry= $scope.travellerObj.passportExpiryDate.toLocaleDateString();
			var passsportExpiryDate=passportExpiry.split('/')[1]+'/'+passportExpiry.split('/')[0]+'/'+passportExpiry.split('/')[2];


			$scope.travellerObjsave.passportExpiryDate= passsportExpiryDate;
			}else{
				$scope.travellerObjsave.passportExpiryDate=	$scope.travellerObjsave.passportExpiryDate;
			}
					  $scope.travellerObjsave.customerPreferences=[];
		  	   $scope.travellerObjsave.customerAddresses=[]; 
			  	  $scope.travellerObjsave.customerCommuncnDtls=[];
		  	  $scope.travellerObjsave.customerFfDtls=[];
					
					$scope.totalCommunicationData=$scope.phonenumbers.concat($scope.emails);
					
						
						$scope.travellerObjsave.customerFfDtls= $scope.freqflyerlist;
					  $scope.travellerObjsave.customerCommuncnDtls=$scope.totalCommunicationData;
					
			         if($scope.customer==undefined||angular.equals($scope.customer, {})){
            $scope.travellerObjsave.customerPreferences = [];
        }else{
					  $scope.travellerObjsave.customerPreferences.push($scope.customer)
        }
		if($scope.addressObj==undefined||angular.equals($scope.addressObj, {})){
            $scope.travellerObjsave.customerAddresses = [];
        }else{
			
				$scope.travellerObjsave.customerAddresses.push($scope.addressObj)
			
		
        }
        if($scope.traveller==undefined ||angular.equals($scope.traveller, {})){
            $scope.travellerObjsave.customerPreferences = [];
			if($scope.customer!=undefined){
				$scope.travellerObjsave.customerPreferences.push($scope.customer)
			}
        }else{
					  $scope.travellerObjsave.customerPreferences.push($scope.traveller)
        }
					

			      

     
               $scope.travellerObjsave.parentRelation=$scope.travellerObj.parentRelation;
     
      $scope.travellerObjsave.shortName="";
      $scope.travellerObjsave.designation= "",
     
    
     $scope.travellerObjsave.languagePref1= $scope.travellerObj.languagePref1,
     $scope.travellerObjsave.languagePref2=  $scope.travellerObj.languagePref2,
     $scope.travellerObjsave.maritialStatus="";
    
	 $scope.travellerObjsave.religion="",
	$scope.travellerObjsave.userId=$scope.userId;
   
     $scope.travellerObjsave.passengerType= "";
     
     $scope.travellerObjsave.customerKey="";
      $scope.travellerObjsave.passportIssueGovt= $scope.travellerObj.passportIssueGovt;
     
      $scope.travellerObjsave.countryResidence=$scope.travellerObj.countryResidence;
      $scope.travellerObjsave.otherLanguagePref=  "";
      $scope.travellerObjsave.otherLanguagePref2= "";
      $scope.travellerObjsave.newsLetter="",
      $scope.travellerObjsave.saudiNationalityId="";
      
	  $scope.travellerObjsave.airline = $scope.travellerObj.airline;
     
	
			$http({			
					method:'POST',
					url:ServerService.profilePath+'customer/create',
					 headers: headersOptions,
					data:$scope.travellerObjsave
					}).then(function (data, status) {	
						var jsondata=data.data;					  
					if (jsondata.success) {
						
						
						sessionStorage.setItem("profileJsondata",JSON.stringify(jsondata));
						
							Lobibox.alert('success', {
							
								msg: jsondata.message,
								callback: function (lobibox, type) {
									if (type === 'ok') {
										location.reload(true);
	
									}
								}
							});
					
						}else {
						// $location.path('/user-profile');
						Lobibox.alert('error', {
						
							msg: jsondata.errorMessage
						});	
					}
				});

			
		}; 
		  
		  
		  
		  
		  
	
})