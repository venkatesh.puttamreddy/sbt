"use strict";
app.controller( 'userTravellerCtrl' , function( $scope, $location, $interval, $http, $timeout, $routeParams,$rootScope, $routeSegment, ServerService, ajaxService,$window,NationService,CountryService) {
	$scope.data = '';
	$('body').attr('id','top');
	$('#top').addClass('thebg');
	NationService.nationGet();
	CountryService.countryGet();
	$scope.serviceBaseURl="http://uat.wowsochi.convergentechnologies.com/myaccount/b2cprofile/api";
	$scope.salutationData = [{label: 'MR', value: 'MR'}, {label: 'MS', value: 'MS'}, {label: 'MRS', value: 'MRS'}, {label: 'MASTER', value: 'MASTER'},
	 {label: 'MISS', value: 'MISS'},{
        label: 'DR',
        value: 'Dr'
	},{
        label: 'PROF',
        value: 'Prof'
	},{
        label: 'CAPT',
        value: 'Capt'
	}];
	
	$scope.logindata = JSON.parse(localStorage.getItem("logindata"));
	$scope.userid=(document.getElementById('userId').value);//$scope.logindata.data.profileData.userid;
	$scope.salutationDat = function(item){
		  
		var title=item.personaldata.salutation;
		if(title == 'MR' ||title=='MASTER'){
			$scope.travellerObj.personaldata.gender = 'M';
		}else if(title== 'MS' || title=='MISS'|| title== 'MRS'){
			$scope.travellerObj.personaldata.gender = 'F';
		}else {
			$scope.travellerObj.personaldata.gender='';
		}
		  }

	$scope.init = {'page':1, 'count':10	};
	$scope.userProfileReq = {};
	$scope.noRecords = false;
	$scope.travelClick = false;
	
	// prepare traveller search request
	$('#profilePassNo').keyup(function() {
		this.value = this.value.toUpperCase();
	  });

	  $(document).ready(function () {  
		$.ajax({  
			type: "GET",  
			url: $scope.serviceBaseURl+"/countries",
			success: function (data,status) {  
				if(status ='success') {
					$rootScope.countryresidence = data.data;
					} else {
					alert('error');
				}	
			}  
		}); 
		
		


		var relationDetails={
			"filter": {
			  "logic": "and",
			  "filters": [
				{
				  "field": "codeType",
				  "operator": "eq",
				  "value": "RELATIONSHIP"
				}
			  ]
			}
		  }
		  $.ajax({  
			type: "POST",  
			url: $scope.serviceBaseURl+"/search/common",
			data:relationDetails,
			contentType:"application/json; charset=utf-8",
			success: function (data,status) {  
				if(status ='success') {
					$rootScope.relationList = data.data;
					} else {
					alert('error');
				}	
			}  
		}); 

		var LANGUAGE_PREF2={
			"filter": {
			  "logic": "and",
			  "filters": [
				{
				  "field": "codeType",
				  "operator": "eq",
				  "value": "LANGUAGE_PREF"
				}
			  ]
			}
		  }
		  $.ajax({  
			type: "POST",  
			url: $scope.serviceBaseURl+"/search/common",
			data:relationDetails,
			contentType:"application/json; charset=utf-8",
			success: function (data,status) {  
				if(status ='success') {
					$rootScope.languagePreference = data.data;
					} else {
					alert('error');
				}	
			}  
		}); 








	}); 

	$scope.travelReq = function(page,count) {
		$scope.userProfileReq.salutation = null;
		$scope.userProfileReq.fname = null;
		$scope.userProfileReq.lname = null;
		$scope.userProfileReq.dob = null;
		$scope.userProfileReq.nationalitycode = null;
		$scope.userProfileReq.mobile = null;
		$scope.userProfileReq.email = null;
		$scope.userProfileReq.passportno = null;
		$scope.userProfileReq.passportIssueDate = null;
		$scope.userProfileReq.passportIssueGovt = null;
		$scope.userProfileReq.passportExpiryDate = null;
		$scope.userProfileReq.country = null;
		$scope.userProfileReq.passengertype = null;
		$scope.userProfileReq.operation = "TRAVELLER";
		$scope.userProfileReq.userid = 0;
		$scope.userProfileReq.createby = 0;
		$scope.userProfileReq.fromindex = 0;
		$scope.userProfileReq.toindex = 0;
		$scope.userProfileReq.page = page;
		$scope.userProfileReq.count = count;
	};
	
	$scope.getTraveller = function (params, paramsObj) {
		if($scope.travelClick==false){
			$scope.userProfileReq = {};
		}
		$scope.travelReq(paramsObj.page,paramsObj.count);
		$scope.page = paramsObj.page;
		return $http.post(ServerService.serverPath+'rest/search/profile',$scope.userProfileReq).then(function (response) {
			if(response.data.status=='SUCCESS' && response.data.data!='null') {
				$scope.travellerInfoData = response.data.data.travellerBeanList;
				$scope.totTrvCount = response.data.data.totalCount;
				$location.path('/user-traveller');
				return {"header": [],"rows": $scope.travellerInfoData,"pagination": {"count": 10,"page": $scope.page,"pages": $scope.totTrvCount/10,"size": $scope.totTrvCount}
				};
				} else {
				$scope.travellerInfoData = [];
				$scope.totTrvCount = 0;
				$scope.noRecords = true;
				Lobibox.alert('error',
				{
					msg: response.data.errorMessage
				});
				$location.path('/user-traveller');
				return {"header": [],"rows": $scope.travellerInfoData,"pagination": {"count": 10,"page": $scope.page,"pages": $scope.totTrvCount/10,"size": $scope.totTrvCount}
				};
			}
		});	
	};
	
	$scope.trvlModify = function(pagination) {
		pagination.count = 10;
		pagination.page = $scope.page;
		pagination.pages = Math.ceil($scope.totTrvCount/10);
		pagination.size = $scope.totTrvCount;
	};
	
	
	// $('.travellerIssuedate').datepicker({ 
	// 	dateFormat:'dd-mm-yy',
	// 	changeMonth: true,
	// 	changeYear: true,
	// 	maxDate: 0,
	// 	yearRange: "-100:+5",
	// 	showAnim: 'slideDown',
	// 	onChangeMonthYear:function(y, m, i){                                
	// 		var d = i.selectedDay;
	// 		$(this).datepicker('setDate', new Date(y, m, d));
	// 	}
	// });
	
	// $('.travellerexpireDate').datepicker({ 
	// 	dateFormat:'dd-mm-yy',
	// 	changeMonth: true,
	// 	changeYear: true,
	// 	minDate: 0,
	// 	yearRange: "-100:+5",
	// 	showAnim: 'slideDown',
	// 	onChangeMonthYear:function(y, m, i){                                
	// 		var d = i.selectedDay;
	// 		$(this).datepicker('setDate', new Date(y, m, d));
	// 	}
	// });

			
	

	// $('#datepicker2').datepicker({ 
	// 	dateFormat:'dd-mm-yy',
	// 	changeMonth: true,
	// 	changeYear: true,
	// 	maxDate: 0,
	// 	yearRange: '1900:'+(new Date).getFullYear(),
	// 	showAnim: 'slideDown',
	// 	onChangeMonthYear:function(y, m, i){
	// 		var d = i.selectedDay;
	// 		$(this).datepicker('setDate', new Date(y, m-1, d));
	// 	}
	// });


	$('.travellerIssuedate').datepicker({  
		dateFormat:'dd-mm-yy',
	singleDatePicker: true,
	showDropdowns: true,
	
	maxDate: 0,
	changeYear: true, 
	changeMonth: true,
	defaultDate: new Date(),
	yearRange: "-100:+5",
	
	
	singleDatePicker: true,
	
	});
	$(".travellerIssuedate").on("change",function(){
	var selected = $(this).val();
	
	});
	// });
	
	$('.travellerexpireDate').datepicker({ 
		dateFormat:'dd-mm-yy',
	singleDatePicker: true,
	showDropdowns: true,
	changeYear: true, 
	changeMonth: true,
	minDate: 0,
	defaultDate: new Date(),
	yearRange: "-100:+55",
	
	
	singleDatePicker: true,
	
	});
	$(".travellerexpireDate").on("change",function(){
	var selected = $(this).val();
	
	});
	// });


	//For Adding New Traveller && Update Traveller Information
	
	// Global Variables
	$scope.travellerObj;
	$scope.addressObjProfile;
	$scope.emails = [];
	$scope.freqflyerlist = [];
	$scope.phonenumbers = [];
	
	//$scope.travellerObj = null;
	$scope.editTravellerData = function (travellerObj, addressObjProfile) {
		//console.log(travellerObj);
		//console.log(addressObjProfile);
		$scope.travellerObj=travellerObj;
		$scope.addressObjProfile=addressObjProfile;
		if ($scope.travellerObj != null) {
			$scope.operation = 'update';
			editPassenger(travellerObj);
			$scope.travellerTitle = "Edit";
			$scope.travellerButton = "Update";
			if($scope.travellerObj.miscdata.freqflyerlist!=null) {
				$scope.freqflyerlist = $scope.travellerObj.miscdata.freqflyerlist;
				} else {
				$scope.freqflyerlist = [];
			}
			} else {
			$scope.travellerObj = {};
			$scope.travellerObj.personaldata = {};
			$scope.travellerObj.personaldata.salutation = $scope.salutationData[0].label;
			$scope.operation = 'insert';
			$scope.travellerTitle = "Add New";
			$scope.travellerButton = "Save";
			$scope.emails = [];
			$scope.freqflyerlist = [];
			$scope.phonenumbers = [];
		}
	};
		
	var fi;
	$( "#profileAirlineName" ).autocomplete({
		source: function( request, response ) {
			ajaxService.ajaxRequest('POST',ServerService.serverPath+'rest/repo/airline','{ "queryString" : "'+request.term+'"}','json')
			.then(function(data,status) {
				if(data!=null && data!='') {
					fi=0;
					response( data );
					}else{
					$( "#profileAirlineName" ).val('');
					$('.ui-autocomplete').hide();
				}
			});
		},
		focus: function( event, ui ) {
			$( "#profileAirlineName" ).val( ui.item.displayname + " - " +"("+ui.item.code+")" );
			return false;
		},
		change : function(event,ui) {
			$timeout(function(){
				jQuery('.form-group').removeClass('has-error');
			},10);
		},
		//autoFocus: true,
		minLength: 2,
		select: function( event, ui ) {
			$( "#profileAirlineName" ).val( ui.item.displayname +" - "+ "("+ui.item.code+")" );
			$( "#profile_AirlineCode" ).val( ui.item.code );
			return false;
		},
		open: function() {
			$( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
		},
		close: function(event, ui) {
			$( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
		}
		}).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
		if(fi==0) {
			fi++;
			return $( "<li>" ).append( "<a class='ui-state-focus'>" + item.displayname +"-" +"("+item.code+")</a>" ).appendTo( ul );
			} else {
			return $( "<li>" ).append( "<a>" + item.displayname + "-" + "("+item.code+")</a>" ).appendTo( ul );
		}
	};
	
	// Add FrequentFlyer Details
	$scope.addFrequentFlyerProfile = function () {	
		var isValid = true;
		var frequentflyerObj = null;
		var mode = 'insert';
		if ($scope.ffChildObjEdit == null) {
			frequentflyerObj = new Object();		
			} else {
			frequentflyerObj = $scope.ffChildObjEdit;
			mode = 'update';	
		}		
		var ffairlinename = $('#profileAirlineName').val();
		var ffnumber = $('#profileFlyerNo').val();
		var ffairline = $('#profile_AirlineCode').val();		
		if (ffairline == "") {
			Lobibox.notify('error', {size: 'mini', delay : 1500, msg: "Please Choose Airline"});
			isValid = false;
			} else if (ffnumber == "") {
			Lobibox.notify('error', {size: 'mini', delay : 1500, msg: "Please Enter Flyer No"});
			isValid = false;	
			} else {
			if (mode == 'insert') {
				if (!checkFFDtlDuplicate(ffairline)) {
					isValid = false;
					Lobibox.notify('warning', {size: 'mini', delay : 1500, msg: "Please Enter Flyer No"});				
				} 				
			}
			if (isValid == true) {
				frequentflyerObj.ffairlinename = ffairlinename;
				frequentflyerObj.ffairline = ffairline;
				frequentflyerObj.ffnumber = ffnumber;					
				clearFFFieldValues ();				
				if (mode == 'insert') {
					$scope.freqflyerlist.push(frequentflyerObj);
					//$scope.$apply();
					console.log($scope.freqflyerlist);
					Lobibox.notify('success', {size: 'mini', delay : 1500, msg: "Airline Added Successfully"});	
					} else {
					Lobibox.notify('success', {size: 'mini', delay : 1500, msg: "Airline Updated Successfully"});	
					$scope.ffChildObjEdit = null;
				}
			}		
		}		 			
	};
	// Check Whether The Airline Is Already Added In The FF Details List
	
	function checkFFDtlDuplicate(currValue) {
		var isValid = true;		
		if($scope.freqflyerlist.length != 0) {		
			for(var i = 0, size = $scope.freqflyerlist.length; i < size ; i++) {
				var value = $scope.freqflyerlist[i].ffairline;
				if (currValue == value) {
					isValid = false;
				} 	 
			}
		}
		return isValid;
	}
	function clearFFFieldValues () {
		$('#profileFlyerNo').val("");
		$('#profileAirlineName').val("");
		$('#profile_AirlineCode').val("");
		$('#addFrequentFlyerProfile').show();
	}
	
	// Add Communication Details
	$scope.addCommunicationProfile = function () {
		var isValid = true;
		var mode = 'insert';
		var phonenumberObj=null;
		var emailObj=null;
		var commtypecode = $('#profile_ContactType').val();
		var commvalue = $('#profile_ContactValue').val();
		
		if (commtypecode == "") {
			Lobibox.notify('error', {size: 'mini', delay : 1500, msg: "Please Enter The Contact Type"});		
			isValid = false;	
			} else if (commvalue == ""){
			Lobibox.notify('error', {size: 'mini', delay : 1500, msg: "Please Enter The Value"});	
			isValid = false;
			} else {		
			if (commtypecode == "EO" || commtypecode == "EP") {
				if (!validEmail(commvalue)) {
					Lobibox.notify('error', {size: 'mini', delay : 1500, msg: "Please Enter Correct email"});
					isValid = false;
					} else {						
					if ($scope.emailObjEdit == null) {
						if (!validateDuplicateEmail(commvalue)) {
							Lobibox.notify('warning', {size: 'mini', delay : 1500, msg: "Email Is Already Added"});
							isValid = false;
							} else {
							emailObj = new Object();					
							emailObj.commtypecode = commtypecode;
							emailObj.commvalue = commvalue;	
							emailObj.isPref = "N";
						}							
						} else {				
						mode = 'update';
						emailObj = $scope.emailObjEdit;						
						emailObj.commtypecode = commtypecode;
						emailObj.commvalue = commvalue;						
					}					
				}				
				if (isValid == true) {					
					clrCommunicationFlds();
					$scope.emailObjEdit = null;
					if (mode == 'insert') {
						$scope.emails.push(emailObj);
						Lobibox.notify('success', {size: 'mini', delay : 1500, msg: "Email Added Successfully"});
						} else {
						Lobibox.notify('success', {size: 'mini', delay : 1500, msg: "Email Updated Successfully"});
						
					}
				}					
				} else  {	
				if (!isNumber(commvalue)) {
					Lobibox.notify('error', {size: 'mini', delay : 1500, msg: "Please Enter Correct number"});
					isValid = false;
					} else {				
					if ($scope.phoneNumObjEdit == null) {
						if (validateDuplicateContact(commvalue) == false) {
							Lobibox.notify('warning', {size: 'mini', delay : 1500, msg: "Number Is Already Added"});					
							isValid = false;
							} else {
							phonenumberObj = new Object();									
							phonenumberObj.commtypecode = commtypecode;
							phonenumberObj.commvalue = commvalue;				 			
							phonenumberObj.isPref = "N";
						}					
						} else {								
						phonenumberObj = $scope.phoneNumObjEdit;
						mode = 'update';				
						phonenumberObj.commtypecode = commtypecode;
						phonenumberObj.commvalue = commvalue;				 			
					}								
					if (isValid == true) {					
						clrCommunicationFlds();					
						if (mode == 'insert') {
							$scope.phonenumbers.push(phonenumberObj);
							Lobibox.notify('success', {size: 'mini', delay : 1500, msg: "PhoneNumber Added Successfully"});
							} else {
							Lobibox.notify('success', {size: 'mini', delay : 1500, msg: "PhoneNumber Updated Successfully"});
							$scope.phoneNumObjEdit = null;
						}
					}
				}					
			}		
		}		
	};
	// Clear Communication UI Fields
	
	function clrCommunicationFlds() {
		$('#profile_ContactType').val("");
		$('#profile_ContactValue').val("");
		$('#addCommunicationProfile').show();
		$('#updateCommunication').hide();
	}
	
	$scope.removePhoneNumbers = function (phonenumber) {		
		$scope.phonenumbers.pop(phonenumber);
	};
	
	$scope.removeEmails = function (email) {
		$scope.emails.pop(email);
	};
	
	$scope.removeFrequentFlyer = function (frequentflyer) {
		$scope.freqflyerlist.pop(frequentflyer);
	};
	// Check The Variable Is A Email
	
	function validEmail(v) {	    
		var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
		return pattern.test(v);
	}
	// Check The Variable Is A Number
	
	function isNumber(n) {
		return !isNaN(parseFloat(n)) && isFinite(n);
	}
	// Check Whether The Phone Number Is Already Added
	
	function validateDuplicateContact(currValue) {
		var isValid = true;		
		if($scope.phonenumbers.length != 0) {		
			for(var i = 0, size = $scope.phonenumbers.length; i < size ; i++) {
				var value = $scope.phonenumbers[i].commvalue;
				if (currValue == value) {
					isValid = false;
				} 	 
			}
		}
		return isValid;
	}
	// Check Whether Email Is Already Added
	
	function validateDuplicateEmail(currValue) {		
		var isValid = true;		
		if($scope.emails.length != 0) {		
			for(var i = 0, size = $scope.emails.length; i < size ; i++) {
				var value = $scope.emails[i].commvalue;
				if (currValue == value) {
					isValid = false;
				} 	 
			}
		}
		return isValid;
	}
	// Save Passenger Details
	
	var $validator = jQuery("#travellerForm").validate({
		highlight: function(element) {
			jQuery(element).closest('.form-group').removeClass('has-success').addClass('has-error');
		},
		success: function(element) {
			jQuery(element).closest('.form-group').removeClass('has-error');
		}
	});
	
	$scope.savePassenger = function () {
		var $valid = jQuery('#travellerForm').valid();
		if(!$valid) {
			return false;
			} else {	
			console.log($scope.operation);
			$('body').addClass('loading');
			if ($scope.operation != 'update') {
				var contactdata = new Object();
				var miscdata = new Object();
				$scope.travellerObj.contactdata = contactdata;
				$scope.travellerObj.miscdata = miscdata;
				$scope.travellerObj.profileid = 0;
				$scope.travellerObj.agntid = 0;
				addNewPassenger($scope.travellerObj);
				} else {
				addNewPassenger($scope.travellerObj);
			}
		}
	};
	
	// Edit Passenger Details
	
	function editPassenger (travellerObj) {		
		
		$scope.travellerObj = travellerObj;
		
		if ($scope.travellerObj.contactdata.address != null) {	
			$scope.addressObjProfile = travellerObj.contactdata.address[0];
		}
		
		if ($scope.travellerObj.contactdata.phonenumbers != null) {
			$scope.phonenumbers = $scope.travellerObj.contactdata.phonenumbers;
		}
		if ($scope.travellerObj.contactdata.emails != null) {
			$scope.emails = $scope.travellerObj.contactdata.emails;
		}
		if ($scope.travellerObj.miscdata.mealpref != null) {
			$('#profileMealPreference').val($scope.travellerObj.miscdata.mealpref[0].prefkey);
		}
		if ($scope.travellerObj.miscdata.seatpref != null) {
			$('#profileSeatPreference').val($scope.travellerObj.miscdata.seatpref[0].prefkey);				
		}
		$scope.freqflyerlist = $scope.travellerObj.miscdata.freqflyerlist;
		
		if (travellerObj.miscdata.documentdata  != null) {
			$scope.documentdata = $scope.travellerObj.miscdata.documentdata;
		}
	};
	
	function addNewPassenger (travellerObj) {
		var address = [];
		/*var contactdata = new Object();
			var miscdata = new Object();
			$scope.travellerObj.contactdata = contactdata;
		$scope.travellerObj.miscdata = miscdata;*/
		if($scope.addressObjProfile== null){
			$scope.travellerObj.contactdata.address = address;
			}else{
			$scope.travellerObj.contactdata.address = address;
			$scope.travellerObj.contactdata.address.push($scope.addressObjProfile);
		}
		$scope.travellerObj.contactdata.emails = $scope.emails;
		$scope.travellerObj.contactdata.phonenumbers = $scope.phonenumbers;
		if ($scope.travellerObj.miscdata.mealpref == null) {
			var mealpref = [];
			var mealPreference = new Object();					
			$scope.travellerObj.miscdata.mealpref = mealpref;
			$scope.travellerObj.miscdata.mealpref.push(mealPreference);
		}
		if ($scope.travellerObj.miscdata.seatpref == null) {
			var seatpref = [];
			var seatPreference = new Object();		
			$scope.travellerObj.miscdata.seatpref = seatpref;
			$scope.travellerObj.miscdata.seatpref.push(seatPreference);
		}
		$scope.travellerObj.miscdata.mealpref[0].prefkey = $('#profileMealPreference').val();				
		$scope.travellerObj.miscdata.seatpref[0].prefkey = $('#profileSeatPreference').val();	
		$scope.travellerObj.miscdata.freqflyerlist = $scope.freqflyerlist;
		console.log($scope.travellerObj);
		$http({			
			method:'put',
			url: ServerService.serverPath+'rest/manipulate/profile',
			data: $scope.travellerObj
			}).success(function(data,status) {							  
			if (data.status =='SUCCESS') {
				$('body').addClass('loading');
				$timeout(function(){
					//$routeSegment.chain[0].reload();
					location.reload(true);
					//$('body').removeClass('loading');
				},500);
					Lobibox.notify('success', {
						size: 'mini',
						msg: data.message
					});
				
				}else {
				Lobibox.notify('error', {
					size: 'mini',
					msg: data.errorMessage
				});
			}
			$("#addTravellerModal").modal('hide');
		});
	}



	
	$('#datepicker2').datepicker({ 
		dateFormat:'dd-mm-yy',
		changeMonth: true,
		changeYear: true,
		maxDate: 0,
		yearRange: '1900:'+(new Date).getFullYear(),
		showAnim: 'slideDown',
		onChangeMonthYear:function(y, m, i){
			var d = i.selectedDay;
			$(this).datepicker('setDate', new Date(y, m-1, d));
		}
	});

	
});