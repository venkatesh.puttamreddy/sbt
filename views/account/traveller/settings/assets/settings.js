app.controller( 'settingsCtrl' , function( $scope, $routeParams, $location, $http, $rootScope, ServerService,$timeout, Upload, $routeSegment, $filter ) {
	
	/* Initialization Values */
		$scope.rolename = document.getElementById('rolename').value;
		console.log($scope.rolename);
		if($scope.rolename=='ROLE_SUPER_USER' || $scope.rolename=='') {
		$scope.loggedValue = document.getElementById('logged').value;
		$scope.homes = [{label:"Banners",value:"banners"},{label:"Deals",value:"deals"},{label:"Services",value:"services"},{label:"Holidays",value:"holidays"}];
		$scope.tmode = 'update';$scope.hmode = 'add';$scope.tchange = false;$scope.createThemeObj = {};$scope.itemsPerPage = 10;$scope.listItemsPerPage = [50];
		$scope.pchange = false; $scope.createTheme=false; $scope.save = false; $scope.copy = false; $scope.paste = false; $scope.copyCmsObj = [];
		$scope.home = false;$scope.deals = false;$scope.services = false;$scope.holidays = false;
		$scope.selectedContent = {};
		$scope.ctThemeName='';
		/* Language */
		
		$scope.languages = [{label:"English",value:"english"},{label:"Arabic",value:"arabic"}];
		$scope.selectedLang = $scope.languages[0];
		$scope.langChange = function(lang) {
			$scope.selectedLang = lang;
			$scope.pages = [];
		};
		
		/* Page */
		
		$http.get(ServerService.serverPath+'rest/contents').then(function (response) {
			if(response.data.status=='SUCCESS') {
				$scope.pages = response.data.data;
			}
		});
		$scope.pageChange = function(s) {
			if(s) {
				$scope.selectedPage = s;
				$scope.selectedScreen = $scope.selectedPage.csScreenName.toLowerCase();
				$scope.selectedPath = 'app/includes/settings/pages/'+$scope.selectedPage.csScreenName.toLowerCase()+'.html';
				$scope.themeGet();
			}
			$scope.tchange = false;
			$scope.ctContent = [];
			$scope.resource = {"header": [],"rows": $scope.ctContent };
			$timeout(function(){
				$('#theme-select').val("");
				$scope.selectedTheme = null;
			},10);
		};
		
		/* Theme */
		
		$scope.themeGet = function() {
			$scope.pchange = true;
			$http.get(ServerService.serverPath+'rest/contents/'+$scope.selectedPage.csId).then(function (response) {
				if(response.data.status=='SUCCESS') {
					$scope.themes = response.data.data;
					} else {
					$scope.themes = [];
					$scope.ctContent = [];
					$scope.resource.rows = $scope.ctContent;
				}
			});
		};
		$scope.createThemeBtn = function() {
			$scope.createThemeObj.ctThemeName = "";
			$scope.createTheme = !$scope.createTheme;
		};
		$scope.cancelThemeBtn = function() {
			$scope.createTheme = false;
		};
		$scope.createThemeFun = function(c) {
			$scope.createTheme=false;
			$scope.save=true; 
			$scope.tmode = 'add';
			$scope.tchange = true;
			$scope.createThemeObj.ctId = '-1';
			$scope.ctContent = [];
			$scope.dealContent = [];
			$scope.holidayContent = [];
			$scope.serviceContent = [];
			$scope.themes.push(c);
			$scope.selectedTheme = $scope.createThemeObj;
		};
		$scope.themeChange = function(t) {
			$scope.tmode = 'update'; $scope.ctContent =[]; $scope.save = false;$scope.selectedContent = {};
			if(t) {
				$scope.tchange = true; $scope.createTheme = false;
				if(t.ctId!='-1' && t!='null') {
					$scope.selectedTheme = t;
					$http.get(ServerService.serverPath+'rest/templates/'+t.ctId).then(function (response) {
						if(response.data.data!='null' && response.data.status=='SUCCESS' && response.data.data!='') {
							$scope.ctContent = angular.copy(JSON.parse(stripslashes(response.data.data.ctContent)));
							if($scope.selectedPage.csScreenName=='HOME') {
								$scope.bannerContent = $filter('filter')($scope.ctContent, {type: 'banner'});
								$scope.dealContent = $filter('filter')($scope.ctContent, {type: 'deals'});
								$scope.holidayContent = $filter('filter')($scope.ctContent, {type: 'holidays'});
								$scope.serviceContent = $filter('filter')($scope.ctContent, {type: 'services'});
								} else {
								$scope.resource.rows = $scope.ctContent;
							}
							$scope.selectedPath = 'app/includes/settings/pages/'+$scope.selectedPage.csScreenName.toLowerCase()+'.html';
							} else {
							$scope.ctContent = [];
							$scope.resource.rows = $scope.ctContent;
							$scope.selectedPath = 'app/includes/settings/pages/'+$scope.selectedPage.csScreenName.toLowerCase()+'.html';
						}
						
					});
					} else {
					$scope.ctContent = [];
					$scope.resource.rows = $scope.ctContent;
				}
				} else {
				$scope.selectedTheme = t;
				$scope.resource.rows = $scope.ctContent;
				$scope.tchange = false;
			}
		};
		$scope.deleteTheme = function(obj) {
			$scope.themes.forEach(function(k,i){
				if($scope.themes[i].label==obj.label) {
					$scope.themes.splice(i,1);
					return false;
				}
			});
		};
		
		/* Validation */
		
		$scope.validateInit = function() {
			if($scope.selectedPage.csScreenName!=null) 
			if($scope.selectedPage.csScreenName!='HOME') {
				var $form =  jQuery('#'+$scope.selectedPage.csScreenName.toLowerCase()+'-form-'+$scope.hmode);
				} else {
				var $form =  jQuery('#'+$scope.selectedPage.csScreenName.toLowerCase()+'-'+$scope.selectedContent.value+'-form-'+$scope.hmode);
			}
			var $validator = $form.validate({
				highlight: function(element) {
					jQuery(element).closest('.form-group').removeClass('has-success').addClass('has-error');
				},
				success: function(element) {
					jQuery(element).closest('.form-group').removeClass('has-error');
				}
			});
		}
		
		/* Content Manipulate */
		
		$scope.getRemainHolidays = function() { /* Home Screen */
			$scope.remainHolidays = [];
			angular.copy($scope.availHolidays,$scope.remainHolidays);
			console.log($scope.remainHolidays.length);
			console.log($scope.holidayContent.length);
			for(i=0; i<$scope.remainHolidays.length;i++){
				for(j=0; j<$scope.holidayContent.length;j++){
					if($scope.remainHolidays[i].name === $scope.holidayContent[j].name){
						$scope.remainHolidays.splice(i,1);
					} 
				}
			}
			console.log($scope.remainHolidays.length);
		};
		$scope.getRemainServices = function() {
			$scope.remainServices = [];
			angular.copy($scope.availServices,$scope.remainServices);
			for(i=0; i<$scope.remainServices.length;i++){
				for(j=0; j<$scope.serviceContent.length;j++){
					if($scope.remainServices[i].name === $scope.serviceContent[j].name){
						$scope.remainServices.splice(i,1);
					} 
				}
			}
		};
		$scope.homeUpdatedContent = function() {
			if($scope.selectedContent.value=='banners') {
				$scope.bannerContent = $filter('filter')($scope.ctContent, {type: 'banner'});
				$scope.resource.rows = $scope.bannerContent;
				console.log($scope.resource.rows);
				} else if($scope.selectedContent.value=='deals') {
				$scope.dealContent = $filter('filter')($scope.ctContent, {type: 'deals'});
				$scope.resource.rows = $scope.dealContent;
				} else if($scope.selectedContent.value=='holidays') {
				$scope.holidayContent = $filter('filter')($scope.ctContent, {type: 'holidays'});
				$scope.resource.rows = $scope.holidayContent;
				$scope.getRemainHolidays();
				$timeout(function(){
					$('.select-holiday').val("");
					$scope.selectedHoliday = {};
				},10);
				} else if($scope.selectedContent.value=='services') {
				$scope.serviceContent = $filter('filter')($scope.ctContent, {type: 'services'});
				$scope.resource.rows = $scope.serviceContent;
				$scope.getRemainServices();
				$timeout(function(){
					$('.select-services').val("");
					$scope.selectedService = {};
				},10);
			}
		}
		
		$scope.contentChange = function(sContent) {
			if(sContent) {
				if(sContent.value=='holidays') {
					$scope.resource.rows = $scope.holidayContent;
					$http.get(ServerService.serverPath+'rest/screens/SCREEN_2').then(function (response) {
						if(response.data.status=='SUCCESS' && response.data.data!='') {
							$scope.availHolidays = JSON.parse(stripslashes(response.data.data));
							$scope.getRemainHolidays();
							//$scope.selectedHoliday = $filter('filter')($scope.availHolidays, {urlname: $scope.editCmsObj.urlname})[0];
							} else {
							$scope.availHolidays = [];
						}
						
					});
					} else if(sContent.value=='services') {
					$scope.resource.rows = $scope.serviceContent;
					$http.get(ServerService.serverPath+'rest/screens/SCREEN_4').then(function (response) {
						if(response.data.status=='SUCCESS' && response.data.data!='') {
							$scope.availServices = JSON.parse(stripslashes(response.data.data));
							$scope.getRemainServices();
							//$scope.selectedServices = $filter('filter')($scope.availServices, {urlname: $scope.editCmsObj.urlname})[0];
							} else {
							$scope.availServices = [];
						}
						
					});
					} else if(sContent.value=='deals') {
					$scope.resource.rows = $scope.dealContent;
					} else if(sContent.value=='banners') {
					$scope.resource.rows = $scope.bannerContent;
				}
			}
		};
		$scope.createContent = function(ctype){
			$scope.validateInit();
			$scope.createCmsObj = { adult:{istrue:true}, child:{istrue:true}, infant:{istrue:true}, bigimage:[], smallimage:[], includes:{}, itinerary:[], type:ctype };
		};
		//$scope.createContent();
		$scope.editContent = function(h) {
			$scope.hmode = 'update';
			$scope.validateInit();
			$scope.editCmsObj = $scope.ctContent[h];
			$scope.hIndex = h;
			if($scope.selectedPage.csScreenName=='HOME') {
				$scope.homeUpdatedContent();
			}
		};
		$scope.addFormValid = false; $scope.editFormValid = false;
		$scope.addContent = function(type,formValid) {
			$scope.hmode = type;
			if($scope.selectedPage.csScreenName!='HOME') {
				var $valid =  jQuery('#'+$scope.selectedPage.csScreenName.toLowerCase()+'-form-'+type).valid();
				console.log($valid);
				} else {
				var $valid =  jQuery('#'+$scope.selectedPage.csScreenName.toLowerCase()+'-'+$scope.selectedContent.value+'-form-'+type).valid();
				console.log('#'+$scope.selectedPage.csScreenName.toLowerCase()+'-'+$scope.selectedContent.value+'-form-'+type);
			}
			
			/*if(!formValid) {
				if(type=='add') {
				$('.ta-scroll-window:eq(0)').parents(':eq(1)').addClass('has-error');
				} else {
				$('.ta-scroll-window:eq(1)').parents(':eq(1)').addClass('has-error');
				}
				return false;
			} else {*/
			if(!$valid) {
				return false;
				} else { 
				//$('.ta-scroll-window').parents(':eq(1)').removeClass('has-error');
				if(!$scope.ctContent) {
					$scope.ctContent = [];
				}
				if($scope.hmode=='add') {
					if($scope.selectedPage.csScreenName!='HOME') {
						$scope.createCmsObj.urlname = $scope.createCmsObj.name.replace(/\s+/g, '-').toLowerCase();
					}
					$scope.ctContent.push($scope.createCmsObj);
					} else {
					if($scope.selectedPage.csScreenName!='HOME') {
						$scope.editCmsObj.urlname = $scope.editCmsObj.name.replace(/\s+/g, '-').toLowerCase();
					}
					$scope.ctContent[$scope.hIndex] = $scope.editCmsObj;
				}
				$scope.save =  true;
				if($scope.selectedPage.csScreenName=='HOME') {
					$scope.homeUpdatedContent();
					} else {
					$scope.resource.rows = $scope.ctContent;
				}
				$('.modal').modal('hide').data( 'bs.modal', null );
				if($scope.selectedPage.csScreenName=='HOME') {
					$('#'+$scope.selectedPage.csScreenName.toLowerCase()+'-'+$scope.selectedContent.value+'-form-'+type)[0].reset();
					} else {
					$('#'+$scope.selectedPage.csScreenName.toLowerCase()+'-form-'+type)[0].reset();
				}
				
			}
		//}
	};

	$scope.totalAdultCount=0;
	$scope.totalChildCount=0;
	$scope.totalInfantCount=0;

	$scope.saveContent = function() {
		$('body').addClass('loading');
		
		$scope.selectedTemp = { ctCsId: $scope.selectedPage.csId, ctLanguage: $scope.selectedLang.value,ctThemeName:$scope.selectedTheme.ctThemeName,adultCount:$scope.totalAdultCount,childCount:$scope.totalChildCount,infantCount:$scope.totalInfantCount };
		var addSlash = addslashes(JSON.stringify($scope.ctContent));
		$scope.selectedTemp.ctContent = addSlash;
		if($scope.tmode=='add') {
			$http.put(ServerService.serverPath+'rest/contents',$scope.selectedTemp).then(function (response) {
				if(response.data.status=='SUCCESS') {
					$scope.save = false; $scope.tchange = true;
					$scope.selectedTheme = response.data.data;
					$scope.ctContent = angular.copy(JSON.parse(stripslashes(response.data.data.ctContent)));
					console.log("$scope.ctContent",$scope.ctContent);
					if($scope.selectedPage.csScreenName=='HOME') {
						$scope.homeUpdatedContent();
						} else {
						$scope.resource.rows = $scope.ctContent;
					}
					Lobibox.alert('success',
					{
						msg: response.data.message
					});
				}
				$('body').removeClass('loading');
			});
			} else {
			$http.post(ServerService.serverPath+'rest/contents/'+$scope.selectedTheme.ctId,$scope.selectedTemp).then(function (response) {
				if(response.data.status=='SUCCESS') {
					$scope.save = false; $scope.tchange = true;
					$scope.selectedTheme = response.data.data;
					$scope.ctContent = angular.copy(JSON.parse(stripslashes(response.data.data.ctContent)));
					if($scope.selectedPage.csScreenName=='HOME') {
						$scope.homeUpdatedContent();
						} else {
						$scope.resource.rows = $scope.ctContent;
					}
					Lobibox.alert('success',
					{
						msg: response.data.message
					});
				}
				$('body').removeClass('loading');
			});
		}
	}
		}
	$scope.deleteContent = function(h) {
		$scope.save = true;
		$scope.ctContent.splice(h,1);
		if($scope.selectedPage.csScreenName=='HOME') {
			$scope.homeUpdatedContent();
			} else {
			$scope.resource.rows = $scope.ctContent;
		}
	};
	$scope.copyContent = function(h,type) {
		if(type=='home') {
			$scope.home = true;
			} else if(type=='deals') {
			$scope.deals = true;
			} else if(type=='services') {
			$scope.services = true;
			} else if(type=='holidays') {
			$scope.holidays = true;
		}
		$scope.copy = true; $scope.ctContent[h].copy = true;
		if($scope.tchange == true) {
			$scope.copyCmsObj = [];
			$scope.tchange = false;
			$scope.copyCmsObj.push(angular.copy($scope.ctContent[h]));
			} else {
			$scope.copyCmsObj.push(angular.copy($scope.ctContent[h]));
		}
	}
	$scope.uncopyContent = function() {
		$scope.copyCmsObj = []; $scope.copy = false;
		$scope.ctContent.forEach(function(k,i){
			$scope.ctContent[i].copy = false;
		});
		if($scope.selectedPage.csScreenName=='HOME') {
			$scope.homeUpdatedContent();
		}
	};
	$scope.pasteContent = function() {
		$scope.paste = true; $scope.tchange = true; $scope.save = true; $scope.copy = false;
		$scope.ctContent.forEach(function(k,i){
			$scope.ctContent[i].copy = false;
		});
		if($scope.selectedPage.csScreenName=='HOME') {
			if($scope.selectedContent.value=='holidays') {
				if($scope.copyCmsObj.length>$scope.remainHolidays.length) {
					alert("Copied More");
					return false;
				}
				} else if($scope.selectedContent.value=='services') {
				if($scope.copyCmsObj.length>$scope.remainServices.length) {
					alert("Copied More");
					return false;
				}
				} else if($scope.selectedContent.value=='deals') {
				if($scope.copyCmsObj.length+$scope.dealContent.length>3) {
					alert("Copied More");
					return false;
				}
			}
		}
		$scope.copyCmsObj.forEach(function(k,i){
			$scope.copyCmsObj[i].copy = false;
			$scope.ctContent.push(angular.copy($scope.copyCmsObj[i]));
		});
		$scope.resource.rows = $scope.ctContent;
		if($scope.selectedPage.csScreenName=='HOME') {
			$scope.homeUpdatedContent();
		}
		$scope.copyCmsObj = [];
	}
	$scope.contentPublish = function() {
		$('body').addClass('loading');
		$http.post(ServerService.serverPath+'rest/templates/'+$scope.selectedPage.csId+'/'+$scope.selectedTheme.ctId).then(function (response) {
			$scope.selectedTheme = response.data.data;
			$('body').removeClass('loading');
		});
	};
	
	$scope.adultCurrency= function() {
		alert('hi');
	}
	


	/* File Upload */
	
	$scope.uploadImage = function(files,imageType,i) {
		if (files && files.length) {
			var file = files;
			var screenName;
			if($scope.selectedPage.csScreenName.toLowerCase()=='holidays') {
				screenName = 'holiday';
				} else if($scope.selectedPage.csScreenName.toLowerCase()=='ourservices') {
				screenName = 'services';
				} else {
				screenName = $scope.selectedPage.csScreenName.toLowerCase();
			}
			Upload.upload({
				url: 'http://150.129.116.146:8585/cms/upload?type=cms&view='+screenName,
				file: file
				}).progress(function (evt) {
				var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
				}).success(function (data, status, headers, config) {
				if(imageType=='bigimage') {
					if($scope.hmode=='add') {
						$scope.createCmsObj.bigimage[i] = data.result.imgUrl;
						} else {
						$scope.editCmsObj.bigimage[i] = data.result.imgUrl;
					}
					} else if(imageType=='smallimage') {
					if($scope.hmode=='add') {
						$scope.createCmsObj.smallimage[i] = data.result.imgUrl;
						} else {
						$scope.editCmsObj.smallimage[i] = data.result.imgUrl;
					}
					} else if(imageType=='services') {
					if($scope.hmode=='add') {
						$scope.createCmsObj.serviceimg = data.result.imgUrl;
						} else {
						$scope.editCmsObj.serviceimg = data.result.imgUrl;
					}
					} else if(imageType=='holidays') {
					if($scope.hmode=='add') {
						$scope.createCmsObj.holidayimg = data.result.imgUrl;
						} else {
						$scope.editCmsObj.holidayimg = data.result.imgUrl;
					}
					} else if(imageType=='aboutus') {
					if($scope.hmode=='add') {
						$scope.createCmsObj.aboutUsimg = data.result.imgUrl;
						} else {
						$scope.editCmsObj.aboutUsimg = data.result.imgUrl;
					}
				}
			});
		}
	};
	
	/* To create Alias for Holidays, Attractions and Our Services */
	
	$scope.assignLink = function(selected,type) {
		if(type=='add' && selected) {
			$scope.createCmsObj.name = selected.name;
			$scope.createCmsObj.urlname = selected.urlname;
			} else {
			$scope.editCmsObj.name = selected.name;
			$scope.editCmsObj.urlname = selected.urlname;
		}
	};
	
	/* Holidays Itinerary */
	
	$scope.addday=false;
	$scope.newday = {};
	$scope.addDay = function(n) {
		if($scope.hmode=='add') {
			$scope.createCmsObj.itinerary.push(n);
			} else {
			$scope.editCmsObj.itinerary.push(n);
		}
	};
	$scope.updateDay = function(i,u) {
		if($scope.hmode=='add') {
			$scope.createCmsObj.itinerary[i] = u;
			} else {
			$scope.editCmsObj.itinerary[i] = u;
		}
	};
	$scope.dayDelete = function(i) {
		if($scope.hmode=='add') {
			$scope.createCmsObj.itinerary.splice(i,1);
			} else {
			$scope.editCmsObj.itinerary.splice(i,1);
		}
		
	};
// } else {
// 	$location.path('/');
// }
});

/* This filter used to to display available screen in select option */

app.filter('nameFilter',function(){
	return function(x) {
		var nameChange;
		if(x=='HOME') {
			nameChange = 'Home';
			} else if(x=='HOLIDAYS') {
			nameChange = 'Holidays';
			} else if(x=='ATTRACTIONS') {
			nameChange = 'Attractions';
			} else if(x=='OURSERVICES') {
			nameChange = 'Our Services';
			} else if(x=='ABOUTUS') {
			nameChange = 'About Us';
			} else if(x=='TERMSCONDITIONS') {
			nameChange = 'Term & Conditions';
			} else if(x=='PRIVACYPOLICY') {
			nameChange = 'Privacy Policy';
			} else if(x=='CONTACTUS') {
			nameChange = 'Contact Us';
			} else if(x=='VISA') {
			nameChange = 'Visa';
		}
		return nameChange;
		};
	});
	
	/* This function used to prepare a string for storage in a database and database queries. */
	
	function addslashes(str) {
		str = str.replace(/\\/g, '\\\\');
		str = str.replace(/\'/g, '\\\'');
		str = str.replace(/\"/g, '\\"');
		str = str.replace(/\0/g, '\\0');
		return str;
	}
	
	/* This function used to clean up data retrieved from a database or from an HTML form. */
	
	function stripslashes(str) {
		str = str.replace(/\\'/g, '\'');
		str = str.replace(/\\"/g, '"');
		str = str.replace(/\\0/g, '\0');
		str = str.replace(/\\\\/g, '\\');
		return str;
	}																											
	