"use strict";
ouaApp.controller( 'userReportCtrl' , function( $scope, $location, $interval, $http, $timeout, $routeParams,$rootScope, $routeSegment, ServerService, ajaxService,$window,NationService,CountryService) {
	$scope.data = '';
	$('body').attr('id','top');
	$('#top').addClass('thebg');
	
	// $scope.serviceBaseURl="https://wowsochi.ae/myaccount/b2cprofile/api";
	$scope.userServiceURL=ServerService.profilePath;
		// $scope.serviceBaseURl="https://wowsochi.ae/b2cprofile/api";
	$scope.init = {'page':1, 'count':10	};
	$scope.userProfileReq = {};
	$scope.noRecords = false;
	$scope.travelClick = false;
	$scope.fCheck = true;
	$scope.hCheck = false;
	$scope.insCheck =false;
	$scope.atCheck =false;
	$scope.serviceData = [{label: 'FLIGHT', value: 'FLT'}, {label: 'HOTEL', value: 'HTL'}, {label: 'VISA', value: 'VISA'}, {label: 'INSURANCE', value: 'INSRNC'}];

	// prepare traveller search request
	
	$scope.agentUserId =JSON.stringify( document.getElementById('agId').value);
	var fi;
	// $( "#userReportEmail" ).autocomplete({
	// 		source: function( request, response ) {
	// 			ajaxService.ajaxRequest('GET',ServerService.serverPath+'rest/repo/users/'+request.term+'?value='+$scope.agentUserId+'&page=1&size=10','json')
	// 			.then(function(data,status) {
	// 				if(data.status=="SUCCESS" && data.data!=null) {
	// 					fi=0;
	// 					response( data.data );
	// 					}else{
	// 					$( "#userReportEmail" ).val('');
	// 					$('.ui-autocomplete').hide();
	// 				}
	// 			});
	// 		},
	// 		focus: function( event, ui ) {
	// 			$( "#userReportEmail" ).val( ui.item.userName);
	// 			return false;
	// 		},
	// 		change : function(event,ui) {
	// 			$timeout(function(){
	// 				jQuery('.form-group').removeClass('has-error');
	// 			},10);
	// 		},
	// 		//autoFocus: true,
	// 		minLength: 3,
	// 		select: function( event, ui ) {
	// 			$( "#userReportEmail" ).val( ui.item.userName);
	// 			$scope.userProfileReq.createBy = ui.item.userId;
	// 			return false;
	// 		},
	// 		open: function() {
	// 			$( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
	// 		},
	// 		close: function(event, ui) {
	// 			$( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
	// 		}
	// 		}).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
	// 		if(fi==0) {
	// 			fi++;
	// 			return $( "<li>" ).append( "<a class='ui-state-focus'>" + item.userName +"</a>" ).appendTo( ul );
	// 			} else {
	// 			return $( "<li>" ).append( "<a>" + item.userName + "</a>" ).appendTo( ul );
	// 		}
	// 	};
	
	$scope.travelReq = function(page,count) {
		$scope.userProfileReq.category = "REPORT";
		$scope.userProfileReq.searchMode = 'grid';
		$scope.userProfileReq.gridMode = "child";
		$scope.userProfileReq.page = page;
		$scope.userProfileReq.count = count;
	};
	$scope.dataURL=$scope.serviceBaseURl+"/company-flight"


		$scope.columnFileds=[
			
			{
			
					field: "bookingDetails",
				title: "BookingDetails"
			},{
				// template:"<div class='customer-name'   onclick='viewDocPnr(#: clientNo #,#: bkngRefNo #,'FLT');'>#: bkngRefNo #</div>",
				field: "bkngRefNo",
				title: "Reference No"
			},{
				field: "bookingDate",
				title: "BookingDate"
			},{
				field: "firstName",
				title: "Passenger"
			}
			,{
				field: "totalFare",
				title: "TotalFare"
			},{
				field: "status",
				title: "Status"
				
			},{
				command: 
			   { text: "View", click: viewFLTPnr }, title: " ", width: "180px" 
		   }
			
		]  


		$("#grid").kendoGrid({
			dataSource:[],
			columns:[]
		});
	$scope.checkFlight = function() {
		$scope.fCheck = true;
		$scope.hCheck = false;
		$scope.insCheck =false;
		$scope.atCheck =false;
		$scope.dataURL=$scope.serviceBaseURl+"/company-flight"
		$scope.columnFileds=[
		
		
		// 	{
		// 		template:"<i class='fa fa-eye'   onclick='viewDocPnr(#: clientNo #,#: bkngRefNo #,'FLT');'></i>",
		// 		field: "bookingDetails",
		// 	title: "View"
		// },
			{
			
					field: "bookingDetails",
				title: "BookingDetails"
			},{
				// template:"<div class='customer-name'  onclick='viewDocPnr(#: clientNo #,#: bkngRefNo #,'FLT');'>#: bkngRefNo #</div>",
				field: "bkngRefNo",
				title: "Reference No"
			},{
				field: "bookingDate",
				title: "BookingDate"
			},{
				field: "firstName",
				title: "Passenger"
			}
			,{
				field: "totalFare",
				title: "TotalFare"
			},{
				field: "status",
				title: "Status"
				
			},{
				command: 
			   { text: "View", click: viewFLTPnr }, title: " ", width: "180px" 
		   }
			
		]
		$("#grid").kendoGrid({
			dataSource:[],
			columns:[]
		});
		// $scope.gridBinding();
	
	};
	$scope.checkInsurance = function(page) {
		$scope.fCheck = false;
		$scope.hCheck = false;
		$scope.insCheck =true;
		$scope.atCheck =false;
	
		$scope.dataURL=$scope.serviceBaseURl+"/company-insurance"
		$scope.columnFileds=[
			{
			
					field: "bookingDetails",
				title: "BookingDetails"
			},{
				// template:"<div class='customer-name'  onclick='viewDocPnr(#: clientNo #,#: bkngRefNo #,'INSRNC');'>#: bookingRefNo #</div>",
				field: "bookingRefNo",
				title: "Reference No"
			},{
				field: "gdsRefNo",
				title: "GDSRefNo"
			},		
			{
				field: "bookingDate",
				title: "BookingDate"
			},{
				field: "firstName",
				title: "Passenger"
			}
			,{
				field: "totalAmount",
				title: "TotalAmount"
			},{
				field: "status",
				title: "Status"
				
			},{
				command: 
			   { text: "View", click: viewinsPnr }, title: " ", width: "180px" 
		   }
			
		]
		$("#grid").kendoGrid({
			dataSource:[],
			columns:[]
		});
		// $scope.gridBinding();
	};

	$scope.checkHoliday = function(page) {
		$scope.fCheck = false;
		$scope.hCheck = true;
		$scope.insCheck =false;
		$scope.atCheck =false;
		$scope.dataURL=$scope.serviceBaseURl+"/company-holiday-attraction"
		$scope.columnFileds=[
		{
			// template:"<div class='customer-name' onclick='viewDocPnr(#: clientNo #,#: bkngRefNo #,'HOLIDAYS');'>#: merchantRef #</div>",
			field: "merchantRef",
			title: "Reference No"
		},{
			field: "bookingTime",
			title: "BookingDate"
		},{
			field: "firstName",
			title: "Passenger"
		},{
			field: "paxType",
			title: "Passenger Type"
		}
		,{
			field: "emailId",
			title: "EmailId"
		},{
			field: "mobileNumber",
			title: "MobileNumber"
			
		},{
			command: 
		   { text: "View", click: viewpacPnr }, title: " ", width: "180px" 
	   }
		]
	
		// $scope.gridBinding();
		$("#grid").kendoGrid({
			dataSource:[],
			columns:[]
		});
	};
	$scope.checkAttraction = function(page) {
		$scope.fCheck = false;
		$scope.hCheck = false;
		$scope.insCheck =false;
		$scope.atCheck =true;
		$scope.dataURL=$scope.serviceBaseURl+"/company-holiday-attraction"
		$scope.columnFileds=[
		{
			// template:"<div class='customer-name'  onclick='viewDocPnr(#: clientNo #,#: bkngRefNo #,'ATTR');'>#: merchantRef #</div>",
			field: "merchantRef",
			title: "Reference No"
		},{
			field: "bookingTime",
			title: "BookingDate"
		},{
			field: "firstName",
			title: "Passenger"
		},{
			field: "paxType",
			title: "Passenger Type"
		}
		,{
			field: "emailId",
			title: "EmailId"
		},{
			field: "mobileNumber",
			title: "MobileNumber"
			
		},{
			command: 
		   { text: "View", click: viewpacPnr }, title: " ", width: "180px" 
	   }
		]
		$("#grid").kendoGrid({
			dataSource:[],
			columns:[]
		});
		// $scope.gridBinding();
	};

	$scope.gridGetBinding= function() {
		$.ajax({
			
            
			url:$scope.dataURL,
			type: "GET",
			//   data: JSON.stringify($scope.dataTosend),
			contentType: "application/json; charset=utf-8",

			success: function (resdata, status) {
				if($scope.fCheck){
					for(var i=0;i<resdata.data.length;i++){
						resdata.data[i].bookingDetails=resdata.data[i].clientNo+' '+resdata.data[i].origin+'->'+resdata.data[i].destination
					}
				}
				
				$("#grid").kendoGrid({
					// toolbar: ["excel"],
					dataSource:resdata.data,
					//  {
						// type: "odata",
						// transport: {
						//     read: "http://192.168.1.37:8885/b2cprofile/api/company-htl"
						// },
						pageSize: 20,
					// },
					height: 550,
					groupable: true,
					sortable: true,
					filterable: true,
         
					pageable: {
						refresh: true,
						pageSizes: true,
						buttonCount: 5
					},
					columns: $scope.columnFileds,
				// });
				// excelExport: function(e) {
					// e.workbook.fileName = "Grid.xlsx";
					// var grid = $("#grid").data("kendoGrid");
					// grid.saveAsExcel();
				  // }
				}).data("kendoGrid");
				// var grid = $("#grid").data("kendoGrid");
				// grid.saveAsExcel();
			}
		});
	}
	$scope.gridBinding = function() {
		$.ajax({
			url:$scope.dataURL,
			type: "POST",
			  data: JSON.stringify($scope.dataTosend),
			contentType: "application/json; charset=utf-8",

			success: function (resdata, status) {
				if($scope.fCheck){
					for(var i=0;i<resdata.data.data.length;i++){
						resdata.data.data[i].bookingDetails=resdata.data.data[i].clientNo+' '+resdata.data.data[i].origin+'->'+resdata.data.data[i].destination
					}
				}else if($scope.insCheck){
					for(var i=0;i<resdata.data.data.length;i++){
						resdata.data.data[i].bookingDetails=resdata.data.data[i].tktRefNo+' '+resdata.data.data[i].origin+'->'+resdata.data.data[i].destination
					}
				}
				
				
				$("#grid").kendoGrid({
					// toolbar: ["excel"],
					toolbar: ["excel"],
            excel: {
                fileName: "BookingReport.xlsx",
                proxyURL: "https://demos.telerik.com/kendo-ui/service/export",
                filterable: true
            },
					dataSource:resdata.data.data,
					//  {
						// type: "odata",
						// transport: {
						//     read: "http://192.168.1.37:8885/b2cprofile/api/company-htl"
						// },
						pageSize: 20,
					// },
					height: 550,
					groupable: true,
					sortable: true,
					pageable: {
						refresh: true,
						pageSizes: true,
						buttonCount: 5
					},
					columns: $scope.columnFileds
				});
			}
		});
	}





	$scope.getTraveller = function (params, paramsObj) {
		
	

		if($scope.travelClick==false){
			$scope.userProfileReq = {};
		}
		$scope.travelReq(paramsObj.page,paramsObj.count);
		$scope.page = paramsObj.page;
		return $http.post(ServerService.serverPath+'rest/docket/search',$scope.userProfileReq).then(function (response) {
			if(response.data.status=='SUCCESS' && response.data.data!='null') {
				$scope.recentBookingData = response.data.data.docketChildList;
				$scope.totTrvCount = response.data.data.totalCount;
				$location.path('/user-reports');
				return {"header": [],"rows": $scope.recentBookingData,"pagination": {"count": 10,"page": $scope.page,"pages": $scope.totTrvCount/10,"size": $scope.totTrvCount}
				};
				} else {
				$scope.recentBookingData = [];
				$scope.noRecords = true;
				$location.path('/user-reports');
			}
		});	
	};
	
	$scope.tripsModify = function(pagination) {
		pagination.count = 10;
		pagination.page = $scope.page;
		pagination.pages = Math.ceil($scope.totTrvCount/10);
		pagination.size = $scope.totTrvCount;
	};
	
	$scope.search = function () {
		$scope.dataTosend= {
			"filter": {
			  "logic": "and",
			  "filters": [
				{
				  "field": $scope.field,
				  "operator": "ge",
				  "value": fromBookDate
				},
				 {
				  "field": $scope.field,
				  "operator": "le",
				  "value": toBookDate
				}
			  ]
			},"sort": [
				{
					"field": "createTime",
					"dir": "desc"
				}
			]
		  }
		if($scope.userProfileReq.fromDate && $scope.userProfileReq.toDate){
		var fromDate =$scope.userProfileReq.fromDate.split('-');
		var fromBookDate = fromDate[2]+'-'+fromDate[1]+'-'+fromDate[0];
		var toDate =$scope.userProfileReq.toDate.split('-');
		var toBookDate = toDate[2]+'-'+toDate[1]+'-'+toDate[0];
		if($scope.fCheck){
			$scope.field='bookingDate';
			$scope.dataURL=$scope.serviceBaseURl+"/search/company-flight"
		}else if($scope.hCheck){
			$scope.dataTosend= {
				"filter": {
				  "logic": "and",
				  "filters": [
					
					{
					 "field": 'screenType',
					 "operator": "eq",
					 "value": 'HOLIDAYS'
				   },
					{
					  "field": $scope.field,
					  "operator": "ge",
					  "value": fromBookDate
					},
					 {
					  "field": $scope.field,
					  "operator": "le",
					  "value": toBookDate
					}
					
				  ]
				},"sort": [
					{
						"field": "createTime",
						"dir": "desc"
					}
				]
			}
			
			$scope.field='bookingTime';
			$scope.dataURL=$scope.serviceBaseURl+"/search/company-holiday-attraction"
		}else if($scope.atCheck){
				$scope.dataTosend= {
					"filter": {
					  "logic": "and",
					  "filters": [
						
						{
						 "field": 'screenType',
						 "operator": "eq",
						 "value": 'ATTRACTIONS'
					   },
						{
						  "field": $scope.field,
						  "operator": "ge",
						  "value": fromBookDate
						},
						 {
						  "field": $scope.field,
						  "operator": "le",
						  "value": toBookDate
						}
						
					  ]
					},"sort": [
						{
							"field": "createTime",
							"dir": "desc"
						}
					]
				}
				
				$scope.field='bookingTime';
				$scope.dataURL=$scope.serviceBaseURl+"/search/company-holiday-attraction"
			}}else if($scope.insCheck){
			$scope.field='bookingDate';
			$scope.dataURL=$scope.serviceBaseURl+"/search/company-insurance"
		}
		// }

		
		
		  if($scope.userProfileReq.fromDate && $scope.userProfileReq.toDate){
			$scope.gridBinding();
		  }else if($scope.atCheck){
			$scope.dataTosend= {
				"filter": {
					"field": 'screenType',
					"operator": "eq",
					"value": 'ATTRACTIONS'
				  }
			  }
			$scope.dataURL=$scope.serviceBaseURl+"/search/company-holiday-attraction"
			$scope.gridBinding();
		  }else if($scope.hCheck){
			$scope.dataTosend= {
				"filter": {
					"field": 'screenType',
					"operator": "eq",
					"value": 'HOLIDAYS'
				  }
			  }
			$scope.dataURL=$scope.serviceBaseURl+"/search/company-holiday-attraction"
			$scope.gridBinding();
		  }else{
			$scope.gridGetBinding();
		  }
		

		$scope.travelClick = true;
		$scope.noRecords = false;
		$scope.travelReq(1,10);
		$scope.page = 1;
		$scope.userProfileReq.searchMode = 'grid';
		$scope.userProfileReq.gridMode = "child";
		$scope.recentBookingData = {};
		$http.post(ServerService.serverPath+'rest/docket/search',$scope.userProfileReq).then(function (response) {
			if(response.data.data!=null && response.data.status=='SUCCESS') {
				$scope.recentBookingData = angular.copy(response.data.data.docketChildList);
				$scope.totTrvCount = angular.copy(response.data.data.totalCount);
				$location.path('/user-reports');
				return {"header": [],"rows": $scope.recentBookingData,"pagination": {"count": 10,"page": $scope.page,"pages": $scope.totTrvCount/10,"size": $scope.totTrvCount}
				};
				} else {
				$scope.recentBookingData = [];
				$scope.noRecords = true;
				$location.path('/user-reports');
			}
		});
	};
	
	$scope.clear = function() {
		$scope.travelClick = true;
		$scope.noRecords = false;
		$scope.userProfileReq = {};
		$scope.travelReq(1,10);
		$scope.page = 1;
		$( "#userReportEmail" ).val('');
		$scope.userProfileReq.searchMode = 'grid';
		$scope.userProfileReq.gridMode = "child";
		$scope.recentBookingData = {};
		$http.post(ServerService.serverPath+'rest/docket/search',$scope.userProfileReq).then(function (response) {
			if(response.data.data!=null) {
				$scope.recentBookingData = angular.copy(response.data.data.docketChildList);
				$scope.totTrvCount = angular.copy(response.data.data.totalCount);
				$location.path('/user-reports');
				return {"header": [],"rows": $scope.recentBookingData,"pagination": {"count": 10,"page": $scope.page,"pages": $scope.totTrvCount/10,"size": $scope.totTrvCount}
				};
				} else{
				$scope.recentBookingData = [];
				$scope.noRecords = true;
				$location.path('/user-reports');
			}
		});
	}
	
	$('.bookingDate-from').datepicker({ 
		dateFormat:'dd-mm-yy',
		changeMonth: true,
		changeYear: true,
		minDate: '-5Y',
		maxDate: 0,
		//yearRange: '1900:'+(new Date).getFullYear(),
		showAnim: 'slideDown',
		onChangeMonthYear:function(y, m, i){                                
			var d = i.selectedDay;
			$(this).datepicker('setDate', new Date(y, m-1, d));
		}
	});
	$('.bookingDate-to').datepicker({ 
		dateFormat:'dd-mm-yy',
		changeMonth: true,
		changeYear: true,
		minDate: '-5Y',
		maxDate: 0,
		//yearRange: '1900:'+(new Date).getFullYear(),
		showAnim: 'slideDown',
		onChangeMonthYear:function(y, m, i){                                
			var d = i.selectedDay;
			$(this).datepicker('setDate', new Date(y, m-1, d));
		}
	});
	
	// $scope.viewDocPnr =function(clientRefNo, pnrNumber, prodType){
	// 	$('body').addClass("loading");
	// 	var docPnrRequest = new Object();
	// 	docPnrRequest.clientRefNo = clientRefNo;
	// 	docPnrRequest.bookingRefNo = pnrNumber;
	// 	docPnrRequest.type = 'DB';
	// 	if(prodType=='FLT'){
	// 		docPnrRequest.serviceType = 'FLT';
	// 	}
	// 	if(prodType=='HTL'){
	// 		docPnrRequest.serviceType = 'HTL';
	// 	}
	// 	if(prodType=='INSRNC'){
	// 		docPnrRequest.serviceType = 'INSRNC';
	// 	}
	// 	if(prodType=='VISA'){
	// 		docPnrRequest.serviceType = 'VISA';
	// 	}
	// 	if(prodType=='HOLIDAYS'){
	// 		docPnrRequest.serviceType = 'VISA';
	// 	}
	// 	if(prodType=='ATTR'){
	// 		docPnrRequest.serviceType = 'VISA';
	// 	}
	// 	if(prodType=='HOLIDAYS' || prodType=='ATTR'){
	// 		http({
	// 			method:'POST',				
	// 			data:docPnrRequestJson,
	// 			url:ServerService.serverPath+'rest/retrieve/itinerary',
	// 			}).success(function(data,status){				
	// 			if(data.status=="SUCCESS"){	
					
	// 			}})

	// 	}else{
		
	// 	var docPnrRequestJson = JSON.stringify(docPnrRequest);	
	// 	$http({
	// 		method:'POST',				
	// 		data:docPnrRequestJson,
	// 		url:ServerService.serverPath+'rest/retrieve/itinerary',
	// 		}).success(function(data,status){				
	// 		if(data.status=="SUCCESS"){	
	// 			var jsondata=data.data;
	// 			if(jsondata.serviceOrderType=='FLT'){
	// 				sessionStorage.setItem("flightconfirmJson",JSON.stringify(jsondata));
	// 				$location.path('/flight-itinerary');
	// 			}
	// 			if(jsondata.serviceOrderType=='HTL'){
	// 				sessionStorage.setItem("hotelBookJson",JSON.stringify(jsondata));
	// 				$location.path('/hotel-itinerary');
	// 			}
	// 			if(jsondata.serviceOrderType=='INSRNC'){
	// 				sessionStorage.setItem("insureconfirmJson",JSON.stringify(jsondata));
	// 				$location.path('/insurance-itinerary');
	// 			}
	// 			if(jsondata.serviceOrderType=='VISA'){
	// 				sessionStorage.setItem("visaItineraryJson",JSON.stringify(jsondata));
	// 				$location.path('/visa-itinerary');
	// 			}
	// 			if (jsondata.serviceOrderType=='FNH'){
	// 				sessionStorage.setItem("flightconfirmJson",JSON.stringify(jsondata.fltBookingInfo));
	// 				sessionStorage.setItem("hotelBookJson",JSON.stringify(jsondata.htlBookingInfo));
	// 				$location.path('/flight-hotel-itinerary');
	// 			}
	// 			if (jsondata.serviceOrderType=='HOLIDAYS'){
	// 				sessionStorage.setItem("holidayconfirmJson",JSON.stringify(jsondata.jsondata));
	// 				// sessionStorage.setItem("hotelBookJson",JSON.stringify(jsondata.htlBookingInfo));
	// 				$location.path('/confirmed-package-itinerary');
	// 			}
	// 			} else {
	// 			Lobibox.alert('error',
	// 			{
	// 				msg: data.errorMessage
	// 			});
	// 		}
	// 		$timeout(function(){
	// 			$('body').removeClass("loading");
	// 			$routeSegment.chain[0].reload();
	// 			//location.reload(true);
	// 			$("html,body").scrollTop(0);
	// 		},1000);
				
	
	// 	})
	// }
	// }
	// <script>
	function viewFLTPnr(e){
		var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
		// e.preventDefault();
		console.log(dataItem);
				$('body').addClass("loading");
				var docPnrRequest = new Object();
				docPnrRequest.clientRefNo = dataItem.clientNo;
				docPnrRequest.bookingRefNo = dataItem.bkngRefNo;
				docPnrRequest.type = 'DB';
				// if(prodType=='FLT'){
					docPnrRequest.serviceType = 'FLT';
				// }
				
				
				var docPnrRequestJson = JSON.stringify(docPnrRequest);	
				$http({
					method:'POST',				
					data:docPnrRequestJson,
					url:ServerService.serverPath+'rest/retrieve/itinerary',
					}).success(function(data,status){				
					if(data.status=="SUCCESS"){	
						var jsondata=data.data;
						$('body').removeClass("loading");
						// if(jsondata.serviceOrderType=='FLT'){
							sessionStorage.setItem("flightconfirmJson",JSON.stringify(jsondata));
							$location.path('/flight-itinerary');
						// }
					
						} else {
						Lobibox.alert('error',
						{
							msg: data.errorMessage
						});
					}
					$timeout(function(){
						$('body').removeClass("loading");
						$routeSegment.chain[0].reload();
						//location.reload(true);
						$("html,body").scrollTop(0);
					},1000);
						
			
				})
			
	}
	function viewinsPnr(e){
		var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
		// e.preventDefault();
		console.log(dataItem);
				$('body').addClass("loading");
				var docPnrRequest = new Object();
				docPnrRequest.clientRefNo = dataItem.tktRefNo;
				docPnrRequest.bookingRefNo = dataItem.bookingRefNo;
				docPnrRequest.type = 'DB';
				// if(prodType=='FLT'){
					docPnrRequest.serviceType = 'INSRNC';
				// }
				
				
				var docPnrRequestJson = JSON.stringify(docPnrRequest);	
				$http({
					method:'POST',				
					data:docPnrRequestJson,
					url:ServerService.serverPath+'rest/retrieve/itinerary',
					}).success(function(data,status){				
					if(data.status=="SUCCESS"){	
						var jsondata=data.data;
						$('body').removeClass("loading");
						// if(jsondata.serviceOrderType=='FLT'){
							sessionStorage.setItem("insureconfirmJson",JSON.stringify(jsondata));
					$location.path('/insurance-itinerary');
						// }
					
						} else {
						Lobibox.alert('error',
						{
							msg: data.errorMessage
						});
					}
					$timeout(function(){
						$('body').removeClass("loading");
						$routeSegment.chain[0].reload();
						//location.reload(true);
						$("html,body").scrollTop(0);
					},1000);
						
			
				})
			
	}
	function viewpacPnr(e){
		var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
		// e.preventDefault();
		console.log(dataItem);
				$('body').addClass("loading");
				var docPnrRequest = new Object();
				// docPnrRequest.clientRefNo = dataItem.tktRefNo;
				// docPnrRequest.bookingRefNo = dataItem.bookingRefNo;
				// docPnrRequest.type = 'DB';
				// // if(prodType=='FLT'){
				// 	docPnrRequest.serviceType = 'HOLIDAYS';
				// }
				
				
				// var docPnrRequestJson = JSON.stringify(docPnrRequest);	
				$http({
					method:'GET',				
					// data:docPnrRequestJson,
					url:ServerService.serverPath+'rest/holiday/retrieve/'+dataItem.merchantRef,
					contentType:"application/json; charset=utf-8"
					}).success(function(data,status){				
					if(data.status=="SUCCESS"){	
						$('body').removeClass("loading");
						var jsondata=data.data;
						// if(jsondata.serviceOrderType=='FLT'){
							sessionStorage.setItem("packageconfirmJson",JSON.stringify(jsondata));
					$location.path('/confirmed-package-itinerary');
						// }
					
						} else {
						Lobibox.alert('error',
						{
							msg: data.errorMessage
						});
					}
					$timeout(function(){
						$('body').removeClass("loading");
						$routeSegment.chain[0].reload();
						//location.reload(true);
						$("html,body").scrollTop(0);
					},1000);
						
			
				})
			
	}
function viewDocPnr(e){
console.log(this);
console.log(this.dataSource._data);
// e.preventDefault();
var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
// e.preventDefault();
console.log(dataItem);
		$('body').addClass("loading");
		var docPnrRequest = new Object();
		docPnrRequest.clientRefNo = dataItem.clientNo;
		docPnrRequest.bookingRefNo = dataItem.bkngRefNo;
		docPnrRequest.type = 'DB';
		if(prodType=='FLT'){
			docPnrRequest.serviceType = 'FLT';
		}
		if(prodType=='HTL'){
			docPnrRequest.serviceType = 'HTL';
		}
		if(prodType=='INSRNC'){
			docPnrRequest.serviceType = 'INSRNC';
		}
		if(prodType=='VISA'){
			docPnrRequest.serviceType = 'VISA';
		}
		if(prodType=='HOLIDAYS'){
			docPnrRequest.serviceType = 'VISA';
		}
		if(prodType=='ATTR'){
			docPnrRequest.serviceType = 'VISA';
		}
		if(prodType=='HOLIDAYS' || prodType=='ATTR'){
			http({
				method:'POST',				
				data:docPnrRequestJson,
				url:ServerService.serverPath+'rest/retrieve/itinerary',
				}).success(function(data,status){				
				if(data.status=="SUCCESS"){	
					$('body').removeClass("loading");
				}})

		}else{
		
		var docPnrRequestJson = JSON.stringify(docPnrRequest);	
		$http({
			method:'POST',				
			data:docPnrRequestJson,
			url:ServerService.serverPath+'rest/retrieve/itinerary',
			}).success(function(data,status){				
			if(data.status=="SUCCESS"){	
				var jsondata=data.data;
				if(jsondata.serviceOrderType=='FLT'){
					sessionStorage.setItem("flightconfirmJson",JSON.stringify(jsondata));
					$location.path('/flight-itinerary');
				}
				if(jsondata.serviceOrderType=='HTL'){
					sessionStorage.setItem("hotelBookJson",JSON.stringify(jsondata));
					$location.path('/hotel-itinerary');
				}
				if(jsondata.serviceOrderType=='INSRNC'){
					sessionStorage.setItem("insureconfirmJson",JSON.stringify(jsondata));
					$location.path('/insurance-itinerary');
				}
				if(jsondata.serviceOrderType=='VISA'){
					sessionStorage.setItem("visaItineraryJson",JSON.stringify(jsondata));
					$location.path('/visa-itinerary');
				}
				if (jsondata.serviceOrderType=='FNH'){
					sessionStorage.setItem("flightconfirmJson",JSON.stringify(jsondata.fltBookingInfo));
					sessionStorage.setItem("hotelBookJson",JSON.stringify(jsondata.htlBookingInfo));
					$location.path('/flight-hotel-itinerary');
				}
				if (jsondata.serviceOrderType=='HOLIDAYS'){
					sessionStorage.setItem("holidayconfirmJson",JSON.stringify(jsondata.jsondata));
					// sessionStorage.setItem("hotelBookJson",JSON.stringify(jsondata.htlBookingInfo));
					$location.path('/confirmed-package-itinerary');
				}
				} else {
				Lobibox.alert('error',
				{
					msg: data.errorMessage
				});
			}
			$timeout(function(){
				$('body').removeClass("loading");
				$routeSegment.chain[0].reload();
				//location.reload(true);
				$("html,body").scrollTop(0);
			},1000);
				
	
		})
	}
	}
{/* </script> */}
});
