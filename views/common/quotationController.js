ouaApp.controller('quotationControllers',function($scope,$rootScope,$location,commonCorpService,$http,$uibModalInstance,$uibModal,ServerService,corpCompanyDetails){
    $scope.gblSelectedHotelResp = JSON.parse(sessionStorage.getItem("HotelQuotationList"));
   
    $scope.hotelQuotationLength = JSON.parse(sessionStorage.getItem("hotelQuotationLength"));
    $scope.selectTraveldata = JSON.parse(sessionStorage.getItem('selectTraveldata'));
    $scope.redirectedURL = window.location.href;
    var cordinatorDetails = {};
    $scope.hotelSearchRQ = JSON.parse(sessionStorage.getItem('hotelSearchdataobj'));

    if(sessionStorage.getItem("QuoteCordinatorDetails") != undefined){
        cordinatorDetails = JSON.parse(sessionStorage.getItem("QuoteCordinatorDetails"));
        $scope.cordinatorDetails.email  = cordinatorDetails.email;
    }
    else if($scope.hotelSearchRQ.customers != undefined && $scope.hotelSearchRQ.customers != "")
    {
        $scope.cordinatorDetails ={};
        $scope.cordinatorDetails.email  = $scope.hotelSearchRQ.htlCorporateDetails.coordinatorEmail || "";

    }
    
    console.log($scope.hotellocation);
    
    corpCompanyDetails.getCompanyId();
    $scope.roomSelection = '';
    $scope.isRadioDisabled = true;

    if (sessionStorage.getItem('QuoteOBJ') != undefined) {
		$scope.quoteOBJ = JSON.parse(sessionStorage.getItem('QuoteOBJ'));
	}
    $scope.isUserView = false;

    $scope.isDisabled =  $scope.redirectedURL.split('#!/')[1] == 'quote-list' ? true : false;
    $scope.hotelQuote ={
        marKupValue : 0,
        disCountValue : 0
    }

    if($scope.selectTraveldata==null){
        $scope.travelFunctionality=true;
    }else{
        $scope.travelFunctionality=false;
    }

    $scope.cancel = function(){
	  $uibModalInstance.dismiss('cancel');
    }

    $scope.quotationReq={};
    $scope.allServiceDetails={};
    $scope.quotationData={
        hotel : {
            "quote-details" : {
                paxProfiles : [],
                searchDetails : ''
                },
            "hotel-items" : []
        },
        ins:{},
        rail:{},
        flight:{},
        transfer:{},

    };
    $scope.flightObj={};
    $scope.hotelObj={};
    $scope.insObj={};
    $scope.transferObj={};
    $scope.railObj={};
    $scope.quotationDetails={};
    $scope.hotelQuotation=[];
    $scope.insQuotation=[];
    $scope.trsnsferQuotation=[];
    $scope.railQuotation=[];
    $scope.hotelQuateSubmit = [];

    $scope.sendQuotation=function(){
        var email=document.getElementById('email').value;
        var internalRemarks=document.getElementById('internalRemarks').value;
        var extranelRemarks=document.getElementById('extranelRemarks').value;

       let date = new Date();

       var todayDate = date.getFullYear()  + "-" + (date.getMonth()+1) + "-" + date.getDate();
       
       $scope.flightDetailsQuotation = JSON.parse(sessionStorage.getItem("sendQuoteData"));
     
    //    $scope.gblSelectedHotelResp.forEach(hoteldetails => {
    //         hoteldetails.gblHotelQuatationList.forEach(element => {
    //             var tmphtl = hoteldetails;
    //             tmphtl.hotelRoomInfos = element;
    //             tmphtl.gblHotelQuatationList = [];
    //             $scope.hotelQuateSubmit.push(tmphtl);
    //         });
    //     });
    $scope.hotelSearchRQ.isPolicyAvailable=$scope.gblSelectedHotelResp[0].isPolicyAvailable;
        $scope.quotationReq.quotationNo=null,
       $scope.quotationReq.quotationStatus=null,
       $scope.quotationReq.requestDate=null;
       $scope.quotationReq.noOfServices=$scope.hotelQuotationLength,
       $scope.quotationReq.coordinatorId=cordinatorDetails.id,
       $scope.quotationReq.toEmail=email;
       $scope.quotationReq.agencyId=$scope.corprateCompanyId,
       $scope.quotationReq.lpoNo=null,
       $scope.quotationReq.fop=null,
       $scope.quotationReq.serviceDetails=$scope.allServiceDetails;
       $scope.quotationReq.serviceDetails.quotation=$scope.quotationData;
       $scope.quotationReq.serviceDetails.quotation.flight=$scope.flightObj;
       $scope.quotationReq.serviceDetails.quotation.hotel['quote-details'].paxProfiles=$scope.selectTraveldata;
       $scope.quotationReq.serviceDetails.quotation.hotel['quote-details'].searchDetails= $scope.hotelSearchRQ 
       $scope.quotationReq.serviceDetails.quotation.hotel['hotel-items']= $scope.gblSelectedHotelResp;
       $scope.quotationReq.serviceDetails.quotation.ins=$scope.insObj;
       $scope.quotationReq.serviceDetails.quotation.flight=$scope.flightObj;
       $scope.quotationReq.serviceDetails.quotation.ins['ins']= $scope.insQuotation;
       $scope.quotationReq.serviceDetails.quotation.transfer=$scope.transferObj;
       $scope.quotationReq.serviceDetails.quotation.transfer['transfer']= $scope.trsnsferQuotation;
       $scope.quotationReq.serviceDetails.quotation.rail=$scope.railObj;
       $scope.quotationReq.serviceDetails.quotation.rail['rail']= $scope.railQuotation;
       $scope.quotationReq.approvalRemarks=null,
       $scope.quotationReq.internalRemarks=internalRemarks;
       $scope.quotationReq.externalRemarks=extranelRemarks;
       $scope.quotationReq.version=1
      
       $scope.artloading = true;
       
        $http.post(ServerService.dropDownPath+'quote/sentForApproval',$scope.quotationReq,{}).
        then(function successCallback(response){
            $scope.artloading = false;
            if(response.data.success==true){
            $scope.cancellationPolicy = response.data
                    sessionStorage.setItem('quotationSaveResponse',JSON.stringify(response.data.data));
                
                    Lobibox.notify('success', {
                    size: 'mini',
                    position: 'top right',
                    msg: response.data.message,
                    delay: 1500
                }); 
        
            $uibModalInstance.dismiss('cancel');  
            $location.path('/hotel');

            }else{
                Lobibox.notify('error', {
                    size: 'mini',
                    position: 'top right',
                    msg:response.data.errorMessage,
                    delay: 1500
                });
            } 
        }, function errorCallback(response) {
            if(response.status == 403){
                var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
                status.then(function(greeting) {
                hotelcply();
                });
            }
        });
   }

       
   $scope.selectingTrvlr=function(event,parentSelector)
   {
    var parentElem = parentSelector ?
    angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;

        $scope.animationEnabled = true;

        var modalInstance=$uibModal.open({
        animation: $scope.animationEnabled,
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: 'addtravel.html',
        controller: 'addtravelerControllers',
        backdrop: true,
        size: 'lg',
        appendTo: parentElem,
        resolve: {
            items: function() {
                return $scope.items
            }
        }
        });
        
        modalInstance.result.then(function() {
            $scope.selectTraveldata = JSON.parse(sessionStorage.getItem('selectTraveldata'));
        })
   }

   $scope.roomDtls = [
       {
        "roomIndex": '',
        "roomRefNo": "",
        "roomCode": ""
       }
   ]

   $scope.quotationReprice = function(sessionId){
        var repriceReq={
            "sessionId": sessionId,
            "resultIndexId": $scope.selectedRoom.resultIndex,
            "corp":true
        }
            $http.post(ServerService.dropDownPath+'customer/'+$scope.corprateCompanyId+'/hotel/reprice', repriceReq, {
            }).then(function successCallback(response) {
                if(response.data.success){

                $scope.artloading = false;
                
                    
				 $scope.repricesession = response.data
				 $scope.repriceResp = response.data.data;
                 var selectedQuoteList = JSON.parse(sessionStorage.getItem('bookingQuotationResponse'));
                selectedQuoteList.serviceDetails.quotation.hotel['hotel-items'][$scope.selectedParentIndx].gblHotelQuatationList[$scope.selectedIndx];
                sessionStorage.setItem('bookingQuotationResponse', JSON.stringify(selectedQuoteList));
                sessionStorage.setItem('repriceResp',JSON.stringify($scope.repriceResp));
                sessionStorage.setItem('repriceRespSessionId',JSON.stringify($scope.repricesession));
                sessionStorage.setItem('selectedHotels',JSON.stringify($scope.selectedRoom));
	            $uibModalInstance.dismiss('cancel');
                
                $location.path('/hotelReview');     

            }else{
                $scope.artloading = false;
                Lobibox.notify('error', {
                    size: 'mini',
                    position: 'top right',
                    msg:response.data.errorMessage,
                    delay: 1500
                });
            }
        });
   }



   $scope.outPolicyCheck = function(size, parentSelector, outPolicy) {
    var parentElem = parentSelector ?
        angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
   
     $rootScope.outPolicyData = outPolicy;
    


    $scope.animationEnabled = true;
    var modalInstance = $uibModal.open({
        animation: $scope.animationEnabled,
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: 'outPolicy.html',
        controller: 'outPolicyHotelController',
        backdrop: true,
        size: size,
        appendTo: parentElem,
        resolve: {
            items: function() {
                return $scope.items
            }
        }
    })
}

   $scope.hotelBook = function(){
        if( $scope.selectedRoom==undefined){
            Lobibox.notify('error', {
                size: 'mini',
                position: 'top right',
                msg:"Please select at least one quotation",
                delay: 1500
            });
            return;
        }
        var roomDetails = $scope.selectedRoom.hotelRoomInfos[`${$scope.key}`];
        var currency="AED";
        $scope.roomDtls[0].roomRefNo = roomDetails.roomRefNo
        $scope.roomDtls[0].roomIndex = roomDetails.roomIndex
        $scope.roomDtls[0].roomCode = roomDetails.roomCode
        $scope.hotelQuotationBook={};
        $scope.hotelQuotationBook.roomDtls = $scope.roomDtls;
        $scope.hotelQuotationBook.hotelSearchResult= $scope.selectedRoom;
        $scope.artloading = true;
        $http.post(ServerService.dropDownPath+'customer/'+$scope.corprateCompanyId+'/H/session/store',$scope.hotelQuotationBook,{ 
        }).then(function successCallback(response){
    
    
           if(response.data.success==true){
            $scope.sessionId = response.data.data;
                  $scope.quotationReprice($scope.sessionId,$scope.hotelQuotationBook);
            }else{
                $scope.artloading = false;
            } 
                   
            $scope.artloading = false;
           
           
      })
    }


   $scope.slectHotelquote =  function(event,selectedroom,parentindex,index){
        $scope.gblSelectedHotelResp[parentindex];
        var tmphotelobj = {};

        $scope.selectedParentIndx = parentindex;
        $scope.selectedIndx = index;

        tmphotelobj = $scope.gblSelectedHotelResp[parentindex];
        sessionStorage.setItem('selectedHotelResp', JSON.stringify({data:$scope.gblSelectedHotelResp[parentindex]}));

        $scope.key = "";
        var roominfos = "";
        Object.keys(tmphotelobj.hotelRoomInfos ).forEach(key => {
            if (tmphotelobj.hotelRoomInfos[key].roomRefNo == selectedroom[0].roomInfos[0].roomRefNo) {
                $scope.key = key;
            }
          });
        tmphotelobj.hotelRoomInfos = {};
        tmphotelobj.hotelRoomInfos[`${$scope.key}`] = selectedroom[0].roomInfos[0];
        
        // tmphotelobj.gblHotelQuatationList = [];
        $scope.selectedRoom = tmphotelobj;

   }

   $scope.hotelTabAction  = function(tab,index)
   {
        switch(tab){
            case 1 : 
                    $(".roomopt_"+index).addClass('active');
                    $(".roomopt_"+index).addClass('show');
                    $(".facitiesopt_"+index).removeClass('active');
                    $(".facitiesopt_"+index).removeClass('show');
                    $(".locopt_"+index).removeClass('active');
                    $(".locopt_"+index).removeClass('show');
                    $(".aboutopt_"+index).removeClass('active');
                    $(".aboutopt_"+index).removeClass('show');
                break;
            case 2 :
                    $(".roomopt_"+index).removeClass('active');
                    $(".roomopt_"+index).removeClass('show');
                    $(".facitiesopt_"+index).addClass('active');
                    $(".facitiesopt_"+index).addClass('show');
                    $(".locopt_"+index).removeClass('active');
                    $(".locopt_"+index).removeClass('show');
                    $(".aboutopt_"+index).removeClass('active');
                    $(".aboutopt_"+index).removeClass('show');
                break;
            case 3 :
                    $(".roomopt_"+index).removeClass('active');
                    $(".roomopt_"+index).removeClass('show');
                    $(".facitiesopt_"+index).removeClass('active');
                    $(".facitiesopt_"+index).removeClass('show');
                    $(".locopt_"+index).addClass('active');
                    $(".locopt_"+index).addClass('show');
                    $(".aboutopt_"+index).removeClass('active');
                    $(".aboutopt_"+index).removeClass('show');
                break;
            case 4 :
                    $(".roomopt_"+index).removeClass('active');
                    $(".roomopt_"+index).removeClass('show');
                    $(".facitiesopt_"+index).removeClass('active');
                    $(".facitiesopt_"+index).removeClass('show');
                    $(".locopt_"+index).removeClass('active');
                    $(".locopt_"+index).removeClass('show');
                    $(".aboutopt_"+index).addClass('active');
                    $(".aboutopt_"+index).addClass('show');
                break;
        }
   }

   $scope.removeQuote = function(index,parentIndex){

        $scope.gblSelectedHotelResp[parentIndex].gblHotelQuatationList.splice(index,1);
   }

   $scope.hotelcancelationPolicy = function (size, parentSelector, roomInfo,parentIndex) {
    sessionStorage.setItem('selectedHotelResp', JSON.stringify({data:$scope.gblSelectedHotelResp[parentIndex]}));
    var parentElem = parentSelector ?
        angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
        $rootScope.selectedroomInfo = roomInfo;
        $scope.animationEnabled = true;
        $rootScope.cancellationPolicy = roomInfo.hotelCPolicy
        $scope.hotelCancelationModalInstance=$uibModal.open({
            templateUrl: 'hotelCancelation.tmpl.html',
            backdrop: 'static',
            size: size,
            keyboard: true,
            scope:$scope
        });
    }

    $scope.hotelRate = function (size, parentSelector, roomInfo,parentIndex) {
    sessionStorage.setItem('selectedHotelResp', JSON.stringify({data:$scope.gblSelectedHotelResp[parentIndex]}));
    var parentElem = parentSelector ?
		angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
		$rootScope.selectedroomInfo = roomInfo;
        $rootScope.rateComments = roomInfo.hotelratecmds;
        $scope.animationEnabled = true;
        $scope.hotelNotesModalInstance=$uibModal.open({
            templateUrl: 'hotelRate.tmpl.html',
            backdrop: 'static',
            size: size,
            keyboard: true,
            scope:$scope
        });
	}

    	
	$scope.cancelpsasolicy = function () {
        $scope.hotelCancelationModalInstance.dismiss('cancel');
      };

    $scope.notespsasolicy = function () {
        $scope.hotelNotesModalInstance.dismiss('cancel');
    };

})
ouaApp.controller('outPolicyHotelController', function($scope, $http,ServerService, $uibModalInstance, $rootScope) {
    $scope.bookingQuotationResponse = JSON.parse(sessionStorage.getItem('bookingQuotationResponse'));
    $scope.policydetails= $rootScope.outPolicyData;
    if($scope.bookingQuotationResponse!=null){
    $scope.corprateCompanyId=$scope.bookingQuotationResponse.agencyId;
      
  
       $scope.hoteldata=$scope.bookingQuotationResponse.serviceDetails.quotation.hotel['hotel-items']
       $scope.hotelSearch=$scope.bookingQuotationResponse.serviceDetails.quotation.hotel['quote-details'].searchDetails
       $scope.policyRequestObj={};
       $scope.policyRequestObj.checkInDate=  $scope.hotelSearch.checkIn
       $scope.policyRequestObj.checkOutDate=  $scope.hotelSearch.checkOut
       $scope.policyRequestObj.city=  $scope.hoteldata[0].cityName
       $scope.policyRequestObj.starRating= $scope.hoteldata[0].starRating,
       $scope.policyRequestObj.totalFare=   $scope.policydetails[0].roomInfos[0].roomFare,
       $scope.policyRequestObj.designation= $scope.hotelSearch.designation
       $scope.quoteURL= "/hotel/deviation";
    }else{
        $scope.hotelSearch = JSON.parse(sessionStorage.getItem('hotelSearchdataobj'));
        $scope.selectedHotelResp = JSON.parse(sessionStorage.getItem('selectedHotelResp')).data;
        $scope.policyRequestObj={};
        $scope.policyRequestObj.checkInDate=  $scope.selectedHotelResp.checkInDate
        $scope.policyRequestObj.checkOutDate=  $scope.selectedHotelResp.checkOutDate
        $scope.policyRequestObj.city=  $scope.selectedHotelResp.cityName
        $scope.policyRequestObj.starRating= $scope.selectedHotelResp.starRating.toFixed(1),
        $scope.policyRequestObj.totalFare=   $scope.policydetails[0].roomInfos[0].roomFare,
        $scope.policyRequestObj.designation= $scope.hotelSearch.designation
        $scope.quoteURL= "/hotel/deviation";
    }
      $scope.tokenresult = JSON.parse(sessionStorage.getItem('authToken'));
      var headersOptions = {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + $scope.tokenresult
      }
  
    $http.post(ServerService.dropDownPath + 'customer/'+ $scope.corprateCompanyId + $scope.quoteURL,$scope.policyRequestObj, {
    
        headers: headersOptions
    }).then(function successCallback(response) {
        
    $scope.loading = false;
    $scope.disabled = false;
    if (response.data.success == true) {
        $scope.outPolicy=response.data.data;

    }else{
$scope.noPolicy="Don't Have Any No Policys"
    }
  
    });
    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };

});
