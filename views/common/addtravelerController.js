ouaApp.controller('addtravelerControllers',function($scope,$rootScope,ServerService,$http,$uibModalInstance,$uibModal,corpCompanyDetails){

  console.log("hii");
  $scope.close =  function(){
    $uibModalInstance.dismiss('cancel');
}
$scope.redirectedURL = window.location.href;
$scope.isCount =  $scope.redirectedURL.split('#!/')[1] == 'hotel' || 'hotelResult' ? true : false;
corpCompanyDetails.getCompanyId();

$scope.searchAdultCount = 0;
$scope.searchChildCount = 0;
$scope.selectTraveldata = [];
var travellerlist = new Map();

if (sessionStorage.getItem('selectedroom') != undefined) {
    $scope.selectedRoom = JSON.parse(sessionStorage.getItem('selectedroom'));
}
if (sessionStorage.getItem('selectTraveldata') != undefined) {
    $scope.selectTraveldata = JSON.parse(sessionStorage.getItem('selectTraveldata'));
    $scope.selectTraveldata.forEach(element => {
        travellerlist.set(element.roomno,element);
    });
}
if (sessionStorage.getItem('hotelSearchdataobj') != undefined) {
    $scope.hotelSearch = JSON.parse(sessionStorage.getItem('hotelSearchdataobj'));

    $scope.hotelSearch.roomBean.forEach(element => {
        $scope.searchAdultCount = $scope.searchAdultCount + Number(element.adultCount);
        $scope.searchChildCount = $scope.searchChildCount + Number(element.childCount);
    });

    $scope.searchPassangerCount = $scope.searchAdultCount + $scope.searchChildCount;

}else{

    $scope.hotelSearch = {
        city: "",
        checkIn: "",
        checkOut: "",
        nationality: "",
        country: "",
        countryOfResidence: "",
        isCompany : false,
        isPax : false,
        fop : "",
        providerList:[],
        supplier : [],
        customers : "",
        travelType : "",
        roomBean: [{
            "adultCount" : "0",
            "childCount" : "0",
            "childrens" : [],
            "roomno" : 1
        }]

    };
}

var childage = function() {
    this.age = "1";
}

var childrens = function() {
    this.childCount = "0";
    this.childBeans = [];
}
var selectTraveller = new Map();
$scope.isSelectTraveller = false;


$scope.initSrchObj = function()
{
  $scope.trvellersrch = {
      paxName:"",
      birthdate:null,
      birthmonth:null,
      birthyear:null,
      email:"",
      mobcntrycodetrobj:"",
      mobile:"",
      passportNo:"",
      surName:""
    };    
    $scope.phNoREQ = false;
    $scope.ccREQ = false;
}


$scope.initSrchObj();

$scope.dependents=false   
$scope.dependent=function(cheked){
$scope.dependents=cheked
}
$scope.selectTravellerData = [];
$scope.addTravel = function(checked, singlePsgr, age, indexvalue,uniqueValue) {
    $scope.hchildCount = ["0","1","2"];
    $scope.hadultCount = ["1","2","3","4"];
 
    var childcount = 0;
    var adtlcount = 0;
    if(checked){
        selectTraveller.set(singlePsgr.customerId,singlePsgr);
        $scope.selectTravellerData=Array.from(selectTraveller.values());
        $scope.paxTravellerCount=true;
        // $scope.selectTravellerData.push(singlePsgr);

        // $scope.flightSearchRQ.adtCnt = 3;
        if (age >= 12 && $scope.isCount) {
            for(a=0; a< $scope.selectTravellerData.length; a++){
                if( $scope.selectTravellerData[a].age >= 12){
                    adtlcount++;
                }
            }
            $scope.hotelSearch.roomBean[$scope.selectedRoom].adultCount =(adtlcount).toString();

        }else if(age < 12 && $scope.isCount){
            $scope.hotelSearch.roomBean[$scope.selectedRoom].childrens = [];
            var childrenBean = new childrens();
    
            $scope.hotelSearch.roomBean[$scope.selectedRoom].childrens.push(childrenBean);

            for(a=0; a< $scope.selectTravellerData.length; a++){
                if($scope.selectTravellerData[a].age < 12){
                    childcount++;
                    var childBeans = new childage();
                    $scope.hotelSearch.roomBean[$scope.selectedRoom].childrens[childcount-1].childBeans.push(childBeans);
                    // $scope.hotelSearch.roomBean[$scope.selectedRoom].childrens[childcount-1].childBeans[0].age = $scope.selectTravellerData[a].age;
                }
            }
            $scope.hotelSearch.roomBean[$scope.selectedRoom].childCount =(childcount).toString();
        }
    }else{
        selectTraveller.delete(singlePsgr.customerId)
        $scope.selectTravellerData=Array.from(selectTraveller.values());
        sessionStorage.setItem("sendQuoteData", JSON.stringify($scope.selectTravellerData));
    }

    sessionStorage.setItem("hotelSearchdataobj", JSON.stringify($scope.hotelSearch));
    $scope.isSelectTraveller = $scope.searchPassangerCount == $scope.selectTravellerData.length ? true : false;
}
// sessionStorage.removeItem("selectTraveldata");


initilizecheck =  function(ischeck){
      
    $scope.allTravellerData.forEach(element => {
        if(ischeck && element.checked)
            $scope.selectTravellerData.push(element);
        else
            element = element.checked = false || "";
    });
  }

  filterSelectedTraveller = function(){
    $scope.selectTravellerData = $scope.allTravellerData.filter((item)=>{
        return item.checked == true;
    })

    if($scope.dependents)
    {
        $scope.allTravellerData.map(element => {
            if(element.dependents)
            {
                element.dependents.forEach(depend => {
                    if(depend.checked)
                        $scope.selectTravellerData.push(depend);
                });
            }
        });
    }

  }

$scope.ConfirmTravellerData=function(){
    $scope.adtTravellerData = [];
    $scope.chdTravellerData = [];
    adtlcount = 0;
    childcount = 0;
    filterSelectedTraveller();
    // initilizecheck(true);

    if($scope.selectTravellerData.length > 0 ){

    $scope.selectTravellerData.forEach(element => {
        if(element.age >= 12 && $scope.isCount){
            $scope.adtTravellerData.push(element);
            adtlcount++;
        }else if(element.age < 12 && $scope.isCount)
        {
            $scope.chdTravellerData.push(element);
            childcount++;
            $scope.hotelSearch.roomBean[$scope.selectedRoom].childrens = [];
            var childrenBean = new childrens();
    
            $scope.hotelSearch.roomBean[$scope.selectedRoom].childrens.push(childrenBean);
            
            var childBeans = new childage();
            $scope.hotelSearch.roomBean[$scope.selectedRoom].childrens[childcount-1].childBeans.push(childBeans);

        }
    });

    if(adtlcount < 1 && childcount >= 1 )
    {
        Lobibox.notify('error', {
            size: 'mini',
            position: 'top right',
            msg:"Please select at least one ADULT!",
            delay: 1500
        });
        return false;
    }else{
        Lobibox.notify('success', {
            size: 'mini',
            position: 'top right',
            msg: "Travellers  added successfully!",
            delay: 1500
        });
    }
    
    $scope.hotelSearch.roomBean[$scope.selectedRoom].adultCount =(adtlcount).toString();
    $scope.hotelSearch.roomBean[$scope.selectedRoom].childCount =(childcount).toString();
    sessionStorage.setItem("hotelSearchdataobj", JSON.stringify($scope.hotelSearch));

    var tmpselecttraveller = {
        "roomno" : $scope.selectedRoom,
        "adtTraveller" : $scope.adtTravellerData,
        "chdTraveller" : $scope.chdTravellerData
    }

    travellerlist.set( $scope.selectedRoom,tmpselecttraveller);
    $scope.selectTraveldata=Array.from(travellerlist.values());

    // $scope.selectTraveldata.push(tmpselecttraveller);
    sessionStorage.setItem('selectTraveldata', JSON.stringify($scope.selectTraveldata));

    $uibModalInstance.close();

    } else{
        Lobibox.notify('error', {
            size: 'mini',
            position: 'top right',
            msg:"Please select at least one traveller!",
            delay: 1500
        });
    }

}
function range(start, end) {
    return Array(end - start + 1).fill().map((_, idx) => start + idx)
  }
  //calculate age from today date
  var curd = new Date();
  var curyear = curd.getFullYear();
  var curmonth = curd.getMonth();
  var curday = curd.getDate();
  var maxinfantdate = new Date(curyear - 2, curmonth, curday);
  var maxchilddate = new Date(curyear - 0, curmonth, curday);
  var maxadultdate = new Date(curyear - 55, curmonth, curday);

 

  var maxCNNY = maxchilddate.getFullYear();

  $scope.months = 12;
   $scope.getminexpirymonth = 12;

   $scope.dobdates = range(1, 31);


   $scope.ayears = range(curyear-55, maxCNNY);
  
   $scope.isdependents = false;

   $scope.dobmonths = range(1, 12);
$scope.searchTrvlr=function(){
    var tmpdependents = ($scope.trvellersrch.paxName.length >2 || $scope.trvellersrch.surName.length>2 || $scope.trvellersrch.passportNo.length > 4);
    $scope.checkedDependent = $scope.dependents;
    $scope.isdependents = tmpdependents && $scope.checkedDependent ? true : false;
    
    $scope.artloadingcancellation = true;
    var templist =[];
    if($scope.trvellersrch.paxName != "")
    {
        var innerobj = {
            "field": "firstName",
            "operator": "contains",
            "value": $scope.trvellersrch.paxName
        }
        templist.push(innerobj);
    }

    if($scope.trvellersrch.surName != "")
    {
        var innerobj = {
            "field": "lastName",
            "operator": "contains",
            "value": $scope.trvellersrch.surName
        }
        templist.push(innerobj);
    }

    if($scope.trvellersrch.birthdate != null && $scope.trvellersrch.birthmonth != null && $scope.trvellersrch.birthyear != null)
    {
        var innerobj = {
            "field": "dateOfBirth",
            "operator": "eq",
            "value": $scope.trvellersrch.birthyear + "-" + $scope.trvellersrch.birthmonth + "-" + $scope.trvellersrch.birthdate
        }
        templist.push(innerobj);
    }

    if($scope.trvellersrch.email != "")
    {
        var innerobj = {
            "field": "emailId",
            "operator": "eq",
            "value": $scope.trvellersrch.email
        }
        templist.push(innerobj);
    }

    if($scope.trvellersrch.passportNo != "")
    {
        var innerobj = {
            "field": "passportNo",
            "operator": "eq",
            "value": $scope.trvellersrch.passportNo
        }
        templist.push(innerobj);
    }

    if($scope.trvellersrch.mobile != "" )
    {
        if($scope.trvellersrch.mobcntrycodetrobj !="")
        {

        }
    }
    if($scope.trvellersrch.mobile != "" && $scope.trvellersrch.mobcntrycodetrobj !="")
    {
        var innerobj = {
            "field": "countryCode",
            "operator": "eq",
            "value": $scope.trvellersrch.mobcntrycodetrobj.name
        }
        templist.push(innerobj);
    }

    if($scope.trvellersrch.mobile != "" && $scope.trvellersrch.mobcntrycodetrobj !="")
    {
        var innerobj = {
            "field": "phoneNumber",
            "operator": "eq",
            "value": $scope.trvellersrch.mobile
        }
        templist.push(innerobj);
    }
    var filterReq = {
        "page": null,
        "getAllResults": true,
        "filter": {
            "logic": "and",
            "filters": templist
        },
        "sort": [
            {
            "field": "createTime",
            "dir": "desc"
            }
        ]
        };

    
    $scope.usersObj=undefined;
    $scope.tokenresult = JSON.parse(sessionStorage.getItem('authToken'));
    var headersOptions = {
'Content-Type': 'application/json',
'Authorization': 'Bearer ' + $scope.tokenresult
}
    $http.post(ServerService.listPath + 'search/corporate-customers',filterReq, {
        headers: headersOptions
    }).then(function successCallback(response) {
        $scope.trvSearchdata = response.data.data.data;
        $scope.artloadingcancellation = false;
        $('#checkedvalue').prop('checked', true);
    
        sessionStorage.setItem('loginTravellerData', JSON.stringify($scope.trvSearchdata));
        $scope.allTravellerData=[];

        $scope.allTravellerData = JSON.parse(sessionStorage.getItem('loginTravellerData'));
        $scope.trvellersrchyears = $scope.ayears;
        $scope.trvellersrchday = $scope.dobdates;
        $scope.trvellersrchmonth = $scope.dobmonths;    
        $scope.countryList = JSON.parse(sessionStorage.getItem('countryList'));
    });
    
}

$scope.searchTrvlr();

})        .directive('expand', function () {
    function link($scope, element, attrs) {
        $scope.$on('onExpandAll', function (event, args) {
            $scope.expanded = args.expanded;
        });
    }
    return {
        link: link
    };
});