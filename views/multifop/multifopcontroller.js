ouaApp.controller('multifopcontroller', function($scope, $http,renderHTMLFactory, $location,$uibModal, ServerService,$rootScope) {

    $scope.fopBeanList = JSON.parse(sessionStorage.getItem("fopBeansList"));
    $scope.ismutifopedit = true;
	$scope.comparetotal = JSON.parse(sessionStorage.getItem('GreatGrandTotal'));
	// $scope.fopChangedTotal = JSON.parse(sessionStorage.getItem('FopChangedTotal'));
    $scope.grandtotalmesg = "";
    
    // $scope.comparetotal = $scope.fopChangedTotal !=undefined ? $scope.fopChangedTotal : $scope.repriceResp.grandTotal;

    $scope.fopTotal = $scope.comparetotal;
    $scope.calculateTotal = function(){
        var tmptotal = 0;
        $scope.companyFOPList.forEach(element => {
            tmptotal += element.updatedFare ; 
            // if(element.updatedFare > 0){
            //     var req = {
            //                 "fopId": element.fopId,
            //                 "paidAmount" : element.updatedFare
            //             }
            //     $scope.mutliFopReq.push(req);
            // }
        });
        $scope.fopTotal = tmptotal;

        if($scope.fopTotal > $scope.comparetotal)
        {
            $scope.grandtotalmesg = "Payment exceeds the grand total!";
        }else{
            $scope.grandtotalmesg = "";
        }
    }

	$rootScope.fopChangedTotal = 0;
    fopRepriceServiceCall = function(){
        $scope.mutliFopReq = [];
        $scope.companyFOPList.forEach((item,index) => {
            if(item.updatedFare > 0){
                var req = {
                    "fopId": item.fopId,
                    "amount" : item.updatedFare
                }
                $scope.mutliFopReq.push(req);
            }
        });

        
        $rootScope.isBookDisable = false;
        var fopQuoteReq ={
            "sessionId": sessionStorage.getItem('FopSessionId'),
            "fopBeans" : $scope.mutliFopReq
        
        }
        $http.post(ServerService.dropDownPath + 'customer/'+$scope.corprateCompanyId+'/hotel/reprice/fop', fopQuoteReq,
        {
        }).then(function successCallback(response) 
        {
            if(response.data.success){
                $scope.artloading = false;
                $scope.isFareChanged = response.data.data.priceChange
                if($scope.isFareChanged)
                {
                    Lobibox.alert('error',{
                        msg:  response.data.data.msg,
                        callback: function (lobibox, type) {
                            if (type === 'ok') 
                            {
                                $rootScope.$emit("FopRepriceCall",{});
                                $rootScope.fopChangedTotal = response.data.data.grandTotal;
                                sessionStorage.setItem('FopChangedTotal', JSON.stringify($rootScope.fopChangedTotal));
                                $scope.ismutifopedit = !$scope.ismutifopedit;
                                $rootScope.$emit("FopBookValueUpdate",{ "value" :$scope.ismutifopedit});

                                // $scope.isCreditCard = $scope.hotelSearch.fopId == 25441 ?  true : false;
                                // sessionStorage.setItem('hotelSearchdataobj', JSON.stringify($scope.hotelSearch));
                                $scope.$apply();
                            }
                        }
                    });

                }
                // else{
                //     $scope.isCreditCard = $scope.hotelSearch.fopId == 25441 ?  true : false;
                //     sessionStorage.setItem('hotelSearchdataobj', JSON.stringify($scope.hotelSearch));
                // }
            }else{
                $scope.artloading = false;
                Lobibox.notify('error', {
                    size: 'mini',
                    position: 'top right',
                    msg:response.data.errorMessage,
                    delay: 1500
                });
            }
        });
    }
    $scope.fopReprice = function(){
        if($scope.fopTotal > $scope.comparetotal)
        {
            $scope.grandtotalmesg = "Payment exceeds the grand total!";
        }
        else if($scope.fopTotal < $scope.comparetotal)
        {
            $scope.grandtotalmesg = "Payment less than the grand total!";
        }
        // else if($scope.fopTotal == $scope.comparetotal)
        // {
        //     $scope.grandtotalmesg = "Changes not happen!";
        // }
        else
        {
            $scope.grandtotalmesg = "";
            fopRepriceServiceCall();     
        }

    }

    $scope.onEdit = function(){
        $scope.ismutifopedit = !$scope.ismutifopedit;
        $rootScope.$emit("FopBookValueUpdate",{ "value" :$scope.ismutifopedit});
        $rootScope.isBookDisable = true;
        $scope.$apply();
    }

    getPcPaymentTypeList = function(){
     
        $http.get(ServerService.listPath + 'common?codeType=PC_PAYMENT_TYPE',{}).then(function successCallback(response){
            if(response.data.success){
                $scope.pcPaymentType = response.data.data;
            }
        })
    }

    getPcPaymentTypeList();
});