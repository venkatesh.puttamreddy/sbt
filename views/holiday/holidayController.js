ouaApp.controller('holidayController', function ($scope, refreshService, $http, $location, ServerService, $timeout) {
    $('html, body').scrollTop(0);
    $("#load").show();
    setTimeout(function () {
        $('#load').hide();
    }, 1000);
    $scope.categoryItemList = [{
        categoryID:"",
        categoryName:"",
        year:"",
        month:"",
        price:""
    }];
    $scope.priceRanges = [];
    var today = new Date();
    month = '' + (today.getMonth() + 1),
        day = '' + today.getDate(),
        year = today.getFullYear();

    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;
    $scope.dateformat = [year, month, day].join('-');

    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });

    function getFilter() {
        $scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));
        var tokenname = $scope.tokenresult.data;
        var headersOptions = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + tokenname.authToken
        }
        $http.get(ServerService.packagePath + 'holiday/filter', { headers: headersOptions })
            .then(function successCallback(response) {
                $scope.artloading = false;

                $scope.filterDetails = (response.data.data);
                $scope.priceStart = $scope.filterDetails.MINPRICE;
                $scope.yearStart = $scope.filterDetails.year.min;
                var startmonthrange = $scope.filterDetails.month.min;
                var startnum = moment(startmonthrange, 'MMM').format('MM');
                var monthStart = Number(startnum);
                if (monthStart < 10) {
                    monthStart = "0" + monthStart;
                }

                var endmonthrange = $scope.filterDetails.month.max;
                var endnum = moment(endmonthrange, 'MMM').format('MM');
                monthEnd = Number(endnum);
                if (monthEnd < 10) {
                    monthEnd = "0" + monthEnd;
                }
                if ($scope.priceStart == null) {
                    $scope.priceEnd = 0;
                } else {
                    $scope.priceEnd = $scope.filterDetails.MAXPRICE;
                    $scope.yearEnd = $scope.filterDetails.year.max;

                    $scope.countryDetails = $scope.filterDetails.COUNTRY;
                    $scope.categoryList = $scope.filterDetails.CATEGORY;
                    $scope.monthAllData = $scope.filterDetails.MONTH;

                    $scope.yearAllData = $scope.filterDetails.YEAR;

                    for(var d=0; d< $scope.categoryList.length; d++){
                        var categorylistName = $scope.categoryList[d].name.toLowerCase();
                        if(categorylistName == 'any'){
                            $scope.categoryItemList[0].categoryID = $scope.categoryList[d].id;
                            $scope.categoryItemList[0].categoryName = $scope.categoryList[d].name.toLowerCase();
                        }
                    }
                    
                    sessionStorage.setItem('holidayMinPrice', ($scope.priceStart));
                    sessionStorage.setItem('holidayMaxPrice', ($scope.priceEnd));

                    $scope.items = [$scope.categoryList];
                    $scope.selected = $scope.categoryList[0];

                    $scope.select = function (item) {
                        $scope.selected = item;
                    };
                    $(".js-range-slider").ionRangeSlider({
                        type: "double",
                        min: $scope.priceStart,
                        max: $scope.priceEnd,
                        from: $scope.priceStart,
                        to: $scope.priceEnd,
                        grid: true,
                        prefix: "",
                        onFinish: function (data) {
                            var fromRange = data.from_pretty.replace(" ", "");
                            var toRange = data.to_pretty.replace(" ", "");
                            $scope.priceRanges = [];
                            $scope.priceRanges = [{
                                minRange: fromRange,
                                maxRange: toRange
                            }];
                            $scope.filterDetails.MINPRICE = Number(fromRange);
                            $scope.filterDetails.MAXPRICE = Number(toRange);
                            count1 = 0;
                            for (var i = 0; i < $scope.filterOBJ1.length; i++) {
                                if ($scope.filterOBJ1[i].field == 'expiryDate' || $scope.filterOBJ1[i].field == 'startDate') {
                                    count1 = 1;
                                } else if ($scope.filterOBJ1[i].field == 'newPrice' && ($scope.filterOBJ1[i].operator == 'gte' || $scope.filterOBJ1[i].operator == 'lte')) {
                                    $scope.filterOBJ1.splice(i, 1);
                                    i--;
                                }
                            }
                            if (count1 == 0) {

                                $scope.filterOBJ1.push({
                                    "field": "expiryDate",
                                    "operator": "gte",
                                    "value": $scope.dateformat,
                                },
                                    {
                                        "field": "startDate",
                                        "operator": "lte",
                                        "value": $scope.dateformat,
                                    })

                            }
                            $scope.filterOBJ1.push(

                                {
                                    "field": "newPrice",
                                    "operator": "gte",
                                    "value": fromRange
                                }, {
                                "field": "newPrice",
                                "operator": "lte",
                                "value": toRange
                            },

                            );



                            $scope.filterOBJ = {
                                "logic": "and",
                                "filters": $scope.filterOBJ1
                            }


                            $scope.getPackagefilterData();

                        }
                    });
                    // yearfilter.....
                    $(".Year-range-slider").ionRangeSlider({
                        type: "double",
                        min: $scope.yearStart,
                        max: $scope.yearEnd,
                        from: $scope.yearStart,
                        to: $scope.yearEnd,
                        grid: true,
                        onFinish: function (data) {
                            var fromRange = data.from_pretty.replace(" ", "");
                            var toRange = data.to_pretty.replace(" ", "");
                            $scope.filterDetails.year.min = Number(fromRange);
                            $scope.filterDetails.year.max = Number(toRange);

                            if ($scope.filterOBJ1.length != 0) {
                                for (var i = 0; i < $scope.filterOBJ1.length; i++) {
                                    if ($scope.filterOBJ1[i].field == 'startDateYear' && $scope.filterOBJ1[i].field == 'expiryDateYear') {
                                        $scope.filterOBJ1.splice(i, 1);
                                        $scope.filterOBJ1.push({
                                            "field": "startDateYear",
                                            "operator": "gte",
                                            "value": fromRange
                                        },
                                            {
                                                "field": "expiryDateYear",
                                                "operator": "lte",
                                                "value": toRange
                                            });
                                        break;
                                    }
                                }
                            } else {
                                $scope.filterOBJ1 = [
                                    {
                                        "field": "startDateYear",
                                        "operator": "gte",
                                        "value": fromRange
                                    },
                                    {
                                        "field": "expiryDateYear",
                                        "operator": "lte",
                                        "value": toRange
                                    }
                                ]
                            }
                            $scope.filterOBJ = {
                                "logic": "and",
                                "filters": $scope.filterOBJ1
                            },
                                $scope.getPackagefilterData();

                        }
                    });
                    // monthfilter...
                    $(".Month-range-slider").ionRangeSlider({
                        type: "double",
                        values: [
                            "0", "Jan", "Feb", "Mar", "Apr", "May", "Jun",
                            "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
                        ],
                        from: monthStart,
                        to: monthEnd,
                        grid: true,
                        onFinish: function (data) {
                            var fromRange = data.from_pretty.replace(" ", "");
                            var toRange = data.to_pretty.replace(" ", "");
                            $scope.filterDetails.month.min = Number(fromRange);
                            $scope.filterDetails.month.max = Number(toRange);
                            // $scope.longthMonth = moment(range, 'MMM').format('MM');
                            countmonth = 0;
                            if ($scope.filterOBJ1.length != 0) {
                                for (var i = 0; i - 1 < $scope.filterOBJ1.length; i++) {
                                    if (i == 0) {
                                        if ($scope.filterOBJ1[i].field == "expityDateMonth" || $scope.filterOBJ1[i].field == 'startDateMonth') {
                                            $scope.filterOBJ1.splice(i, 1);
                                            countmonth++;


                                        }
                                    } else {
                                        if ($scope.filterOBJ1[i - 1].field == "expityDateMonth" || $scope.filterOBJ1[i - 1].field == 'startDateMonth') {
                                            $scope.filterOBJ1.splice((i - 1), 1);
                                            i--
                                            countmonth++;
                                        }
                                    }
                                }
                            } 


                            $scope.filterOBJ1.push(
                                {
                                    "field": "startDateMonth",
                                    "operator": "gte",
                                    "value": fromRange
                                },
                                {
                                    "field": "expityDateMonth",
                                    "operator": "lte",
                                    "value": toRange
                                },
                                {
                                    "field": "status",
                                    "operator": "eq",
                                    "value": "Y"
                                },
                                {
                                    "field": "expiryDate",
                                    "operator": "gte",
                                    "value": $scope.dateformat
                                  },
                                  {
                                    "field": "startDate",
                                    "operator": "lte",
                                    "value": $scope.dateformat
                                  }
                            )
                            $scope.filterOBJ = {
                                "logic": "and",
                                "filters": $scope.filterOBJ1
                            },
                                $scope.getPackagefilterData();


                        }
                    });
                }



            }, function errorCallback(response) {
                if (response.status == 403) {
                    var status = refreshService.refreshToken(tokenname.refreshToken, tokenname.authToken);
                    status.then(function (greeting) {
                        getFilter();
                    });
                }
            });
    }

    function getHolidaysHdr() {
        $scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));
        var tokenname = $scope.tokenresult.data;
        var headersOptions = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + tokenname.authToken
        }
        $http.post(ServerService.listPath + 'search/holiday-header', ($scope.reqobj), { headers: headersOptions })
            .then(function successCallback(response) {
                $scope.artloading = false;
                $scope.holidayDetails = (response.data.data.data);
                for (var i = 0; i < response.data.data.data.length; i++) {
                    var includesting = $scope.holidayDetails[i].includesId;
                    $scope.holidayIncludes = JSON.parse(includesting);
                    $scope.holidayDetails[i].includesId = $scope.holidayIncludes
                    $scope.holidayDetails[i].images = ServerService.listPath + 'file/download/' + (response.data.data.data[i].images);
                }
            }, function errorCallback(response) {
                if (response.status == 403) {
                    var status = refreshService.refreshToken(tokenname.refreshToken, tokenname.authToken);
                    status.then(function (greeting) {
                        getHolidaysHdr();
                    });
                }
            });
    }

    $scope.urlPath = $location.path();
    if ($scope.urlPath == '/holiday') {
        $timeout(function () {
            $scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));
            var tokenname = $scope.tokenresult.data;
            var headersOptions = {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + tokenname.authToken
            }
            if (tokenname != null) {
                $scope.reqobj = {
                    "page": null,
                    "pageSize": 10,
                    "take": 10,
                    "skip": 0,
                    "getAllresults": true,
                    "filter": {
                        "logic": "and",
                        "filters":
                            [
                                {
                                    "field": "expiryDate",
                                    "operator": "gte",
                                    "value": $scope.dateformat,
                                },
                                {
                                    "field": "startDate",
                                    "operator": "lte",
                                    "value": $scope.dateformat,
                                }
                            ]
                    },
                    "sort": [
                        {
                            "field": "displayOrder",
                            "dir": "asc"
                        }
                    ]
                }
                $scope.artloading = true;
                getHolidaysHdr();
                $scope.filterOBJ1 = [];
                $scope.filterOBJ2 = [];
                $scope.artloading = true;
                getFilter();
                $scope.priceStart = JSON.parse(sessionStorage.getItem('holidayMinPrice'));
                $scope.priceEnd = JSON.parse(sessionStorage.getItem('holidayMaxPrice'));




                $scope.yearFilter = function (range, type, isSelected) {
                    if ($scope.filterOBJ1.length != 0) {
                        for (var i = 0; i < $scope.filterOBJ1.length; i++) {
                            if ($scope.filterOBJ1[i].field == 'expiryDateYear') {
                                $scope.filterOBJ1.splice(i, 1);
                                $scope.filterOBJ1.push({
                                    "field": "expiryDateYear",
                                    "operator": "eq",
                                    "value": range
                                });
                                break;
                            }
                        }
                    } else {
                        $scope.filterOBJ1 = [
                            {
                                "field": "expiryDateYear",
                                "operator": "eq",
                                "value": range
                            },
                        ]
                    }
                    $scope.filterOBJ = {
                        "logic": "and",
                        "filters": $scope.filterOBJ1
                    },
                        $scope.getPackagefilterData();
                }
                $scope.monthFilter = function (range, type, isSelected) {
                    $scope.longthMonth = moment(range, 'MMM').format('MM');
                    countmonth = 0;
                    if ($scope.filterOBJ1.length != 0) {
                        for (var i = 0; i < $scope.filterOBJ1.length; i++) {
                            if (i == 0) {
                                if ($scope.filterOBJ1[i].field == "expityDateMonth" || $scope.filterOBJ1[i].field == 'startDateMonth') {
                                    $scope.filterOBJ1.splice(i, 1);
                                    countmonth++;


                                }
                            } else {
                                if ($scope.filterOBJ1[i - 1].field == "expityDateMonth" || $scope.filterOBJ1[i - 1].field == 'startDateMonth') {
                                    $scope.filterOBJ1.splice((i - 1), 1);
                                
                                    countmonth++;


                                }
                            }


                        }
                    } else {

                    }


                    $scope.filterOBJ1.push(
                        {
                            "field": "expityDateMonth",
                            "operator": "lte",
                            "value": $scope.longthMonth
                        },
                        {
                            "field": "startDateMonth",
                            "operator": "gte",
                            "value": $scope.longthMonth
                        }
                    )
                    $scope.filterOBJ = {
                        "logic": "and",
                        "filters": $scope.filterOBJ1
                    },
                        $scope.getPackagefilterData();
                }
                $scope.filterOBJ14=[];
                $scope.countryFilter = function (item, isSelected) {
                    console.log($scope.categoryItemList);
                    if (isSelected.target.checked) {
                        count = 0;
                        $scope.filterOBJ2.push({
                            "field": "countryId",
                            "operator": "eq",
                            "value": item.code
                        });
                        $scope.filterOBJ1 = [{
                            "logic": "or",
                            "filters": $scope.filterOBJ2
						},{
							"field": "expiryDate",
							"operator": "gte",
							"value": $scope.dateformat
						  },
						  {
							"field": "startDate",
							"operator": "lte",
							"value": $scope.dateformat
                          }];

                          if($scope.categoryItemList[0].categoryName != 'any'){
                            $scope.filterOBJ1.push({
                                "field": "categoryId",
                                "operator": "eq",
                                "value": $scope.categoryItemList[0].categoryID
                              })
                          }

                          if($scope.priceRanges.length != 0){
                              $scope.filterOBJ1.push({
                                "field": "newPrice",
                                "operator": "gte",
                                "value": $scope.priceRanges[0].minRange
                                }, 
                                {
                                "field": "newPrice",
                                "operator": "lte",
                                "value": $scope.priceRanges[0].maxRange
                             })
                          }

						$scope.filterOBJ = {
                            "logic": "and",
							"filters": $scope.filterOBJ1
						}
                            $scope.getPackagefilterData();
                    } else {
                        for (var i = 0; i < $scope.filterOBJ1[0].filters.length; i++) {
                            if ($scope.filterOBJ1[0].filters[i].value == item.code) {
                                $scope.filterOBJ1[0].filters.splice(i, 1);
                                if($scope.filterOBJ1[0].filters.length == 0){
                                    $scope.filterOBJ1 = [{
                                        "field": "expiryDate",
                                        "operator": "gte",
                                        "value": $scope.dateformat
                                      },
                                      {
                                        "field": "startDate",
                                        "operator": "lte",
                                        "value": $scope.dateformat
                                      }];

                                      if($scope.categoryItemList[0].categoryName != 'any'){
                                        $scope.filterOBJ1.push({
                                            "field": "categoryId",
                                            "operator": "eq",
                                            "value": $scope.categoryItemList[0].categoryID
                                          })
                                      }

                                      if($scope.priceRanges.length != 0){
                                        $scope.filterOBJ1.push({
                                          "field": "newPrice",
                                          "operator": "gte",
                                          "value": $scope.priceRanges[0].minRange
                                          }, 
                                          {
                                          "field": "newPrice",
                                          "operator": "lte",
                                          "value": $scope.priceRanges[0].maxRange
                                       })
                                    }

                                    $scope.filterOBJ = {
                                        "logic": "and",
                                        "filters": $scope.filterOBJ1
                                    }
                                    $scope.getPackagefilterData();
                                }
                                if($scope.filterOBJ1[0].filters.length > 0){
                                    $scope.getPackagefilterData();
                                }
                                
                            }
                
                }

                    }
                    
                }

                $scope.isOnly=function(item, isSelected,indexvalue,totalCuntrys){    
                         $scope.filterOBJ1 = [];
                         $scope.filterOBJ2=[];
                       
                        count = 0;
                        $scope.filterOBJ2.push({
                            "field": "countryId",
                            "operator": "eq",
                            "value": item.code
                        });
                        $('#checkboxlux_selected_' + indexvalue).prop('checked', true);
                    
                        $scope.filterOBJ1 = [{
                            "logic": "or",
                            "filters": $scope.filterOBJ2
                             },
                             {
                                 "field": "expiryDate",
                                 "operator": "gte",
                                 "value": $scope.dateformat
                             },
                             {
                                 "field": "startDate",
                                 "operator": "lte",
                                 "value": $scope.dateformat
                             }];

                             if($scope.categoryItemList[0].categoryName != 'any'){
                                $scope.filterOBJ1.push({
                                    "field": "categoryId",
                                    "operator": "eq",
                                    "value": $scope.categoryItemList[0].categoryID
                                  })
                              }

                              if($scope.priceRanges.length != 0){
                                $scope.filterOBJ1.push({
                                  "field": "newPrice",
                                  "operator": "gte",
                                  "value": $scope.priceRanges[0].minRange
                                  }, 
                                  {
                                  "field": "newPrice",
                                  "operator": "lte",
                                  "value": $scope.priceRanges[0].maxRange
                               })
                            }
                  
                    $scope.filterOBJ = {
                        "logic": "and",
                        "filters": $scope.filterOBJ1
                    },
                    $scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));
                    var tokenname = $scope.tokenresult.data;
                    var headersOptions = {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + tokenname.authToken
                    }
                    $scope.reqobj = {
                        "page": null,
                        "pageSize": 10,
                        "take": 10,
                        "skip": 0,
                        "getAllresults": true,
                        "filter": $scope.filterOBJ,
                        "sort": [
                            {
                                "field": "displayOrder",
                                "dir": "asc"
                            }
                        ]
                    }

                    $scope.artloading = true;
                    $http.post(ServerService.listPath + 'search/holiday-header', $scope.reqobj, {
                        headers: headersOptions
                    }).then(function successCallback(response) {
                        $scope.artloading = false;
                        $scope.holidayDetails = [];
                        response.data.data.data.forEach(element => {
                            $scope.holidayDetails.push(element);
                        });
                        for (var i = 0; i < response.data.data.data.length; i++) {
                            $scope.holidayDetails[i].images = ServerService.listPath + 'file/download/' + (response.data.data.data[i].images);

                        }
                         
        for(var s=0; s<totalCuntrys.length; s++){
            if(s==indexvalue){
             
                $('#checkbox_selected_' + s).prop('checked', true);
               
            }else{
                $('#checkbox_selected_' + s).prop('checked', false);
            }
                    }
                        $scope.safeApply();
                        if ($scope.holidayDetails.length == 0) {

                            Lobibox.alert('error', {
                                msg: "No Package Found for this Range"
                            });

                        }

                    }, function errorCallback(response) {
                        if (response.status == 403) {
                            var status = refreshService.refreshToken(tokenname.refreshToken, tokenname.authToken);
                            status.then(function (greeting) {
                                $scope.isOnly();
                            });
                        }
                    });
                }

                $("#catItem").addClass('active')
                $scope.categoryFilter = function (item) {
                    $scope.categoryItemList[0].categoryID = item.id;
                    $scope.categoryItemList[0].categoryName = item.name.toLowerCase();
                    $("#catItem").removeClass('active')
                    $scope.selected = item;
                    if ($scope.filterOBJ1.length != 0) {
                        for (var i = 0; i < $scope.filterOBJ1.length; i++) {
                            if ($scope.filterOBJ1[i].field == 'categoryId') {
                                $scope.filterOBJ1.splice(i, 1);
                            }
                        }
                        if (item.id != 1) {

                            $scope.filterOBJ1.push(

                                {
                                    "field": "categoryId",
                                    "operator": "eq",
                                    "value": item.id
                                }
                            )

                        }
                        count2 = 0;
                        for (var j = 0; j < $scope.filterOBJ1.length; j++) {
                            if ($scope.filterOBJ1[j].field == 'expiryDate' || $scope.filterOBJ1[j].field == 'startDate') {
                                count2 = 1;
                            }
                        }
                        if (count2 == 0) {
                            $scope.filterOBJ1.push(
                                {
                                    "field": "expiryDate",
                                    "operator": "gte",
                                    "value": $scope.dateformat,
                                },
                                {
                                    "field": "startDate",
                                    "operator": "lte",
                                    "value": $scope.dateformat,
                                }
                            )
                        }

                        if($scope.priceRanges.length != 0){
                            for (var i = 0; i < $scope.filterOBJ1.length; i++) {
                                if ($scope.filterOBJ1[i].field == 'newPrice' && $scope.filterOBJ1[i].operator == 'gte') {
                                    $scope.filterOBJ1[i].value = $scope.priceRanges[0].minRange;
                                } else if($scope.filterOBJ1[i].field == 'newPrice' && $scope.filterOBJ1[i].operator == 'lte'){
                                    $scope.filterOBJ1[i].value = $scope.priceRanges[0].maxRange;
                                }
                            }
                        }

                        
                    } else {
                        count2 = 0;
                        for (var j = 0; j < $scope.filterOBJ1.length; j++) {
                            if ($scope.filterOBJ1[j].field == 'expiryDate' || $scope.filterOBJ1[j].field == 'startDate') {
                                count2 = 1;
                            }
                        }
                        if (count2 == 0) {
                            $scope.filterOBJ1.push(
                                {
                                    "field": "expiryDate",
                                    "operator": "gte",
                                    "value": $scope.dateformat,
                                },
                                {
                                    "field": "startDate",
                                    "operator": "lte",
                                    "value": $scope.dateformat,
                                }
                            )
                        }
                        if (item.id != 1) {

                            $scope.filterOBJ1.push(

                                {
                                    "field": "categoryId",
                                    "operator": "eq",
                                    "value": item.id
                                },

                            )
                        }

                    }
                    $scope.filterOBJ = {
                        "logic": "and",
                        "filters": $scope.filterOBJ1
                    },
                        $scope.getPackagefilterData();
                }
                // .......filterfunction............
                $scope.getPackagefilterData = function () {

                    $scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));
                    var tokenname = $scope.tokenresult.data;
                    var headersOptions = {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + tokenname.authToken
                    }
                    $scope.reqobj = {
                        "page": null,
                        "pageSize": 10,
                        "take": 100,
                        "skip": 0,
                        "getAllresults": true,
                        "filter": $scope.filterOBJ,
                        "sort": [
                            {
                                "field": "displayOrder",
                                "dir": "asc"
                            }
                        ]
                    }

                    $scope.artloading = true;
                    $http.post(ServerService.listPath + 'search/holiday-header', $scope.reqobj, {
                        headers: headersOptions
                    }).then(function successCallback(response) {
                        $scope.artloading = false;
                        $scope.holidayDetails = [];
                        response.data.data.data.forEach(element => {
                            $scope.holidayDetails.push(element);
                        });
                        for (var i = 0; i < response.data.data.data.length; i++) {
                            $scope.holidayDetails[i].images = ServerService.listPath + 'file/download/' + (response.data.data.data[i].images);

                        }
                        $scope.safeApply();
                        if ($scope.holidayDetails.length == 0) {

                            Lobibox.alert('error', {
                                msg: "No Package Found for this Range"
                            });

                        }

                    }, function errorCallback(response) {
                        if (response.status == 403) {
                            var status = refreshService.refreshToken(tokenname.refreshToken, tokenname.authToken);
                            status.then(function (greeting) {
                                $scope.getPackagefilterData();
                            });
                        }
                    });
                }
                // ends....................


                $scope.artloading = false;

                function gethldyshdr() {
                    $scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));
                    var tokenname = $scope.tokenresult.data;
                    var headersOptions = {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + tokenname.authToken
                    }
                    $http.post(ServerService.listPath + 'search/holiday-header', ($scope.reqobj), { headers: headersOptions })
                        .then(function successCallback(response) {
                            $scope.artloading = false;
                            $scope.holidayDetails = (response.data.data.data);
                            for (var i = 0; i < response.data.data.data.length; i++) {
                                $scope.holidayDetails[i].images = ServerService.listPath + 'file/download/' + (response.data.data.data[i].images);
                            }
                            if ($scope.holidayDetails.length == 0) {

                                Lobibox.alert('error', {
                                    msg: "No Package Found for this Range"
                                });
                            }

                        }, function errorCallback(response) {
                            if (response.status == 403) {
                                var status = refreshService.refreshToken(tokenname.refreshToken, tokenname.authToken);
                                status.then(function (greeting) {
                                    gethldyshdr();
                                });
                            }
                        });
                }

                function gethldyshdr2(ui) {
                    $http.get(ServerService.packagePath + 'holiday/details/' + ui.item.id, { headers: headersOptions })
                        .then(function successCallback(response) {
                            $scope.tourDetails = response.data.data;
                            sessionStorage.setItem('packageDetails', JSON.stringify($scope.tourDetails))
                            $location.path('tourDetails/' + $scope.hhid1 + '/' + $scope.packagename1);
                        }, function errorCallback(response) {
                            if (response.status == 403) {
                                var status = refreshService.refreshToken(tokenname.refreshToken, tokenname.authToken);
                                status.then(function (greeting) {
                                    gethldyshdr2(ui);
                                });
                            }
                        });
                }

                function getcrudhldyhdr(ui) {
                    $scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));
                    var tokenname = $scope.tokenresult.data;
                    var headersOptions = {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + tokenname.authToken
                    }
                    $http.get(ServerService.listPath + 'crud/list/holiday-header', { headers: headersOptions })
                        .then(function successCallback(response) {
                            for (var j = 0; j < response.data.data.length; j++) {
                                if (response.data.data[j].hhId == ui.item.id) {
                                    $scope.packagelist = response.data.data[j];
                                    $scope.hhid1 = response.data.data[j].hhId;
                                    $scope.packagename1 = response.data.data[j].packageName;
                                    sessionStorage.setItem('packageDetailView', JSON.stringify($scope.packagelist));
                                    gethldyshdr2(ui);
                                }
                            }
                        }, function errorCallback(response) {
                            if (response.status == 403) {
                                var status = refreshService.refreshToken(tokenname.refreshToken, tokenname.authToken);
                                status.then(function (greeting) {
                                    getcrudhldyhdr(ui);
                                });
                            }
                        });
                }

                $('#keyword').autocomplete({
                    source: function (request, response) {
                        $scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));
                        var tokenname = $scope.tokenresult.data;
                        var headersOptions = {
                            'Content-Type': 'application/json',
                            'Authorization': 'Bearer ' + tokenname.authToken
                        }

                        $scope.artloading = true;
                        $http.get(ServerService.packagePath + 'holiday/search-header/' + request.term, { headers: headersOptions })
                            .then(function (data, status) {
                                var data = data.data.data;

                                $scope.artloading = false;
                                if (data != null && data != '') {
                                    f = 0;
                                    response(data);
                                } else {
                                    $("#keyword").val('');
                                    $("#keyword").focus(0);
                                    $('.ui-autocomplete').hide();
                                }
                            }, function errorCallback(response) {
                                if (response.status == 403) {
                                    var status = refreshService.refreshToken(tokenname.refreshToken, tokenname.authToken);
                                    status.then(function (greeting) {
                                        $("#keyword").val('');
                                    });
                                }
                            });

                    },
                    focus: function (event, ui) {
                        $("#keyword").val(ui.item.name);
                        return false;
                    },
                    change: function (event, ui) {
                        $("#keyword").val($scope.searchTxt);
                    },
                    minLength: 3,
                    select: function (event, ui) {
                        $("#keyword").val(ui.item.name);
                        $scope.searchTxt = ui.item.name;
                        $scope.$apply();
                        $('#keyword').css('font-size', '20px');
                        if (ui.item.filterType == 'COUNTRY') {

                            $scope.reqobj = {
                                "page": null,
                                "pageSize": 10,
                                "take": 10,
                                "skip": 0,
                                "getAllresults": true,
                                "filter": {
                                    "logic": "and",
                                    "filters": [{
                                        "field": "countryId",
                                        "operator": "eq",
                                        "value": ui.item.code
                                    }]
                                },
                                "sort": [
                                    {
                                        "field": "displayOrder",
                                        "dir": "asc"
                                    }
                                ]
                            }
                            $scope.artloading = true;
                            gethldyshdr();
                        } else {
                            getcrudhldyhdr(ui);
                        }
                        $("#keyword").parent().removeClass('has-error');
                        $scope.errorDisp = false;
                        flag = true;
                        return false;
                    },
                    open: function () {
                        $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
                    },
                    close: function (event, ui) {
                        $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
                    }
                }).data("ui-autocomplete")._renderItem = function (ul, item) {
                    if (f == 0) {
                        f++;
                        $scope.searchTxt = item.name;
                        return $("<li>").append("<a class='ui-state-focus'>" + item.name + "</a>").appendTo(ul);
                    } else {
                        return $("<li>").append("<a>" + item.name + "</a>").appendTo(ul);
                    }

                };
                getholidaydtls = function (item) {
                    $scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));
                    var tokenname = $scope.tokenresult.data;
                    var headersOptions = {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + tokenname.authToken
                    }
                    $http.get(ServerService.packagePath + 'holiday/details/' + item.hhId, { headers: headersOptions })
                        .then(function (response) {
                            $scope.artloading = false;
                            $scope.tourDetails = response.data.data;
                            sessionStorage.setItem('packageDetails', JSON.stringify($scope.tourDetails));
                            $location.path('tourDetails/' + $scope.hhid + '/' + $scope.packagename);
                        }, function errorCallback(response) {
                            if (response.status == 403) {
                                var status = refreshService.refreshToken(tokenname.refreshToken, tokenname.authToken);
                                status.then(function (greeting) {
                                    getholidaydtls(item);
                                });
                            }
                        });
                }

                $scope.onRedirect = function (item) {
                    $scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));
                    var tokenname = $scope.tokenresult.data;

                    var headersOptions = {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + tokenname.authToken
                    }
                    $scope.artloading = true;
                    $http.get(ServerService.listPath + 'crud/list/holiday-header', { headers: headersOptions })
                        .then(function (response) {
                            for (var j = 0; j < response.data.data.length; j++) {
                                if (response.data.data[j].hhId == item.hhId) {
                                    $scope.packagelist = response.data.data[j];
                                    $scope.hhid = response.data.data[j].hhId;
                                    $scope.packagename = response.data.data[j].packageName;
                                    sessionStorage.setItem('packageDetailView', JSON.stringify($scope.packagelist));
                                    getholidaydtls(item);
                                }
                            }

                        }, function errorCallback(response) {
                            if (response.status == 403) {
                                var status = refreshService.refreshToken(tokenname.refreshToken, tokenname.authToken);
                                status.then(function (greeting) {
                                    $scope.onRedirect(item);
                                });
                            }
                        });




                }
            }
        }, 1000);


    }
    $scope.safeApply = function (fn) {
        var phase = this.$root.$$phase;
        if (phase == '$apply' || phase == '$digest') {
            if (fn && (typeof (fn) === 'function')) {
                fn();
            }
        } else {
            this.$apply(fn);
        }
    };
});