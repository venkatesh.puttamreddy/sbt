ouaApp.controller('holidaySearchResultController', function($scope, $http, $uibModal, $q, $log, $document, $rootScope, $cookieStore, $cookies, $location, starService, ConstantService, ServerService, customSession) {
        $('html, body').scrollTop(0);
        $scope.hotelResponse = JSON.parse(sessionStorage.getItem('hotelPlusSearchResp'));
        $scope.amentiesQuantity = 4;
        $scope.options = "More";
        $scope.moreAmenties = function() {
            if ($scope.options == "More") {
                $scope.options = "Less";
                $scope.amentiesQuantity = $scope.hotelResponse.filterParams.amenityBeans.length;
            } else {
                $scope.options = "More";
                $scope.amentiesQuantity = 4;
            }

        }
        sessionStorage.removeItem('HotelroomIndex');
        sessionStorage.removeItem('hotelSearchResp');
        sessionStorage.removeItem('repriceResp');
        sessionStorage.removeItem('roombean');
        sessionStorage.removeItem('selectedHotelResp');
        //sessionStorage.removeItem('selectedFilght')
        $scope.hotelResponse = JSON.parse(sessionStorage.getItem('hotelPlusSearchResp'));
        $scope.flightResponse = JSON.parse(sessionStorage.getItem('flightplusSearchResp'));
        $scope.hotelSearchResp1 = $scope.hotelResponse.searchResults;
        $scope.filterParams = $scope.hotelResponse.filterParams;

        $scope.totalHotels = $scope.hotelSearchResp1;
        $rootScope.selectedflightResponse = $scope.flightResponse.flightSearchResults[0];
        var selectedFlt;
        var selectedFlt = JSON.parse(sessionStorage.getItem('fltIndex'));
        if (selectedFlt != null) {
            $rootScope.selectedflightResponse = $scope.flightResponse.flightSearchResults[selectedFlt];
            /* 	$scope.obj= JSON.parse(sessionStorage.getItem('selectedFliResp'));
            	$scope.obj.selectedResult.flightSearchResults[0] = $rootScope.selectedflightResponse; */
            // sessionStorage.setItem('newSelectedFliResp', JSON.stringify($scope.selectedflightResponse));
        }

        $scope.currency = ConstantService.currency
        $scope.night = parseInt($scope.hotelResponse.checkOutDate) - parseInt($scope.hotelResponse.checkInDate);

        $scope.disabled = false;
        $scope.loading = false;
        /* selectedHotel function */
        $scope.hotelResults = {}
        $scope.selectedHotel = function(val) {
            $scope.loading = true;
            var flightFlag = false;
            var hotelFlag = false;
            $('html, body').scrollTop(0);
            var hotels = $scope.hotelResponse.searchResults
            $rootScope.selectInd = false;
            $scope.flightInd = $rootScope.selectInd;
            $scope.flightPlus = {}
            $scope.flightPlus.flightSearchResults = [];
            if ($scope.flightInd == false) {
                $scope.flightsearchRes = JSON.parse(sessionStorage.getItem('flightplusSearchResp'))
                $http.get(ServerService.serverPath + 'rest/validate/session').then(function(resp) {

                    if (resp.data.isSessionValid == true) {
                        document.cookie = "flightIndex=" + 0;
                        $scope.flightPlus.frmAirprt = $scope.flightsearchRes.frmAirprt;
                        $scope.flightPlus.toAirprt = $scope.flightsearchRes.toAirprt;
                        $scope.flightPlus.adtCnt = $scope.flightsearchRes.adtCnt;
                        $scope.flightPlus.chldCnt = $scope.flightsearchRes.chldCnt;
                        $scope.flightPlus.infntCnt = $scope.flightsearchRes.infntCnt;
                        $scope.flightPlus.trvlType = $scope.flightsearchRes.trvlType;
                        $scope.flightPlus.srchType = $scope.flightsearchRes.srchType;
                        $scope.flightPlus.deptDate = $scope.flightsearchRes.deptDate;
                        $scope.flightPlus.arrDate = $scope.flightsearchRes.arrDate;
                        $scope.flightPlus.curncy = 'AED';
                        $scope.flightPlus.agncyConvrsnRate = $scope.flightsearchRes.agncyConvrsnRate;
                        $scope.flightPlus.clasType = $scope.flightsearchRes.clasType;
                        $scope.flightPlus.cnvrtdCrncyCode = $scope.flightsearchRes.cnvrtdCrncyCode;
                        $scope.flightPlus.cnvrsnRate = $scope.flightsearchRes.cnvrsnRate;
                        if (sessionStorage.getItem('selectedFilght')) {
                            $scope.flightPlus.flightSearchResults.push(JSON.parse(sessionStorage.getItem('selectedFilght')));
                        } else {
                            $scope.flightPlus.flightSearchResults.push($scope.flightsearchRes.flightSearchResults[0]);
                        }

                        $q.when(
                            $http.post(ServerService.serverPath + 'rest/flight/price', $scope.flightPlus, {
                                headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
                            }).then(function(response) {
                                if (response.data.status == 'SUCCESS') {
                                    $scope.selectedFliResp = response.data.data;
                                    sessionStorage.setItem('selectedFliResp', JSON.stringify($scope.selectedFliResp));
                                    flightFlag = true;
                                    if (hotelFlag == true) {
                                        $location.path('/holidayDetails');
                                    }
                                    // $location.path('/holidayDetails');
                                    // getHotelResp(val,hotels);
                                } else {
                                    Lobibox.alert('error', {
                                        msg: response.data.errorMessage
                                    });
                                }

                            }),
                            $http.get(ServerService.serverPath + 'rest/validate/session').then(function(response) {
                                if (response.data.isSessionValid == true) {
                                    $scope.disabled = true;
                                    $scope.loading = true;
                                    document.cookie = "hotelIndex=" + val;
                                    var hotelIndex = getCookie("hotelIndex");
                                    var paramIndex = hotelIndex;

                                    // $scope.hotelResults.hotelCode = hotels[val].htlCode;
                                    $scope.hotelResults.hotelCode = $scope.totalHotels[val].htlCode;
                                    $scope.hotelResults.hotelName = 'null';
                                    $scope.hotelResults.isHotelOnly = true;
                                    $http.post(ServerService.serverPath + 'rest/hotel/search/hoteldetails', $scope.hotelResults, {
                                        headers: {
                                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'

                                        }
                                    }).then(function(response) {
                                        // $scope.disabled = false;
                                        // $scope.loading = false;
                                        if (response.data.data != null || response.data.data != '' && response.data.status == 'SUCCESS') {
                                            $scope.selectedHotResp = response.data.data;
                                            sessionStorage.setItem('selectedHotResp', JSON.stringify($scope.selectedHotResp));
                                            hotelFlag = true;
                                            if (flightFlag == true) {
                                                $location.path('/holidayDetails');
                                            }
                                            // $location.path('/holidayDetails');
                                        } else {
                                            Lobibox.alert('error', {
                                                msg: response.data.errorMessage
                                            });
                                        }
                                    })
                                }
                            })

                        ).then(function() {
                            $scope.loading = false;
                        });

                    }

                })
            }


        }

        /* function getHotelResp(val,hotels){
        	
        } */
        /* end */
        /*filter module functions  */
        $scope.stars = $scope.filterParams.starRatingBeans;
        for (var i in $scope.filterParams.starRatingBeans) {
            if ($scope.filterParams.starRatingBeans[i].value == "Star Rating")
                $scope.filterParams.starRatingBeans[i].code = 1;
            if ($scope.filterParams.starRatingBeans[i].value == "Key Rating")
                $scope.filterParams.starRatingBeans[i].code = 2;
            if ($scope.filterParams.starRatingBeans[i].value == "Official AA Rating")
                $scope.filterParams.starRatingBeans[i].code = 3;
            if ($scope.filterParams.starRatingBeans[i].value == "Official Visit England Rating")
                $scope.filterParams.starRatingBeans[i].code = 4;
            if ($scope.filterParams.starRatingBeans[i].value == "Official Visit Wales Rating")
                $scope.filterParams.starRatingBeans[i].code = 5;
        }
        $scope.hotelResponse.filterParams.starRatingBeans.sort(function(obj1, obj2) {
                // Ascending: first age less than the previous
                return obj1.code - obj2.code;
            })
            /* starChange */
        $scope.starsObj = {}
        $scope.starsObj.starsList = []
        $scope.starLists = $scope.filterParams.starRatingBeans;
        $scope.luxaryList = $scope.filterParams.luxuryBeans;
        $scope.minPrice = $scope.filterParams.priceBean.minPrice;
        $scope.maxPrice = $scope.filterParams.priceBean.maxPrice;
        var starValue;
        var luxValue;
        /* $scope.starChange = function (value) {
        	$scope.starValue = $scope.filterParams.starRatingBeans[value].value;
        	starValue = $scope.starValue;
        	var filteredData = starService.getStar($scope.hotelResponse, starValue, luxValue);
        	$scope.hotelSearchResp1 = filteredData;
        }/* 
        /* end */

        /* starToggle function */
        $scope.hotelFilter = function(hotelName) {

            var UiValue = $scope.hotelName;
            $scope.filterParamObj.hotelName = $scope.hotelName;
            $scope.filterParam($scope.filterParamObj);


            // $scope.hotelSearchResp1=[];


            // for(var i=0;i<$scope.hotelResponse.searchResults.length;i++)
            // {

            // if($scope.hotelResponse.searchResults[i].name.toLowerCase().indexOf(UiValue.toLowerCase())!=-1){

            // $scope.hotelSearchResp1.push($scope.hotelResponse.searchResults[i]);


            // }
            // }

            //  $scope.totalHotels = $scope.hotelSearchResp1;  

        }
        $scope.UnSelectAll = function() {
            $scope.isAllStartsSelected = false;
            $scope.isAllLuxurySelected = false;
            $scope.isAllAmenitiesSelected = false;
            $scope.startsObjects.selectedStartList = [];
            $scope.luxuryObjects.selectedLuxuryList = [];
            $scope.amenityObjects.selectedAmenityList = [];

            $scope.maximum = $scope.filterParams.priceBean.maxPrice;
            $scope.minimum = $scope.filterParams.priceBean.minPrice;
            $scope.hotelSearchResp1 = $scope.hotelResponse.searchResults;

            $scope.totalHotels = $scope.hotelSearchResp1;
            return;
        }
        $scope.starsObj = {}
        $scope.starsObj.starsList = []
        $scope.starLists = $scope.filterParams.starRatingBeans;
        $scope.luxaryList = $scope.filterParams.luxuryBeans;
        var starValue;
        var luxValue;
        $scope.checkAll = false;
        $scope.starToggle = function() {
                if (!$scope.checkAll) {
                    $scope.checkAll = true;
                    $scope.starsObj.starsList = $scope.starLists.map(function(item) {
                        $scope.hotelSearchResp1 = $scope.hotelResponse.searchResults;
                        return item.value;
                    })
                } else {
                    $scope.checkAll = false;
                    $scope.starsObj.starsList = [];
                    $scope.hotelSearchResp1 = {};
                }
            }
            /* end */

        /* luxuryChange function */
        $scope.luxuryChange = function(index) {
                /* luxValue = $scope.filterParams.luxuryBeans[index].value;
	   var filteredData = starService.getStar($scope.hotelResponse, starValue, luxValue);
		 $scope.hotelSearchResp1 = filteredData; */
            }
            /* end */

        /* toggleLux function */
        $scope.luxObj = {}
        $scope.luxObj.luxObjList = [];
        $scope.toggleLux = function() {
                if (!$scope.checkAll) {
                    $scope.checkAll = true;
                    $scope.luxObj.luxObjList = $scope.luxaryList.map(function(item) {
                        $scope.hotelSearchResp1 = $scope.hotelResponse.searchResults;
                        return item.value;
                    })

                } else {
                    $scope.checkAll = false;
                    $scope.luxObj.luxObjList = [];
                    $scope.hotelSearchResp1 = {}
                }
            }
            //  }
            /* end */
            /*
            	default values for filters
            	*/
        $scope.isAllStartsSelected = false; //true;
        $scope.isAllLuxurySelected = false; //true;
        $scope.isAllAmenitiesSelected = false; //true;
        /*default values for filters end
         */
        $scope.hotelNames = [];

        $scope.getAllHotelNames = function() {
            $scope.hotelNames = $scope.hotelResponse.searchResults.map(function(hotelName) {
                return hotelName.name;
            })
        }


        $scope.filterParamObj = {
            defaultSortType: "PRICE",
            defaultSortOrder: "ASC",
            isOnlySorting: false,
            hotelName: null,
            starRatingBeans: null,
            priceBean: null,
            luxuryBeans: null,
            amenityBeans: null,
        }
        $scope.maximum = $scope.filterParams.priceBean.maxPrice;
        $scope.minimum = $scope.filterParams.priceBean.minPrice;
        $scope.starRatingBeans = [];
        $scope.luxuryBeans = [];
        $scope.startsObjects = {}
        $scope.startsObjects.selectedStartList = [];
        $scope.luxuryObjects = {}
        $scope.luxuryObjects.selectedLuxuryList = [];
        $scope.amenityObjects = {}
        $scope.amenityObjects.selectedAmenityList = [];
        $scope.starLists = $scope.filterParams.starRatingBeans;
        $scope.luxaryList = $scope.filterParams.luxuryBeans;
        $scope.amenityList = $scope.filterParams.amenityBeans;


        /*ng-init functions */
        // $scope.checkAllStars = function () {
        // 	$scope.startsObjects.selectedStartList = $scope.starLists.map(function (star) {
        // 		return star.value;
        // 	})
        // }
        // $scope.checkAllLuxury = function () {
        // 	$scope.luxuryObjects.selectedLuxuryList = $scope.luxaryList.map(function (luxury) {
        // 		return luxury.value;
        // 	})
        // }
        // $scope.checkAllAmenity = function () {
        // 	$scope.amenityObjects.selectedAmenityList = $scope.amenityList.map(function (amenity) {
        // 		return amenity.code;
        // 	})

        // }

        /*ng-init functions end*/

        /* starToggle function */
        $scope.checkAll = false;
        $scope.starToggle = function() {
                if (!$scope.isAllStartsSelected) {
                    $scope.checkAll = false;
                    $scope.startsObjects.selectedStartList = [];

                    $scope.hotelSearchResp1 = {};
                    $scope.startsObjects.selectedStartList = $scope.starLists.map(function(item) {
                        $scope.hotelSearchResp1 = $scope.hotelResponse.searchResults;
                        return item.value;
                    })
                    $scope.checkAll = false;
                    $scope.isAllStartsSelected = false;
                    $scope.startsObjects.selectedStartList = [];
                    $scope.totalHotels = $scope.hotelSearchResp1;
                } else {
                    $scope.checkAll = true;
                    $scope.isAllStartsSelected = true;
                    $scope.startsObjects.selectedStartList = $scope.starLists.map(function(item) {
                        $scope.hotelSearchResp1 = $scope.hotelResponse.searchResults;
                        return item.value;
                    })
                    $scope.totalHotels = $scope.hotelSearchResp1;


                }
            }
            /* end */
            /* toggleLux function */
        $scope.luxToggle = function() {
            if (!$scope.isAllLuxurySelected) {
                $scope.checkAll = false;
                $scope.luxuryObjects.selectedLuxuryList = [];
                $scope.hotelSearchResp1 = {};
                $scope.isAllLuxurySelected = false;
                $scope.totalHotels = $scope.hotelSearchResp1;
            } else {
                $scope.checkAll = true;
                $scope.isAllLuxurySelected = true;
                $scope.luxuryObjects.selectedLuxuryList = $scope.luxaryList.map(function(item) {
                    $scope.hotelSearchResp1 = $scope.hotelResponse.searchResults;
                    return item.value;
                })
                $scope.totalHotels = $scope.hotelSearchResp1;


            }
        }

        /* toggleLux function end */

        /*toggleAmenities Function  */

        $scope.amenitiesToggle = function() {
            if (!$scope.isAllAmenitiesSelected) {
                // $scope.checkAll = false;
                $scope.amenityObjects.selectedAmenityList = [];
                $scope.hotelSearchResp1 = {};
                $scope.amenityObjects.selectedAmenityList = $scope.starLists.map(function(amenity) {
                    $scope.hotelSearchResp1 = $scope.hotelResponse.searchResults;
                    return amenity.value;
                })
                $scope.checkAll = false;
                $scope.isAllAmenitiesSelected = false;
                $scope.totalHotels = $scope.hotelSearchResp1;
            } else {
                $scope.checkAll = true;
                $scope.isAllAmenitiesSelected = true;
                $scope.amenityObjects.selectedAmenityList = $scope.amenityList.map(function(amenity) {
                    $scope.hotelSearchResp1 = $scope.hotelResponse.searchResults;
                    return amenity.code;
                })
                $scope.totalHotels = $scope.hotelSearchResp1;


            }
        }


        /* start filter function */
        $scope.starChange = function() {
                var tempSelectedStars = $scope.startsObjects.selectedStartList.filter(function(el) {

                    return el != null && el != "";

                });
                if (tempSelectedStars.length == 0) {
                    $scope.hotelSearchResp1 = $scope.hotelResponse.searchResults;

                    $scope.totalHotels = $scope.hotelSearchResp1;
                    return;
                }
                if ($scope.startsObjects.selectedStartList.length == $scope.filterParams.starRatingBeans.length)
                    $scope.isAllStartsSelected = true;
                else
                    $scope.isAllStartsSelected = false;
                $scope.starRatingBeans = [];
                for (var s in $scope.startsObjects.selectedStartList) {
                    var targetStart = $scope.startsObjects.selectedStartList[s];
                    for (var i in $scope.filterParams.starRatingBeans) {
                        var orginalStart = $scope.filterParams.starRatingBeans[i].value;
                        if (orginalStart == targetStart) {
                            $scope.starRatingBeans.push({
                                code: null,
                                value: $scope.filterParams.starRatingBeans[i].value,
                                minPrice: $scope.filterParams.starRatingBeans[i].minPrice,
                                maxPrice: $scope.filterParams.starRatingBeans[i].maxPrice,
                                count: $scope.filterParams.starRatingBeans[i].count
                            })

                        }
                    }
                }

                if ($scope.starRatingBeans != null && $scope.starRatingBeans.length > 0) {
                    $scope.filterParamObj.starRatingBeans = $scope.starRatingBeans;
                    $scope.filterParam($scope.filterParamObj);
                } else {
                    $scope.hotelSearchResp1 = []
                    $scope.totalHotels = $scope.hotelSearchResp1;
                }
            }
            /* start filter function end */
            /* luxury filter function */
        $scope.luxuryChange = function() {
                $scope.luxuryBeans = [];
                if ($scope.luxuryObjects.selectedLuxuryList.length == $scope.filterParams.luxuryBeans.length)
                    $scope.isAllLuxurySelected = true;
                else
                    $scope.isAllLuxurySelected = false;
                for (var lux in $scope.luxuryObjects.selectedLuxuryList) {
                    var targetLuxValue = $scope.luxuryObjects.selectedLuxuryList[lux];
                    for (var i in $scope.filterParams.luxuryBeans) {
                        var orginalLux = $scope.filterParams.luxuryBeans[i].value;
                        if (orginalLux == targetLuxValue) {
                            $scope.luxuryBeans.push({
                                value: $scope.filterParams.luxuryBeans[i].value,
                                minPrice: $scope.filterParams.luxuryBeans[i].minPrice,
                                maxPrice: $scope.filterParams.luxuryBeans[i].maxPrice,
                                count: $scope.filterParams.luxuryBeans[i].count
                            })

                        }
                    }
                }
                if ($scope.luxuryBeans != null && $scope.luxuryBeans.length > 0) {
                    $scope.filterParamObj.luxuryBeans = $scope.luxuryBeans;
                    $scope.filterParam($scope.filterParamObj);

                } else {
                    $scope.hotelSearchResp1 = []
                    $scope.totalHotels = $scope.hotelSearchResp1;
                }
            }
            /*luxury filter function  end */

        /*amenity filter function  */
        $scope.amenityChange = function() {
            $scope.amenityBeans = [];
            if ($scope.amenityObjects.selectedAmenityList.length == $scope.filterParams.amenityBeans.length)
                $scope.isAllAmenitiesSelected = true;
            else
                $scope.isAllAmenitiesSelected = false;
            for (var lux in $scope.amenityObjects.selectedAmenityList) {
                var targetLuxValue = $scope.amenityObjects.selectedAmenityList[lux];
                for (var i in $scope.filterParams.amenityBeans) {
                    var orginalLux = $scope.filterParams.amenityBeans[i].code;
                    if (orginalLux == targetLuxValue) {
                        $scope.amenityBeans.push({
                            code: $scope.filterParams.amenityBeans[i].code,
                            value: $scope.filterParams.amenityBeans[i].value,
                            minPrice: $scope.filterParams.amenityBeans[i].minPrice,
                            maxPrice: $scope.filterParams.amenityBeans[i].maxPrice,
                            count: $scope.filterParams.amenityBeans[i].count
                        })

                    }
                }
            }
            if ($scope.amenityBeans != null && $scope.amenityBeans.length > 0) {
                $scope.filterParamObj.amenityBeans = $scope.amenityBeans;
                $scope.filterParam($scope.filterParamObj);

            } else {
                $scope.hotelSearchResp1 = []
                $scope.totalHotels = $scope.hotelSearchResp1;
            }
        }


        /*amenity filter function end */





        /*  Price Filter Function*/

        $scope.priceBean = { isEnabledPrice: true, maxPrice: $scope.filterParams.priceBean.maxPrice, minPrice: $scope.filterParams.priceBean.minPrice }
        $scope.filterParamObj.priceBean = $scope.priceBean;

        $("#price-range").slider({
            range: true,
            min: $scope.filterParams.priceBean.minPrice,
            max: $scope.filterParams.priceBean.maxPrice,
            step: 5,
            values: [$scope.filterParams.priceBean.minPrice, $scope.filterParams.priceBean.maxPrice],
            slide: function(e, ui) {
                var min = ui.values[0],
                    max = ui.values[1];
                $('.slider-price1').html(min);
                $('.slider-price2').html(max);
                $scope.filterParamObj.priceBean = { isEnabledPrice: true, minPrice: ui.values[0], maxPrice: ui.values[1] };
                if ($scope.filterParamObj.priceBean.length == 0) {
                    $scope.loadmore = false;
                    $scope.filterParamObj.priceBean = '';
                    $scope.noflight = true;
                    $rootScope.fResultsLen = 0;
                    $('.overlay').show().delay(1000).fadeOut();
                } else {
                    $scope.filterParam($scope.filterParamObj);

                }
            }
        });

        /* Price Filter Function End*/

        /*Hotel Name Filter Function*/
        $scope.hotelChange = function(searchHotelName) {
            $scope.hotelName = searchHotelName;
        }

        /**/

        /*
        Invoke Hotel Filter Service
        */

        $scope.filterParam = function(filtermodel) {

                $http.post(ServerService.serverPath + 'rest/hotel/filter/apply', filtermodel, {
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                    }
                }).then(function(response) {
                    $scope.hotelSearchResp1 = response.data.data;
                    $scope.totalHotels = $scope.hotelSearchResp1;

                })
            }
            /*
            	Invoke Hotel Filter Service end
            */

        $http.get("json/flightReview.json")
            .then(function(response) {
                $scope.flightList = response.data;
            });
        $http.get("json/hotelResultFilter.json")
            .then(function(response) {
                $scope.filterOptions = response.data;
            });
        $http.get("json/hotelResultList.json")
            .then(function(response) {
                $scope.hotelList = response.data;
            });
        $scope.date = {
            startDate: moment(),
            endDate: moment(),
            locale: {
                format: "D-MMM-YY"
            }
        };
        /*  $scope.range = function (n) {
		 n = Number(n);
		 return new Array(n);
	 };
 
	  */

        $scope.animationsEnabled = true;

        $scope.openChangeFlight = function(size, parentSelector) {
            var parentElem = parentSelector ?
                angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
            $uibModal.open({
                animation: $scope.animationsEnabled,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'changeFlight.html',
                controller: 'ModalInstanceCtrl',
                size: size,
                appendTo: parentElem
            });
        };


        $scope.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        };


        $scope.openFlightDetails = function(size, parentSelector) {
            var parentElem = parentSelector ?
                angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
            $uibModal.open({
                animation: $scope.animationsEnabled,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'flightDetails.html',
                controller: 'flightDetailsCtrl',
                size: size,
                appendTo: parentElem
            });
        };


        $scope.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        };

        /* open FareInfo */
        $scope.openFareInfo = function(size, parentSelector, index) {
                var parentElem = parentSelector ?
                    angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
                //	$rootScope.selectedFareIndex = index;
                $scope.animationEnabled = true;
                var modalInstance = $uibModal.open({
                    animation: $scope.animationEnabled,
                    ariaLabelledBy: 'modal-title',
                    ariaDescribedBy: 'modal-body',
                    templateUrl: 'flightFareinfoContent.html',
                    controller: 'holidayFareinfoController',
                    backdrop: true,
                    size: size,
                    appendTo: parentElem,
                    resolve: {
                        items: function() {
                            //	return $scope.items
                        }
                    }
                })
            }
            /* end */

    }).controller('ModalInstanceCtrl', function($scope, $uibModalInstance, $rootScope) {
        $scope.flightsearchRes = JSON.parse(sessionStorage.getItem('flightplusSearchResp'))
            /* selectedFlight function */
        $scope.flightPlus = {}
        $scope.flightPlus.flightSearchResults = [];
        $scope.disabled = false;
        $scope.loading = false;
        $rootScope.selectInd = false;
        $scope.selectedFlight = function(index) {
            $rootScope.selectInd = true;
            $rootScope.selectedflightResponse = $scope.flightsearchRes.flightSearchResults[index];
            sessionStorage.setItem('fltIndex', JSON.stringify(index));
            sessionStorage.setItem('selectedFilght', JSON.stringify($scope.selectedflightResponse));
            $uibModalInstance.close();
            /*
             $http.get(ServerService.serverPath+'rest/validate/session').then(function(resp){
            	
            	if(resp.data.isSessionValid == true){
            		//console.log("RESP "+resp);
            		document.cookie="flightIndex="+index;
            $scope.flightPlus.frmAirprt = $scope.flightsearchRes.frmAirprt;
            $scope.flightPlus.toAirprt = $scope.flightsearchRes.toAirprt;
            $scope.flightPlus.adtCnt = $scope.flightsearchRes.adtCnt;
            $scope.flightPlus.chldCnt = $scope.flightsearchRes.chldCnt;
            $scope.flightPlus.infntCnt = $scope.flightsearchRes.infntCnt;
            $scope.flightPlus.trvlType = $scope.flightsearchRes.trvlType;
            $scope.flightPlus.srchType = $scope.flightsearchRes.srchType;
            $scope.flightPlus.deptDate = $scope.flightsearchRes.deptDate;
            $scope.flightPlus.arrDate = $scope.flightsearchRes.arrDate;
            $scope.flightPlus.curncy = 'AED';
            $scope.flightPlus.agncyConvrsnRate = $scope.flightsearchRes.agncyConvrsnRate;
            $scope.flightPlus.clasType = $scope.flightsearchRes.clasType;
            $scope.flightPlus.cnvrtdCrncyCode = $scope.flightsearchRes.cnvrtdCrncyCode;
            $scope.flightPlus.cnvrsnRate = $scope.flightsearchRes.cnvrsnRate;
            $scope.flightPlus.flightSearchResults.push($scope.flightsearchRes.flightSearchResults[index]);
            $http.post(ServerService.serverPath+'rest/flight/price',$scope.flightPlus,{
            	headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
            }).then(function(response){
            	
            	$scope.disabled = false;
            $scope.loading = false;
            	$uibModalInstance.close();
            	if(response.data.status == 'SUCCESS'){
            		$scope.selectedFliResp = response.data.data;
            	sessionStorage.setItem('selectedFliResp',JSON.stringify($scope.selectedFliResp));
            	} else {
            		Lobibox.alert('error',{
            			msg: response.data.errorMessage
            			});
            		}
            	
            	})
		
            		}
            	
            	}) */
            /* end */

        }

        /* if($rootScope.selectInd == true){
        	var index = sessionStorage.getItem('fltIndex');
        	$http.get(ServerService.serverPath+'rest/validate/session').then(function(resp){
        		
        		if(resp.data.isSessionValid == true){
        			//console.log("RESP "+resp);
        			document.cookie="flightIndex="+index;
        	$scope.flightPlus.frmAirprt = $scope.flightsearchRes.frmAirprt;
        	$scope.flightPlus.toAirprt = $scope.flightsearchRes.toAirprt;
        	$scope.flightPlus.adtCnt = $scope.flightsearchRes.adtCnt;
        	$scope.flightPlus.chldCnt = $scope.flightsearchRes.chldCnt;
        	$scope.flightPlus.infntCnt = $scope.flightsearchRes.infntCnt;
        	$scope.flightPlus.trvlType = $scope.flightsearchRes.trvlType;
        	$scope.flightPlus.srchType = $scope.flightsearchRes.srchType;
        	$scope.flightPlus.deptDate = $scope.flightsearchRes.deptDate;
        	$scope.flightPlus.arrDate = $scope.flightsearchRes.arrDate;
        	$scope.flightPlus.curncy = 'AED';
        	$scope.flightPlus.agncyConvrsnRate = $scope.flightsearchRes.agncyConvrsnRate;
        	$scope.flightPlus.clasType = $scope.flightsearchRes.clasType;
        	$scope.flightPlus.cnvrtdCrncyCode = $scope.flightsearchRes.cnvrtdCrncyCode;
        	$scope.flightPlus.cnvrsnRate = $scope.flightsearchRes.cnvrsnRate;
        	$scope.flightPlus.flightSearchResults.push($scope.flightsearchRes.flightSearchResults[index]);
        	$http.post(ServerService.serverPath+'rest/flight/price',$scope.flightPlus,{
        		headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
        	}).then(function(response){
        		
        		$scope.disabled = false;
        	$scope.loading = false;
        		$uibModalInstance.close();
        		if(response.data.status == 'SUCCESS'){
        			$scope.selectedFliResp = response.data.data;
        		sessionStorage.setItem('selectedFliResp',JSON.stringify($scope.selectedFliResp));
        		} else {
        			Lobibox.alert('error',{
        				msg: response.data.errorMessage
        				});
        			}
        		
        		})
        	
        			}
        		
        		})
        	
        }  */

        /* end */

        $scope.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        };

    })
    /* .controller('flightDetailsCtrl', function ($http, $scope, $uibModalInstance) {
         $scope.flightsearchResInfo = JSON.parse(sessionStorage.getItem('flightplusSearchResp'))
    	  $scope.flightsearchResdetails = $scope.flightsearchResInfo.flightSearchResults[0];
    	 
    	 var selectedFlt;
    	var selectedFlt = JSON.parse(sessionStorage.getItem('fltIndex'));
    	if(selectedFlt !=null){
    	$scope.flightsearchResdetails=$scope.flightsearchResInfo.flightSearchResults[selectedFlt];
    	}

        $scope.ok = function () {
            $uibModalInstance.close($scope.selected.item);
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }); */
    .controller('flightDetailsCtrl', function($scope, $uibModalInstance, $rootScope, stopsService) {

        $scope.flightDetails = JSON.parse(sessionStorage.getItem('flightplusSearchResp'))
        $scope.cabinClass = $scope.flightDetails.clasType;
        $scope.flightDetails1 = $rootScope.selectedflightResponse;
        if ($scope.flightDetails1 == undefined) {
            $scope.flightDetails1 = JSON.parse(sessionStorage.getItem('selectedFliResp')).selectedResult.flightSearchResults[0];
        }
        // sessionStorage.setItem('flightplusSearchResp')
        var value = $rootScope.selectedIndex;
        $scope.flightDetails2 = $scope.flightDetails.data
            //$scope.flightDetails1 = $scope.$parent.flightResultList1
        $scope.filter = $rootScope.sCheckId
        $scope.airlineCode = $rootScope.airlineCodes;
        var flag = false;
        var filterDatas = {};
        if ($scope.filter != null && $scope.airlineCode != null) {

            filterDatas = stopsService.getStop($scope.flightDetails2, $scope.filter, $scope.airlineCode)
            $scope.flightDetails1 = filterDatas[value];
        } else if (!flag) {
            if ($scope.filter != null || $scope.filter != undefined) {
                filterDatas = stopsService.getStop($scope.flightDetails2, $scope.filter, $scope.airlineCode)
                $scope.flightDetails1 = filterDatas[value];
            } else if ($scope.airlineCode != null || $scope.airlineCode != undefined) {
                filterDatas = stopsService.getStop($scope.flightDetails2, $scope.filter, $scope.airlineCode)
                $scope.flightDetails1 = filterDatas[value];
            }
        }
        /* if($scope.filter != null || $scope.filter != undefined){
        	var filterDatas = stopsService.getStop($scope.flightDetails2, $scope.filter, $scope.airlineCode)
        	$scope.flightDetails1 = filterDatas[value];
        } */

        $scope.waitingDuration = [];
        var count = 0;
        $scope.getWaitingDuration = function(a, b, ind, x, key) {
            //$scope.waitingDuration;	      
            if (a != undefined && b !== undefined) {
                var startDate = new Date(a);
                var endDate = new Date(b);
                var d = (endDate.getTime() - startDate.getTime()) / 1000;
                d = Number(d);
                var h = Math.floor(d / 3600);
                var m = Math.floor(d % 3600 / 60);
                var hDisplay = h + "h";
                var mDisplay = m + "m";
                $scope.waitingDuration[parseInt(ind + "" + x + "" + key)] = hDisplay + " " + mDisplay;

            }

        }

        $scope.totalDuration = [];
        $scope.getTotalDuration = function(a, b, i) {
            //console.log(i);
            $scope.totalDuration[i] = timeConvert(parseInt(a) + parseInt(b));
            //console.log($scope.totalDuration);
        }

        function timeConvert(n) {
            var num = n;
            var hours = (num / 60);
            var rhours = Math.floor(hours);
            var minutes = (hours - rhours) * 60;
            var rminutes = Math.round(minutes);
            return rhours + "h " + rminutes + " m";
        }

        $scope.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        };
    });

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1);
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
ouaApp.service('starService', function() {
    this.getStar = function(listing, starItem, luxItem) {
        var star = [];
        var hotelResp = listing;
        var hotelList = hotelResp.searchResults;
        for (var s = 0; s < hotelList.length; s++) {
            var temp = hotelList[s]
            var starValue = hotelResp.searchResults[s].starRating;
            if (starValue == starItem) {
                star.push(temp);
            }

        }
        //console.log(star);
        return star;
    }


})
ouaApp.controller('holidayFareinfoController', function($scope, ConstantService, $uibModalInstance, $rootScope) {
    //console.log(" holidayFareinfoController ");
    $scope.currency = ConstantService.currency;
    if (sessionStorage.getItem('selectedFilght')) {
        //var index = $rootScope.selectedFareIndex
        $scope.flightDetail = JSON.parse(sessionStorage.getItem('selectedFilght'));
        $scope.flightDetail1 = $scope.flightDetail;
        $scope.flightDetail2 = $scope.flightDetail1.fareslst;
    } else {
        $scope.flightDetail = JSON.parse(sessionStorage.getItem('flightplusSearchResp'));
        $scope.flightDetail1 = $scope.flightDetail.flightSearchResults[0];
        $scope.flightDetail2 = $scope.flightDetail1.fareslst
    }

    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };
})