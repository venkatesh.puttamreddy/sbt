ouaApp.controller('flightHotelCtrl', function($scope, ConstantService, $uibModal, $http, $location, $interval, $timeout, ServerService, NationService, ajaxService, $rootScope) {
    $scope.currency = ConstantService.currency;
    sessionStorage.removeItem("selectedHotels");
    sessionStorage.removeItem("selectedHotelRoomsIndex");
    sessionStorage.removeItem("repriceResp");
    NationService.nationGet();
    var cityMap = null
        //	$scope.passengerCount = 0
    $scope.cabinClassList = ["Economy", "Premium Economy", "Business", "First"];
    $scope.airlineList = [{ title: "All Airlines", code: "" }, { title: "Emirates", code: "EK" }, { title: "Qatar Airways", code: "QR" }, { title: "Flydubai", code: "FZ" }, { title: "Egyptair", code: "MS" }, { title: "Jet Airways", code: "9W" }, { title: "Saudi Arabian", code: "SV" }, { title: "Etihad Airways", code: "EY" }, { title: "Turkish Airlines", code: "TK" }, { title: "Aer Lingus", code: "EI" }, { title: "Aeroflot", code: "SU" }, { title: "Aerosvit Airlines", code: "VV" }, { title: "Afriqiyah Airways", code: "8U" }, { title: "Aigle Azur", code: "ZI" }, { title: "Air Algerie", code: "AH" }, { title: "Air Arabia", code: "G9" }, { title: "Air Astana", code: "KC" }, { title: "Air Baltic", code: "BT" }, { title: "Air Berlin", code: "AB" }, { title: "Air Canada", code: "AC" }, { title: "Air China", code: "CA" }, { title: "Air France", code: "AF" }, { title: "Air India", code: "AI" }, { title: "Air Macau", code: "NX" }, { title: "Air New Zealand", code: "NZ" }, { title: "Air Pacific", code: "FJ" }, { title: "Alitalia", code: "AZ" }, { title: "American Airlines", code: "AA" }, { title: "Austrian Airlines", code: "OS" }, { title: "Azerbaijan Airlines", code: "J2" }, { title: "BMI", code: "BD" }, { title: "Bangkok Airways", code: "PG" }, { title: "Biman Bangladesh Airlines", code: "BG" }, { title: "British Airways", code: "BA" }, { title: "Cathay Pacific", code: "CX" }, { title: "China Airlines", code: "CI" }, { title: "Condor", code: "DE" }, { title: "Continental Airlines", code: "CO" }, { title: "Czech Airlines", code: "OK" }, { title: "Darwin Airlines", code: "F7" }, { title: "Delta Air Lines", code: "DL" }, { title: "Eritrean Airlines", code: "B8" }, { title: "Ethiopian Airlines", code: "ET" }, { title: "Eva Air", code: "BR" }, { title: "Finnair", code: "AY" }, { title: "Flynas", code: "XY" }, { title: "Garuda Indonesia", code: "GA" }, { title: "Gulf Air", code: "GF" }, { title: "Hahn Air", code: "HR" }, { title: "Hainan Airlines", code: "HU" }, { title: "Heliair Monaco", code: "YO" }, { title: "Hong Kong Airlines", code: "HX" }, { title: "Icelandair", code: "FI" }, { title: "Jet Lite", code: "S2" }, { title: "Kenya Airways", code: "KQ" }, { title: "Kuban Airlines", code: "GW" }, { title: "Kuwait Airways", code: "KU" }, { title: "Liat", code: "LI" }, { title: "Lufthansa", code: "LH" }, { title: "Malaysia Airlines", code: "MH" }, { title: "Middle East Airlines", code: "ME" }, { title: "Mihin Lanka", code: "MJ" }, { title: "Montenegro Airlines", code: "YM" }, { title: "Olympic Airlines", code: "OA" }, { title: "Oman Air", code: "WY" }, { title: "Pakistan International", code: "PK" }, { title: "Philippine Airlines", code: "PR" }, { title: "Precision Air", code: "PW" }, { title: "Qantas", code: "QF" }, { title: "Rossiya Airlines", code: "FV" }, { title: "Royal Air Maroc", code: "AT" }, { title: "Royal Brunei", code: "BI" }, { title: "Royal Jordanian Airlines", code: "RJ" }, { title: "Royal Phnom Penh Airways", code: "RL" }, { title: "Rwandair Express", code: "WB" }, { title: "SAS", code: "SK" }, { title: "Shanghai Airlines", code: "FM" }, { title: "Siberian Airlines", code: "S7" }, { title: "SilkAir", code: "MI" }, { title: "Singapore Airlines", code: "SQ" }, { title: "South African Airways", code: "SA" }, { title: "SriLankan Airlines", code: "UL" }, { title: "Swiss", code: "LX" }, { title: "TAAG Angola Airlines", code: "DT" }, { title: "Tarom", code: "RO" }, { title: "Thai Airways International", code: "TG" }, { title: "Thomas Cook Airlines", code: "HQ" }, { title: "Tunis Air", code: "TU" }, { title: "US Airways", code: "US" }, { title: "Ukraine International Airlines", code: "PS" }, { title: "United Airlines", code: "UA" }, { title: "Ural Airlines", code: "U6" }, { title: "Vietnam Airlines", code: "VN" }, { title: "Virgin Atlantic", code: "VS" }];

    $(document).ready(function() {
        $("select").mouseenter(function() {
            $("select").css("color", "#ed1c24");
        });
        $("select").mouseleave(function() {
            $("select").css("color", "white");
        });
    });

    $scope.cabinClass = "Economy";
    $scope.airline = {};
    $scope.airline.title = "All Airlines";
    $scope.airline.code = "";
    $scope.airlinee = $scope.airline.code;
    $scope.prefAirlineList = [];

    $scope.changeCabinClass = function(value) {
        $scope.cabinClass = value;
    }

    $scope.changeAirline = function(value) {
        $scope.airline.title = value.title;
        $scope.airlinee = value.code;
    }
    var date = new Date();
    var currentDate = date.getDate() + 1;
    $scope.dateOption = {
        opens: "left",
        minDate: new Date(date.getFullYear(), date.getMonth(), currentDate),
    }
    $rootScope.classData = ['Economy', 'Business', 'First'];
    /*assgin flight search data  */
    if (localStorage.getItem("flightSearchObj")) {
        $scope.searchFlightObjJson = {};
        var tempSearchFlightObjJson = JSON.parse(localStorage.getItem("flightSearchObj"));
        $scope.searchFlightObjJson.adtCnt = tempSearchFlightObjJson.adtCnt;
        $scope.searchFlightObjJson.chldCnt = tempSearchFlightObjJson.chldCnt;
        $scope.searchFlightObjJson.infCnt = tempSearchFlightObjJson.infCnt;
        $scope.fInfantCount = tempSearchFlightObjJson.fInfantCount;
        $scope.searchFlightObjJson.classType = tempSearchFlightObjJson.classType;
        var fromAirport = tempSearchFlightObjJson.frmAirport;
        var toAirport = tempSearchFlightObjJson.toAirport;
        $scope.searchFlightObjJson.frmAirport = fromAirport;
        $scope.searchFlightObjJson.toAirport = toAirport;
        $('#from_places').val(fromAirport);
        $('#to_places').val(toAirport);

    } else {
        $scope.searchFlightObjJson = {};
        $scope.searchFlightObjJson.adtCnt = 1;
        $scope.searchFlightObjJson.chldCnt = 0;
        $scope.searchFlightObjJson.infCnt = 0;
        $scope.fInfantCount = [0, 1];
        $scope.searchFlightObjJson.classType = $scope.classData[0];
        $scope.searchFlightObjJson.frmAirport = 'MUSCAT, Oman - Seeb (MCT)';
        $("#from_places").val('MUSCAT, Oman - Seeb (MCT)');
    }
    if (localStorage.getItem("flightSearchDate")) {
        $scope.flightSearchDate = JSON.parse(localStorage.getItem("flightSearchDate"));
    }
    /* assgin flight search data  end*/

    /*assgin hotel search data */
    if (localStorage.getItem("fromHotel")) {
        $scope.fromHotel = localStorage.getItem("fromHotel");
    }
    if (localStorage.getItem("hoteldate")) {
        $scope.hoteldate = JSON.parse(localStorage.getItem("hoteldate"));
    } else {
        $scope.hoteldate = {
            startDate: null,
            endDate: null
        }

    }
    /*assgin hotel search data end*/


    /*if(localStorage.getItem('flightRequest')!=undefined) {
    	//$scope.searchFlightObjJson = JSON.parse(sessionStorage.getItem('flightRequest'));
    	$scope.searchFlightObjJson = JSON.parse(localStorage.getItem('flightRequest'));
    		
    	$('#from_places').val($scope.searchFlightObjJson.frmAirport);
    	$('#to_places').val($scope.searchFlightObjJson.toAirport);
    	var infArr = [];
    	for( var k = 0; k<=$scope.searchFlightObjJson.adtCnt; k++) {
    		infArr.push(k);
    	}
    	$scope.fInfantCount = infArr;
    	//console.log($scope.searchFlightObjJson);
    	} else {
    	
    }*/

    $scope.searchFlightObjJson.srchType = 2;
    $scope.searchFlightObjJson.deptTime = "Any Time";
    $scope.searchFlightObjJson.retTime = "Any Time";
    //$scope.searchFlightObjJson.classType = 'Economy';
    //$scope.searchFlightObjJson.prefNonStop = false;
    $scope.searchFlightObjJson.pageIndex = 1;
    $scope.searchFlightObjJson.resultPerPage = 1000;

    if (jQuery('#from_places').val() == '') {
        jQuery('#from_places').focus();
    }
    var fromset = false;
    var toset = false;
    var flag = false;
    var flag1 = false;
    var f;
    $("#from_places").autocomplete({
        source: function(request, response) {
            ajaxService.ajaxRequest('POST', ServerService.serverPath + 'rest/repo/airport', '{ "queryString" : "' + request.term + '"}', 'json')
                .then(function(data, status) {
                    if (data != null && data != '') {
                        f = 0;
                        response(data);
                    } else {
                        $("#from_places").val('');
                        $("#from_places").focus(0);
                        $('.ui-autocomplete').hide();
                    }
                });
        },
        focus: function(event, ui) {
            $("#from_places").val(ui.item.city + ", " + ui.item.country + " - " + ui.item.name + " (" + ui.item.code + ")");
            return false;
        },
        change: function(event, ui) {
            $("#from_places").val($scope.searchFlightObjJson.frmAirport);
            if ($("#to_places").val() == $("#from_places").val()) {
                $("#from_places").val("");
                return false;
            }
            $timeout(function() {
                jQuery('.form-group').removeClass('has-error');
                $scope.emptyError = false;
            }, 10);
            /*if(flag==false) {
            	$('#from_places').val('');
            	$('#from_places').focus(0);
            	return false;
            }*/
        },
        autoFocus: true,
        minLength: 3,
        select: function(event, ui) {
            if ($("#from_places").val() == $("#to_places").val()) {
                $("#from_places").val('');
                $('.destcheck').show().fadeOut(5000);
            } else {
                $("#from_places").val(ui.item.city + ", " + ui.item.country + " - " + ui.item.name + " (" + ui.item.code + ")");
                $scope.searchFlightObjJson.frmAirport = ui.item.city + ", " + ui.item.country + " - " + ui.item.name + " (" + ui.item.code + ")";
                $scope.$apply();
                $scope.errorDisp1 = false;
                $("#from_places").parent().removeClass('has-error');
                $('#to_places').focus(0);
                flag = true;
            }
            return false;
        },
        open: function() {
            $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
        },
        close: function(event, ui) {
            $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
        }
    }).data("ui-autocomplete")._renderItem = function(ul, item) {
        if (f == 0) {
            f++;
            $scope.searchFlightObjJson.frmAirport = item.city + ", " + item.country + " - " + item.name + " (" + item.code + ")";
            return $("<li>").append("<a class='ui-state-focus'>" + item.city + "," + item.country + "-" + item.name + "(" + item.code + ")</a>").appendTo(ul);
        } else {
            return $("<li>").append("<a>" + item.city + "," + item.country + "-" + item.name + "(" + item.code + ")</a>").appendTo(ul);
        }
    };
    $("#to_places").autocomplete({
        source: function(request, response) {
            ajaxService.ajaxRequest('POST', ServerService.serverPath + 'rest/repo/airport', '{ "queryString" : "' + request.term + '"}', 'json')
                .then(function(data, status) {
                    if (data != null && data != '') {
                        f = 0;
                        response(data);
                    } else {
                        $("#to_places").val('');
                        $("#to_places").focus(0);
                        $('.ui-autocomplete').hide();
                    }
                });
        },
        focus: function(event, ui) {
            $("#to_places").val(ui.item.city + ", " + ui.item.country + " - " + ui.item.name + " (" + ui.item.code + ")");
            return false;
        },
        change: function(event, ui) {
            $scope.$apply();
            $("#to_places").val($scope.searchFlightObjJson.toAirport);
            if ($("#to_places").val() == $("#from_places").val()) {
                $("#to_places").val("");
                $('.destcheck').show().fadeOut(10000);
                return false;
            }
            $timeout(function() {
                jQuery('.form-group').removeClass('has-error');
                $scope.emptyError = false;
            }, 10);
            /*if(flag1==false) {
            	$('#to_places').val('');
            	$('#to_places').focus(0);
            	return false;
            }*/
        },
        autoFocus: true,
        minLength: 3,
        select: function(event, ui) {
            if ($("#from_places").val() == $("#to_places").val()) {
                $("#to_places").val('');
                $('.destcheck').show().fadeOut(10000);
            } else {
                flag1 = true;
                $("#to_places").val(ui.item.city + ", " + ui.item.country + " - " + ui.item.name + " (" + ui.item.code + ")");
                $scope.searchFlightObjJson.toAirport = ui.item.city + ", " + ui.item.country + " - " + ui.item.name + " (" + ui.item.code + ")";
                $scope.$apply();
                $("#to_places").parent().removeClass('has-error');
                $scope.errorDisp = false;
                $('#daterange1').focus();
            }
            return false;
        },
        open: function() {
            $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
        },
        close: function() {
            $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
        }
    }).data("ui-autocomplete")._renderItem = function(ul, item) {
        if (f == 0) {
            f++;
            $scope.searchFlightObjJson.toAirport = item.city + ", " + item.country + " - " + item.name + " (" + item.code + ")";
            return $("<li>").append("<a class='ui-state-focus'>" + item.city + "," + item.country + "-" + item.name + "(" + item.code + ")</a>").appendTo(ul);
        } else {
            return $("<li>").append("<a>" + item.city + "," + item.country + "-" + item.name + "(" + item.code + ")</a>").appendTo(ul);
        }
    };

    var hotser = false;
    jQuery('#search1').keyup(function() {
        /* if (hotser == true) {
        	$(this).val('');
        	hotser = false;
        } */
        if (jQuery(this).val().length == 3) {
            ajaxService.ajaxRequest('POST', ServerService.serverPath + 'rest/hotel/cities', '{ "queryString" : "' + jQuery(this).val() + '"}', 'json')
                .then(function(data, status) {
                    var availableTags = [];
                    cityMap = new Map();
                    for (var i = 0; i < data.length; i++) {
                        if (data[i].city.toLowerCase() == jQuery('#search1').val().toLowerCase()) {
                            availableTags.push(data[i].city + ', ' + data[i].provinceName + ', ' + data[i].country);
                            cityMap.set(data[i].city + ', ' + data[i].provinceName + ', ' + data[i].country, data[i].cityId);
                        }
                    }
                    for (var j = 0; j < data.length; j++) {
                        if (data[j].city.toLowerCase() != jQuery('#search1').val().toLowerCase()) {
                            if (data[j].city.toLowerCase().search(jQuery('#search1').val().toLowerCase()) == 0) {
                                availableTags.push(data[j].city + ', ' + data[j].provinceName + ', ' + data[j].country);
                                cityMap.set(data[j].city + ', ' + data[j].provinceName + ', ' + data[j].country, data[j].cityId);
                            }

                        }
                    }
                    for (var k = 0; k < data.length; k++) {
                        if (data[k].city.toLowerCase() != jQuery('#search1').val().toLowerCase()) {
                            if (data[k].city.toLowerCase().search(jQuery('#search1').val().toLowerCase()) != 0) {
                                availableTags.push(data[k].city + ', ' + data[k].provinceName + ', ' + data[k].country);
                                cityMap.set(data[k].city + ', ' + data[k].provinceName + ', ' + data[k].country, data[k].cityId);
                            }

                        }
                    }
                    jQuery("#search1").autocomplete({


                        source: availableTags,
                        autoFocus: true,

                        select: function(event, ui) {
                            hotser = true;
                            reminder = ui.item.value;
                            $("#search1").parent().removeClass('has-error');
                            $scope.errorDisp2 = false;
                            jQuery('#datepicker').focus(0);
                        },

                        focus: function(event, ui) {
                            event.preventDefault();
                        }


                    });
                    var e = jQuery.Event("keydown");
                    e.keyCode = 50;
                    $("#search1").trigger(e);

                });
        }
        localStorage.setItem("cityMap", JSON.stringify(cityMap))
    });

    $scope.states = [];
    $scope.flightfromAutoComplet = function(value) {
        if (value != null) {
            if (value.length >= 2) {
                var place = { 'queryString': value }
                $http.post(ServerService.serverPath + 'rest/repo/airport', '{"queryString" : "' + value + '"}', {
                    headers: {
                        'Content-Type': 'application/json; charset=UTF-8'
                    }
                }).then(function(response) {

                    if (response.data != 'null' && response.data.length > 0) {
                        $scope.states = []
                        for (var i = 0; i < response.data.length; i++) {
                            var airfrom = {
                                Code: response.data[i].code,
                                Title: response.data[i].city + "," + response.data[i].country + "-" + response.data[i].name + "(" + response.data[i].code + ")",

                            }
                            $scope.states.push(airfrom);

                        }
                        $scope.states.sort();
                    }
                })
            }
        }
    };
    $scope.city = []
    $scope.cityAutoComplet = function(totalcitys) {
        //console.log("HotelInCity" + totalcitys);
        if (totalcitys.length == 2) {
            //	console.log("HotelInCity" + totalcitys);
            $http.post(ServerService.serverPath + 'rest/hotel/cities', '{"queryString" : "' + totalcitys + '"}', {
                headers: {
                    'Content-Type': 'application/json; charset=UTF-8'
                }
            }).then(function(response) {
                /* $scope.hotelstates = JSON.stringify(response.data); */
                for (var i = 0; i < response.data.length; i++) {
                    var hotelcity = {
                        Title: response.data[i].city + "," + response.data[i].country,
                        Code: response.data[i].code
                    }
                    $scope.city.push(hotelcity);
                }
            })
        }
    }



    var intitialDate = {
        startDate: moment(),
        endDate: moment(),
        locale: {
            format: "D/MM/YYYY"
        }
    };

    $scope.date = intitialDate;


    /* search function */
    $scope.flightRequest = {}
    $scope.hotelRequest = {}
    $scope.roombean = JSON.parse(sessionStorage.getItem('roombean'));
    /* if($scope.roombean != null){
    	$scope.passengerCount = $scope.roombean.length	
    } */
    /* end */

    /* holidaySearch function */
    $scope.loading = false;
    $scope.disabled = false;
    var adultCount = 0;
    var childCount = 0;
    $scope.hotelRequest = {}

    $scope.roomList = JSON.parse(localStorage.getItem('holidayRoomList'));

    function createRoomBean() {
        var roomBeanObject = {};
        roomBeanObject.index = 1;
        roomBeanObject.adultCount = "2";
        roomBeanObject.cots = 0;
        roomBeanObject.childCount = "0";
        roomBeanObject.roomDesc = 'Single';
        roomBeanObject.xtraBed = false;
        roomBeanObject.roomCount = 1;
        roomBeanObject.withChildrens = false;
        roomBeanObject.childrens = null;
        return roomBeanObject;
    }

    if ($scope.roomList != null) {
        for (i = 0; i < $scope.roomList.length; i++) {
            adultCount = adultCount + parseInt($scope.roomList[i].adultCount);
            childCount = childCount + parseInt($scope.roomList[i].childCount);
        }
    } else {
        $scope.roomList = [];
        var roomBean = createRoomBean();
        roomBean.roomCount = 1;
        $scope.roomList.push(roomBean);
        adultCount = 2;
        childCount = 0;

        $rootScope.totalPassengers = adultCount + childCount;
        localStorage.setItem('holidayRoomList', JSON.stringify($scope.roomList));
    }
    // $rootScope.passengerInfo = "Adults - " + adultCount + " + Child - " + childCount;
    if (childCount > 0) {
        $rootScope.passengerInfo = "Adults - " + adultCount + " + Child - " + childCount;
    } else {
        $rootScope.passengerInfo = "Adults - " + adultCount;
    }
    setInterval(function() {
        if ($("#daterange1").val() != "") {
            $("#daterange1").parent().removeClass('has-error');
            //$scope.errorDisp3 = false;
        } else if ($("#hoteldaterange").val() != "") {
            $("#hoteldaterange").parent().removeClass('has-error');
            $scope.errorDisp4 = false;
        }
    }, 100);


    if (localStorage.getItem('hotelPlusRequest1') != undefined || localStorage.getItem('hotelPlusRequest1') != null) {
        var hotelObj = JSON.parse(localStorage.getItem('hotelPlusRequest1'))
        $scope.cityIdies = hotelObj.cityId

    }

    $scope.holidaySearch = function(valid) {
        adultCount = 0;
        $scope.submitted = true;
        var fv = 0;
        if ($("#from_places").val() == "") {
            fv = 1;

            if ($scope.holidayForm.from_places.$invalid == false && $scope.submitted == true) {
                $("#from_places").parent().addClass('has-error');
                $scope.errorDisp1 = true;

            }
        }
        if ($("#to_places").val() == "") {
            fv = 1;
            if ($scope.holidayForm.to_places.$invalid == false && $scope.submitted == true) {
                $("#to_places").parent().addClass('has-error');
                $scope.errorDisp = true;

            }

        }

        if ($("#search1").val() == "") {
            fv = 1;
            if ($scope.holidayForm.search1.$invalid == false && $scope.submitted == true) {
                $("#search1").parent().addClass('has-error');
                $scope.errorDisp2 = true;

            }
        }

        if ($("#daterange3").val() == "") {
            fv = 1;
            if ($scope.holidayForm.daterange3.$invalid == false && $scope.submitted == true) {
                $("#daterange3").parent().addClass('has-error');
                //$scope.errorDisp3 = true;

            }
        }

        if ($("#hoteldaterange").val() == "") {
            fv = 1;
            if ($scope.holidayForm.hoteldaterange.$invalid == false && $scope.submitted == true) {
                $("#hoteldaterange").parent().addClass('has-error');
                $scope.errorDisp4 = true;

            }
        }

        if (fv == 0) {
            $scope.submitted = false;


        } else {
            $scope.submitted = true;
            return $scope;
        }

        if (valid) {
            localStorage.setItem("flightSearchObj", JSON.stringify($scope.searchFlightObjJson));
            localStorage.setItem("flightSearchDate", JSON.stringify($scope.flightSearchDate));
            localStorage.setItem("fromHotel", $scope.fromHotel);
            localStorage.setItem("hoteldate", JSON.stringify($scope.hoteldate));
            var fromPlace = $('#from_places').val();
            var toPlace = $('#to_places').val();
            var flightDate1 = $scope.flightSearchDate;
            var fromFlightDate = getFormattedDate(flightDate1.startDate)
            var toFlightDate = getFormattedDate(flightDate1.endDate);

            /* var flightDate = $('#daterange1').val();
            var fromFlightDate = flightDate.split('-')[0];
            var toFlightDate = flightDate.split('-')[1]; */

            var hotelDate = $('#hoteldaterange').val();
            var hotelCheckIn = hotelDate.split('-')[0];
            var hotelCheckOut = hotelDate.split('-')[1];

            var fromHotelPlace = $scope.fromHotel;
            if (cityMap != null && cityMap != 'undefined')
                var cityId = cityMap.get(fromHotelPlace);
            localStorage.setItem("cityId", cityId)
            var country = fromHotelPlace.split(',')[2];
            var city = fromHotelPlace.split(',')[0];

            var night = (parseInt(hotelCheckOut) - parseInt(hotelCheckIn))
                /* $scope.roombean = JSON.parse(sessionStorage.getItem('roombean'));
                $scope.infantCount = JSON.parse(sessionStorage.getItem('infant'));
                $scope.childCount = JSON.parse(sessionStorage.getItem('child'));
                $scope.adultCount = JSON.parse(sessionStorage.getItem('adult'));
			
                if($scope.infantCount == null || $scope.infantCount == undefined){
                	$scope.infantCount = 0;
                	} else {
                	$scope.infantCount = $scope.infantCount;
                } */
            $scope.roomList = JSON.parse(localStorage.getItem('holidayRoomList'));
            if ($scope.roomList != null) {
                for (i = 0; i < $scope.roomList.length; i++) {
                    var tempAdultCount = 1;
                    tempAdultCount = parseInt($scope.roomList[i].adultCount);
                    delete $scope.roomList[i]['$$hashKey'];
                    switch (tempAdultCount) {
                        case 1:
                            $scope.roomList[i].roomDesc = 'Single' + i;
                            break;
                        case 2:
                            $scope.roomList[i].roomDesc = 'Double' + i;
                            break;
                        case 3:
                            $scope.roomList[i].roomDesc = 'Triple' + i;
                            break;
                        case 4:
                            $scope.roomList[i].roomDesc = 'Quad' + i;
                            break;
                    }
                    tempAdultCount++;
                    adultCount = adultCount + parseInt($scope.roomList[i].adultCount);
                    childCount = childCount + parseInt($scope.roomList[i].childCount);
                }
            }

            $scope.flightRequest.frmAirport = fromPlace;
            $scope.flightRequest.toAirport = toPlace;
            $scope.flightRequest.adtCnt = adultCount;
            $scope.flightRequest.chldCnt = childCount;
            $scope.flightRequest.strDeptDate = fromFlightDate;
            $scope.flightRequest.strRetDate = toFlightDate;
            $scope.flightRequest.binNumber = '552107';

            /* $scope.flightRequest.classType = 'Economy'; */
            $scope.flightRequest.classType = $scope.cabinClass;
            if ($scope.airlinee != "") {
                $scope.prefAirlineList.push($scope.airlinee);
            }
            $scope.flightRequest.prefAirlineLst = $scope.prefAirlineList;
            $scope.flightRequest.deptTime = 'Any Time';
            $scope.flightRequest.retTime = 'Any Time';
            $scope.flightRequest.infCnt = $scope.infantCount;
            $scope.flightRequest.onlyFlight = false;
            $scope.flightRequest.pageIndex = 1;
            $scope.flightRequest.prefNonStop = false;
            $scope.flightRequest.resultPerPage = 1000;
            $scope.flightRequest.srchType = 2;
            $scope.hotelRequest.checkIn = hotelCheckIn;
            $scope.hotelRequest.checkOut = hotelCheckOut;
            $scope.hotelRequest.place = fromHotelPlace;
            if (cityId != null || cityId != undefined) {
                $scope.hotelRequest.cityId = cityId;
            } else {
                $scope.hotelRequest.cityId = $scope.cityIdies;
            }
            $scope.hotelRequest.country = country;
            $scope.hotelRequest.city = city;
            $scope.hotelRequest.currency = $scope.currency;
            $scope.hotelRequest.htlName = null;
            $scope.hotelRequest.onlyHotel = false;
            $scope.hotelRequest.searchMode = 'search';
            $scope.hotelRequest.starRating = 0;

            // $scope.hotelRequest.roomBean = $scope.roombean;
            // $scope.hotelRequest.traveller = $scope.roombean.length
            $scope.hotelRequest.roomBean = $scope.roomList;

            $scope.hotelRequest.binNumber = '552107';
            $scope.hotelRequest.locationCode = city;
            sessionStorage.removeItem('fltIndex');

            var infCnt = 0;
            // var childCnt = 0;
            for (var x = 0; x < $scope.hotelRequest.roomBean.length; x++) {
                $scope.firstObj = $scope.hotelRequest.roomBean[x];
                if ($scope.firstObj.childrens != null) {
                    for (var y = 0; y < $scope.firstObj.childrens.length; y++) {
                        $scope.secondObj = $scope.firstObj.childrens[y];
                        for (var z = 0; z < $scope.secondObj.childBeans.length; z++) {
                            $scope.thirdObj = $scope.secondObj.childBeans[z];
                            if ($scope.thirdObj.age < 2) {
                                infCnt = infCnt + 1;
                                // }else{
                                // childCnt=childCnt+1;
                            }
                        }
                    }
                }
            }
            $scope.flightRequest.chldCnt = $scope.flightRequest.chldCnt - infCnt;
            $scope.flightRequest.infCnt = infCnt;
            sessionStorage.setItem('hotelPlusRequest', JSON.stringify($scope.hotelRequest));
            localStorage.setItem('hotelPlusRequest1', JSON.stringify($scope.hotelRequest));
            sessionStorage.setItem('flightPlusRequest', JSON.stringify($scope.flightRequest));
            sessionStorage.removeItem("travellersListByRoom");
            $location.path('/searchingHoliday');
        }
    }

    var date = new Date();
    var currentDate = date.getDate() + 1;
    $scope.opts = {
        opens: "left",
        minDate: new Date(date.getFullYear(), date.getMonth(), currentDate),
    }

    $scope.hotelOpt = {
        opens: "left",
        minDate: new Date(date.getFullYear(), date.getMonth(), currentDate),
    }

    getFormattedDate = function(searchDate) {
            var date = new Date(searchDate);
            var month = date.getMonth() + 1;
            var day = date.getDate();
            var year = date.getFullYear();
            return day + "/" + month + "/" + year;
        }
        /* open modal function */

    $scope.animationEnabled = true;
    $scope.open = function(size, parentSelector) {
            var parentElem = parentSelector ?
                angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
            var modalInstance = $uibModal.open({
                animation: $scope.animationEnabled,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'modalContent.html',
                controller: 'modalContentCtrl',
                backdrop: true,
                size: size,
                appendTo: parentElem,
                resolve: {
                    items: function() {
                        return $scope.items
                    }
                }
            })
        }
        /* end */

}).controller('modalContentCtrl', function($scope, $rootScope, $uibModalInstance) {


    $scope.maxRooms = 5;

    $scope.roomBean = {
        index: 1,
        adultCount: 1,
        cots: 0,
        childCount: 1,
        roomDesc: "Single",
        xtraBed: false,
        roomCount: 1,
        withChildrens: true,
        childrens: null
    };

    $scope.childBean = {
        index: 0,
        childCount: 1,
        childBeans: []
    };

    $scope.childInfo = {
        name: null,
        age: 4
    };

    function createChildBean() {
        var childBean = {};
        childBean.index = 0;
        childBean.childCount = 1;
        childBean.childBeans = [];
        return childBean;

    }

    function createChildInfo() {
        var childInfo = {};
        childInfo.name = null;
        childInfo.age = "1";
        return childInfo;

    }

    if (localStorage.getItem('holidayRoomList') == undefined) {
        $scope.roomList = [];
        $scope.roomCount = 1;
        var roomBean = createRoomBean();
        if (localStorage.getItem("adultCount")) {
            roomBean.adultCount = localStorage.getItem("adultCount");

        }
        roomBean.roomCount = 1;
        $scope.roomList.push(roomBean);
    } else {
        $scope.roomList = JSON.parse(localStorage.getItem('holidayRoomList'));
        $scope.roomCount = $scope.roomList.length;
    }

    function createRoomBean() {
        var roomBeanObject = {};
        roomBeanObject.index = 1;
        roomBeanObject.adultCount = "2";
        roomBeanObject.cots = 0;
        roomBeanObject.childCount = "0";
        roomBeanObject.roomDesc = 'Single';
        roomBeanObject.xtraBed = false;
        roomBeanObject.roomCount = 1;
        roomBeanObject.withChildrens = false;
        roomBeanObject.childrens = null;
        return roomBeanObject;
    }

    $scope.addRoom = function() {
        var roomBean = createRoomBean();
        roomBean.roomCount = 1;
        $scope.roomList.push(roomBean);
    }

    $scope.removeRoom = function(index) {
        $scope.roomList.splice(index, 1);
        if ($scope.roomList.length > 0) {
            for (var i = 0; i < $scope.roomList.length; i++) {
                if ($scope.roomList[i].childrens != null) {
                    $scope.withChild = [];
                    $scope.withChild.push(0);
                    break;
                } else {
                    $scope.withChild = [];
                }
            }
        }
    }


    $scope.updateRoomList = function() {
        if ($scope.roomCount > 0) {
            if ($scope.roomList.length < $scope.roomCount) {
                for (i = $scope.roomList.length; i < $scope.roomCount; i++) {
                    var roomBean = createRoomBean();
                    roomBean.roomCount = 1;
                    $scope.roomList.push(roomBean);
                }
            } else {
                $scope.roomList.splice($scope.roomCount);
            }

        }

    }
    if ($scope.roomList.length > 0) {
        for (var i = 0; i < $scope.roomList.length; i++) {
            if ($scope.roomList[i].childrens != null) {
                $scope.withChild = [];
                $scope.withChild.push(0);
                break;
            } else {
                $scope.withChild = [];
            }
        }
    }
    $scope.updateChildList = function(index) {
        if ($scope.roomList[index].childrens == null) {
            $scope.roomList[index].childrens = [];
            $scope.roomList[index].childrens.push(createChildBean());
            $scope.roomList[index].childrens[0].childCount = $scope.roomList[index].childCount;
            $scope.withChild.push(0);
            $scope.roomList[index].withChildrens = true;
            for (i = 0; i < $scope.roomList[index].childCount; i++) {

                $scope.roomList[index].childrens[0].childBeans.push(createChildInfo());
            }
        } else {
            if ($scope.roomList[index].childCount == 0) {

                $scope.withChild.pop();
                $scope.roomList[index].withChildrens = false;
                $scope.roomList[index].childrens[0].childBeans = [];
                $scope.roomList[index].childrens = null;
            } else if ($scope.roomList[index].childrens[0].childBeans.length < $scope.roomList[index].childCount)
                $scope.roomList[index].childrens[0].childBeans.push(createChildInfo());
            else {
                $scope.roomList[index].childrens[0].childBeans.splice(1);
            }
        }

    }


    $scope.ok = function() {
        localStorage.setItem('holidayRoomList', JSON.stringify($scope.roomList));
        $scope.roomList = JSON.parse(localStorage.getItem('holidayRoomList'));
        var adultCount = 0;
        var childCount = 0;
        for (i = 0; i < $scope.roomList.length; i++) {
            adultCount = adultCount + parseInt($scope.roomList[i].adultCount);
            childCount = childCount + parseInt($scope.roomList[i].childCount);

        }
        $rootScope.totalPassengers = adultCount + childCount;
        // $rootScope.passengerInfo = "Adults - " + adultCount + " + Childs - " + childCount;
        if (childCount > 0) {
            $rootScope.passengerInfo = "Adults - " + adultCount + " + Child - " + childCount;
        } else {
            $rootScope.passengerInfo = "Adults - " + adultCount;
        }
        $uibModalInstance.close();

    }
    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    }

});