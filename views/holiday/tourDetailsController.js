ouaApp.controller('tourDetailsController', function($scope, $http,refreshService, $stateParams, ServerService, baseUrlService, $location,) {
    $('html, body').scrollTop(0);
    var packID = $stateParams.id;
    $scope.artloading = false;
    holdeparture.min = new Date().toISOString().split("T")[0];

   getHolidaysDetails = function(){
    $scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));
    var tokenname = $scope.tokenresult.data;
    var headersOptions = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + tokenname.authToken
    }

    $http.get(ServerService.listPath + 'crud/list/holiday-header', { headers: headersOptions })
    .then(function successCallback(response) {
        for (var j = 0; j < response.data.data.length; j++) {
            if (response.data.data[j].hhId == packID) {
                $scope.packageDetailView = response.data.data[j];
                $http.get(ServerService.packagePath + 'holiday/details/' + packID, { headers: headersOptions })
                .then(function successCallback(response) {
                   $scope.packageDetails = response.data.data;
                   $scope.isThumbnail = $scope.packageDetails[0].thumbnailImages.length;
                    if($scope.isThumbnail > 0){
                        $scope.uaefullimage = $scope.packageDetails[0].thumbnailImages[0];
                    }
                    if($scope.isThumbnail == 0){
                        $scope.uaefullimage = $scope.packageDetailView.images;
                    }
            
                });
            }
        }
    }, function errorCallback(response) {
        if(response.status == 403){
            var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
            status.then(function(greeting) {
                getHolidaysDetails();
            });                
        }
      });

   
   }

   getHolidaysHdr = function(){
    $scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));
    var tokenname = $scope.tokenresult.data;
    var headersOptions = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + tokenname.authToken
    }
    $scope.artloading = true;
    $http.get(ServerService.listPath + 'crud/list/holiday-header', { headers: headersOptions })
    .then(function successCallback(response) {
        $scope.artloading = false;
        for (var j = 0; j < response.data.data.length; j++) {
            if (response.data.data[j].hhId == packID) {
                $scope.packageDetailView = response.data.data[j];
            }
        }
    }, function errorCallback(response) {
        if(response.status == 403){
            var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
            status.then(function(greeting) {
               getHolidaysHdr();
            });                
        }
      });
   }

   $scope.onThambnailChange = function(imgfile){
    $scope.uaefullimage = imgfile;
}

    getHolidaysHdr();
   getHolidaysDetails();
  
    
    $scope.brouchuredownload = function() {
        $scope.brouchureId = $scope.packageDetails[0].brochureUploadId;
        if ($scope.brouchureId == null) {
            Lobibox.alert('error', {
                msg: "No brouchure Uploaded For this Package."
            })
        } else {
            window.open(baseUrlService.baseUrl+"/rest/api/v1/file/download/" + $scope.brouchureId);
        }
    }
    $scope.timeload = function(){
        $("#load").show();
        setTimeout(function () {
        //    test(); 
           $('#load').hide();
        }, 500);
    }
    $scope.goPackage = function() {
        $scope.timeload();
        // $scope.artloading = true;
    $location.path('holiday');
    // $scope.artloading = false;
    }


    $scope.imagepath = ServerService.listPath + "file/download/";

    $scope.user = {};
    $scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));
    var tokenname = $scope.tokenresult.data.authToken;
    $scope.captcha=sessionStorage.getItem("captcha");
    var headersOptions = {
        "g-recaptcha-response" : $scope.captcha,
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + tokenname
    }
    $scope.modelDismiss = function() {
        $uibModalInstance.dismiss('cancel');
    }
    $scope.user.totalDays = 0;
    $scope.daysCount = function() {
        var dat1 = document.getElementById('holdeparture').value;
        date1 = new Date(dat1)
        var dat2 = document.getElementById('holdreturn').value;
        var date2 = new Date(dat2)
        var oneDay = 24 * 60 * 60 * 1000;
        var diffDays = Math.abs((date1.getTime() - date2.getTime()) / (oneDay));
        $scope.user.totalDays = diffDays;
    }
    $scope.user.childNo = 0;
    $scope.user.infantNo = 0;
    $scope.user.adultNo = 1;
    $scope.holidaydep = function() {
        $scope.minmultiDate = $('#holdeparture').val();
        var depdat1 = document.getElementById('holdeparture').value;
        depdate1 = new Date(depdat1)

        today = new Date()
        currentYear = depdate1.getFullYear();
        currentMonth = depdate1.getMonth();
        currentDay = depdate1.getDate();
        $scope.minreturnDate = new Date(currentYear, currentMonth, currentDay + 2);

        holdreturn.min = new Date($scope.minreturnDate).toISOString().split("T")[0];
    }

    $scope.tourDetailEnquiryForm = function() {

   
        d = new Date($('#holdeparture').val()),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();
        var HoldDate = [day, month, year].join('-');
        $scope.user.depDate = HoldDate;
        $scope.adultminDate;

        r = new Date($('#holdreturn').val()),
            month = '' + (r.getMonth() + 1),
            day = '' + r.getDate(),
            year = r.getFullYear();
        var HolrDate = [day, month, year].join('-');
        $scope.user.returnDate = HolrDate;
        $scope.user.contactNumber = "+"+ $scope.user.countryCode +" "+ $scope.user.contactNumber;
        var tokenresult = JSON.parse(localStorage.getItem('authentication'));
        var tokenname = tokenresult.data;
        var headersOptions = {
            "g-recaptcha-response" :   $scope.token,
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + tokenname.authToken
        }
        if ($scope.isvalid === true) {
            $scope.timeload();
            $scope.user.enquiryServiceType = "Holiday";
            $scope.user.packageName = $scope.packageDetailView.packageName;
            $scope.artloading = true;
            $http.post(ServerService.packagePath + 'holiday/enquiry', JSON.stringify($scope.user), {

                headers: headersOptions
            }).then(function successCallback(response) {
                if (response.data.success) {
                    document.getElementById('enqurysbt').disabled = false;
                    Lobibox.alert('success', {
                        msg: response.data.message
                    })
                    $(".modal-backdrop").remove();
                    $scope.artloading = false;
                    $('#myModal').click();
                    FormVal.reset();
                } else {
                    document.getElementById('enqurysbt').disabled = false;
                    $scope.artloading = false;
                    Lobibox.alert('error', {
                        msg: response.data.errorMessage
                    })

                }

            }, function errorCallback(response) {
                document.getElementById('enqurysbt').disabled = false;
                if(response.status == 403){
                    var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
                    status.then(function(greeting) {
                       $scope.holsubmitForm(isvalid);
                    });                
                }
              });
        } else {
            document.getElementById('enqurysbt').disabled = false;
        }

    }




    $scope.holsubmitForm = function(isvalid) {
        document.getElementById('enqurysbt').disabled = true;
        $scope.recaptchaId = JSON.parse(localStorage.getItem('configKeys'));
        $scope.isvalid=isvalid;
            grecaptcha.ready(function() {
                grecaptcha.execute($scope.recaptchaId.google_recaptcha_public_key, {})
                        .then(function(token) {
                            // sessionStorage.setItem("captcha",token);
                            $scope.token=token;
                            $scope.tourDetailEnquiryForm();
                        });
            });
        

       

    }


    $scope.printDiv = function(tourprintDiv) {
        var printContents = document.getElementById('tourprintDiv').innerHTML;
        var popupWin = window.open('', '_blank', 'width=300,height=300');
        popupWin.document.open();
        popupWin.document.write('<html><head><link href="https://cdn.jsdelivr.net/npm/@mdi/font@5.x/css/materialdesignicons.min.css" rel="stylesheet"><link href="https://cdn.jsdelivr.net/npm/vuetify@2.x/dist/vuetify.min.css" rel="stylesheet"><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" /><link rel="stylesheet" href="assets/css/ion.rangeSlider.min.css" /><link rel="stylesheet" href="styles.css" /><link rel="stylesheet" href="assets/css/holiday.css" /><link rel="stylesheet" href="assets/css/jquery.desoslide.css" /><link href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.css" rel="stylesheet" /><link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css" /></head><body onload="window.print()">' + printContents + '</body></html>');
        popupWin.document.close();
    }

    $scope.countryList = {};
    $scope.dropdownList = function() {
        $scope.tokenresult = JSON.parse(localStorage.getItem('authentication'));
        var tokenname = $scope.tokenresult.data;
        var headersOptions = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + tokenname.authToken
        }
        $http.get(ServerService.listPath + 'list/phone-country', {

            headers: headersOptions
        }).then(function successCallback(response) {
            $scope.loading = false;
            $scope.disabled = false;
            if (response.data.success) {
                $scope.countryList = response.data.data;
            }
        }, function errorCallback(response) {
            if(response.status == 403){
                var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
                status.then(function(greeting) {
                    $scope.dropdownList();
                });                
            }
          });
    }
    $scope.dropdownList();

});