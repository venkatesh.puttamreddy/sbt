ouaApp.controller('homeController', function ($scope, $http, $location, $uibModal, baseUrlService, expiryService, ConstantService, ServerService, $rootScope) {
    $scope.currency = ConstantService.currency
    var currentURL = $location.absUrl();
    
    $scope.desimg = 'views/images/fileplaceholder.png';
    $scope.mobimg = 'views/images/fileplaceholder.png';
    $scope.relativePath = currentURL.substring(0, currentURL.lastIndexOf("/") + 1);
    $scope.carouselTime = $scope.carouselTime;
    $scope.description = $scope.description;
    $scope.tabActivate=sessionStorage.activeService;
    $scope.bookingResponse = [];
    $scope.selectingTrvlr=function(bookingData,header)
    {
        $scope.booking=bookingData
        $scope.headerTable=header
      
      $scope.modalInstance=$uibModal.open({
          templateUrl: 'myTestModal.tmpl.html',
          backdrop: 'static',
          keyboard: true,
          scope:$scope
      });
     
    }
   function getphncountrycode(){
        $scope.tokenresult = JSON.parse(sessionStorage.getItem('authToken'));
        var headersOptions = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + $scope.tokenresult 
        }
        $scope.searchReq = JSON.parse(sessionStorage.getItem('InsuranceSearchReq'));

        $http.get(ServerService.listPath + 'list/phone-country', {

            headers: headersOptions
        }).then(function successCallback(response) {
            $scope.loading = false;
            $scope.disabled = false;
            if (response.data.success) {
                $scope.countryList = response.data.data;
                sessionStorage.setItem("countryList",JSON.stringify($scope.countryList));
               
            }
        });
    }
    getphncountrycode();

    $scope.close=function(){
        $scope.modalInstance.dismiss();//$scope.modalInstance.close() also works I think
    };
    if( $scope.tabActivate=="1"){
        $scope.value="FLT";
    }else if( $scope.tabActivate=="2"){
        $scope.value="HTL";
    }else{
        $scope.value="INS"; 
    }
    $scope.allBooking=function(){
   var bookingrequest= {
        "page": 1,
        "pageSize":10,
        "take":10,
        // "skip": 1,
        // "getAllResults": true,
        "filter": {
            "logic": "and",
            "filters": [
                // {
                //     "field": "product",
                //     "operator": "eq",
                //     "value":  $scope.value
                // }
            ]
        },
        "sort": [
            {
                "field": "createTime",
                "dir": "desc"
            }
        ]
    }


    
    $scope.tokenresult = JSON.parse(sessionStorage.getItem('authToken'));
    var headersOptions = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + $scope.tokenresult
    }
    if($scope.tabActivate=="1"){
        bookings=[];
    $http.post(ServerService.listPath + 'search/bookingAir', JSON.stringify(bookingrequest), {
    
        headers: headersOptions
    }).then(function successCallback(response) {
       
        $scope.bookingResponse=response.data.data.data;
        for(var a=0; $scope.bookingResponse.length; a++) { 
            if($scope.bookingResponse[a].action!=null){                                                                                
         var booking=$scope.bookingResponse[a].action;
         var book=booking.replace(/[\[\]']/g,'' );
         var bookticket=book.split(',');
         $scope.bookingResponse[a].action=bookticket
            }
        }
       
    }, function errorCallback(response) {
        if(response.status == 403){
           var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
                          status.then(function(greeting) {
                            allBooking();
                   });
        
        }
       
      });
    }else if($scope.tabActivate=="2"){
        $http.post(ServerService.listPath + 'search/bookingHtl', JSON.stringify(bookingrequest), {
    
            headers: headersOptions
        }).then(function successCallback(response) {
           
            $scope.bookingResponse=response.data.data.data;
            for(var a=0; $scope.bookingResponse.length; a++) { 
                if($scope.bookingResponse[a].action!=null){                                                                                
             var booking=$scope.bookingResponse[a].action;
             var book=booking.replace(/[\[\]']/g,'' );
             var bookticket=book.split(',');
             $scope.bookingResponse[a].action=bookticket
                }
            }
            
        }, function errorCallback(response) {
            if(response.status == 403){
               var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
                              status.then(function(greeting) {
                                allBooking();
                       });
            
            }
           
          });
    }else{
        $http.post(ServerService.listPath + 'search/bookingIns', JSON.stringify(bookingrequest), {
    
            headers: headersOptions
        }).then(function successCallback(response) {
           
            $scope.bookingResponse=response.data.data.data;
            for(var a=0; $scope.bookingResponse.length; a++) { 
                if($scope.bookingResponse[a].action!=null){                                                                                
             var booking=$scope.bookingResponse[a].action;
             var book=booking.replace(/[\[\]']/g,'' );
             var bookticket=book.split(',');
             $scope.bookingResponse[a].action=bookticket
                }
            }
        }, function errorCallback(response) {
            if(response.status == 403){
               var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
                              status.then(function(greeting) {
                                allBooking();
                       });
            
            }
           
          });  
    }

    }

    $scope.ConfirmcancelTicket=function(valid){
        var remarks= $('#exampleFormControlTextarea1').val();
     
        if(remarks==""){
            $scope.errorMesg = "Enter All * (Mandatory) Fields"
            return false;
        }
        if( $scope.headerTable=='Cancel Ticket'){
        var remarks= $('#exampleFormControlTextarea1').val();
        $('.btn-block').prop('disabled', true);

        
        
        var cancelrequest={
            "pnrNo":  $scope.booking.bookingRefNo,
            "remarks": remarks
            }

            $scope.tokenresult = JSON.parse(sessionStorage.getItem('authToken'));
            var headersOptions = {
    'Content-Type': 'application/json',
    'Authorization': 'Bearer ' + $scope.tokenresult
}
$scope.artloadingcancellation=true;
$http.post(ServerService.serverPath + 'flight/pnr/cancel',cancelrequest, {
    headers: headersOptions
}).then(function successCallback(response) {
    if(response.data.success==true){
        $scope.artloadingcancellation=false;
    $scope.bookingResponse=response.data.data;
    $scope.modalInstance.dismiss();
    Lobibox.alert('success', {
        msg: response.data.message,
        callback: function (lobibox, type) {
            if (type === 'ok') {
                location.reload();
            }
        }
        });
    // Lobibox.alert('success', {
    //     msg: response.data.message
    // });
   
    }else{
        $scope.artloadingcancellation=false;
        $scope.modalInstance.dismiss();
        Lobibox.alert('error', {
            msg: response.data.errorMessage
        });
    }
})
        }else{
            var remarks= $('#exampleFormControlTextarea1').val();
            $('.btn-block').prop('disabled', true);
            var voidrequest={
                "pnrNo":  $scope.booking.bookingRefNo,
                "ticketNo": "",
                "remarks": remarks
                }
    
                $scope.tokenresult = JSON.parse(sessionStorage.getItem('authToken'));
                var headersOptions = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + $scope.tokenresult
    }
    $scope.artloadingcancellation=true;
    $http.post(ServerService.serverPath + 'flight/pnr/void',voidrequest, {
        headers: headersOptions
    }).then(function successCallback(response) {
        if(response.data.success==true){
            $scope.artloadingcancellation=false;
            $scope.bookingResponse=response.data.data;
            $scope.modalInstance.dismiss();
            Lobibox.alert('success', {
                msg: response.data.message,
                callback: function (lobibox, type) {
                    if (type === 'ok') {
                        location.reload();
                    }
                }
                });
           
            }else{
                $scope.artloadingcancellation=false;
                $scope.modalInstance.dismiss();
                Lobibox.alert('error', {
                    msg: response.data.errorMessage
                });
            }
    
    })
        }
    }
    
    $scope.viewHotelDetails = function(bookdata)
    {
        $rootScope.bookdata = bookdata;
        // https://api.convergentechnologies.com/gateway/ibp/api/v1/booking-dtls/F/6Z3QIS
        var token = JSON.parse(sessionStorage.authToken);
        var headersOptions = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token
        }
        $scope.artloading = true;
        $http.get(ServerService.serverPath + 'booking-dtls/H/'+$rootScope.bookdata.bookingRefNo, {
        
            headers: headersOptions
        }).then(function successCallback(response) {
        $scope.artloading = false;
        if(response.data.success)
            {
                sessionStorage.setItem('bookingItneryResponse',JSON.stringify(response.data));
                $rootScope.bookdata = bookdata;

                var modalInstance = $uibModal.open({
                    animation: $scope.animationEnabled,
                    ariaLabelledBy: 'modal-title',
                    ariaDescribedBy: 'modal-body',
                    templateUrl: 'ViewhotelBookings.html',
                    controller: 'ViewhotelBookingsController',
                    size: 'lg',
                    backdrop: true,
                    // size: size,
                    // appendTo: parentElem,
                    resolve: {
                        items: function() {
                            return $scope.items
                        }
                    }
                })
            }
            else
            {
                sessionStorage.removeItem('bookingItneryResponse');
                Lobibox.alert('error', {
                    msg: response.data.errorMessage
                });
            }
        }, function errorCallback(response) {
            if(response.status == 403){
               var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
                status.then(function(greeting) {
                    allBooking();
                });
                // $scope.autoAirlines(value);
            }
            // called asynchronously if an error occurs
            // or server returns response with an error status.
          });

 

    }

    $scope.InsurenceviewDetails = function(bookdata)
    {
        var token = JSON.parse(sessionStorage.authToken);
        var headersOptions = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token
        }
        $scope.artloading = true;
        $http.get(ServerService.serverPath + 'booking-dtls/I/'+bookdata.bookingRefNo, {
        
            headers: headersOptions
        }).then(function successCallback(response) {
            $scope.artloading = false;
            if(response.data.success)
            {
                sessionStorage.setItem('bookingItneryResponse',JSON.stringify(response.data));
                $rootScope.bookdata = bookdata;

                var modalInstance = $uibModal.open({
                    animation: $scope.animationEnabled,
                    ariaLabelledBy: 'modal-title',
                    ariaDescribedBy: 'modal-body',
                    templateUrl: 'ViewinsBookings.html',
                    controller: 'ViewinsBookingsController',
                    size: 'lg',
                    backdrop: true,
                    // size: size,
                    // appendTo: parentElem,
                    resolve: {
                        items: function() {
                            return $scope.items
                        }
                    }
                })
            }
            else
            {
                sessionStorage.removeItem('bookingItneryResponse');
                Lobibox.alert('error', {
                    msg: response.data.errorMessage
                });
            }
        }, function errorCallback(response) {
            if(response.status == 403){
               var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
                status.then(function(greeting) {
                    allBooking();
                });
                // $scope.autoAirlines(value);
            }
            // called asynchronously if an error occurs
            // or server returns response with an error status.
          });

    }

    $scope.viewDetails = function(bookdata)
    {
        $rootScope.bookdata = bookdata;
        // https://api.convergentechnologies.com/gateway/ibp/api/v1/booking-dtls/F/6Z3QIS
        var token = JSON.parse(sessionStorage.authToken);
        var headersOptions = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token
        }
        $scope.artloading = true;
        $http.get(ServerService.serverPath + 'booking-dtls/F/'+bookdata.bookingRefNo, {
        
            headers: headersOptions
        }).then(function successCallback(response) {
        $scope.artloading = false;
        if(response.data.success)
            {
                sessionStorage.setItem('bookingItneryResponse',JSON.stringify(response.data));       
                 var modalInstance = $uibModal.open({
                    animation: $scope.animationEnabled,
                    ariaLabelledBy: 'modal-title',
                    ariaDescribedBy: 'modal-body',
                    size: 'lg',
                    templateUrl: 'ViewflightBookings.html',
                    controller: 'ViewflightBookingsController',
                    backdrop: true,
                    // size: size,
                    // appendTo: parentElem,
                    resolve: {
                        items: function() {
                            return $scope.items
                        }
                    }
                })

                
                modalInstance.result.then(function() {
                    $scope.allBooking();
                })

            }
            else
            {
                sessionStorage.removeItem('bookingItneryResponse');
                Lobibox.alert('error', {
                    msg: response.data.errorMessage
                });
            }
        }, function errorCallback(response) {
            if(response.status == 403){
               var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
                status.then(function(greeting) {
                    allBooking();
                });
                // $scope.autoAirlines(value);
            }
            // called asynchronously if an error occurs
            // or server returns response with an error status.
          });
        
    }

    

    $scope.allBooking();

    $scope.issueTicket = function(agencyId)
    {
        var token = JSON.parse(sessionStorage.authToken);
        var headersOptions = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token
        }
        $scope.artloading = true;
        $http.get(ServerService.serverPath + 'flight/credit/issueTicket/'+agencyId, {
        
            headers: headersOptions
        }).then(function successCallback(response) {
        $scope.artloading = false;
        if(response.data.success)
            {
                $scope.curentpage = 5;
                Lobibox.alert('success', {
                    msg: response.data.message
                });

                $scope.allBooking();
            }
            else
            {
                Lobibox.alert('error', {
                    msg: response.data.errorMessage
                });
            }
            $uibModalInstance.dismiss('cancel');
            $rootScope.$emit("CallParentMethod", {});

        }, function errorCallback(response) {
            if(response.status == 403){
               var status = refreshService.refreshToken(tokenname.refreshToken,tokenname.authToken);
                status.then(function(greeting) {
                    allBooking();
                });
                // $scope.autoAirlines(value);
            }
            // called asynchronously if an error occurs
            // or server returns response with an error status.
          });
    }

    if (sessionStorage.getItem("paymentError")) {
        var paymentError = JSON.parse(sessionStorage.getItem("paymentError"));
        Lobibox.alert('error', {
            msg: paymentError.errorMessage,
            callback: function (lobibox, type) {
                if (type === 'ok') {
                    sessionStorage.removeItem("paymentError");
                }
            }
        });

    }

    $scope.logged = JSON.parse(localStorage.getItem('loginName'));
    $scope.ourservice = [];
    $(window).scroll(function () {
        var sticky = $("#scrollBar"),
            scroll = $(window).scrollTop();
        if (scroll >= 1 && scroll <= 5) {

        }

    });

    $scope.imagepath = ServerService.listPath + "file/download/";
   
    if ($scope.logged != undefined) {
        $scope.role = JSON.parse(sessionStorage.getItem('companyDetails'));
    }

    $scope.slideOptions = {
        loop: true,
        autoplay: true,
        smartSpeed: 500,
        autoplayTimeout: 100,
        autoplaySpeed: 250,
        singleItem: true,
        autoplayHoverPause: true,
        margin: 70,
        dots: false,
        nav: true,
        navText: '',
        responsive: {
            300: {
                items: 1,
            },
            480: {
                items: 2,
            },
            768: {
                items: 3,
            },
            1170: {
                items: 4,
            },
        }
    };
    $scope.selectArray = [{
        duration: null,
        orderNo: null,
        desimg: $scope.desimg,
        mobimg: $scope.mobimg
    }];

    $scope.privewSelectArray = Object.create($scope.selectArray);
    $scope.sportImages = [$scope.desimg, $scope.desimg, $scope.desimg];
    $scope.addMultipleDialog = function (index) {
        if ($scope.selectArray.length <= 4) {
            var newItem = {
                duration: null,
                orderNo: null,
                desimg: 'views/images/fileplaceholder.png',
                mobimg: 'views/images/fileplaceholder.png'
            };
            $scope.selectArray.push(newItem);
        }
        if ($scope.selectArray.length > 4) {
            $scope.addDisable = true;
        }
    }
    
    $scope.removeMultipleDialog = function (index) {
        $scope.selectArray.splice(index, 1);
        $scope.privewSelectArray.splice(index, 1);
        if ($scope.selectArray.length <= 4) {
            $scope.addDisable = false;
        }
    }

    $scope.changeImageOrder = function (event) {
        var sorted = $scope.privewSelectArray.sort(function (a, b) {
            return a.orderNo - b.orderNo;
        });
    }

    $scope.addCarouselTime = function () {
        angular.forEach($scope.selectArray, function (val, key) { })
    }

    $scope.imageUpload = function (event) {
        var files = event.target.files;

        for (var i = 0; i < files.length; i++) {
            var file = files[i];
            var reader = new FileReader();
            reader.onload = $scope.imageIsLoaded;
            reader.readAsDataURL(file);
        }
    }

    $scope.imageIsLoaded = function (e) {
        $scope.$apply(function () {
            $scope.img = e.target.result;
        });
    }

    $scope.desImageUpload = function (event) {
        var files = event.target.files;

        for (var i = 0; i < files.length; i++) {
            var file = files[i];
            var reader = new FileReader();
            reader.onload = $scope.desImageIsLoaded;
            reader.readAsDataURL(file);
        }
    }

    $scope.desImageIsLoaded = function (e) {
        $scope.$apply(function () {
            $scope.desimg = e.target.result;
            var temp = { desimg: $scope.desimg };
            $scope.selectArray[$scope.selectArray.length - 1].desimg = ($scope.desimg);

        });
    }

    $scope.mobImageUpload = function (event) {
        var files = event.target.files;

        for (var i = 0; i < files.length; i++) {
            var file = files[i];
            var reader = new FileReader();
            reader.onload = $scope.mobImageIsLoaded;
            reader.readAsDataURL(file);
        }
    }

    $scope.mobImageIsLoaded = function (e) {
        $scope.$apply(function () {
            $scope.mobimg = e.target.result;
            var temp = { mobimg: $scope.mobimg };
            $scope.selectArray[$scope.selectArray.length - 1].mobimg = ($scope.mobimg);
        });
    }

    $scope.bannerOptions = {
        loop: true,
        autoplay: true,
        smartSpeed: 500,
        autoplayTimeout: 10000,
        singleItem: true,
        autoplayHoverPause: true,
        margin: 30,
        dots: true,
        nav: true,
        navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>'],
        responsive: {
            300: {
                items: 1,
            },
            480: {
                items: 1,
            },
            768: {
                items: 1,
            },
            1170: {
                items: 1,
            },
        }
    };

    $scope.tabs = [{
        title: '<i class="icon-flights"></i> Flight + <i class="icon-hotels"></i> Hotel',
        content: 'Dynamic content 1'
    },
    {
        title: 'Dynamic Title 2',
        content: 'Dynamic content 2',
        disabled: true
    }
    ];

    $scope.states = [];
    $scope.flightfromAutoComplet = function (value) {
        if (value != null) {
            if (value.length >= 2) {
                var place = { 'queryString': value }
                $http.post(ServerService.serverPath + 'rest/repo/airport', '{"queryString" : "' + value + '"}', {
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                    }
                }).then(function (response) {

                    if (response.data != 'null' && response.data.length > 0) {
                        $scope.states = []
                        for (var i = 0; i < response.data.length; i++) {
                            var airfrom = {
                                Code: response.data[i].code,
                                Title: response.data[i].city + "," + response.data[i].country + "-" + response.data[i].name + "(" + response.data[i].code + ")",

                            }
                            $scope.states.push(airfrom);

                        }
                        $scope.states.sort();
                    }
                })
            }
        }
    };

    var intitialDate = {
        startDate: moment(),
        endDate: moment(),
        locale: {
            format: "D-MMM-YY"
        }
    };

    $scope.date = intitialDate;
    $scope.hotelDate = intitialDate;

    /* insuranceSearch function */
    $scope.travelInsurances = {};
    $scope.insurancedate = {
        startDate: null,
        endDate: null
    }

    $scope.travelInsurances.adultCount = 1
    $scope.travelInsurances.childCount = 0
    $scope.travelInsurances.infantCount = 0

    $scope.loading = false;
    $scope.disabled = false;
    $scope.insuranceSearch = function () {
        $scope.loading = true;
        $scope.disabled = true;
        var insurance_from = $('#insurancefrom_place').val();
        var insurance_to = $('#insuranceto_place').val();
        var insurance_date = $('#insurance_date').val();
        var insurance_adult = $('#insuranceAdult').val();
        var insurance_child = $('#insuranceChild').val();

        var startDate = insurance_date.split('-')[0];
        var endDate = insurance_date.split('-')[1];

        if (insurance_adult == "" || insurance_adult == undefined) {
            $scope.travelInsurances.adultCount = 0
        }
       
        if (insurance_child == "" || insurance_child == undefined) {
            $scope.travelInsurances.childCount = 0
        }
       
        $scope.searchInsure = {
            'adultCount': $scope.travelInsurances.adultCount,
            'childCount': $scope.travelInsurances.childCount,
            'departDateTime': startDate,
            'arrivalDateTime': endDate,
            'currency': 'BHD',
            'channel': 'IBE_DNABH',
            'cultureCode': 'EN',
            'countryCode': 'BH',
            'orgin': insurance_from,
            'destination': insurance_to,
            'infantCount': $scope.travelInsurances.infantCount,
            'srchType': 2
        }

        $scope.Insurance = {
            'travelInsurance': {
                'adultCount': $scope.travelInsurances.adultCount,
                'childCount': $scope.travelInsurances.childCount,
                'departDateTime': startDate,
                'arrivalDateTime': endDate,
                'currency': 'BHD',
                'channel': 'IBE_DNABH',
                'cultureCode': 'EN',
                'countryCode': 'BH',
                'orgin': insurance_from,
                'destination': insurance_to,
                'infantCount': $scope.travelInsurances.infantCount,
                'srchType': 2
            }
        }
        sessionStorage.setItem('insuranceSearchObj', JSON.stringify($scope.searchInsure))
        sessionStorage.setItem('InsuranceSearchRequest', JSON.stringify($scope.Insurance));
        $location.path('/searchingInsurance');
    }

    $scope.submitted = false;
    $scope.insuranceSearchForm = function (valid) {
    $scope.submitted = true;
        if (valid) {
            var insurance_from = $('#insurancefrom_place').val();
            var insurance_to = $('#insuranceto_place').val();
            var insurance_date = $('#insurance_date').val();
            var insurance_adult = $('#insuranceAdult').val();
            var insurance_child = $('#insuranceChild').val();

            var startDate = insurance_date.split('-')[0];
            var endDate = insurance_date.split('-')[1];

            if (insurance_adult == "" || insurance_adult == undefined) {
                $scope.travelInsurances.adultCount = 0
            }
            if (insurance_child == "" || insurance_child == undefined) {
                $scope.travelInsurances.childCount = 0
            }

            $scope.searchInsure = {
                'adultCount': $scope.travelInsurances.adultCount,
                'childCount': $scope.travelInsurances.childCount,
                'departDateTime': startDate,
                'arrivalDateTime': endDate,
                'currency': 'BHD',
                'channel': 'IBE_DNABH',
                'cultureCode': 'EN',
                'countryCode': 'BH',
                'orgin': insurance_from,
                'destination': insurance_to,
                'infantCount': $scope.travelInsurances.infantCount,
                'srchType': 2
            }

            $scope.Insurance = {
                'travelInsurance': {
                    'adultCount': $scope.travelInsurances.adultCount,
                    'childCount': $scope.travelInsurances.childCount,
                    'departDateTime': startDate,
                    'arrivalDateTime': endDate,
                    'currency': 'BHD',
                    'channel': 'IBE_DNABH',
                    'cultureCode': 'EN',
                    'countryCode': 'BH',
                    'orgin': insurance_from,
                    'destination': insurance_to,
                    'infantCount': $scope.travelInsurances.infantCount,
                    'srchType': 2
                }
            }
            sessionStorage.setItem('insuranceSearchObj', JSON.stringify($scope.searchInsure))
            sessionStorage.setItem('InsuranceSearchRequest', JSON.stringify($scope.Insurance));
            $location.path('/searchingInsurance');
        }
    }

    $scope.CabinClass = "Economy";
    $scope.updateCabinClass = function (value) {
        $scope.CabinClass = value;
    }
    $scope.AirlineValue = "All Airline";
    $scope.updateAirline = function (value) {
        $scope.AirlineValue = value;
    }

    $scope.opts = {
        opens: 'left',
        eventHandlers: {
            'show.daterangepicker': function (ev, picker) {
               
            }
        }
    }
    $scope.checkboxModel = {
        value: false
    };

    $scope.flightOpts = {
        singleDatePicker: $scope.checkboxModel.value,
        minDate: new Date()
    }

    $scope.oneWayChange = function () {
        $scope.date = ($scope.checkboxModel.value) ? $scope.date.startDate : $scope.date;
    }

    var booking = JSON.parse(sessionStorage.getItem('userBookingdetails'))
    var display = JSON.parse(localStorage.getItem('loginName'));
    sessionStorage.getItem('authToken');
    if (booking == '' || booking == null) {
        $scope.bookingHide1 = false;
    } else {
        $scope.bookingHide1 = booking.bookingHide1;
        $scope.bookingHide = booking.bookingHide;
        if (display != null) {
            $scope.dispName = display.fname;
        }
    }

}).controller('passengerCtrl', function ($http, $scope, $uibModalInstance, $uibModal) {
    $scope.paasCount = {}


    $scope.ok = function () {
        $uibModalInstance.close($scope.selected.item);
    };

    $scope.cancel = function () {
        $uibModalInstance.close();
    };;

    $scope.save = function () {
        sessionStorage.setItem('pasengerList', JSON.stringify($scope.paasCount));
        $uibModalInstance.dismiss('cancel');
    }
});
