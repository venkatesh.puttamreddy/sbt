ouaApp.controller('menuRedirectController', function ($scope, $http, $location, $uibModal, baseUrlService, expiryService, ConstantService, ServerService, $rootScope,$timeout,$window) {
    if(sessionStorage.mainMenu != undefined)
    {
        $rootScope.newtabmenuactive = sessionStorage.getItem("newactiveService")? JSON.parse(sessionStorage.getItem("newactiveService")):0;
        $scope.allMenus = JSON.parse(sessionStorage.getItem("mainMenu"));
        $location.path('/'+$scope.allMenus[$rootScope.newtabmenuactive].path.split('#!/')[1]);

    }

});